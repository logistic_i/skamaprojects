﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softone;

namespace SkamaWeb
{
    [WorksOn("SALDOC")]
    public class Saldoc : TXCode
    {
        private string _moduleName;
        public override void Initialize()
        {
            _moduleName = XModule.ObjName;
            base.Initialize();
        }

        public override void AfterInsert()
        {
            base.AfterInsert();
        }

        public override void AfterLocate()
        {
            var objParams = XModule.Params.Where(x => !string.IsNullOrEmpty(x)).ToList();

            try
            {
                var findoc = Convert.ToInt32(XModule.GetTable(_moduleName).Current["FINDOC"]);
                if (findoc < 0)
                {
                    return;
                }

                var webstateParam = objParams.Where(x => x.StartsWith("WEBSTATE")).FirstOrDefault();
                if (webstateParam == null)
                {
                    return;
                }
                var webState = webstateParam.Split('=').Last();
                if (webState == "NONE")
                {
                    return;
                }

                var webModuleParam = objParams.Where(x => x.StartsWith("WEBMODULE")).FirstOrDefault();
                if (webModuleParam == null)
                {
                    return;
                }    
                var webModule = webModuleParam.Split('=').Last();
                if (webModule != "1100")
                {
                    return;
                }

                var series = Convert.ToInt32(XModule.GetTable(_moduleName).Current["SERIES"]);
                var seriesTbl = XSupport.GetMemoryTable("SERIES");
                var fpIndex = seriesTbl.Find("SOSOURCE;SERIES", 1351, series);
                if (fpIndex == -1)
                {
                    return;
                }
                var fprms = seriesTbl.GetAsInteger(fpIndex, "FPRMS");
                if (fprms != 10105)
                {
                    return;
                }

                var client = new SoftoneClient(XSupport);
                var mantisDocId = client.CreateMantisDoc(XModule);
            }
            catch (Exception ex )
            {
                var error = ex.Message; //todo logs 
            }

            base.AfterLocate();
        }

    }
}
