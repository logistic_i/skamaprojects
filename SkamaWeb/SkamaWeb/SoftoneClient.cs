﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softone;

namespace SkamaWeb
{
    public class SoftoneClient
    {
        private readonly XSupport _xSupport;

        public SoftoneClient(XSupport xSupport)
        {
            _xSupport = xSupport;
        }

        internal int CreateMantisDoc(XModule xModule)
        {
            try
            {
                var whouse = Convert.ToInt32(xModule.GetTable("MTRDOC").Current["WHOUSE"]);
                var series = SoftoneTools.GetMantisSeries(_xSupport, whouse);
                if (series == -1)
                {
                    throw new Exception($"Δεν βρέθηκε σειρά ΠΑΡW για τον αποθηκευτικό χώρο {whouse}");
                }

                using (var Saldoc = _xSupport.CreateModule("SALDOC"))
                {
                    SoftoneTools.HideWarningsFromS1Module(Saldoc, _xSupport);
                    Saldoc.InsertData();

                    Saldoc.GetTable("SALDOC").Current["TRNDATE"] = Convert.ToDateTime(xModule.GetTable("SALDOC").Current["TRNDATE"]);
                    Saldoc.GetTable("SALDOC").Current["SERIES"] = series;
                    Saldoc.GetTable("SALDOC").Current["TRDR"] = Convert.ToInt32(xModule.GetTable("SALDOC").Current["TRDR"]);
                    Saldoc.GetTable("SALDOC").Current["CONVMODE"] = 1;

                    using (var Itelines = Saldoc.GetTable("ITELINES"))
                    {
                        using (var moduleLines = xModule.GetTable("ITELINES"))
                        {
                            for (int i = 0; i < moduleLines.Count; i++)
                            {
                                Itelines.Current.Append();
                                Itelines.Current["MTRL"] = moduleLines.GetAsInteger(i, "MTRL");
                                Itelines.Current["QTY1"] = moduleLines.GetAsFloat(i, "QTY1");
                                Itelines.Current["PRICE"] = moduleLines.GetAsFloat(i, "PRICE");
                                Itelines.Current["DISC1PRC"] = moduleLines.GetAsFloat(i, "DISC1PRC");
                                Itelines.Current["FINDOCS"] = moduleLines.GetAsInteger(i, "FINDOCS");
                                Itelines.Current["MTRLINESS"] = moduleLines.GetAsInteger(i, "MTRLINESS");
                                Itelines.Current.Post();
                            }
                        }
                    }

                    return Saldoc.PostData();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
