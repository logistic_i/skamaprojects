﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softone;

namespace SkamaWeb
{
    public static class SoftoneTools
    {
        public static void HideWarningsFromS1Module(XModule XModule, XSupport XSupport)
        {
            object otherModule = XSupport.GetStockObj("ModuleIntf", true);
            object[] myArray1;
            myArray1 = new object[3];
            myArray1[0] = XModule.Handle;
            myArray1[1] = "WARNINGS";
            myArray1[2] = "OFF";
            XSupport.CallPublished(otherModule, "SetParamValue", myArray1);
        }

        internal static int GetMantisSeries(XSupport xSupport, int whouse)
        {
            var seriesTbl = xSupport.GetMemoryTable("SERIES");
            var index = seriesTbl.Find("SOSOURCE;FPRMS;WHOUSE", 1351, 10143, whouse);
            if (index == -1)
            {
                return index;
            }
            var series = seriesTbl.GetAsInteger(index, "SERIES");
            return series;
        }
    }
}
