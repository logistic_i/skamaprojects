﻿using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using TecCom;
using TecCom.OpenMessaging.DTO;
using TecOrder;
using TecOrdersManager.Models;

namespace TecOrdersManager
{
    public class XMLManager
    {
        private string _unProcessedPath = null; 
        private string _validsPath = null;
        private string _invalidsPath = null;

        public XMLManager()
        {
            _unProcessedPath = Properties.Settings.Default.UnProcessed;
            _validsPath = Properties.Settings.Default.Valid;
            _invalidsPath = Properties.Settings.Default.Invalid;
        }

        public List<FileRet> GetUnprocessedFiles()
        {

            var listToRet = new List<FileRet>();

            if (string.IsNullOrEmpty(_unProcessedPath))
            {
                return listToRet;
            }

            Directory.CreateDirectory(_unProcessedPath);

            var files = Directory.EnumerateFiles(_unProcessedPath, "*.*", SearchOption.AllDirectories)
                            .Where(x => Path.GetExtension(x) == ".xml")
                            .Select(x => Path.GetFullPath(x)).ToList();
            if (!files.Any())
            {
                return listToRet;
            }

            foreach (var file in files)
            {
                var ret = new FileRet();
                var fileName = file.Split('\\').LastOrDefault();
                try
                {
                    var xml = XDocument.Load(file);
                    var documentDescendant = xml.Root.Descendants("Document").FirstOrDefault();
                    if (documentDescendant == null)
                    {
                        throw new Exception("Descendant named Document not Found!");
                    }
                    var docType = documentDescendant.Attribute("Type")?.Value;
                    if (docType == null)
                    {
                        throw new Exception("Attribute Type is missing!");
                    }
                    var docStr = xml.ToString();
                    switch (docType)
                    {
                        case nameof(ParameterTypeID.OrdRsp):
                            {
                                OrdRsp response = (OrdRsp)XMLHelper.GetObjectFromXML(typeof(OrdRsp), docStr);
                                ret.FileName = fileName;
                                ret.FullFileName = file;
                                ret.Document = response;
                                break;

                            }
                        case nameof(ParameterTypeID.DesAdv):
                            {
                                DesAdv response = (DesAdv)XMLHelper.GetObjectFromXML(typeof(DesAdv), docStr);
                                ret.FileName = fileName;
                                ret.FullFileName = file;
                                ret.Document = response;
                                break;
                            }
                        case nameof(ParameterTypeID.Invoice):
                            {
                                Invoice response = (Invoice)XMLHelper.GetObjectFromXML(typeof(Invoice), docStr);
                                ret.FileName = fileName;
                                ret.FullFileName = file;
                                ret.Document = response;
                                break;
                            }
                        default:
                            throw new Exception("Unknown Document Type!");
                    }
                }
                catch (Exception ex)
                {
                    Log.Error($"File : {fileName} | {ex.Message}");
                }
                listToRet.Add(ret);
            }

            return listToRet;
        }

        internal void MoveFile(string sourceFile, string fileName, string supplierNumber, bool isValid)
        {
            try
            {
                var subFolder = "ORDRSP";
                if (fileName.ToUpper().StartsWith("DESADV") || fileName.ToUpper().StartsWith("DISP"))
                {
                    subFolder = "DESADV";
                }
                else if (fileName.ToUpper().StartsWith("INVOIC"))
                {
                    subFolder = "INVOIC";
                }
                var targetFolder = $"{_validsPath}\\{subFolder}\\{supplierNumber}";
                if (!isValid)
                {
                    targetFolder = $"{_invalidsPath}\\{subFolder}\\{supplierNumber}";
                }
                Directory.CreateDirectory(targetFolder);
                var destinationFile = $"{targetFolder}\\{fileName}";
                if (File.Exists(destinationFile))
                {
                    File.Delete(destinationFile);
                }
                File.Move(sourceFile, destinationFile);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
    }
}