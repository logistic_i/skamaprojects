﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Softone;
using TecOrdersManager.Enums;
using TecOrdersManager.Models;

namespace TecOrdersManager
{
    public class SoftoneTools
    {
        private XSupport _xSupport = null;
        public SoftoneTools()
        {

        }

        public SoftoneTools(XSupport xSupport)
        {
            _xSupport = xSupport;
        }

        public XSupport Login(S1LoginParams loginParams)
        {
            if (string.IsNullOrWhiteSpace(loginParams.SoftonePath))
            {
                throw new Exception("SoftOne Path is null or empty");
            }

            XSupport.InitInterop(0, Path.Combine(loginParams.SoftonePath, "XDll.dll"));

            _xSupport = XSupport.Login(loginParams.XcoName
                                     , loginParams.UserName
                                     , loginParams.Password
                                     , loginParams.Company
                                     , loginParams.Branch
                                     , loginParams.LoginDate);
            return _xSupport;
        }


        public S1WebResponse SetOrUpdateData(string objectName, string formName, string key, object data, dynamic paramss)
        {
            paramss.WARNINGS = "OFF";
            paramss.NOMESSAGES = 1;

            var req = new S1WebRequest
            {
                Service = "setData",
                Object = objectName,
                ObjectParams = paramss,
                Form = formName,
                Data = data,
                Key = !string.IsNullOrEmpty(key) ? key : "0"
            };

            var requestStr = JsonConvert.SerializeObject(req);
            var resultStr = _xSupport.CallWebService(requestStr, out string contentType);
            var res = JsonConvert.DeserializeObject<S1WebResponse>(resultStr);
            return res;
        }

        public List<DataRow> GetSQLData(string query)
        {
            var data = new List<DataRow>();
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    data.AddRange(ds.CreateDataTable(true).AsEnumerable().ToList());
                }
            }
            return data;
        }

        public XModule LocateRecord(string objectName, int recordId)
        {
            using (var obj = _xSupport.CreateModule(objectName))
            {
                obj.LocateData(recordId);
                return obj;
            }
        }

        public void DeleteRecord(string objectName, int recordId)
        {
            using (var obj = _xSupport.CreateModule(objectName))
            {
                obj.LocateData(recordId);
                obj.DeleteData();
            }
        }

        public int GetIdFromMemoryTable(string tableName, string keys, string returnValue, params object[] values)
        {
            var seriesTbl = _xSupport.GetMemoryTable(tableName);
            var index = seriesTbl.Find(keys, values);
            if (index == -1)
            {
                return index;
            }
            var id = seriesTbl.GetAsInteger(index, returnValue);
            return id;
        }

        public static string SendMessageToUser(XSupport xSupport, int ConfirmUser, string note)
        {
            using (var module = xSupport.CreateModule("SOREMINDER"))
            {
                try
                {
                    using (var table = module.GetTable("SOREMINDER"))
                    {
                        module.InsertData();
                        table.Current.Insert();
                        table.Current["SOEXECUSER"] = ConfirmUser;
                        table.Current["SOVAL"] = string.Format("'[NOTES]\r\n{0}\r\n[REFS]\r\n'\r\n", note);
                        table.Current["OBJCAPTION"] = note;
                        module.PostData();
                    }
                    return "";
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
        }

        private void HideWarningsFromS1Module(XModule XModule, XSupport XSupport)
        {
            object otherModule = XSupport.GetStockObj("ModuleIntf", true);
            object[] myArray1;
            myArray1 = new object[3];
            myArray1[0] = XModule.Handle;
            myArray1[1] = "WARNINGS";
            myArray1[2] = "OFF";        
            XSupport.CallPublished(otherModule, "SetParamValue", myArray1);
        }
    }
}
