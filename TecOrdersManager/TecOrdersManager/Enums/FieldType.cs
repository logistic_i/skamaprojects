﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecOrdersManager.Enums
{
    public enum FieldType
    {
        Integer,
        String,
        DateTime,
        Double
    }
}
