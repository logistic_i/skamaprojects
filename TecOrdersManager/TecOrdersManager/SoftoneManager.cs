﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;
using Softone;
using TecOrder;
using TecOrdersManager.Models;

namespace TecOrdersManager
{
    public class SoftoneManager
    {
        private XSupport _xSupport = null;
        private SoftoneTools _tools = null;
        private readonly int _company;
        private int _defaultItem;
        private XMLManager _xmlManager;

        public SoftoneManager(XSupport xSupport, XMLManager xmlManager)
        {
            _xSupport = xSupport;
            _company = _xSupport.ConnectionInfo.CompanyId;
            _tools = new SoftoneTools(_xSupport);
            _defaultItem = GetDefaultItemId();
            _xmlManager = xmlManager;
        }

        internal void ProcessFiles(List<FileRet> ret)
        {
            var ordRsps = ret.Where(x => x.FileName.StartsWith("ORDRSP") || x.FileName.StartsWith("OrderResponse")).ToList();
            var desadvs = ret.Where(x => x.FileName.StartsWith("DESADV") || x.FileName.StartsWith("DispatchAdvice")).ToList();
            var invoices = ret.Where(x => x.FileName.StartsWith("INVOIC") || x.FileName.StartsWith("Invoice")).ToList();

            #region test

            //var boschOrders = ordRsps.Where(x => x.FileName.Contains("104004247")).ToList();
            //var boschDesadvs = desadvs.Where(x => x.FileName.Contains("104004247")).ToList();
            //var boschInvoices = invoices.Where(x => x.FileName.Contains("104004247")).ToList();

            //var testInvoice = boschInvoices.First().Document as Invoice;
            //var xx = testInvoice.InvoiceDetail.Select(x => x.OrderRef.SellerOrderNumber).Distinct().ToList();

            //var orderConfirmations = boschOrders.Where(x => xx.Contains(((OrdRsp)x.Document).OrdRspHeader.OrdRspId)).ToList();

            //foreach(var dd in orderConfirmations)
            //{
            //    ProcessOrderResponse(dd);
            //}

            //var desadvss = boschDesadvs.Where(x => xx.Contains(((DesAdv)x.Document).DesAdvHeader.OrderRef.SellerOrderNumber)).ToList();

            //foreach (var des in desadvss)
            //{
            //    ProcessDesadv(des);
            //}

            //var xxx = boschInvoices.Where(x => x.FileName == "Invoice_SKG_104004247_1135eac7-703e-4916-b4be-6bd8dc7391c9.xml").First();
            //ProcessInvoice(xxx);
            ////ProcessInvoice(boschInvoices.First());

            ////var relOrderConfirmations = testInvoice.InvoiceDetail.Select(x => x.);
            ///
            #endregion

            foreach (var doc in ordRsps)
            {
                ProcessOrderResponse(doc);
            }

            foreach (var doc in desadvs)
            {
                ProcessDesadv(doc);
            }

            foreach (var doc in invoices)
            {
                ProcessInvoice(doc);
            }
        }

        private void ProcessOrderResponse(FileRet ret)
        {
            var fileName = ret.FileName;
            var ordResp = (OrdRsp)ret.Document;
            var supplierNumber = ordResp.OrdRspHeader.SellerParty.PartyNumber;

            try
            {
                var buyerOrderNumber = ordResp.OrdRspHeader.OrderRef.BuyerOrderNumber;
                var supplierOrderNumber = ordResp.OrdRspHeader.OrderRef.SellerOrderNumber;
                var tecOrderId = ordResp.OrdRspHeader.OrdRspId;
                var trdr = GetSupplierId(supplierNumber);
                var partyNumber = ordResp.OrdRspHeader.BuyerParty.PartyNumber;
                var orderDateStr = ordResp.OrdRspHeader.OrdRspIssueDate.Date.Text;

                if (string.IsNullOrEmpty(orderDateStr))
                {
                    throw new Exception($"File : {fileName} | Date of referrence order is missing!");
                }

                DateTime.TryParseExact(orderDateStr, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime ordDate);
                if (ordDate == null)
                {
                    throw new Exception($"File : {fileName} | Error in parsing datetime object");
                }
                var fiscprd = ordDate.Year;

                var softoneOrderRespId = CheckIfDocIsAllreadyPosted(tecOrderId, trdr, orderDateStr);
                if (softoneOrderRespId > 0)
                {
                    throw new Exception($"File : {fileName} | Order Response of initial order {buyerOrderNumber} with id {supplierOrderNumber} is allready posted.");
                }

                var orderId = GetSoftoneOrderId(orderDateStr, buyerOrderNumber, trdr);
                //if (orderId == 0)
                //{
                //    throw new Exception($"File : {fileName} | Softone order {buyerOrderNumber} not found!");
                //}

                if (string.IsNullOrEmpty(partyNumber))
                {
                    throw new Exception($"File : {fileName} | Party Number is missing!");
                }

                var deliveryPartyNumber = ordResp.OrdRspHeader.DeliveryParty?.PartyNumber;
                var shipTo = !string.IsNullOrEmpty(deliveryPartyNumber) ? deliveryPartyNumber : partyNumber;
                var softoneBranch = GetSoftoneBranchByDeliveryNumber(shipTo, trdr);

                if (softoneBranch == 0)
                {
                    throw new Exception($"File : {fileName} | Unable to find softone branch for supplier {supplierNumber} and shipto {shipTo}!");
                }

                var series = GetOrderRespSeries(softoneBranch);
                if (series == -1)
                {
                    throw new Exception($"File : {fileName} | Unable to find softone series for response order!");
                }

                if (string.IsNullOrEmpty(tecOrderId))
                {
                    throw new Exception($"File : {fileName} | Order Id is missing!");
                }

                if (fiscprd != _xSupport.ConnectionInfo.YearId)
                {
                    var loginParams = new S1LoginParams
                    {
                        XcoName = Properties.Settings.Default.XCOName,
                        UserName = Properties.Settings.Default.UserName,
                        Password = Properties.Settings.Default.Password,
                        Company = Properties.Settings.Default.Company,
                        Branch = Properties.Settings.Default.Branch,
                        SoftonePath = Properties.Settings.Default.SoftOnePath,
                        LoginDate = ordDate.Date
                    };
                    var otherYearxSupport = _tools.Login(loginParams);
                    if (otherYearxSupport == null)
                    {
                        throw new Exception("Unable Login to Softone!");
                    }
                    _xSupport = otherYearxSupport;
                }

                var linenum = 9990001;

                var data = new
                {
                    PURDOC = new
                    {
                        TRNDATE = DateTime.ParseExact(orderDateStr, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"),
                        SERIES = series,
                        TRDR = trdr,
                        FINCODE = supplierOrderNumber,
                        CONVMODE = 1
                    },
                    MTRDOC = new
                    {
                        CCCTECORDID = ordResp.OrdRspHeader.OrdRspId,
                        CCCEDIFILENAME = fileName
                    },

                    ITELINES = (from x in ordResp.OrdRspItem.Where(x => x.ItemInstruction.Value != "Rejected")
                                let mtrl = GetMtrlBySupplierCode(x.ProductId.ProductNumber, trdr)
                                let delivDate = x.DeliveryInfo.FirstOrDefault()?.Date?.Text
                                let linenumm = linenum++
                                select new
                                {
                                    MTRL = mtrl == 0 ? _defaultItem : mtrl,
                                    QTY1 = Convert.ToDouble(x.ConfirmedQuantity.Quantity.Text.Replace('.', ',')),
                                    PRICE = Convert.ToDouble(x.ProductPrice.Price.Replace('.', ',')),
                                    FINDOCS = orderId,
                                    MTRLINESS = Convert.ToInt32(x.OrderItemRef.BuyerOrderItemRef),
                                    CCCEDICODE = mtrl == 0 ? x.ProductId.ProductNumber : null,
                                    CCCEDIDELIVDATE = !string.IsNullOrEmpty(delivDate) ?
                                                        DateTime.ParseExact(delivDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")
                                                        : null,
                                    CCCEDIPOSITION = x.PositionNumber,
                                    CCCBUYERREF = Convert.ToInt32(x.OrderItemRef.BuyerOrderItemRef),
                                    CCCSELLERREF = Convert.ToInt32(x.OrderItemRef.SellerOrderItemRef),
                                    LINENUM = linenumm
                                }).ToList()
                };

                dynamic paramss = new ExpandoObject();
                paramss.ISCUSTOM = 1;
                S1WebResponse res = _tools.SetOrUpdateData("PURDOC", "", "", data, paramss);
                if (res.Success)
                {
                    Log.Information($"File : {fileName} | Successful import of Order Response {tecOrderId}");
                    _xmlManager.MoveFile(ret.FullFileName, fileName, supplierNumber, true);
                }
                else
                {
                    Log.Error($"File : {fileName} | {res.Error}");
                    _xmlManager.MoveFile(ret.FullFileName, fileName, supplierNumber, false);
                }
            }
            catch (Exception ex)
            {
                Log.Error($"File : {fileName} | {ex.Message}");
                _xmlManager.MoveFile(ret.FullFileName, fileName, supplierNumber, false);
            }
        }

        private void ProcessDesadv(FileRet ret)
        {
            var desadv = (DesAdv)ret.Document;
            var fileName = ret.FileName;
            var supplierNumber = desadv.DesAdvHeader.SellerParty.PartyNumber;
            try
            {
                var buyerOrderNumber = desadv.DesAdvHeader.OrderRef.BuyerOrderNumber;
                var supplierOrderNumber = desadv.DesAdvHeader.OrderRef.SellerOrderNumber;
                var desadvId = desadv.DesAdvHeader.DesAdvId;
                var trdr = GetSupplierId(supplierNumber);
                var transportMode = desadv.DesAdvHeader.TransportDetails?.TransportMode;
                var transportMeans = desadv.DesAdvHeader.TransportDetails?.TransportTypeMeansCode;
                var transportMeansFree = desadv.DesAdvHeader.TransportDetails?.TransportTypeMeansFree;
                var carrierIdentification = desadv.DesAdvHeader.TransportDetails?.CarrierIdentificationILN;

                var partyNumber = desadv.DesAdvHeader?.BuyerParty?.PartyNumber;
                if (string.IsNullOrEmpty(partyNumber))
                {
                    throw new Exception($"File : {fileName} | Party Number is missing!");
                }

                var deliveryPartyNumber = desadv.DesAdvHeader?.DeliveryParty?.PartyNumber;
                var shipTo = !string.IsNullOrEmpty(deliveryPartyNumber) ? deliveryPartyNumber : partyNumber;
                var softoneBranch = GetSoftoneBranchByDeliveryNumber(shipTo, trdr);
                if (softoneBranch == 0)
                {
                    throw new Exception($"File : {fileName} | Unable to find softone branch for supplier {supplierNumber} and shipto {shipTo}!");
                }

                var series = GetDesadvSeries(softoneBranch);
                if (series == -1)
                {
                    throw new Exception($"File : {fileName} | Unable to find softone series for response order!");
                }

                var orderDateStr = desadv.DesAdvHeader.OrderRef.Date.Text;
                if (string.IsNullOrEmpty(orderDateStr))
                {
                    throw new Exception($"File : {fileName} | Date of referrence order is missing!");
                }

                DateTime.TryParseExact(orderDateStr, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime ordDate);
                if (ordDate == null)
                {
                    throw new Exception($"File : {fileName} | Error in parsing datetime object");
                }
                var fiscprd = ordDate.Year;

                var issueDateStr = desadv.DesAdvHeader.DesAdvIssueDate.Date.Text;
                if (string.IsNullOrEmpty(issueDateStr))
                {
                    throw new Exception($"File : {fileName} | Desadv Issue date is missing!");
                }

                DateTime.TryParseExact(issueDateStr, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime issueDate);
                if (issueDate == null)
                {
                    throw new Exception($"File : {fileName} | Error in parsing datetime object");
                }

                var findoc = CheckIfDocIsAllreadyPosted(desadvId, trdr, issueDateStr);
                if (findoc > 0)
                {
                    throw new Exception($"File : {fileName} | DESADV with id {desadvId} is allready posted.");
                }

                var deliveryDateStr = desadv.DesAdvHeader.DeliveryDate.Date.Text;
                var deliveryDate = DateTime.MinValue;
                if (!string.IsNullOrEmpty(deliveryDateStr))
                {
                    deliveryDate = DateTime.ParseExact(deliveryDateStr, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                }

                var listOfS1Packages = new List<S1Package>();

                if (desadv.Package != null && desadv.Package.Any())
                {
                    var desadvPackages = desadv.Package.Where(x => x.PkgId.Any()).ToList();

                    foreach (var package in desadvPackages)
                    {
                        var grossWeightTxt = package.Measurements?.MeasurementUnit.FirstOrDefault(x => x.MeasurementUnitQualifier == "AAB")?.Text;
                        double.TryParse(grossWeightTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out double grossWeight);
                        var netWeightTxt = package.Measurements?.MeasurementUnit.FirstOrDefault(x => x.MeasurementUnitQualifier == "AAA")?.Text;
                        double.TryParse(netWeightTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out double netWeight);
                        var volumeTxt = package.Measurements?.MeasurementUnit.FirstOrDefault(x => x.MeasurementUnitQualifier == "ABJ")?.Text;
                        double.TryParse(volumeTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out double volume);
                        var lengthTxt = package.Measurements?.MeasurementUnit.FirstOrDefault(x => x.MeasurementUnitQualifier == "LN")?.Text;
                        double.TryParse(lengthTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out double length);
                        var widthTxt = package.Measurements?.MeasurementUnit.FirstOrDefault(x => x.MeasurementUnitQualifier == "WD")?.Text;
                        double.TryParse(widthTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out double width);
                        var heightTxt = package.Measurements?.MeasurementUnit.FirstOrDefault(x => x.MeasurementUnitQualifier == "HT")?.Text;
                        double.TryParse(heightTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out double height);

                        string sscc = string.Empty;
                        string supplierNo = string.Empty;
                        if (package.PkgId != null && package.PkgId.Any())
                        {
                            sscc = package.PkgId.Where(x => x.PkgIdentNumberQualifier == "BJ").FirstOrDefault()?.PkgIdentNumber;
                            supplierNo = package.PkgId.Where(x => x.PkgIdentNumberQualifier == "BN").FirstOrDefault()?.PkgIdentNumber;
                        }

                        var s1Package = new S1Package
                        {
                            NetWeight = netWeight != default(double) ? netWeight : (double?)null,
                            GrossWeight = grossWeight != default(double) ? grossWeight : (double?)null,
                            Volume = volume != default(double) ? volume : (double?)null,
                            Length = length != default(double) ? length : (double?)null,
                            Width = width != default(double) ? width : (double?)null,
                            Height = height != default(double) ? height : (double?)null,
                            FreeText = package.FreeText?.Text,
                            PkgId = int.Parse(package.PkgNumber),
                            SSCC = !string.IsNullOrEmpty(sscc) ? sscc : null,
                            SupplierNo = !string.IsNullOrEmpty(supplierNo) ? supplierNo : null
                        };
                        listOfS1Packages.Add(s1Package);
                    }
                }

                var docItems = desadv.Package.SelectMany(x => x.PkgItem).ToList();

                var itemCodes = docItems.Select(x => x.ProductId.ProductNumber).ToList();
                var ress = string.Join(",", itemCodes);
                var linenum = 9990001;

                var data = new
                {
                    PURDOC = new
                    {
                        TRNDATE = DateTime.ParseExact(issueDateStr, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"),
                        SERIES = series,
                        TRDR = trdr,
                        FINCODE = desadvId,
                        CONVMODE = 1
                    },
                    MTRDOC = new
                    {
                        CCCTECORDID = desadvId,
                        CCCEDIFILENAME = fileName,
                        DELIVDATE = deliveryDate != DateTime.MinValue ? deliveryDate : (DateTime?)null
                    },
                    ITELINES = (from x in docItems
                                let mtrl = GetMtrlBySupplierCode(x.ProductId.ProductNumber, trdr)
                                let softoneOrderNumber = x.OrderRef.SellerOrderNumber
                                let buyerOrderItemRef = !string.IsNullOrEmpty(x.OrderItemRef.BuyerOrderItemRef) ? int.Parse(x.OrderItemRef.BuyerOrderItemRef) : 0
                                let sellerOrderItemRef = !string.IsNullOrEmpty(x.OrderItemRef.SellerOrderItemRef) ? int.Parse(x.OrderItemRef.SellerOrderItemRef) : 0
                                let ordDateStr = x.OrderRef.Date.Text
                                let findocs = GetSoftoneOrderId(ordDateStr, softoneOrderNumber, trdr)
                                let mtrliness = GetRefLine(findocs, sellerOrderItemRef)
                                let linenumm = linenum++
                                select new
                                {
                                    MTRL = mtrl == 0 ? _defaultItem : mtrl,
                                    QTY1 = double.Parse(x.DeliveredQuantity.Quantity.Text.Replace('.', ',')),
                                    FINDOCS = mtrliness > 0 ? findocs > 0 ? findocs : (int?)null : (int?)null,
                                    MTRLINESS = mtrliness > 0 ? mtrliness : (int?)null,
                                    CCCEDICODE = mtrl == 0 ? x.ProductId.ProductNumber : null,
                                    CCCPKGID = desadv.Package.Where(y => y.PkgItem.Contains(x)).FirstOrDefault()?.PkgNumber,
                                    CCCEDIPOSITION = x.PositionNumber,
                                    CCCBUYERREF = buyerOrderItemRef,
                                    CCCSELLERREF = sellerOrderItemRef,
                                    LINENUM = linenumm
                                }).ToList(),
                    CCCPACKINGLIST = listOfS1Packages.Select(x => new
                    {
                        GROSSWEIGHT = x.GrossWeight,
                        NETWEIGHT = x.NetWeight,
                        PKGVOLUME = x.Volume,
                        PKGLENGTH = x.Length,
                        PKGWIDTH = x.Width,
                        PKGHEIGHT = x.Height,
                        SSCC = x.SSCC,
                        SUPNO = x.SupplierNo,
                        PKGID = x.PkgId,
                    }).ToList()
                };

                dynamic paramss = new ExpandoObject();
                paramss.ISCUSTOM = 1;
                S1WebResponse res = _tools.SetOrUpdateData("PURDOC", "EDI", "", data, paramss);
                if (res.Success)
                {
                    var newId = Convert.ToInt32(res.Id);
                    using (var Purdoc = _xSupport.CreateModule("PURDOC"))
                    {
                        Purdoc.LocateData(newId);
                        Purdoc.PostData();
                    }
                    Log.Information($"File : {fileName} | Successful import of DESADV {desadvId}");
                    _xmlManager.MoveFile(ret.FullFileName, fileName, supplierNumber, true);
                }
                else
                {
                    Log.Error($"File : {fileName} | {res.Error}");
                    _xmlManager.MoveFile(ret.FullFileName, fileName, supplierNumber, false);
                }
            }
            catch (Exception ex)
            {
                Log.Error($"File : {fileName} | {ex.Message}");
                _xmlManager.MoveFile(ret.FullFileName, fileName, supplierNumber, false);
            }
        }

        private void ProcessInvoice(FileRet ret)
        {
            var invoice = (Invoice)ret.Document;
            var fileName = ret.FileName;
            var supplierNumber = invoice.InvoiceHeader.SellerParty.PartyNumber;

            try
            {
                var invoiceId = invoice.InvoiceHeader.InvoiceId;
                if (string.IsNullOrEmpty(invoiceId))
                {
                    throw new Exception($"File : {fileName} | Ιncorrect message format! Mandatory field InvoiceId is null or empty.");
                }

                var issueDate = invoice.InvoiceHeader.InvoiceIssueDate.Date.Text;
                if (string.IsNullOrEmpty(issueDate))
                {
                    throw new Exception($"File : {fileName} | Ιncorrect message format! Mandatory field IssueDate is null or empty.");
                }

                var invoiceDate = DateTime.ParseExact(issueDate, "yyyyMMdd", CultureInfo.InvariantCulture);
                var fiscprd = invoiceDate.Year;

                var dueDate = invoice.InvoiceHeader.InvoiceDueDate.Date.Text;
                if (string.IsNullOrEmpty(dueDate))
                {
                    throw new Exception($"File : {fileName} | Ιncorrect message format! Mandatory field DueDate is null or empty.");
                }
               
                if (string.IsNullOrEmpty(supplierNumber))
                {
                    throw new Exception($"File : {fileName} | Ιncorrect message format! Mandatory field Seller Party Number is null or empty.");
                }

                var buyerNumber = invoice.InvoiceHeader.BuyerParty.PartyNumber;
                if (string.IsNullOrEmpty(buyerNumber))
                {
                    throw new Exception($"File : {fileName} | Ιncorrect message format! Mandatory field Buyer Party Number is null or empty.");
                }

                var deliveryNumber = invoice.InvoiceHeader.DeliveryParty?.PartyNumber;
                var shipTo = !string.IsNullOrEmpty(deliveryNumber) ? deliveryNumber : buyerNumber;
                var invoiceParty = invoice.InvoiceHeader.InvoiceOrg.InvoiceParty.PartyNumber;
                if (string.IsNullOrEmpty(invoiceParty))
                {
                    throw new Exception($"File : {fileName} | Ιncorrect message format! Mandatory field Invoice Party Number is null or empty.");
                }

                var trdr = GetSupplierId(supplierNumber);
                if (trdr == 0)
                {
                    throw new Exception($"File : {fileName} | Unable to find supplier {supplierNumber}. Please check Edi Configuration.");
                }

                var trdCategory = GetSupplierΑccountingCategory(trdr);
                if (trdCategory == 0)
                {
                    throw new Exception($"File : {fileName} | Accounting category missing for Supplier {supplierNumber}");
                }

                var softoneBranch = GetSoftoneBranchByDeliveryNumber(shipTo, trdr);
                if (softoneBranch == 0)
                {
                    throw new Exception($"File : {fileName} | Unable to find softone branch for supplier {supplierNumber} and shipto {shipTo}! Please check EDI Configuration");
                }

                var series = GetInvoiceSeries(softoneBranch, trdCategory);
                if (series == -1)
                {
                    throw new Exception($"File : {fileName} | Unable to find Softone Series for Softone branch {softoneBranch} and accounting category {trdCategory}.");
                }

                var findoc = CheckIfDocIsAllreadyPosted(invoiceId, trdr, issueDate);
                if (findoc > 0)
                {
                    throw new Exception($"File : {fileName} | INVOICE with id {invoiceId} is allready posted.");
                }

                if (fiscprd != _xSupport.ConnectionInfo.YearId)
                {
                    var loginParams = new S1LoginParams
                    {
                        XcoName = Properties.Settings.Default.XCOName,
                        UserName = Properties.Settings.Default.UserName,
                        Password = Properties.Settings.Default.Password,
                        Company = Properties.Settings.Default.Company,
                        Branch = Properties.Settings.Default.Branch,
                        SoftonePath = Properties.Settings.Default.SoftOnePath,
                        LoginDate = invoiceDate.Date
                    };
                    var otherYearxSupport = _tools.Login(loginParams);
                    if (otherYearxSupport == null)
                    {
                        throw new Exception("Unable Login to Softone!");
                    }
                    _xSupport = otherYearxSupport;
                }

                var desadvRef = invoice.InvoiceHeader.DesAdvRef?.DocumentNumber;
                var desadvDate = invoice.InvoiceHeader.DesAdvRef?.Date.Text;
                var instructions = invoice.InvoiceHeader.PaymentInstruction;
                var paymentTerms = invoice.InvoiceHeader.PaymentTerms;
                var taxTreatmentCode = invoice.InvoiceHeader.TaxTreatmentCode.Value;

                var PURDOC = new
                {
                    TRNDATE = invoiceDate,
                    SERIES = series,
                    TRDR = trdr,
                    FINCODE = invoiceId,
                    COMMENTS = instructions,
                    //CONVMODE = 1 /*ΕΝΔΕΧΟΜΕΝΩΣ ΝΑ ΑΦΑΙΡΕΘΕΙ*/
                };

                var MTRDOC = new
                {
                    CCCTECORDID = invoiceId,
                    CCCEDIFILENAME = fileName,
                    DELIVDATE = !string.IsNullOrEmpty(dueDate)
                                ? DateTime.ParseExact(dueDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd")
                                : null
                };

                var invoiceLines = new List<InvoiceLine>();

                var totalVal = invoice.InvoiceDetail.Sum(x => double.Parse(x.TotalValue.Amount, NumberStyles.Any, CultureInfo.InvariantCulture));


                var linenum = 9990001;
                foreach (var detail in invoice.InvoiceDetail)               
                {
                    var mtrlTuple = GetMtrlAndTypeBySupplierCode(detail.ProductId.ProductNumber, trdr);
                    var mtrl = mtrlTuple.Item1;
                    var mtrtype = mtrlTuple.Item2;
                    var qty1 = double.Parse(detail.Quantity.Text, NumberStyles.Any, CultureInfo.InvariantCulture);
                    var price = 0.0;
                    if (detail.TotalPrice != null)
                    {
                        price = double.Parse(detail.TotalPrice.Amount, NumberStyles.Any, CultureInfo.InvariantCulture) /
                        double.Parse(detail.Quantity.Text, NumberStyles.Any, CultureInfo.InvariantCulture);
                    }
                    var taxPercent = double.Parse(detail.Tax.Percent, NumberStyles.Any, CultureInfo.InvariantCulture);
                    var taxId = GetVatIdByPercent(taxPercent);
                    double disc1prc = 0;
                    double disc2prc = 0;
                    double disc3prc = 0;
                    double disc1val = 0;
                    double disc2val = 0;
                    double disc3val = 0;
                    var discountList = detail.AllowOrCharge.Where(x => x.AllowOrChargeIdentifier.Value.ToLower() == "allow").ToList();
                    for (int i = 0; i < 3; i++)
                    {
                        if (discountList.ElementAtOrDefault(i) != null)
                        {
                            var prc = discountList[i].Percent;

                            if (i == 0)
                            {
                                if (!string.IsNullOrEmpty(discountList[i].Percent))
                                {
                                    disc1prc = double.Parse(discountList[i].Percent, NumberStyles.Any, CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(discountList[i].Amount))
                                    {
                                        disc1val = double.Parse(discountList[i].Amount, NumberStyles.Any, CultureInfo.InvariantCulture);
                                    }
                                }
                            }
                            if (i == 1)
                            {
                                if (!string.IsNullOrEmpty(discountList[i].Percent))
                                {
                                    disc2prc = double.Parse(discountList[i].Percent, NumberStyles.Any, CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(discountList[i].Amount))
                                    {
                                        disc2val = double.Parse(discountList[i].Amount, NumberStyles.Any, CultureInfo.InvariantCulture);
                                    }
                                }
                            }
                            if (i == 2)
                            {
                                if (!string.IsNullOrEmpty(discountList[i].Percent))
                                {
                                    disc3prc = double.Parse(discountList[i].Percent, NumberStyles.Any, CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(discountList[i].Amount))
                                    {
                                        disc3val = double.Parse(discountList[i].Amount, NumberStyles.Any, CultureInfo.InvariantCulture);
                                    }
                                }
                            }
                        }
                    }
                    var chargeList = detail.AllowOrCharge.Where(x => x.AllowOrChargeIdentifier.Value.ToLower() == "charge").ToList();
                    if (chargeList.Any())
                    {
                        
                    }
                    var returnableAmount = 0.0;
                    var returnable = discountList.FirstOrDefault(x => x.AllowOrChargeCode == "ReturnableContainer")?.Amount;
                    if (returnable != null)
                    {
                        returnableAmount = double.Parse(returnable, NumberStyles.Any, CultureInfo.InvariantCulture);
                    }
                    var lineval = double.Parse(detail.TotalValue.Amount, NumberStyles.Any, CultureInfo.InvariantCulture) - returnableAmount;
                    var findocs = GetSoftoneOrderId(detail.DesAdvRef.Date.Text, detail.DesAdvRef.DocumentNumber, trdr);
                    int.TryParse(detail.OrderItemRef.BuyerOrderItemRef, NumberStyles.Any, CultureInfo.InvariantCulture, out int buyerRef);
                    int.TryParse(detail.OrderItemRef.SellerOrderItemRef, NumberStyles.Any, CultureInfo.InvariantCulture, out int sellerRef);
                    var mtrliness = GetRefLine(findocs, sellerRef);
                    invoiceLines.Add(new InvoiceLine
                    {
                        Mtrl = mtrl == 0 ? _defaultItem : mtrl,
                        Code = detail.ProductId.ProductNumber,
                        Qty1 = qty1,
                        Price = price,
                        Disc1Prc = disc1prc != default(double) ? disc1prc : (double?)null,
                        Disc2Prc = disc2prc != default(double) ? disc2prc : (double?)null,
                        Disc3Prc = disc3prc != default(double) ? disc3prc : (double?)null,
                        Disc1Val = disc1prc != default(double) ? disc1prc : (double?)null,
                        Disc2Val = disc2prc != default(double) ? disc2prc : (double?)null,
                        Disc3Val = disc3prc != default(double) ? disc3prc : (double?)null,
                        TaxId = taxId,
                        LineVal = lineval,
                        Findocs = findocs > 0 ? findocs : (int?)null,
                        Mtrliness = mtrliness > 0 ? mtrliness : (int?)null,
                        PositionNumber = int.Parse(detail.PositionNumber),
                        BuyerRef = buyerRef != default(int) ? buyerRef : (int?)null,
                        SellerRef = sellerRef != default(int) ? sellerRef : (int?)null,
                        Linenum = linenum
                    });

                    linenum++;
                }

                var ITELINES = invoiceLines.Select(x => new
                {
                    MTRL = x.Mtrl,
                    QTY1 = x.Qty1,
                    PRICE = x.Price > 0.0 ? x.Price : (double?)null,
                    DISC1PRC = x.Disc1Prc,
                    DISC2PRC = x.Disc2Prc,
                    DISC3PRC = x.Disc3Prc,
                    LINEVAL = x.LineVal,
                    FINDOCS = x.Findocs,
                    MTRLINESS = x.Mtrliness,
                    CCCEDICODE = x.Mtrl == _defaultItem ? x.Code : null,
                    CCCEDIPOSITION = x.PositionNumber,
                    CCCBUYERREF = x.BuyerRef,
                    CCCSELLERREF = x.SellerRef,
                    LINENUM = x.Linenum
                }).ToList();


                var invoiceSummary = invoice.InvoiceSummary;
                var listOfExtraCharges = invoiceSummary.AllowOrCharge;
                var vatPercent = double.Parse(invoiceSummary.TaxTotals.Tax.Percent, NumberStyles.Any, CultureInfo.InvariantCulture);
                var vatVal = double.Parse(invoiceSummary.TaxTotals.Tax.Amount, NumberStyles.Any, CultureInfo.InvariantCulture);// to become list
                var subVal = double.Parse(invoiceSummary.InvoiceTotals.InvoiceNetValue.Amount, NumberStyles.Any, CultureInfo.InvariantCulture);

                var vatId = GetVatIdByPercent(vatPercent);

                var VATANAL = new
                {
                    VAT = vatId,
                    SUBVAL = subVal,
                    VATVAL = vatVal
                };

                var data = new
                {
                    PURDOC,
                    MTRDOC,
                    ITELINES,
                    VATANAL
                };

                dynamic paramss = new ExpandoObject();
                paramss.ISCUSTOM = 1;
                S1WebResponse res = _tools.SetOrUpdateData("PURDOC", "EDI", "", data, paramss);
                if (res.Success)
                {
                    var newId = int.Parse(res.Id);
                    UpdateVolumeFieldWithLineval(newId);
                    Log.Information($"File : {fileName} | Successful import of INVOICE {invoiceId}");
                    _xmlManager.MoveFile(ret.FullFileName, fileName, supplierNumber, true);
                }
                else
                {
                    Log.Error($"File : {fileName} | {res.Error}");
                    _xmlManager.MoveFile(ret.FullFileName, fileName, supplierNumber, false);
                }
            }
            catch (Exception ex)
            {
                Log.Error($"File : {fileName} | {ex.Message}");
                _xmlManager.MoveFile(ret.FullFileName, fileName, supplierNumber, false);
            }
        }

        private void UpdateVolumeFieldWithLineval(int newId)
        {
            try
            {
                var query = $"UPDATE MTRLINES SET NUM01 = LNETLINEVAL, NUM02 = QTY1, VOLUME = LINEVAL WHERE MTRTYPE <> 3 AND FINDOC = {newId}";
                _xSupport.ExecuteSQL(query, null);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }

        private int GetOrderRespSeries(int softoneBranch)
        {
            return _tools.GetIdFromMemoryTable("SERIES", "COMPANY;SOSOURCE;BRANCH;FPRMS", "SERIES", _company, 1251, softoneBranch, 20142);
        }

        private int GetDesadvSeries(int softoneBranch)
        {
            return _tools.GetIdFromMemoryTable("SERIES", "COMPANY;SOSOURCE;BRANCH;FPRMS", "SERIES", _company, 1251, softoneBranch, 20143);
        }

        private int GetInvoiceSeries(int softoneBranch, int trdCategory)
        {
            int fprms;
            switch (trdCategory)
            {
                case 5000:
                    fprms = 20144;
                    break;
                case 5001:
                    fprms = 20144;
                    break;
                case 5002:
                    fprms = 20144;
                    break;
                case 5003:
                    fprms = 20145;
                    break;
                default:
                    fprms = 20144;
                    break;
            }
            return _tools.GetIdFromMemoryTable("SERIES", "COMPANY;SOSOURCE;BRANCH;FPRMS", "SERIES", _company, 1251, softoneBranch, fprms);
        }

        private int GetSoftoneBranchByDeliveryNumber(string shipTo, int trdr)
        {
            var query = $@"SELECT 
	                            L.CUSCODE, 
	                            ISNULL(L.DELIVERYNUMBER,L.CUSCODE) AS DELIVERYNUMBER,
	                            CASE L.SHIPTO WHEN 2 THEN 1001 ELSE 1002 END AS SHIPTO
                            FROM CCCEDI C
                            INNER JOIN CCCEDILOCATIONS L ON L.CCCEDI = C.CCCEDI
                            WHERE 
	                            C.TRDR = {trdr}
                                AND ISNULL(L.DELIVERYNUMBER,L.CUSCODE) = '{shipTo}'";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    return ds.GetAsInteger(0, "SHIPTO");
                }
                else
                {
                    return 0;
                }
            }
        }

        private int GetDefaultItemId()
        {
            var query = $"SELECT MTRL FROM MTRL WHERE COMPANY = {_company} AND SODTYPE = 51 AND ISACTIVE = 1 AND CODE = 'EDI9999'";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    return ds.GetAsInteger(0, "MTRL");
                }
                else
                {
                    return 0;
                }
            }
        }

        private int GetVatIdByPercent(double vatPercent)
        {
            using (var ds = _xSupport.GetSQLDataSet($"SELECT VAT FROM VAT WHERE ISACTIVE = 1 AND PERCNT = {vatPercent}"))
            {
                if (ds.Count > 0)
                {
                    return ds.GetAsInteger(0, "VAT");
                }
                else
                {
                    return 0;
                }
            }
        }

        internal bool DefaultItemExists()
        {
            return _defaultItem > 0;
        }

        private int GetMtrlBySupplierCode(string makerCode, int trdr)
        {
            var query = $@"SELECT 
	                            M.MTRL 
                            FROM 
	                            MTRSUPCODE S 
	                            INNER JOIN MTRL M ON M.MTRL = S.MTRL 
                            WHERE 
	                            S.MTRSUPCODE = '{makerCode}' 
	                            AND S.TRDR = {trdr}
	                            AND M.COMPANY = {_company}
	                            AND M.SODTYPE = 51
	                            AND M.ISACTIVE = 1";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    return ds.GetAsInteger(0, "MTRL");
                }
                else
                {
                    return 0;
                }
            }
        }

        private Tuple<int, int> GetMtrlAndTypeBySupplierCode(string makerCode, int trdr)
        {
            var query = $@"SELECT 
	                            M.MTRL, M.MTRTYPE 
                            FROM 
	                            MTRSUPCODE S 
	                            INNER JOIN MTRL M ON M.MTRL = S.MTRL 
                            WHERE 
	                            S.MTRSUPCODE = '{makerCode}' 
	                            AND S.TRDR = {trdr}
	                            AND M.COMPANY = {_company}
	                            AND M.SODTYPE = 51
	                            AND M.ISACTIVE = 1";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    return new Tuple<int, int>(ds.GetAsInteger(0, "MTRL"), ds.GetAsInteger(0, "MTRTYPE"));
                }
                else
                {
                    return new Tuple<int, int>(0, 0);
                }
            }
        }

        private int GetSoftoneOrderId(string orderDateStr, string sellerOrderNumber, int trdr)
        {
            DateTime.TryParseExact(orderDateStr, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime ordDate);
            if (ordDate == default(DateTime))
            {
                return 0;
            }
            var fiscprd = ordDate.Year;
            var period = ordDate.Month;
            var query = $@"SELECT F.FINDOC FROM FINDOC F INNER JOIN MTRDOC M ON M.FINDOC = F.FINDOC WHERE F.COMPANY = 1001 
                            AND F.SOSOURCE = 1251 AND F.FPRMS = 20146
                            AND  M.CCCTECORDID = '{sellerOrderNumber}' AND F.TRDR = {trdr}";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    return ds.GetAsInteger(0, "FINDOC");
                }
                else
                {
                    return 0;
                }
            }
        }

        private int GetRefLine(int findocs, int sellerOrderItemRef)
        {
            var query = $"SELECT MTRLINES FROM MTRLINES WHERE FINDOC = {findocs} AND CCCSELLERREF = {sellerOrderItemRef}";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    return ds.GetAsInteger(0, "MTRLINES");
                }
                else
                {
                    return 0;
                }
            }
        }

        private int GetSupplierId(string supplierNumber)
        {
            var query = $"SELECT TRDR FROM TRDR WHERE COMPANY = {_company} AND SODTYPE = 12 AND ISACTIVE = 1 AND CODE = '{supplierNumber}'";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    return ds.GetAsInteger(0, "TRDR");
                }
                else
                {
                    return 0;
                }
            }
        }

        private int GetSupplierΑccountingCategory(int trdr)
        {
            var q = $"SELECT ISNULL(TRDCATEGORY,0) AS ACC FROM TRDR WHERE TRDR = {trdr}";
            using (var ds = _xSupport.GetSQLDataSet(q, null))
            {
                if (ds.Count > 0)
                {
                    return ds.GetAsInteger(0, "ACC");
                }
                else
                {
                    return 0;
                }
            }
        }

        private int CheckIfDocIsAllreadyPosted(string tecOrderId, int trdr, string orderDateStr)
        {
            var query = $@"SELECT F.FINDOC FROM FINDOC F INNER JOIN MTRDOC M ON M.FINDOC = F.FINDOC 
                           WHERE F.COMPANY = {_company} AND F.SOSOURCE = 1251 AND F.TRNDATE = '{orderDateStr}' 
                           AND F.TRDR = {trdr} AND M.CCCTECORDID = '{tecOrderId}'";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    return ds.GetAsInteger(0, "FINDOC");
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
