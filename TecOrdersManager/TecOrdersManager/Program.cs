﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TecOrdersManager.Models;
using TecOrder;

namespace TecOrdersManager
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                           .MinimumLevel.Debug()
                           .WriteTo.File("logs\\edi.log", rollingInterval: RollingInterval.Day, retainedFileCountLimit: 7)
                           .CreateLogger();
            try
            {
                //var xmlPath = Properties.Settings.Default.UnProcessed;
                //var tecClient = new TecClient("7182000040330", "Rc62YxTb");
                //var listOfOrderResponses = tecClient.GetDocuments(TecCom.OpenMessaging.DTO.ParameterTypeID.OrdRsp, xmlPath);
                //var listOfDesadvs = tecClient.GetDocuments(TecCom.OpenMessaging.DTO.ParameterTypeID.DesAdv, xmlPath);
                //var listOfInvoices = tecClient.GetDocuments(TecCom.OpenMessaging.DTO.ParameterTypeID.Invoice, xmlPath);

                var xmlManager = new XMLManager();
                var ret = xmlManager.GetUnprocessedFiles();
                if (!ret.Any())
                {
                    return;
                }

                var loginParams = new S1LoginParams
                {
                    XcoName = Properties.Settings.Default.XCOName,
                    UserName = Properties.Settings.Default.UserName,
                    Password = Properties.Settings.Default.Password,
                    Company = Properties.Settings.Default.Company,
                    Branch = Properties.Settings.Default.Branch,
                    SoftonePath = Properties.Settings.Default.SoftOnePath,
                    LoginDate = DateTime.Now
                };
                var _tools = new SoftoneTools();
                var _xSupport = _tools.Login(loginParams);
                if (_xSupport == null)
                {
                    throw new Exception("Unable Login to Softone!");
                }
                var _s1Manager = new SoftoneManager(_xSupport, xmlManager);
                var check = _s1Manager.DefaultItemExists();
                if (!check)
                {
                    throw new Exception("Default EDI Item with Code EDI9999 not found! Operation aborted.");
                }
                _s1Manager.ProcessFiles(ret);

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
    }
}
