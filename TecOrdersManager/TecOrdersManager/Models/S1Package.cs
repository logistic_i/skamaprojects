﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecOrdersManager.Models
{
    public class S1Package
    {
        public double? NetWeight { get; set; }
        public double? GrossWeight { get; set; }
        public double? Volume { get; set; }
        public double? Length { get; set; }
        public double? Width { get; set; }
        public double? Height { get; set; }
        public string FreeText { get; set; }
        public int PkgId { get; set; }
        public string SSCC { get; set; }
        public string SupplierNo { get; set; }
    }
}
