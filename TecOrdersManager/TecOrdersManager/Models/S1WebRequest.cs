﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecOrdersManager.Models
{
    public class S1WebRequest
    {
        public string Service { get; set; }
        public string Object { get; set; }
        public object ObjectParams { get; set; }
        public object Data { get; set; }
        public string Form { get; set; }
        public string Key { get; set; }
    }
}
