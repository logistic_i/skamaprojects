﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecOrdersManager.Models
{
    public class S1WebResponse
    {
        public bool Success { get; set; }
        public int TotalCount { get; set; }
        public string Error { get; set; }
        public string ErrorCode { get; set; }
        public string Id { get; set; }
    }
}
