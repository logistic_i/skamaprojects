﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecOrdersManager.Models
{
    public class S1LoginParams
    {
        public string XcoName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int Company { get; set; }
        public int Branch { get; set; }
        public string SoftonePath { get; set; }
        public DateTime LoginDate { get; set; } = DateTime.Now;
    }
}
