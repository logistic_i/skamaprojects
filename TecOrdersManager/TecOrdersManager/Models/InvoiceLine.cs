﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecOrdersManager.Models
{
    public class InvoiceLine
    {
        public int Mtrl { get; set; }
        public string Code { get; set; }
        public double Qty1 { get; set; }
        public double Price { get; set; }
        public double? Disc1Prc { get; set; }
        public double? Disc2Prc { get; set; }
        public double? Disc3Prc { get; set; }
        public double? Disc1Val { get; set; }
        public double? Disc2Val { get; set; }
        public double? Disc3Val { get; set; }
        public int TaxId { get; set; }
        public double LineVal { get; set; }
        public int? Findocs { get; set; }
        public int? Mtrliness { get; set; }
        public int PositionNumber { get; set; }
        public int? BuyerRef { get; set; }
        public int? SellerRef { get; set; }
        public int Linenum { get; set; }
    }
}
