﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TecOrder;

namespace TecOrdersManager.Models
{

    public class FileRet 
    {
        public string FileName { get; set; }
        public string FullFileName { get; set; }
        public object Document { get; set; }
    }
}
