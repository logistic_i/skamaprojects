﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Softone;

namespace SkamaImport
{
    [WorksOn("CCCIMPDATA")]
    public class ImpData : TXCode
    {
        public override void BeforePost()
        {
            try
            {
                var importfilters = XModule.GetTable("CCCIMPORTFILTERS");
                var startCode = "";// Convert.ToString(importfilters.Current["STARTCODE"]);
                var firstLineInUse = Convert.ToInt32(importfilters.Current["FIRSTLINEINUSE"]);
                var sheetNameSoftone = Convert.ToString(importfilters.Current["SHEETNAME"]);
                var remarks = importfilters.Current["REMARKS"];
                var numLinesToRemove = 0;

                var jobObj = importfilters.Current["JOB"];
                if (jobObj == DBNull.Value)
                {
                    MessageBox.Show("συμπληρώστε αρχείο");
                    return;
                }
                var job = Convert.ToInt32(jobObj);
                var fileName = Convert.ToString(importfilters.Current["PATH"]);

                //Εισαγωγή Ειδών
                if (job == 1 && !string.IsNullOrEmpty(fileName))
                {
                    importfilters.Current["REMARKS"] = "";
                    remarks = "--- Εισαγωγή-Ενημέρωση Ειδών ---" + Environment.NewLine;
                    if (firstLineInUse == 0)
                    {
                        numLinesToRemove = 0;
                    }
                    else
                    {
                        numLinesToRemove = (firstLineInUse - 1);
                    }

                    var excelClient = new ExcelEditor();
                    excelClient.LoadFromFile(fileName);
                    var sheetName = sheetNameSoftone;   // "L.D. Template";
                    var excelData = excelClient.ExportExcelData(sheetName);
                    var errors = new List<string>();

                    if (excelData.Any())
                    {
                        excelData.RemoveRange(0, numLinesToRemove);
                        var tools = new Tools(XModule, XSupport);
                        var items = tools.GetItems(excelData);
                        if (items.Any())
                        {
                            var text1 = "";
                            var text2 = "";
                            var text3 = "";
                            var newItems = items.Where(x => x.NewItem && x.NewExItem).ToList();
                            if (newItems.Count() > 0)
                            {
                                text1 = $@"{newItems.Count()} είδη για εισαγωγή στα είδη εκτός Αποθήκης." + Environment.NewLine;
                            }

                            var oldItems = items.Where(x => !x.NewItem && x.NewExItem).ToList();
                            if (oldItems.Count() > 0)
                            {
                                text2 = $@"{oldItems.Count()} είδη για ενημέρωση στην Αποθήκη." + Environment.NewLine;
                            }

                            var oldExItems = items.Where(x => x.NewItem && !x.NewExItem).ToList();
                            if (oldExItems.Count() > 0)
                            {
                                text3 = $@"{oldExItems.Count()} είδη για ενημέρωση στα είδη εκτός Αποθήκης." + Environment.NewLine;
                            }

                            remarks = remarks + text1 + text2 + text3;
                            //var countItems = newItems.Count() + oldItems.Count() + oldExItems.Count();
                            var listOfMessages = new List<string>();

                            if (oldItems.Any())
                            {
                                using (var ItemObj = XSupport.CreateModule("ITEM"))
                                {
                                    //HideWarnings
                                    foreach (var item in oldItems)
                                    {
                                        try
                                        {
                                           
                                            if (item.CoreItemError != 1)
                                            {
                                                ItemObj.LocateData(item.Mtrl);
                                                var msg = tools.UpdateInsertItem(ItemObj, item, "update");
                                                if (!string.IsNullOrEmpty(msg))
                                                {
                                                    listOfMessages.Add(msg);
                                                }
                                            }
                                            else
                                            {
                                                var msg = $"Δεν πραγματοποιήθηκε η ενημέρωση του είδους {item.Code} - {item.Name} , (γραμμή αρχείου excel {item.ExcelLineNumber}) διότι το είδος έχει Core (θα πρέπει να το διαγράψετε το είδος από την Αρχείο Ειδών και να το περάσετε ξανά), ";
                                                errors.Add(msg);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            var myMsg = $"Δεν πραγματοποιήθηκε η ενημέρωση του είδους {item.Code} - {item.Name} , (γραμμή αρχείου excel {item.ExcelLineNumber}), ";
                                            string msg = CreateExceptionMessge(ex, myMsg);
                                            errors.Add(msg);
                                        }
                                    }
                                }
                            }

                            ///*//11/02/2020 prosorina exo
                            if (oldExItems.Any())
                            {
                                using (var ItemObj = XSupport.CreateModule("CCCITEM"))
                                {
                                    //HideWarnings
                                    foreach (var item in oldExItems)
                                    {
                                        try
                                        {
                                            ItemObj.LocateData(item.Mtrl);
                                            var msg = tools.UpdateInsertItem(ItemObj, item, "update");
                                            if (!string.IsNullOrEmpty(msg))
                                            {
                                                listOfMessages.Add(msg);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            var myMsg = $"Δεν πραγματοποιήθηκε η ενημέρωση του είδους {item.Code} - {item.Name} , (γραμμή αρχείου excel {item.ExcelLineNumber}), ";
                                            string msg = CreateExceptionMessge(ex, myMsg);
                                            errors.Add(msg);
                                        }
                                    }
                                }
                            }

                            if (newItems.Any())
                            {
                                using (var ItemObj = XSupport.CreateModule("CCCITEM"))
                                {
                                    //HideWarnings
                                    foreach (var item in newItems)
                                    {
                                        try
                                        {
                                            ItemObj.InsertData();
                                            var msg = tools.UpdateInsertItem(ItemObj, item, "insert");
                                            if (!string.IsNullOrEmpty(msg))
                                            {
                                                listOfMessages.Add(msg);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            var myMsg = $"Δεν πραγματοποιήθηκε η καταχώριση του είδους {item.Code} - {item.Name} , (γραμμή αρχείου excel {item.ExcelLineNumber}), ";
                                            string msg = CreateExceptionMessge(ex, myMsg);
                                            errors.Add(msg + Environment.NewLine);
                                        }
                                    }
                                }
                            }
                            //*/ //11/02/2020 prosorina exo

                            var logmtrunitchanges = "";
                            if (listOfMessages.Any())
                            {
                                logmtrunitchanges = $"{Environment.NewLine}Κατά την διαδικασία ενημέρωσης ειδών δεν πραγματοποιείτε ενημέρωση μονάδων μέτρησης ή σχέσεων τους στα είδη της Αποθήκης. Τα είδη της Αποθήκης που στο αρχείο είχαν μεταβολές είναι τα παρακάτω:{Environment.NewLine}";
                                var logsdata = string.Join("" + Environment.NewLine, listOfMessages);
                                logmtrunitchanges = logmtrunitchanges + logsdata + Environment.NewLine;
                            }

                            remarks = remarks + logmtrunitchanges + Environment.NewLine+ "--- Σφάλματα ---" + Environment.NewLine;
                            var errorsdata = string.Join(""+Environment.NewLine, errors);
                            remarks = remarks + errorsdata + Environment.NewLine + "--- Τέλος Διαδικασίας ---";
                            importfilters.Current["REMARKS"] = remarks;

                            if (newItems.Count() > 0 || oldItems.Count() > 0 || oldExItems.Count() > 0)
                            {
                                //importfilters.Current["PATH"] = "";
                                //importfilters.Current["JOB"] = 0;
                                MessageBox.Show("Η διαδικασία εισαγωγής και ενημέρωσης Ειδών ολοκληρώθηκε.");
                            }
                        }
                    }
                }

                //Εισαγωγή Τιμολογιακών Πολιτικών
                if (job == 2 && !string.IsNullOrEmpty(fileName))
                {
                    importfilters.Current["REMARKS"] = "";
                    remarks = "--- Εισαγωγή-Ενημέρωση Τιμολογιακών Πολιτικών ---" + Environment.NewLine;
                    if (firstLineInUse == 0)
                    {
                        numLinesToRemove = 0;
                    }
                    else
                    {
                        numLinesToRemove = (firstLineInUse - 1);
                    }

                    var excelClient = new ExcelEditor();
                    excelClient.LoadFromFile(fileName);
                    var sheetName = sheetNameSoftone;   // "Sheet1";
                    var excelData = excelClient.ExportExcelData(sheetName);
                    var errors = new List<string>();

                    if (excelData.Any())
                    {
                        excelData.RemoveRange(0, numLinesToRemove);
                        var tools = new Tools(XModule, XSupport);
                        var prcrdata = tools.GetPrcrData(excelData);
                        var textAll = "";
                        if (prcrdata.Any())
                        {
                            //progress bar με τα αποτελέσματα από το prcrdata.Any()
                            var newItems = prcrdata.Where(x => x.NewItem && x.NewExItem && (x.CategoryA == "" && x.CategoryB == "" && x.CategoryC == "" )).ToList(); //δεν υπάρχουν πουθενά
                            
                            if (newItems.Any())
                            {
                                using (var ItemObj = XSupport.CreateModule("CCCITEM"))
                                {
                                    foreach (var coreitem in newItems)
                                    {
                                        try
                                        {
                                            ItemObj.InsertData();
                                            var id = tools.InsertNewItem(ItemObj, coreitem, "insert");
                                            coreitem.Mtrl = id;
                                            coreitem.NewItem = true;
                                            coreitem.NewExItem = false;
                                        }
                                        catch (Exception ex)
                                        {
                                            var myMsg = $"Δεν πραγματοποιήθηκε η καταχώριση του είδους {coreitem.Code} - {coreitem.Name} , (γραμμή αρχείου excel {coreitem.ExcelLineNumber}), ";
                                            string msg = CreateExceptionMessge(ex, myMsg);
                                            errors.Add(msg + Environment.NewLine);
                                        }
                                    }
                                }
                            }
                            //22/04/2021
                            /*
                            var newItems = prcrdata.Where(x => x.NewItem && x.NewExItem && (x.CoreCost == "" || x.CoreCost == "0")  
                                                                && (x.CategoryA == "" && x.CategoryB == "" && x.CategoryC == "")).ToList(); //δεν υπάρχουν πουθενά

                            var newItemsCore = prcrdata.Where(x => x.NewItem && x.NewExItem && (x.CoreCost !="" && x.CoreCost !="0") 
                                                                && (x.CategoryA == "" && x.CategoryB == "" && x.CategoryC == "")).ToList(); //δεν υπάρχουν πουθενά αλλά έχουν Core cost
                            */

                            var oldItems = prcrdata.Where(x => !x.NewItem && x.NewExItem).ToList(); //υπάρχουν στον MTRL
                            var oldExItems = prcrdata.Where(x => x.NewItem && !x.NewExItem).ToList(); //υπάρχουν στον CCCMTRL

                            // για εισαγωγή στον PRCRDATA
                            //var oldprcrule1 = prcrdata.Where(x => !x.NewItem && x.NewExItem && x.Trdr > 0 && x.Mtrl > 0 && (x.Price != "" || x.Discount != "" || x.NetPrice != "") ).ToList();
                            //12/07/2021
                            var oldprcrule1 = prcrdata.Where(x => !x.NewItem && x.NewExItem && x.Trdr > 0 && x.Mtrl > 0 && ((x.Price != "" || x.Discount != "" || x.NetPrice != "") || (x.PrcrdataLines.Count>0))).ToList();

                            var oldprcrule2 = prcrdata.Where(x => x.Trdr > 0 && x.CategoryA != "" && x.CategoryB == "" && x.CategoryC == "" && x.Discount != "").ToList();
                            var oldprcrule3 = prcrdata.Where(x => x.Trdr > 0 && x.CategoryB != "" && x.CategoryC == "" && x.Discount != "").ToList();
                            var oldprcrule4 = prcrdata.Where(x => x.Trdr > 0 && x.CategoryC != "" && x.Discount != "").ToList();
                            var oldprcrule5 = prcrdata.Where(x => !x.NewItem && x.NewExItem && x.Trdr > 0 && x.Mtrl > 0 && x.LeadCharge != "").ToList();
                            var oldprcrule6 = prcrdata.Where(x => !x.NewItem && x.NewExItem && x.Trdr > 0 && x.Mtrl > 0 && x.CoreCost != "").ToList();

                            // για εισαγωγή στον CCCPRCRDATA
                            //var oldExprcrule1 = prcrdata.Where(x => x.NewItem && !x.NewExItem && x.Trdr > 0 && x.Mtrl > 0 && (x.Price != "" || x.Discount != "" || x.NetPrice != "")).ToList();
                            //12/07/2021
                            var oldExprcrule1 = prcrdata.Where(x => x.NewItem && !x.NewExItem && x.Trdr > 0 && x.Mtrl > 0 && ((x.Price != "" || x.Discount != "" || x.NetPrice != "") || (x.PrcrdataLines.Count > 0))).ToList();
                            var oldExprcrule5 = prcrdata.Where(x => x.NewItem && !x.NewExItem && x.Trdr > 0 && x.Mtrl > 0 && x.LeadCharge != "").ToList();
                            var oldExprcrule6 = prcrdata.Where(x => x.NewItem && !x.NewExItem && x.Trdr > 0 && x.Mtrl > 0 && x.CoreCost != "").ToList();

                            //22/04/2021
                            //Είδη που δεν υπάρχουν πουθενά και εχουν Core Cost 
                            //var newExprcrule6 = prcrdata.Where(x => x.NewItem && x.NewExItem && x.Trdr > 0 && x.Mtrl == 0 && (x.CoreCost == "" || x.CoreCost == "0")).ToList();

                            /*
                            if (newItems.Any())
                            {
                                using (var ItemObj = XSupport.CreateModule("CCCITEM"))
                                {
                                    foreach (var coreitem in newItems)
                                    {
                                        try
                                        {
                                            ItemObj.InsertData();
                                            var id = tools.InsertNewItem(ItemObj, coreitem, "insert");
                                            coreitem.Mtrl = id;
                                            coreitem.NewExItem = false;
                                        }
                                        catch (Exception ex)
                                        {
                                            var myMsg = $"Δεν πραγματοποιήθηκε η καταχώριση του είδους {coreitem.Code} - {coreitem.Name} , (γραμμή αρχείου excel {coreitem.ExcelLineNumber}), ";
                                            string msg = CreateExceptionMessge(ex, myMsg);
                                        }
                                    }
                                }
                            }
                            */
                            /*
                            //21/05/2021 prosorina anenergo
                            if (newItems.Count() != 0) // prepei != gia ta test mas to exoume ==
                            {
                                var falseItemList = string.Join("", newItems.Select(x => "Το είδος " + x.CccEngName + ", (γραμμή αρχείου excel " + x.ExcelLineNumber + ")" + Environment.NewLine).ToList());

                                remarks = remarks 
                                    + "--- Σφάλματα ---" 
                                    + Environment.NewLine 
                                    + "Η διαδικασία διακόπτετε διότι υπάρχουν νέα είδη προς καταχώρηση. Για να συνεχίσετε την διαδικασία εισαγωγής τιμολογιακών πολιτικών πρέπει να εισάγετε πρώτα τα είδη και μετά την τιμολογιακή πολιτική τους."
                                    + Environment.NewLine 
                                    + "Τα είδη που πρέπει να καταχωρηθούν για να συνεχιστεί η διαδικασία είναι:"
                                    + Environment.NewLine
                                    + falseItemList
                                    + "--- Τέλος Διαδικασίας ---";
                                importfilters.Current["REMARKS"] = remarks;

                                MessageBox.Show("Η διαδικασία διακόπτετε διότι υπάρχουν νέα είδη προς καταχώρηση. Για να συνεχίσετε την διαδικασία εισαγωγής τιμολογιακών πολιτικών πρέπει να εισάγετε πρώτα τα είδη και μετά την τιμολογιακή πολιτική τους.");
                            }//21/05/2021 prosorina anenergo
                            */


                            //else //21/05/2021 prosorina anenergo
                            //{
                            // για εισαγωγή στον PRCRDATA
                            var prcrule = 0;
                                if (oldprcrule1.Any()) //Προμηθευτής – Είδος – Τιμή – Έκπτωση
                                {
                                    prcrule = 9001;
                                    //HideWarnings
                                    var tool = new Tools(XModule,XSupport);
                                    foreach (var data in oldprcrule1)
                                    {
                                        try
                                        {
                                            tool.InsertPrcrData(data, "PRCRDATA", "PRCRDATALNS", prcrule);
                                        }
                                        catch (Exception ex)
                                        {
                                            var myMsg = $"Δεν πραγματοποιήθηκε η εγγραφή του είδους {data.Code} - {data.Name} στην τιμολογιακή πολιτική «Προμηθευτής – Είδος – Τιμή – Έκπτωση», (γραμμή αρχείου excel {data.ExcelLineNumber}), ";
                                            string msg = CreateExceptionMessge(ex, myMsg);
                                            errors.Add(msg + Environment.NewLine);
                                        }
                                    }
                                    textAll = textAll + $@"{oldprcrule1.Count()} Εγγραφές για την εισαγωγή στην τιμολογιακή πολιτική «Προμηθευτής – Είδος – Τιμή – Έκπτωση»." + Environment.NewLine;
                                }

                                if (oldprcrule2.Any()) //Προμηθευτής – Κατηγορία Α –Έκπτωση
                                {
                                    prcrule = 9002;
                                    //HideWarnings
                                    var tool = new Tools(XModule, XSupport);
                                    foreach (var data in oldprcrule2)
                                    {
                                        try
                                        {
                                            tool.InsertPrcrData(data, "PRCRDATA", "", prcrule);
                                        }
                                        catch (Exception ex)
                                        {
                                            var myMsg = $"Δεν πραγματοποιήθηκε η εγγραφή του είδους {data.Code} - {data.Name} στην τιμολογιακή πολιτική «Προμηθευτής – Κατηγορία Α –Έκπτωση», (γραμμή αρχείου excel {data.ExcelLineNumber}), ";
                                            string msg = CreateExceptionMessge(ex, myMsg);
                                            errors.Add(msg + Environment.NewLine);
                                        }
                                    }
                                    textAll = textAll + $@"{oldprcrule2.Count()} Εγγραφές για την εισαγωγή στην τιμολογιακή πολιτική «Προμηθευτής – Κατηγορία Α –Έκπτωση»." + Environment.NewLine;
                                }

                                if (oldprcrule3.Any()) //Προμηθευτής – Κατηγορία Β –Έκπτωση
                                {
                                    prcrule = 9003;
                                    //HideWarnings
                                    var tool = new Tools(XModule, XSupport);
                                    foreach (var data in oldprcrule3)
                                    {
                                        try
                                        {
                                            tool.InsertPrcrData(data, "PRCRDATA", "", prcrule);
                                        }
                                        catch (Exception ex)
                                        {
                                            var myMsg = $"Δεν πραγματοποιήθηκε η εγγραφή του είδους {data.Code} - {data.Name} στην τιμολογιακή πολιτική «Προμηθευτής – Κατηγορία Β –Έκπτωση», (γραμμή αρχείου excel {data.ExcelLineNumber}), ";
                                            string msg = CreateExceptionMessge(ex, myMsg);
                                            errors.Add(msg + Environment.NewLine);
                                        }
                                    }
                                    textAll = textAll + $@"{oldprcrule3.Count()} Εγγραφές για την εισαγωγή στην τιμολογιακή πολιτική «Προμηθευτής – Κατηγορία Β –Έκπτωση»." + Environment.NewLine;
                                }

                                if (oldprcrule4.Any()) //Προμηθευτής – Κατηγορία Γ –Έκπτωση
                                {
                                    prcrule = 9004;
                                    //HideWarnings
                                    var tool = new Tools(XModule, XSupport);
                                    foreach (var data in oldprcrule4)
                                    {
                                        try
                                        {
                                            tool.InsertPrcrData(data, "PRCRDATA", "", prcrule);
                                        }
                                        catch (Exception ex)
                                        {
                                            var myMsg = $"Δεν πραγματοποιήθηκε η εγγραφή του είδους {data.Code} - {data.Name} στην τιμολογιακή πολιτική «Προμηθευτής – Κατηγορία Γ –Έκπτωση», (γραμμή αρχείου excel {data.ExcelLineNumber}), ";
                                            string msg = CreateExceptionMessge(ex, myMsg);
                                            errors.Add(msg + Environment.NewLine);
                                        }
                                    }
                                    textAll = textAll + $@"{oldprcrule4.Count()} Εγγραφές για την εισαγωγή στην τιμολογιακή πολιτική «Προμηθευτής – Κατηγορία Γ –Έκπτωση»." + Environment.NewLine;
                                }

                                if (oldprcrule5.Any()) //Προμηθευτής – Είδος – Lead Charge
                                {
                                    prcrule = 9005;
                                    //HideWarnings
                                    var tool = new Tools(XModule, XSupport);
                                    foreach (var data in oldprcrule5)
                                    {
                                        try
                                        {
                                        data.Discount = "0";
                                        tool.InsertPrcrData(data, "PRCRDATA", "", prcrule);
                                        }
                                        catch (Exception ex)
                                        {
                                            var myMsg = $"Δεν πραγματοποιήθηκε η εγγραφή του είδους {data.Code} - {data.Name} στην τιμολογιακή πολιτική «Προμηθευτής – Είδος – Lead Charge», (γραμμή αρχείου excel {data.ExcelLineNumber}), ";
                                            string msg = CreateExceptionMessge(ex, myMsg);
                                            errors.Add(msg + Environment.NewLine);
                                        }
                                    }
                                    textAll = textAll + $@"{oldprcrule5.Count()} Εγγραφές για την εισαγωγή στην τιμολογιακή πολιτική «Προμηθευτής – Είδος – Lead Charge»." + Environment.NewLine;
                                }

                                if (oldprcrule6.Any()) //Προμηθευτής – Είδος – Core Cost
                                {
                                    prcrule = 9006;
                                    //HideWarnings
                                    var tool = new Tools(XModule, XSupport);
                                    foreach (var data in oldprcrule6)
                                    {
                                        try
                                        {
                                            data.Discount = "0";
                                            tool.InsertPrcrData(data, "PRCRDATA", "", prcrule);
                                        }
                                        catch (Exception ex)
                                        {
                                            var myMsg = $"Δεν πραγματοποιήθηκε η εγγραφή του είδους {data.Code} - {data.Name} στην τιμολογιακή πολιτική «Προμηθευτής – Είδος – Core Cost», (γραμμή αρχείου excel {data.ExcelLineNumber}), ";
                                            string msg = CreateExceptionMessge(ex, myMsg);
                                            errors.Add(msg + Environment.NewLine);
                                        }
                                    }
                                    textAll = textAll + $@"{oldprcrule6.Count()} Εγγραφές για την εισαγωγή στην τιμολογιακή πολιτική «Προμηθευτής – Είδος – Core Cost»." + Environment.NewLine;
                                }

                                // για εισαγωγή στον CCCPRCRDTA
                                if (oldExprcrule1.Any()) //Προμηθευτής – Είδος – Τιμή – Έκπτωση
                                {
                                    prcrule = 9001;
                                    //HideWarnings
                                    var tool = new Tools(XModule, XSupport);
                                    foreach (var data in oldExprcrule1)
                                    {
                                        try
                                        {
                                            tool.InsertPrcrData(data, "CCCPRCRDATA", "CCCPRCRDATALNS", prcrule);
                                        }
                                        catch (Exception ex)
                                        {
                                            var myMsg = $"Δεν πραγματοποιήθηκε η εγγραφή του είδους {data.Code} - {data.Name} στην τιμολογιακή πολιτική «Προμηθευτής – Είδος – Τιμή – Έκπτωση» (Μη Ενεργή), (γραμμή αρχείου excel {data.ExcelLineNumber}), ";
                                            string msg = CreateExceptionMessge(ex, myMsg);
                                            errors.Add(msg + Environment.NewLine);
                                        }
                                    }
                                    textAll = textAll + $@"{oldExprcrule1.Count()} Εγγραφές για την εισαγωγή στην τιμολογιακή πολιτική «Προμηθευτής – Είδος – Τιμή – Έκπτωση» (Μη Ενεργή)." + Environment.NewLine;
                                }

                                if (oldExprcrule5.Any()) //Προμηθευτής – Είδος – Lead Charge
                                {
                                    prcrule = 9005;
                                    //HideWarnings
                                    var tool = new Tools(XModule, XSupport);
                                    foreach (var data in oldExprcrule5)
                                    {
                                        try
                                        {
                                        data.Discount = "0";
                                        tool.InsertPrcrData(data, "CCCPRCRDATA", "", prcrule);
                                        }
                                        catch (Exception ex)
                                        {
                                            var myMsg = $"Δεν πραγματοποιήθηκε η εγγραφή του είδους {data.Code} - {data.Name} στην τιμολογιακή πολιτική «Προμηθευτής – Είδος – Lead Charge» (Μη Ενεργή), (γραμμή αρχείου excel {data.ExcelLineNumber}), ";
                                            string msg = CreateExceptionMessge(ex, myMsg);
                                            errors.Add(msg + Environment.NewLine);
                                        }
                                    }
                                    textAll = textAll + $@"{oldExprcrule5.Count()} Εγγραφές για την εισαγωγή στην τιμολογιακή πολιτική «Προμηθευτής – Είδος – Lead Charge» (Μη Ενεργή)." + Environment.NewLine;
                                }

                                if (oldExprcrule6.Any()) //Προμηθευτής – Είδος – Core Cost
                                {
                                    prcrule = 9006;
                                    //HideWarnings
                                    var tool = new Tools(XModule, XSupport);
                                    foreach (var data in oldExprcrule6)
                                    {
                                        try
                                        {
                                            data.Discount = "0";
                                            tool.InsertPrcrData(data, "CCCPRCRDATA", "", prcrule);
                                        }
                                        catch (Exception ex)
                                        {
                                            var myMsg = $"Δεν πραγματοποιήθηκε η εγγραφή του είδους {data.Code} - {data.Name} στην τιμολογιακή πολιτική «Προμηθευτής – Είδος – Core Cost» (Μη Ενεργή), (γραμμή αρχείου excel {data.ExcelLineNumber}), ";
                                            string msg = CreateExceptionMessge(ex, myMsg);
                                            errors.Add(msg + Environment.NewLine);
                                        }
                                    }
                                    textAll = textAll + $@"{oldExprcrule6.Count()} Εγγραφές για την εισαγωγή στην τιμολογιακή πολιτική «Προμηθευτής – Είδος – Core Cost» (Μη Ενεργή)." + Environment.NewLine;
                                }

                                remarks = remarks + textAll;
                                remarks = remarks + Environment.NewLine + "--- Σφάλματα ---" + Environment.NewLine;
                                var errorsdata = string.Join("" + Environment.NewLine, errors);
                                remarks = remarks + errorsdata + Environment.NewLine + "--- Τέλος Διαδικασίας ---";
                                importfilters.Current["REMARKS"] = remarks;

                                if (oldprcrule1.Count() > 0 || oldprcrule2.Count() > 0 || oldprcrule3.Count() > 0 || oldprcrule4.Count() > 0|| oldprcrule5.Count() > 0 || oldprcrule6.Count() > 0 
                                    || oldExprcrule1.Count() > 0 || oldExprcrule5.Count() > 0 || oldExprcrule6.Count() > 0)
                                {
                                    //importfilters.Current["PATH"] = "";
                                    //importfilters.Current["JOB"] = 0;
                                    MessageBox.Show("Η διαδικασία εισαγωγής Τιμολογιακών Πολιτικών ολοκληρώθηκε.");
                                }
                            //}//10/02/2021 prosorina anenergo
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            base.BeforePost();
        }

        private string CreateExceptionMessge(Exception ex, string myMsg=null)
        {
            var msg = string.IsNullOrWhiteSpace(myMsg) ? ex.Message :  myMsg + " (" + ex.Message + ").";

            while (ex.InnerException != null)
            {
                msg += Environment.NewLine + ex.InnerException.Message;
                ex = ex.InnerException;
            }
            return msg;
        }
    }
}
