﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkamaImport.Models
{
    public class MtrSubST
    {
        public string MtrSubstituteCode { get; set; }
        public string MtrUnit { get; set; }
        public string Qty1 { get; set; }
    }
}
