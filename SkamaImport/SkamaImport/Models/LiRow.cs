﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkamaImport.Models
{
    public class LiRow
    {
        public List<string> Columns { get; set; } = new List<string>();
    }
}
