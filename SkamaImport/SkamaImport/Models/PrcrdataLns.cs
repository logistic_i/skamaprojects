﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkamaImport
{
    public class PrcrdataLns
    {
        public int ExcelLineNumber { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string CccEngName { get; set; }
        public int Mtrl { get; set; }
        public string SupplierCode { get; set; }
        public int Trdr { get; set; }
        public string Moq { get; set; }
        public string MoqPrice { get; set; }
        public string MoqNetPrice { get; set; }
        public string MoqDiscount { get; set; }
    }
}
