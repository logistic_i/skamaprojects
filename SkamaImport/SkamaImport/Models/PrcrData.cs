﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkamaImport
{
    public class PrcrData
    {
        public int ExcelLineNumber { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string SkamaCode { get; set; }
        public string CccEngName { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public Boolean NewItem { get; set; }
        public Boolean NewExItem { get; set; }
        public int Mtrl { get; set; }
        public string SupplierCode { get; set; }
        public int Trdr { get; set; }
        public string CategoryA { get; set; }
        public string CategoryB { get; set; }
        public string CategoryC { get; set; }
        public string LeadCharge { get; set; }
        public string CoreCost { get; set; }
        //public string MarkupW { get; set; }
        //public string MarkupR { get; set; }
        public List<PrcrdataLns> PrcrdataLines { get; set; }
        public string Price { get; set; }
        public string NetPrice { get; set; }
        public string Discount { get; set; }
        //public string MtrManfctrId { get; set; }
        public string MtrManfctrCode { get; set; }
        //public string Moq1 { get; set; }
        //public string Moq1Price { get; set; }
        //public string Moq1Discount { get; set; }
        //public string Moq2 { get; set; }
        //public string Moq2Price { get; set; }
        //public string Moq2Discount { get; set; }
        //public string Moq3 { get; set; }
        //public string Moq3Price { get; set; }
        //public string Moq3Discount { get; set; }
        //public string Moq4 { get; set; }
        //public string Moq4Price { get; set; }
        //public string Moq4Discount { get; set; }
        //public string Moq5 { get; set; }
        //public string Moq5Price { get; set; }
        //public string Moq5Discount { get; set; }
        //public string Moq6 { get; set; }
        //public string Moq6Price { get; set; }
        //public string Moq6Discount { get; set; }
    }
}
