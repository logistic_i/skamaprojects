﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkamaImport.Models
{
    public class Item
    {
        public int ExcelLineNumber { get; set; }
        public string SupplierCode { get; set; }
        public int Trdr { get; set; }
        public string CccEngName { get; set; }
        public int Mtrl { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public Boolean NewItem { get; set; }
        public Boolean NewExItem { get; set; }
        public string MtrManfctr { get; set; }
        public string Utbl01 { get; set; }
        public string Utbl02 { get; set; }
        public string Utbl03 { get; set; }
        public string CccStatus { get; set; }
        public int Intrastat { get; set; }
        public string CccOrigin { get; set; }
        public string MtrUnit1 { get; set; }
        public string Dim1 { get; set; }
        public string Dim2 { get; set; }
        public string Dim3 { get; set; }
        public string Weight { get; set; }
        public string Gweight { get; set; }
        public string Volume { get; set; }
        public List<MtrSubST> MtrSubstituteCode { get; set; }
        //public string MtrSubstituteCode { get; set; }
        public string MtrUnit3 { get; set; }
        public string CccDim1 { get; set; }
        public string CccDim2 { get; set; }
        public string CccDim3 { get; set; }
        public string CccWeight { get; set; }
        public string CccGweight { get; set; }
        public string CccVolume { get; set; }
        public string Mu31 { get; set; }
        public string ReOrderLevel { get; set; }
        public string CccStep { get; set; }
        public string MtrLotUse { get; set; }
        public string LotCodeMask { get; set; }
        public string CccProductDate { get; set; }
        public string CccExpireDate { get; set; }
        public string CccLifeTime { get; set; }
        public string CccShelfTime { get; set; }
        public string MtrUnit2 { get; set; }
        public string Mu21 { get; set; }
        public string CccBoxDim1 { get; set; }
        public string CccBoxDim2 { get; set; }
        public string CccBoxDim3 { get; set; }
        public string CccBoxWeight { get; set; }
        public string CccBoxGweight { get; set; }
        public string CccBoxVolume { get; set; }
        public string CccMtrUnit4 { get; set; }
        public string CccMu51 { get; set; }
        public string CccMu61 { get; set; }
        public string CccCartonDim1 { get; set; }
        public string CccCartonDim2 { get; set; }
        public string CccCartonDim3 { get; set; }
        public string CccCartonWeight { get; set; }
        public string CccCartonGweight { get; set; }
        public string CccCartonVolume { get; set; }
        public string CccTypePallet { get; set; }
        public string CccQtyPerPallet { get; set; }
        public string CccFirstRel { get; set; }
        public string CccSecondRel { get; set; }
        public string CccPackBarCode { get; set; }
        public string IsCore { get; set; }
        public int CoreItemError { get; set; }
    }
}
