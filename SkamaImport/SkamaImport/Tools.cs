﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using SkamaImport.Models;
using Softone;

namespace SkamaImport
{
    public class Tools
    {
        private XModule _xModule;
        private XSupport _xSupport;
        public Tools(XModule xModule, XSupport xSupport)
        {
            _xModule = xModule;
            _xSupport = xSupport;
        }

        public List<Item> GetItems(List<LiRow> rows)
        {
            var items = new List<Item>();
            var importfilters = _xModule.GetTable("CCCIMPORTFILTERS");
            var startCode = "";// Convert.ToString(importfilters.Current["STARTCODE"]);
            var firstLineInUse = Convert.ToInt32(importfilters.Current["FIRSTLINEINUSE"]);

            try
            {
                var i = firstLineInUse;
                foreach (var row in rows)
                {
                    if (row.Columns.Any())
                    {
                        //Έλεγχος ύπαρξης είδους στην αποθήκη.
                        var cccEngName = inBounds(1, row.Columns) ? row.Columns[1] : "";
                        var supplierCode = inBounds(0, row.Columns) ? row.Columns[0] : "";
                        var mtrlCode = "";
                        var oldmtrl = 0;
                        var newExItem = false;
                        var newItem = false;

                        //Ανεύρεση ID Κατασκευαστής
                        var mtrmanfctrCode = inBounds(3, row.Columns) ? row.Columns[3] : "";
                        var mtrmanfctr = "";
                        var query4 = $@"SELECT CODE,NAME,MTRMANFCTR,CCCSTARTITEMCODE,CCCCORECODE,CCCCOREPOSITION FROM MTRMANFCTR WHERE CODE='{mtrmanfctrCode}' AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
                        using (var ds4 = _xSupport.GetSQLDataSet(query4, null))
                        {
                            if (ds4.Count > 0)
                            {
                                //var corecode = ds4.GetAsString(0, "CCCCORECODE");
                                //var coreposition = ds4.GetAsInteger(0, "CCCCOREPOSITION");
                                mtrmanfctr = Convert.ToString(ds4.GetAsInteger(0, "MTRMANFCTR"));
                                startCode = ds4.GetAsString(0, "CCCSTARTITEMCODE");
                            }
                        }

                        //Έλεγχος ύπαρξης είδους CORE
                        var coreitemforerror = 0;
                        var iscore = inBounds(64, row.Columns) ? row.Columns[64] : "";
                        var maincode = "";
                        var querymaincode = $@"SELECT CASE WHEN ISNULL(CCCMAINPOSITION,0) = 1 THEN ISNULL(CCCSTARTITEMCODE,'')+ISNULL(CCCMAINCODE, '') + '{cccEngName}'
                                                WHEN ISNULL(CCCMAINPOSITION,0) = 2 THEN ISNULL(CCCSTARTITEMCODE,'')+'{cccEngName}' + ISNULL(CCCMAINCODE, '')
                                                WHEN ISNULL(CCCMAINPOSITION,0) = 3 THEN ISNULL(CCCSTARTITEMCODE,'')+stuff('{cccEngName}', 1, LEN(CCCMAINCODE), CCCMAINCODE)
                                                ELSE ISNULL(CCCSTARTITEMCODE,'')+'{cccEngName}' END AS CODE FROM MTRMANFCTR WHERE CODE='{mtrmanfctrCode}' AND COMPANY = {_xSupport.ConnectionInfo.CompanyId}";
                        using (var dsmaincode = _xSupport.GetSQLDataSet(querymaincode, null))
                        {
                            if (dsmaincode.Count > 0)
                            {
                                maincode = dsmaincode.GetAsString(0, "CODE");
                            }
                        }
                        var corecode = "";
                        var querycorecode = $@"SELECT CASE WHEN ISNULL(CCCCOREPOSITION,0) = 1 THEN ISNULL(CCCSTARTITEMCODE,'')+ISNULL(CCCCORECODE,'')+'{cccEngName}'
                                                WHEN ISNULL(CCCCOREPOSITION,0) = 2 THEN ISNULL(CCCSTARTITEMCODE,'')+'{cccEngName}'+ISNULL(CCCCORECODE,'')
                                                WHEN ISNULL(CCCCOREPOSITION,0) = 3 THEN ISNULL(CCCSTARTITEMCODE,'')+stuff('{cccEngName}', 1, LEN(CCCCORECODE), CCCCORECODE)
                                                ELSE ISNULL(CCCSTARTITEMCODE,'')+'{cccEngName}' END AS CODE FROM MTRMANFCTR WHERE CODE='{mtrmanfctrCode}' AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
                        using (var dscorecode = _xSupport.GetSQLDataSet(querycorecode, null))
                        {
                            if (dscorecode.Count > 0)
                            {
                                corecode = dscorecode.GetAsString(0, "CODE");
                            }
                        }

                        //πρόβλημα με τον χαρακτήρα ':' στην SQL.
                        var newcccEngName = cccEngName.Contains(':') == true? cccEngName.Replace(':','|') : cccEngName;

                        var query = $@"SELECT M.CODE,M.MTRL FROM MTRL M INNER JOIN MTRSUPCODE MC ON MC.MTRL = M.MTRL 
                                    WHERE MC.MTRSUPCODE = REPLACE('{newcccEngName}','|',CHAR(58)) AND M.SODTYPE = 51 AND M.COMPANY = {_xSupport.ConnectionInfo.CompanyId}
                                    AND MC.TRDR = (SELECT TRDR FROM TRDR WHERE CODE='{supplierCode}' AND COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=12)";
                        using (var ds = _xSupport.GetSQLDataSet(query, null))
                        {
                            if (ds.Count > 0)
                            {
                                oldmtrl = ds.GetAsInteger(0, "MTRL");
                                mtrlCode = ds.GetAsString(0, "CODE");
                                newItem = false;
                                newExItem =true;

                                //Έλεγχος ύπαρξης είδους CORE στο MTRL
                                var querycheckcore = $@"SELECT M.MTRL,M.CODE,M.NAME,MB.MTRLBAIL,M2.CODE AS CODEBAIL
                                                        FROM MTRL M INNER JOIN MTRBAIL MB ON M.MTRL=MB.MTRLBAIL LEFT JOIN MTRL M2 ON M2.MTRL=MB.MTRL
                                                        WHERE M.MTRL={oldmtrl}";
                                using (var dscheckcore = _xSupport.GetSQLDataSet(querycheckcore, null))
                                {
                                    if (dscheckcore.Count == 0 && iscore == "1")
                                    {
                                        coreitemforerror = 1;
                                    }
                                    else
                                    {
                                        coreitemforerror = 0;
                                    }
                                }
                            }
                            else 
                            {
                                //Έλεγχος ύπαρξης είδους στα είδη εκτός αποθήκης.
                                var query41 = $@"SELECT M.CODE,M.CCCMTRL FROM CCCMTRL M INNER JOIN CCCMTRSUPCODE MC ON MC.CCCMTRL = M.CCCMTRL 
                                    WHERE MC.CCCMTRSUPCODE = REPLACE('{newcccEngName}','|',CHAR(58)) AND M.SODTYPE = 51 AND M.COMPANY = {_xSupport.ConnectionInfo.CompanyId}
                                    AND MC.TRDR = (SELECT TRDR FROM TRDR WHERE CODE='{supplierCode}' AND COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=12)";
                                using (var ds4 = _xSupport.GetSQLDataSet(query41, null))
                                {
                                    if (ds4.Count > 0)
                                    {
                                        oldmtrl = ds4.GetAsInteger(0, "CCCMTRL");
                                        mtrlCode = ds4.GetAsString(0, "CODE");
                                        newItem = true;
                                        newExItem = false;

                                        //Έλεγχος ύπαρξης είδους CORE στο CCCMTRL
                                        var querycheckcore = $@"SELECT M.CCCMTRL,ISNULL(M.TRANSTOMTRL,0) AS TRANSTOMTRL,M.CODE,M.NAME,MB.CCCMTRLBAIL,M2.CODE AS CODEBAIL
                                                                FROM CCCMTRL M INNER JOIN CCCMTRBAIL MB ON M.CCCMTRL=MB.CCCMTRLBAIL 
                                                                LEFT JOIN CCCMTRL M2 ON M2.CCCMTRL=MB.CCCMTRL
                                                                WHERE ISNULL(M.TRANSTOMTRL,0) =0 AND M.CCCMTRL={oldmtrl}
                                                                UNION ALL
                                                                SELECT M.CCCMTRL,ISNULL(M.TRANSTOMTRL,0) AS TRANSTOMTRL,M.CODE,M.NAME,MB.CCCMTRLBAIL,M2.CODE AS CODEBAIL
                                                                FROM CCCMTRL M INNER JOIN CCCMTRBAIL MB ON M.CCCMTRL=MB.CCCMTRL 
                                                                LEFT JOIN CCCMTRL M2 ON M2.CCCMTRL=MB.CCCMTRL
                                                                WHERE ISNULL(M.TRANSTOMTRL,0) =0 AND M.CCCMTRL={oldmtrl}";
                                        using (var dscheckcore = _xSupport.GetSQLDataSet(querycheckcore, null))
                                        {
                                            if (dscheckcore.Count == 0 && iscore =="1")
                                            {
                                                //DELETE
                                                using (var ItemObjDel = _xSupport.CreateModule("CCCITEM"))
                                                {
                                                    ItemObjDel.LocateData(oldmtrl);
                                                    ItemObjDel.DeleteData();
                                                }
                                                if (iscore == "1")
                                                {
                                                    mtrlCode = corecode;
                                                }
                                                else
                                                {
                                                    mtrlCode = startCode + cccEngName;
                                                }
                                                newItem = true;
                                                newExItem = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (iscore == "1")
                                        {
                                            mtrlCode = corecode;
                                        }
                                        else
                                        {
                                            mtrlCode = startCode + cccEngName;
                                        }
                                        newItem = true;
                                        newExItem = true;
                                    }
                                }
                            }
                        }

                        //Ανεύρεση ID Συνήθη Προμηθευτή
                        //var supplierCode = inBounds(0, row.Columns) ? row.Columns[0] : "";
                        var supplierTrdr = 0;
                        var query2 = $@"SELECT TRDR,CODE,NAME,AFM FROM TRDR WHERE CODE='{supplierCode}' AND COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=12";
                        using (var ds2 = _xSupport.GetSQLDataSet(query2, null))
                        {
                            if (ds2.Count > 0)
                            {
                                supplierTrdr = ds2.GetAsInteger(0,"TRDR");
                            }
                        }

                        //Ανεύρεση ID Συνήθη INTRASTAT
                        var intrastatCode = inBounds(11, row.Columns) ? row.Columns[11] : "";
                        var intrastat = 0;
                        var query3 = $@"SELECT INTRASTAT FROM INTRASTAT WHERE CODE='{intrastatCode}' AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
                        using (var ds3 = _xSupport.GetSQLDataSet(query3, null))
                        {
                            if (ds3.Count > 0)
                            {
                                intrastat = ds3.GetAsInteger(0, "INTRASTAT");
                            }
                        }

                        //Ανεύρεση ID Product Group 1
                        var utbl01Code = inBounds(4, row.Columns) ? row.Columns[4] : "";
                        var utbl01 = "";
                        var query5 = $@"SELECT UTBL01 FROM UTBL01 WHERE CCCCODE='{utbl01Code}' AND CCCTRDR = {supplierTrdr} AND SODTYPE = 51 AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
                        using (var ds5 = _xSupport.GetSQLDataSet(query5, null))
                        {
                            if (ds5.Count > 0)
                            {
                                utbl01 = Convert.ToString(ds5.GetAsInteger(0, "UTBL01"));
                            }
                        }

                        //Ανεύρεση ID Product Group 2
                        var utbl02Code = inBounds(6, row.Columns) ? row.Columns[6] : "";
                        var utbl02 = "";
                        var query6 = $@"SELECT UTBL02 FROM UTBL02 WHERE CCCCODE='{utbl02Code}' AND CCCTRDR = {supplierTrdr} AND SODTYPE = 51 AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
                        using (var ds6 = _xSupport.GetSQLDataSet(query6, null))
                        {
                            if (ds6.Count > 0)
                            {
                                utbl02 = Convert.ToString(ds6.GetAsInteger(0, "UTBL02"));
                            }
                        }

                        //Ανεύρεση ID Product Group 3
                        var utbl03Code = inBounds(8, row.Columns) ? row.Columns[8] : "";
                        var utbl03 = "";
                        var query7 = $@"SELECT UTBL03 FROM UTBL03 WHERE CCCCODE='{utbl03Code}' AND CCCTRDR = {supplierTrdr} AND SODTYPE = 51 AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
                        using (var ds7 = _xSupport.GetSQLDataSet(query7, null))
                        {
                            if (ds7.Count > 0)
                            {
                                utbl03 = Convert.ToString(ds7.GetAsInteger(0, "UTBL03"));
                            }
                        }

                        //Ανεύρεση ID Country of Origin
                        var originCode = inBounds(12, row.Columns) ? row.Columns[12] : "";
                        var origin = "";
                        var query8 = $@"SELECT CCCORIGIN FROM CCCORIGIN WHERE CODE='{originCode}' AND ISACTIVE = 1 AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
                        using (var ds8 = _xSupport.GetSQLDataSet(query8, null))
                        {
                            if (ds8.Count > 0)
                            {
                                origin = Convert.ToString(ds8.GetAsInteger(0, "CCCORIGIN"));
                            }
                        }

                        var MtrSubstituteCodeList = new List<MtrSubST>();
                        MtrSubstituteCodeList.Add(new MtrSubST()
                        {
                            MtrSubstituteCode = inBounds(20, row.Columns) ? row.Columns[20] : "",
                            MtrUnit = inBounds(13, row.Columns) ? row.Columns[13] : "",
                            Qty1 = "1"
                        }) ;
                        MtrSubstituteCodeList.Add(new MtrSubST()
                        {
                            MtrSubstituteCode = inBounds(28, row.Columns) ? row.Columns[28] : "",
                            MtrUnit = inBounds(21, row.Columns) ? row.Columns[21] : "",
                            Qty1 = inBounds(29, row.Columns) ? row.Columns[29] : ""
                        });
                        MtrSubstituteCodeList.Add(new MtrSubST()
                        {
                            MtrSubstituteCode = inBounds(48, row.Columns) ? row.Columns[48] : "",
                            MtrUnit = inBounds(40, row.Columns) ? row.Columns[40] : "",
                            Qty1 = inBounds(41, row.Columns) ? row.Columns[41] : ""
                        });
                        MtrSubstituteCodeList.Add(new MtrSubST()
                        {
                            MtrSubstituteCode = inBounds(58, row.Columns) ? row.Columns[58] : "",
                            MtrUnit = inBounds(49, row.Columns) ? row.Columns[49] : "",
                            Qty1 = inBounds(50, row.Columns) ? row.Columns[50] : ""
                        });

                        MtrSubstituteCodeList = MtrSubstituteCodeList.GroupBy(x => x.MtrSubstituteCode)
                            .Select(y => new MtrSubST { MtrSubstituteCode = y.Key, MtrUnit = y.FirstOrDefault()?.MtrUnit, Qty1 = y.FirstOrDefault()?.Qty1 })
                            .Where(q => q.MtrSubstituteCode != "").ToList();

                        //List<string> MtrSubstituteCodeList = new List<string>();
                        //MtrSubstituteCodeList.Add(inBounds(20, row.Columns) ? row.Columns[20] : "");
                        //MtrSubstituteCodeList.Add(inBounds(28, row.Columns) ? row.Columns[28] : "");
                        //MtrSubstituteCodeList.Add(inBounds(48, row.Columns) ? row.Columns[48] : "");
                        //MtrSubstituteCodeList.Add(inBounds(58, row.Columns) ? row.Columns[58] : "");

                        items.Add(new Item
                        {
                            ExcelLineNumber = i,
                            SupplierCode = inBounds(0, row.Columns) ? row.Columns[0] : "",
                            CccEngName = inBounds(1, row.Columns) ? row.Columns[1] : "",
                            Name = inBounds(2, row.Columns) ? row.Columns[2] : "",

                            MtrManfctr = mtrmanfctr, //inBounds(3, row.Columns) ? row.Columns[3] : "",
                            Utbl01 = utbl01, //inBounds(4, row.Columns) ? row.Columns[4] : "",
                            Utbl02 = utbl02, //inBounds(6, row.Columns) ? row.Columns[6] : "",
                            Utbl03 = utbl03, //inBounds(8, row.Columns) ? row.Columns[8] : "",

                            CccStatus = inBounds(10, row.Columns) ? row.Columns[10] : "",
                            Intrastat = intrastat,
                            CccOrigin = origin,//inBounds(12, row.Columns) ? row.Columns[12] : "",
                            MtrUnit1 = inBounds(13, row.Columns) ? row.Columns[13] : "",
                            Dim1 = inBounds(14, row.Columns) ? row.Columns[14] : "",
                            Dim2 = inBounds(15, row.Columns) ? row.Columns[15] : "",
                            Dim3 = inBounds(16, row.Columns) ? row.Columns[16] : "",
                            Weight = inBounds(17, row.Columns) ? row.Columns[17] : "",
                            Gweight = inBounds(18, row.Columns) ? row.Columns[18] : "",
                            Volume = inBounds(19, row.Columns) ? row.Columns[19] : "",

                            //MtrSubstituteCode = MtrSubstituteCodeList.Where(x => !string.IsNullOrEmpty(x)).Distinct().ToList(),
                            MtrSubstituteCode = MtrSubstituteCodeList,

                            MtrUnit3 = inBounds(21, row.Columns) ? row.Columns[21] : "",
                            CccDim1 = inBounds(22, row.Columns) ? row.Columns[22] : "",
                            CccDim2 = inBounds(23, row.Columns) ? row.Columns[23] : "",
                            CccDim3 = inBounds(24, row.Columns) ? row.Columns[24] : "",
                            CccWeight = inBounds(25, row.Columns) ? row.Columns[25] : "",
                            CccGweight = inBounds(26, row.Columns) ? row.Columns[26] : "",
                            CccVolume = inBounds(27, row.Columns) ? row.Columns[27] : "",
                            Mu31 = inBounds(29, row.Columns) ? row.Columns[29] : "",
                            ReOrderLevel = inBounds(30, row.Columns) ? row.Columns[30] : "",
                            CccStep = inBounds(31, row.Columns) ? row.Columns[31] : "",
                            MtrLotUse = inBounds(32, row.Columns) ? row.Columns[32] : "",
                            LotCodeMask = inBounds(33, row.Columns) ? row.Columns[33] : "",
                            CccProductDate = inBounds(34, row.Columns) ? row.Columns[34] : "",
                            CccExpireDate = inBounds(35, row.Columns) ? row.Columns[35] : "",
                            CccLifeTime = inBounds(36, row.Columns) ? row.Columns[36] : "",
                            CccShelfTime = inBounds(37, row.Columns) ? row.Columns[37] : "",
                            MtrUnit2 = inBounds(40, row.Columns) ? row.Columns[40] : "",
                            Mu21 = inBounds(41, row.Columns) ? row.Columns[41] : "",
                            CccBoxDim1 = inBounds(42, row.Columns) ? row.Columns[42] : "",
                            CccBoxDim2 = inBounds(43, row.Columns) ? row.Columns[43] : "",
                            CccBoxDim3 = inBounds(44, row.Columns) ? row.Columns[44] : "",
                            CccBoxWeight = inBounds(45, row.Columns) ? row.Columns[45] : "",
                            CccBoxGweight = inBounds(46, row.Columns) ? row.Columns[46] : "",
                            CccBoxVolume = inBounds(47, row.Columns) ? row.Columns[47] : "",
                            CccMtrUnit4 = inBounds(49, row.Columns) ? row.Columns[49] : "",
                            CccMu51 = inBounds(50, row.Columns) ? row.Columns[50] : "",
                            CccMu61 = inBounds(51, row.Columns) ? row.Columns[51] : "",
                            CccCartonDim1 = inBounds(52, row.Columns) ? row.Columns[52] : "",
                            CccCartonDim2 = inBounds(53, row.Columns) ? row.Columns[53] : "",
                            CccCartonDim3 = inBounds(54, row.Columns) ? row.Columns[54] : "",
                            CccCartonWeight = inBounds(55, row.Columns) ? row.Columns[55] : "",
                            CccCartonGweight = inBounds(56, row.Columns) ? row.Columns[56] : "",
                            CccCartonVolume = inBounds(57, row.Columns) ? row.Columns[57] : "",
                            CccTypePallet = inBounds(59, row.Columns) ? row.Columns[59] : "",
                            CccQtyPerPallet = inBounds(60, row.Columns) ? row.Columns[60] : "",
                            CccFirstRel = inBounds(61, row.Columns) ? row.Columns[61] : "",
                            CccSecondRel = inBounds(62, row.Columns) ? row.Columns[62] : "",
                            CccPackBarCode = inBounds(63, row.Columns) ? row.Columns[63] : "",
                            IsCore = inBounds(64, row.Columns) ? row.Columns[64] : "",
                            CoreItemError = coreitemforerror,
                            Code = mtrlCode,
                            Mtrl = oldmtrl,
                            NewItem = newItem,
                            NewExItem = newExItem,
                            Trdr = supplierTrdr
                        }) ;
                    }
                    i++;
                }

                return items;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<PrcrData> GetPrcrData(List<LiRow> rows)
        {
            var prcrdatas = new List<PrcrData>();
            var importfilters = _xModule.GetTable("CCCIMPORTFILTERS");
            var startCode = "";// Convert.ToString(importfilters.Current["STARTCODE"]);
            var firstLineInUse = Convert.ToInt32(importfilters.Current["FIRSTLINEINUSE"]);
            try
            {
                rows = rows.Where(x => x.Columns.Any()).ToList();
                /*
                var cccEngNameList = rows.Select(x => inBounds(3, x.Columns) ? x.Columns[3] : "").ToList();
                var q = $@"SELECT M.CODE,M.MTRL FROM MTRL M INNER JOIN MTRSUPCODE MC ON MC.MTRL = M.MTRL 
                                    WHERE MC.MTRSUPCODE in '{string.Join("','", cccEngNameList)}' AND M.SODTYPE = 51 AND M.COMPANY = {_xSupport.ConnectionInfo.CompanyId}";
                using (XTable ds = _xSupport.GetSQLDataSet(q, null))
                {
                    var dataTable = ds.CreateDataTable(true);
                    var res = dataTable.AsEnumerable().Select(row => new
                    {
                        oldmtrl = row.Field<int>("MTRL"),
                        mtrlCode = row.Field<string>("CODE"),
                        newItem = false,
                        newExItem = true
                    }).ToList();
                }
                */
                var i = firstLineInUse;
                foreach (var row in rows)
                {
                    //Έλεγχος ύπαρξης είδους στην αποθήκη.
                    var cccEngName = inBounds(3, row.Columns) ? row.Columns[3] : "";
                    var newcccEngName = cccEngName.Contains(':') == true ? cccEngName.Replace(':', '|') : cccEngName;
                    var supplierCode = inBounds(4, row.Columns) ? row.Columns[4] : "";
                    var mtrlCode = "";
                    var oldmtrl = 0;
                    var newExItem = false;
                    var newItem = false;

                    //Ανεύρεση ID Κατασκευαστής
                    //var mtrmanfctrid = inBounds(37, row.Columns) ? row.Columns[37] : "";
                    var mtrManfctrCode = inBounds(37, row.Columns) ? row.Columns[37] : "";
                    /*
                    var query44 = $@"SELECT CODE,NAME,MTRMANFCTR,CCCSTARTITEMCODE,CCCCORECODE,CCCCOREPOSITION 
                                    FROM MTRMANFCTR WHERE MTRMANFCTR='{mtrmanfctrid}' AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
                    */
                    var query44 = $@"SELECT CODE,NAME,MTRMANFCTR,CCCSTARTITEMCODE,CCCCORECODE,CCCCOREPOSITION 
                                    FROM MTRMANFCTR WHERE CODE='{mtrManfctrCode}' AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";

                    using (var ds4 = _xSupport.GetSQLDataSet(query44, null))
                    {
                        if (ds4.Count > 0)
                        {
                            startCode = ds4.GetAsString(0, "CCCSTARTITEMCODE");
                        }
                    }

                    var hasCoreCost = inBounds(9, row.Columns) ? row.Columns[9] : "";
                    var maincode = "";
                    /*
                    var querymaincode = $@"SELECT CASE WHEN ISNULL(CCCMAINPOSITION,0) = 1 THEN ISNULL(CCCSTARTITEMCODE,'')+ISNULL(CCCMAINCODE, '') + '{cccEngName}'
                                                WHEN ISNULL(CCCMAINPOSITION,0) = 2 THEN ISNULL(CCCSTARTITEMCODE,'')+'{cccEngName}' + ISNULL(CCCMAINCODE, '')
                                                WHEN ISNULL(CCCMAINPOSITION,0) = 3 THEN ISNULL(CCCSTARTITEMCODE,'')+stuff('{cccEngName}', 1, LEN(CCCMAINCODE), CCCMAINCODE)
                                                ELSE ISNULL(CCCSTARTITEMCODE,'')+'{cccEngName}' END AS CODE FROM MTRMANFCTR 
                                                WHERE MTRMANFCTR='{mtrmanfctrid}' AND COMPANY = {_xSupport.ConnectionInfo.CompanyId}";
                    */
                    var querymaincode = $@"SELECT CASE WHEN ISNULL(CCCMAINPOSITION,0) = 1 THEN ISNULL(CCCSTARTITEMCODE,'')+ISNULL(CCCMAINCODE, '') + '{cccEngName}'
                                                WHEN ISNULL(CCCMAINPOSITION,0) = 2 THEN ISNULL(CCCSTARTITEMCODE,'')+'{cccEngName}' + ISNULL(CCCMAINCODE, '')
                                                WHEN ISNULL(CCCMAINPOSITION,0) = 3 THEN ISNULL(CCCSTARTITEMCODE,'')+stuff('{cccEngName}', 1, LEN(CCCMAINCODE), CCCMAINCODE)
                                                ELSE ISNULL(CCCSTARTITEMCODE,'')+'{cccEngName}' END AS CODE FROM MTRMANFCTR 
                                                WHERE CODE='{mtrManfctrCode}' AND COMPANY = {_xSupport.ConnectionInfo.CompanyId}";

                    using (var dsmaincode = _xSupport.GetSQLDataSet(querymaincode, null))
                    {
                        if (dsmaincode.Count > 0)
                        {
                            maincode = dsmaincode.GetAsString(0, "CODE");
                        }
                    }
                    var corecode = "";
                    /*
                    var querycorecode = $@"SELECT CASE WHEN ISNULL(CCCCOREPOSITION,0) = 1 THEN ISNULL(CCCSTARTITEMCODE,'')+ISNULL(CCCCORECODE,'')+'{cccEngName}'
                                                WHEN ISNULL(CCCCOREPOSITION,0) = 2 THEN ISNULL(CCCSTARTITEMCODE,'')+'{cccEngName}'+ISNULL(CCCCORECODE,'')
                                                WHEN ISNULL(CCCCOREPOSITION,0) = 3 THEN ISNULL(CCCSTARTITEMCODE,'')+stuff('{cccEngName}', 1, LEN(CCCCORECODE), CCCCORECODE)
                                                ELSE ISNULL(CCCSTARTITEMCODE,'')+'{cccEngName}' END AS CODE FROM MTRMANFCTR WHERE MTRMANFCTR='{mtrmanfctrid}' AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
                    */
                    var querycorecode = $@"SELECT CASE WHEN ISNULL(CCCCOREPOSITION,0) = 1 THEN ISNULL(CCCSTARTITEMCODE,'')+ISNULL(CCCCORECODE,'')+'{cccEngName}'
                                                WHEN ISNULL(CCCCOREPOSITION,0) = 2 THEN ISNULL(CCCSTARTITEMCODE,'')+'{cccEngName}'+ISNULL(CCCCORECODE,'')
                                                WHEN ISNULL(CCCCOREPOSITION,0) = 3 THEN ISNULL(CCCSTARTITEMCODE,'')+stuff('{cccEngName}', 1, LEN(CCCCORECODE), CCCCORECODE)
                                                ELSE ISNULL(CCCSTARTITEMCODE,'')+'{cccEngName}' END AS CODE FROM MTRMANFCTR WHERE CODE='{mtrManfctrCode}' AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
                    using (var dscorecode = _xSupport.GetSQLDataSet(querycorecode, null))
                    {
                        if (dscorecode.Count > 0)
                        {
                            corecode = dscorecode.GetAsString(0, "CODE");
                        }
                    }

                    //VRISKOUME TON KYRIO KODIKO KAI STIN PERIPTVSI TOU CORE TO REM
                    var query = $@"SELECT M.CODE,M.MTRL FROM MTRL M INNER JOIN MTRSUPCODE MC ON MC.MTRL = M.MTRL 
                                    WHERE MC.MTRSUPCODE = REPLACE('{newcccEngName}','|',CHAR(58)) AND M.SODTYPE = 51 AND M.COMPANY = {_xSupport.ConnectionInfo.CompanyId}
                                    AND MC.TRDR = (SELECT TRDR FROM TRDR WHERE CODE='{supplierCode}' AND COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=12)";

                    using (var ds = _xSupport.GetSQLDataSet(query, null))
                        {
                            if (ds.Count > 0)
                            {
                                oldmtrl = ds.GetAsInteger(0, "MTRL");
                                mtrlCode = ds.GetAsString(0, "CODE");
                                newItem = false;
                                newExItem = true;
                            }
                            else
                            {
                                //Έλεγχος ύπαρξης είδους στα είδη εκτός αποθήκης.
                                var query4 = $@"SELECT M.CODE,M.CCCMTRL FROM CCCMTRL M INNER JOIN CCCMTRSUPCODE MC ON MC.CCCMTRL = M.CCCMTRL 
                                    WHERE MC.CCCMTRSUPCODE = REPLACE('{newcccEngName}','|',CHAR(58)) AND M.SODTYPE = 51 AND M.COMPANY = {_xSupport.ConnectionInfo.CompanyId}
                                    AND MC.TRDR = (SELECT TRDR FROM TRDR WHERE CODE='{supplierCode}' AND COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=12)";

                            using (var ds4 = _xSupport.GetSQLDataSet(query4, null))
                                {
                                    if (ds4.Count > 0)
                                    {
                                        oldmtrl = ds4.GetAsInteger(0, "CCCMTRL");
                                        mtrlCode = ds4.GetAsString(0, "CODE");
                                        newItem = true;
                                        newExItem = false;
                                    }
                                    else
                                    {
                                        if ((hasCoreCost !="" ? hasCoreCost: "0") != "0")
                                        {
                                            mtrlCode = corecode;
                                        }
                                        else
                                        {
                                            mtrlCode = startCode + cccEngName;
                                        }
                                        newItem = true;
                                        newExItem = true;
                                    }
                                }
                            }
                        }

                        //Ανεύρεση ID Προμηθευτή
                        //var supplierCode = inBounds(4, row.Columns) ? row.Columns[4] : "";
                        var supplierTrdr = 0;
                        var query2 = $@"SELECT TRDR,CODE,NAME,AFM FROM TRDR WHERE CODE='{supplierCode}' AND COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=12";
                        using (var ds2 = _xSupport.GetSQLDataSet(query2, null))
                        {
                            if (ds2.Count > 0)
                            {
                                supplierTrdr = ds2.GetAsInteger(0, "TRDR");
                            }
                        }

                        var moq = "";
                        var PrcrDataLnsList = new List<PrcrdataLns>();

                        moq = inBounds(13, row.Columns) ? row.Columns[13] : "";
                        //if (moq != "" || moq != "0")
                        if (moq != "")
                        {
                            PrcrDataLnsList.Add(new PrcrdataLns()
                            {
                                ExcelLineNumber = i,
                                FromDate = inBounds(0, row.Columns) ? row.Columns[0] : "",
                                ToDate = inBounds(1, row.Columns) ? row.Columns[1] : "",
                                CccEngName = inBounds(3, row.Columns) ? row.Columns[3] : "",
                                Mtrl = oldmtrl,
                                SupplierCode = inBounds(4, row.Columns) ? row.Columns[4] : "",
                                Trdr = supplierTrdr,
                                Moq = inBounds(13, row.Columns) ? row.Columns[13] : "",
                                MoqPrice = inBounds(14, row.Columns) ? row.Columns[14] : "",
                                MoqDiscount = inBounds(15, row.Columns) ? row.Columns[15] : "",
                                MoqNetPrice = inBounds(16, row.Columns) ? row.Columns[16] : ""
                            });
                        }

                        moq = inBounds(17, row.Columns) ? row.Columns[17] : "";
                        if (moq != "")
                        {
                            PrcrDataLnsList.Add(new PrcrdataLns()
                            {
                                ExcelLineNumber = i,
                                FromDate = inBounds(0, row.Columns) ? row.Columns[0] : "",
                                ToDate = inBounds(1, row.Columns) ? row.Columns[1] : "",
                                CccEngName = inBounds(3, row.Columns) ? row.Columns[3] : "",
                                Mtrl = oldmtrl,
                                SupplierCode = inBounds(4, row.Columns) ? row.Columns[4] : "",
                                Trdr = supplierTrdr,
                                Moq = inBounds(17, row.Columns) ? row.Columns[17] : "",
                                MoqPrice = inBounds(18, row.Columns) ? row.Columns[18] : "",
                                MoqDiscount = inBounds(19, row.Columns) ? row.Columns[19] : "",
                                MoqNetPrice = inBounds(20, row.Columns) ? row.Columns[20] : ""
                            });
                        }

                        moq = inBounds(21, row.Columns) ? row.Columns[21] : "";
                        if (moq != "")
                        {
                            PrcrDataLnsList.Add(new PrcrdataLns()
                            {
                                ExcelLineNumber = i,
                                FromDate = inBounds(0, row.Columns) ? row.Columns[0] : "",
                                ToDate = inBounds(1, row.Columns) ? row.Columns[1] : "",
                                CccEngName = inBounds(3, row.Columns) ? row.Columns[3] : "",
                                Mtrl = oldmtrl,
                                SupplierCode = inBounds(4, row.Columns) ? row.Columns[4] : "",
                                Trdr = supplierTrdr,
                                Moq = inBounds(21, row.Columns) ? row.Columns[21] : "",
                                MoqPrice = inBounds(22, row.Columns) ? row.Columns[22] : "",
                                MoqDiscount = inBounds(23, row.Columns) ? row.Columns[23] : "",
                                MoqNetPrice = inBounds(24, row.Columns) ? row.Columns[24] : ""
                            });
                        }

                        moq = inBounds(25, row.Columns) ? row.Columns[25] : "";
                        if (moq != "")
                        {
                            PrcrDataLnsList.Add(new PrcrdataLns()
                            {
                                ExcelLineNumber = i,
                                FromDate = inBounds(0, row.Columns) ? row.Columns[0] : "",
                                ToDate = inBounds(1, row.Columns) ? row.Columns[1] : "",
                                CccEngName = inBounds(3, row.Columns) ? row.Columns[3] : "",
                                Mtrl = oldmtrl,
                                SupplierCode = inBounds(4, row.Columns) ? row.Columns[4] : "",
                                Trdr = supplierTrdr,
                                Moq = inBounds(25, row.Columns) ? row.Columns[25] : "",
                                MoqPrice = inBounds(26, row.Columns) ? row.Columns[26] : "",
                                MoqDiscount = inBounds(27, row.Columns) ? row.Columns[27] : "",
                                MoqNetPrice = inBounds(28, row.Columns) ? row.Columns[28] : ""
                            });
                        }

                        moq = inBounds(29, row.Columns) ? row.Columns[29] : "";
                        if (moq != "")
                        {
                            PrcrDataLnsList.Add(new PrcrdataLns()
                            {
                                ExcelLineNumber = i,
                                FromDate = inBounds(0, row.Columns) ? row.Columns[0] : "",
                                ToDate = inBounds(1, row.Columns) ? row.Columns[1] : "",
                                CccEngName = inBounds(3, row.Columns) ? row.Columns[3] : "",
                                Mtrl = oldmtrl,
                                SupplierCode = inBounds(4, row.Columns) ? row.Columns[4] : "",
                                Trdr = supplierTrdr,
                                Moq = inBounds(29, row.Columns) ? row.Columns[29] : "",
                                MoqPrice = inBounds(30, row.Columns) ? row.Columns[30] : "",
                                MoqDiscount = inBounds(31, row.Columns) ? row.Columns[31] : "",
                                MoqNetPrice = inBounds(32, row.Columns) ? row.Columns[32] : ""
                            });
                        }

                        moq = inBounds(33, row.Columns) ? row.Columns[33] : "";
                        if (moq != "")
                        {
                            PrcrDataLnsList.Add(new PrcrdataLns()
                            {
                                ExcelLineNumber = i,
                                FromDate = inBounds(0, row.Columns) ? row.Columns[0] : "",
                                ToDate = inBounds(1, row.Columns) ? row.Columns[1] : "",
                                CccEngName = inBounds(3, row.Columns) ? row.Columns[3] : "",
                                Mtrl = oldmtrl,
                                SupplierCode = inBounds(4, row.Columns) ? row.Columns[4] : "",
                                Trdr = supplierTrdr,
                                Moq = inBounds(33, row.Columns) ? row.Columns[33] : "",
                                MoqPrice = inBounds(34, row.Columns) ? row.Columns[34] : "",
                                MoqDiscount = inBounds(35, row.Columns) ? row.Columns[35] : "",
                                MoqNetPrice = inBounds(36, row.Columns) ? row.Columns[36] : ""
                            });
                        }

                        PrcrDataLnsList = PrcrDataLnsList.Where(q => q.CccEngName != "").ToList();

                        // PrcrDataLnsList = PrcrDataLnsList.GroupBy(x => x.FromDate)
                        //    .Select(y => new MtrSubST { MtrSubstituteCode = y.Key, MtrUnit = y.FirstOrDefault()?.MtrUnit, Qty1 = y.FirstOrDefault()?.Qty1 })
                        //    .Where(q => q.MtrSubstituteCode != "").ToList();

                        prcrdatas.Add(new PrcrData
                        {
                            ExcelLineNumber = i,
                            FromDate = inBounds(0, row.Columns) ? row.Columns[0] : "",
                            ToDate = inBounds(1, row.Columns) ? row.Columns[1] : "",
                            SkamaCode = inBounds(2, row.Columns) ? row.Columns[2] : "",
                            CccEngName = inBounds(3, row.Columns) ? row.Columns[3] : "",
                            SupplierCode = inBounds(4, row.Columns) ? row.Columns[4] : "",
                            CategoryA = inBounds(5, row.Columns) ? row.Columns[5] : "",
                            CategoryB = inBounds(6, row.Columns) ? row.Columns[6] : "",
                            CategoryC = inBounds(7, row.Columns) ? row.Columns[7] : "",
                            LeadCharge = inBounds(8, row.Columns) ? row.Columns[8] : "",
                            CoreCost = inBounds(9, row.Columns) ? row.Columns[9] : "",
                            Price = inBounds(10, row.Columns) ? row.Columns[10] : "",
                            Discount = inBounds(11, row.Columns) ? row.Columns[11] : "",
                            NetPrice = inBounds(12, row.Columns) ? row.Columns[12] : "",
                            //MarkupW = inBounds(10, row.Columns) ? row.Columns[10] : "",
                            //MarkupR = inBounds(11, row.Columns) ? row.Columns[11] : "",
                            PrcrdataLines = PrcrDataLnsList,
                            Code = mtrlCode,
                            Mtrl = oldmtrl,
                            NewItem = newItem,
                            NewExItem = newExItem,
                            Trdr = supplierTrdr,
                            //MtrManfctrId = inBounds(37, row.Columns) ? row.Columns[37] : "",
                            MtrManfctrCode = inBounds(37, row.Columns) ? row.Columns[37] : ""
                        });
                    i++;
                }
                return prcrdatas;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //27/04/2021 insert new item from Core Cost
        internal int InsertNewItem(XModule itemObj, PrcrData item, string job)
        {
            var itemTableName = "CCCMTRL";
            var mtrsubcodeTableName = "CCCMTRSUPCODE";
            var mtrbailTableName = "CCCMTRBAIL";
            var mtracn = 0;
            var mtrlId = 0;
            var mtrlIdCore = 0;
            var corecode = "";
            var coreposition = 0;
            var maincode = "";
            var mainposition = 0;
            var startCode = "";
            var mtrmanfctr = 0;

            //Ανεύρεση ID Κατασκευαστής
            /*
            var query4 = $@"SELECT CODE,NAME,MTRMANFCTR,CCCSTARTITEMCODE,CCCCORECODE,CCCCOREPOSITION,CCCMAINCODE,CCCMAINPOSITION 
            FROM MTRMANFCTR WHERE MTRMANFCTR='{item.MtrManfctrId}' AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
            */
            var query4 = $@"SELECT CODE,NAME,MTRMANFCTR,CCCSTARTITEMCODE,CCCCORECODE,CCCCOREPOSITION,CCCMAINCODE,CCCMAINPOSITION 
            FROM MTRMANFCTR WHERE CODE='{item.MtrManfctrCode}' AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";

            using (var ds4 = _xSupport.GetSQLDataSet(query4, null))
            {
                if (ds4.Count > 0)
                {
                    corecode = ds4.GetAsString(0, "CCCCORECODE");
                    coreposition = ds4.GetAsInteger(0, "CCCCOREPOSITION");
                    maincode = ds4.GetAsString(0, "CCCMAINCODE");
                    mainposition = ds4.GetAsInteger(0, "CCCMAINPOSITION");
                    startCode = ds4.GetAsString(0, "CCCSTARTITEMCODE");
                    mtrmanfctr = ds4.GetAsInteger(0, "MTRMANFCTR");
                    var errortxt = "";
                    if (startCode == "")
                    {
                        errortxt = errortxt + $@"Παρακαλώ συμπληρώστε πρόθεμα νέου είδους για το Brand {ds4.GetAsString(0, "CODE")} - {ds4.GetAsString(0, "NAME")},για να συνεχίσετε την εισαγωγή ειδών.";
                    }

                    if ((corecode == "" || coreposition == 0) && (maincode == "" || mainposition == 0) && 
                        ((item.CoreCost != "" ? item.CoreCost:"0")  != "0"))
                    {
                        errortxt = errortxt != ""
                            ? errortxt + Environment.NewLine + $@"Το είδος {item.CccEngName} έχει Core , θα πρέπει να συμπληρωθούν τα πεδία «Πρόθεμα Core» και «Θέση προθέματος» στο Brand για να συνεχίσετε την εισαγωγή ειδών."
                            : errortxt + $@"Το είδος {item.CccEngName} έχει Core , θα πρέπει να συμπληρωθούν τα πεδία «Πρόθεμα Core» και «Θέση προθέματος» στο Brand για να συνεχίσετε την εισαγωγή ειδών.";
                    }
                    if (errortxt != "")
                    {
                        throw new Exception(errortxt);
                    }
                }
                else
                {
                    throw new Exception($@"Παρακαλώ συμπληρώστε το Brand για το είδος {item.CccEngName},για να συνεχίσετε την εισαγωγή ειδών.");
                }
            }


            var querymtracn = $@"SELECT ISNULL(MTRACN,0) AS MTRACN,CCCTRDR FROM MTRACN WHERE COMPANY={_xSupport.ConnectionInfo.CompanyId} 
                                 AND SODTYPE=51 AND ISNULL(CCCTRDR,0)={item.Trdr}";

            using (var dsmtracn = _xSupport.GetSQLDataSet(querymtracn, null))
            {
                if (dsmtracn.Count > 0 && dsmtracn.GetAsInteger(0, "MTRACN") != 0)
                {
                    mtracn = dsmtracn.GetAsInteger(0, "MTRACN");
                }
            }

            itemObj.GetTable(itemTableName).Current["CODE"] = item.Code;
            itemObj.GetTable(itemTableName).Current["NAME"] = item.CccEngName;
            itemObj.GetTable(itemTableName).Current["CCCENGNAME"] = item.CccEngName;
            if (mtrmanfctr > 0)
            {
                itemObj.GetTable(itemTableName).Current["MTRMANFCTR"] = mtrmanfctr;
            }
            itemObj.GetTable(itemTableName).Current["MTRUNIT1"] = 101;
            itemObj.GetTable(itemTableName).Current["MTRUNIT3"] = 101;
            itemObj.GetTable(itemTableName).Current["MU31"] = (double)1;
            itemObj.GetTable(itemTableName).Current["MU13MODE"] = 1;
            itemObj.GetTable(itemTableName).Current["MTRUNIT4"] = 101;
            itemObj.GetTable(itemTableName).Current["MU41"] = (double)1;
            itemObj.GetTable(itemTableName).Current["MU14MODE"] = 1;
            itemObj.GetTable(itemTableName).Current["VAT"] = 1410;
            itemObj.GetTable(itemTableName).Current["SODTYPE"] = 51;

            if (item.CoreCost == "0" || item.CoreCost == "")
            {
                itemObj.GetTable(itemTableName).Current["MTRSUP"] = item.Trdr;

                var mtrsubCode = itemObj.GetTable(mtrsubcodeTableName).CreateDataTable(true).AsEnumerable().ToList();
                var count2 = mtrsubCode.Where(x => x[mtrsubcodeTableName].ToString() == item.CccEngName).ToList().Count;
                if (count2 == 0)
                {
                    using (var MtrSubCode = itemObj.GetTable(mtrsubcodeTableName))
                    {
                        MtrSubCode.Current.Append();
                        MtrSubCode.Current["TRDR"] = item.Trdr;
                        MtrSubCode.Current["CCCMTRSUPCODE"] = item.CccEngName;
                        MtrSubCode.Current.Post();
                    }
                }
            }

            if (mtracn != 0 && item.CoreCost != "0" && ((item.CoreCost !="" ? item.CoreCost:"0" ) != "0"))
            {
                itemObj.GetTable(itemTableName).Current["MTRACN"] = mtracn;
                //itemObj.GetTable(itemTableName).Current["MTRTYPE"] = 3;
            }
            else
            {
                itemObj.GetTable(itemTableName).Current["MTRACN"] = 101;
                //itemObj.GetTable(itemTableName).Current["MTRTYPE"] = 0;
            }
            mtrlId = itemObj.PostData();

            //CREATE IS CORE ITEM
            if ((item.CoreCost !=""? item.CoreCost:"0") != "0")
            {
                var coreCode = item.Code;
                var queryCoreCode = /*$@"SELECT CASE WHEN ISNULL(CCCCOREPOSITION,0) = 1 THEN ISNULL(CCCSTARTITEMCODE,'')+ISNULL(CCCCORECODE,'')+'{item.CccEngName}'
                                        WHEN ISNULL(CCCCOREPOSITION,0) = 2 THEN ISNULL(CCCSTARTITEMCODE,'')+'{item.CccEngName}'+ISNULL(CCCCORECODE,'')
                                        ELSE '' END AS CORECODE FROM MTRMANFCTR WHERE MTRMANFCTR={item.MtrManfctrId} AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
                    */
                    $@"SELECT CASE WHEN ISNULL(CCCMAINPOSITION,0) = 1 THEN ISNULL(CCCSTARTITEMCODE,'')+ISNULL(CCCMAINCODE,'')+'{item.CccEngName}'
                                                WHEN ISNULL(CCCMAINPOSITION,0) = 2 THEN ISNULL(CCCSTARTITEMCODE,'')+'{item.CccEngName}'+ISNULL(CCCMAINCODE,'')
                                                WHEN ISNULL(CCCMAINPOSITION,0) = 3 THEN ISNULL(CCCSTARTITEMCODE,'')+stuff('{item.CccEngName}', 1, LEN(CCCMAINCODE), CCCMAINCODE)
                                                ELSE ISNULL(CCCSTARTITEMCODE,'')+'{item.CccEngName}' END AS CODE FROM MTRMANFCTR WHERE CODE='{item.MtrManfctrCode}' AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
                using (var dsCoreCode = _xSupport.GetSQLDataSet(queryCoreCode, null))
                {
                    if (dsCoreCode.Count > 0 && dsCoreCode.GetAsString(0, "CODE") != "")
                    {
                        coreCode = dsCoreCode.GetAsString(0, "CODE");
                    }
                }

                var queryCheckCore = $@"SELECT CCCMTRL,CODE,NAME FROM CCCMTRL WHERE COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=51 AND CODE='{coreCode}'";
                using (var dsCore = _xSupport.GetSQLDataSet(queryCheckCore, null))
                {
                    if (dsCore.Count == 0 && coreCode != "")
                    {
                        using (var ItemObjCore = _xSupport.CreateModule("CCCITEM"))
                        {
                            ItemObjCore.InsertData();
                            ItemObjCore.GetTable(itemTableName).Current["CODE"] = coreCode;
                            ItemObjCore.GetTable(itemTableName).Current["NAME"] = item.CccEngName;
                            ItemObjCore.GetTable(itemTableName).Current["CCCENGNAME"] = item.CccEngName;
                            if (mtrmanfctr > 0)
                            {
                                ItemObjCore.GetTable(itemTableName).Current["MTRMANFCTR"] = mtrmanfctr;
                            }
                            ItemObjCore.GetTable(itemTableName).Current["MTRUNIT1"] = 101;
                            ItemObjCore.GetTable(itemTableName).Current["MTRUNIT3"] = 101;
                            ItemObjCore.GetTable(itemTableName).Current["MU31"] = (double)1;
                            ItemObjCore.GetTable(itemTableName).Current["MU13MODE"] = 1;
                            ItemObjCore.GetTable(itemTableName).Current["MTRUNIT4"] = 101;
                            ItemObjCore.GetTable(itemTableName).Current["MU41"] = (double)1;
                            ItemObjCore.GetTable(itemTableName).Current["MU14MODE"] = 1;
                            ItemObjCore.GetTable(itemTableName).Current["VAT"] = 1410;
                            ItemObjCore.GetTable(itemTableName).Current["SODTYPE"] = 51;
                            ItemObjCore.GetTable(itemTableName).Current["MTRACN"] = 101;
                            ItemObjCore.GetTable(itemTableName).Current["MTRSUP"] = item.Trdr;

                            var mtrsubCode = ItemObjCore.GetTable(mtrsubcodeTableName).CreateDataTable(true).AsEnumerable().ToList();
                            var count2 = mtrsubCode.Where(x => x[mtrsubcodeTableName].ToString() == item.CccEngName).ToList().Count;
                            if (count2 == 0)
                            {
                                using (var MtrSubCode = ItemObjCore.GetTable(mtrsubcodeTableName))
                                {
                                    MtrSubCode.Current.Append();
                                    MtrSubCode.Current["TRDR"] = item.Trdr;
                                    MtrSubCode.Current["CCCMTRSUPCODE"] = item.CccEngName;
                                    MtrSubCode.Current.Post();
                                }
                            }
                            //Εγγυοδοσία
                            ItemObjCore.GetTable(itemTableName).Current["HASBAIL"] = 1;
                            using (var MtrBail = ItemObjCore.GetTable(mtrbailTableName))
                            {
                                MtrBail.Current.Append();
                                MtrBail.Current["CCCMTRLBAIL"] = mtrlId;
                                MtrBail.Current.Post();
                            }
                            mtrlIdCore = ItemObjCore.PostData();
                        }
                    }
                }
            }

            if (mtrlIdCore == 0)
            {
                return mtrlId;
            }
            else
            {
                return mtrlIdCore;
            }
        }

        internal string UpdateInsertItem(XModule itemObj, Item item, string job)
        //internal string UpdateInsertItem(XModule itemObj, Item item, string job)
        {
            var itemTableName = "";
            var extraTableName = "";
            var mtrsubtitTableName = "";
            var mtrsubcodeTableName = "";
            var objName = itemObj.ObjName;
            var corecode = "";
            var coreposition = 0;
            var maincode = "";
            var mainposition = 0;
            var startCode = "";

            //Ανεύρεση ID Κατασκευαστής
            var query4 = $@"SELECT CODE,NAME,MTRMANFCTR,CCCSTARTITEMCODE,CCCCORECODE,CCCCOREPOSITION,CCCMAINCODE,CCCMAINPOSITION FROM MTRMANFCTR WHERE MTRMANFCTR='{item.MtrManfctr}' AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
            using (var ds4 = _xSupport.GetSQLDataSet(query4, null))
            {
                if (ds4.Count > 0)
                {
                    corecode = ds4.GetAsString(0, "CCCCORECODE");
                    coreposition = ds4.GetAsInteger(0, "CCCCOREPOSITION");
                    maincode = ds4.GetAsString(0, "CCCMAINCODE");
                    mainposition = ds4.GetAsInteger(0, "CCCMAINPOSITION");

                    startCode = ds4.GetAsString(0, "CCCSTARTITEMCODE");
                    var errortxt = "";
                    if (startCode == "")
                    {
                        errortxt = errortxt + $@"Παρακαλώ συμπληρώστε πρόθεμα νέου είδους για το Brand {ds4.GetAsString(0, "CODE")} - {ds4.GetAsString(0, "NAME")},για να συνεχίσετε την εισαγωγή ειδών.";
                    }

                    if ((corecode == "" || coreposition == 0) && ( maincode == "" || mainposition == 0) && item.IsCore != "")
                    {
                        errortxt = errortxt !=""
                            ? errortxt + Environment.NewLine + $@"Το είδος {item.CccEngName} έχει Core , θα πρέπει να συμπληρωθούν τα πεδία «Πρόθεμα Core» και «Θέση προθέματος» στο Brand για να συνεχίσετε την εισαγωγή ειδών."
                            : errortxt + $@"Το είδος {item.CccEngName} έχει Core , θα πρέπει να συμπληρωθούν τα πεδία «Πρόθεμα Core» και «Θέση προθέματος» στο Brand για να συνεχίσετε την εισαγωγή ειδών.";
                    }
                    if (errortxt != "")
                    {
                        throw new Exception(errortxt);
                    }
                }
                else
                {
                    throw new Exception($@"Παρακαλώ συμπληρώστε το Brand για το είδος {item.CccEngName},για να συνεχίσετε την εισαγωγή ειδών.");
                }
            }

            var mtracn = 0;
            var querymtracn = $@"SELECT ISNULL(MTRACN,0) AS MTRACN,CCCTRDR FROM MTRACN WHERE COMPANY={_xSupport.ConnectionInfo.CompanyId} 
                                     AND SODTYPE=51 AND ISNULL(CCCTRDR,0)={item.Trdr}";

            using (var dsmtracn = _xSupport.GetSQLDataSet(querymtracn, null))
            {
                if (dsmtracn.Count > 0 && dsmtracn.GetAsInteger(0, "MTRACN") != 0)
                {
                    mtracn = dsmtracn.GetAsInteger(0, "MTRACN");
                }
            }


            if ((job == "insert") || (job == "update" && objName == "CCCITEM"))
            {
                itemTableName = "CCCMTRL";
                extraTableName = "CCCMTREXTRA";
                mtrsubtitTableName = "CCCMTRSUBSTITUTE";
                mtrsubcodeTableName = "CCCMTRSUPCODE";
            }
            if (job == "update" && objName == "ITEM")
            {
                itemTableName = "MTRL";
                extraTableName = "MTREXTRA";
                mtrsubtitTableName = "MTRSUBSTITUTE";
                mtrsubcodeTableName = "MTRSUPCODE";
            }

            if ((job == "insert") || (job == "update" && objName == "CCCITEM"))
            {
                itemObj.GetTable(itemTableName).Current["CODE"] = item.Code;
                itemObj.GetTable(itemTableName).Current["NAME"] = item.Name;
                itemObj.GetTable(itemTableName).Current["CCCENGNAME"] = item.CccEngName;
                itemObj.GetTable(itemTableName).Current["NAME1"] = item.Name;

                if (mtracn != 0 && item.IsCore == "1")
                {
                    itemObj.GetTable(itemTableName).Current["MTRACN"] = mtracn;
                }
                else 
                {
                    itemObj.GetTable(itemTableName).Current["MTRACN"] = 101;
                }

                if (item.MtrUnit1 != "")
                {
                    itemObj.GetTable(itemTableName).Current["MTRUNIT1"] = item.MtrUnit1 != "" ? Convert.ToInt32(item.MtrUnit1) : (int?)null;
                }
                else
                {
                    throw new Exception("Το είδος για να καταχωρηθεί πρέπει να έχει συμπληρωμένη την Μονάδα Μέτρησης 1.");
                }
                itemObj.GetTable(itemTableName).Current["VAT"] = 1410;
                itemObj.GetTable(itemTableName).Current["SODTYPE"] = 51;
                //itemObj.GetTable(itemTableName).Current["MTRUNIT2"] = item.MtrUnit2 != "" ? Convert.ToInt32(item.MtrUnit2) : item.MtrUnit1 != "" ? Convert.ToInt32(item.MtrUnit1) : (int?)null;
                
                if (item.MtrUnit3 != "")
                {
                    itemObj.GetTable(itemTableName).Current["MTRUNIT2"] = item.MtrUnit3 != "" && item.MtrUnit3 != item.MtrUnit1 ? Convert.ToInt32(item.MtrUnit3) : (int?)null;
                    var varmtrunit2 = item.MtrUnit3 != "" && item.MtrUnit3 != item.MtrUnit1 ? item.MtrUnit3 : "";

                    if (item.MtrUnit3 == varmtrunit2 && item.Mu31 != "")
                    {
                        itemObj.GetTable(itemTableName).Current["MU21"] = item.Mu31 != "" ? Convert.ToDouble(item.Mu31) : 1;
                        itemObj.GetTable(itemTableName).Current["MU12MODE"] = 1;
                    }
                    if (item.MtrUnit3 == varmtrunit2 && item.Mu31 == "")
                    {
                        throw new Exception("Το είδος για να καταχωρηθεί πρέπει να έχει συμπληρωμένη την σχέση της Μονάδας Μέτρησης 1 με την Μονάδα Μέτρησης 2.");
                    }

                    //var varmtrunit2 = item.MtrUnit3 != "" ? item.MtrUnit2 : item.MtrUnit1 != "" ? item.MtrUnit1 : "";
                    /*
                    if (item.MtrUnit1 == varmtrunit2)
                    {
                        itemObj.GetTable(itemTableName).Current["MU21"] = ((double)1 / (item.Mu21 != "" ? Convert.ToDouble(item.Mu21) : 1));
                        itemObj.GetTable(itemTableName).Current["CCCMU21"] = item.Mu21 != "" ? Convert.ToDouble(item.Mu21) : 1;
                        itemObj.GetTable(itemTableName).Current["MU12MODE"] = 1;
                    }
                    
                    if (item.MtrUnit1 != varmtrunit2 && item.Mu21 != "")
                    {
                        itemObj.GetTable(itemTableName).Current["MU21"] = ((double)1 / (item.Mu21 != "" ? Convert.ToDouble(item.Mu21) : 1));
                        itemObj.GetTable(itemTableName).Current["CCCMU21"] = item.Mu21 != "" ? Convert.ToDouble(item.Mu21) : 1;
                        itemObj.GetTable(itemTableName).Current["MU12MODE"] = 1;
                    }
                    if (item.MtrUnit1 != varmtrunit2 && item.Mu21 == "")
                    {
                        throw new Exception("Το είδος για να καταχωρηθεί πρέπει να έχει συμπληρωμένη την σχέση της Μονάδας Μέτρησης 1 με την Μονάδα Μέτρησης 2.");
                    }
                    */

                    //if (item.MtrUnit3 == item.MtrUnit1 || item.MtrUnit3 == item.MtrUnit2)
                    if (item.MtrUnit3 == item.MtrUnit1 || item.MtrUnit3 == varmtrunit2)
                    {
                        itemObj.GetTable(itemTableName).Current["MTRUNIT3"] = item.MtrUnit3 != "" ? Convert.ToInt32(item.MtrUnit3) : item.MtrUnit1 != "" ? Convert.ToInt32(item.MtrUnit1) : (int?)null;
                        var varmtrunit3 = item.MtrUnit3 != "" ? item.MtrUnit3 : item.MtrUnit1 != "" ? item.MtrUnit1 : "";

                        if (varmtrunit3 != "")
                        {
                            if (item.MtrUnit1 == varmtrunit3)
                            {
                                if (item.Mu31 != "")
                                {
                                    //itemObj.GetTable(itemTableName).Current["MU31"] = item.Mu31 != "" ? Convert.ToDouble(item.Mu31) : item.Mu21 != "" ? Convert.ToDouble(item.Mu21) : 1;
                                    itemObj.GetTable(itemTableName).Current["MU31"] = item.Mu31 != "" ? Convert.ToDouble(item.Mu31) : (double)1;
                                    itemObj.GetTable(itemTableName).Current["MU13MODE"] = 1;
                                }
                                else
                                {
                                    itemObj.GetTable(itemTableName).Current["MU31"] = (double)1;
                                    itemObj.GetTable(itemTableName).Current["MU13MODE"] = 1;
                                }
                            }
                            else
                            {
                                if (item.Mu31 != "")
                                {
                                    //itemObj.GetTable(itemTableName).Current["MU31"] = item.Mu31 != "" ? Convert.ToDouble(item.Mu31) : item.Mu21 != "" ? Convert.ToDouble(item.Mu21) : 1;
                                    itemObj.GetTable(itemTableName).Current["MU31"] = item.Mu31 != "" ? Convert.ToDouble(item.Mu31) : (double)1;
                                    itemObj.GetTable(itemTableName).Current["MU13MODE"] = 1;
                                }
                                else
                                {
                                    throw new Exception("Το είδος για να καταχωρηθεί πρέπει να έχει συμπληρωμένη την σχέση της Μονάδας Μέτρησης 1 με την Μονάδα Μέτρησης Αγορών.");
                                }
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Το είδος έχει Μονάδα Μέτρησης Αγορών διαφορετική από την Μονάδα Μέτρησης 1 ή την Μονάδα Μέτρησης 2.");
                    }
                }
                else
                {
                    throw new Exception("Το είδος για να καταχωρηθεί πρέπει να έχει συμπληρωμένη την Μονάδα Μέτρησης Αγορών.");
                }

                itemObj.GetTable(itemTableName).Current["MTRUNIT4"] = item.MtrUnit1 != "" ? Convert.ToInt32(item.MtrUnit1) : (int?)null;
                itemObj.GetTable(itemTableName).Current["MU41"] = (double)1;
                itemObj.GetTable(itemTableName).Current["MU14MODE"] = 1;

                itemObj.GetTable(itemTableName).Current["CCCMTRUNIT5"] = item.MtrUnit2 != "" ? Convert.ToInt32(item.MtrUnit2) : (int?)null;
                var varmtrunit5 = item.MtrUnit2 != "" ? item.MtrUnit2 : "";
                if (varmtrunit5 !="")
                {
                    itemObj.GetTable(itemTableName).Current["CCCMU71"] = item.Mu21 != "" ? Convert.ToDouble(item.Mu21) : (double)1;
                }

                itemObj.GetTable(itemTableName).Current["MTRLOTUSE"] = item.MtrLotUse != "" ? item.MtrLotUse == "Yes" || item.MtrLotUse =="YES" || item.MtrLotUse =="1" ? 1 : 0 : 0;
                itemObj.GetTable(itemTableName).Current["CCCTYPEPALLET"] = item.CccTypePallet != "" ? Convert.ToInt32(item.CccTypePallet) : (int?)null;

                var varccctypepallet = item.CccTypePallet != "" ? item.CccTypePallet : "";
                var varcccqtyperpallet = item.CccQtyPerPallet != "" ? Convert.ToDouble(item.CccQtyPerPallet) : 0.0;
                if (varccctypepallet != "" && varcccqtyperpallet != 0.0)
                {
                    itemObj.GetTable(itemTableName).Current["CCCQTYPERPALLET"] = item.CccQtyPerPallet != "" ? Convert.ToDouble(item.CccQtyPerPallet) : 0.0;
                }
                if (varccctypepallet != "" && varcccqtyperpallet == 0.0)
                {
                    throw new Exception("Το είδος για να καταχωρηθεί ,όταν ο τύπος της παλέτας είναι συμπληρωμένος, πρέπει να είναι συμπληρωμένη και η ποσότητα ανά παλέτα.");
                }
            }

            if (job == "update" && objName == "ITEM")
            {
                itemObj.GetTable(itemTableName).Current["CCCMANTISUPD"] = 0;  
            }

            //Συλλογή ειδών με διαφορετικές μ.μ και σχέσεις μονάδων κατά την ενημέρωση.
            var logmtrunitMsg = "";
            if (job == "update" && objName == "ITEM")
            {
                var currentmtrunit1 = Convert.ToInt32(itemObj.GetTable(itemTableName).Current["MTRUNIT1"] != DBNull.Value ? itemObj.GetTable(itemTableName).Current["MTRUNIT1"] :0);
                var currentmtrunit2 = Convert.ToInt32(itemObj.GetTable(itemTableName).Current["MTRUNIT2"] != DBNull.Value ? itemObj.GetTable(itemTableName).Current["MTRUNIT2"] : 0);
                var currentmtrunit3 = Convert.ToInt32(itemObj.GetTable(itemTableName).Current["MTRUNIT3"] != DBNull.Value ? itemObj.GetTable(itemTableName).Current["MTRUNIT3"] : 0);
                var currentmtrunit4 = Convert.ToInt32(itemObj.GetTable(itemTableName).Current["MTRUNIT4"] != DBNull.Value ? itemObj.GetTable(itemTableName).Current["MTRUNIT4"] : 0);
                var currentmu21 = Convert.ToDouble(itemObj.GetTable(itemTableName).Current["MU21"] != DBNull.Value ? itemObj.GetTable(itemTableName).Current["MU21"] : 0);
                var currentmu31 = Convert.ToDouble(itemObj.GetTable(itemTableName).Current["MU31"] != DBNull.Value ? itemObj.GetTable(itemTableName).Current["MU31"] : 0);

                var currenttypepallet = Convert.ToInt32(itemObj.GetTable(itemTableName).Current["CCCTYPEPALLET"] != DBNull.Value ? itemObj.GetTable(itemTableName).Current["CCCTYPEPALLET"] : 0);
                var currentqtyperpallet = Convert.ToDouble(itemObj.GetTable(itemTableName).Current["CCCQTYPERPALLET"] != DBNull.Value ? itemObj.GetTable(itemTableName).Current["CCCQTYPERPALLET"]:0.0);

                var newmtrunit1 = item.MtrUnit1 != "" ? Convert.ToInt32(item.MtrUnit1) : (int?)null;
                var newmtrunit2 = item.MtrUnit2 != "" ? Convert.ToInt32(item.MtrUnit2) : item.MtrUnit1 != "" ? Convert.ToInt32(item.MtrUnit1) : (int?)null;
                var newmtrunit3 = item.MtrUnit3 != "" ? Convert.ToInt32(item.MtrUnit3) : item.MtrUnit1 != "" ? Convert.ToInt32(item.MtrUnit1) : (int?)null;
                var newmtrunit4 = item.MtrUnit1 != "" ? Convert.ToInt32(item.MtrUnit1) : (int?)null;
                var newmu21 = ((double)1 / (item.Mu21 != "" ? Convert.ToDouble(item.Mu21) : 1));
                var newmu31 = 0.0;

                // Κατά την ενημέρωση του item δεν αλλάζουν οι μονάδες μέτρησης και οι σχέσεις τους 20/01/21
                //itemObj.GetTable(itemTableName).Current["MTRUNIT3"] = item.MtrUnit3 != "" ? Convert.ToInt32(item.MtrUnit3) : item.MtrUnit1 != "" ? Convert.ToInt32(item.MtrUnit1) : (int?)null;

                var varmtrunit3 = item.MtrUnit3 != "" ? item.MtrUnit3 : item.MtrUnit1 != "" ? item.MtrUnit1 : "";

                if (item.MtrUnit1 == varmtrunit3)
                {
                    if (item.Mu31 != "")
                    {
                        newmu31 = item.Mu31 != "" ? Convert.ToDouble(item.Mu31) : item.Mu21 != "" ? Convert.ToDouble(item.Mu21) : 1;
                    }
                    else
                    {
                        newmu31 = (double)1;
                    }
                }
                else
                {
                    if (item.Mu31 != "")
                    {
                        newmu31 = item.Mu31 != "" ? Convert.ToDouble(item.Mu31) : item.Mu21 != "" ? Convert.ToDouble(item.Mu21) : 1;
                    }
                }

                var newtypepallet = item.CccTypePallet != "" ? Convert.ToInt32(item.CccTypePallet) : 0;
                var newqtyperpallet = item.CccQtyPerPallet != "" ? Convert.ToDouble(item.CccQtyPerPallet) : 0.0;

                //20/01/21
                itemObj.GetTable(itemTableName).Current["CCCTYPEPALLET"] = newtypepallet;
                itemObj.GetTable(itemTableName).Current["CCCQTYPERPALLET"] = newqtyperpallet;

                itemObj.GetTable(itemTableName).Current["CCCMTRUNIT5"] = item.MtrUnit2 != "" ? Convert.ToInt32(item.MtrUnit2) : (int?)null;
                var varmtrunit5 = item.MtrUnit2 != "" ? item.MtrUnit2 : "";
                if (varmtrunit5 != "")
                {
                    itemObj.GetTable(itemTableName).Current["CCCMU71"] = item.Mu21 != "" ? Convert.ToDouble(item.Mu21) : (double)1;
                }

                if (currentmtrunit1 != newmtrunit1 
                    || currentmtrunit2 != newmtrunit2 
                    || currentmtrunit3 != newmtrunit3 
                    || currentmtrunit4 != newmtrunit4
                    || currentmu21 != newmu21
                    || currentmu31 != newmu31
                    || currenttypepallet != newtypepallet
                    || currentqtyperpallet != newqtyperpallet)
                {
                    logmtrunitMsg = $"{item.Code} - {item.Name} , (γραμμή αρχείου excel {item.ExcelLineNumber})";
                    var logchanges = "";
                    logchanges = currentmtrunit1 != newmtrunit1 ? logchanges + ",διαφορετική Μ.Μ.1" : logchanges;
                    logchanges = currentmtrunit2 != newmtrunit2 ? logchanges + ",διαφορετική Μ.Μ.2" : logchanges;
                    logchanges = currentmtrunit3 != newmtrunit3 ? logchanges + ",διαφορετική Μ.Μ.Αγορών" : logchanges;
                    logchanges = currentmtrunit4 != newmtrunit4 ? logchanges + ",διαφορετική Μ.Μ.Πωλήσεων" : logchanges;
                    logchanges = currentmu21 != newmu21 ? logchanges + ",διαφορετική σχέση της Μ.Μ.1 με την Μ.Μ.2" : logchanges;
                    logchanges = currentmu31 != newmu31 ? logchanges + ",διαφορετική σχέση της Μ.Μ.1 με την Μονάδα Μέτρησης Αγορών" : logchanges;
                    logchanges = currenttypepallet != newtypepallet ? logchanges + ",διαφορετικός τύπος παλέτας" : logchanges;
                    logchanges = currentqtyperpallet != newqtyperpallet ? logchanges + ",διαφορετική ποσότητα ανά παλέτα" : logchanges;

                    //τελικό Msg
                    logmtrunitMsg = logmtrunitMsg + logchanges;
                }
            }

            //itemObj.GetTable(itemTableName).Current["CCCENGNAME"] = item.CccEngName;
            //itemObj.GetTable(itemTableName).Current["NAME1"] = item.Name;
            itemObj.GetTable(itemTableName).Current["MTRMANFCTR"] = item.MtrManfctr != "" ? Convert.ToInt32(item.MtrManfctr) : (int?)null;
            itemObj.GetTable(extraTableName).Current["UTBL01"] = item.Utbl01 != "" ? Convert.ToInt32(item.Utbl01) : (int?)null;
            itemObj.GetTable(extraTableName).Current["UTBL02"] = item.Utbl02 != "" ? Convert.ToInt32(item.Utbl02) : (int?)null;
            itemObj.GetTable(extraTableName).Current["UTBL03"] = item.Utbl03 != "" ? Convert.ToInt32(item.Utbl03) : (int?)null;
            //itemObj.GetTable(itemTableName).Current["MTRSUP"] = item.Trdr;
            itemObj.GetTable(itemTableName).Current["CCCSTATUS"] = item.CccStatus;
            itemObj.GetTable(itemTableName).Current["INTRASTAT"] = item.Intrastat >0 ? item.Intrastat : (int?)null;
            itemObj.GetTable(itemTableName).Current["CCCORIGIN"] = item.CccOrigin != "" ? Convert.ToInt32(item.CccOrigin) : (int?)null;
            itemObj.GetTable(itemTableName).Current["DIM1"] = item.Dim1 != "" ? Convert.ToDouble(item.Dim1) : 0.0;
            itemObj.GetTable(itemTableName).Current["DIM2"] = item.Dim2 != "" ? Convert.ToDouble(item.Dim2) : 0.0;
            itemObj.GetTable(itemTableName).Current["DIM3"] = item.Dim3 != "" ? Convert.ToDouble(item.Dim3) : 0.0;
            itemObj.GetTable(itemTableName).Current["WEIGHT"] = item.Weight != "" ? Convert.ToDouble(item.Weight) : 0.0;
            itemObj.GetTable(itemTableName).Current["GWEIGHT"] = item.Gweight != "" ? Convert.ToDouble(item.Gweight) : 0.0;
            itemObj.GetTable(itemTableName).Current["VOLUME"] = item.Volume != "" ? Convert.ToDouble(item.Volume) : 0.0;
            //itemObj.GetTable(itemTableName).Current["MTRUNIT3"] = item.MtrUnit3 != "" ? Convert.ToInt32(item.MtrUnit3) : item.MtrUnit1 != "" ? Convert.ToInt32(item.MtrUnit1) : (int?)null;
            //itemObj.GetTable(itemTableName).Current["MTRUNIT4"] = item.MtrUnit1 != "" ? Convert.ToInt32(item.MtrUnit1) : (int?)null;
            itemObj.GetTable(itemTableName).Current["CCCDIM1"] = item.CccDim1 != "" ? Convert.ToDouble(item.CccDim1) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCDIM2"] = item.CccDim2 != "" ? Convert.ToDouble(item.CccDim2) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCDIM3"] = item.CccDim3 != "" ? Convert.ToDouble(item.CccDim3) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCWEIGHT"] = item.CccWeight != "" ? Convert.ToDouble(item.CccWeight) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCGWEIGHT"] = item.CccGweight != "" ? Convert.ToDouble(item.CccGweight) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCVOLUME"] = item.CccVolume != "" ? Convert.ToDouble(item.CccVolume) : 0.0;
            //itemObj.GetTable(itemTableName).Current["MU31"] = item.Mu31 != "" ? Convert.ToDouble(item.Mu31) : item.Mu21 != "" ? Convert.ToDouble(item.Mu21) : 1;
            itemObj.GetTable(itemTableName).Current["REORDERLEVEL"] = item.ReOrderLevel != "" ? Convert.ToDouble(item.ReOrderLevel) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCSTEP"] = item.CccStep != "" ? Convert.ToDouble(item.CccStep) : 0.0;
            //itemObj.GetTable(itemTableName).Current["MTRLOTUSE"] = item.MtrLotUse != "" ? Convert.ToInt32(item.MtrLotUse) : 0;
            var lotuse = item.MtrLotUse != "" ? item.MtrLotUse == "Yes" || item.MtrLotUse == "YES" || item.MtrLotUse == "1" ? 1 : 0 : 0;
            //Convert.ToInt32(item.MtrLotUse) : 0;
            if (lotuse == 1)
            {
                itemObj.GetTable(itemTableName).Current["LOTCODEMASK"] = item.LotCodeMask;
            }
            itemObj.GetTable(itemTableName).Current["CCCPRODUCTDATE"] = item.CccProductDate != "" ? item.CccProductDate == "Yes" || item.CccProductDate == "YES" || item.CccProductDate == "1" ? 1 : 0 : (int?)null;
            itemObj.GetTable(itemTableName).Current["CCCEXPIREDATE"] = item.CccExpireDate != "" ? item.CccExpireDate == "Yes" || item.CccExpireDate == "YES" || item.CccExpireDate == "1" ? 1 : 0 : (int?)null;
            itemObj.GetTable(itemTableName).Current["CCCLIFETIME"] = item.CccLifeTime != "" ? Convert.ToInt32(item.CccLifeTime) : (int?)null;
            itemObj.GetTable(itemTableName).Current["CCCSHELFTIME"] = item.CccShelfTime != "" ? Convert.ToInt32(item.CccShelfTime) : (int?)null;
            //itemObj.GetTable(itemTableName).Current["MTRUNIT2"] = item.MtrUnit2 != "" ? Convert.ToInt32(item.MtrUnit2) : item.MtrUnit1 != "" ? Convert.ToInt32(item.MtrUnit1) : (int?)null;
            //itemObj.GetTable(itemTableName).Current["MU21"] = item.Mu21 != "" ? Convert.ToDouble(item.Mu21) : 1;
            itemObj.GetTable(itemTableName).Current["CCCBOXDIM1"] = item.CccBoxDim1 != "" ? Convert.ToDouble(item.CccBoxDim1) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCBOXDIM2"] = item.CccBoxDim2 != "" ? Convert.ToDouble(item.CccBoxDim2) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCBOXDIM3"] = item.CccBoxDim3 != "" ? Convert.ToDouble(item.CccBoxDim3) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCBOXWEIGHT"] = item.CccBoxWeight != "" ? Convert.ToDouble(item.CccBoxWeight) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCBOXGWEIGHT"] = item.CccBoxGweight != "" ? Convert.ToDouble(item.CccBoxGweight) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCBOXVOLUME"] = item.CccBoxVolume != "" ? Convert.ToDouble(item.CccBoxVolume) : 0.0;
            //itemObj.GetTable(itemTableName).Current["CCCMTRUNIT4"] = item.CccMtrUnit4 != "" ? Convert.ToInt32(item.CccMtrUnit4) : item.MtrUnit1 != "" ? Convert.ToInt32(item.MtrUnit1) : (int?)null;
            //itemObj.GetTable(itemTableName).Current["CCCMU51"] = item.CccMu51 != "" ? Convert.ToDouble(item.CccMu51) : item.Mu21 != "" ? Convert.ToDouble(item.Mu21) : 1;
            //itemObj.GetTable(itemTableName).Current["CCCMU61"] = item.CccMu61 != "" ? Convert.ToDouble(item.CccMu61) : item.Mu21 != "" ? Convert.ToDouble(item.Mu21) : 1;
            itemObj.GetTable(itemTableName).Current["CCCMTRUNIT4"] = item.CccMtrUnit4 != "" ? Convert.ToInt32(item.CccMtrUnit4) : (int?)null;
            itemObj.GetTable(itemTableName).Current["CCCMU51"] = item.CccMu51 != "" ? Convert.ToDouble(item.CccMu51) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCMU61"] = item.CccMu61 != "" ? Convert.ToDouble(item.CccMu61) : 0.0;

            itemObj.GetTable(itemTableName).Current["CCCCARTONDIM1"] = item.CccCartonDim1 != "" ? Convert.ToDouble(item.CccCartonDim1) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCCARTONDIM2"] = item.CccCartonDim2 != "" ? Convert.ToDouble(item.CccCartonDim2) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCCARTONDIM3"] = item.CccCartonDim3 != "" ? Convert.ToDouble(item.CccCartonDim3) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCCARTONWEIGHT"] = item.CccCartonWeight != "" ? Convert.ToDouble(item.CccCartonWeight) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCCARTONGWEIGHT"] = item.CccCartonGweight != "" ? Convert.ToDouble(item.CccCartonGweight) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCCARTONVOLUME"] = item.CccCartonVolume != "" ? Convert.ToDouble(item.CccCartonVolume) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCFIRSTREL"] = item.CccFirstRel != "" ? Convert.ToDouble(item.CccFirstRel) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCSECONDREL"] = item.CccSecondRel != "" ? Convert.ToDouble(item.CccSecondRel) : 0.0;
            itemObj.GetTable(itemTableName).Current["CCCPACKBARCODE"] = item.CccPackBarCode;

            //
            var mtrsubTmp = new List<DataRow>();
            if (item.IsCore == "1")
            {
                if (job == "insert")
                {
                    itemObj.GetTable(itemTableName).Current["MTRTYPE"] = 3;
                }
                else
                {
                    itemObj.GetTable(itemTableName).Current["MTRSUP"] = item.Trdr;

                    mtrsubTmp = itemObj.GetTable(mtrsubtitTableName).CreateDataTable(true).AsEnumerable().ToList();
                    foreach (var newCode in item.MtrSubstituteCode)
                    {
                        var count = mtrsubTmp.Where(x => x["CODE"].ToString() == newCode.MtrSubstituteCode).ToList().Count;
                        if (count == 0)
                        {
                            using (var MtrSubstitute = itemObj.GetTable(mtrsubtitTableName))
                            {
                                MtrSubstitute.Current.Append();
                                MtrSubstitute.Current["CODE"] = newCode.MtrSubstituteCode;
                                MtrSubstitute.Current["NAME"] = item.Name;
                                MtrSubstitute.Current["CCCMTRUNIT"] = Convert.ToInt32(newCode.MtrUnit);
                                MtrSubstitute.Current["QTY1"] = Convert.ToDouble(newCode.Qty1);
                                MtrSubstitute.Current.Post();
                            }
                        }
                    }
                }
            }
            else
            {
                itemObj.GetTable(itemTableName).Current["MTRSUP"] = item.Trdr;

                mtrsubTmp = itemObj.GetTable(mtrsubtitTableName).CreateDataTable(true).AsEnumerable().ToList();
                foreach (var newCode in item.MtrSubstituteCode)
                {
                    var count = mtrsubTmp.Where(x => x["CODE"].ToString() == newCode.MtrSubstituteCode).ToList().Count;
                    if (count == 0)
                    {
                        using (var MtrSubstitute = itemObj.GetTable(mtrsubtitTableName))
                        {
                            MtrSubstitute.Current.Append();
                            MtrSubstitute.Current["CODE"] = newCode.MtrSubstituteCode;
                            MtrSubstitute.Current["NAME"] = item.Name;
                            MtrSubstitute.Current["CCCMTRUNIT"] = Convert.ToInt32(newCode.MtrUnit);
                            MtrSubstitute.Current["QTY1"] = Convert.ToDouble(newCode.Qty1);
                            MtrSubstitute.Current.Post();
                        }
                    }
                }

                if (job == "insert")
                {
                    var mtrsubCode = itemObj.GetTable(mtrsubcodeTableName).CreateDataTable(true).AsEnumerable().ToList();
                    var count2 = mtrsubCode.Where(x => x[mtrsubcodeTableName].ToString() == item.CccEngName).ToList().Count;
                    if (count2 == 0)
                    {
                        using (var MtrSubCode = itemObj.GetTable(mtrsubcodeTableName))
                        {
                            MtrSubCode.Current.Append();
                            MtrSubCode.Current["TRDR"] = item.Trdr;
                            MtrSubCode.Current["CCCMTRSUPCODE"] = item.CccEngName;
                            MtrSubCode.Current.Post();
                        }
                    }
                }
            }
            var mtrlId = itemObj.PostData();

            //CREATE IS CORE ITEM
            if (item.IsCore == "1")
            {
                var coreCode = item.Code;
                var queryCoreCode = /*$@"SELECT CASE WHEN ISNULL(CCCCOREPOSITION,0) = 1 THEN ISNULL(CCCSTARTITEMCODE,'')+ISNULL(CCCCORECODE,'')+'{item.CccEngName}'
                                        WHEN ISNULL(CCCCOREPOSITION,0) = 2 THEN ISNULL(CCCSTARTITEMCODE,'')+'{item.CccEngName}'+ISNULL(CCCCORECODE,'')
                                        ELSE '' END AS CORECODE FROM MTRMANFCTR WHERE MTRMANFCTR={item.MtrManfctr} AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
                    */
                    $@"SELECT CASE WHEN ISNULL(CCCMAINPOSITION,0) = 1 THEN ISNULL(CCCSTARTITEMCODE,'')+ISNULL(CCCMAINCODE, '') + '{item.CccEngName}'
                                                WHEN ISNULL(CCCMAINPOSITION,0) = 2 THEN ISNULL(CCCSTARTITEMCODE,'')+'{item.CccEngName}' + ISNULL(CCCMAINCODE, '')
                                                WHEN ISNULL(CCCMAINPOSITION,0) = 3 THEN ISNULL(CCCSTARTITEMCODE,'')+stuff('{item.CccEngName}', 1, LEN(CCCMAINCODE), CCCMAINCODE)
                                                ELSE ISNULL(CCCSTARTITEMCODE,'')+'{item.CccEngName}' END AS CODE FROM MTRMANFCTR WHERE MTRMANFCTR={item.MtrManfctr} AND COMPANY = {_xSupport.ConnectionInfo.CompanyId}";
                
                using (var dsCoreCode = _xSupport.GetSQLDataSet(queryCoreCode, null))
                {
                    if (dsCoreCode.Count > 0 && dsCoreCode.GetAsString(0, "CODE") != "")
                    {
                        coreCode = dsCoreCode.GetAsString(0, "CODE"); 
                    }
                }

                var queryCheckCore = $@"SELECT MTRL,CODE,NAME FROM MTRL WHERE COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=51
                                        AND CODE='{coreCode}'
                                        UNION ALL
                                        SELECT CCCMTRL,CODE,NAME FROM CCCMTRL WHERE COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=51
                                        AND CODE='{coreCode}'";

                using (var dsCore = _xSupport.GetSQLDataSet(queryCheckCore, null))
                {
                    if (dsCore.Count == 0 && coreCode !="")
                    {
                        var mtrbailTableName = "";
                        if (objName == "CCCITEM")
                        {
                            mtrbailTableName = "CCCMTRBAIL";
                        }
                        if (objName == "ITEM")
                        {
                            mtrbailTableName = "ITEBAIL";
                        }

                        using (var ItemObjCore = _xSupport.CreateModule(objName))
                        {
                            ItemObjCore.InsertData();
                            ItemObjCore.GetTable(itemTableName).Current["CODE"] = coreCode;
                            ItemObjCore.GetTable(itemTableName).Current["NAME"] = item.Name;
                            ItemObjCore.GetTable(itemTableName).Current["MTRACN"] = 101;

                            if (item.MtrUnit1 != "")
                            {
                                ItemObjCore.GetTable(itemTableName).Current["MTRUNIT1"] = item.MtrUnit1 != "" ? Convert.ToInt32(item.MtrUnit1) : (int?)null;
                            }
                            else
                            {
                                throw new Exception("Το είδος για να καταχωρηθεί πρέπει να έχει συμπληρωμένη την Μονάδα Μέτρησης 1.");
                            }

                            ItemObjCore.GetTable(itemTableName).Current["VAT"] = 1410;
                            ItemObjCore.GetTable(itemTableName).Current["SODTYPE"] = 51;
                            if (item.MtrUnit3 != "")
                            {
                                ItemObjCore.GetTable(itemTableName).Current["MTRUNIT2"] = item.MtrUnit3 != "" && item.MtrUnit3 != item.MtrUnit1 ? Convert.ToInt32(item.MtrUnit3) : (int?)null;
                                var varmtrunit2 = item.MtrUnit3 != "" && item.MtrUnit3 != item.MtrUnit1 ? item.MtrUnit3 : "";

                                if (item.MtrUnit3 == varmtrunit2 && item.Mu31 != "")
                                {
                                    ItemObjCore.GetTable(itemTableName).Current["MU21"] = item.Mu31 != "" ? Convert.ToDouble(item.Mu31) : 1;
                                    ItemObjCore.GetTable(itemTableName).Current["MU12MODE"] = 1;
                                }
                                if (item.MtrUnit3 == varmtrunit2 && item.Mu31 == "")
                                {
                                    throw new Exception("Το είδος για να καταχωρηθεί πρέπει να έχει συμπληρωμένη την σχέση της Μονάδας Μέτρησης 1 με την Μονάδα Μέτρησης 2.");
                                }

                                if (item.MtrUnit3 == item.MtrUnit1 || item.MtrUnit3 == varmtrunit2)
                                {
                                    ItemObjCore.GetTable(itemTableName).Current["MTRUNIT3"] = item.MtrUnit3 != "" ? Convert.ToInt32(item.MtrUnit3) : item.MtrUnit1 != "" ? Convert.ToInt32(item.MtrUnit1) : (int?)null;
                                    var varmtrunit3 = item.MtrUnit3 != "" ? item.MtrUnit3 : item.MtrUnit1 != "" ? item.MtrUnit1 : "";

                                    if (varmtrunit3 != "")
                                    {
                                        if (item.MtrUnit1 == varmtrunit3)
                                        {
                                            if (item.Mu31 != "")
                                            {
                                                ItemObjCore.GetTable(itemTableName).Current["MU31"] = item.Mu31 != "" ? Convert.ToDouble(item.Mu31) : (double)1;
                                                ItemObjCore.GetTable(itemTableName).Current["MU13MODE"] = 1;
                                            }
                                            else
                                            {
                                                ItemObjCore.GetTable(itemTableName).Current["MU31"] = (double)1;
                                                ItemObjCore.GetTable(itemTableName).Current["MU13MODE"] = 1;
                                            }
                                        }
                                        else
                                        {
                                            if (item.Mu31 != "")
                                            {
                                                ItemObjCore.GetTable(itemTableName).Current["MU31"] = item.Mu31 != "" ? Convert.ToDouble(item.Mu31) : (double)1;
                                                ItemObjCore.GetTable(itemTableName).Current["MU13MODE"] = 1;
                                            }
                                            else
                                            {
                                                throw new Exception("Το είδος για να καταχωρηθεί πρέπει να έχει συμπληρωμένη την σχέση της Μονάδας Μέτρησης 1 με την Μονάδα Μέτρησης Αγορών.");
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    throw new Exception("Το είδος έχει Μονάδα Μέτρησης Αγορών διαφορετική από την Μονάδα Μέτρησης 1 ή την Μονάδα Μέτρησης 2.");
                                }
                            }
                            else
                            {
                                throw new Exception("Το είδος για να καταχωρηθεί πρέπει να έχει συμπληρωμένη την Μονάδα Μέτρησης Αγορών.");
                            }
                            ItemObjCore.GetTable(itemTableName).Current["MTRUNIT4"] = item.MtrUnit1 != "" ? Convert.ToInt32(item.MtrUnit1) : (int?)null;
                            ItemObjCore.GetTable(itemTableName).Current["MU41"] = (double)1;
                            ItemObjCore.GetTable(itemTableName).Current["MU14MODE"] = 1;
                            ItemObjCore.GetTable(itemTableName).Current["CCCMTRUNIT5"] = item.MtrUnit2 != "" ? Convert.ToInt32(item.MtrUnit2) : (int?)null;
                            var varmtrunit5 = item.MtrUnit2 != "" ? item.MtrUnit2 : "";
                            if (varmtrunit5 != "")
                            {
                                ItemObjCore.GetTable(itemTableName).Current["CCCMU71"] = item.Mu21 != "" ? Convert.ToDouble(item.Mu21) : (double)1;
                            }

                            ItemObjCore.GetTable(itemTableName).Current["MTRLOTUSE"] = item.MtrLotUse != "" ? item.MtrLotUse == "Yes" || item.MtrLotUse == "YES" || item.MtrLotUse == "1" ? 1 : 0 : 0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCTYPEPALLET"] = item.CccTypePallet != "" ? Convert.ToInt32(item.CccTypePallet) : (int?)null;

                            var varccctypepallet = item.CccTypePallet != "" ? item.CccTypePallet : "";
                            var varcccqtyperpallet = item.CccQtyPerPallet != "" ? Convert.ToDouble(item.CccQtyPerPallet) : 0.0;
                            if (varccctypepallet != "" && varcccqtyperpallet != 0.0)
                            {
                                ItemObjCore.GetTable(itemTableName).Current["CCCQTYPERPALLET"] = item.CccQtyPerPallet != "" ? Convert.ToDouble(item.CccQtyPerPallet) : 0.0;
                            }
                            if (varccctypepallet != "" && varcccqtyperpallet == 0.0)
                            {
                                throw new Exception("Το είδος για να καταχωρηθεί ,όταν ο τύπος της παλέτας είναι συμπληρωμένος, πρέπει να είναι συμπληρωμένη και η ποσότητα ανά παλέτα.");
                            }
                            ItemObjCore.GetTable(itemTableName).Current["CCCENGNAME"] = item.CccEngName;
                            ItemObjCore.GetTable(itemTableName).Current["NAME1"] = item.Name;
                            ItemObjCore.GetTable(itemTableName).Current["MTRMANFCTR"] = item.MtrManfctr != "" ? Convert.ToInt32(item.MtrManfctr) : (int?)null;
                            ItemObjCore.GetTable(extraTableName).Current["UTBL01"] = item.Utbl01 != "" ? Convert.ToInt32(item.Utbl01) : (int?)null;
                            ItemObjCore.GetTable(extraTableName).Current["UTBL02"] = item.Utbl02 != "" ? Convert.ToInt32(item.Utbl02) : (int?)null;
                            ItemObjCore.GetTable(extraTableName).Current["UTBL03"] = item.Utbl03 != "" ? Convert.ToInt32(item.Utbl03) : (int?)null;
                            ItemObjCore.GetTable(itemTableName).Current["MTRSUP"] = item.Trdr;
                            ItemObjCore.GetTable(itemTableName).Current["CCCSTATUS"] = item.CccStatus;
                            ItemObjCore.GetTable(itemTableName).Current["INTRASTAT"] = item.Intrastat > 0 ? item.Intrastat : (int?)null;
                            ItemObjCore.GetTable(itemTableName).Current["CCCORIGIN"] = item.CccOrigin != "" ? Convert.ToInt32(item.CccOrigin) : (int?)null;
                            ItemObjCore.GetTable(itemTableName).Current["DIM1"] = item.Dim1 != "" ? Convert.ToDouble(item.Dim1) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["DIM2"] = item.Dim2 != "" ? Convert.ToDouble(item.Dim2) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["DIM3"] = item.Dim3 != "" ? Convert.ToDouble(item.Dim3) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["WEIGHT"] = item.Weight != "" ? Convert.ToDouble(item.Weight) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["GWEIGHT"] = item.Gweight != "" ? Convert.ToDouble(item.Gweight) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["VOLUME"] = item.Volume != "" ? Convert.ToDouble(item.Volume) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCDIM1"] = item.CccDim1 != "" ? Convert.ToDouble(item.CccDim1) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCDIM2"] = item.CccDim2 != "" ? Convert.ToDouble(item.CccDim2) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCDIM3"] = item.CccDim3 != "" ? Convert.ToDouble(item.CccDim3) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCWEIGHT"] = item.CccWeight != "" ? Convert.ToDouble(item.CccWeight) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCGWEIGHT"] = item.CccGweight != "" ? Convert.ToDouble(item.CccGweight) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCVOLUME"] = item.CccVolume != "" ? Convert.ToDouble(item.CccVolume) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["REORDERLEVEL"] = item.ReOrderLevel != "" ? Convert.ToDouble(item.ReOrderLevel) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCSTEP"] = item.CccStep != "" ? Convert.ToDouble(item.CccStep) : 0.0;
                            lotuse = item.MtrLotUse != "" ? item.MtrLotUse == "Yes" || item.MtrLotUse == "YES" || item.MtrLotUse == "1" ? 1 : 0 : 0;
                            if (lotuse == 1)
                            {
                                ItemObjCore.GetTable(itemTableName).Current["LOTCODEMASK"] = item.LotCodeMask;
                            }
                            ItemObjCore.GetTable(itemTableName).Current["CCCPRODUCTDATE"] = item.CccProductDate != "" ? item.CccProductDate == "Yes" || item.CccProductDate == "YES" || item.CccProductDate == "1" ? 1 : 0 : (int?)null;
                            ItemObjCore.GetTable(itemTableName).Current["CCCEXPIREDATE"] = item.CccExpireDate != "" ? item.CccExpireDate == "Yes" || item.CccExpireDate == "YES" || item.CccExpireDate == "1" ? 1 : 0 : (int?)null;
                            ItemObjCore.GetTable(itemTableName).Current["CCCLIFETIME"] = item.CccLifeTime != "" ? Convert.ToInt32(item.CccLifeTime) : (int?)null;
                            ItemObjCore.GetTable(itemTableName).Current["CCCSHELFTIME"] = item.CccShelfTime != "" ? Convert.ToInt32(item.CccShelfTime) : (int?)null;
                            ItemObjCore.GetTable(itemTableName).Current["CCCBOXDIM1"] = item.CccBoxDim1 != "" ? Convert.ToDouble(item.CccBoxDim1) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCBOXDIM2"] = item.CccBoxDim2 != "" ? Convert.ToDouble(item.CccBoxDim2) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCBOXDIM3"] = item.CccBoxDim3 != "" ? Convert.ToDouble(item.CccBoxDim3) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCBOXWEIGHT"] = item.CccBoxWeight != "" ? Convert.ToDouble(item.CccBoxWeight) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCBOXGWEIGHT"] = item.CccBoxGweight != "" ? Convert.ToDouble(item.CccBoxGweight) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCBOXVOLUME"] = item.CccBoxVolume != "" ? Convert.ToDouble(item.CccBoxVolume) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCMTRUNIT4"] = item.CccMtrUnit4 != "" ? Convert.ToInt32(item.CccMtrUnit4) : (int?)null;
                            ItemObjCore.GetTable(itemTableName).Current["CCCMU51"] = item.CccMu51 != "" ? Convert.ToDouble(item.CccMu51) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCMU61"] = item.CccMu61 != "" ? Convert.ToDouble(item.CccMu61) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCCARTONDIM1"] = item.CccCartonDim1 != "" ? Convert.ToDouble(item.CccCartonDim1) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCCARTONDIM2"] = item.CccCartonDim2 != "" ? Convert.ToDouble(item.CccCartonDim2) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCCARTONDIM3"] = item.CccCartonDim3 != "" ? Convert.ToDouble(item.CccCartonDim3) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCCARTONWEIGHT"] = item.CccCartonWeight != "" ? Convert.ToDouble(item.CccCartonWeight) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCCARTONGWEIGHT"] = item.CccCartonGweight != "" ? Convert.ToDouble(item.CccCartonGweight) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCCARTONVOLUME"] = item.CccCartonVolume != "" ? Convert.ToDouble(item.CccCartonVolume) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCFIRSTREL"] = item.CccFirstRel != "" ? Convert.ToDouble(item.CccFirstRel) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCSECONDREL"] = item.CccSecondRel != "" ? Convert.ToDouble(item.CccSecondRel) : 0.0;
                            ItemObjCore.GetTable(itemTableName).Current["CCCPACKBARCODE"] = item.CccPackBarCode;

                            mtrsubTmp = ItemObjCore.GetTable(mtrsubtitTableName).CreateDataTable(true).AsEnumerable().ToList();
                            foreach (var newCode in item.MtrSubstituteCode)
                            {
                                var count = mtrsubTmp.Where(x => x["CODE"].ToString() == newCode.MtrSubstituteCode).ToList().Count;
                                if (count == 0)
                                {
                                    using (var MtrSubstitute = ItemObjCore.GetTable(mtrsubtitTableName))
                                    {
                                        MtrSubstitute.Current.Append();
                                        MtrSubstitute.Current["CODE"] = newCode.MtrSubstituteCode;
                                        MtrSubstitute.Current["NAME"] = item.Name;
                                        MtrSubstitute.Current["CCCMTRUNIT"] = Convert.ToInt32(newCode.MtrUnit);
                                        MtrSubstitute.Current["QTY1"] = Convert.ToDouble(newCode.Qty1);
                                        MtrSubstitute.Current.Post();
                                    }
                                }
                            }

                            var mtrsubCode = ItemObjCore.GetTable(mtrsubcodeTableName).CreateDataTable(true).AsEnumerable().ToList();
                            var count2 = mtrsubCode.Where(x => x[mtrsubcodeTableName].ToString() == item.CccEngName).ToList().Count;
                            if (count2 == 0)
                            {
                                using (var MtrSubCode = ItemObjCore.GetTable(mtrsubcodeTableName))
                                {
                                    MtrSubCode.Current.Append();
                                    MtrSubCode.Current["TRDR"] = item.Trdr;
                                    MtrSubCode.Current["CCCMTRSUPCODE"] = item.CccEngName;
                                    MtrSubCode.Current.Post();
                                }
                            }

                            //Εγγυοδοσία
                            ItemObjCore.GetTable(itemTableName).Current["HASBAIL"] = 1;
                            using (var MtrBail = ItemObjCore.GetTable(mtrbailTableName))
                            {
                                MtrBail.Current.Append();
                                //MtrBail.Current["BAILTYPE"] = 1;
                                MtrBail.Current["CCCMTRLBAIL"] = mtrlId;
                                //MtrBail.Current["MU11"] = (double)1;
                                MtrBail.Current.Post();
                            }
                            var mtrlIdCore = ItemObjCore.PostData();
                        }
                    }
                }
            }

            return logmtrunitMsg;
        }

        internal void InsertPrcrData(PrcrData prcrdata, string objname, string objnamelns, int prcrule)
        {
            //xoris klimakosi
            if (objnamelns == "")
            {
                var strdate = DateTime.Now.ToString("yyyyMMdd");
                var price = "";
                if (prcrule != 9005 && prcrule != 9006)
                {
                    price = (prcrdata.Price != "" ? prcrdata.Price : (prcrdata.NetPrice != "" ? prcrdata.NetPrice : "0")).Replace(",", ".");
                }
                var fromdate = Convert.ToDateTime(prcrdata.FromDate).ToString("yyyyMMdd");
                var discount =Convert.ToString(Convert.ToDouble(prcrdata.Price != "" ? (prcrdata.Discount != "" ? prcrdata.Discount : "0") : "0")*100).Replace(",", ".");
                var dim1 = prcrdata.Trdr;
                var dim2 = 0;
                var objMtrl = "";
                var mtrbailTableName = "";
                var mtrlbail = "";

                switch (prcrule)
                {
                    case 9002: //Προμηθευτής – Κατηγορία Α –Έκπτωση
                        var query2 = $@"SELECT * FROM UTBL01 WHERE SODTYPE=51 AND CCCCODE='{prcrdata.CategoryA}' AND CCCTRDR = {prcrdata.Trdr} AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
                        using (var ds2 = _xSupport.GetSQLDataSet(query2, null))
                        {
                            if (ds2.Count > 0)
                            {
                                dim2 = ds2.GetAsInteger(0, "UTBL01");
                            }
                        }
                        discount = Convert.ToString(Convert.ToDouble(prcrdata.Discount != "" ? prcrdata.Discount : "0") * 100).Replace(",", ".");
                        break;

                    case 9003: //Προμηθευτής – Κατηγορία Β –Έκπτωση
                        var query3 = $@"SELECT * FROM UTBL02 WHERE SODTYPE=51 AND CCCCODE='{prcrdata.CategoryB}' AND CCCTRDR = {prcrdata.Trdr} AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
                        using (var ds3 = _xSupport.GetSQLDataSet(query3, null))
                        {
                            if (ds3.Count > 0)
                            {
                                dim2 = ds3.GetAsInteger(0, "UTBL02");
                            }
                        }
                        discount = Convert.ToString(Convert.ToDouble(prcrdata.Discount != "" ? prcrdata.Discount : "0")*100).Replace(",", ".");
                        break;

                    case 9004: //Προμηθευτής – Κατηγορία Γ –Έκπτωση
                        var query4 = $@"SELECT * FROM UTBL03 WHERE SODTYPE=51 AND CCCCODE='{prcrdata.CategoryC}' AND CCCTRDR = {prcrdata.Trdr} AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";
                        using (var ds4 = _xSupport.GetSQLDataSet(query4, null))
                        {
                            if (ds4.Count > 0)
                            {
                                dim2 = ds4.GetAsInteger(0, "UTBL03");
                            }
                        }
                        discount = Convert.ToString(Convert.ToDouble(prcrdata.Discount != "" ? prcrdata.Discount : "0") * 100).Replace(",", ".");
                        break;

                    case 9005: //Προμηθευτής – Είδος – Lead Charge
                        dim2 = prcrdata.Mtrl;
                        price = prcrdata.LeadCharge.Replace(",", ".");
                        break;

                    case 9006: //Προμηθευτής – Είδος – Core Cost
                        /*
                        var lastcharpersupplier = "";
                        if(prcrdata.Trdr == 20141) //ROBERT BOSCH ΑΝΩΝΥΜΗ ΕΜΠΟΡΙΚΗ ΕΤΑΙΡΕΙΑ ΗΛΕΚΤΡΟΝΙΚΩΝ ΚΑΙ ΗΛΕΚΤΡΟΛΟΓΙΚΩΝ ΠΡΟΙΟΝΤΩΝ
                        {
                            lastcharpersupplier = "REM";
                        }
                        */

                        if (objname == "CCCPRCRDATA")
                        {
                            objMtrl = "CCCMTRL";
                            mtrbailTableName = "CCCMTRBAIL";
                            mtrlbail = "CCCMTRLBAIL";
                        }
                        else
                        {
                            objMtrl = "MTRL";
                            mtrbailTableName = "MTRBAIL";
                            mtrlbail = "MTRLBAIL";
                        }

                        /*
                        var querycorecostmtrl = $@"SELECT * FROM {objMtrl} WHERE CODE =
                                                (SELECT CASE WHEN CHARINDEX('{lastcharpersupplier}', '{prcrdata.Code}')>0 
                                                THEN SUBSTRING('{prcrdata.Code}', 1,
                                                CASE WHEN ((CHARINDEX('{lastcharpersupplier}', '{prcrdata.Code}')-1))<0 THEN 0 ELSE ((CHARINDEX('{lastcharpersupplier}', '{prcrdata.Code}')-1)) END
                                                ) ELSE '{prcrdata.Code}' END )";
                        */
                        var coreCode = prcrdata.Code;

                        var queryCoreCode = /*$@"SELECT CASE WHEN ISNULL(CCCCOREPOSITION,0) = 1 THEN ISNULL(CCCSTARTITEMCODE,'')+ISNULL(CCCCORECODE,'')+'{prcrdata.CccEngName}'
                                        WHEN ISNULL(CCCCOREPOSITION,0) = 2 THEN ISNULL(CCCSTARTITEMCODE,'')+'{prcrdata.CccEngName}'+ISNULL(CCCCORECODE,'')
                                        ELSE '' END AS CORECODE FROM MTRMANFCTR WHERE MTRMANFCTR={prcrdata.MtrManfctrId} AND COMPANY={_xSupport.ConnectionInfo.CompanyId}";*/
                            $@"SELECT CASE WHEN ISNULL(CCCMAINPOSITION,0) = 1 THEN ISNULL(CCCSTARTITEMCODE,'')+ISNULL(CCCMAINCODE, '') + '{prcrdata.CccEngName}'
                                                WHEN ISNULL(CCCMAINPOSITION,0) = 2 THEN ISNULL(CCCSTARTITEMCODE,'')+'{prcrdata.CccEngName}' + ISNULL(CCCMAINCODE, '')
                                                WHEN ISNULL(CCCMAINPOSITION,0) = 3 THEN ISNULL(CCCSTARTITEMCODE,'')+stuff('{prcrdata.CccEngName}', 1, LEN(CCCMAINCODE), CCCMAINCODE)
                                                ELSE ISNULL(CCCSTARTITEMCODE,'')+'{prcrdata.CccEngName}' END AS CODE FROM MTRMANFCTR WHERE CODE='{prcrdata.MtrManfctrCode}' AND COMPANY = {_xSupport.ConnectionInfo.CompanyId}";

                        using (var dsCoreCode = _xSupport.GetSQLDataSet(queryCoreCode, null))
                        {
                            if (dsCoreCode.Count > 0 && dsCoreCode.GetAsString(0, "CODE") != "")
                            {
                                coreCode = dsCoreCode.GetAsString(0, "CODE");
                            }
                        }

                        var querycorecostmtrl = $@"SELECT A.CCCMTRL,A.CODE AS CODECORE,B.{mtrlbail} AS MTRLBAIL
                                                    FROM {objMtrl} A 
                                                    INNER JOIN {mtrbailTableName} B ON A.{objMtrl} = B.{objMtrl}  
                                                    WHERE A.CODE='{coreCode}'";
                            //$@"SELECT * FROM {objMtrl} WHERE CODE ={coreCode} AND COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=51";

                        using (var dscorecostmtrl = _xSupport.GetSQLDataSet(querycorecostmtrl, null))
                        {
                            if (dscorecostmtrl.Count > 0)
                            {
                                //dim2 = prcrdata.Mtrl;
                                //dim2 = dscorecostmtrl.GetAsInteger(0, "MTRL");
                                //dim2 = dscorecostmtrl.GetAsInteger(0, objMtrl);
                                dim2 = dscorecostmtrl.GetAsInteger(0, "MTRLBAIL");
                            }
                        }
                        price = prcrdata.CoreCost.Replace(",", ".");
                        break;
                }

                var query = "";
                if (prcrule != 9002 && prcrule != 9003 && prcrule != 9004)
                {
                    if (price != "0")
                    {
                        query = $@"INSERT INTO {objname} (COMPANY, SODTYPE, SOTYPE, PRCRULE, DIM1, DIM2, DIM3, FROMDATE, LOCKID, FLD01, FLD02) 
                        VALUES ({_xSupport.ConnectionInfo.CompanyId}, 12, 1, {prcrule}, {dim1}, {dim2}, 0, '{fromdate}', 0, {price}, {discount})";
                    }
                }
                else //Προμηθευτής – Κατηγορίες – Έκπτωση
                {
                    if (discount != "0")
                    {
                        query = $@"INSERT INTO {objname} (COMPANY, SODTYPE, SOTYPE, PRCRULE, DIM1, DIM2, DIM3, FROMDATE, LOCKID, FLD01) 
                        VALUES ({_xSupport.ConnectionInfo.CompanyId}, 12, 1, {prcrule}, {dim1}, {dim2}, 0, '{fromdate}', 0,{discount})";
                    }
                }

                var querycheck = $@"SELECT * FROM {objname} WHERE PRCRULE ={prcrule} AND COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=12 AND SOTYPE=1 
                                    AND DIM1={dim1} AND DIM2={dim2} AND DIM3=0 AND FROMDATE ='{fromdate}'";

                using (var dscheck = _xSupport.GetSQLDataSet(querycheck, null))
                {
                    if (dscheck.Count > 0)
                    {
                        var update = "";
                        if (prcrule != 9002 && prcrule != 9003 && prcrule != 9004)
                        {
                            if (price != "0")
                            {
                                update = $@"UPDATE {objname} SET FLD01 = {price} ,FLD02 = {discount} WHERE PRCRULE ={prcrule} AND COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=12 AND SOTYPE=1 
                                    AND DIM1={dim1} AND DIM2={dim2} AND DIM3=0 AND FROMDATE ='{fromdate}'";
                            }
                        }
                        else
                        {
                            if (discount != "0")
                            {
                                update = $@"UPDATE {objname} SET FLD01 = {discount} WHERE PRCRULE ={prcrule} AND COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=12 AND SOTYPE=1 
                                    AND DIM1={dim1} AND DIM2={dim2} AND DIM3=0 AND FROMDATE ='{fromdate}'";
                            }
                        }
                       //_xSupport.ExecuteSQL(update, null);
                    }
                    if(dscheck.Count == 0 && dim2 > 0)
                    {
                        _xSupport.ExecuteSQL(query, null);
                    }
                }  
            }

            //meklimakosi
            if (objnamelns != "")
            {
                var strdate2 = DateTime.Now.ToString("yyyyMMdd");
                var fromdate = Convert.ToDateTime(prcrdata.FromDate).ToString("yyyyMMdd");
                var query2 = $@"INSERT INTO {objname} (COMPANY, SODTYPE, SOTYPE, PRCRULE, DIM1, DIM2, DIM3, FROMDATE, LOCKID) 
                            VALUES ({_xSupport.ConnectionInfo.CompanyId}, 12, 1, {prcrule}, {prcrdata.Trdr}, {prcrdata.Mtrl}, 0, '{fromdate}', 2)";

                var querycheck = $@"SELECT * FROM {objname} WHERE PRCRULE ={prcrule} AND COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=12 AND SOTYPE=1 
                                    AND DIM1={prcrdata.Trdr} AND DIM2={prcrdata.Mtrl} AND DIM3=0 AND FROMDATE ='{fromdate}'";

                using (var dscheck = _xSupport.GetSQLDataSet(querycheck, null))
                {
                    if (dscheck.Count == 0)
                    {
                        _xSupport.ExecuteSQL(query2, null);
                    }
                }

                var querymaxlinenum = $@"SELECT MAX(PRCRDATALNS) MAXNUM FROM {objnamelns} WHERE PRCRULE = {prcrule}";
                var maxnum = 0;
                using (var dsmaxlinemun = _xSupport.GetSQLDataSet(querymaxlinenum, null))
                {
                    if (dsmaxlinemun.Count > 0)
                    {
                        maxnum = dsmaxlinemun.GetAsInteger(0, "MAXNUM");
                    }
                }

                if (prcrdata.PrcrdataLines.Count() == 0)
                {
                    var scaleqty = "1";
                    var price = (prcrdata.Price != "" ? prcrdata.Price : (prcrdata.NetPrice != "" ? prcrdata.NetPrice : "0")).Replace(",", ".");
                    var discount = Convert.ToString(Convert.ToDouble(prcrdata.Price != "" ? (prcrdata.Discount != "" ? prcrdata.Discount : "0") : "0")*100).Replace(",", ".");
                    var query3 = $@"INSERT INTO {objnamelns} (COMPANY, SODTYPE, SOTYPE, PRCRULE, DIM1, DIM2, DIM3, PRCRDATALNS, LINENUM, FROMDATE, FLD01, FLD02, SCALEQTY) 
                                 VALUES ({_xSupport.ConnectionInfo.CompanyId}, 12, 1, {prcrule}, {prcrdata.Trdr}, {prcrdata.Mtrl}, 0,{maxnum},{maxnum},'{fromdate}',{price}, {discount}, {scaleqty})";

                    var querychecklines = $@"SELECT * FROM {objnamelns} WHERE PRCRULE ={prcrule} AND COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=12 AND SOTYPE=1 
                                    AND DIM1={prcrdata.Trdr} AND DIM2={prcrdata.Mtrl} AND DIM3=0 AND SCALEQTY ={scaleqty} AND FROMDATE ='{fromdate}'";

                    using (var dscheck = _xSupport.GetSQLDataSet(querychecklines, null))
                    {
                        if (dscheck.Count > 0)
                        {
                            if (price != "0")
                            {
                                var update = $@"UPDATE {objnamelns} SET FLD01 = {price} ,FLD02 = {discount} WHERE PRCRULE ={prcrule} AND COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=12 AND SOTYPE=1 
                                    AND DIM1={prcrdata.Trdr} AND DIM2={prcrdata.Mtrl} AND DIM3=0 AND SCALEQTY ={scaleqty} AND FROMDATE ='{fromdate}'";
                                //_xSupport.ExecuteSQL(update, null);
                            }
                        }
                        else
                        {
                            if (price != "0")
                            {
                                _xSupport.ExecuteSQL(query3, null);
                            }
                        }
                    }
                }
                else
                {
                    var dataForOneQty = prcrdata.PrcrdataLines.Where(x => x.Moq == "1").ToList();
                    var scaleqtyExtra = "0";
                    var priceExtra = "0";
                    var discountExtra = "0";

                    if (dataForOneQty.Count == 0 && (prcrdata.Price !="" || prcrdata.NetPrice !="" || prcrdata.Discount !=""))
                    {
                        maxnum++;
                        scaleqtyExtra = "1";
                        priceExtra = prcrdata.Price != "" ? (prcrdata.Price).Replace(",", ".") : (prcrdata.NetPrice != "" ? prcrdata.NetPrice : "0").Replace(",", ".");
                        discountExtra = prcrdata.Discount != "" ? (prcrdata.Discount).Replace(",", ".") : "0";
                        var queryExtra = $@"INSERT INTO {objnamelns} (COMPANY, SODTYPE, SOTYPE, PRCRULE, DIM1, DIM2, DIM3, PRCRDATALNS, LINENUM, FROMDATE, FLD01, FLD02, SCALEQTY) 
                                     VALUES ({_xSupport.ConnectionInfo.CompanyId}, 12, 1, {prcrule}, {prcrdata.Trdr}, {prcrdata.Mtrl}, 0,{maxnum},{maxnum},'{fromdate}',{priceExtra}, {discountExtra}, {scaleqtyExtra})";
                        
                        var querychecklinesExtra = $@"SELECT * FROM {objnamelns} WHERE PRCRULE ={prcrule} AND COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=12 AND SOTYPE=1 
                                    AND DIM1={prcrdata.Trdr} AND DIM2={prcrdata.Mtrl} AND DIM3=0 AND SCALEQTY ={scaleqtyExtra} AND FROMDATE ='{fromdate}'";

                        using (var dscheck = _xSupport.GetSQLDataSet(querychecklinesExtra, null))
                        {
                            if (dscheck.Count > 0)
                            {
                                if (priceExtra != "0")
                                {
                                    var update = $@"UPDATE {objnamelns} SET FLD01 = {priceExtra} ,FLD02 = {discountExtra} WHERE PRCRULE ={prcrule} AND COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=12 AND SOTYPE=1 
                                                    AND DIM1={prcrdata.Trdr} AND DIM2={prcrdata.Mtrl} AND DIM3=0 AND SCALEQTY ={scaleqtyExtra} AND FROMDATE ='{fromdate}'";
                                    //_xSupport.ExecuteSQL(update, null);
                                }
                            }
                            else
                            {
                                if (priceExtra != "0")
                                {
                                    _xSupport.ExecuteSQL(queryExtra, null);
                                }
                            }
                        }
                    }
                    else if (dataForOneQty.Count == 1 && (prcrdata.Price != "" || prcrdata.NetPrice != "" || prcrdata.Discount != ""))
                    {
                        priceExtra = prcrdata.Price != "" ? (prcrdata.Price).Replace(",", ".") : (prcrdata.NetPrice != "" ? prcrdata.NetPrice : "0").Replace(",", ".");
                        discountExtra = prcrdata.Discount != "" ? (prcrdata.Discount).Replace(",", ".") : "0";
                    }


                    foreach (var newPrcrDataline in prcrdata.PrcrdataLines)
                    {
                        maxnum++;
                        var scaleqty = (prcrdata.PrcrdataLines.Count() > 0 ? newPrcrDataline.Moq : "1").Replace(",", ".");
                        var price = "";
                        var discount = "";
                        if (scaleqty == "1" && (priceExtra != "0" || discountExtra != "0"))
                        {
                            price = priceExtra;
                            discount = discountExtra;
                        }
                        else
                        {
                            price = (prcrdata.PrcrdataLines.Count() > 0 ? (newPrcrDataline.MoqPrice != "" ? newPrcrDataline.MoqPrice : newPrcrDataline.MoqNetPrice) : (prcrdata.Price != "" ? prcrdata.Price : (prcrdata.NetPrice != "" ? prcrdata.NetPrice : "0"))).Replace(",", ".");
                            discount = (prcrdata.PrcrdataLines.Count() > 0 ? (newPrcrDataline.MoqPrice != "" ? (newPrcrDataline.MoqDiscount == "" ? "0" : newPrcrDataline.MoqDiscount) : "0") : (prcrdata.Price != "" ? (prcrdata.Discount != "" ? prcrdata.Discount : "0") : "0")).Replace(",", ".");
                        }
                        var count = prcrdata.PrcrdataLines.Count();
                        if (count > 0)
                        {
                            var query3 = $@"INSERT INTO {objnamelns} (COMPANY, SODTYPE, SOTYPE, PRCRULE, DIM1, DIM2, DIM3, PRCRDATALNS, LINENUM, FROMDATE, FLD01, FLD02, SCALEQTY) 
                                     VALUES ({_xSupport.ConnectionInfo.CompanyId}, 12, 1, {prcrule}, {prcrdata.Trdr}, {prcrdata.Mtrl}, 0,{maxnum},{maxnum},'{fromdate}',{price}, {discount}, {scaleqty})";

                            var querychecklines = $@"SELECT * FROM {objnamelns} WHERE PRCRULE ={prcrule} AND COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=12 AND SOTYPE=1 
                                    AND DIM1={prcrdata.Trdr} AND DIM2={prcrdata.Mtrl} AND DIM3=0 AND SCALEQTY ={scaleqty} AND FROMDATE ='{fromdate}'";

                            using (var dscheck = _xSupport.GetSQLDataSet(querychecklines, null))
                            {
                                if (dscheck.Count > 0)
                                {
                                    if (price != "0")
                                    {
                                        var update = $@"UPDATE {objnamelns} SET FLD01 = {price} ,FLD02 = {discount} WHERE PRCRULE ={prcrule} AND COMPANY={_xSupport.ConnectionInfo.CompanyId} AND SODTYPE=12 AND SOTYPE=1 
                                                    AND DIM1={prcrdata.Trdr} AND DIM2={prcrdata.Mtrl} AND DIM3=0 AND SCALEQTY ={scaleqty} AND FROMDATE ='{fromdate}'";
                                        //_xSupport.ExecuteSQL(update, null);
                                    }
                                }
                                else
                                {
                                    if (price != "0")
                                    {
                                        _xSupport.ExecuteSQL(query3, null);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private static bool inBounds(int index, List<string> array)
        {
            return (index >= 0) && (index < array.Count);
        }
    }
}
