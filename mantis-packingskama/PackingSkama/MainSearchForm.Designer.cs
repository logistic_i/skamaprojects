﻿using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Tile.ViewInfo;

namespace PackingSkama
{
    partial class MainSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        //private void InitializeComponent()
        //{
        //    System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainSearchForm));
        //    this.PackingFlag = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.gridControl1 = new DevExpress.XtraGrid.GridControl();
        //    this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
        //    this.ord_ID = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.ord_Code = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.ost_ID = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.ost_Code = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.ord_TypeID = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.ord_InputDate = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.ost_ExecuteDate = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.ost_ShipDate = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.orr_Code = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.orr_FullName = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.msg_greek = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.ost_StatusID = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.cpk_GroupID = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.RoutID = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.RoutDesc = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.TranspID = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.TranspDescr = new DevExpress.XtraGrid.Columns.GridColumn();
        //    this.MainToolStrip = new System.Windows.Forms.ToolStrip();
        //    this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
        //    this.toolStripSearch = new System.Windows.Forms.ToolStripButton();
        //    this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
        //    this.toolStripClear = new System.Windows.Forms.ToolStripButton();
        //    this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
        //    this.toolStripExit = new System.Windows.Forms.ToolStripButton();
        //    this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
        //    this.label1 = new System.Windows.Forms.Label();
        //    this.label2 = new System.Windows.Forms.Label();
        //    this.label3 = new System.Windows.Forms.Label();
        //    this.label4 = new System.Windows.Forms.Label();
        //    this.dateTimeFromDate = new System.Windows.Forms.DateTimePicker();
        //    this.dateTimeEndDate = new System.Windows.Forms.DateTimePicker();
        //    this.textOrderSearch = new System.Windows.Forms.TextBox();
        //    this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
        //    this.buttonSentData = new System.Windows.Forms.Button();
        //    ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
        //    ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
        //    this.MainToolStrip.SuspendLayout();
        //    this.SuspendLayout();

        //    PackingFlag


        //    this.PackingFlag.Caption = "PackingFlag";
        //    this.PackingFlag.FieldName = "PackingFlag";
        //    this.PackingFlag.Name = "PackingFlag";

        //    gridControl1


        //    this.gridControl1.Location = new System.Drawing.Point(24, 104);
        //    this.gridControl1.MainView = this.gridView1;
        //    this.gridControl1.Name = "gridControl1";
        //    this.gridControl1.Size = new System.Drawing.Size(1600, 584);
        //    this.gridControl1.TabIndex = 0;
        //    this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
        //    this.gridView1});

        //    gridView1


        //    this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
        //    this.ord_ID,
        //    this.ord_Code,
        //    this.ost_ID,
        //    this.ost_Code,
        //    this.ord_TypeID,
        //    this.ord_InputDate,
        //    this.ost_ExecuteDate,
        //    this.ost_ShipDate,
        //    this.orr_Code,
        //    this.orr_FullName,
        //    this.msg_greek,
        //    this.PackingFlag,
        //    this.cpk_GroupID,
        //    this.RoutID,
        //    this.RoutDesc,
        //    this.TranspID,
        //    this.TranspDescr
        //    });
        //    this.gridView1.GridControl = this.gridControl1;
        //    this.gridView1.Name = "gridView1";
        //    this.gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
        //    this.gridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
        //    this.gridView1.OptionsBehavior.ReadOnly = true;
        //    this.gridView1.OptionsSelection.MultiSelect = true;
        //    this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
        //    this.gridView1.OptionsSelection.ShowCheckBoxSelectorInGroupRow = DevExpress.Utils.DefaultBoolean.True;
        //    this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
        //    this.gridView1.SelectionChanged += GridView1_SelectionChanged;
        //    this.gridView1.PopupMenuShowing += GridView1_PopupMenuShowing;
        //    toolStripSearch.Click += ToolStripSearch_Click;
        //    toolStripExit.Click += ToolStripExit_Click;
        //    toolStripClear.Click += ToolStripClear_Click;

        //    ord_ID


        //    this.ord_ID.Caption = "ID Παραγγελίας";
        //    this.ord_ID.FieldName = "ord_ID";
        //    this.ord_ID.Name = "ord_ID";

        //    ord_Code


        //    this.ord_Code.Caption = "Κωδικός Παραγγελίας";
        //    this.ord_Code.FieldName = "ord_Code";
        //    this.ord_Code.Name = "ord_Code";
        //    this.ord_Code.Visible = true;
        //    this.ord_Code.VisibleIndex = 1;

        //    ost_ID


        //    this.ost_ID.Caption = "ID Αποστολής";
        //    this.ost_ID.FieldName = "ost_ID";
        //    this.ost_ID.Name = "ost_ID";

        //    ost_Code


        //    this.ost_Code.Caption = "Αποστολή Παραγγελίας";
        //    this.ost_Code.FieldName = "ost_Code";
        //    this.ost_Code.Name = "ost_Code";
        //    this.ost_Code.Visible = true;
        //    this.ost_Code.VisibleIndex = 2;

        //    ord_TypeID


        //    this.ord_TypeID.Caption = "ID Τύπου Παραγγελίας";
        //    this.ord_TypeID.FieldName = "ord_TypeID";
        //    this.ord_TypeID.Name = "ord_TypeID";

        //    ord_InputDate


        //    this.ord_InputDate.Caption = "Ημ. Παραγγελίας";
        //    this.ord_InputDate.FieldName = "ord_InputDate";
        //    this.ord_InputDate.Name = "ord_InputDate";
        //    this.ord_InputDate.Visible = true;
        //    this.ord_InputDate.VisibleIndex = 3;

        //    ost_ExecuteDate


        //    this.ost_ExecuteDate.Caption = "Ημ. Εκτέλεσης";
        //    this.ost_ExecuteDate.FieldName = "ost_ExecuteDate";
        //    this.ost_ExecuteDate.Name = "ost_ExecuteDate";
        //    this.ost_ExecuteDate.Visible = true;
        //    this.ost_ExecuteDate.VisibleIndex = 4;

        //    ost_ShipDate


        //    this.ost_ShipDate.Caption = "Ημ. Αποστολής";
        //    this.ost_ShipDate.FieldName = "ost_ShipDate";
        //    this.ost_ShipDate.Name = "ost_ShipDate";
        //    this.ost_ShipDate.Visible = true;
        //    this.ost_ShipDate.VisibleIndex = 5;

        //    orr_Code


        //    this.orr_Code.Caption = "Κωδ. Παραλήπτη";
        //    this.orr_Code.FieldName = "orr_Code";
        //    this.orr_Code.Name = "orr_Code";
        //    this.orr_Code.Visible = true;
        //    this.orr_Code.VisibleIndex = 6;

        //    orr_FullName


        //    this.orr_FullName.Caption = "Επωνυμία Παραλήπτη";
        //    this.orr_FullName.FieldName = "orr_FullName";
        //    this.orr_FullName.Name = "orr_FullName";
        //    this.orr_FullName.Visible = true;
        //    this.orr_FullName.VisibleIndex = 7;

        //    msg_greek


        //    this.msg_greek.Caption = "Κατάσταση";
        //    this.msg_greek.FieldName = "msg_greek";
        //    this.msg_greek.Name = "msg_greek";
        //    this.msg_greek.Visible = true;
        //    this.msg_greek.VisibleIndex = 8;

        //    ost_StatusID


        //    this.ost_StatusID.Caption = "ost_StatusID";
        //    this.ost_StatusID.FieldName = "ost_StatusID";
        //    this.ost_StatusID.Name = "ost_StatusID";
        //    cpk_GroupID
        //    this.cpk_GroupID.Caption = "cpk_GroupID";
        //    this.cpk_GroupID.FieldName = "cpk_GroupID";
        //    this.cpk_GroupID.Name = "cpk_GroupID";
        //    this.cpk_GroupID.Visible = false;
        //    RoutID
        //    this.RoutID.Caption = "RoutID";
        //    this.RoutID.FieldName = "RoutID";
        //    this.RoutID.Name = "RoutID";
        //    this.RoutID.Visible = false;
        //    RoutDescr
        //    this.RoutDesc.Caption = "Δρομολόγιο";
        //    this.RoutDesc.FieldName = "RoutDesc";
        //    this.RoutDesc.Name = "RoutDesc";
        //    this.RoutDesc.Visible = true;
        //    this.RoutDesc.VisibleIndex = 9;
        //    TranspID
        //    this.TranspID.Caption = "TranspID";
        //    this.TranspID.FieldName = "TranspID";
        //    this.TranspID.Name = "TranspID";
        //    this.Visible = false;
        //    TranspDescr
        //    this.TranspDescr.Caption = "Μεταφορέας";
        //    this.TranspDescr.FieldName = "TranspDesc";
        //    this.TranspDescr.Name = "TranspDesc";
        //    this.Visible = true;
        //    this.TranspDescr.VisibleIndex = 10;


        //    MainToolStrip


        //    this.MainToolStrip.AllowMerge = false;
        //    this.MainToolStrip.BackColor = System.Drawing.SystemColors.ActiveCaption;
        //    this.MainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
        //    this.toolStripSeparator1,
        //    this.toolStripSearch,
        //    this.toolStripSeparator2,
        //    this.toolStripClear,
        //    this.toolStripSeparator3,
        //    this.toolStripExit,
        //    this.toolStripSeparator4});
        //    this.MainToolStrip.Location = new System.Drawing.Point(0, 0);
        //    this.MainToolStrip.Name = "MainToolStrip";
        //    this.MainToolStrip.Size = new System.Drawing.Size(1181, 25);
        //    this.MainToolStrip.TabIndex = 1;
        //    this.MainToolStrip.Text = "MainToolStrip";

        //    toolStripSeparator1


        //    this.toolStripSeparator1.Name = "toolStripSeparator1";
        //    this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);

        //    toolStripSearch


        //    this.toolStripSearch.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
        //    this.toolStripSearch.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSearch.Image")));
        //    this.toolStripSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
        //    this.toolStripSearch.Name = "toolStripSearch";
        //    this.toolStripSearch.Size = new System.Drawing.Size(91, 22);
        //    this.toolStripSearch.Text = "Αναζήτηση";

        //    toolStripSeparator2


        //    this.toolStripSeparator2.Name = "toolStripSeparator2";
        //    this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);

        //    toolStripClear


        //    this.toolStripClear.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
        //    this.toolStripClear.Image = ((System.Drawing.Image)(resources.GetObject("toolStripClear.Image")));
        //    this.toolStripClear.ImageTransparentColor = System.Drawing.Color.Magenta;
        //    this.toolStripClear.Name = "toolStripClear";
        //    this.toolStripClear.Size = new System.Drawing.Size(107, 22);
        //    this.toolStripClear.Text = "Αρχικοποίηση";

        //    toolStripSeparator3


        //    this.toolStripSeparator3.Name = "toolStripSeparator3";
        //    this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);

        //    toolStripExit


        //    this.toolStripExit.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
        //    this.toolStripExit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripExit.Image")));
        //    this.toolStripExit.ImageTransparentColor = System.Drawing.Color.Magenta;
        //    this.toolStripExit.Name = "toolStripExit";
        //    this.toolStripExit.Size = new System.Drawing.Size(68, 22);
        //    this.toolStripExit.Text = "Έξοδος";

        //    toolStripSeparator4


        //    this.toolStripSeparator4.Name = "toolStripSeparator4";
        //    this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);

        //    label1


        //    this.label1.AutoSize = true;
        //    this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
        //    this.label1.Location = new System.Drawing.Point(24, 33);
        //    this.label1.Name = "label1";
        //    this.label1.Size = new System.Drawing.Size(161, 16);
        //    this.label1.TabIndex = 4;
        //    this.label1.Text = "Ημερομηνία καταχώρησης";

        //    label2


        //    this.label2.AutoSize = true;
        //    this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
        //    this.label2.Location = new System.Drawing.Point(24, 55);
        //    this.label2.Name = "label2";
        //    this.label2.Size = new System.Drawing.Size(39, 16);
        //    this.label2.TabIndex = 5;
        //    this.label2.Text = "Από :";

        //    label3


        //    this.label3.AutoSize = true;
        //    this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
        //    this.label3.Location = new System.Drawing.Point(171, 55);
        //    this.label3.Name = "label3";
        //    this.label3.Size = new System.Drawing.Size(38, 16);
        //    this.label3.TabIndex = 6;
        //    this.label3.Text = "Εώς :";

        //    label4


        //    this.label4.AutoSize = true;
        //    this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
        //    this.label4.Location = new System.Drawing.Point(430, 55);
        //    this.label4.Name = "label4";
        //    this.label4.Size = new System.Drawing.Size(157, 16);
        //    this.label4.TabIndex = 7;
        //    this.label4.Text = "Αναζήτηση Παραγγελιών";

        //    dateTimeFromDate


        //    this.dateTimeFromDate.Checked = false;
        //    this.dateTimeFromDate.CustomFormat = "dd-MM-yyyy";
        //    this.dateTimeFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
        //    this.dateTimeFromDate.ImeMode = System.Windows.Forms.ImeMode.Disable;
        //    this.dateTimeFromDate.Location = new System.Drawing.Point(24, 78);
        //    this.dateTimeFromDate.Name = "dateTimeFromDate";
        //    this.dateTimeFromDate.ShowCheckBox = true;
        //    this.dateTimeFromDate.Size = new System.Drawing.Size(115, 20);
        //    this.dateTimeFromDate.TabIndex = 2;

        //    dateTimeEndDate


        //    this.dateTimeEndDate.Checked = false;
        //    this.dateTimeEndDate.CustomFormat = "dd-MM-yyyy";
        //    this.dateTimeEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
        //    this.dateTimeEndDate.Location = new System.Drawing.Point(174, 78);
        //    this.dateTimeEndDate.Name = "dateTimeEndDate";
        //    this.dateTimeEndDate.ShowCheckBox = true;
        //    this.dateTimeEndDate.Size = new System.Drawing.Size(115, 20);
        //    this.dateTimeEndDate.TabIndex = 3;

        //    textOrderSearch


        //    this.textOrderSearch.Location = new System.Drawing.Point(433, 78);
        //    this.textOrderSearch.Name = "textOrderSearch";
        //    this.textOrderSearch.Size = new System.Drawing.Size(300, 20);
        //    this.textOrderSearch.TabIndex = 8;

        //    buttonSentData


        //    this.buttonSentData.Image = ((System.Drawing.Image)(resources.GetObject("buttonSentData.Image")));
        //    this.buttonSentData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        //    this.buttonSentData.Location = new System.Drawing.Point(826, 55);
        //    this.buttonSentData.Name = "buttonSentData";
        //    this.buttonSentData.Size = new System.Drawing.Size(242, 43);
        //    this.buttonSentData.TabIndex = 9;
        //    this.buttonSentData.Text = "Αποστολή Παραστατικών";
        //    this.buttonSentData.UseVisualStyleBackColor = true;

        //    MainSearchForm


        //    this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        //    this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
        //    this.ClientSize = new System.Drawing.Size(1700, 720);
        //    this.Controls.Add(this.buttonSentData);
        //    this.Controls.Add(this.textOrderSearch);
        //    this.Controls.Add(this.label4);
        //    this.Controls.Add(this.label3);
        //    this.Controls.Add(this.label2);
        //    this.Controls.Add(this.label1);
        //    this.Controls.Add(this.dateTimeEndDate);
        //    this.Controls.Add(this.dateTimeFromDate);
        //    this.Controls.Add(this.MainToolStrip);
        //    this.Controls.Add(this.gridControl1);
        //    this.Name = "MainSearchForm";
        //    this.Text = "Κιβωτιοποίηση";
        //    this.Load += new System.EventHandler(this.MainSearchForm_Load);
        //    ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
        //    ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
        //    this.MainToolStrip.ResumeLayout(false);
        //    this.MainToolStrip.PerformLayout();
        //    this.ResumeLayout(false);
        //    this.PerformLayout();

        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainSearchForm));
            this.MainToolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSearch = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripExit = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonProperties = new System.Windows.Forms.ToolStripButton();
            this.ost_StatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTimeFromDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimeEndDate = new System.Windows.Forms.DateTimePicker();
            this.textOrderSearch = new System.Windows.Forms.TextBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.buttonSentDoc = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxOrderScamaCategory = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxDrom = new System.Windows.Forms.ComboBox();
            this.dateTimeEndDelivD = new System.Windows.Forms.DateTimePicker();
            this.dateTimeFromDelivD = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ord_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ord_Code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ost_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ost_Code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ord_TypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ord_InputDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ost_ExecuteDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ost_ShipDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.orr_Code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.orr_FullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.msg_greek = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PackingFlag = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cpq_Qty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cpk_GroupID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RoutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RoutDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TranspID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TranspDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cpu_Descr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RefCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RefID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AllFlag = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tglAllOrders = new DevExpress.XtraEditors.ToggleSwitch();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.MainToolStrip.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tglAllOrders.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // MainToolStrip
            // 
            this.MainToolStrip.AllowMerge = false;
            this.MainToolStrip.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.MainToolStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.MainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.toolStripSearch,
            this.toolStripSeparator2,
            this.toolStripClear,
            this.toolStripSeparator3,
            this.toolStripExit,
            this.toolStripSeparator4,
            this.toolStripButtonProperties});
            this.MainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.MainToolStrip.Name = "MainToolStrip";
            this.MainToolStrip.Padding = new System.Windows.Forms.Padding(0, 0, 16, 0);
            this.MainToolStrip.Size = new System.Drawing.Size(1259, 27);
            this.MainToolStrip.TabIndex = 1;
            this.MainToolStrip.Text = "MainToolStrip";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripSearch
            // 
            this.toolStripSearch.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripSearch.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSearch.Image")));
            this.toolStripSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSearch.Name = "toolStripSearch";
            this.toolStripSearch.Size = new System.Drawing.Size(95, 24);
            this.toolStripSearch.Text = "Αναζήτηση";
            this.toolStripSearch.Click += new System.EventHandler(this.ToolStripSearch_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripClear
            // 
            this.toolStripClear.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripClear.Image = ((System.Drawing.Image)(resources.GetObject("toolStripClear.Image")));
            this.toolStripClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripClear.Name = "toolStripClear";
            this.toolStripClear.Size = new System.Drawing.Size(111, 24);
            this.toolStripClear.Text = "Αρχικοποίηση";
            this.toolStripClear.Click += new System.EventHandler(this.ToolStripClear_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripExit
            // 
            this.toolStripExit.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripExit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripExit.Image")));
            this.toolStripExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripExit.Name = "toolStripExit";
            this.toolStripExit.Size = new System.Drawing.Size(72, 24);
            this.toolStripExit.Text = "Έξοδος";
            this.toolStripExit.Click += new System.EventHandler(this.ToolStripExit_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripButtonProperties
            // 
            this.toolStripButtonProperties.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonProperties.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonProperties.Image")));
            this.toolStripButtonProperties.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonProperties.Name = "toolStripButtonProperties";
            this.toolStripButtonProperties.Size = new System.Drawing.Size(24, 24);
            this.toolStripButtonProperties.ToolTipText = "Properties";
            this.toolStripButtonProperties.Click += new System.EventHandler(this.OnPropertiesButtonClick);
            // 
            // ost_StatusID
            // 
            this.ost_StatusID.Caption = "ost_StatusID";
            this.ost_StatusID.FieldName = "ost_StatusID";
            this.ost_StatusID.Name = "ost_StatusID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label1.Location = new System.Drawing.Point(48, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(48, 0, 48, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Ημερομηνία καταχώρησης";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label2.Location = new System.Drawing.Point(48, 57);
            this.label2.Margin = new System.Windows.Forms.Padding(48, 0, 48, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Από :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label3.Location = new System.Drawing.Point(222, 57);
            this.label3.Margin = new System.Windows.Forms.Padding(48, 0, 48, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Εώς :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label4.Location = new System.Drawing.Point(48, 145);
            this.label4.Margin = new System.Windows.Forms.Padding(48, 0, 48, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(157, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "Αναζήτηση Παραγγελιών";
            // 
            // dateTimeFromDate
            // 
            this.dateTimeFromDate.Checked = false;
            this.dateTimeFromDate.CustomFormat = "dd-MM-yyyy";
            this.dateTimeFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeFromDate.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dateTimeFromDate.Location = new System.Drawing.Point(90, 55);
            this.dateTimeFromDate.Margin = new System.Windows.Forms.Padding(48, 22, 48, 22);
            this.dateTimeFromDate.Name = "dateTimeFromDate";
            this.dateTimeFromDate.ShowCheckBox = true;
            this.dateTimeFromDate.Size = new System.Drawing.Size(107, 20);
            this.dateTimeFromDate.TabIndex = 2;
            // 
            // dateTimeEndDate
            // 
            this.dateTimeEndDate.Checked = false;
            this.dateTimeEndDate.CustomFormat = "dd-MM-yyyy";
            this.dateTimeEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeEndDate.Location = new System.Drawing.Point(278, 55);
            this.dateTimeEndDate.Margin = new System.Windows.Forms.Padding(48, 22, 48, 22);
            this.dateTimeEndDate.Name = "dateTimeEndDate";
            this.dateTimeEndDate.ShowCheckBox = true;
            this.dateTimeEndDate.Size = new System.Drawing.Size(107, 20);
            this.dateTimeEndDate.TabIndex = 3;
            // 
            // textOrderSearch
            // 
            this.textOrderSearch.Location = new System.Drawing.Point(225, 141);
            this.textOrderSearch.Margin = new System.Windows.Forms.Padding(48, 22, 48, 22);
            this.textOrderSearch.MaxLength = 67;
            this.textOrderSearch.Name = "textOrderSearch";
            this.textOrderSearch.Size = new System.Drawing.Size(294, 20);
            this.textOrderSearch.TabIndex = 8;
            // 
            // buttonSentDoc
            // 
            this.buttonSentDoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.buttonSentDoc.Location = new System.Drawing.Point(891, 55);
            this.buttonSentDoc.Name = "buttonSentDoc";
            this.buttonSentDoc.Size = new System.Drawing.Size(330, 55);
            this.buttonSentDoc.TabIndex = 10;
            this.buttonSentDoc.Text = "Αποστολή Παραστατικών";
            this.buttonSentDoc.UseVisualStyleBackColor = true;
            this.buttonSentDoc.Click += new System.EventHandler(this.buttonSentDoc_Click);
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.labelControl1);
            this.panel2.Controls.Add(this.tglAllOrders);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.comboBoxOrderScamaCategory);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.comboBoxDrom);
            this.panel2.Controls.Add(this.dateTimeEndDelivD);
            this.panel2.Controls.Add(this.dateTimeFromDelivD);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.dateTimeFromDate);
            this.panel2.Controls.Add(this.buttonSentDoc);
            this.panel2.Controls.Add(this.dateTimeEndDate);
            this.panel2.Controls.Add(this.textOrderSearch);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 27);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1259, 188);
            this.panel2.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label9.Location = new System.Drawing.Point(561, 116);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(151, 16);
            this.label9.TabIndex = 19;
            this.label9.Text = "Κατηγορία Παραγγελίας";
            // 
            // comboBoxOrderScamaCategory
            // 
            this.comboBoxOrderScamaCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOrderScamaCategory.FormattingEnabled = true;
            this.comboBoxOrderScamaCategory.Location = new System.Drawing.Point(561, 140);
            this.comboBoxOrderScamaCategory.Name = "comboBoxOrderScamaCategory";
            this.comboBoxOrderScamaCategory.Size = new System.Drawing.Size(297, 21);
            this.comboBoxOrderScamaCategory.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label8.Location = new System.Drawing.Point(436, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 16);
            this.label8.TabIndex = 17;
            this.label8.Text = "Δρομομολόγιο";
            // 
            // comboBoxDrom
            // 
            this.comboBoxDrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDrom.FormattingEnabled = true;
            this.comboBoxDrom.Location = new System.Drawing.Point(436, 82);
            this.comboBoxDrom.Name = "comboBoxDrom";
            this.comboBoxDrom.Size = new System.Drawing.Size(422, 21);
            this.comboBoxDrom.TabIndex = 16;
            // 
            // dateTimeEndDelivD
            // 
            this.dateTimeEndDelivD.Checked = false;
            this.dateTimeEndDelivD.CustomFormat = "dd-MM-yyyy";
            this.dateTimeEndDelivD.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeEndDelivD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dateTimeEndDelivD.Location = new System.Drawing.Point(278, 111);
            this.dateTimeEndDelivD.Name = "dateTimeEndDelivD";
            this.dateTimeEndDelivD.ShowCheckBox = true;
            this.dateTimeEndDelivD.Size = new System.Drawing.Size(107, 20);
            this.dateTimeEndDelivD.TabIndex = 15;
            // 
            // dateTimeFromDelivD
            // 
            this.dateTimeFromDelivD.Checked = false;
            this.dateTimeFromDelivD.CustomFormat = "dd-MM-yyyy";
            this.dateTimeFromDelivD.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeFromDelivD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dateTimeFromDelivD.Location = new System.Drawing.Point(90, 111);
            this.dateTimeFromDelivD.Name = "dateTimeFromDelivD";
            this.dateTimeFromDelivD.ShowCheckBox = true;
            this.dateTimeFromDelivD.Size = new System.Drawing.Size(107, 20);
            this.dateTimeFromDelivD.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label7.Location = new System.Drawing.Point(222, 116);
            this.label7.Margin = new System.Windows.Forms.Padding(48, 0, 48, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 16);
            this.label7.TabIndex = 13;
            this.label7.Text = "Εώς :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label6.Location = new System.Drawing.Point(48, 116);
            this.label6.Margin = new System.Windows.Forms.Padding(48, 0, 48, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "Από :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label5.Location = new System.Drawing.Point(51, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(151, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "Ημερομηνία Παράδοσης";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 215);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1259, 331);
            this.gridControl1.TabIndex = 9;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ord_ID,
            this.ord_Code,
            this.ost_ID,
            this.ost_Code,
            this.ord_TypeID,
            this.ord_InputDate,
            this.ost_ExecuteDate,
            this.ost_ShipDate,
            this.orr_Code,
            this.orr_FullName,
            this.msg_greek,
            this.PackingFlag,
            this.cpq_Qty,
            this.cpk_GroupID,
            this.RoutID,
            this.RoutDesc,
            this.TranspID,
            this.TranspDesc,
            this.cpu_Descr,
            this.RefCode,
            this.RefID,
            this.AllFlag});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView1.OptionsSelection.ShowCheckBoxSelectorInGroupRow = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView1_SelectionChanged);
            // 
            // ord_ID
            // 
            this.ord_ID.Caption = "ID Παραγγελίας";
            this.ord_ID.FieldName = "ord_ID";
            this.ord_ID.Name = "ord_ID";
            // 
            // ord_Code
            // 
            this.ord_Code.Caption = "Κωδικός Παραγγελίας";
            this.ord_Code.FieldName = "ord_Code";
            this.ord_Code.Name = "ord_Code";
            this.ord_Code.Visible = true;
            this.ord_Code.VisibleIndex = 1;
            // 
            // ost_ID
            // 
            this.ost_ID.Caption = "ID Αποστολής";
            this.ost_ID.FieldName = "ost_ID";
            this.ost_ID.Name = "ost_ID";
            // 
            // ost_Code
            // 
            this.ost_Code.Caption = "Αποστολή Παραγγελίας";
            this.ost_Code.FieldName = "ost_Code";
            this.ost_Code.Name = "ost_Code";
            this.ost_Code.Visible = true;
            this.ost_Code.VisibleIndex = 2;
            // 
            // ord_TypeID
            // 
            this.ord_TypeID.Caption = "ID Τύπου Παραγγελίας";
            this.ord_TypeID.FieldName = "ord_TypeID";
            this.ord_TypeID.Name = "ord_TypeID";
            // 
            // ord_InputDate
            // 
            this.ord_InputDate.Caption = "Ημ. Παραγγελίας";
            this.ord_InputDate.FieldName = "ord_InputDate";
            this.ord_InputDate.Name = "ord_InputDate";
            this.ord_InputDate.Visible = true;
            this.ord_InputDate.VisibleIndex = 3;
            // 
            // ost_ExecuteDate
            // 
            this.ost_ExecuteDate.Caption = "Ημ. Εκτέλεσης";
            this.ost_ExecuteDate.FieldName = "ost_ExecuteDate";
            this.ost_ExecuteDate.Name = "ost_ExecuteDate";
            this.ost_ExecuteDate.Visible = true;
            this.ost_ExecuteDate.VisibleIndex = 4;
            // 
            // ost_ShipDate
            // 
            this.ost_ShipDate.Caption = "Ημ. Αποστολής";
            this.ost_ShipDate.FieldName = "ost_ShipDate";
            this.ost_ShipDate.Name = "ost_ShipDate";
            this.ost_ShipDate.Visible = true;
            this.ost_ShipDate.VisibleIndex = 5;
            // 
            // orr_Code
            // 
            this.orr_Code.Caption = "Κωδ. Παραλήπτη";
            this.orr_Code.FieldName = "orr_Code";
            this.orr_Code.Name = "orr_Code";
            this.orr_Code.Visible = true;
            this.orr_Code.VisibleIndex = 6;
            // 
            // orr_FullName
            // 
            this.orr_FullName.Caption = "Επωνυμία Παραλήπτη";
            this.orr_FullName.FieldName = "orr_FullName";
            this.orr_FullName.Name = "orr_FullName";
            this.orr_FullName.Visible = true;
            this.orr_FullName.VisibleIndex = 7;
            // 
            // msg_greek
            // 
            this.msg_greek.Caption = "Κατάσταση";
            this.msg_greek.FieldName = "msg_greek";
            this.msg_greek.Name = "msg_greek";
            this.msg_greek.Visible = true;
            this.msg_greek.VisibleIndex = 8;
            // 
            // PackingFlag
            // 
            this.PackingFlag.Name = "PackingFlag";
            // 
            // cpq_Qty
            // 
            this.cpq_Qty.Caption = "Ποσότητα Κιβωτιοποίησης";
            this.cpq_Qty.FieldName = "cpq_Qty";
            this.cpq_Qty.Name = "cpq_Qty";
            this.cpq_Qty.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.cpq_Qty.Visible = true;
            this.cpq_Qty.VisibleIndex = 13;
            // 
            // cpk_GroupID
            // 
            this.cpk_GroupID.Caption = "Group Παραγγελιών";
            this.cpk_GroupID.FieldName = "cpk_GroupID";
            this.cpk_GroupID.Name = "cpk_GroupID";
            this.cpk_GroupID.Visible = true;
            this.cpk_GroupID.VisibleIndex = 11;
            // 
            // RoutID
            // 
            this.RoutID.Caption = "RoutID";
            this.RoutID.FieldName = "RoutID";
            this.RoutID.Name = "RoutID";
            // 
            // RoutDesc
            // 
            this.RoutDesc.Caption = "Δρομολόγιο";
            this.RoutDesc.FieldName = "RoutDesc";
            this.RoutDesc.Name = "RoutDesc";
            this.RoutDesc.Visible = true;
            this.RoutDesc.VisibleIndex = 9;
            // 
            // TranspID
            // 
            this.TranspID.Caption = "TranspID";
            this.TranspID.FieldName = "TranspID";
            this.TranspID.Name = "TranspID";
            // 
            // TranspDesc
            // 
            this.TranspDesc.Caption = "Μεταφορέας";
            this.TranspDesc.FieldName = "TranspDesc";
            this.TranspDesc.Name = "TranspDesc";
            this.TranspDesc.Visible = true;
            this.TranspDesc.VisibleIndex = 10;
            // 
            // cpu_Descr
            // 
            this.cpu_Descr.Caption = "Μονάδα Κιβωτιοποίησης";
            this.cpu_Descr.FieldName = "cpu_Descr";
            this.cpu_Descr.Name = "cpu_Descr";
            this.cpu_Descr.Visible = true;
            this.cpu_Descr.VisibleIndex = 12;
            this.cpu_Descr.Width = 60;
            // 
            // RefCode
            // 
            this.RefCode.Caption = "RefCode";
            this.RefCode.FieldName = "RefCode";
            this.RefCode.Name = "RefCode";
            this.RefCode.Visible = true;
            this.RefCode.VisibleIndex = 15;
            // 
            // RefID
            // 
            this.RefID.Caption = "RefID";
            this.RefID.FieldName = "RefID";
            this.RefID.Name = "RefID";
            this.RefID.Visible = true;
            this.RefID.VisibleIndex = 16;
            // 
            // AllFlag
            // 
            this.AllFlag.Caption = "Μαζική Εκτέλεση";
            this.AllFlag.FieldName = "AllFlag";
            this.AllFlag.Name = "AllFlag";
            this.AllFlag.Visible = true;
            this.AllFlag.VisibleIndex = 14;
            // 
            // tglAllOrders
            // 
            this.tglAllOrders.Location = new System.Drawing.Point(891, 141);
            this.tglAllOrders.Name = "tglAllOrders";
            this.tglAllOrders.Properties.OffText = "Off";
            this.tglAllOrders.Properties.OnText = "On";
            this.tglAllOrders.Size = new System.Drawing.Size(95, 18);
            this.tglAllOrders.TabIndex = 20;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(891, 121);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(131, 13);
            this.labelControl1.TabIndex = 21;
            this.labelControl1.Text = "Απεσταλμένες Παραγγελίες";
            // 
            // MainSearchForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1259, 546);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.MainToolStrip);
            this.Margin = new System.Windows.Forms.Padding(48, 22, 48, 22);
            this.Name = "MainSearchForm";
            this.Text = "Κιβωτιοποίηση";
            this.Load += new System.EventHandler(this.MainSearchForm_Load);
            this.MainToolStrip.ResumeLayout(false);
            this.MainToolStrip.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tglAllOrders.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip MainToolStrip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripSearch;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripClear;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripExit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.DateTimePicker dateTimeFromDate;
        private System.Windows.Forms.DateTimePicker dateTimeEndDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraGrid.Columns.GridColumn ost_StatusID;
        private DevExpress.XtraGrid.Columns.GridColumn TranspDescr;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        public System.Windows.Forms.TextBox textOrderSearch;
        private System.Windows.Forms.Button buttonSentDoc;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripButton toolStripButtonProperties;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxDrom;
        private System.Windows.Forms.DateTimePicker dateTimeEndDelivD;
        private System.Windows.Forms.DateTimePicker dateTimeFromDelivD;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn ord_ID;
        private DevExpress.XtraGrid.Columns.GridColumn ord_Code;
        private DevExpress.XtraGrid.Columns.GridColumn ost_ID;
        private DevExpress.XtraGrid.Columns.GridColumn ost_Code;
        private DevExpress.XtraGrid.Columns.GridColumn ord_TypeID;
        private DevExpress.XtraGrid.Columns.GridColumn ord_InputDate;
        private DevExpress.XtraGrid.Columns.GridColumn ost_ExecuteDate;
        private DevExpress.XtraGrid.Columns.GridColumn ost_ShipDate;
        private DevExpress.XtraGrid.Columns.GridColumn orr_Code;
        private DevExpress.XtraGrid.Columns.GridColumn orr_FullName;
        private DevExpress.XtraGrid.Columns.GridColumn msg_greek;
        private DevExpress.XtraGrid.Columns.GridColumn PackingFlag;
        private DevExpress.XtraGrid.Columns.GridColumn cpq_Qty;
        private DevExpress.XtraGrid.Columns.GridColumn cpk_GroupID;
        private DevExpress.XtraGrid.Columns.GridColumn RoutID;
        private DevExpress.XtraGrid.Columns.GridColumn RoutDesc;
        private DevExpress.XtraGrid.Columns.GridColumn TranspID;
        private DevExpress.XtraGrid.Columns.GridColumn TranspDesc;
        private DevExpress.XtraGrid.Columns.GridColumn cpu_Descr;
        private DevExpress.XtraGrid.Columns.GridColumn RefCode;
        private DevExpress.XtraGrid.Columns.GridColumn RefID;
        private DevExpress.XtraGrid.Columns.GridColumn AllFlag;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxOrderScamaCategory;
        private LabelControl labelControl1;
        private ToggleSwitch tglAllOrders;
    }
}