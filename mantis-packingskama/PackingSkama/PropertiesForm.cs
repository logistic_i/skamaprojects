﻿using Mantis.LVision.LVForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PackingSkama
{
    public partial class PropertiesForm :LVSearchForm// Form
    {
        public PropertiesForm()
        {
            InitializeComponent();
        }

        private void PropertiesForm_Load(object sender, EventArgs e)
        {
            try
            {
                label3.Text = Environment.MachineName;
                label2.Text = GetData.GetMacAddress(); //macadress υπολογιστη
                comboBox1.DataSource = GetData.GetPrinterLists(); //γεμιζω την λιστα με τους εκτυπωτες
                FillPrinterCombobox();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }        
        }

        public void FillPrinterCombobox()
        {
            var PrinterName = GetData.GetPrinter();       
                if(PrinterName == null)
                {
                    comboBox1.SelectedItem = null;
                }
                else
                {
                    comboBox1.SelectedItem = PrinterName;
                }
        }

        private void OnPropertiesButtonClick(object sender, EventArgs e)
        {
            string selectedvalue;
            if (comboBox1.SelectedIndex > 0)
            {
              selectedvalue = comboBox1.SelectedValue.ToString();
            }
            else
            {
                selectedvalue = null;
            }
                var Constring = GetData.GetConDB();
                using (var con = new SqlConnection(Constring))//ελεγχος εγκυροτητας
                {
                    con.Open();
                    SqlCommand merge = new SqlCommand($"with aa as "+
                   $"(select '{label2.Text.ToString()}' as MACADDRESS, case when '{selectedvalue}'='' then null else '{selectedvalue}' end AS PRINTERNAME) " +
                    $"MERGE CCCPackingPrinters  AS TARGET "+
                    $"USING aa AS SOURCE "+
                    $"ON(TARGET.cpp_macadress = SOURCE.MACADDRESS) "+
                    $"WHEN MATCHED AND TARGET.cpp_macadress = SOURCE.MACADDRESS "+
                     $"THEN UPDATE SET TARGET.cpp_printername = SOURCE.PRINTERNAME "+
                    $"WHEN NOT MATCHED BY TARGET "+
                    $"THEN INSERT(CPP_MACADRESS, CPP_PRINTERNAME) VALUES(SOURCE.MACADDRESS, SOURCE.PRINTERNAME); ", con);
                    merge.ExecuteNonQuery();
                    MessageBox.Show("H Ενημέρωση Ολοκληρώθηκε!");
                }
            this.Close();
        }
    }
}
