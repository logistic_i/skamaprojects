﻿using Mantis.LVision.LVForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PackingSkama
{
    public partial class PackingChangeQtyForm :   LVSearchForm
    {
        public List<DataForInsert> items { get; set; }
        public PackingChangeQtyForm(List<DataForInsert> checkeditems)
        {
            InitializeComponent();
            items = checkeditems;
            buttonChangePackingqty.Click += buttonChangePackingqty_OnClick;
        }

        private void buttonChangePackingqty_OnClick(object sender, EventArgs e)
        {
            UpdateQty();
        }

        void UpdateQty ()          
        {
            try
            {
                if (!string.IsNullOrEmpty(textBoxChangeQty.Text))
                {
                    if (textBoxChangeQty.Text != "0")
                    {
                        double chars;
                        var hasChars = double.TryParse(textBoxChangeQty.Text.Replace(',', '.'), out chars);
                        if (hasChars == true && !textBoxChangeQty.Text.Contains("."))//ελεγχος εγκυροτητας
                        {
                            var Constring = GetData.GetConDB();
                            using (var con = new SqlConnection(Constring))
                            {
                                con.Open();
                                var groupid = items.GroupBy(x => x.GroupID).ToList().First().Key;
                                float num = float.Parse(textBoxChangeQty.Text);
                                var quantity = num.ToString().Replace(',', '.');
                                SqlCommand maindata = new SqlCommand($"update cccpackingqty set cpq_Qty = {quantity}"+
                                    $" where cpq_GroupID = {groupid} ", con);
                                maindata.ExecuteNonQuery();                               
                                this.Close();
                                MessageBox.Show("Η Ποσότητα έχει Αλλαχθεί!");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Το Κείμενο Δεν Περιέχει Αριθμούς!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Δεν Επιτρέπεται Μηδενική Ποσότητα1");
                    }
                }
                else
                {
                    MessageBox.Show("Μην Επιτρετό Κείμενο!");
                }               
            }
            catch (Exception)
            {
                throw;
            }         
        }     

    }
}
