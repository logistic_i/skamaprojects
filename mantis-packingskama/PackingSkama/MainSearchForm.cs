﻿using DevExpress.Utils.Menu;
using DevExpress.XtraEditors.Filtering;
using DevExpress.XtraGrid.Views.Grid;
using Mantis.LVision.Interfaces;
using Mantis.LVision.LVForms;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Forms;


namespace PackingSkama
{
   public partial class MainSearchForm : LVSearchForm

  //public partial class MainSearchForm : Form

    {
        public int LogisticSS { get; set; }
        public int User { get; set; }       
        public static string ConnStr { get; set; }

        public List<KeyValuePair<int, bool>> ListKeyValue { get; set; }

        public MainSearchForm()
        {
            InitializeComponent();         
        }

        public override bool LV_OnInit()
        {//τιμες login Mantis
            LogisticSS = this.LogisticSiteID;
            User = this.UserID;
            GetData.conStrng= this.ConnectionString;
            FillDromList();
            FillOrderScamaCategory();
            tglAllOrders.IsOn = false;
            MessageBox.Show("Τα custom εργαλεία και η ανάπτυξη κώδικα με εργαλεία τρίτων αποτελεί πνευματικά δικαιώματα της Logistic-i.", "Copyright © LOGISTIC-i 2021"
                     , MessageBoxButtons.OK, MessageBoxIcon.Information);

            return true;
        }



        private List<ScamaCategory> FillOrderScamaCategory()
        {
            List<ScamaCategory> ListOfCategory = new List<ScamaCategory>();
            var Constring = GetData.GetConDB();
            using (var con = new SqlConnection(Constring))
            {

                con.Open();
                SqlCommand MainResults = con.CreateCommand();
                MainResults.CommandText = GetData.OrderScamaCategoryQuery();
                SqlDataAdapter da = new SqlDataAdapter(MainResults);
                DataTable dtOrdercategoryResults = new DataTable();
                da.Fill(dtOrdercategoryResults);
                ScamaCategory empty = new ScamaCategory();
                empty.categoryCode = "0";
                empty.categoryDescr = "";
                ListOfCategory.Add(empty);

                for (int i = 0; i < dtOrdercategoryResults.Rows.Count; i++)
                {
                    ScamaCategory item = new ScamaCategory();
                    item.categoryCode = dtOrdercategoryResults.Rows[i][0].ToString();
                    item.categoryDescr = dtOrdercategoryResults.Rows[i][1].ToString();
                    ListOfCategory.Add(item);
                }
            }
          
            comboBoxOrderScamaCategory.DataSource = ListOfCategory;
            comboBoxOrderScamaCategory.DisplayMember = "categoryDescr";
            comboBoxOrderScamaCategory.ValueMember = "categoryCode";
            comboBoxOrderScamaCategory.SelectedItem = null;
            return ListOfCategory;
        }

        public class ScamaCategory
        {
            public string categoryCode { get; set; }
            public string categoryDescr { get; set; }
        }

        public List<Drom> FillDromList()
        {
            List<Drom> ListOfDroms = new List<Drom>();
            var Constring = GetData.GetConDB();
            using (var con = new SqlConnection(Constring))
            {
                
                con.Open();
                SqlCommand MainResults = con.CreateCommand();
                MainResults.CommandText = GetData.DromQuery(); 
                SqlDataAdapter da = new SqlDataAdapter(MainResults);
                DataTable dtDromResults = new DataTable();
                da.Fill(dtDromResults);
                Drom empty = new Drom();
                empty.DromCode = "0";
                empty.DromDescr = "";
                ListOfDroms.Add(empty);
                
                for (int i = 0; i < dtDromResults.Rows.Count; i++)
                {
                    Drom item = new Drom();
                    item.DromCode = dtDromResults.Rows[i][0].ToString();
                    item.DromDescr = dtDromResults.Rows[i][1].ToString();
                    ListOfDroms.Add(item);                               
                }
            }
            comboBoxDrom.DataSource = ListOfDroms;
            comboBoxDrom.DisplayMember = "DromDescr";
            comboBoxDrom.ValueMember = "DromCode";
            comboBoxDrom.SelectedItem = null;
            return ListOfDroms;           
        }

        public class Drom
        {
            public string DromCode { get; set; }
            public string DromDescr { get; set; } 
        }

        public void ToolStripSearch_Click(object sender, EventArgs e)
        {//κουμπι αναζητησης
            SearchData();
        }

        private void ToolStripClear_Click(object sender, EventArgs e)
        {//κουμπι αρχικοποιησης
            dateTimeFromDate.Value = DateTime.Today;
            dateTimeFromDate.Checked = false;
            dateTimeEndDate.Value = DateTime.Today;
            dateTimeEndDate.Checked = false;
            dateTimeFromDelivD.Value = DateTime.Today;
            dateTimeFromDelivD.Checked = false;
            dateTimeEndDelivD.Value = DateTime.Today;
            dateTimeEndDelivD.Checked = false;
            comboBoxDrom.SelectedValue = 0;
            comboBoxOrderScamaCategory.SelectedValue = 0;
            tglAllOrders.IsOn = false;
            textOrderSearch.Clear();
        }

        private void ToolStripExit_Click(object sender, EventArgs e)
        {//κουμπι εξοδου
            this.Close();
        }

        public void SearchData()
        {//αποτελεσματα αναζητησης
            try
            {
                DataTable dtMainResults = new DataTable();
                var Constring = GetData.GetConDB();//connection string
                var NewDrom = "";
                var OrderScamaCategory = "";
                //var ssf = Convert.ToInt32(comboBoxDrom.SelectedValue);
                if (comboBoxDrom.SelectedValue== null || Convert.ToInt32(comboBoxDrom.SelectedValue) == 0)
                {
                    NewDrom = "";
                }
                else
                {
                    NewDrom = comboBoxDrom.SelectedValue.ToString();
                }


                if (comboBoxOrderScamaCategory.SelectedValue == null || Convert.ToInt32(comboBoxOrderScamaCategory.SelectedValue) == 0)
                {
                    OrderScamaCategory = "";
                }
                else
                {
                    OrderScamaCategory = comboBoxOrderScamaCategory.SelectedValue.ToString();
                }

                //query
                var Query = GetData.MainSearchResults(dateTimeFromDate.Text, dateTimeFromDate.Checked, dateTimeEndDate.Text, dateTimeEndDate.Checked, textOrderSearch.Text, LogisticSS
                                , NewDrom,dateTimeFromDelivD.Text,dateTimeFromDelivD.Checked,dateTimeEndDelivD.Text,dateTimeEndDelivD.Checked,OrderScamaCategory,tglAllOrders.IsOn);
                using (var con = new SqlConnection(Constring))
                {
                    con.Open();
                    SqlCommand MainResults = con.CreateCommand();
                    MainResults.CommandText = Query;
                    SqlDataAdapter da = new SqlDataAdapter(MainResults);
                    da.Fill(dtMainResults);
                    gridControl1.DataSource = dtMainResults;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MainSearchForm_Load(object sender, EventArgs e)
        {

        }

        private void GridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {//εισαγωγή στο δεξι click τις επιλογες
            GridView view = sender as GridView;
            if (e.MenuType == GridMenuType.Row && view.SelectedRowsCount > 0)
            {
                int rowHandle = e.HitInfo.RowHandle;
                e.Menu.Items.Clear();

                if (tglAllOrders.IsOn == false)
                {
                    DXMenuItem pack = CreatePackItem(view, rowHandle);//Κιβωτιοποίηση
                    e.Menu.Items.Add(pack);

                    DXMenuItem cancel = CreateCancelItem(view, rowHandle);//Αλλαγή Ποσότητας Κιβωτιοποίησης
                    e.Menu.Items.Add(cancel);

                    DXMenuItem PrintLabel = PrintItem(view, rowHandle);//Εκτύπωση Ετικετών 
                    e.Menu.Items.Add(PrintLabel);

                    DXMenuItem delete = CreateDeleteItem(view, rowHandle);//Διαγραφή Παραγγελίας
                    e.Menu.Items.Add(delete);
                }

                if (tglAllOrders.IsOn == true)
                {
                    DXMenuItem PrintLabel = PrintItem(view, rowHandle);//Εκτύπωση Ετικετών 
                    e.Menu.Items.Add(PrintLabel);
                }
            }
        }

        private DXMenuItem PrintItem(GridView view, int rowHandle)
        {
            DXMenuItem Print = new DXMenuItem("Εκτύπωση Ετικετών", new EventHandler(OnMenuItemPrintclick));
            return Print;
        }

        private void OnMenuItemPrintclick(object sender, EventArgs e)
        {//επιλογή εκτύπωση ετικετών
            try
            {
                InsertDataForSoftonePrint();
                PrintData();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

       public  void PrintData()
        {//εκτυπωση ετικετων
            try

            {
                var ValidPrinter = GetValidPrinter();
                if (ValidPrinter == null)
                {
                    MessageBox.Show("Δεν Έχετε Ορίσει Εκτυπωτή Για Ετικέτες!");
                }
                else
                {                   
                    var checkedrows = GetIdsForPrint();
                    var AgrsID = string.Join<int>(";", checkedrows);
                    var MantisPath = Path.GetDirectoryName(Application.ExecutablePath);
                    var fileName = $"{MantisPath}\\MantisPrint\\MantisPrint.exe";
                    var args = $"{AgrsID} \"{ValidPrinter}\" 999 0 0";
                    var PrintedID = string.Join<int>(",", checkedrows);


                    //εκτέλεση του MantisPrint.exe
                    this.SuspendLayout();
                    Process proc = new Process();
                    proc.StartInfo.FileName = fileName;
                    proc.StartInfo.Arguments = args;
                    proc.Start();
                    this.ResumeLayout();

                    GetData.UpdatePrintedID(PrintedID);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);            
            }                 
        }

        public void PrintDataSend(int GroupId)
        {//εκτυπωση ετικετων
            try

            {
                var ValidPrinter = GetValidPrinter();
                if (ValidPrinter == null)
                {
                    MessageBox.Show("Δεν Έχετε Ορίσει Εκτυπωτή Για Ετικέτες!");
                }
                else
                {
                    var checkedrows = GetIdsForPrintSend(GroupId);
                    var AgrsID = string.Join<int>(";", checkedrows);
                    var MantisPath = Path.GetDirectoryName(Application.ExecutablePath);
                    var fileName = $"{MantisPath}\\MantisPrint\\MantisPrint.exe";
                    var args = $"{AgrsID} \"{ValidPrinter}\" 999 0";
                    var PrintedID = string.Join<int>(",", checkedrows);


                    //εκτέλεση του MantisPrint.exe
                    this.SuspendLayout();
                    Process proc = new Process();
                    proc.StartInfo.FileName = fileName;
                    proc.StartInfo.Arguments = args;
                    proc.Start();
                    this.ResumeLayout();

                    GetData.UpdatePrintedID(PrintedID);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private static string GetValidPrinter()
        {// εάν έχει επιλεγμένο εκτυπωτή ή όχι
            var PrinterName = GetData.GetPrinter();
            return PrinterName;
        }

        private void InsertDataForSoftonePrint()
        {//εισαγωγή δεδομένων group στον πίνακα CCCPackingSoftonePrintTable
            var checkedrows = GetCheckedItemsForPrint();

            var Constring = GetData.GetConDB();
            using (var con = new SqlConnection(Constring))
            {
                con.Open();
                for (int i = 0; i < checkedrows.Count; i++)
                {
                    SqlCommand merge = new SqlCommand($"exec dbo.mp_CCCPackingInsertPrintTable @GroupID = {checkedrows[i]},@LosID={LogisticSS} ", con);
                    merge.ExecuteNonQuery();

                }           
            }           
        }

        public List<int> GetIdsForPrint()
        {//εισαγωγη σε λιστα τα cpspIDs που θα στείλω για εκτύπωση
            List<int> IdsList = new List<int>();
            var groupids = GetCheckedItemsForPrint();
            var AgrsID = string.Join<int>(",", groupids);
            var Constring = GetData.GetConDB();
            var Query = $"select cpsp_ID from CCCPackingSoftonePrintTable where cpsp_GroupID in ({AgrsID}) and isnull(cpsp_PrintedLED,0)=0";
            using (var con = new SqlConnection(Constring))
            {
                con.Open();
                SqlCommand MainResults = con.CreateCommand();
                MainResults.CommandText = Query;
                SqlDataAdapter da = new SqlDataAdapter(MainResults);
                DataTable dtMainResults = new DataTable();
                da.Fill(dtMainResults);

                for (int i = 0; i < dtMainResults.Rows.Count; i++)
                {
                    IdsList.Add(Convert.ToInt32(dtMainResults.Rows[i][0]));
                }
            }
            return IdsList;
        }
        public List<int> GetIdsForPrintSend(int GroupID )
        {//εισαγωγη σε λιστα τα cpspIDs που θα στείλω για εκτύπωση
            List<int> IdsList = new List<int>();
            //var groupids =// GetCheckedItemsForPrint();
            //var AgrsID = string.Join<int>(",", groupids);
            var Constring = GetData.GetConDB();
            var Query = $"select cpsp_ID from CCCPackingSoftonePrintTable where cpsp_GroupID in ({GroupID}) and isnull(cpsp_PrintedLED,0)=0";
            using (var con = new SqlConnection(Constring))
            {
                con.Open();
                SqlCommand MainResults = con.CreateCommand();
                MainResults.CommandText = Query;
                SqlDataAdapter da = new SqlDataAdapter(MainResults);
                DataTable dtMainResults = new DataTable();
                da.Fill(dtMainResults);

                for (int i = 0; i < dtMainResults.Rows.Count; i++)
                {
                    IdsList.Add(Convert.ToInt32(dtMainResults.Rows[i][0]));
                }
            }
            return IdsList;
        }

        public List<int> GetCheckedItemsForPrint()
        {//παιρνεις τα selecteditems

            var checkeditems = GetCheckedData();
            List<int> ClickedRows = new List<int>();
            if (!checkeditems.Any())
            {
                return new List<int>();
            }
            foreach (var itm in checkeditems)
            {
                var xx = itm.GroupID;
                if (xx == -1)
                {
                    throw new System.ArgumentException("Έχετε Επιλέξει Παραγγελία Που Δεν Έχει Κιβωτιοποιηθεί!");
                    //MessageBox.Show("Έχετε Επιλέξει Παραγγελία Που Δεν Έχει Κιβωτιοποιηθεί!");            
                }
                else
                {
                    ClickedRows.Add(itm.GroupID);
                }
            }
            List<int> RowsList = ClickedRows.GroupBy(x => x)
                           .Select(grp => grp.Key)
                           .ToList();
            return RowsList;
        }



        private DXMenuItem CreateDeleteItem(GridView view, int rowHandle)
        {
            DXMenuItem delete = new DXMenuItem("Διαγραφή Παραγελλίας", new EventHandler(OnMenuItemDeleteclick));
            return delete;
        }

        private void OnMenuItemDeleteclick(object sender, EventArgs e)
        {//διαγραφή Παραγγελιών
            DeleteOrders();//διαγραφή
            GetExpandedGroups();
            SearchData();//ανανεωση datasource
            PreserveGroupExpanded();
        }

        private void DeleteOrders()
        {
            var item = GetCheckedData();
            var groups = item.GroupBy(y => y.GroupID).ToList();
            var grouparnitiko = groups.Where(x => x.Key < 0).ToList();
            if (grouparnitiko.Count == 0)//έλεγχος εάν υπάρχει παραγγελια που δεν εχει κιβωτιοποιηθεί
            {
                var grouthetiko = groups.Where(x => x.Key > 0).ToList();
                if (grouthetiko.Count == 1)//έλγχος εάν ανοίκουν στο ιδιο cpk_groupID οι παραγγελίες
                {
                    var groupid = groups.GroupBy(x => x.Key).ToList().First().Key;
                    DeleteAllorNot(groupid);//έλεγος και διαγραφή παραγγελίων
                    MessageBox.Show("Η Διαγραφή Έχει Ολοκληρωθεί!");
                }
                else
                {
                    MessageBox.Show("Έχετε Επιλέξει Διαφορετικές Παραγγελίες");
                }
            }
            else
            {
                MessageBox.Show("Έχετε Επιλέξει Παραγγελία Που Δεν Έχει Κιβωτιοποίηθεί");
            }
        }

        private void DeleteAllorNot(int groupid)
        {
            var countitems = GetCheckedData().Count;//count τα items
            var countdatabaseItems = GetDataBaseitems(groupid);//count απο την βάση για το συγκεκριμένο Group
            if (countitems == countdatabaseItems)//εάν είναι ίσα σημαίνει ότι διαγράφει ολόκληρο το Group παραγγελιών
            {
                DeleteallRecords(GetCheckedData());
            }
            else//διαγράφη τμήμα παραγγελιών από το Group
            {
                DeleteRecord(GetCheckedData());
                // MessageBox.Show("Διαγραφή μόνο αυτής");
            }
        }

        private void DeleteRecord(List<DataForInsert> items)
        {//διαγραφή παραγγελιών από το group
            try
            {
                var Constring = GetData.GetConDB();
                using (var con = new SqlConnection(Constring))
                {
                    con.Open();
                    for (int i = 0; i < items.Count; i++)
                    {//διαγραφή μόνο από τον cccpacking
                        SqlCommand delpacking = new SqlCommand($"DELETE FROM CCCPACKING WHERE " +
                                    $"cpk_ShipmentID = {items[i].OstID}", con);
                        delpacking.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
         
        private void DeleteallRecords(List<DataForInsert> items)
        {//διαγραφή ολόκληρου group
            try
            {
                var Constring = GetData.GetConDB();
                using (var con = new SqlConnection(Constring))
                {
                    con.Open();
                    for (int i = 0; i < items.Count; i++)
                    {//διαγραφη από τον πίνακα cccpacking
                        SqlCommand delpacking = new SqlCommand($"DELETE FROM CCCPACKING WHERE " +
                                    $"cpk_ShipmentID = {items[i].OstID}", con);
                        delpacking.ExecuteNonQuery();
                    }
                    //διαγραφή άπό τον πίνακα cccpackingqty
                    var groups = items.GroupBy(y => y.GroupID).ToList().First().Key;
                    SqlCommand delpackingqty = new SqlCommand($"DELETE FROM CCCPACKINGQTY WHERE " +
                               $"cpq_GroupID= {groups}", con);
                    delpackingqty.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private int GetDataBaseitems(int groupid)
        {//items count από την βάση
            var ids = 0;

            try
            {
                var query = GetData.GetDataItems(groupid);
                var Constring = GetData.GetConDB();
                using (var con = new SqlConnection(Constring))
                {
                    con.Open();
                    SqlCommand maxgroupid = new SqlCommand(query, con);
                    var maxid = Convert.ToInt32(maxgroupid.ExecuteScalar());
                    ids = maxid;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return ids;
        }

        private DXMenuItem CreateCancelItem(GridView view, int rowHandle)
        {
            DXMenuItem cancel = new DXMenuItem("Αλλαγή Ποσότητας Κιβωτιοποίησης", new EventHandler(OnMenuItemCancelclick));
            return cancel;
        }

        private DXMenuItem CreatePackItem(GridView view, int rowHandle)
        {
            DXMenuItem packing = new DXMenuItem("Κιβωτιοποίηση", new EventHandler(OnMenuItemPackingclick));
            return packing;
        }

        private void OnMenuItemPackingclick(object sender, EventArgs e)
        {//κιβωτιοποίηση
            var existsItems = ItemsCheckPack();

            if (existsItems == false)
            {
                var checkedData = GetCheckedData();
                var mdetailform = new PackingQTYForm(checkedData).ShowDialog();//ανοιγμα φορμας PackingQtyform
                GetExpandedGroups();
                SearchData();
                gridView1.ClearSelection();
                PreserveGroupExpanded();
            }
        }

        private void PreserveGroupExpanded()
        {
            var SS = ListKeyValue;
            if (ListKeyValue != null && ListKeyValue.Count > 0)
            {
                var maxId = ListKeyValue.Min(x => x.Key);
                for (int i = -1; i >= maxId; i--)
                {
                    var isexpanded = ListKeyValue.Where(x => x.Key == i).FirstOrDefault().Value;

                    if (isexpanded)
                        gridView1.SetRowExpanded(i, isexpanded);
                    else
                        gridView1.SetRowExpanded(i, isexpanded);
                }
            }
        }

        private void GetExpandedGroups()
        {
            var expanded = new List<KeyValuePair<int, bool>>();
            for (int i = 0; i < gridView1.DataRowCount; i++)
            {
                var parentRow = gridView1.GetParentRowHandle(i);
                var rowExpanded = gridView1.GetRowExpanded(i);
                var isexpanded = new KeyValuePair<int, bool>(parentRow, rowExpanded);
                if (parentRow != Int32.MinValue)
                    expanded.Add(isexpanded);

                while (parentRow != Int32.MinValue) //root of the groups
                {
                    parentRow = gridView1.GetParentRowHandle(parentRow);
                    var groupExpanded = gridView1.GetRowExpanded(parentRow);
                    isexpanded = new KeyValuePair<int, bool>(parentRow, groupExpanded);
                    if (parentRow != Int32.MinValue)
                        expanded.Add(isexpanded);
                };
            }
            ListKeyValue = expanded.Distinct().ToList();
        }


        private void CheckChangeRows()
        {//ελέγχος στα checked items εάν είναι στο ιδιο groupid ή δεν έχει κιβωτιοποιηθεί καν
            var item = GetCheckedData();
            var groups = item.GroupBy(y => y.GroupID).ToList();
            var grouparnitiko = groups.Where(x => x.Key < 0).ToList();
            if (grouparnitiko.Count == 0)
            {
                var grouthetiko = groups.Where(x => x.Key > 0).ToList();
                if (grouthetiko.Count == 1)
                {
                    var packingchangeqty = new PackingChangeQtyForm(item).ShowDialog();
                    gridView1.ClearSelection();
                }
                else
                {
                    MessageBox.Show("Έχετε Επιλέξει Διαφορετικές Παραγγελίες");
                }
            }
            else
            {
                MessageBox.Show("Έχετε Επιλέξει Παραγγελία Που Δεν Έχει Κιβωτιοποίηθεί");
            }
        }



        private void gridView1_RowStyle(object sender, RowStyleEventArgs e)
        {//χρωματισμος gridview
            var flag = Convert.ToInt32(gridView1.GetRowCellValue(e.RowHandle, "PackingFlag"));
            var ostled = Convert.ToInt32(gridView1.GetRowCellValue(e.RowHandle, "ost_StatusID"));
            var allflag = Convert.ToInt32(gridView1.GetRowCellValue(e.RowHandle, "AllFlagReady"));

            var count = gridView1.DataRowCount;
            if (allflag==1)
            {
                e.Appearance.BackColor = Color.OrangeRed;
            }

            if (flag == 1)//εάν έχει κιβωτιοποιηθεί
            {
                e.Appearance.BackColor = Color.LightGreen;
            }
            if (ostled == 4 || ostled == 2 || ostled == 3 || ostled == 5 || ostled == 6)//εάν είναι σε κατάσταση η αποστολή συλλέχθηκε και όχι ελέγχθηκε που πρέπει να είναι
            {
                e.Appearance.BackColor = Color.OrangeRed;
            }
            e.HighPriority = true;
        }

        private void GridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {//δεν αφηνει να επιλεξεις παραστατικο οταν η αποστολη ειναι σε κατασταση συλλεχθηκε
            var item = gridView1.GetSelectedRows();
            for (int i = 0; i < item.Length; i++)
            {
                var sss = Convert.ToInt32(gridView1.GetRowCellValue(item[i], "ost_StatusID"));
                if (sss == 4 || sss == 2 || sss == 3 || sss == 5 || sss == 6)
                {
                    gridView1.UnselectRow(item[i]);
                }
            }
        }

        private void OnMenuItemCancelclick(object sende, EventArgs e)
        {//αλλαγή ποσότητας κιβωτιοποίησης
            CheckChangeRows();
            GetExpandedGroups();
            SearchData();
            PreserveGroupExpanded();
        }

        private bool ItemsCheckPack()
        {//ελεγχος και μήνυμα εάν επιλέξει παραγγελία που έχει ήδη κιβωτιοποιηθεί
            var hasItems = false;
            var orders = GetData.OrdersExist(GetCheckedItems());
            if (orders.Rows.Count > 0)
            {
                hasItems = true;
                var message = "Ο Κωδικός Παραγγελίας Έχει Ήδη Κιβωτιοποιηθεί!\n";
                for (int i = 0; i < orders.Rows.Count; i++)
                {
                    var orderId = orders.Rows[i][1].ToString();
                    message += $"{orderId}\n";
                }
                MessageBox.Show(message);
            }
            return hasItems;
        }

        public List<int> GetCheckedItems()
        {//παιρνεις τα selecteditems
            List<int> ClickedRows = new List<int>();
            for (int i = 0; i < gridView1.GetSelectedRows().Length; i++)
            {
                if (gridView1.GetChildRowCount(i) >= 0)
                {
                    ClickedRows.Add(Convert.ToInt32(gridView1.GetDataRow(gridView1.GetSelectedRows()[i]).ItemArray[0].ToString()));
                }
            }
            return ClickedRows;
        }

        public List<DataForInsert> GetCheckedData()
        {//λιστα με items στο object
            var objData = new List<DataForInsert>();
            try
            {
                for (int i = 0; i < gridView1.GetSelectedRows().Length; i++)
                {
                    if (gridView1.GetSelectedRows()[i] >= 0)
                    {
                        objData.Add(new DataForInsert
                        {
                            OrdID = Convert.ToInt32(gridView1.GetDataRow(gridView1.GetSelectedRows()[i]).ItemArray[0].ToString()),
                            OrdCode = gridView1.GetDataRow(gridView1.GetSelectedRows()[i]).ItemArray[1].ToString(),
                            OstID = Convert.ToInt32(gridView1.GetDataRow(gridView1.GetSelectedRows()[i]).ItemArray[2].ToString()),
                            OstCode = gridView1.GetDataRow(gridView1.GetSelectedRows()[i]).ItemArray[3].ToString(),
                            GroupID = Convert.ToInt32(gridView1.GetDataRow(gridView1.GetSelectedRows()[i]).ItemArray[13] == DBNull.Value ? -1 : gridView1.GetDataRow(gridView1.GetSelectedRows()[i]).ItemArray[13])
                        }); ;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return objData;
        }
         
        private void buttonSentDoc_Click(object sender, EventArgs e)
        {//Αποστολή στο Softone και flag *ord_CCCLoadFlag* τις παραγγελίες για φόρτωση
            var countrows = gridView1.DataRowCount;
            if (countrows > 0)
            {
                this.SuspendLayout();
                var listitems = GetOrderForPack();
                if (!listitems.Any())
                {
                    MessageBox.Show("Δεν Έχει Γίνει Σωστή Επιλογή Παραγγελιών!");
                }
                else
                {
                    CheckOrdersWithoutPrint(listitems);
                }           
                GetExpandedGroups();
                SearchData();
                PreserveGroupExpanded();
                this.ResumeLayout();
            }
            else
            {
                MessageBox.Show("Δεν Έχετε Κάνει Αναζήτηση Παραγγελιών!");
            }
        }

        public void CheckOrdersWithoutPrint(List<int> OrderIDS)
        {//
            var ids = string.Join(",", OrderIDS.Select(n => n).ToArray());

            List<(int, int, string)> IdsList = new List<(int , int, string)>();
            var Constring = GetData.GetConDB();
            var Query = $"select cpk_GroupID,cpk_OrderID,cpk_OrderCode from CCCPacking " +
                            $"left join CCCPackingSoftonePrintTable on cpk_GroupID = cpsp_GroupID " +
                            $"where cpk_OrderID in ({ids}) and cpsp_PrintedLED is null";
            var OrderCodes = "";       
            using (var con = new SqlConnection(Constring))
            {
                con.Open();
                SqlCommand MainResults = con.CreateCommand();
                MainResults.CommandText = Query;
                SqlDataAdapter da = new SqlDataAdapter(MainResults);
                DataTable dtMainResults = new DataTable();
                da.Fill(dtMainResults);
                
                for (int i = 0; i < dtMainResults.Rows.Count; i++)
                {
                    IdsList.Add((Convert.ToInt32(dtMainResults.Rows[i][0]),Convert.ToInt32(dtMainResults.Rows[i][1]),dtMainResults.Rows[i][2].ToString()));
                    OrderCodes +=  $"{dtMainResults.Rows[i][2].ToString()}"+ "\r\n";
                }
            }
            
            if (IdsList.Count>0)
            {
                DialogResult dialogResult = MessageBox.Show("Δεν Έχετε Εκτυπώσει Τις Παραγγελίες" + "\r\n" + $"{OrderCodes}"+"\r\n"+"Θέλετε Να Εκτυπωθούν?", "Εκτύπωση Ετικετών", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    //MessageBox.Show("πατήσατε ναι");
                    InsertDataForSoftonePrintSent(IdsList);
                }
                if (dialogResult == DialogResult.No)
                {
                    var AllList = GetOrderForPack();
                    for (int i = 0; i < AllList.Count; i++)
                    {
                        RunProcedureExport(AllList[i]);
                    }

                }
               // MessageBox.Show("Δεν Έχετε Εκτυπώσει Τις Παραγγελίες" + "\r\n" + $"{OrderCodes}");
            }
            else
            {
                var AllOrdersPack = GetOrderForPack();

                for (int i = 0; i < AllOrdersPack.Count; i++)
                {
                    RunProcedureExport(AllOrdersPack[i]);
                }
                
              //  MessageBox.Show("ΤΙΣ ΕΧΕΤΕ ΕΚΤΥΠΩΣΕΙ ΟΛΕΣ");
            }

        }



        private void InsertDataForSoftonePrintSent(List<(int, int , string)> GroupIDs)
        {//εισαγωγή δεδομένων group στον πίνακα CCCPackingSoftonePrintTable
            var checkedrows = GroupIDs;

            var Constring = GetData.GetConDB();
            using (var con = new SqlConnection(Constring))
            {
                con.Open();
                for (int i = 0; i < checkedrows.Count; i++)
                {                 
                    SqlCommand merge = new SqlCommand($"exec dbo.mp_CCCPackingInsertPrintTable @GroupID = {checkedrows[i].Item1}", con);
                    merge.ExecuteNonQuery();
                    PrintDataSend(Convert.ToInt32(checkedrows[i].Item1));
                   // RunProcedureExport(Convert.ToInt32(checkedrows[i].Item2));
                }

                var orderlist = GetOrderForPack();
                for (int i = 0; i < orderlist.Count; i++)
                {

                    RunProcedureExport(Convert.ToInt32(orderlist[i]));
                }
            }
        }

        private void RunProcedureExport(int OrderID)
        {//procedure για την εξαγωγή των παραγγελιων στον ενδιαμεσο πινακα
            try
            {
                var Constring = GetData.GetConDB();
                using (var con = new SqlConnection(Constring))
                {
                    con.Open();
                    SqlCommand maindata = new SqlCommand($"exec dbo.mp_ExportOrders @OrderID = {OrderID}", con);
                    maindata.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

      

        private List<int> GetOrderForPack()
        {//λιστα με τις παραγγελιες απο το datasource που είναι κιβωτιοποιημενες packingflag =1 και ειναι επιλεγμενες
            var orderslist = new List<int>();
            try
            {
                    for (int i = 0; i < gridView1.GetSelectedRows().Length; i++)
                    {
                        if (gridView1.GetChildRowCount(i) >= 0)
                        {
                            var forpick = Convert.ToInt32(gridView1.GetDataRow(gridView1.GetSelectedRows()[i]).ItemArray[12] == DBNull.Value ? 0 : gridView1.GetDataRow(gridView1.GetSelectedRows()[i]).ItemArray[12]);
                            if (forpick == 1)
                            {
                                orderslist.Add(Convert.ToInt32(gridView1.GetDataRow(gridView1.GetSelectedRows()[i]).ItemArray[0] == DBNull.Value ? 0 : gridView1.GetDataRow(gridView1.GetSelectedRows()[i]).ItemArray[0]));
                            }
                        }
                    }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return orderslist;
        }

        private void OnPropertiesButtonClick(object sender, EventArgs e)
        {

            var properiesform = new PropertiesForm().ShowDialog();
        }
    }
}