﻿namespace PackingSkama
{
    partial class PackingQTYForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PackingQTYForm));
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPackingQty = new System.Windows.Forms.TextBox();
            this.buttonPackingOk = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbPackingUnit = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label1.Location = new System.Drawing.Point(58, 87);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(200, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ποσότητα Κιβωτιοποίησης";
            // 
            // textBoxPackingQty
            // 
            this.textBoxPackingQty.Location = new System.Drawing.Point(105, 128);
            this.textBoxPackingQty.Name = "textBoxPackingQty";
            this.textBoxPackingQty.Size = new System.Drawing.Size(100, 22);
            this.textBoxPackingQty.TabIndex = 2;
            // 
            // buttonPackingOk
            // 
            this.buttonPackingOk.Location = new System.Drawing.Point(260, 158);
            this.buttonPackingOk.Name = "buttonPackingOk";
            this.buttonPackingOk.Size = new System.Drawing.Size(86, 23);
            this.buttonPackingOk.TabIndex = 2;
            this.buttonPackingOk.Text = "Ενημέρωση";
            this.buttonPackingOk.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label2.Location = new System.Drawing.Point(58, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(181, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Μονάδα Κιβωτιοποίησης";
            // 
            // cmbPackingUnit
            // 
            this.cmbPackingUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPackingUnit.FormattingEnabled = true;
            this.cmbPackingUnit.Location = new System.Drawing.Point(62, 42);
            this.cmbPackingUnit.Name = "cmbPackingUnit";
            this.cmbPackingUnit.Size = new System.Drawing.Size(196, 24);
            this.cmbPackingUnit.TabIndex = 1;
            // 
            // PackingQTYForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 193);
            this.Controls.Add(this.cmbPackingUnit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonPackingOk);
            this.Controls.Add(this.textBoxPackingQty);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "PackingQTYForm";
            this.Text = "Ποσότητα Κιβωτιοποίησης";
            this.Load += new System.EventHandler(this.PackingQTYForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPackingQty;
        private System.Windows.Forms.Button buttonPackingOk;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbPackingUnit;
    }
}