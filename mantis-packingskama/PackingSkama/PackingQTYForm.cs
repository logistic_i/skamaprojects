﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.CodeParser;
using DevExpress.XtraEditors.Controls;
using Mantis.LVision.LVForms;
using Mantis.LVision.LVForms.BingMapsSearchAPI;
using Telerik.WinControls.UI;
using Telerik.WinForms.Documents.RichTextBoxCommands;

namespace PackingSkama
{
    public partial class PackingQTYForm :  LVSearchForm
    {
        private List<GetUnitPack> _units = new List<GetUnitPack>();
        public List<DataForInsert> items { get; set; }
      
        public PackingQTYForm(List<DataForInsert> checkeditems)
        {
            InitializeComponent();
            items = checkeditems;
            buttonPackingOk.Click += ButtonPackingOk_Click;        
        }

        private void ButtonPackingOk_Click(object sender, EventArgs e)
        {
            InsertData();
        }


        private void InsertData()
        {//Εισαγωγή Δεδομένων
            try
            {

                if (Convert.ToInt32(cmbPackingUnit.SelectedIndex)>=0)
                {
                    if (!string.IsNullOrEmpty(textBoxPackingQty.Text))
                    {
                        double chars;
                        var hasChars = double.TryParse(textBoxPackingQty.Text.Replace(',', '.'), out chars);
                        if (hasChars == true && !textBoxPackingQty.Text.Contains("."))
                        {
                            var Constring = GetData.GetConDB();
                            using (var con = new SqlConnection(Constring))//ελεγχος εγκυροτητας
                            {
                                con.Open();
                                SqlCommand maxgroupid = new SqlCommand("select isnull(max(cpk_groupid),0)+1 from cccpacking", con);
                                var maxid = maxgroupid.ExecuteScalar();
                                for (int i = 0; i < items.Count; i++)
                                {
                                    SqlCommand maindata = new SqlCommand($"INSERT INTO CCCPACKING " +
                                    $"(cpk_OrderID,cpk_OrderCode,cpk_ShipmentID,cpk_ShipmentCode,cpk_GroupID,cpk_Flag1)" +
                                    $"values ({items[i].OrdID},'{items[i].OrdCode}',{items[i].OstID},'{items[i].OstCode}',{maxid},1)", con);
                                    maindata.ExecuteNonQuery();
                                }
                                
                                float num = float.Parse(textBoxPackingQty.Text);
                                var quantity = num.ToString().Replace(',', '.');
                                SqlCommand insqty = new SqlCommand($"INSERT INTO CCCPACKINGQTY" +
                                        $"(cpq_GroupID,cpq_Qty,cpq_UnitID) values ({maxid},'{quantity}',{Convert.ToInt32(cmbPackingUnit.SelectedValue)})", con);
                                insqty.ExecuteNonQuery();
                            }
                            MessageBox.Show("Η Καταχώρηση Ολοκληρώθηκε");
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Το Κείμενο Δεν Περιέχει Αριθμούς!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Δεν Έχετε Προσθέσει Ποσότητα!");
                    }
                }
                else
                {
                    MessageBox.Show("Δεν Έχετε Επιλέξει Μονάδα Μέτρησης!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void PackingQTYForm_Load(object sender, EventArgs e)
        {
            try
            {
                _units = FillUnits();
                cmbPackingUnit.DataSource = _units;
                cmbPackingUnit.DisplayMember = "cpu_Descr";
                cmbPackingUnit.ValueMember = "cpu_ID";
                cmbPackingUnit.SelectedItem = null;
            }
           catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }



        public List<GetUnitPack> FillUnits()
        {//γεμιζω απο τον πινακα CCCPackingUnits όπου ISactive=1 τις μονάδες κιβωτιοποίησης


                List<GetUnitPack> ListOfUnits = new List<GetUnitPack>();
                SqlConnection connection = new SqlConnection(GetData.GetConDB());          
                var CommandString = GetData.PackUnitQuery();
                using (SqlCommand cmd = new SqlCommand(CommandString, connection))
                {
                    connection.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            GetUnitPack item = new GetUnitPack();
                            item.cpu_ID = reader.GetInt32(0);
                            item.cpu_Code = reader.GetString(1);
                            item.cpu_Descr = reader.GetString(2);
                            item.cpu_ShortDescr = reader.GetString(3);
                            item.cpu_EngDescr = reader.GetString(4);
                            item.cpu_ShortEngDescr = reader.GetString(5);
                            item.cpu_IsActive = reader.GetInt32(6);
                            ListOfUnits.Add(item);
                        }
                    }
                }
            return ListOfUnits;

        }


    }
}
