﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackingSkama
{
    public class DataForInsert
    {
        public int OrdID { get; set; }
        public string OrdCode { get; set; }
        public int OstID { get; set; }
        public string OstCode { get; set; }
        public int GroupID { get; set; }
    }

}
