﻿namespace PackingSkama
{
    partial class PackingChangeQtyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PackingChangeQtyForm));
            this.textBoxChangeQty = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonChangePackingqty = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxChangeQty
            // 
            this.textBoxChangeQty.Location = new System.Drawing.Point(63, 67);
            this.textBoxChangeQty.Name = "textBoxChangeQty";
            this.textBoxChangeQty.Size = new System.Drawing.Size(100, 20);
            this.textBoxChangeQty.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label1.Location = new System.Drawing.Point(46, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Αλλαγή Ποσότητας";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label2.Location = new System.Drawing.Point(59, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Κιβωτιοποίησης";
            // 
            // buttonChangePackingqty
            // 
            this.buttonChangePackingqty.Location = new System.Drawing.Point(153, 93);
            this.buttonChangePackingqty.Name = "buttonChangePackingqty";
            this.buttonChangePackingqty.Size = new System.Drawing.Size(86, 23);
            this.buttonChangePackingqty.TabIndex = 3;
            this.buttonChangePackingqty.Text = "Ενημέρωση";
            this.buttonChangePackingqty.UseVisualStyleBackColor = true;
            // 
            // PackingChangeQtyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(251, 128);
            this.Controls.Add(this.buttonChangePackingqty);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxChangeQty);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PackingChangeQtyForm";
            this.Text = "Αλλαγή Ποσότητας";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxChangeQty;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonChangePackingqty;
    }
}