﻿using DevExpress.Utils.DPI;
using DevExpress.XtraEditors.Filtering;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace PackingSkama
{
   public class GetUnitPack
    {
        public int cpu_ID { get; set; }
        public string cpu_Code { get; set; }
        public string cpu_Descr { get; set; }
        public string cpu_ShortDescr { get; set; }
        public string cpu_EngDescr { get; set; }
        public string cpu_ShortEngDescr { get; set; }
        public int cpu_IsActive { get; set; }

    }
}