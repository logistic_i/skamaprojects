﻿using DevExpress.Utils.DPI;
using DevExpress.Xpo.DB.Helpers;
using DevExpress.XtraEditors.Filtering;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telerik.WinControls.UI;

namespace PackingSkama
{
    public static class GetData
    {
        public static string conStrng { get; set; }
        public static string GetConDB()
        {
           
            var conStr = conStrng;// ConfigurationManager.AppSettings["ConnectionString"]; 
//#if DEBUG
//            conStr = Properties.Settings.Default.connStr;

//#endif          
            return conStr;
        }
        internal static string GetMacAddress()
        {
            var firstMacAddress = NetworkInterface.GetAllNetworkInterfaces()
                                  .Where(x => x.OperationalStatus == OperationalStatus.Up)
                                  .Where(x => x.NetworkInterfaceType != NetworkInterfaceType.Ppp)
                                  .Where(x => x.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                                  .Select(x => x.GetPhysicalAddress().ToString())
                                  .FirstOrDefault(x => !string.IsNullOrWhiteSpace(x));
            return firstMacAddress;
        }

        internal static string MainSearchResults(string FromInputDate, bool FromInputDateChecked, string EndInputDate, bool EndInputDateChecked, string SearchText, int logisticsiteid, string Drom,
            string FromDeliv, bool FromDelivChecked,string EndDeliv , bool EndDelivChecked,string OrderCategoryScama, bool IsonAllOrders)
        {
            var WhereClause = "";
            var OrderBy = $" order by ost_StatusID desc, orr_FullName asc,ord_Code asc";
            var query = $"select ord_ID,ord_Code,ost_ID,ost_Code,ord_TypeID,ord_InputDate,ost_ExecuteDate," +
                      $"ost_ShipDate,orr_Code,orr_FullName, ost_StatusID,msg_greek,isnull(cpk_Flag1,0) as PackingFlag,cpk_GroupID, " +
                      $"oav1.oav_Code as RoutID ,all1.all_Value as RoutDesc,oav2.oav_Code as TranspID,all2.all_Value as TranspDesc,cpq_Qty,cpu_Descr,  " +
                      $"case when len(oav3.oav_Value)>1 then oav3.oav_Value end as RefCode,case when len(oav4.oav_Value)>1 then oav4.oav_Value end as RefID, " +
                      $"case when oav5.oav_Value=1 then 'ΟΛΙΚΗ' end as AllFlag,isnull(dbo.ValidLoadOrder(ord_LogisticSiteID,ord_Code),0) as AllFlagReady  " +
                      $"from LV_Order with(nolock) " +
                      $"inner join LV_OrderShipment with(nolock) on ost_OrderID = ord_ID " +
                      $"inner join LV_OrderReceiver with(nolock) on orr_OrderID = ord_ID " +
                      $"inner join LV_OrderStatus with(nolock) on ors_ID = ost_StatusID " +
                      $"inner join LV_Messages with(nolock)on msg_code = ors_MessageCode and msg_languageID = 2 " +
                      $"left join CCCPacking with(nolock) on ord_ID= cpk_OrderID and ost_ID = cpk_ShipmentID " +
                      $"left join CCCPackingQty with(nolock) on cpq_GroupID=cpk_GroupID " +
                      $"left join CCCPackingUnits with(nolock) on cpq_UnitID= cpu_ID " +
                      $"left join LV_OrderAttributesValues oav1 with(nolock) on oav1.oav_OrderID = ord_ID and oav1.oav_AttributeID = 11 " +
                      $"left join LV_OrderAttributeList oal1 on oal1.oal_Code = oav1.oav_Value and oal1.oal_AttributeID = 11 " +
                      $"left join LV_AttributeListValue all1 with(nolock) on all1.all_OrdAttrListID = oal1.oal_ID and all1.all_LanguageID = 2 " +
                      $"left join LV_OrderAttributesValues oav2 with(nolock) on oav2.oav_OrderID = ord_ID and oav2.oav_AttributeID = 12 " +
                      $"left join LV_OrderAttributeList oal2 on oal2.oal_Code = oav2.oav_Value and oal2.oal_AttributeID = 12 " +
                      $"left join LV_AttributeListValue all2 with(nolock) on all2.all_OrdAttrListID = oal2.oal_ID and all2.all_LanguageID = 2 " +
                      $"left join LV_OrderAttributesValues oav3 with(nolock) on oav3.oav_OrderID = ord_ID and oav3.oav_AttributeID = 14 "+
                      $"left join LV_OrderAttributesValues oav4 with(nolock) on oav4.oav_OrderID = ord_ID and oav4.oav_AttributeID = 15 "+
                      $"left join LV_OrderAttributesValues oav5 with(nolock) on oav5.oav_OrderID = ord_ID and oav5.oav_AttributeID = 16 " +
                      $"left join LV_OrderAttributesValues oav6 with(nolock) on oav6.oav_OrderID = ord_ID and oav6.oav_AttributeID = 20 " +
                     $"left join (select distinct cpsp_GroupID from CCCPackingSoftonePrintTable with(nolock))cpsp on cpsp.cpsp_GroupID=cpk_GroupID ";
            if (IsonAllOrders == false)
            {
                WhereClause += $"where ost_StatusID in (2,3,4,5,6,15) and isnull(ord_CCCLoadFlag,0)=0 and isnull(ord_exportedled,0)=0 and ord_LogisticSiteID={logisticsiteid} ";
            }
            if (IsonAllOrders == true)
            {
                WhereClause += $"where ord_LogisticSiteID={logisticsiteid} and cpk_ID is not null and cpsp.cpsp_GroupID is not null and isnull(ord_exportedled,0)=1  ";
            }



            if (FromInputDateChecked == true)
            {
                WhereClause += $" and cast(ord_inputdate as date) >= '{Convert.ToDateTime(FromInputDate).ToString("yyyy/MM/dd")}'";
            }
            if (EndInputDateChecked == true)
            {
                WhereClause += $" and cast(ord_inputdate as date) <= '{Convert.ToDateTime(EndInputDate).ToString("yyyy/MM/dd")}'";
            }
            if (!string.IsNullOrEmpty(SearchText))
            {
                WhereClause += $" and concat(ord_Code,' ',orr_Code,' ',orr_FullName,' ',all1.all_Value,' ',all2.all_Value) like '%{SearchText}%'";
            }
            if(!string.IsNullOrEmpty(Drom))
            {
                WhereClause += $" and oav1.oav_Code in ('{Drom}')";
            }
            if (!string.IsNullOrEmpty(OrderCategoryScama))
            {
                WhereClause += $" and oav6.oav_Code in ('{OrderCategoryScama}')";
            }
            if (FromDelivChecked == true)
            {
                WhereClause += $" and cast(ord_ExpDeliveryDate as date) >= '{Convert.ToDateTime(FromDeliv).ToString("yyyy/MM/dd")}'";
            }
            if (EndDelivChecked == true)
            {
                WhereClause += $" and cast(ord_ExpDeliveryDate as date) <= '{Convert.ToDateTime(EndDeliv).ToString("yyyy/MM/dd")}'";
            }

            query += WhereClause;
            query += OrderBy;
            return query;
            //throw new NotImplementedException();
        }

        internal static string GetDataItems(int groupid)
        {
            var query = $"select count(cpk_ID) from cccpacking where cpk_GroupID ={groupid}";
            return query;
        }

        internal static DataTable OrdersExist(List<int> checkedrows)
        {
            var ids = string.Join(",", checkedrows.Select(n => n).ToArray());
            var constr = GetConDB();
            var query = $"select distinct Cpk_Orderid,cpk_OrderCode from cccpacking where cpk_orderid in ({ids})";
            using (var con = new SqlConnection(constr))
            {
                con.Open();
                SqlCommand MainResults = con.CreateCommand();
                MainResults.CommandText = query;
                SqlDataAdapter da = new SqlDataAdapter(MainResults);
                DataTable dtMainResults = new DataTable();
                da.Fill(dtMainResults);
                return dtMainResults;
            }

        }

        public static List<string> GetPrinterLists()
        {
            List<string> PrinterLists = new List<string>();
            //εγκατεστημενοι εκτυπωτες στον υπολογιστη
            String pkInstalledPrinters;
            PrinterLists.Add("-------ΚΕΝΟ-----------");
            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                pkInstalledPrinters = PrinterSettings.InstalledPrinters[i];
                PrinterLists.Add(pkInstalledPrinters);
            }
            
            return PrinterLists;
        }

        internal static string PackUnitQuery()
        {
            var query = "select cpu_ID,cpu_Code,cpu_Descr,cpu_ShortDescr,cpu_EngDescr,cpu_ShortEngDescr,cpu_IsActive " +
                        "from CCCPackingUnits where isnull(cpu_IsActive,0)=1";
            return query;
        }

        internal static string DromQuery()
        {
            var query = "select oal_Code,oal_Value from V_OrderAttributeList where LanguageID=2 and oal_AttributeID = 11";
            return query;
        }


        internal static string OrderScamaCategoryQuery()
        {
            var query = "select oal_Code,oal_Value from V_OrderAttributeList where LanguageID=2 and oal_AttributeID = 20";
            return query;
        }
        public static void UpdatePrintedID(string Ids)
        {//update CCCPackingSoftonePrintTable τα cpsp_ID που έχουν εκτυπωσει
            var Constring = GetConDB();
            using (var con = new SqlConnection(Constring))//ελεγχος εγκυροτητας
            {
                con.Open();
                SqlCommand merge = new SqlCommand($"update CCCPackingSoftonePrintTable set cpsp_PrintedLED=1 where cpsp_ID in ({Ids})", con);
                merge.ExecuteNonQuery();                
            }
        }

        public static string GetPrinter()
        {
         
            string PrinterName;
            var Constring = GetConDB();
            using (var con = new SqlConnection(Constring))
            {
                con.Open();
                SqlCommand maxgroupid = new SqlCommand($"select cpp_PrinterName from CCCPackingPrinters where cpp_MacAdress='{GetMacAddress()}'" +
                                    $" and cpp_PrinterName is not null ", con);
               var maxid = maxgroupid.ExecuteScalar();
                if (maxid == null)
                {
                    PrinterName = null;
                }
                else
                {
                    PrinterName = maxid.ToString();
                }
            }
            return PrinterName;
        }
    }
}
