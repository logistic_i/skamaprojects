﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TecOrder;
using TecOrder.Dtos;

namespace DemoTecOrderClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var username = "7182000040330"; // SKAMA username
            var password = "Rc62YxTb"; // SKAMA PASSWORD
            var orderClient = new TecClient(username, password);

            var businessRelation = new BusinessRelation();
            businessRelation.BuyerNumber = "Test456"; //Test456 //104004247
            businessRelation.SupplierNumber = "SKAMA"; //SKAMA //0147

            var buyer = Helper.GetDummyBuyer(); // Dummy data for buyer, supplier and products
            var supplier = Helper.GetDummySupplier();
            var products = Helper.GetDummyProducts();

            var orderProducts = products.Where(x => x.ProductNumber == "0810" || x.ProductNumber == "0811").ToList();

            var res = orderClient.SubmitInquiry(businessRelation, orderProducts);

            if (res.Products != null && res.Products.Any())
            {
                foreach (var product in res.Products)
                {
                    if (product.Responses != null && product.Responses.Any())
                    {
                        var resp = product.Responses.First();
                        var msg = $"ProductNumber : {product.ProductNumber} | {resp.Class}! Code : {resp.Code}. Info : {resp.Text}".Replace("%", resp.Parameter);
                    }
                }
            }
            else
            {

            }

            var stockOrderRes = orderClient.SendStockOrder(businessRelation, buyer, supplier, orderProducts);

            //var expressOrder = orderClient.SendExpressOrder(businessRelation, buyer, supplier, orderProducts);
        }
    }
}
