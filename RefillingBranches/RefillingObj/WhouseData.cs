﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefillingObj
{
    class WhouseData
    {
        public int Whouse { get; set; }
        public int CentralWhouse { get; set; }
        public double CentralQtyRef { get; set; }
        public double CentralPackageRef { get; set; }
        public int Prioritization { get; set; }
        public double Remain { get; set; }
        public double AvailableRemain { get; set; }
        public double CccCommercialQty { get; set; }
        public double RemainLimMin { get; set; }
        public double RemainLimMax { get; set; }
        public double CccQtyStepReplacement { get; set; }
        public double Anamenomena { get; set; }
        public double Desmeymena { get; set; }
        public int NewOrder { get; set; }
        public double Ph1Qty { get; set; }
        public double Ph2Qty { get; set; }
        public double Ph3Qty { get; set; }
        public double Ph3Step1 { get; set; }
        public double Ph3Step2 { get; set; }
        public double Ph3Step3 { get; set; }
        public double Ph3Step4 { get; set; }
        public double Ph3QtyFin { get; set; }
        public double RefillingQty { get; set; }
        public double RefillingQtyUsr { get; set; }
        public double Ph4Qty { get; set; }
        public int FinalPhase { get; set; }
        public int RefMtrUnit1 { get; set; }
        public int RefMtrUnit2 { get; set; }
        public double Qty1Findocs { get; set; }
        public string CccRelDocs { get; set; }
    }
}
