﻿namespace RefillingObj
{
    partial class RefillingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding23 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding24 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding25 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding26 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding27 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding28 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding29 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding30 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding31 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding32 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding33 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding34 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding35 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding13 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding36 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding37 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding38 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding39 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding40 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding41 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding42 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding21 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
            this.pivotGridControl1 = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.fieldMtrlCode = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldMtrlName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldMtrMark = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldMtrModel = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAbcCateg = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCccCommercialQty = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldWhouse = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCentralWhouse = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRemain = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRemainLimMin = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRemainLimMax = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRefillingQty = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRefillingQtyUsr = new DevExpress.XtraPivotGrid.PivotGridField();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.fieldFinalPhase = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldIsCentral = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldMtrUnit1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldMtrUnit2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRefMtrUnit1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRefMtrUnit2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCentralQtyRef = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldChoice = new DevExpress.XtraPivotGrid.PivotGridField();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.fieldUtbl04 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pivotGridControl1
            // 
            this.pivotGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl1.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.fieldMtrlCode,
            this.fieldMtrlName,
            this.fieldMtrMark,
            this.fieldMtrModel,
            this.fieldAbcCateg,
            this.fieldCccCommercialQty,
            this.fieldWhouse,
            this.fieldCentralWhouse,
            this.fieldRemain,
            this.fieldRemainLimMin,
            this.fieldRemainLimMax,
            this.fieldRefillingQty,
            this.fieldRefillingQtyUsr,
            this.fieldFinalPhase,
            this.fieldIsCentral,
            this.fieldMtrUnit1,
            this.fieldMtrUnit2,
            this.fieldRefMtrUnit1,
            this.fieldRefMtrUnit2,
            this.fieldCentralQtyRef,
            this.fieldChoice,
            this.fieldUtbl04});
            this.pivotGridControl1.Location = new System.Drawing.Point(2, 2);
            this.pivotGridControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pivotGridControl1.Name = "pivotGridControl1";
            this.pivotGridControl1.OptionsData.DataProcessingEngine = DevExpress.XtraPivotGrid.PivotDataProcessingEngine.Optimized;
            this.pivotGridControl1.OptionsDataField.Caption = "Γενικός Πίνακας Αναπλήρωσης Καταστημάτων";
            this.pivotGridControl1.OptionsDataField.RowHeaderWidth = 133;
            this.pivotGridControl1.OptionsView.RowTreeOffset = 28;
            this.pivotGridControl1.OptionsView.RowTreeWidth = 133;
            this.pivotGridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemCheckEdit1});
            this.pivotGridControl1.Size = new System.Drawing.Size(1491, 519);
            this.pivotGridControl1.TabIndex = 0;
            this.pivotGridControl1.CustomCellValue += new System.EventHandler<DevExpress.XtraPivotGrid.PivotCellValueEventArgs>(this.PivotGridControl1_CustomCellValue);
            this.pivotGridControl1.CellClick += new DevExpress.XtraPivotGrid.PivotCellEventHandler(this.pivotGridControl1_CellClick);
            this.pivotGridControl1.CustomAppearance += new DevExpress.XtraPivotGrid.PivotCustomAppearanceEventHandler(this.pivotGridControl1_CustomAppearance);
            this.pivotGridControl1.EditValueChanged += new DevExpress.XtraPivotGrid.EditValueChangedEventHandler(this.PivotGridControl1_EditValueChanged);
            this.pivotGridControl1.ShownEditor += new System.EventHandler<DevExpress.XtraPivotGrid.PivotCellEditEventArgs>(this.pivotGridControl1_ShownEditor);
            this.pivotGridControl1.EditorKeyDown += new System.Windows.Forms.KeyEventHandler(this.pivotGridControl1_EditorKeyDown);
            this.pivotGridControl1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pivotGridControl1_KeyDown);
            // 
            // fieldMtrlCode
            // 
            this.fieldMtrlCode.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fieldMtrlCode.AreaIndex = 0;
            this.fieldMtrlCode.Caption = "Κωδικός Είδους";
            dataSourceColumnBinding23.ColumnName = "MtrlCode";
            this.fieldMtrlCode.DataBinding = dataSourceColumnBinding23;
            this.fieldMtrlCode.MinWidth = 27;
            this.fieldMtrlCode.Name = "fieldMtrlCode";
            this.fieldMtrlCode.Width = 180;
            // 
            // fieldMtrlName
            // 
            this.fieldMtrlName.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fieldMtrlName.AreaIndex = 1;
            this.fieldMtrlName.Caption = "Περιγραφή Είδους";
            dataSourceColumnBinding24.ColumnName = "MtrlName";
            this.fieldMtrlName.DataBinding = dataSourceColumnBinding24;
            this.fieldMtrlName.MinWidth = 40;
            this.fieldMtrlName.Name = "fieldMtrlName";
            this.fieldMtrlName.Width = 133;
            // 
            // fieldMtrMark
            // 
            this.fieldMtrMark.AreaIndex = 0;
            this.fieldMtrMark.Caption = "Sub Business Unit";
            dataSourceColumnBinding25.ColumnName = "MtrMark";
            this.fieldMtrMark.DataBinding = dataSourceColumnBinding25;
            this.fieldMtrMark.MinWidth = 27;
            this.fieldMtrMark.Name = "fieldMtrMark";
            this.fieldMtrMark.Width = 133;
            // 
            // fieldMtrModel
            // 
            this.fieldMtrModel.AreaIndex = 1;
            this.fieldMtrModel.Caption = "Segment";
            dataSourceColumnBinding26.ColumnName = "MtrModel";
            this.fieldMtrModel.DataBinding = dataSourceColumnBinding26;
            this.fieldMtrModel.MinWidth = 27;
            this.fieldMtrModel.Name = "fieldMtrModel";
            this.fieldMtrModel.Width = 133;
            // 
            // fieldAbcCateg
            // 
            this.fieldAbcCateg.AreaIndex = 2;
            this.fieldAbcCateg.Caption = "ABC Κατηγορία";
            dataSourceColumnBinding27.ColumnName = "AbcCateg";
            this.fieldAbcCateg.DataBinding = dataSourceColumnBinding27;
            this.fieldAbcCateg.MinWidth = 27;
            this.fieldAbcCateg.Name = "fieldAbcCateg";
            this.fieldAbcCateg.Width = 133;
            // 
            // fieldCccCommercialQty
            // 
            this.fieldCccCommercialQty.AreaIndex = 3;
            this.fieldCccCommercialQty.Caption = "Εμπορική Ποσότητα";
            dataSourceColumnBinding28.ColumnName = "CccCommercialQty";
            this.fieldCccCommercialQty.DataBinding = dataSourceColumnBinding28;
            this.fieldCccCommercialQty.MinWidth = 27;
            this.fieldCccCommercialQty.Name = "fieldCccCommercialQty";
            this.fieldCccCommercialQty.Width = 133;
            // 
            // fieldWhouse
            // 
            this.fieldWhouse.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fieldWhouse.AreaIndex = 3;
            this.fieldWhouse.Caption = "Α.Χ.";
            dataSourceColumnBinding29.ColumnName = "Whouse";
            this.fieldWhouse.DataBinding = dataSourceColumnBinding29;
            this.fieldWhouse.MinWidth = 27;
            this.fieldWhouse.Name = "fieldWhouse";
            this.fieldWhouse.Width = 133;
            // 
            // fieldCentralWhouse
            // 
            this.fieldCentralWhouse.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fieldCentralWhouse.AreaIndex = 4;
            this.fieldCentralWhouse.Caption = "Central Whouse";
            dataSourceColumnBinding30.ColumnName = "CentralWhouse";
            this.fieldCentralWhouse.DataBinding = dataSourceColumnBinding30;
            this.fieldCentralWhouse.MinWidth = 27;
            this.fieldCentralWhouse.Name = "fieldCentralWhouse";
            this.fieldCentralWhouse.Width = 133;
            // 
            // fieldRemain
            // 
            this.fieldRemain.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldRemain.AreaIndex = 0;
            this.fieldRemain.Caption = "Υπόλοιπο";
            this.fieldRemain.CellFormat.FormatString = "n2";
            this.fieldRemain.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            dataSourceColumnBinding31.ColumnName = "Remain";
            this.fieldRemain.DataBinding = dataSourceColumnBinding31;
            this.fieldRemain.MinWidth = 27;
            this.fieldRemain.Name = "fieldRemain";
            this.fieldRemain.Width = 133;
            // 
            // fieldRemainLimMin
            // 
            this.fieldRemainLimMin.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldRemainLimMin.AreaIndex = 1;
            this.fieldRemainLimMin.Caption = "Ελάχιστo όριο ασφαλείας";
            this.fieldRemainLimMin.CellFormat.FormatString = "n2";
            this.fieldRemainLimMin.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            dataSourceColumnBinding32.ColumnName = "RemainLimMin";
            this.fieldRemainLimMin.DataBinding = dataSourceColumnBinding32;
            this.fieldRemainLimMin.MinWidth = 27;
            this.fieldRemainLimMin.Name = "fieldRemainLimMin";
            this.fieldRemainLimMin.Width = 203;
            // 
            // fieldRemainLimMax
            // 
            this.fieldRemainLimMax.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldRemainLimMax.AreaIndex = 2;
            this.fieldRemainLimMax.Caption = "Μέγιστο όριο ασφαλείας";
            this.fieldRemainLimMax.CellFormat.FormatString = "n2";
            this.fieldRemainLimMax.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            dataSourceColumnBinding33.ColumnName = "RemainLimMax";
            this.fieldRemainLimMax.DataBinding = dataSourceColumnBinding33;
            this.fieldRemainLimMax.MinWidth = 27;
            this.fieldRemainLimMax.Name = "fieldRemainLimMax";
            this.fieldRemainLimMax.Width = 199;
            // 
            // fieldRefillingQty
            // 
            this.fieldRefillingQty.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldRefillingQty.AreaIndex = 3;
            this.fieldRefillingQty.Caption = "Ποσ. Αναπλήρωσης";
            this.fieldRefillingQty.CellFormat.FormatString = "n2";
            this.fieldRefillingQty.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            dataSourceColumnBinding34.ColumnName = "RefillingQty";
            this.fieldRefillingQty.DataBinding = dataSourceColumnBinding34;
            this.fieldRefillingQty.MinWidth = 27;
            this.fieldRefillingQty.Name = "fieldRefillingQty";
            this.fieldRefillingQty.Width = 171;
            // 
            // fieldRefillingQtyUsr
            // 
            this.fieldRefillingQtyUsr.Appearance.Cell.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.fieldRefillingQtyUsr.Appearance.Cell.Options.UseFont = true;
            this.fieldRefillingQtyUsr.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldRefillingQtyUsr.AreaIndex = 5;
            this.fieldRefillingQtyUsr.Caption = "Ποσ. Αναπλήρωσης Χρήστη";
            this.fieldRefillingQtyUsr.CellFormat.FormatString = "n2";
            this.fieldRefillingQtyUsr.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            dataSourceColumnBinding35.ColumnName = "RefillingQtyUsr";
            this.fieldRefillingQtyUsr.DataBinding = dataSourceColumnBinding35;
            this.fieldRefillingQtyUsr.FieldEdit = this.repositoryItemTextEdit1;
            this.fieldRefillingQtyUsr.MinWidth = 27;
            this.fieldRefillingQtyUsr.Name = "fieldRefillingQtyUsr";
            this.fieldRefillingQtyUsr.Width = 189;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.BeepOnError = false;
            this.repositoryItemTextEdit1.DisplayFormat.FormatString = "n2";
            this.repositoryItemTextEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit1.HideSelection = false;
            this.repositoryItemTextEdit1.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.NumericMaskManager));
            this.repositoryItemTextEdit1.MaskSettings.Set("MaskManagerSignature", "allowNull=False");
            this.repositoryItemTextEdit1.MaskSettings.Set("mask", "n2");
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // fieldFinalPhase
            // 
            this.fieldFinalPhase.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldFinalPhase.AreaIndex = 7;
            this.fieldFinalPhase.Caption = "Φάση Πρότασης";
            dataSourceColumnBinding13.ColumnName = "FinalPhase";
            this.fieldFinalPhase.DataBinding = dataSourceColumnBinding13;
            this.fieldFinalPhase.MinWidth = 27;
            this.fieldFinalPhase.Name = "fieldFinalPhase";
            this.fieldFinalPhase.Width = 133;
            // 
            // fieldIsCentral
            // 
            this.fieldIsCentral.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fieldIsCentral.AreaIndex = 2;
            this.fieldIsCentral.Caption = "Τύπος Αναπλήρωσης";
            dataSourceColumnBinding36.ColumnName = "IsCentral";
            this.fieldIsCentral.DataBinding = dataSourceColumnBinding36;
            this.fieldIsCentral.MinWidth = 27;
            this.fieldIsCentral.Name = "fieldIsCentral";
            this.fieldIsCentral.Width = 133;
            // 
            // fieldMtrUnit1
            // 
            this.fieldMtrUnit1.AreaIndex = 4;
            this.fieldMtrUnit1.Caption = "Μ.Μ.1";
            dataSourceColumnBinding37.ColumnName = "MtrUnit1";
            this.fieldMtrUnit1.DataBinding = dataSourceColumnBinding37;
            this.fieldMtrUnit1.MinWidth = 27;
            this.fieldMtrUnit1.Name = "fieldMtrUnit1";
            this.fieldMtrUnit1.Width = 133;
            // 
            // fieldMtrUnit2
            // 
            this.fieldMtrUnit2.AreaIndex = 5;
            this.fieldMtrUnit2.Caption = "Μ.Μ.2";
            dataSourceColumnBinding38.ColumnName = "MtrUnit2";
            this.fieldMtrUnit2.DataBinding = dataSourceColumnBinding38;
            this.fieldMtrUnit2.MinWidth = 27;
            this.fieldMtrUnit2.Name = "fieldMtrUnit2";
            this.fieldMtrUnit2.Width = 133;
            // 
            // fieldRefMtrUnit1
            // 
            this.fieldRefMtrUnit1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldRefMtrUnit1.AreaIndex = 4;
            this.fieldRefMtrUnit1.Caption = "Μ.Μ. Αναπλήρωσης";
            dataSourceColumnBinding39.ColumnName = "RefMtrUnit1";
            this.fieldRefMtrUnit1.DataBinding = dataSourceColumnBinding39;
            this.fieldRefMtrUnit1.MinWidth = 27;
            this.fieldRefMtrUnit1.Name = "fieldRefMtrUnit1";
            this.fieldRefMtrUnit1.Width = 133;
            // 
            // fieldRefMtrUnit2
            // 
            this.fieldRefMtrUnit2.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldRefMtrUnit2.AreaIndex = 6;
            this.fieldRefMtrUnit2.Caption = "Μ.Μ. Πρότασης";
            dataSourceColumnBinding40.ColumnName = "RefMtrUnit2";
            this.fieldRefMtrUnit2.DataBinding = dataSourceColumnBinding40;
            this.fieldRefMtrUnit2.MinWidth = 27;
            this.fieldRefMtrUnit2.Name = "fieldRefMtrUnit2";
            this.fieldRefMtrUnit2.Width = 133;
            // 
            // fieldCentralQtyRef
            // 
            this.fieldCentralQtyRef.Caption = "Ποσ. προς Αναπλήρωση Κεντικού";
            dataSourceColumnBinding41.ColumnName = "CentralQtyRef";
            this.fieldCentralQtyRef.DataBinding = dataSourceColumnBinding41;
            this.fieldCentralQtyRef.MinWidth = 27;
            this.fieldCentralQtyRef.Name = "fieldCentralQtyRef";
            this.fieldCentralQtyRef.Visible = false;
            this.fieldCentralQtyRef.Width = 133;
            // 
            // fieldChoice
            // 
            this.fieldChoice.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldChoice.AreaIndex = 8;
            this.fieldChoice.Caption = "Επιλογή";
            dataSourceColumnBinding42.ColumnName = "Choice";
            this.fieldChoice.DataBinding = dataSourceColumnBinding42;
            this.fieldChoice.FieldEdit = this.repositoryItemCheckEdit1;
            this.fieldChoice.MinWidth = 27;
            this.fieldChoice.Name = "fieldChoice";
            this.fieldChoice.Width = 133;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // fieldUtbl04
            // 
            this.fieldUtbl04.AreaIndex = 6;
            this.fieldUtbl04.Caption = "Υποομάδα momentum";
            dataSourceColumnBinding21.ColumnName = "Utbl04";
            this.fieldUtbl04.DataBinding = dataSourceColumnBinding21;
            this.fieldUtbl04.MinWidth = 27;
            this.fieldUtbl04.Name = "fieldUtbl04";
            this.fieldUtbl04.Options.AllowEdit = false;
            this.fieldUtbl04.Width = 133;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.progressBarControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 523);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1495, 31);
            this.panelControl1.TabIndex = 1;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.pivotGridControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1495, 523);
            this.panelControl2.TabIndex = 2;
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBarControl1.Location = new System.Drawing.Point(2, 2);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Properties.ShowTitle = true;
            this.progressBarControl1.Properties.Step = 1;
            this.progressBarControl1.Size = new System.Drawing.Size(1491, 27);
            this.progressBarControl1.TabIndex = 0;
            this.progressBarControl1.UseWaitCursor = true;
            // 
            // RefillingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1495, 554);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "RefillingForm";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldMtrlCode;
        private DevExpress.XtraPivotGrid.PivotGridField fieldMtrlName;
        private DevExpress.XtraPivotGrid.PivotGridField fieldMtrMark;
        private DevExpress.XtraPivotGrid.PivotGridField fieldMtrModel;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAbcCateg;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCccCommercialQty;
        private DevExpress.XtraPivotGrid.PivotGridField fieldWhouse;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCentralWhouse;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRemain;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRemainLimMin;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRemainLimMax;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRefillingQty;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRefillingQtyUsr;
        private DevExpress.XtraPivotGrid.PivotGridField fieldFinalPhase;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldIsCentral;
        private DevExpress.XtraPivotGrid.PivotGridField fieldMtrUnit1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldMtrUnit2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRefMtrUnit1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRefMtrUnit2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCentralQtyRef;
        private DevExpress.XtraPivotGrid.PivotGridField fieldChoice;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldUtbl04;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        public DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
    }
}