﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefillingObj
{
    class DataFromSoftone
    {
        public int IsCentral { get; set; }
        public int Whouse { get; set; }
        public int CentralWhouse { get; set; }
        public int Mtrl { get; set; }
        public string MtrlCode { get; set; }
        public string MtrlName { get; set; }
        public int MtrUnit1 { get; set; }
        public int MtrUnit2 { get; set; }
        public double Mu21 { get; set; }
        public int RefMtrUnit1 { get; set; }
        public int RefMtrUnit2 { get; set; }
        public double CentralQtyRef { get; set; }
        public int MtrMark { get; set; }
        public int MtrModel { get; set; }
        public string AbcCateg { get; set; }
        public double Remain { get; set; }
        public double CccCommercialQty { get; set; }
        public double RemainLimMin { get; set; }
        public double RemainLimMax { get; set; }
        public double CccQtyStepReplacement { get; set; }
        public double Ph3Qty { get; set; }
        public double Ph3QtyFin { get; set; }
        public double RefillingQty { get; set; }
        public double RefillingQtyUsr { get; set; }
        public double Ph4Qty { get; set; }
        public int FinalPhase { get; set; }
        public bool Choice { get; set; }
        public int Utbl04 { get; set; }
        public int FindocRef { get; set; }
        public string CccRelDocs { get; set; }
    }
}
