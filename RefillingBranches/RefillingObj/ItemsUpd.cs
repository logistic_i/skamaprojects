﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softone;

namespace RefillingObj
{
    [WorksOn("ITEM")]
    public class ItemsUpd:TXCode
    {
        private bool scenarioChanged = false;
        private int scenario = 0;

        public override void AfterInsert()
        {
            var Item = XModule.GetTable("ITEM");
            var Mtrl = Convert.ToInt32(Item.Current["MTRL"]);
            var MtrBrnLimits = XModule.GetTable("MTRBRNLIMITS");
            if (Mtrl < 0)
            {
                var query = $@"SELECT BRANCH FROM BRANCH WHERE COMPANY={XSupport.ConnectionInfo.CompanyId} AND CCCSUGGESTION=1";
                using (XTable dsbranch = XSupport.GetSQLDataSet(query))
                {
                    if (dsbranch.Count > 0)
                    {
                        for (int i = 0; i < dsbranch.Count; i++)
                        {
                            var recNo1 = MtrBrnLimits.Find("BRANCH", dsbranch.GetAsInteger(i, "BRANCH"));
                            if (recNo1 != -1)
                            {
                                MtrBrnLimits.Current.Edit(recNo1);
                                MtrBrnLimits.Current["BRANCH"] = dsbranch.GetAsInteger(i, "BRANCH");
                                MtrBrnLimits.Current["WHOUSE"] = dsbranch.GetAsInteger(i, "BRANCH");
                                MtrBrnLimits.Current.Post();
                            }
                            else
                            {
                                MtrBrnLimits.Current.Append();
                                MtrBrnLimits.Current["BRANCH"] = dsbranch.GetAsInteger(i, "BRANCH");
                                MtrBrnLimits.Current["WHOUSE"] = dsbranch.GetAsInteger(i, "BRANCH");
                                MtrBrnLimits.Current.Post();
                            }
                        }
                    }
                }
            }
            base.AfterInsert();
        }
        public override void BeforePost()
        {
            scenarioChanged = false;
            scenario = 0;
            var Item = XModule.GetTable("ITEM");
            var Mtrl = Convert.ToInt32(Item.Current["MTRL"]);
            var MtrSup = Convert.ToInt32(Item.Current["MTRSUP"] != DBNull.Value ? Item.Current["MTRSUP"] : 0);
            var MtrBrnLimits = XModule.GetTable("MTRBRNLIMITS");
            //gia tin periptvsh toy SOFTONE IMPORT
            if (Mtrl < 0 && MtrBrnLimits.Count==0)
            {
                var query = $@"SELECT BRANCH FROM BRANCH WHERE COMPANY={XSupport.ConnectionInfo.CompanyId} AND CCCSUGGESTION=1";
                using (XTable dsbranch = XSupport.GetSQLDataSet(query))
                {
                    if (dsbranch.Count > 0)
                    {
                        for (int i = 0; i < dsbranch.Count; i++)
                        {
                            var recNo1 = MtrBrnLimits.Find("BRANCH", dsbranch.GetAsInteger(i, "BRANCH"));
                            if (recNo1 != -1)
                            {
                                MtrBrnLimits.Current.Edit(recNo1);
                                MtrBrnLimits.Current["BRANCH"] = dsbranch.GetAsInteger(i, "BRANCH");
                                MtrBrnLimits.Current["WHOUSE"] = dsbranch.GetAsInteger(i, "BRANCH");
                                MtrBrnLimits.Current.Post();
                            }
                            else
                            {
                                MtrBrnLimits.Current.Append();
                                MtrBrnLimits.Current["BRANCH"] = dsbranch.GetAsInteger(i, "BRANCH");
                                MtrBrnLimits.Current["WHOUSE"] = dsbranch.GetAsInteger(i, "BRANCH");
                                MtrBrnLimits.Current.Post();
                            }
                        }
                    }
                }

                var supchainscenario = Convert.ToInt32(Item.Current["CCCSUPCHAINSCENH"] != DBNull.Value ? Item.Current["CCCSUPCHAINSCENH"] : 0);
                var query2 = $@"SELECT * FROM CCCSUPCHAINSCEND WHERE COMPANY={XSupport.ConnectionInfo.CompanyId} AND CCCSUPCHAINSCENH={supchainscenario}";
                using (XTable ds = XSupport.GetSQLDataSet(query2))
                {
                    if (ds.Count > 0)
                    {
                        for (int i = 0; i < ds.Count; i++)
                        {
                            var recNo1 = MtrBrnLimits.Find("WHOUSE", ds.GetAsInteger(i, "BUYERWH"));
                            if (recNo1 != -1)
                            {
                                MtrBrnLimits.Current.Edit(recNo1);
                                MtrBrnLimits.Current["CCCRECEIPTSTORE"] = ds.GetAsInteger(i, "SUPPLIERWH");
                                MtrBrnLimits.Current.Post();
                            }
                        }
                    }
                    if(ds.Count ==0 && supchainscenario>0)
                    {
                        while (MtrBrnLimits.Count > 0)
                        {
                            MtrBrnLimits.Current["CCCRECEIPTSTORE"] = DBNull.Value;
                        }
                    }
                }
            }

            var dbMtrsup = 0;
            var dbMtrsupquery = $@"SELECT isnull(MTRSUP,0) as MTRSUP FROM MTRL WHERE MTRL={Mtrl}";
            using (XTable dbMtrsupds = XSupport.GetSQLDataSet(dbMtrsupquery))
            {
                if (dbMtrsupds.Count > 0)
                {
                    dbMtrsup = dbMtrsupds.GetAsInteger(0, "MTRSUP");
                }
            }

            if (MtrSup != dbMtrsup)
            {
                var scenarioquery = $@"SELECT isnull(CCCSUPCHAINSCENH,0) as CCCSUPCHAINSCENH FROM TRDR WHERE TRDR = {MtrSup} AND ISNULL(CCCSUPCHAINSCENH,0) <> 0";
                using (XTable checkds = XSupport.GetSQLDataSet(scenarioquery))
                {
                    if (checkds.Count > 0)
                    {
                        scenario = checkds.GetAsInteger(0, "CCCSUPCHAINSCENH");
                    }
                }

                if (MtrBrnLimits.Count > 0 /*&& scenario > 0*/)
                {
                    if (Mtrl > 0)
                    {
                        var repeats = MtrBrnLimits.Count;
                        for (int i = 0; i < repeats; i++)
                        {
                            var whouse = MtrBrnLimits.GetAsInteger(i, "WHOUSE");
                            var receiptStore = MtrBrnLimits.GetAsInteger(i, "CCCRECEIPTSTORE");                       
                            var checkquery = $@"SELECT BUYERWH,SUPPLIERWH FROM CCCSUPCHAINSCEND WHERE CCCSUPCHAINSCENH = {scenario} AND BUYERWH ={whouse}";
                            using (XTable checkds = XSupport.GetSQLDataSet(checkquery))
                            {
                                if (checkds.Count > 0)
                                {
                                    var dsSupplierWh = checkds.GetAsInteger(0, "SUPPLIERWH");
                                    if (dsSupplierWh != receiptStore)
                                    {
                                        scenarioChanged = true;
                                    }
                                }
                                else
                                {
                                    scenarioChanged = true;
                                }
                            }                           
                        }
                    }
                    else
                    {
                        scenarioChanged = true;
                    }
                }
            }
            base.BeforePost();
        }

        public override void AfterPost()
        {
            var Item = XModule.GetTable("ITEM");
            var Mtrl = Convert.ToInt32(Item.Current["MTRL"]) > 0 ? Convert.ToInt32(Item.Current["MTRL"]) : XModule.ObjID();
            var MtrSup = Convert.ToInt32(Item.Current["MTRSUP"] != DBNull.Value ? Item.Current["MTRSUP"] : 0);

            if (scenarioChanged == true)
            {
                var ans = 0;
                ans = XSupport.AskYesNoCancel("Αλλαγή Αποθήκης Παραλαβής Αγορών", $"Θέλετε να πραγματοποιήσετε αλλαγή στην Αποθήκη Παραλαβής Αγορών των ειδών λόγω αλλαγής δεδομένων του βασικό προμηθευτή?");
                if (ans == 6) // 6=Yes, 7=No, 2=Cancel
                {
                    XSupport.ExecuteSQL($@"UPDATE Table_A SET Table_A.CCCRECEIPTSTORE = Table_B.SUPPLIERWH
                                        FROM MTRBRNLIMITS AS Table_A
                                        INNER JOIN (SELECT T.TRDR,T.CODE AS TCODE,T.NAME AS TNAME,T.CCCSUPCHAINSCENH,M.MTRL,M.CODE AS MTRLCODE,M.NAME AS MTRLNAME,S1.BUYERWH,S1.SUPPLIERWH,M1.WHOUSE,M1.CCCRECEIPTSTORE
                                        FROM MTRL M 
                                        LEFT JOIN MTRBRNLIMITS M1 ON M1.MTRL=M.MTRL LEFT JOIN TRDR T ON T.TRDR=M.MTRSUP AND T.TRDR={MtrSup}
                                        LEFT JOIN CCCSUPCHAINSCEND S1 ON S1.CCCSUPCHAINSCENH=T.CCCSUPCHAINSCENH AND M1.WHOUSE=S1.BUYERWH
                                        WHERE M.MTRL={Mtrl}) AS Table_B ON Table_A.MTRL = Table_B.MTRL AND Table_A.WHOUSE= Table_B.WHOUSE");

                    XSupport.ExecuteSQL($@"UPDATE MTRL SET CCCSUPCHAINSCENH =(SELECT CCCSUPCHAINSCENH FROM TRDR WHERE TRDR ={MtrSup} AND COMPANY={XSupport.ConnectionInfo.CompanyId}) WHERE MTRL={Mtrl}");
                }
            }
            base.AfterPost();
        }
    }
}
