﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraPivotGrid;
using Softone;

namespace RefillingObj
{
    [WorksOn("CCCREFILLING")]
    public class RefillingControler : TXCode
    {
        private RefillingForm refillingForm = null;
        private List<DataFromSoftone> softoneData = new List<DataFromSoftone>();
        public override void Initialize()
        {
            S1Tools.SetPropertyCustom(XModule, "PANEL", "Page1", "VISIBLE", "FALSE");
            refillingForm = new RefillingForm(XSupport, XModule);
            XModule.InsertControl(refillingForm, "*PANEL(RefillingForm,Ανάλυση Αναπλήρωσης ανά Α.Χ.)");
            InitData();
            base.Initialize();
        }

        public override void AfterLocate()
        {
            FillData();
            var CccRefillingH = XModule.GetTable("CCCREFILLINGH");
            var CccRefillingId = Convert.ToInt32(CccRefillingH.Current["CCCREFILLINGH"]);
            var trndateRef = Convert.ToDateTime(CccRefillingH.Current["TRNDATEREF"]);
            var today = DateTime.Now;
            if (today.Date <= trndateRef.Date)
            {
                S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelRef", "VISIBLE", "TRUE");
                //S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCREFILLINGH.TRNDATEREF", "READONLY", "FALSE");
            }
            else 
            {
                S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelRef", "VISIBLE", "FALSE");
                //S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCREFILLINGH.TRNDATEREF", "READONLY", "TRUE");
            }
            base.AfterLocate();
        }

        public override void AfterCancel()
        {
            FillData();
            base.AfterCancel();
        }

        public override void AfterPost()
        {
            var CccRefillingH = XModule.GetTable("CCCREFILLINGH");
            var CccRefillingWhouse = XModule.GetTable("CCCREFILLINGWHOUSE");
            var CccRefillingWhouseCen = XModule.GetTable("CCCREFILLINGWHOUSECEN");
            var CccRefillingId = Convert.ToInt32(CccRefillingH.Current["CCCREFILLINGH"]);

            if (CccRefillingId > 0)
            {
                if (softoneData.Count > 0)
                {
                    var choisedata = softoneData.Where(x => x.Choice == true).ToList();
                    refillingForm.progressBarControl1.Properties.Minimum = 0;
                    refillingForm.progressBarControl1.Properties.Maximum = choisedata.Count;
                    //var totalupdate = ""; 
                    foreach (var data in choisedata)
                    {
                        var update = $@"UPDATE CCCREFILLINGWHOUSE SET ISCENTRAL = {data.IsCentral},WHOUSE = {data.Whouse},CENTRALWHOUSE = {data.CentralWhouse},MTRL = { data.Mtrl},REFMTRUNIT1 = {data.RefMtrUnit1},REFMTRUNIT2 = {data.RefMtrUnit2},CENTRALQTYREF = { data.CentralQtyRef},CCCCOMMERCIALQTY = { data.CccCommercialQty},REMAINLIMMIN = { data.RemainLimMin},REMAINLIMMAX = { data.RemainLimMax},REFILLINGQTY = { data.RefillingQty},REFILLINGQTYUSR = { data.RefillingQtyUsr},FINALPHASE = { data.FinalPhase},REMAIN = { data.Remain},CHOICE = { (data.Choice == true ? 1 : 0)},CCCFINDOCREF = {(data.FindocRef != 0 ? data.FindocRef.ToString() : "NULL")} WHERE CCCREFILLINGH = { CccRefillingId } AND MTRL = {data.Mtrl} AND WHOUSE = { data.Whouse }";
                        XSupport.ExecuteSQL(update, null);
                        refillingForm.progressBarControl1.Position++;
                        Application.DoEvents();
                        //totalupdate = totalupdate + System.Environment.NewLine + update;
                    }
                    
                   //XSupport.ExecuteSQL(totalupdate, null);
                    
                    /*
                    var totalstr = "";
                    foreach (var data in softoneData)
                    {
                        var update = $@"({data.IsCentral},{data.Whouse},{data.CentralWhouse},{ data.Mtrl},{data.RefMtrUnit1},{data.RefMtrUnit2},{ data.CentralQtyRef},{ data.CccCommercialQty},{ data.RemainLimMin},{ data.RemainLimMax},{ data.RefillingQty},{ data.RefillingQtyUsr},{ data.FinalPhase},{ data.Remain},{ (data.Choice == true ? 1 : 0)},{(data.FindocRef != 0 ? data.FindocRef.ToString() : "NULL")} )";
                        totalstr = totalstr + (totalstr==""?"":",") + update;
                    }
                    var totalupdate = $@"UPDATE  tgt
                                        SET
                                          ISCENTRAL = src.ISCENTRAL,  WHOUSE = src.WHOUSE,  CENTRALWHOUSE = src.CENTRALWHOUSE,
                                          MTRL = src.MTRL,  REFMTRUNIT1 = src.REFMTRUNIT1,  REFMTRUNIT2 = src.REFMTRUNIT2,
                                          CENTRALQTYREF = src.CENTRALQTYREF,  CCCCOMMERCIALQTY = src.CCCCOMMERCIALQTY,
                                          REMAINLIMMIN = src.REMAINLIMMIN,  REMAINLIMMAX = src.REMAINLIMMAX,
                                          REFILLINGQTY = src.REFILLINGQTY,  REFILLINGQTYUSR = src.REFILLINGQTYUSR,
                                          FINALPHASE = src.FINALPHASE,  REMAIN = src.REMAIN,  CHOICE = src.CHOICE,  CCCFINDOCREF = src.CCCFINDOCREF
                                        FROM
                                          dbo.CCCREFILLINGWHOUSE AS tgt
                                          INNER JOIN
                                          (
                                            VALUES
                                            {totalstr}
                                          ) AS src (ISCENTRAL, WHOUSE, CENTRALWHOUSE, MTRL,REFMTRUNIT1,REFMTRUNIT2,CENTRALQTYREF,CCCCOMMERCIALQTY,
			                                        REMAINLIMMIN,REMAINLIMMAX,REFILLINGQTY,REFILLINGQTYUSR,FINALPHASE,REMAIN,CHOICE,CCCFINDOCREF)
                                            ON tgt.MTRL = src.MTRL AND tgt.WHOUSE = src.WHOUSE AND tgt.CCCREFILLINGH={CccRefillingId}";
                    XSupport.ExecuteSQL(totalupdate, null);
                    */
                }
            }
            base.AfterPost();
        }

        /*
        public override void BeforePost()
        { 
            var CccRefillingH = XModule.GetTable("CCCREFILLINGH");
            var CccRefillingWhouse = XModule.GetTable("CCCREFILLINGWHOUSE");
            var CccRefillingWhouseCen = XModule.GetTable("CCCREFILLINGWHOUSECEN");
            var CccRefillingId = Convert.ToInt32(CccRefillingH.Current["CCCREFILLINGH"]);

            var groupRefillingWhouse = softoneData.Where(x => x.IsCentral == 0).ToList();
            var groupRefillingWhouseCen = softoneData.Where(x => x.IsCentral == 1).ToList();

            if (CccRefillingId>0)
            {
                if (softoneData.Count > 0)
                {
                    foreach (var data in groupRefillingWhouse)
                    {
                        //CccRefillingWhouse.Current.Edit(0);
                        var recNo1 = CccRefillingWhouse.Find("MTRL;WHOUSE;CCCREFILLINGH", data.Mtrl, data.Whouse, CccRefillingId);
                        var dt = CccRefillingWhouse.CreateDataTable(true);
                        if (recNo1 != -1)
                        {
                            CccRefillingWhouse.Current.Edit(recNo1);
                            CccRefillingWhouse.Current["ISCENTRAL"] = data.IsCentral;
                            CccRefillingWhouse.Current["WHOUSE"] = data.Whouse;
                            CccRefillingWhouse.Current["CENTRALWHOUSE"] = data.CentralWhouse;
                            CccRefillingWhouse.Current["MTRL"] = data.Mtrl;

                            CccRefillingWhouse.Current["REFMTRUNIT1"] = data.RefMtrUnit1;
                            CccRefillingWhouse.Current["REFMTRUNIT2"] = data.RefMtrUnit2;
                            CccRefillingWhouse.Current["CENTRALQTYREF"] = data.CentralQtyRef;

                            CccRefillingWhouse.Current["CCCCOMMERCIALQTY"] = data.CccCommercialQty;
                            CccRefillingWhouse.Current["REMAINLIMMIN"] = data.RemainLimMin;
                            CccRefillingWhouse.Current["REMAINLIMMAX"] = data.RemainLimMax;
                            CccRefillingWhouse.Current["REFILLINGQTY"] = data.RefillingQty;
                            CccRefillingWhouse.Current["REFILLINGQTYUSR"] = data.RefillingQtyUsr;
                            CccRefillingWhouse.Current["FINALPHASE"] = data.FinalPhase;
                            CccRefillingWhouse.Current["REMAIN"] = data.Remain;
                            CccRefillingWhouse.Current["CHOICE"] = data.Choice == true ? 1 : 0;
                            if (data.FindocRef != 0)
                            {
                                CccRefillingWhouse.Current["CCCFINDOCREF"] = data.FindocRef;
                            }
                            CccRefillingWhouse.Current.Post();
                            //CccRefillingWhouse.Current.Edit(0);//γυρνάμε στην αρχή
                        }
                    }

                    foreach (var data in groupRefillingWhouseCen)
                    {
                        var recNo1 = CccRefillingWhouseCen.Find("MTRL;WHOUSE;CCCREFILLINGH", data.Mtrl, data.Whouse, CccRefillingId);
                        if (recNo1 != -1)
                        {
                            CccRefillingWhouseCen.Current.Edit(recNo1);
                            CccRefillingWhouseCen.Current["ISCENTRAL"] = data.IsCentral;
                            CccRefillingWhouseCen.Current["WHOUSE"] = data.Whouse;
                            CccRefillingWhouseCen.Current["CENTRALWHOUSE"] = data.CentralWhouse;
                            CccRefillingWhouseCen.Current["MTRL"] = data.Mtrl;

                            CccRefillingWhouseCen.Current["REFMTRUNIT1"] = data.RefMtrUnit1;
                            CccRefillingWhouseCen.Current["REFMTRUNIT2"] = data.RefMtrUnit2;
                            CccRefillingWhouseCen.Current["CENTRALQTYREF"] = data.CentralQtyRef;

                            CccRefillingWhouseCen.Current["CCCCOMMERCIALQTY"] = data.CccCommercialQty;
                            CccRefillingWhouseCen.Current["REMAINLIMMIN"] = data.RemainLimMin;
                            CccRefillingWhouseCen.Current["REMAINLIMMAX"] = data.RemainLimMax;
                            CccRefillingWhouseCen.Current["REFILLINGQTY"] = data.RefillingQty;
                            CccRefillingWhouseCen.Current["REFILLINGQTYUSR"] = data.RefillingQtyUsr;
                            CccRefillingWhouseCen.Current["FINALPHASE"] = data.FinalPhase;
                            CccRefillingWhouseCen.Current["REMAIN"] = data.Remain;
                            CccRefillingWhouseCen.Current["CHOICE"] = data.Choice==true?1:0;
                            if (data.FindocRef != 0)
                            {
                                CccRefillingWhouseCen.Current["CCCFINDOCREF"] = data.FindocRef;
                            }
                            CccRefillingWhouseCen.Current.Post();
                        }
                    }
                }
            }
            base.BeforePost();
        }
        */
        public override object ExecCommand(int Cmd)
        {
            if(Cmd == 20210513) //Δημιουργημένα Παραστατικά Αναπλήρωσης
            {
                var CccRefillingH = XModule.GetTable("CCCREFILLINGH");
                var CccFindocsRef = XModule.GetTable("CCCFINDOCSREF");
                var CccRefillingId = Convert.ToInt32(CccRefillingH.Current["CCCREFILLINGH"]);
                if (CccFindocsRef.Count > 0)
                {
                    while (CccFindocsRef.Count > 0)
                    {
                        CccFindocsRef.Current.Delete();
                    }
                }

                var query = $@"SELECT F.CCCREFILLINGH,F.FINDOC,F.FINCODE,F.TRNDATE,F.TRDR,F.BRANCH,MT.WHOUSE,MT.BRANCHSEC,MT.WHOUSESEC 
                            FROM FINDOC F 
                            INNER JOIN MTRDOC MT ON MT.FINDOC=F.FINDOC
                            WHERE F.CCCREFILLINGH ={CccRefillingId}";
                using (XTable ds = XSupport.GetSQLDataSet(query))
                {
                    if (ds.Count > 0)
                    {
                        for (int i = 0; i < ds.Count; i++)
                        {
                            CccFindocsRef.Current.Append();
                            CccFindocsRef.Current["FINDOC"] = ds.GetAsInteger(i, "FINDOC");
                            CccFindocsRef.Current["TRDR"] = ds.GetAsInteger(i, "TRDR");
                            CccFindocsRef.Current.Post();
                        }
                        XModule.OpenSubForm("SFFINDOCSREF", 1);
                    }
                    else 
                    {
                        XSupport.Warning("Δεν υπάρχουν δημιουργημένα παραστατικά αναπλήρωσης για την τρέχουσα αναπλήρωση κατασημάτων.");
                    }
                }
            }

            if (Cmd == 20210319) //Δημιουργία Παραστατικού Αναπλήρωσης
            {
                var CccRefillingH = XModule.GetTable("CCCREFILLINGH");
                var CccRefillingId = Convert.ToInt32(CccRefillingH.Current["CCCREFILLINGH"]);
                var Theme = Convert.ToString(CccRefillingH.Current["NAME"]);
                var trndateRef = Convert.ToDateTime(CccRefillingH.Current["TRNDATEREF"]);
                
                var chosenLines = softoneData.Where(c => c.Choice == true && c.FindocRef == 0 ).ToList();
                if (chosenLines.Count > 0)
                {
                    var centralWhousesList = chosenLines.Where(w => w.CentralWhouse > 0).Select(w => w.CentralWhouse).Distinct().ToList();
                    var whousesList = chosenLines.Where(w => w.Whouse > 0).Select(w => w.Whouse).Distinct().ToList();

                    foreach (var cwh in centralWhousesList)
                    {
                        if (cwh == 1001 || cwh == 1002)
                        {
                            foreach (var wh in whousesList)
                            {
                                var mtrlList = chosenLines.Where(w => w.CentralWhouse == cwh && w.Whouse == wh && w.RefillingQtyUsr > 0).ToList();
                                if (mtrlList.Count > 0)
                                {
                                    try
                                    {
                                        using (var SalDoc = XSupport.CreateModule("SALDOC;LikeDefault"))
                                        {
                                            //S1Tools.HideWarningsFromS1Module(SalDoc, XSupport);
                                            SalDoc.InsertData();
                                            if (cwh == 1001)
                                            {
                                                SalDoc.GetTable("FINDOC").Current["SERIES"] = 10148; //Παραγγελία αναπλήρωσης καταστημάτων Σίνδος
                                            }
                                            if (cwh == 1002)
                                            {
                                                SalDoc.GetTable("FINDOC").Current["SERIES"] = 10248; //Παραγγελία αναπλήρωσης καταστημάτων Πάππου
                                            }
                                            SalDoc.GetTable("FINDOC").Current["TRNDATE"] = trndateRef;

                                            SalDoc.GetTable("FINDOC").Current["TRDR"] = 27003; //003991 SKAMA ΑΝΩΝΥΜΗ ΕΤΑΙΡΙΑ

                                            var queryBranch = $@"SELECT TRDBRANCH FROM TRDBRANCH WHERE CCCBRANCH={wh} AND ISACTIVE=1 AND TRDR = 27003";
                                            using (XTable ds = XSupport.GetSQLDataSet(queryBranch))
                                            {
                                                if (ds.Count > 0)
                                                {
                                                    SalDoc.GetTable("FINDOC").Current["TRDBRANCH"] = ds.GetAsInteger(0, "TRDBRANCH");
                                                }
                                            }

                                            SalDoc.GetTable("FINDOC").Current["COMMENTS"] = Theme;
                                            SalDoc.GetTable("FINDOC").Current["CCCREFILLINGH"] = CccRefillingId;
                                            SalDoc.GetTable("MTRDOC").Current["BRANCHSEC"] = wh; //Υποκατάστημα και Α.Χ. είναι 1 προς 1
                                            SalDoc.GetTable("MTRDOC").Current["WHOUSESEC"] = wh;
                                            foreach (var mtrl in mtrlList)
                                            {
                                                SalDoc.GetTable("ITELINES").Current.Append();
                                                SalDoc.GetTable("ITELINES").Current["MTRL"] = mtrl.Mtrl;
                                                if (mtrl.FinalPhase != 4)
                                                {
                                                    SalDoc.GetTable("ITELINES").Current["QTY1"] = mtrl.RefillingQtyUsr;
                                                }
                                                else
                                                {
                                                    SalDoc.GetTable("ITELINES").Current["QTY1"] = (mtrl.RefillingQtyUsr * mtrl.Mu21);
                                                }
                                                SalDoc.GetTable("ITELINES").Current["WHOUSESEC"] = wh;
                                                SalDoc.GetTable("ITELINES").Current.Post();
                                            }
                                            var findoc = SalDoc.PostData();

                                            foreach (var mtrl in mtrlList)
                                            {
                                                mtrl.FindocRef = findoc;
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        XSupport.Exception(ex.Message);
                                    }
                                }
                            }
                        }
                    }
                    XSupport.Warning("Ολοκληρώθηκε η δημιουργία παραστατικών αναπλήρωσης.");
                    XModule.FocusField("CCCREFILLINGH.NAME");
                    XModule.Exec("Button:Save");
                }
                else 
                {
                    XSupport.Warning("Για να δημιουργήσετε παραστατικά αναπλήρωσης πρέπει να έχετε επιλέξει είδη ή τα είδη που επιλέξατε να μην συμμετέχουν ήδη σε κάποιο παραστατικό αναπλήρωσης.");
                }
            }
            return base.ExecCommand(Cmd);
        }

        public class MtrMark
        {
            public int ID { get; set; }
            public string Description { get; set; }
        }
        public class MtrModel
        {
            public int ID { get; set; }
            public string Description { get; set; }
        }
        public class MtrUnit
        {
            public int ID { get; set; }
            public string Description { get; set; }
        }
        public class Whouse
        {
            public int ID { get; set; }
            public string Description { get; set; }
        }
        public class Phase
        {
            public int ID { get; set; }
            public string Description { get; set; }
        }
        public class IsCentral
        {
            public int ID { get; set; }
            public string Description { get; set; }
        }
        public class Utbl04
        {
            public int ID { get; set; }
            public string Description { get; set; }
        }

        private List<MtrMark> MtrMarkList = new List<MtrMark>();
        private List<MtrModel> MtrModelList = new List<MtrModel>();
        private List<MtrUnit> MtrUnitList = new List<MtrUnit>();
        private List<Whouse> WhouseList = new List<Whouse>();
        private List<Phase> PhaseList = new List<Phase>();
        private List<IsCentral> IsCentralList = new List<IsCentral>();
        private List<Utbl04> Utbl04List = new List<Utbl04>();

        private void InitData()
        {
            var editorquery = $@"SELECT OBJ,ID,CODE,NAME FROM (
                                SELECT 'MTRMARK' AS OBJ,MTRMARK AS ID,CODE,NAME FROM MTRMARK WHERE COMPANY={XSupport.ConnectionInfo.CompanyId}
                                UNION ALL
                                SELECT 'MTRUNIT' AS OBJ,MTRUNIT AS ID,MTRUNIT,NAME FROM MTRUNIT WHERE COMPANY={XSupport.ConnectionInfo.CompanyId}
                                UNION ALL
                                SELECT 'MTRMODEL' AS OBJ,MTRMODEL AS ID,CODE,NAME FROM MTRMODEL WHERE COMPANY={XSupport.ConnectionInfo.CompanyId}
                                UNION ALL
                                SELECT 'WHOUSE' AS OBJ,WHOUSE AS ID,WHOUSE,NAME FROM WHOUSE WHERE COMPANY={XSupport.ConnectionInfo.CompanyId} 
                                UNION ALL
                                SELECT 'UTBL04' AS OBJ,UTBL04 AS ID,CODE,NAME FROM UTBL04 WHERE COMPANY={XSupport.ConnectionInfo.CompanyId} AND SODTYPE=51) Q";
            using (XTable ds = XSupport.GetSQLDataSet(editorquery))
            {
                if (ds.Count > 0)
                {
                    for (int i = 0; i < ds.Count; i++)
                    {
                        if (ds.GetAsString(i, "OBJ") == "MTRMARK")
                        {
                            MtrMarkList.Add(new MtrMark()
                            {
                                ID = ds.GetAsInteger(i, "ID"),
                                Description = ds.GetAsString(i, "NAME"),
                            });
                        }
                        if (ds.GetAsString(i, "OBJ") == "MTRMODEL")
                        {
                            MtrModelList.Add(new MtrModel()
                            {
                                ID = ds.GetAsInteger(i, "ID"),
                                Description = ds.GetAsString(i, "NAME"),
                            });
                        }
                        if (ds.GetAsString(i, "OBJ") == "MTRUNIT")
                        {
                            MtrUnitList.Add(new MtrUnit()
                            {
                                ID = ds.GetAsInteger(i, "ID"),
                                Description = ds.GetAsString(i, "NAME"),
                            });
                        }
                        if (ds.GetAsString(i, "OBJ") == "WHOUSE")
                        {
                            WhouseList.Add(new Whouse()
                            {
                                ID = ds.GetAsInteger(i, "ID"),
                                Description = ds.GetAsString(i, "NAME"),
                            });
                        }
                        if (ds.GetAsString(i, "OBJ") == "UTBL04")
                        {
                            Utbl04List.Add(new Utbl04()
                            {
                                ID = ds.GetAsInteger(i, "ID"),
                                Description = ds.GetAsString(i, "NAME"),
                            });
                        }
                    }
                }
            }

            //PhaseList.Add(new Phase() { ID = 0, Description = "1η Φάση" });
            PhaseList.Add(new Phase() { ID = 1, Description = "1η Φάση" });
            PhaseList.Add(new Phase() { ID = 2, Description = "2η Φάση" });
            PhaseList.Add(new Phase() { ID = 3, Description = "3η Φάση" });
            PhaseList.Add(new Phase() { ID = 4, Description = "4η Φάση" });

            IsCentralList.Add(new IsCentral() { ID = 0, Description = "Αναπλήρωση Υποκ/των" });
            IsCentralList.Add(new IsCentral() { ID = 1, Description = "Αναπλήρωση Κεντ. Α.Χ." });

            ////Editors
            RepositoryItemLookUpEdit riLookupMtrMark = new RepositoryItemLookUpEdit();
            riLookupMtrMark.DataSource = MtrMarkList;
            riLookupMtrMark.ValueMember = "ID";
            riLookupMtrMark.DisplayMember = "Description";
            riLookupMtrMark.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            riLookupMtrMark.DropDownRows = MtrMarkList.Count;
            riLookupMtrMark.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            refillingForm.pivotGridControl1.RepositoryItems.Add(riLookupMtrMark);
            refillingForm.pivotGridControl1.Fields["fieldMtrMark"].FieldEdit = riLookupMtrMark;

            RepositoryItemLookUpEdit riLookupMtrModel = new RepositoryItemLookUpEdit();
            riLookupMtrModel.DataSource = MtrModelList;
            riLookupMtrModel.ValueMember = "ID";
            riLookupMtrModel.DisplayMember = "Description";
            riLookupMtrModel.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            riLookupMtrModel.DropDownRows = MtrModelList.Count;
            riLookupMtrModel.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            refillingForm.pivotGridControl1.RepositoryItems.Add(riLookupMtrModel);
            refillingForm.pivotGridControl1.Fields["fieldMtrModel"].FieldEdit = riLookupMtrModel;

            RepositoryItemLookUpEdit riLookupWhouse = new RepositoryItemLookUpEdit();
            riLookupWhouse.DataSource = WhouseList;
            riLookupWhouse.ValueMember = "ID";
            riLookupWhouse.DisplayMember = "Description";
            riLookupWhouse.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            riLookupWhouse.DropDownRows = WhouseList.Count;
            riLookupWhouse.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            refillingForm.pivotGridControl1.RepositoryItems.Add(riLookupWhouse);
            refillingForm.pivotGridControl1.Fields["fieldWhouse"].FieldEdit = riLookupWhouse;
            refillingForm.pivotGridControl1.Fields["fieldCentralWhouse"].FieldEdit = riLookupWhouse;
            
            RepositoryItemLookUpEdit riLookupPhase = new RepositoryItemLookUpEdit();
            riLookupPhase.DataSource = PhaseList;
            riLookupPhase.ValueMember = "ID";
            riLookupPhase.DisplayMember = "Description";
            riLookupPhase.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            riLookupPhase.DropDownRows = PhaseList.Count;
            riLookupPhase.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            refillingForm.pivotGridControl1.RepositoryItems.Add(riLookupPhase);
            refillingForm.pivotGridControl1.Fields["FinalPhase"].FieldEdit = riLookupPhase;

            RepositoryItemLookUpEdit riLookupIsCentral = new RepositoryItemLookUpEdit();
            riLookupIsCentral.DataSource = IsCentralList;
            riLookupIsCentral.ValueMember = "ID";
            riLookupIsCentral.DisplayMember = "Description";
            riLookupIsCentral.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            riLookupIsCentral.DropDownRows = IsCentralList.Count;
            riLookupIsCentral.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            refillingForm.pivotGridControl1.RepositoryItems.Add(riLookupIsCentral);
            refillingForm.pivotGridControl1.Fields["IsCentral"].FieldEdit = riLookupIsCentral;

            RepositoryItemLookUpEdit riLookupMtrUnit = new RepositoryItemLookUpEdit();
            riLookupMtrUnit.DataSource = MtrUnitList;
            riLookupMtrUnit.ValueMember = "ID";
            riLookupMtrUnit.DisplayMember = "Description";
            riLookupMtrUnit.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            riLookupMtrUnit.DropDownRows = MtrUnitList.Count;
            riLookupMtrUnit.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            refillingForm.pivotGridControl1.RepositoryItems.Add(riLookupMtrUnit);
            refillingForm.pivotGridControl1.Fields["MtrUnit1"].FieldEdit = riLookupMtrUnit;
            refillingForm.pivotGridControl1.Fields["MtrUnit2"].FieldEdit = riLookupMtrUnit;
            refillingForm.pivotGridControl1.Fields["RefMtrUnit1"].FieldEdit = riLookupMtrUnit;
            refillingForm.pivotGridControl1.Fields["RefMtrUnit2"].FieldEdit = riLookupMtrUnit;

            RepositoryItemLookUpEdit riLookupUtbl04 = new RepositoryItemLookUpEdit();
            riLookupUtbl04.DataSource = Utbl04List;
            riLookupUtbl04.ValueMember = "ID";
            riLookupUtbl04.DisplayMember = "Description";
            riLookupUtbl04.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            riLookupUtbl04.DropDownRows = Utbl04List.Count;
            riLookupUtbl04.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            refillingForm.pivotGridControl1.RepositoryItems.Add(riLookupUtbl04);
            refillingForm.pivotGridControl1.Fields["fieldUtbl04"].FieldEdit = riLookupUtbl04;
        }

        private void FillData()
        {
            var mtrCateg = XSupport.GetMemoryTable("MTRMARK").CreateDataTable(true);
            var CccRefillingWhouse = XModule.GetTable("CCCREFILLINGWHOUSE");
            var CccRefillingWhouseCen = XModule.GetTable("CCCREFILLINGWHOUSECEN");

            softoneData = new List<DataFromSoftone>();
            for (int i = 0; i < CccRefillingWhouse.Count; i++)
            {
                softoneData.Add(new DataFromSoftone
                {
                    IsCentral = CccRefillingWhouse.GetAsInteger(i, "ISCENTRAL"),
                    Whouse = CccRefillingWhouse.GetAsInteger(i, "WHOUSE"),
                    CentralWhouse = CccRefillingWhouse.GetAsInteger(i, "CENTRALWHOUSE"),
                    Mtrl = CccRefillingWhouse.GetAsInteger(i, "MTRL"),
                    MtrlCode = CccRefillingWhouse.GetAsString(i, "M_CODE"),
                    MtrlName = CccRefillingWhouse.GetAsString(i, "M_NAME"),
                    MtrMark = CccRefillingWhouse.GetAsInteger(i, "M_MTRMARK"),
                    MtrModel = CccRefillingWhouse.GetAsInteger(i, "M_MTRMODEL"),
                    AbcCateg = CccRefillingWhouse.GetAsString(i, "M_CCCABCCATEG"),

                    MtrUnit1 = CccRefillingWhouse.GetAsInteger(i, "M_MTRUNIT1"),
                    MtrUnit2 = CccRefillingWhouse.GetAsInteger(i, "M_MTRUNIT2"),
                    Mu21 = CccRefillingWhouse.GetAsFloat(i, "M_MU21"),
                    RefMtrUnit1 = CccRefillingWhouse.GetAsInteger(i, "REFMTRUNIT1"),
                    RefMtrUnit2 = CccRefillingWhouse.GetAsInteger(i, "REFMTRUNIT2"),
                    CentralQtyRef = CccRefillingWhouse.GetAsFloat(i, "CENTRALQTYREF"),

                    CccCommercialQty = CccRefillingWhouse.GetAsFloat(i, "CCCCOMMERCIALQTY"),
                    RemainLimMin = CccRefillingWhouse.GetAsFloat(i, "REMAINLIMMIN"),
                    RemainLimMax = CccRefillingWhouse.GetAsFloat(i, "REMAINLIMMAX"),
                    RefillingQty = CccRefillingWhouse.GetAsFloat(i, "REFILLINGQTY"),
                    RefillingQtyUsr = CccRefillingWhouse.GetAsFloat(i, "REFILLINGQTYUSR"),
                    FinalPhase = CccRefillingWhouse.GetAsInteger(i, "FINALPHASE"),
                    Remain = CccRefillingWhouse.GetAsFloat(i, "REMAIN"),
                    Choice = CccRefillingWhouse.GetAsInteger(i, "CHOICE")==1 ? true :false,
                    FindocRef = CccRefillingWhouse.GetAsInteger(i, "CCCFINDOCREF"),
                    Utbl04 = CccRefillingWhouse.GetAsInteger(i, "E_UTBL04")
                });
            }

            for (int i = 0; i < CccRefillingWhouseCen.Count; i++)
            {
                softoneData.Add(new DataFromSoftone
                {
                    IsCentral = CccRefillingWhouseCen.GetAsInteger(i, "ISCENTRAL"),
                    Whouse = CccRefillingWhouseCen.GetAsInteger(i, "WHOUSE"),
                    CentralWhouse = CccRefillingWhouseCen.GetAsInteger(i, "CENTRALWHOUSE"),
                    Mtrl = CccRefillingWhouseCen.GetAsInteger(i, "MTRL"),
                    MtrlCode = CccRefillingWhouseCen.GetAsString(i, "M_CODE"),
                    MtrlName = CccRefillingWhouseCen.GetAsString(i, "M_NAME"),
                    MtrMark = CccRefillingWhouseCen.GetAsInteger(i, "M_MTRMARK"),
                    MtrModel = CccRefillingWhouseCen.GetAsInteger(i, "M_MTRMODEL"),
                    AbcCateg = CccRefillingWhouseCen.GetAsString(i, "M_CCCABCCATEG"),

                    MtrUnit1 = CccRefillingWhouseCen.GetAsInteger(i, "M_MTRUNIT1"),
                    MtrUnit2 = CccRefillingWhouseCen.GetAsInteger(i, "M_MTRUNIT2"),
                    Mu21 = CccRefillingWhouseCen.GetAsFloat(i, "M_MU21"),
                    RefMtrUnit1 = CccRefillingWhouseCen.GetAsInteger(i, "REFMTRUNIT1"),
                    RefMtrUnit2 = CccRefillingWhouseCen.GetAsInteger(i, "REFMTRUNIT2"),
                    CentralQtyRef = CccRefillingWhouseCen.GetAsFloat(i, "CENTRALQTYREF"),

                    CccCommercialQty = CccRefillingWhouseCen.GetAsFloat(i, "CCCCOMMERCIALQTY"),
                    RemainLimMin = CccRefillingWhouseCen.GetAsFloat(i, "REMAINLIMMIN"),
                    RemainLimMax = CccRefillingWhouseCen.GetAsFloat(i, "REMAINLIMMAX"),
                    RefillingQty = CccRefillingWhouseCen.GetAsFloat(i, "REFILLINGQTY"),
                    RefillingQtyUsr = CccRefillingWhouseCen.GetAsFloat(i, "REFILLINGQTYUSR"),
                    FinalPhase = CccRefillingWhouseCen.GetAsInteger(i, "FINALPHASE"),
                    Remain = CccRefillingWhouseCen.GetAsFloat(i, "REMAIN"),
                    Choice = CccRefillingWhouseCen.GetAsInteger(i, "CHOICE")==1?true:false,
                    FindocRef = CccRefillingWhouseCen.GetAsInteger(i, "CCCFINDOCREF"),
                    Utbl04 = CccRefillingWhouseCen.GetAsInteger(i, "E_UTBL04")
                });
            }

            if (softoneData.Count > 0)
            {
                refillingForm.PivotDs = softoneData;
                //refillingForm.pivotGridControl1.DataSource = softoneData;
                refillingForm.pivotGridControl1.BestFit();
            }
        }
    }
}
