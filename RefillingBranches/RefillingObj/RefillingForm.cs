﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraPivotGrid;
using Softone;

namespace RefillingObj
{
    public partial class RefillingForm : Form
    {
        private XSupport xSupport = null;
        private XModule xModule = null;
        private List<DataFromSoftone> _pivotDs;

        internal List<DataFromSoftone> PivotDs
        {
            get
            {
                return _pivotDs;
            } 
            set 
            {
                _pivotDs = value;
                pivotGridControl1.DataSource = _pivotDs;
            } 
        }

        public RefillingForm(XSupport _xSupport, XModule _xModule)
        {
            xSupport = _xSupport;
            xModule = _xModule;
            InitializeComponent();
            pivotGridControl1.CustomCellValue += PivotGridControl1_CustomCellValue;
            pivotGridControl1.EditValueChanged += PivotGridControl1_EditValueChanged;
            repositoryItemCheckEdit1.CheckedChanged += RepositoryItemCheckEdit1_CheckedChanged;
        }

        private void RepositoryItemCheckEdit1_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                
                CheckEdit checkEditor = (CheckEdit)sender;
                var checkvalue = (bool)checkEditor.EditValue;
                var cellInfo = pivotGridControl1.Cells.GetFocusedCellInfo();
                var ds = pivotGridControl1.CreateDrillDownDataSource(cellInfo.ColumnIndex, cellInfo.RowIndex);
                var row = ((List<DataFromSoftone>)pivotGridControl1.DataSource)[ds[0].ListSourceRowIndex];
                //ds.SetValue(cellInfo.RowIndex, cellInfo.DataField, checkvalue);
                var myRec = _pivotDs.FirstOrDefault(x => x.MtrlCode == row.MtrlCode && x.Whouse == row.Whouse);
                myRec.Choice = checkvalue;
                pivotGridControl1.DataSource = _pivotDs;
                //pivotGridControl1.CloseEditor();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PivotGridControl1_EditValueChanged(object sender, DevExpress.XtraPivotGrid.EditValueChangedEventArgs e)
        {
            var CccRefillingH = xModule.GetTable("CCCREFILLINGH");
            var trndateRef = Convert.ToDateTime(CccRefillingH.Current["TRNDATEREF"]);
            var today = DateTime.Now;
            if (today.Date <= trndateRef.Date)
            {
                if ((e.DataField.Name == "fieldRefillingQtyUsr" || e.DataField.Name == "fieldChoice") && e.Value != null)
                {
                    if (e.DataField.Name == "fieldRefillingQtyUsr")
                    {
                        ChangeCellValueDouble(e, Convert.ToDouble(e.Value), Convert.ToDouble(e.Editor.EditValue));
                    }
                    if (e.DataField.Name == "fieldChoice")
                    {
                        ChangeCellValueBool(e, Convert.ToBoolean(e.Value), Convert.ToBoolean(e.Editor.EditValue));
                    }
                    var CccRefilling = xModule.GetTable("CCCREFILLINGH");
                    CccRefilling.Current["CALCFIELD"] = 1;
                }
            }
        }

        private void ChangeCellValueDouble(EditValueChangedEventArgs e, double oldValue, double newValue)
        {
            PivotDrillDownDataSource ds = e.CreateDrillDownDataSource();
            var mtrlcode = e.GetFieldValue(fieldMtrlCode).ToString();
            var source = (List<DataFromSoftone>)pivotGridControl1.DataSource;
            var fsource = source.Where(x =>x.MtrlCode == mtrlcode).ToList();
            var refmtrunit1 = fsource.Select(y => y.RefMtrUnit1).Distinct().FirstOrDefault();
            var refmtrunit2 = fsource.Select(y => y.RefMtrUnit2).Distinct().FirstOrDefault();
            var mu21 = fsource.Select(y => y.Mu21).Distinct().FirstOrDefault();
            //RefMtrUnit1
            var sum1 = fsource.Sum(x=>x.RefillingQty);
            var sum2 = (double)0;
            if (refmtrunit1 == refmtrunit2)
            {
                sum2 = fsource.Sum(x => x.RefillingQtyUsr);
            }
            else
            {
                sum2 = (fsource.Sum(x => x.RefillingQtyUsr) * mu21);
            }

            if (sum2 <= sum1)
            {
                for (int i = 0; i < ds.RowCount; i++)
                {
                    ds.SetValue(i, e.DataField, newValue);
                }
            }
            else 
            {
                xSupport.Warning($@"Έχετε ξεπεράσει την υπολογιζόμενη ποσότητα προς αναπλήρωση για το είδος '{mtrlcode}', παρακαλώ μεταβάλετε τις ποσότητες προς αναπλήρωση ώστε να μην ξεπερνάνε την υπολογιζόμενη ποσότητα προς αναπλήρωση.");
                for (int i = 0; i < ds.RowCount; i++)
                {
                    ds.SetValue(i, e.DataField, oldValue);
                }
            }
        }
        private void ChangeCellValueBool(EditValueChangedEventArgs e, bool oldValue, bool newValue)
        {
            PivotDrillDownDataSource ds = e.CreateDrillDownDataSource();
            for (int i = 0; i < ds.RowCount; i++)
            {
                ds.SetValue(i, e.DataField, newValue);
            }
        }

        private void PivotGridControl1_CustomCellValue(object sender, DevExpress.XtraPivotGrid.PivotCellValueEventArgs e)
        {
            try
            {
                if (e.DataField.Name == "fieldRefillingQtyUsr" && e.Value != null)
                {
                    e.Value = (Convert.ToDouble(e.Value));
                }

                if (e.DataField.Name == "fieldChoice" && e.Value != null)
                {
                    e.Value = (Convert.ToInt32(e.Value) > 0) ? true : false;
                }

                if (e.DataField.Name == "fieldRemainLimMin" || e.DataField.Name == "fieldRemainLimMax" || e.DataField.Name == "fieldChoice")
                {
                    // Do not display grand total or Custom Totals or totals values.
                    if (e.RowValueType == PivotGridValueType.Total || e.RowValueType == PivotGridValueType.GrandTotal || e.RowValueType == PivotGridValueType.CustomTotal)
                    {
                        e.Value = null;
                        return;
                    }

                    /*
                    var rowValues = e.GetRowFields().Select(f => f.FieldName == "CategoryName" ? "Beverages" : e.GetFieldValue(f)).ToArray();
                    var columnValues = e.GetColumnFields().Select(f => f.FieldName == "CategoryName" ? "Beverages" : e.GetFieldValue(f)).ToArray();
                    decimal beveragesValue = Convert.ToDecimal(e.GetCellValue(columnValues, rowValues, e.DataField));
                    if (beveragesValue == 0)
                        e.Value = null;
                    else
                        e.Value = Convert.ToDecimal(e.Value) / beveragesValue;
                    */
                }
                else return;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
        }

        private void pivotGridControl1_CustomAppearance(object sender, PivotCustomAppearanceEventArgs e)
        {
            try
            {
                if (e.DataField.Name == "fieldRefillingQtyUsr")
                {
                    e.Appearance.BackColor = Color.Lime;
                }
                if (e.DataField.Name == "fieldRemain")
                {
                    e.Appearance.BackColor = Color.Khaki;
                }
                if (e.DataField.Name == "fieldRemainLimMin")
                {
                    e.Appearance.BackColor = Color.LightCoral;
                }
                if (e.DataField.Name == "fieldRemainLimMax")
                {
                    e.Appearance.BackColor = Color.LightGreen;
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
        }

        private void pivotGridControl1_ShownEditor(object sender, PivotCellEditEventArgs e)
        {
            try
            {
                PivotGridControl gridView = sender as PivotGridControl;
                if (gridView.ActiveEditor.EditValue != null && gridView.ActiveEditor.EditValue.GetType().Name == "Double")
                {
                    gridView.ActiveEditor.SelectAll();
                }
                else
                {
                    if (gridView.ActiveEditor.EditValue != null && (bool)gridView.ActiveEditor.EditValue != false && (bool)gridView.ActiveEditor.EditValue != true)
                        gridView.ActiveEditor.SelectAll();
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
        }

        private void repositoryItemTextEdit1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                var cellInfo = pivotGridControl1.Cells.GetFocusedCellInfo();
                var fieldname = cellInfo.DataField.Name;
                TextEdit editor = sender as TextEdit;
                PivotGridControl pivotGridControl = editor.Parent as PivotGridControl;
                if ((e.KeyCode == Keys.Tab || e.KeyCode == Keys.Down || e.KeyCode == Keys.Enter) && (fieldname != "fieldChoice"))
                {
                    pivotGridControl.PostEditor();
                    pivotGridControl.CloseEditor();
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
        }
        private void pivotGridControl1_EditorKeyDown(object sender, KeyEventArgs e)
        {
            var cellInfo = pivotGridControl1.Cells.GetFocusedCellInfo();
            var fieldname = cellInfo.DataField.Name;
            if ((e.KeyCode == Keys.Tab || e.KeyCode == Keys.Down || e.KeyCode == Keys.Enter) && (fieldname != "fieldChoice"))
            {
                PivotGridControl pivotGridControl = sender as PivotGridControl;
                if (e.Shift)
                    NavigateBackByColumns(pivotGridControl);
                else
                    NavigateFollowByColumns(pivotGridControl);
                e.Handled = true;
            }
        }
        void NavigateBackByColumns(PivotGridControl pivot)
        {
            Point currentFocus = pivot.Cells.FocusedCell;
            if (currentFocus.X == 0 && currentFocus.Y == 0)
                return;

            if (currentFocus.Y == 0)
            {
                currentFocus.X--;
                currentFocus.Y = pivot.Cells.RowCount - 1;
            }
            else
                currentFocus.Y--;
            pivot.Cells.FocusedCell = currentFocus;
            pivot.ShowEditor();
        }
        void NavigateFollowByColumns(PivotGridControl pivot)
        {
            Point currentFocus = pivot.Cells.FocusedCell;
            if (currentFocus.X == pivot.Cells.ColumnCount - 1 && currentFocus.Y == pivot.Cells.RowCount - 1)
                return;

            if (currentFocus.Y == pivot.Cells.RowCount - 1)
            {
                currentFocus.X++;
                currentFocus.Y = 0;
            }
            else
                currentFocus.Y++;
            pivot.Cells.FocusedCell = currentFocus;
            pivot.ShowEditor();
        }
        private void pivotGridControl1_KeyDown(object sender, KeyEventArgs e)
        {
            var cellInfo = pivotGridControl1.Cells.GetFocusedCellInfo();
            var fieldname = cellInfo.DataField.Name;
            if ((e.KeyCode == Keys.Tab || e.KeyCode == Keys.Down || e.KeyCode == Keys.Enter) && (fieldname != "fieldChoice"))
            {
                PivotGridControl pivotGridControl = sender as PivotGridControl;
                if (e.Shift)
                    NavigateBackByColumns(pivotGridControl);
                else
                    NavigateFollowByColumns(pivotGridControl);
                e.Handled = true;
            }
        }

        private void pivotGridControl1_CellClick(object sender, PivotCellEventArgs e)
        {
            //try
            //{
            //    //CheckEdit checkEditor = (CheckEdit)sender;
            //    CheckEdit checkEditor = (CheckEdit)pivotGridControl1.CloseEditor();
            //    var checkvalue = !(bool)checkEditor.EditValue;
                
            //    checkEditor.EditValue = checkvalue;
            //    var cellInfo = pivotGridControl1.Cells.GetFocusedCellInfo();
            //    var ds = pivotGridControl1.CreateDrillDownDataSource(cellInfo.ColumnIndex, cellInfo.RowIndex);
            //    ds.SetValue(cellInfo.RowIndex, cellInfo.DataField, checkvalue);

            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }
    }
}
