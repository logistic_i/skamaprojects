﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraPivotGrid;
using Softone;

namespace RefillingObj
{
    [WorksOn("CCCREFILLINGBYFINDOCS")]
    class RefillingControlerByFindocs : TXCode
    {
        private RefillingForm refillingForm = null;
        private List<DataFromSoftone> softoneData = new List<DataFromSoftone>();
        public override void Initialize()
        {
            S1Tools.SetPropertyCustom(XModule, "PANEL", "Page1", "VISIBLE", "FALSE");
            refillingForm = new RefillingForm(XSupport, XModule);
            XModule.InsertControl(refillingForm, "*PANEL(RefillingForm,Ανάλυση Αναπλήρωσης ανά Α.Χ.)");
            InitData();
            base.Initialize();
        }

        public override void AfterInsert()
        {
            S1Tools.SetPropertyCustom(XModule, "PANEL", "Page2", "VISIBLE", "FALSE");
            S1Tools.SetPropertyCustom(XModule, "PANEL", "PageRef", "VISIBLE", "FALSE");
            S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelF", "VISIBLE", "FALSE");
            S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelB", "VISIBLE", "TRUE");
            var CccRefillingH = XModule.GetTable("CCCREFILLINGH");
            CccRefillingH.Current["NAME"] = $@"Αναπλήρωση Καταστημάτων({DateTime.Now.ToString("dddd, dd MMMM yyyy")})";
            CccRefillingH.Current["TRNDATEREF"] = DateTime.Now;
            CccRefillingH.Current["TRNDATE"] = DateTime.Now;
            base.AfterInsert();
        }

        public override void AfterLocate()
        {
            FillData();
            if (softoneData.Count > 0)
            {
                S1Tools.SetPropertyCustom(XModule, "PANEL", "Page2", "VISIBLE", "TRUE");
                S1Tools.SetPropertyCustom(XModule, "PANEL", "PageRef", "VISIBLE", "TRUE");
                S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelF", "VISIBLE", "TRUE");
                S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelB", "VISIBLE", "FALSE");
            }
            else 
            {
                S1Tools.SetPropertyCustom(XModule, "PANEL", "Page2", "VISIBLE", "FALSE");
                S1Tools.SetPropertyCustom(XModule, "PANEL", "PageRef", "VISIBLE", "FALSE");
                S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelF", "VISIBLE", "FALSE");
                S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelB", "VISIBLE", "TRUE");
            }

            var CccRefillingH = XModule.GetTable("CCCREFILLINGH");
            var CccRefillingId = Convert.ToInt32(CccRefillingH.Current["CCCREFILLINGH"]);
            var trndateRef = Convert.ToDateTime(CccRefillingH.Current["TRNDATEREF"]);
            var today = DateTime.Now;
            if (today.Date <= trndateRef.Date)
            {
                S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelRef", "VISIBLE", "TRUE");
                //S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCREFILLINGH.TRNDATEREF", "READONLY", "FALSE");
            }
            else
            {
                S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelRef", "VISIBLE", "FALSE");
                //S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCREFILLINGH.TRNDATEREF", "READONLY", "TRUE");
            }
            base.AfterLocate();
        }

        public override void AfterCancel()
        {
            FillData();
            base.AfterCancel();
        }

        public override void AfterPost()
        {
            var CccRefillingH = XModule.GetTable("CCCREFILLINGH");
            var CccRefillingWhouse = XModule.GetTable("CCCREFILLINGWHOUSE");
            var CccRefillingWhouseCen = XModule.GetTable("CCCREFILLINGWHOUSECEN");
            var CccRefillingId = Convert.ToInt32(CccRefillingH.Current["CCCREFILLINGH"]);

            if (CccRefillingId > 0)
            {
                if (softoneData.Count > 0)
                {
                    var totalupdate = "";
                    foreach (var data in softoneData)
                    {
                        var update = $@"UPDATE CCCREFILLINGWHOUSE SET ISCENTRAL = {data.IsCentral},WHOUSE = {data.Whouse},CENTRALWHOUSE = {data.CentralWhouse},MTRL = { data.Mtrl},REFMTRUNIT1 = {data.RefMtrUnit1},REFMTRUNIT2 = {data.RefMtrUnit2},CENTRALQTYREF = { data.CentralQtyRef},CCCCOMMERCIALQTY = { data.CccCommercialQty},REMAINLIMMIN = { data.RemainLimMin},REMAINLIMMAX = { data.RemainLimMax},REFILLINGQTY = { data.RefillingQty},REFILLINGQTYUSR = { data.RefillingQtyUsr},FINALPHASE = { data.FinalPhase},REMAIN = { data.Remain},CHOICE = { (data.Choice == true ? 1 : 0)},CCCFINDOCREF = {(data.FindocRef != 0 ? data.FindocRef.ToString() : "NULL")} WHERE CCCREFILLINGH = { CccRefillingId } AND MTRL = {data.Mtrl} AND WHOUSE = { data.Whouse }";
                        totalupdate = totalupdate + System.Environment.NewLine + update;
                    }
                    XSupport.ExecuteSQL(totalupdate, null);
                }
            }
            base.AfterPost();
        }

        public override object ExecCommand(int Cmd)
        {
            if (Cmd == 20210502)//Υπολογισμός Αναπλήρωσης
            {
                XModule.GetTable("CCCFINDOCFORREF").Current.Append();
                var findocsStr = "";
                var CccFindocForRef = XModule.GetTable("CCCFINDOCFORREF").CreateDataTable(true).AsEnumerable().ToList();
                if (CccFindocForRef.Any())
                {
                    var findocs = CccFindocForRef.Select(x => x["FINDOC"] != DBNull.Value ? Convert.ToInt32(x["FINDOC"]) : 0).ToList();
                    findocsStr = string.Join(",", findocs);
                }
                if (findocsStr != "")
                {
                    RefillingCalculations(findocsStr);
                    XModule.CloseSubForm("SFFINDOCS");
                }
                else 
                {
                    XSupport.Warning("Για να πραγματοποιηθεί ο υπολογισμός αναπλήρωσης πρέπει να έχετε επιλέξει τουλάχιστον ένα παραστατικό προς αναπλήρωση.");
                }
            }

            if (Cmd == 20210501) //Επιλογή Παραστατικών προς Αναπλήρωσης
            {
                XModule.OpenSubForm("SFFINDOCS", 1);
            }

            if (Cmd == 20210513) //Δημιουργημένα Παραστατικά Αναπλήρωσης
            {
                var CccRefillingH = XModule.GetTable("CCCREFILLINGH");
                var CccFindocsRef = XModule.GetTable("CCCFINDOCSREF");
                var CccRefillingId = Convert.ToInt32(CccRefillingH.Current["CCCREFILLINGH"]);
                if (CccFindocsRef.Count > 0)
                {
                    while (CccFindocsRef.Count > 0)
                    {
                        CccFindocsRef.Current.Delete();
                    }
                }

                var query = $@"SELECT F.CCCREFILLINGH,F.FINDOC,F.FINCODE,F.TRNDATE,F.TRDR,F.BRANCH,MT.WHOUSE,MT.BRANCHSEC,MT.WHOUSESEC 
                            FROM FINDOC F 
                            INNER JOIN MTRDOC MT ON MT.FINDOC=F.FINDOC
                            WHERE F.CCCREFILLINGH ={CccRefillingId}";
                using (XTable ds = XSupport.GetSQLDataSet(query))
                {
                    if (ds.Count > 0)
                    {
                        for (int i = 0; i < ds.Count; i++)
                        {
                            CccFindocsRef.Current.Append();
                            CccFindocsRef.Current["FINDOC"] = ds.GetAsInteger(i, "FINDOC");
                            CccFindocsRef.Current["TRDR"] = ds.GetAsInteger(i, "TRDR");
                            CccFindocsRef.Current.Post();
                        }
                        XModule.OpenSubForm("SFFINDOCSREF", 1);
                    }
                    else
                    {
                        XSupport.Warning("Δεν υπάρχουν δημιουργημένα παραστατικά αναπλήρωσης για την τρέχουσα αναπλήρωση κατασημάτων.");
                    }
                }
            }

            if (Cmd == 20210319) //Δημιουργία Παραστατικού Αναπλήρωσης
            {
                var CccRefillingH = XModule.GetTable("CCCREFILLINGH");
                var CccRefillingId = Convert.ToInt32(CccRefillingH.Current["CCCREFILLINGH"]);
                var Theme = Convert.ToString(CccRefillingH.Current["NAME"]);
                var trndateRef = Convert.ToDateTime(CccRefillingH.Current["TRNDATEREF"]);

                var chosenLines = softoneData.Where(c => c.Choice == true && c.FindocRef == 0).ToList();
                if (chosenLines.Count > 0)
                {
                    var centralWhousesList = chosenLines.Where(w => w.CentralWhouse > 0).Select(w => w.CentralWhouse).Distinct().ToList();
                    var whousesList = chosenLines.Where(w => w.Whouse > 0).Select(w => w.Whouse).Distinct().ToList();

                    foreach (var cwh in centralWhousesList)
                    {
                        if (cwh == 1001 || cwh == 1002)
                        {
                            foreach (var wh in whousesList)
                            {
                                var mtrlList = chosenLines.Where(w => w.CentralWhouse == cwh && w.Whouse == wh && w.RefillingQtyUsr > 0).ToList();
                                if (mtrlList.Count > 0)
                                {
                                    try
                                    {
                                        using (var SalDoc = XSupport.CreateModule("SALDOC;LikeDefault"))
                                        {
                                            //S1Tools.HideWarningsFromS1Module(SalDoc, XSupport);
                                            SalDoc.InsertData();
                                            if (cwh == 1001)
                                            {
                                                SalDoc.GetTable("FINDOC").Current["SERIES"] = 10147; //Παραγγελία αναπλήρωσης καταστημάτων Σίνδος τιμολογίου Εξωτερικού
                                            }
                                            if (cwh == 1002)
                                            {
                                                SalDoc.GetTable("FINDOC").Current["SERIES"] = 10247; //Παραγγελία αναπλήρωσης καταστημάτων Πάππου τιμολογίου Εξωτερικού
                                            }
                                            SalDoc.GetTable("FINDOC").Current["TRNDATE"] = trndateRef;

                                            SalDoc.GetTable("FINDOC").Current["TRDR"] = 27003; //003991 SKAMA ΑΝΩΝΥΜΗ ΕΤΑΙΡΙΑ

                                            var queryBranch = $@"SELECT TRDBRANCH FROM TRDBRANCH WHERE CCCBRANCH={wh} AND ISACTIVE=1 AND TRDR = 27003";
                                            using (XTable ds = XSupport.GetSQLDataSet(queryBranch))
                                            {
                                                if (ds.Count > 0)
                                                {
                                                    SalDoc.GetTable("FINDOC").Current["TRDBRANCH"] = ds.GetAsInteger(0, "TRDBRANCH");
                                                }
                                            }

                                            SalDoc.GetTable("FINDOC").Current["COMMENTS"] = Theme;
                                            SalDoc.GetTable("FINDOC").Current["CCCREFILLINGH"] = CccRefillingId;
                                            SalDoc.GetTable("MTRDOC").Current["BRANCHSEC"] = wh; //Υποκατάστημα και Α.Χ. είναι 1 προς 1
                                            SalDoc.GetTable("MTRDOC").Current["WHOUSESEC"] = wh;
                                            foreach (var mtrl in mtrlList)
                                            {
                                                SalDoc.GetTable("ITELINES").Current.Append();
                                                SalDoc.GetTable("ITELINES").Current["MTRL"] = mtrl.Mtrl;
                                                if (mtrl.FinalPhase != 4)
                                                {
                                                    SalDoc.GetTable("ITELINES").Current["QTY1"] = mtrl.RefillingQtyUsr;
                                                }
                                                else
                                                {
                                                    SalDoc.GetTable("ITELINES").Current["QTY1"] = (mtrl.RefillingQtyUsr*mtrl.Mu21);
                                                }
                                                SalDoc.GetTable("ITELINES").Current["WHOUSESEC"] = wh;
                                                SalDoc.GetTable("ITELINES").Current["CCCRELDOCS"] = mtrl.CccRelDocs;
                                                SalDoc.GetTable("ITELINES").Current.Post();
                                            }
                                            var findoc = SalDoc.PostData();

                                            foreach (var mtrl in mtrlList)
                                            {
                                                mtrl.FindocRef = findoc;
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        XSupport.Exception(ex.Message);
                                    }
                                }
                            }
                        }
                    }
                    XSupport.Warning("Ολοκληρώθηκε η δημιουργία παραστατικών αναπλήρωσης.");
                    XModule.FocusField("CCCREFILLINGH.NAME");
                    XModule.Exec("Button:Save");
                }
            }
            return base.ExecCommand(Cmd);
        }
        public class MtrMark
        {
            public int ID { get; set; }
            public string Description { get; set; }
        }
        public class MtrModel
        {
            public int ID { get; set; }
            public string Description { get; set; }
        }
        public class MtrUnit
        {
            public int ID { get; set; }
            public string Description { get; set; }
        }
        public class Whouse
        {
            public int ID { get; set; }
            public string Description { get; set; }
        }
        public class Phase
        {
            public int ID { get; set; }
            public string Description { get; set; }
        }
        public class IsCentral
        {
            public int ID { get; set; }
            public string Description { get; set; }
        }
        public class Utbl04
        {
            public int ID { get; set; }
            public string Description { get; set; }
        }

        private List<MtrMark> MtrMarkList = new List<MtrMark>();
        private List<MtrModel> MtrModelList = new List<MtrModel>();
        private List<MtrUnit> MtrUnitList = new List<MtrUnit>();
        private List<Whouse> WhouseList = new List<Whouse>();
        private List<Phase> PhaseList = new List<Phase>();
        private List<IsCentral> IsCentralList = new List<IsCentral>();
        private List<Utbl04> Utbl04List = new List<Utbl04>();

        private void InitData()
        {
            var editorquery = $@"SELECT OBJ,ID,CODE,NAME FROM (
                                SELECT 'MTRMARK' AS OBJ,MTRMARK AS ID,CODE,NAME FROM MTRMARK WHERE COMPANY={XSupport.ConnectionInfo.CompanyId}
                                UNION ALL
                                SELECT 'MTRUNIT' AS OBJ,MTRUNIT AS ID,MTRUNIT,NAME FROM MTRUNIT WHERE COMPANY={XSupport.ConnectionInfo.CompanyId}
                                UNION ALL
                                SELECT 'MTRMODEL' AS OBJ,MTRMODEL AS ID,CODE,NAME FROM MTRMODEL WHERE COMPANY={XSupport.ConnectionInfo.CompanyId}
                                UNION ALL
                                SELECT 'WHOUSE' AS OBJ,WHOUSE AS ID,WHOUSE,NAME FROM WHOUSE WHERE COMPANY={XSupport.ConnectionInfo.CompanyId} 
                                UNION ALL
                                SELECT 'UTBL04' AS OBJ,UTBL04 AS ID,CODE,NAME FROM UTBL04 WHERE COMPANY={XSupport.ConnectionInfo.CompanyId} AND SODTYPE=51) Q";
            using (XTable ds = XSupport.GetSQLDataSet(editorquery))
            {
                if (ds.Count > 0)
                {
                    for (int i = 0; i < ds.Count; i++)
                    {
                        if (ds.GetAsString(i, "OBJ") == "MTRMARK")
                        {
                            MtrMarkList.Add(new MtrMark()
                            {
                                ID = ds.GetAsInteger(i, "ID"),
                                Description = ds.GetAsString(i, "NAME"),
                            });
                        }
                        if (ds.GetAsString(i, "OBJ") == "MTRMODEL")
                        {
                            MtrModelList.Add(new MtrModel()
                            {
                                ID = ds.GetAsInteger(i, "ID"),
                                Description = ds.GetAsString(i, "NAME"),
                            });
                        }
                        if (ds.GetAsString(i, "OBJ") == "MTRUNIT")
                        {
                            MtrUnitList.Add(new MtrUnit()
                            {
                                ID = ds.GetAsInteger(i, "ID"),
                                Description = ds.GetAsString(i, "NAME"),
                            });
                        }
                        if (ds.GetAsString(i, "OBJ") == "WHOUSE")
                        {
                            WhouseList.Add(new Whouse()
                            {
                                ID = ds.GetAsInteger(i, "ID"),
                                Description = ds.GetAsString(i, "NAME"),
                            });
                        }
                        if (ds.GetAsString(i, "OBJ") == "UTBL04")
                        {
                            Utbl04List.Add(new Utbl04()
                            {
                                ID = ds.GetAsInteger(i, "ID"),
                                Description = ds.GetAsString(i, "NAME"),
                            });
                        }
                    }
                }
            }

            //PhaseList.Add(new Phase() { ID = 0, Description = "1η Φάση" });
            PhaseList.Add(new Phase() { ID = 1, Description = "1η Φάση" });
            PhaseList.Add(new Phase() { ID = 2, Description = "2η Φάση" });
            PhaseList.Add(new Phase() { ID = 3, Description = "3η Φάση" });
            PhaseList.Add(new Phase() { ID = 4, Description = "4η Φάση" });

            IsCentralList.Add(new IsCentral() { ID = 0, Description = "Αναπλήρωση Υποκ/των" });
            IsCentralList.Add(new IsCentral() { ID = 1, Description = "Αναπλήρωση Κεντ. Α.Χ." });

            ////Editors
            RepositoryItemLookUpEdit riLookupMtrMark = new RepositoryItemLookUpEdit();
            riLookupMtrMark.DataSource = MtrMarkList;
            riLookupMtrMark.ValueMember = "ID";
            riLookupMtrMark.DisplayMember = "Description";
            riLookupMtrMark.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            riLookupMtrMark.DropDownRows = MtrMarkList.Count;
            riLookupMtrMark.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            refillingForm.pivotGridControl1.RepositoryItems.Add(riLookupMtrMark);
            refillingForm.pivotGridControl1.Fields["fieldMtrMark"].FieldEdit = riLookupMtrMark;

            RepositoryItemLookUpEdit riLookupMtrModel = new RepositoryItemLookUpEdit();
            riLookupMtrModel.DataSource = MtrModelList;
            riLookupMtrModel.ValueMember = "ID";
            riLookupMtrModel.DisplayMember = "Description";
            riLookupMtrModel.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            riLookupMtrModel.DropDownRows = MtrModelList.Count;
            riLookupMtrModel.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            refillingForm.pivotGridControl1.RepositoryItems.Add(riLookupMtrModel);
            refillingForm.pivotGridControl1.Fields["fieldMtrModel"].FieldEdit = riLookupMtrModel;

            RepositoryItemLookUpEdit riLookupWhouse = new RepositoryItemLookUpEdit();
            riLookupWhouse.DataSource = WhouseList;
            riLookupWhouse.ValueMember = "ID";
            riLookupWhouse.DisplayMember = "Description";
            riLookupWhouse.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            riLookupWhouse.DropDownRows = WhouseList.Count;
            riLookupWhouse.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            refillingForm.pivotGridControl1.RepositoryItems.Add(riLookupWhouse);
            refillingForm.pivotGridControl1.Fields["fieldWhouse"].FieldEdit = riLookupWhouse;
            refillingForm.pivotGridControl1.Fields["fieldCentralWhouse"].FieldEdit = riLookupWhouse;

            RepositoryItemLookUpEdit riLookupPhase = new RepositoryItemLookUpEdit();
            riLookupPhase.DataSource = PhaseList;
            riLookupPhase.ValueMember = "ID";
            riLookupPhase.DisplayMember = "Description";
            riLookupPhase.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            riLookupPhase.DropDownRows = PhaseList.Count;
            riLookupPhase.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            refillingForm.pivotGridControl1.RepositoryItems.Add(riLookupPhase);
            refillingForm.pivotGridControl1.Fields["FinalPhase"].FieldEdit = riLookupPhase;

            RepositoryItemLookUpEdit riLookupIsCentral = new RepositoryItemLookUpEdit();
            riLookupIsCentral.DataSource = IsCentralList;
            riLookupIsCentral.ValueMember = "ID";
            riLookupIsCentral.DisplayMember = "Description";
            riLookupIsCentral.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            riLookupIsCentral.DropDownRows = IsCentralList.Count;
            riLookupIsCentral.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            refillingForm.pivotGridControl1.RepositoryItems.Add(riLookupIsCentral);
            refillingForm.pivotGridControl1.Fields["IsCentral"].FieldEdit = riLookupIsCentral;

            RepositoryItemLookUpEdit riLookupMtrUnit = new RepositoryItemLookUpEdit();
            riLookupMtrUnit.DataSource = MtrUnitList;
            riLookupMtrUnit.ValueMember = "ID";
            riLookupMtrUnit.DisplayMember = "Description";
            riLookupMtrUnit.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            riLookupMtrUnit.DropDownRows = MtrUnitList.Count;
            riLookupMtrUnit.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            refillingForm.pivotGridControl1.RepositoryItems.Add(riLookupMtrUnit);
            refillingForm.pivotGridControl1.Fields["MtrUnit1"].FieldEdit = riLookupMtrUnit;
            refillingForm.pivotGridControl1.Fields["MtrUnit2"].FieldEdit = riLookupMtrUnit;
            refillingForm.pivotGridControl1.Fields["RefMtrUnit1"].FieldEdit = riLookupMtrUnit;
            refillingForm.pivotGridControl1.Fields["RefMtrUnit2"].FieldEdit = riLookupMtrUnit;

            RepositoryItemLookUpEdit riLookupUtbl04 = new RepositoryItemLookUpEdit();
            riLookupUtbl04.DataSource = Utbl04List;
            riLookupUtbl04.ValueMember = "ID";
            riLookupUtbl04.DisplayMember = "Description";
            riLookupUtbl04.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            riLookupUtbl04.DropDownRows = Utbl04List.Count;
            riLookupUtbl04.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            refillingForm.pivotGridControl1.RepositoryItems.Add(riLookupUtbl04);
            refillingForm.pivotGridControl1.Fields["fieldUtbl04"].FieldEdit = riLookupUtbl04;
        }

        private void FillData()
        {
            var mtrCateg = XSupport.GetMemoryTable("MTRMARK").CreateDataTable(true);
            var CccRefillingWhouse = XModule.GetTable("CCCREFILLINGWHOUSE");
            var CccRefillingWhouseCen = XModule.GetTable("CCCREFILLINGWHOUSECEN");

            softoneData = new List<DataFromSoftone>();
            for (int i = 0; i < CccRefillingWhouse.Count; i++)
            {
                softoneData.Add(new DataFromSoftone
                {
                    IsCentral = CccRefillingWhouse.GetAsInteger(i, "ISCENTRAL"),
                    Whouse = CccRefillingWhouse.GetAsInteger(i, "WHOUSE"),
                    CentralWhouse = CccRefillingWhouse.GetAsInteger(i, "CENTRALWHOUSE"),
                    Mtrl = CccRefillingWhouse.GetAsInteger(i, "MTRL"),
                    MtrlCode = CccRefillingWhouse.GetAsString(i, "M_CODE"),
                    MtrlName = CccRefillingWhouse.GetAsString(i, "M_NAME"),
                    MtrMark = CccRefillingWhouse.GetAsInteger(i, "M_MTRMARK"),
                    MtrModel = CccRefillingWhouse.GetAsInteger(i, "M_MTRMODEL"),
                    AbcCateg = CccRefillingWhouse.GetAsString(i, "M_CCCABCCATEG"),

                    MtrUnit1 = CccRefillingWhouse.GetAsInteger(i, "M_MTRUNIT1"),
                    MtrUnit2 = CccRefillingWhouse.GetAsInteger(i, "M_MTRUNIT2"),
                    Mu21 = CccRefillingWhouse.GetAsFloat(i, "M_MU21"),
                    RefMtrUnit1 = CccRefillingWhouse.GetAsInteger(i, "REFMTRUNIT1"),
                    RefMtrUnit2 = CccRefillingWhouse.GetAsInteger(i, "REFMTRUNIT2"),
                    CentralQtyRef = CccRefillingWhouse.GetAsFloat(i, "CENTRALQTYREF"),

                    CccCommercialQty = CccRefillingWhouse.GetAsFloat(i, "CCCCOMMERCIALQTY"),
                    RemainLimMin = CccRefillingWhouse.GetAsFloat(i, "REMAINLIMMIN"),
                    RemainLimMax = CccRefillingWhouse.GetAsFloat(i, "REMAINLIMMAX"),
                    RefillingQty = CccRefillingWhouse.GetAsFloat(i, "REFILLINGQTY"),
                    RefillingQtyUsr = CccRefillingWhouse.GetAsFloat(i, "REFILLINGQTYUSR"),
                    FinalPhase = CccRefillingWhouse.GetAsInteger(i, "FINALPHASE"),
                    Remain = CccRefillingWhouse.GetAsFloat(i, "REMAIN"),
                    Choice = CccRefillingWhouse.GetAsInteger(i, "CHOICE") == 1 ? true : false,
                    FindocRef = CccRefillingWhouse.GetAsInteger(i, "CCCFINDOCREF"),
                    Utbl04 = CccRefillingWhouse.GetAsInteger(i, "E_UTBL04"),
                    CccRelDocs = CccRefillingWhouse.GetAsString(i, "CCCRELDOCS")
                });
            }

            for (int i = 0; i < CccRefillingWhouseCen.Count; i++)
            {
                softoneData.Add(new DataFromSoftone
                {
                    IsCentral = CccRefillingWhouseCen.GetAsInteger(i, "ISCENTRAL"),
                    Whouse = CccRefillingWhouseCen.GetAsInteger(i, "WHOUSE"),
                    CentralWhouse = CccRefillingWhouseCen.GetAsInteger(i, "CENTRALWHOUSE"),
                    Mtrl = CccRefillingWhouseCen.GetAsInteger(i, "MTRL"),
                    MtrlCode = CccRefillingWhouseCen.GetAsString(i, "M_CODE"),
                    MtrlName = CccRefillingWhouseCen.GetAsString(i, "M_NAME"),
                    MtrMark = CccRefillingWhouseCen.GetAsInteger(i, "M_MTRMARK"),
                    MtrModel = CccRefillingWhouseCen.GetAsInteger(i, "M_MTRMODEL"),
                    AbcCateg = CccRefillingWhouseCen.GetAsString(i, "M_CCCABCCATEG"),

                    MtrUnit1 = CccRefillingWhouseCen.GetAsInteger(i, "M_MTRUNIT1"),
                    MtrUnit2 = CccRefillingWhouseCen.GetAsInteger(i, "M_MTRUNIT2"),
                    Mu21 = CccRefillingWhouseCen.GetAsFloat(i, "M_MU21"),
                    RefMtrUnit1 = CccRefillingWhouseCen.GetAsInteger(i, "REFMTRUNIT1"),
                    RefMtrUnit2 = CccRefillingWhouseCen.GetAsInteger(i, "REFMTRUNIT2"),
                    CentralQtyRef = CccRefillingWhouseCen.GetAsFloat(i, "CENTRALQTYREF"),

                    CccCommercialQty = CccRefillingWhouseCen.GetAsFloat(i, "CCCCOMMERCIALQTY"),
                    RemainLimMin = CccRefillingWhouseCen.GetAsFloat(i, "REMAINLIMMIN"),
                    RemainLimMax = CccRefillingWhouseCen.GetAsFloat(i, "REMAINLIMMAX"),
                    RefillingQty = CccRefillingWhouseCen.GetAsFloat(i, "REFILLINGQTY"),
                    RefillingQtyUsr = CccRefillingWhouseCen.GetAsFloat(i, "REFILLINGQTYUSR"),
                    FinalPhase = CccRefillingWhouseCen.GetAsInteger(i, "FINALPHASE"),
                    Remain = CccRefillingWhouseCen.GetAsFloat(i, "REMAIN"),
                    Choice = CccRefillingWhouseCen.GetAsInteger(i, "CHOICE") == 1 ? true : false,
                    FindocRef = CccRefillingWhouseCen.GetAsInteger(i, "CCCFINDOCREF"),
                    Utbl04 = CccRefillingWhouseCen.GetAsInteger(i, "E_UTBL04"),
                    CccRelDocs = CccRefillingWhouseCen.GetAsString(i, "CCCRELDOCS")
                });
            }

            if (softoneData.Count > 0)
            {
                refillingForm.PivotDs = softoneData;
                //refillingForm.pivotGridControl1.DataSource = softoneData;
                refillingForm.pivotGridControl1.BestFit();
            }
        }

        private void RefillingCalculations(string findocsStr)
        {
            var CccRefillingH = XModule.GetTable("CCCREFILLINGH");
            var CccRefillingId = Convert.ToInt32(CccRefillingH.Current["CCCREFILLINGH"]);
            var whouseReceiptStore = Convert.ToInt32(CccRefillingH.Current["WHOUSE"]);
            var trndate = Convert.ToDateTime(CccRefillingH.Current["TRNDATE"]);
            var trndateRef = Convert.ToDateTime(CccRefillingH.Current["TRNDATEREF"]);
            var currentdate = trndate.ToString("yyyyMMdd");
            var CccFindocForRef = XModule.GetTable("CCCFINDOCFORREF");

            if (CccFindocForRef.Count > 0)
            {

                var query = $@"SELECT A.WHOUSE,A.MTRL,M.CODE AS MTRLCODE,M.NAME AS MTRLNAME,M.MTRUNIT1,
                        CASE WHEN ISNULL(M.MTRUNIT2,0)=0 THEN M.MTRUNIT1 ELSE M.MTRUNIT2 END AS MTRUNIT2,
                        CASE WHEN ISNULL(M.MTRUNIT2,0)=0 THEN 1 ELSE ISNULL(M.MU21,1) END MU21,
                        ISNULL(A.CCCRECEIPTSTORE,0) AS CENTRALWHOUSE,M.MTRSUP,T.CCCSUPCHAINSCENH,
                        (CASE WHEN A.WHOUSE = 1002 THEN 10 WHEN A.WHOUSE = 1001 THEN 20 WHEN A.WHOUSE = 1003 THEN 30
                        WHEN A.WHOUSE = 1005 THEN 40 WHEN A.WHOUSE = 1008 THEN 50 WHEN A.WHOUSE = 1021 THEN 60
                        WHEN A.WHOUSE = 1011 THEN 70 WHEN A.WHOUSE = 1022 THEN 80 WHEN A.WHOUSE = 1004 THEN 90
                        WHEN A.WHOUSE = 1007 THEN 100 WHEN A.WHOUSE = 1006 THEN 110 WHEN A.WHOUSE = 1014 THEN 120
                        WHEN A.WHOUSE = 1012 THEN 130 WHEN A.WHOUSE = 1023 THEN 140 ELSE 0 END) AS PRIORITIZATION,
                        (ISNULL(OPNWH,0)+ISNULL(TRNWH,0)) AS REMAIN,
                        ISNULL(M.CCCCOMMERCIALQTY,0) AS CCCCOMMERCIALQTY,
                        ISNULL(A.REMAINLIMMIN,0) AS REMAINLIMMIN,ISNULL(A.REMAINLIMMAX,0) AS REMAINLIMMAX,ISNULL(CCCQTYSTEPREPLACEMENT,1) AS CCCQTYSTEPREPLACEMENT,
                        ISNULL(C.ANAMENOMENA,0) AS ANAMENOMENA,ISNULL(D.DESMEYMENA,0) AS DESMEYMENA,

                        L.QTY1 AS QTY1FINDOCS,ISNULL(L.CCCRELDOCS,'') AS CCCRELDOCS

                        FROM 
                        MTRBRNLIMITS A
                        INNER JOIN MTRL M ON M.MTRL=A.MTRL

                        INNER JOIN (
                        /*SELECT ML.MTRL,SUM(ML.QTY1) AS QTY1 FROM FINDOC F INNER JOIN MTRLINES ML ON ML.FINDOC=F.FINDOC WHERE F.FINDOC IN ({findocsStr}) GROUP BY ML.MTRL*/

                        SELECT ML.MTRL,SUM(ML.QTY1) AS QTY1,STUFF((SELECT '|' + CAST(ML2.FINDOC AS varchar) +';'+CAST(ML2.MTRLINES AS varchar) 
                        FROM FINDOC F2 INNER JOIN MTRLINES ML2 ON ML2.FINDOC=F2.FINDOC
                        WHERE F2.FINDOC IN ({findocsStr}) AND ML.MTRL = ML2.MTRL ORDER BY ML2.MTRL FOR XML PATH('')), 1, 1, '') AS CCCRELDOCS
                        FROM FINDOC F INNER JOIN MTRLINES ML ON ML.FINDOC=F.FINDOC
                        WHERE F.FINDOC IN ({findocsStr})
                        GROUP BY ML.MTRL

                        ) L ON L.MTRL=M.MTRL

                        LEFT JOIN BRANCH B ON B.BRANCH=A.BRANCH AND A.COMPANY=B.COMPANY
                        LEFT JOIN (SELECT ML.COMPANY,ML.MTRL,ML.WHOUSE,SUM(ISNULL(ML.QTY1,0)-ISNULL(ML.QTY1COV,0)-ISNULL(ML.QTY1CANC,0)) AS ANAMENOMENA
                        FROM MTRLINES ML LEFT JOIN RESTMODE R ON ML.RESTMODE  = R.RESTMODE AND ML.COMPANY=R.COMPANY
                        WHERE ML.COMPANY={XSupport.ConnectionInfo.CompanyId} AND R.RESTCATEG = 1 GROUP BY ML.COMPANY,ML.MTRL,ML.WHOUSE) AS C ON C.MTRL=A.MTRL AND C.WHOUSE=A.WHOUSE AND C.COMPANY=A.COMPANY
                        LEFT JOIN (SELECT ML.COMPANY,ML.MTRL,ML.WHOUSE,SUM(ISNULL(ML.QTY1,0)-ISNULL(ML.QTY1COV,0)-ISNULL(ML.QTY1CANC,0)) AS DESMEYMENA
                        FROM MTRLINES ML LEFT JOIN RESTMODE R ON ML.RESTMODE  = R.RESTMODE AND ML.COMPANY=R.COMPANY
                        WHERE ML.COMPANY={XSupport.ConnectionInfo.CompanyId} AND R.RESTCATEG = 2 GROUP BY ML.COMPANY,ML.MTRL,ML.WHOUSE) AS D ON D.MTRL=A.MTRL AND D.WHOUSE=A.WHOUSE AND D.COMPANY=A.COMPANY
                        LEFT JOIN (SELECT MB.COMPANY,MB.MTRL ,MB.WHOUSE,SUM(ISNULL(MB.IMPQTY1,0)-ISNULL(MB.EXPQTY1,0)) AS OPNWH
                        FROM MTRBALSHEET MB WHERE MB.COMPANY={XSupport.ConnectionInfo.CompanyId} AND MB.FISCPRD=YEAR('{currentdate}') AND MB.PERIOD<MONTH('{currentdate}') GROUP BY MB.MTRL ,MB.WHOUSE,MB.COMPANY) E ON E.MTRL=A.MTRL AND E.WHOUSE=A.WHOUSE AND E.COMPANY=A.COMPANY
                        LEFT JOIN (SELECT T.COMPANY,T.MTRL ,T.WHOUSE,SUM((TP.FLG01*ISNULL(T.QTY1,0)-TP.FLG04*ISNULL(T.QTY1,0))) AS TRNWH 
                        FROM MTRTRN T JOIN TPRMS TP ON TP.COMPANY=T.COMPANY AND TP.SODTYPE=T.SODTYPE AND TP.TPRMS=T.TPRMS 
                        WHERE T.COMPANY={XSupport.ConnectionInfo.CompanyId} AND T.FISCPRD=YEAR('{currentdate}') AND T.SODTYPE=51 AND T.TRNDATE>=DATEADD(mm, DATEDIFF(mm, 0, '{currentdate}'), 0) 
                        AND T.TRNDATE<='{currentdate}' GROUP BY T.MTRL,T.WHOUSE,T.COMPANY) F ON F.MTRL=A.MTRL AND F.WHOUSE=A.WHOUSE AND F.COMPANY=A.COMPANY
                        LEFT JOIN TRDR T ON T.TRDR=M.MTRSUP
                        WHERE 
                        A.COMPANY ={XSupport.ConnectionInfo.CompanyId}
                        AND M.ISACTIVE=1
                        /*AND (ISNULL(A.CCCRECEIPTSTORE,0) = {whouseReceiptStore} OR A.WHOUSE ={whouseReceiptStore})*/
                        AND A.WHOUSE IN (1001,1002,1003,1004,1005,1006,1007,1008,1011,1012,1014,1021,1022,1023)
                        /*AND ISNULL(A.REMAINLIMMIN,0.0) <> 0.0 AND ISNULL(A.REMAINLIMMAX,0.0) <>0*/
                        AND T.CCCSUPCHAINSCENH IS NOT NULL
                        ORDER BY A.MTRL,A.WHOUSE";

                try
                {
                    var DataForRefilling = new List<MtrlPerWhouseData>();
                    using (XTable ds = XSupport.GetSQLDataSet(query))
                    {
                        if (ds.Count > 0)
                        {
                            var dataList = ds.CreateDataTable(true).AsEnumerable().ToList();
                            var groupedList = dataList.GroupBy(x => Convert.ToInt32(x["MTRL"].ToString())).ToList();
                            foreach (var group in groupedList)
                            {
                                var myItem = new MtrlPerWhouseData
                                {
                                    Mtrl = group.Key,
                                    MtrlCode = group.First()["MTRLCODE"].ToString(),
                                    MtrlName = group.First()["MTRLNAME"].ToString(),
                                    MtrUnit1 = Convert.ToInt32(group.First()["MTRUNIT1"]),
                                    MtrUnit2 = Convert.ToInt32(group.First()["MTRUNIT2"]),
                                    Mu21 = Convert.ToDouble(group.First()["MU21"]),
                                    WhouseData = group.Select(x => new WhouseData
                                    {
                                        Whouse = Convert.ToInt32(x["WHOUSE"]),
                                        CentralWhouse = Convert.ToInt32(x["CENTRALWHOUSE"]),
                                        Prioritization = Convert.ToInt32(x["PRIORITIZATION"]),
                                        Remain = Convert.ToDouble(x["REMAIN"]),
                                        AvailableRemain = (Convert.ToDouble(x["REMAIN"])
                                                        + (Convert.ToDouble(x["ANAMENOMENA"]) > 0 ? Convert.ToDouble(x["ANAMENOMENA"]) : 0.0)
                                                        - (Convert.ToDouble(x["DESMEYMENA"]) > 0 ? Convert.ToDouble(x["DESMEYMENA"]) : 0.0)),
                                        CccCommercialQty = Convert.ToDouble(x["CCCCOMMERCIALQTY"]),
                                        RemainLimMin = Convert.ToDouble(x["REMAINLIMMIN"]),
                                        RemainLimMax = Convert.ToDouble(x["REMAINLIMMAX"]),
                                        CccQtyStepReplacement = Convert.ToDouble(x["CCCQTYSTEPREPLACEMENT"]),
                                        Anamenomena = Convert.ToDouble(x["ANAMENOMENA"]) > 0 ? Convert.ToDouble(x["ANAMENOMENA"]) : 0.0,
                                        Desmeymena = Convert.ToDouble(x["DESMEYMENA"]) > 0 ? Convert.ToDouble(x["DESMEYMENA"]) : 0.0,
                                        NewOrder = 0,
                                        RefillingQty = 0.0,
                                        RefillingQtyUsr = 0.0,
                                        RefMtrUnit1 = Convert.ToInt32(group.First()["MTRUNIT1"]),
                                        RefMtrUnit2 = Convert.ToInt32(group.First()["MTRUNIT2"]),
                                        Qty1Findocs = Convert.ToDouble(x["QTY1FINDOCS"]) > 0 ? Convert.ToDouble(x["QTY1FINDOCS"]) : 0.0,
                                        CccRelDocs = Convert.ToString(x["CCCRELDOCS"])                                       
                                        //Packages = 0.0
                                    }).OrderBy(x => x.Prioritization).ToList()
                                };
                                DataForRefilling.Add(myItem);
                            }

                            if (DataForRefilling.Count > 0)
                            {
                                //loop mtrl data
                                foreach (var data in DataForRefilling)
                                {
                                    var mtrunit2 = data.MtrUnit2;
                                    var mu21 = data.Mu21;

                                    //list of central whouse
                                    //var centralWhouseList = data.WhouseData.Where(w => w.Whouse == w.CentralWhouse).ToList();
                                    var centralWhouses = data.WhouseData.Where(w => w.CentralWhouse > 0).Select(w => w.CentralWhouse).Distinct().ToList();

                                    foreach (var cwh in centralWhouses)
                                    {
                                        //new
                                        var centralWhouseList = data.WhouseData.Where(w => w.Whouse == cwh).ToList();

                                        //loop list of central whouse
                                        foreach (var cwhouse in centralWhouseList)
                                        {
                                            var centralWhouse = cwhouse.Whouse;

                                            //Διαθέσιμο για αναπληρώσεις καταστημάτων
                                            //Ανάθεση ποσότητας κατά είδος για αναπλήρωση από την κεντρική αποθήκη.

                                            //Στον Κανονικό Υπολογισμό
                                            var qtyToRefillold = cwhouse.Remain - cwhouse.RemainLimMin > 0.0 ? cwhouse.Remain - cwhouse.RemainLimMin : 0.0;
                                            //Στον Υπολογισμό βάση παραστατικού
                                            var qtyToRefill = (cwhouse.Qty1Findocs - cwhouse.Desmeymena) > 0.0 ? (cwhouse.Qty1Findocs - cwhouse.Desmeymena)  : 0.0;

                                            cwhouse.CentralQtyRef = qtyToRefill;
                                            cwhouse.CentralPackageRef = Math.Round((qtyToRefill / mu21), MidpointRounding.AwayFromZero);

                                            if (qtyToRefill > 0.0)
                                            {
                                                var branchWhouseList = new MtrlPerWhouseData
                                                {
                                                    Mtrl = data.Mtrl,
                                                    MtrlCode = data.MtrlCode,
                                                    MtrlName = data.MtrlName,
                                                    MtrUnit2 = data.MtrUnit2,
                                                    Mu21 = data.Mu21,
                                                    //old
                                                    //WhouseData = data.WhouseData.Where(x => x.Whouse != x.CentralWhouse && x.CentralWhouse == centralWhouse && !centralWhouses.Contains(x.Whouse)).OrderBy(x => x.Prioritization).ToList()
                                                    WhouseData = data.WhouseData.Where(x => x.Whouse != x.CentralWhouse 
                                                    && x.CentralWhouse == centralWhouse 
                                                    && !centralWhouses.Contains(x.Whouse)
                                                    ).OrderBy(x => x.Prioritization).ToList()
                                                };

                                                //Phase 1 //Ιεραρχική αναπλήρωση καταστημάτων έως την Εμπορική Ποσότητα
                                                foreach (var whouse in branchWhouseList.WhouseData)
                                                {
                                                    whouse.Ph1Qty = whouse.Remain - whouse.CccCommercialQty <= 0 ? (whouse.Remain - whouse.CccCommercialQty) * (-1) : 0.0;
                                                    //whouse.Ph1Qty = (whouse.Qty1Findocs - whouse.Desmeymena) - whouse.CccCommercialQty <= 0 ? ((whouse.Qty1Findocs - whouse.Desmeymena) - whouse.CccCommercialQty) * (-1) : 0.0;
                                                }
                                                var sumQtyPhase1 = branchWhouseList.WhouseData.Sum(x => x.Ph1Qty);

                                                //έλεγχος διαθεσιμότητας στην κεντική αποθήκη 
                                                if (cwhouse.CentralQtyRef - sumQtyPhase1 > 0)
                                                {
                                                    //Phase 2 //Ιεραρχική αναπλήρωση καταστημάτων έως το επίπεδο αποθέματος Min.
                                                    foreach (var whouse in branchWhouseList.WhouseData)
                                                    {
                                                        whouse.Ph2Qty = whouse.Remain - whouse.RemainLimMin <= 0 ? (whouse.Remain - whouse.RemainLimMin) * (-1) : 0.0;
                                                        //whouse.Ph2Qty = (whouse.Qty1Findocs - whouse.Desmeymena) - whouse.RemainLimMin <= 0 ? ((whouse.Qty1Findocs - whouse.Desmeymena) - whouse.RemainLimMin) * (-1) : 0.0;
                                                    }
                                                    var sumQtyPhase2 = branchWhouseList.WhouseData.Sum(x => x.Ph2Qty);

                                                    if (cwhouse.CentralQtyRef - sumQtyPhase2 > 0)
                                                    {
                                                        //Phase 3 //Αναλογική αναπλήρωση καταστημάτων έως το επίπεδο αποθέματος Max.
                                                        foreach (var whouse in branchWhouseList.WhouseData)
                                                        {
                                                            whouse.Ph3Qty = whouse.Remain - whouse.RemainLimMax <= 0 ? (whouse.Remain - whouse.RemainLimMax) * (-1) : 0.0;
                                                            //whouse.Ph3Qty = (whouse.Qty1Findocs - whouse.Desmeymena) - whouse.RemainLimMax <= 0 ? ((whouse.Qty1Findocs - whouse.Desmeymena) - whouse.RemainLimMax) * (-1) : 0.0;
                                                        }
                                                        var sumQtyPhase3 = branchWhouseList.WhouseData.Sum(x => x.Ph3Qty);

                                                        if ((cwhouse.CentralQtyRef - sumQtyPhase3 > 0))
                                                        {
                                                            foreach (var whouse in branchWhouseList.WhouseData)
                                                            {
                                                                whouse.Ph3QtyFin = whouse.Ph3Qty;
                                                                whouse.RefillingQty = whouse.Ph3QtyFin;
                                                                whouse.RefillingQtyUsr = whouse.Ph3QtyFin;
                                                                whouse.FinalPhase = 3;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            foreach (var whouse in branchWhouseList.WhouseData)
                                                            {
                                                                whouse.Ph3Step1 = whouse.Ph3Qty * whouse.Prioritization;
                                                            }
                                                            var sumStep1 = branchWhouseList.WhouseData.Sum(x => x.Ph3Step1);
                                                            var syntelestis = (cwhouse.CentralQtyRef - sumQtyPhase3) / sumStep1;

                                                            foreach (var whouse in branchWhouseList.WhouseData)
                                                            {
                                                                whouse.Ph3Step2 = whouse.Ph3Step1 * syntelestis * (-1);
                                                                whouse.Ph3Step3 = Math.Round(whouse.Ph3Step2, MidpointRounding.AwayFromZero);
                                                                whouse.Ph3Step4 = whouse.Ph3Qty - whouse.Ph3Step3;
                                                                whouse.Ph3QtyFin = whouse.Ph3Step4;
                                                                whouse.RefillingQty = whouse.Ph3QtyFin;
                                                                whouse.RefillingQtyUsr = whouse.Ph3QtyFin;
                                                                whouse.FinalPhase = 3;
                                                            }
                                                        }

                                                        //Phase 4 //Στρογγυλοποίηση της ποσότητας βάση της συσκευασίας διακινήσεων 
                                                        if (mtrunit2 == 107 || mtrunit2 == 108) //Κιβώτια ή Πακέτο
                                                        {
                                                            foreach (var whouse in branchWhouseList.WhouseData)
                                                            {
                                                                whouse.Ph4Qty = Math.Round((whouse.Ph3QtyFin / mu21), MidpointRounding.AwayFromZero);
                                                                whouse.RefillingQtyUsr = whouse.Ph4Qty;
                                                                whouse.FinalPhase = 4;
                                                            }

                                                            ///ELEGXOS SOS POSOTITON META THN FASI 4
                                                            //var sumPackages = branchWhouseList.WhouseData.Where(x => x.Whouse != x.CentralWhouse && x.CentralWhouse>0).Sum(y => y.RefillingQty);
                                                            var sumPackages = branchWhouseList.WhouseData.Where(x => x.Whouse != x.CentralWhouse && x.CentralWhouse > 0).Sum(y => y.RefillingQtyUsr);
                                                            var centralSumPackages = cwhouse.CentralPackageRef;
                                                            if (sumPackages > centralSumPackages)
                                                            {
                                                                var qtyFix = centralSumPackages - sumPackages;

                                                                var fixBranchWhouseList = new MtrlPerWhouseData
                                                                {
                                                                    Mtrl = branchWhouseList.Mtrl,
                                                                    MtrlCode = branchWhouseList.MtrlCode,
                                                                    MtrlName = branchWhouseList.MtrlName,
                                                                    MtrUnit2 = branchWhouseList.MtrUnit2,
                                                                    Mu21 = branchWhouseList.Mu21,
                                                                    WhouseData = branchWhouseList.WhouseData.OrderByDescending(x => x.Ph4Qty).ToList()
                                                                };

                                                                foreach (var fix in fixBranchWhouseList.WhouseData)
                                                                {
                                                                    if (qtyFix != 0)
                                                                    {
                                                                        //fix.RefillingQty = fix.RefillingQty + qtyFix;
                                                                        fix.RefillingQtyUsr = fix.RefillingQtyUsr + qtyFix;
                                                                        qtyFix = qtyFix + 1;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            foreach (var whouse in branchWhouseList.WhouseData)
                                                            {
                                                                whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var remainForRefilling = cwhouse.CentralQtyRef;
                                                        var totalSum = 0.0;
                                                        var endsum = 0;
                                                        foreach (var whouse in branchWhouseList.WhouseData)
                                                        {
                                                            totalSum = +totalSum + whouse.Ph2Qty;
                                                            if (remainForRefilling - totalSum > 0)
                                                            {
                                                                var newPh2Qty = whouse.Ph2Qty;
                                                                whouse.Ph2Qty = newPh2Qty;
                                                                whouse.RefillingQty = newPh2Qty;
                                                                whouse.RefillingQtyUsr = newPh2Qty;
                                                                whouse.FinalPhase = 2;
                                                                whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                            }
                                                            else
                                                            {
                                                                if (endsum == 0)
                                                                {
                                                                    var newPh2Qty = totalSum - remainForRefilling;
                                                                    whouse.Ph2Qty = newPh2Qty;
                                                                    whouse.RefillingQty = newPh2Qty;
                                                                    whouse.RefillingQtyUsr = newPh2Qty;
                                                                    whouse.FinalPhase = 2;
                                                                    endsum = 1;
                                                                    whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                                }
                                                                else
                                                                {
                                                                    whouse.Ph2Qty = 0.0;
                                                                    whouse.RefillingQty = 0.0;
                                                                    whouse.RefillingQtyUsr = 0.0;
                                                                    whouse.FinalPhase = 2;
                                                                    whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    var remainForRefilling = cwhouse.CentralQtyRef;
                                                    var totalSum = 0.0;
                                                    var endsum = 0;
                                                    foreach (var whouse in branchWhouseList.WhouseData)
                                                    {
                                                        totalSum = +totalSum + whouse.Ph1Qty;
                                                        if (remainForRefilling - totalSum > 0)
                                                        {
                                                            var newPh1Qty = whouse.Ph1Qty;
                                                            whouse.Ph1Qty = newPh1Qty;
                                                            whouse.RefillingQty = newPh1Qty;
                                                            whouse.RefillingQtyUsr = newPh1Qty;
                                                            whouse.FinalPhase = 1;
                                                            whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                        }
                                                        else
                                                        {
                                                            if (endsum == 0)
                                                            {
                                                                var newPh1Qty = totalSum - remainForRefilling;
                                                                whouse.Ph1Qty = newPh1Qty;
                                                                whouse.RefillingQty = newPh1Qty;
                                                                whouse.RefillingQtyUsr = newPh1Qty;
                                                                whouse.FinalPhase = 1;
                                                                whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                                endsum = 1;
                                                            }
                                                            else
                                                            {
                                                                whouse.Ph1Qty = 0.0;
                                                                whouse.RefillingQty = 0.0;
                                                                whouse.RefillingQtyUsr = 0.0;
                                                                whouse.FinalPhase = 1;
                                                                whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                            }
                                                        }
                                                    }
                                                }

                                                //Αναπλήρωση κεντρικής αποθήκης
                                                //Μετά τον έλεγχο για αναπλήρωση των καταστημάτων πραγματοποιούμε έλεγχο για την αναπλήρωση της κεντρικής αποθήκης , 
                                                //με την υπόλοιπη ποσότητα (Όπως έχει διαμορφωθεί στην φάση 3 ή στην φάση 4 αν υπάρχει) που έχει απομείνει μετά την αναπλήρωση των καταστημάτων.
                                                //Ακολουθούμε την ίδια διαδικασία με τα καταστήματα.

                                                var sumRefillingQty = 0.0;
                                                var finalPhase = branchWhouseList.WhouseData.Select(x => x.FinalPhase).Distinct().ToList().FirstOrDefault();
                                                if (finalPhase == 4)
                                                {
                                                    sumRefillingQty = branchWhouseList.WhouseData.Sum(x => (x.RefillingQtyUsr * mu21));
                                                }
                                                else
                                                {
                                                    sumRefillingQty = branchWhouseList.WhouseData.Sum(x => x.RefillingQty);
                                                }

                                                var newqtyToRefill = cwhouse.CentralQtyRef - sumRefillingQty;
                                                cwhouse.CentralQtyRef = newqtyToRefill;
                                                cwhouse.CentralPackageRef = Math.Round((newqtyToRefill / mu21), MidpointRounding.AwayFromZero);
                                                if (newqtyToRefill > 0)
                                                {
                                                    var newbranchWhouseList = new MtrlPerWhouseData
                                                    {
                                                        Mtrl = data.Mtrl,
                                                        MtrlCode = data.MtrlCode,
                                                        MtrlName = data.MtrlName,
                                                        MtrUnit2 = data.MtrUnit2,
                                                        Mu21 = data.Mu21,
                                                        WhouseData = data.WhouseData.Where(x => x.Whouse != x.CentralWhouse && x.CentralWhouse == centralWhouse && centralWhouses.Contains(x.Whouse)).OrderBy(x => x.Prioritization).ToList()
                                                    };
                                                    //Phase 1 //Ιεραρχική αναπλήρωση καταστημάτων έως την Εμπορική Ποσότητα
                                                    foreach (var whouse in newbranchWhouseList.WhouseData)
                                                    {
                                                        whouse.Ph1Qty = whouse.Remain - whouse.CccCommercialQty <= 0 ? (whouse.Remain - whouse.CccCommercialQty) * (-1) : 0.0;
                                                    }
                                                    var newsumQtyPhase1 = newbranchWhouseList.WhouseData.Sum(x => x.Ph1Qty);
                                                    //έλεγχος διαθεσιμότητας στην κεντική αποθήκη 
                                                    if (cwhouse.CentralQtyRef - sumQtyPhase1 > 0)
                                                    {
                                                        //Phase 2 //Ιεραρχική αναπλήρωση καταστημάτων έως το επίπεδο αποθέματος Min.
                                                        foreach (var whouse in newbranchWhouseList.WhouseData)
                                                        {
                                                            whouse.Ph2Qty = whouse.Remain - whouse.RemainLimMin <= 0 ? (whouse.Remain - whouse.RemainLimMin) * (-1) : 0.0;
                                                        }
                                                        var sumQtyPhase2 = newbranchWhouseList.WhouseData.Sum(x => x.Ph2Qty);

                                                        if (cwhouse.CentralQtyRef - sumQtyPhase2 > 0)
                                                        {
                                                            //Phase 3 //Αναλογική αναπλήρωση καταστημάτων έως το επίπεδο αποθέματος Max.
                                                            foreach (var whouse in newbranchWhouseList.WhouseData)
                                                            {
                                                                whouse.Ph3Qty = whouse.Remain - whouse.RemainLimMax <= 0 ? (whouse.Remain - whouse.RemainLimMax) * (-1) : 0.0;
                                                            }
                                                            var sumQtyPhase3 = newbranchWhouseList.WhouseData.Sum(x => x.Ph3Qty);

                                                            if ((cwhouse.CentralQtyRef - sumQtyPhase3 > 0))
                                                            {
                                                                foreach (var whouse in newbranchWhouseList.WhouseData)
                                                                {
                                                                    whouse.Ph3QtyFin = whouse.Ph3Qty;
                                                                    whouse.RefillingQty = whouse.Ph3QtyFin;
                                                                    whouse.RefillingQtyUsr = whouse.Ph3QtyFin;
                                                                    whouse.FinalPhase = 3;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                foreach (var whouse in newbranchWhouseList.WhouseData)
                                                                {
                                                                    whouse.Ph3Step1 = whouse.Ph3Qty * whouse.Prioritization;
                                                                }
                                                                var sumStep1 = newbranchWhouseList.WhouseData.Sum(x => x.Ph3Step1);
                                                                var syntelestis = (cwhouse.CentralQtyRef - sumQtyPhase3) / sumStep1;

                                                                foreach (var whouse in newbranchWhouseList.WhouseData)
                                                                {
                                                                    whouse.Ph3Step2 = whouse.Ph3Step1 * syntelestis * (-1);
                                                                    whouse.Ph3Step3 = Math.Round(whouse.Ph3Step2, MidpointRounding.AwayFromZero);
                                                                    whouse.Ph3Step4 = whouse.Ph3Qty - whouse.Ph3Step3;
                                                                    whouse.Ph3QtyFin = whouse.Ph3Step4;
                                                                    whouse.RefillingQty = whouse.Ph3QtyFin;
                                                                    whouse.RefillingQtyUsr = whouse.Ph3QtyFin;
                                                                    whouse.FinalPhase = 3;
                                                                }
                                                            }

                                                            //Phase 4 //Στρογγυλοποίηση της ποσότητας βάση της συσκευασίας διακινήσεων 
                                                            if (mtrunit2 == 107 || mtrunit2 == 108) //Κιβώτια ή Πακέτο
                                                            {
                                                                foreach (var whouse in newbranchWhouseList.WhouseData)
                                                                {
                                                                    whouse.Ph4Qty = Math.Round((whouse.Ph3QtyFin / mu21), MidpointRounding.AwayFromZero);
                                                                    whouse.RefillingQtyUsr = whouse.Ph4Qty;
                                                                    whouse.FinalPhase = 4;
                                                                }

                                                                ///ELEGXOS SOS POSOTITON META THN FASI 4
                                                                //var sumPackages = branchWhouseList.WhouseData.Where(x => x.Whouse != x.CentralWhouse && x.CentralWhouse>0).Sum(y => y.RefillingQty);
                                                                var sumPackages = newbranchWhouseList.WhouseData.Where(x => x.Whouse != x.CentralWhouse && x.CentralWhouse > 0).Sum(y => y.RefillingQtyUsr);
                                                                var centralSumPackages = cwhouse.CentralPackageRef;
                                                                if (sumPackages > centralSumPackages)
                                                                {
                                                                    var qtyFix = centralSumPackages - sumPackages;

                                                                    var fixBranchWhouseList = new MtrlPerWhouseData
                                                                    {
                                                                        Mtrl = newbranchWhouseList.Mtrl,
                                                                        MtrlCode = newbranchWhouseList.MtrlCode,
                                                                        MtrlName = newbranchWhouseList.MtrlName,
                                                                        MtrUnit2 = newbranchWhouseList.MtrUnit2,
                                                                        Mu21 = newbranchWhouseList.Mu21,
                                                                        WhouseData = newbranchWhouseList.WhouseData.OrderByDescending(x => x.Ph4Qty).ToList()
                                                                    };

                                                                    foreach (var fix in fixBranchWhouseList.WhouseData)
                                                                    {
                                                                        if (qtyFix != 0)
                                                                        {
                                                                            fix.RefillingQtyUsr = fix.RefillingQtyUsr + qtyFix;
                                                                            qtyFix = qtyFix + 1;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                foreach (var whouse in newbranchWhouseList.WhouseData)
                                                                {
                                                                    whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            var remainForRefilling = cwhouse.CentralQtyRef;
                                                            var totalSum = 0.0;
                                                            var endsum = 0;
                                                            foreach (var whouse in newbranchWhouseList.WhouseData)
                                                            {
                                                                totalSum = +totalSum + whouse.Ph2Qty;
                                                                if (remainForRefilling - totalSum > 0)
                                                                {
                                                                    var newPh2Qty = whouse.Ph2Qty;
                                                                    whouse.Ph2Qty = newPh2Qty;
                                                                    whouse.RefillingQty = newPh2Qty;
                                                                    whouse.RefillingQtyUsr = newPh2Qty;
                                                                    whouse.FinalPhase = 2;
                                                                    whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                                }
                                                                else
                                                                {
                                                                    if (endsum == 0)
                                                                    {
                                                                        var newPh2Qty = totalSum - remainForRefilling;
                                                                        whouse.Ph2Qty = newPh2Qty;
                                                                        whouse.RefillingQty = newPh2Qty;
                                                                        whouse.RefillingQtyUsr = newPh2Qty;
                                                                        whouse.FinalPhase = 2;
                                                                        endsum = 1;
                                                                        whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                                    }
                                                                    else
                                                                    {
                                                                        whouse.Ph2Qty = 0.0;
                                                                        whouse.RefillingQty = 0.0;
                                                                        whouse.RefillingQtyUsr = 0.0;
                                                                        whouse.FinalPhase = 2;
                                                                        whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var remainForRefilling = cwhouse.CentralQtyRef;
                                                        var totalSum = 0.0;
                                                        var endsum = 0;
                                                        foreach (var whouse in newbranchWhouseList.WhouseData)
                                                        {
                                                            totalSum = +totalSum + whouse.Ph1Qty;
                                                            if (remainForRefilling - totalSum > 0)
                                                            {
                                                                var newPh1Qty = whouse.Ph1Qty;
                                                                whouse.Ph1Qty = newPh1Qty;
                                                                whouse.RefillingQty = newPh1Qty;
                                                                whouse.RefillingQtyUsr = newPh1Qty;
                                                                whouse.FinalPhase = 1;
                                                                whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                            }
                                                            else
                                                            {
                                                                if (endsum == 0)
                                                                {
                                                                    var newPh1Qty = totalSum - remainForRefilling;
                                                                    whouse.Ph1Qty = newPh1Qty;
                                                                    whouse.RefillingQty = newPh1Qty;
                                                                    whouse.RefillingQtyUsr = newPh1Qty;
                                                                    whouse.FinalPhase = 1;
                                                                    whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                                    endsum = 1;
                                                                }
                                                                else
                                                                {
                                                                    whouse.Ph1Qty = 0.0;
                                                                    whouse.RefillingQty = 0.0;
                                                                    whouse.RefillingQtyUsr = 0.0;
                                                                    whouse.FinalPhase = 1;
                                                                    whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //Αναπλήρωση κεντρικής αποθήκης  
                                            }
                                        }
                                    }
                                }

                                //apotelesmata
                                try
                                {
                                    foreach (var data in DataForRefilling)
                                    {
                                        var centralWhouses = data.WhouseData.Where(w => w.CentralWhouse > 0).Select(w => w.CentralWhouse).Distinct().ToList();
                                        var sumRefillingQty = data.WhouseData.Sum(q => q.RefillingQtyUsr);
                                        //θα πρέπει να υπάρχει ποσότητα προς αναπλήρωση για να πραγματοποιθεί η καταχώρηση
                                        if (sumRefillingQty > 0)
                                        {
                                            using (var RefillingMtrl = XModule.GetTable("CCCREFILLINGMTRL"))
                                            {
                                                RefillingMtrl.Current.Append();
                                                RefillingMtrl.Current["MTRL"] = data.Mtrl;
                                                using (var RefillingWhouse = XModule.GetTable("CCCREFILLINGWHOUSE"))
                                                {
                                                    foreach (var whouse in data.WhouseData)
                                                    {
                                                        //if (whouse.Whouse != whouse.CentralWhouse)
                                                        if (whouse.Whouse != whouse.CentralWhouse && whouse.CentralWhouse > 0 && !centralWhouses.Contains(whouse.Whouse))
                                                        {
                                                            RefillingWhouse.Current.Append();
                                                            RefillingWhouse.Current["ISCENTRAL"] = 0;
                                                            //RefillingWhouse.Current["CENTRALQTYREF"] = 0;                                                       
                                                            RefillingWhouse.Current["MTRL"] = data.Mtrl;
                                                            RefillingWhouse.Current["WHOUSE"] = whouse.Whouse;
                                                            RefillingWhouse.Current["REMAIN"] = whouse.Remain;
                                                            RefillingWhouse.Current["CCCCOMMERCIALQTY"] = whouse.CccCommercialQty;
                                                            RefillingWhouse.Current["REMAINLIMMIN"] = whouse.RemainLimMin;
                                                            RefillingWhouse.Current["REMAINLIMMAX"] = whouse.RemainLimMax;
                                                            //RefillingWhouse.Current["NOMOVES"] = 0;
                                                            RefillingWhouse.Current["REFILLINGQTY"] = whouse.RefillingQty;
                                                            RefillingWhouse.Current["REFILLINGQTYUSR"] = whouse.RefillingQtyUsr;
                                                            RefillingWhouse.Current["FINALPHASE"] = whouse.FinalPhase;
                                                            RefillingWhouse.Current["CENTRALWHOUSE"] = whouse.CentralWhouse;
                                                            RefillingWhouse.Current["REFMTRUNIT1"] = whouse.RefMtrUnit1;
                                                            RefillingWhouse.Current["REFMTRUNIT2"] = whouse.RefMtrUnit2;
                                                            RefillingWhouse.Current["CCCRELDOCS"] = whouse.CccRelDocs;
                                                            RefillingWhouse.Current["CHOICE"] = 0;
                                                            RefillingWhouse.Current.Post();
                                                        }
                                                    }
                                                }

                                                foreach (var cwh in centralWhouses)
                                                {
                                                    using (var RefillingWhouse = XModule.GetTable("CCCREFILLINGWHOUSECEN"))
                                                    {
                                                        foreach (var whouse in data.WhouseData)
                                                        {
                                                            //if (whouse.Whouse == whouse.CentralWhouse)
                                                            if (whouse.Whouse == cwh && whouse.CentralWhouse > 0 && centralWhouses.Contains(whouse.Whouse))
                                                            {
                                                                RefillingWhouse.Current.Append();
                                                                RefillingWhouse.Current["ISCENTRAL"] = 1;
                                                                RefillingWhouse.Current["CENTRALQTYREF"] = whouse.CentralQtyRef;
                                                                RefillingWhouse.Current["MTRL"] = data.Mtrl;
                                                                RefillingWhouse.Current["WHOUSE"] = whouse.Whouse;
                                                                RefillingWhouse.Current["REMAIN"] = whouse.Remain;
                                                                RefillingWhouse.Current["CCCCOMMERCIALQTY"] = whouse.CccCommercialQty;
                                                                RefillingWhouse.Current["REMAINLIMMIN"] = whouse.RemainLimMin;
                                                                RefillingWhouse.Current["REMAINLIMMAX"] = whouse.RemainLimMax;
                                                                //RefillingWhouse.Current["NOMOVES"] = 0;
                                                                RefillingWhouse.Current["REFILLINGQTY"] = whouse.RefillingQty;
                                                                RefillingWhouse.Current["REFILLINGQTYUSR"] = whouse.RefillingQtyUsr;
                                                                RefillingWhouse.Current["FINALPHASE"] = whouse.FinalPhase;
                                                                RefillingWhouse.Current["CENTRALWHOUSE"] = whouse.CentralWhouse;
                                                                RefillingWhouse.Current["REFMTRUNIT1"] = whouse.RefMtrUnit1;
                                                                RefillingWhouse.Current["REFMTRUNIT2"] = whouse.RefMtrUnit2;
                                                                RefillingWhouse.Current["CCCRELDOCS"] = whouse.CccRelDocs;
                                                                RefillingWhouse.Current["CHOICE"] = 0;
                                                                RefillingWhouse.Current.Post();
                                                            }
                                                        }
                                                    }
                                                }
                                                RefillingMtrl.Current.Post();
                                            }
                                        }
                                    }

                                    XSupport.Warning("Ολοκληρώθηκε ο Υπολογισμός Αναπλήρωσεων.");
                                    XModule.FocusField("CCCREFILLINGH.NAME");
                                    XModule.Exec("Button:Save");

                                }
                                catch (Exception ex)
                                {
                                    XSupport.Warning(ex.Message);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    XSupport.Warning(ex.Message);
                }
            }
            else
            {
                XSupport.Warning("Για να πραγματοποιηθεί ο υπολογισμός αναπλήρωσης πρέπει να έχετε επιλέξει τουλάχιστον ένα παραστατικό προς αναπλήρωση.");
            }
        }
    }
}
