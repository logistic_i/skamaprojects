﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefillingObj
{
    class MtrlPerWhouseData
    {
        public int Mtrl { get; set; }
        public string MtrlCode { get; set; }
        public string MtrlName { get; set; }
        public int MtrUnit1 { get; set; }
        public int MtrUnit2 { get; set; }
        public double Mu21 { get; set; }
        public List<WhouseData> WhouseData { get; set; }
    }
}
