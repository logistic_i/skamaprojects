﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softone;

namespace RefillingObj
{
    [WorksOn("CCCSUPSCENARIOS")]
    public class SupScenarios:TXCode
    {
        private bool scenarioChanged = false;


        public override void Initialize()
        {
            base.Initialize();
        }

        public override void BeforePost()
        {
            scenarioChanged = false;
            var SupChainScenH = XModule.GetTable("CCCSUPCHAINSCENH");
            var SupChainScenHId = Convert.ToInt32(SupChainScenH.Current["CCCSUPCHAINSCENH"]);
            var SupChainScenD = XModule.GetTable("CCCSUPCHAINSCEND");
            XModule.FocusField("CCCSUPCHAINSCENH.CODE");

            if (SupChainScenHId > 0)
            {
                if(SupChainScenD.Count>0)
                {
                    for (int i = 0; i < SupChainScenD.Count; i++)
                    {
                        SupChainScenD.Current.Edit(i);
                        SupChainScenD.Current.Post();
                    }
                    var repeats = SupChainScenD.Count;
                    for (int i=0; i <repeats; i++)
                    {
                        var buyerWh = SupChainScenD.GetAsInteger(i, "BUYERWH");
                        var supplierWh = SupChainScenD.GetAsInteger(i, "SUPPLIERWH");
                        var checkquery = $@"SELECT isnull(SUPPLIERWH,0) as SUPPLIERWH FROM CCCSUPCHAINSCEND WHERE CCCSUPCHAINSCENH ={SupChainScenHId} AND BUYERWH ={buyerWh}";
                        using (XTable checkds = XSupport.GetSQLDataSet(checkquery))
                        {
                            if (checkds.Count > 0)
                            {
                                var dsSupplierWh = checkds.GetAsInteger(0, "SUPPLIERWH");
                                if (dsSupplierWh != supplierWh)
                                {
                                    scenarioChanged = true;
                                }
                            }
                            else
                            {
                                scenarioChanged = true;
                            }
                        }
                    }
                }
            }
            else
            {
                scenarioChanged = true;
            }
            base.BeforePost();
        }

        public override void AfterPost()
        {
            var SupChainScenH = XModule.GetTable("CCCSUPCHAINSCENH");
            var SupChainScenHId = Convert.ToInt32(SupChainScenH.Current["CCCSUPCHAINSCENH"]) > 0 ? Convert.ToInt32(SupChainScenH.Current["CCCSUPCHAINSCENH"]) : XModule.ObjID();
            var ScenarioName = Convert.ToString(SupChainScenH.Current["NAME"] != DBNull.Value ? SupChainScenH.Current["NAME"] :"");

            var checkquery = $@"SELECT T.CCCSUPCHAINSCENH,T.TRDR,T.CODE,T.NAME ,M.MTRL 
                                FROM TRDR T INNER JOIN MTRL M ON M.MTRSUP=T.TRDR WHERE T.CCCSUPCHAINSCENH={SupChainScenHId}
                                UNION ALL
                                SELECT M.CCCSUPCHAINSCENH,T.TRDR,T.CODE,T.NAME ,M.MTRL FROM MTRL M
                                INNER JOIN TRDR T ON M.MTRSUP=T.TRDR WHERE M.CCCSUPCHAINSCENH={SupChainScenHId} AND M.CCCSUPCHAINSCENH<>T.CCCSUPCHAINSCENH";
            using (XTable checkds = XSupport.GetSQLDataSet(checkquery))
            {
                if (checkds.Count > 0 && scenarioChanged == true)
                {
                    var ans = 0;
                    ans = XSupport.AskYesNoCancel("Αλλαγή Αποθήκης Παραλαβής Αγορών", $"Με την αλλαγή του σεναρίου, θέλετε να πραγματοποιήσετε αλλαγή στην Αποθήκη Παραλαβής Αγορών των ειδών που έχουν βασικό προμηθευτή, ο οποίος έχει σενάριο το {ScenarioName}?");
                    if (ans == 6) // 6=Yes, 7=No, 2=Cancel
                    {
                        XSupport.ExecuteSQL($@"UPDATE Table_A SET Table_A.CCCRECEIPTSTORE = Table_B.SUPPLIERWH
                                        FROM MTRBRNLIMITS AS Table_A
                                        INNER JOIN (SELECT T.TRDR,T.CODE AS TCODE,T.NAME AS TNAME,T.CCCSUPCHAINSCENH
                                        ,M.MTRL,M.CODE AS MTRLCODE,M.NAME AS MTRLNAME,S1.BUYERWH,S1.SUPPLIERWH,M1.CCCRECEIPTSTORE
                                        FROM TRDR T
                                        INNER JOIN MTRL M ON M.MTRSUP =T.TRDR INNER JOIN CCCSUPCHAINSCEND S1 ON S1.CCCSUPCHAINSCENH=T.CCCSUPCHAINSCENH
                                        INNER JOIN MTRBRNLIMITS M1 ON M1.MTRL=M.MTRL AND M1.WHOUSE=S1.BUYERWH
                                        WHERE 
                                        /*M.MTRL=2349 AND */
                                        (M.CCCSUPCHAINSCENH IS NULL OR M.CCCSUPCHAINSCENH=T.CCCSUPCHAINSCENH)
                                        AND T.CCCSUPCHAINSCENH={SupChainScenHId}) AS Table_B ON Table_A.MTRL = Table_B.MTRL AND Table_A.WHOUSE= Table_B.BUYERWH");
                    }

                    var checkschen = $@"SELECT M.MTRL,M.CCCSUPCHAINSCENH FROM MTRL M LEFT JOIN TRDR T ON M.MTRSUP =T.TRDR
                                        WHERE M.CCCSUPCHAINSCENH = {SupChainScenHId} AND (M.CCCSUPCHAINSCENH IS NOT NULL AND M.CCCSUPCHAINSCENH<>T.CCCSUPCHAINSCENH)";

                    using (XTable checkschends = XSupport.GetSQLDataSet(checkschen))
                    {
                        if (checkschends.Count > 0)
                        {
                            ans = 0;
                            ans = XSupport.AskYesNoCancel("Αλλαγή Αποθήκης Παραλαβής Αγορών", $"Υπάρχουν είδη με σενάριο το {ScenarioName}, το οποίο είναι διαφορετικό από το σενάριο του βασικού προμηθευτή τους, θέλετε να πραγματοποιήσετε αλλαγή στην Αποθήκη Παραλαβής Αγορών?");
                            if (ans == 6) // 6=Yes, 7=No, 2=Cancel
                            {
                                XSupport.ExecuteSQL($@"UPDATE Table_A SET Table_A.CCCRECEIPTSTORE = Table_B.SUPPLIERWH
                                        FROM MTRBRNLIMITS AS Table_A
                                        INNER JOIN (
                                        SELECT M.MTRL,M.CODE,M.NAME,M.CCCSUPCHAINSCENH,M1.CCCRECEIPTSTORE,S1.BUYERWH,S1.SUPPLIERWH
                                        FROM MTRL M 
                                        LEFT JOIN CCCSUPCHAINSCEND S1 ON S1.CCCSUPCHAINSCENH=M.CCCSUPCHAINSCENH
                                        LEFT JOIN MTRBRNLIMITS M1 ON M1.MTRL=M.MTRL AND M1.WHOUSE=S1.BUYERWH
                                        LEFT JOIN TRDR T ON M.MTRSUP =T.TRDR 
                                        WHERE 
                                        M.CCCSUPCHAINSCENH = {SupChainScenHId}
                                        AND (M.CCCSUPCHAINSCENH IS NOT NULL AND M.CCCSUPCHAINSCENH<>T.CCCSUPCHAINSCENH)
                                        ) AS Table_B ON Table_A.MTRL = Table_B.MTRL AND Table_A.WHOUSE= Table_B.BUYERWH");
                            }
                        }
                    }
                }
            }
            base.AfterPost();
        }
    }
}
