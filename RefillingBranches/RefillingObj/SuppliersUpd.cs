﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softone;

namespace RefillingObj
{
    [WorksOn("SUPPLIER")]
    public class SuppliersUpd : TXCode
    {
        private bool scenarioChanged = false;
        private int dbScenario = 0;

        public override void BeforePost()
        {
            scenarioChanged = false;
            var Supplier = XModule.GetTable("SUPPLIER");
            var Trdr = Convert.ToInt32(Supplier.Current["TRDR"]);
            var SupChainScenH = Convert.ToInt32(Supplier.Current["CCCSUPCHAINSCENH"]!= DBNull.Value ? Supplier.Current["CCCSUPCHAINSCENH"]:0);
            if (Trdr > 0)
            {
                var checkquery = $@"SELECT isnull(CCCSUPCHAINSCENH,0) as CCCSUPCHAINSCENH FROM TRDR WHERE TRDR ={Trdr}";
                using (XTable checkds = XSupport.GetSQLDataSet(checkquery))
                {
                    if (checkds.Count > 0)
                    {
                        dbScenario = checkds.GetAsInteger(0, "CCCSUPCHAINSCENH");
                        if (dbScenario != SupChainScenH)
                        {
                            scenarioChanged = true;
                        }
                    }
                }
            }
            else
            {
                scenarioChanged = true;
            }
            base.BeforePost();
        }

        public override void AfterPost()
        {
            var Supplier = XModule.GetTable("SUPPLIER");
            var Trdr = Convert.ToInt32(Supplier.Current["TRDR"]) >0 ? Convert.ToInt32(Supplier.Current["TRDR"]) : XModule.ObjID() ;
            var TrdrCode = Convert.ToString(Supplier.Current["CODE"] != DBNull.Value ? Supplier.Current["CODE"] : "");
            var TrdrName = Convert.ToString(Supplier.Current["NAME"] != DBNull.Value ? Supplier.Current["NAME"] : "");
            var SupChainScenH = Convert.ToInt32(Supplier.Current["CCCSUPCHAINSCENH"] != DBNull.Value ? Supplier.Current["CCCSUPCHAINSCENH"]:0);

            //if(SupChainScenH>0 && scenarioChanged == true)
            if (scenarioChanged == true)
            {
                var ans = 0;
                ans = XSupport.AskYesNoCancel("Αλλαγή Αποθήκης Παραλαβής Αγορών", $"Θέλετε να πραγματοποιήσετε αλλαγή στην Αποθήκη Παραλαβής Αγορών των ειδών που έχουν βασικό προμηθευτή τον {TrdrName}?");
                if ((ans == 6) || (ans !=6 && ans !=7 && ans !=2)) // 6=Yes, 7=No, 2=Cancel //ans = 48836663 import Excel
                {
                    if (SupChainScenH != 0)
                    {
                        XSupport.ExecuteSQL($@"UPDATE Table_A SET Table_A.CCCRECEIPTSTORE = Table_B.SUPPLIERWH
                                        FROM MTRBRNLIMITS AS Table_A
                                        INNER JOIN (SELECT T.TRDR,T.CODE AS TCODE,T.NAME AS TNAME,T.CCCSUPCHAINSCENH
                                        ,M.MTRL,M.CODE AS MTRLCODE,M.NAME AS MTRLNAME,S1.BUYERWH,S1.SUPPLIERWH,M1.CCCRECEIPTSTORE
                                        FROM TRDR T
                                        INNER JOIN MTRL M ON M.MTRSUP =T.TRDR INNER JOIN CCCSUPCHAINSCEND S1 ON S1.CCCSUPCHAINSCENH=T.CCCSUPCHAINSCENH
                                        INNER JOIN MTRBRNLIMITS M1 ON M1.MTRL=M.MTRL AND M1.WHOUSE=S1.BUYERWH
                                        WHERE 
                                        /*M.MTRL=2349 AND */
                                        T.TRDR={Trdr} 
                                        AND (M.CCCSUPCHAINSCENH IS NULL OR M.CCCSUPCHAINSCENH={dbScenario})
                                        ) AS Table_B ON Table_A.MTRL = Table_B.MTRL AND Table_A.WHOUSE= Table_B.BUYERWH");

                        XSupport.ExecuteSQL($@"UPDATE MTRL SET CCCSUPCHAINSCENH = {SupChainScenH} WHERE MTRSUP ={Trdr} AND (CCCSUPCHAINSCENH IS NULL OR CCCSUPCHAINSCENH={dbScenario}) and company ={XSupport.ConnectionInfo.CompanyId}");
                    }
                    else
                    {
                        XSupport.ExecuteSQL($@"UPDATE MTRBRNLIMITS SET CCCRECEIPTSTORE=NULL WHERE MTRL IN (SELECT MTRL FROM MTRL WHERE MTRSUP={Trdr} AND CCCSUPCHAINSCENH = {dbScenario})");
                        XSupport.ExecuteSQL($@"UPDATE MTRL SET CCCSUPCHAINSCENH = NULL WHERE MTRSUP ={Trdr} AND CCCSUPCHAINSCENH = {dbScenario} and company ={XSupport.ConnectionInfo.CompanyId}");
                    }



                    var checkschen = $@"SELECT T.TRDR,T.CODE AS TCODE,T.NAME AS TNAME,T.CCCSUPCHAINSCENH,M.MTRL,M.CODE AS MTRLCODE,M.NAME AS MTRLNAME,M.CCCSUPCHAINSCENH
                                        FROM TRDR T INNER JOIN MTRL M ON M.MTRSUP =T.TRDR
                                        WHERE T.TRDR= {Trdr} AND (M.CCCSUPCHAINSCENH IS NOT NULL AND (M.CCCSUPCHAINSCENH<>{dbScenario} AND M.CCCSUPCHAINSCENH <> {SupChainScenH}))";

                    using (XTable checkschends = XSupport.GetSQLDataSet(checkschen))
                    {
                        if (checkschends.Count > 0)
                        {
                            ans = 0;
                            ans = XSupport.AskYesNoCancel("Αλλαγή Αποθήκης Παραλαβής Αγορών", $"Υπάρχουν είδη με διαφορετικό σενάριο από το σενάριο του βασικού προμηθευτή τους, θέλετε να πραγματοποιήσετε αλλαγή στην Αποθήκη Παραλαβής Αγορών?");
                            if (ans == 6) // 6=Yes, 7=No, 2=Cancel
                            {
                                XSupport.ExecuteSQL($@"UPDATE Table_A SET Table_A.CCCRECEIPTSTORE = Table_B.SUPPLIERWH
                                        FROM MTRBRNLIMITS AS Table_A
                                        INNER JOIN (SELECT T.TRDR,T.CODE AS TCODE,T.NAME AS TNAME,T.CCCSUPCHAINSCENH
                                        ,M.MTRL,M.CODE AS MTRLCODE,M.NAME AS MTRLNAME,S1.BUYERWH,S1.SUPPLIERWH,M1.CCCRECEIPTSTORE
                                        FROM TRDR T
                                        INNER JOIN MTRL M ON M.MTRSUP =T.TRDR INNER JOIN CCCSUPCHAINSCEND S1 ON S1.CCCSUPCHAINSCENH=T.CCCSUPCHAINSCENH
                                        INNER JOIN MTRBRNLIMITS M1 ON M1.MTRL=M.MTRL AND M1.WHOUSE=S1.BUYERWH
                                        WHERE 
                                        /*M.MTRL=2349 AND */
                                        T.TRDR={Trdr} 
                                        AND (M.CCCSUPCHAINSCENH IS NOT NULL AND (M.CCCSUPCHAINSCENH<>{dbScenario} AND M.CCCSUPCHAINSCENH <> {SupChainScenH}))
                                        ) AS Table_B ON Table_A.MTRL = Table_B.MTRL AND Table_A.WHOUSE= Table_B.BUYERWH");

                                XSupport.ExecuteSQL($@"UPDATE MTRL SET CCCSUPCHAINSCENH = {SupChainScenH} WHERE MTRSUP ={Trdr} AND (CCCSUPCHAINSCENH IS NOT NULL AND (CCCSUPCHAINSCENH<>{dbScenario} AND CCCSUPCHAINSCENH <> {SupChainScenH})) and company ={XSupport.ConnectionInfo.CompanyId}");
                            }
                        }
                    }
                }
                else
                {
                    if (SupChainScenH != dbScenario)
                    {
                        XSupport.ExecuteSQL($@"UPDATE TRDR SET CCCSUPCHAINSCENH = {dbScenario} WHERE TRDR ={Trdr} and company ={XSupport.ConnectionInfo.CompanyId}");
                    }     
                }
            }
            base.AfterPost();
        }
    }
}
