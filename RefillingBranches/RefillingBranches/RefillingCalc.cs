﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Softone;

namespace RefillingBranches
{
    public class RefillingCalc
    {
        XSupport xSupport = null;

        public RefillingCalc(XSupport _xSupport)
        {
            xSupport = _xSupport;
        }

        public void RefillingCalculation()
        {
            var currentdate = DateTime.Now.ToString("yyyyMMdd");

            var query = $@"SELECT A.WHOUSE,A.MTRL,M.CODE AS MTRLCODE,M.NAME AS MTRLNAME,M.MTRUNIT1,
                        CASE WHEN ISNULL(M.MTRUNIT2,0)=0 THEN M.MTRUNIT1 ELSE M.MTRUNIT2 END AS MTRUNIT2,
                        CASE WHEN ISNULL(M.MTRUNIT2,0)=0 THEN 1 ELSE ISNULL(M.MU21,1) END MU21,
                        /*(CASE WHEN B.CCCBRANCHZONE =1 THEN 1001 WHEN B.CCCBRANCHZONE =2 THEN 1002 ELSE 0 END) AS CENTRALWHOUSE,*/

                        ISNULL(A.CCCRECEIPTSTORE,0) AS CENTRALWHOUSE,M.MTRSUP,T.CCCSUPCHAINSCENH,

                        (CASE WHEN A.WHOUSE = 1002 THEN 10 
                        WHEN A.WHOUSE = 1001 THEN 20
                        WHEN A.WHOUSE = 1003 THEN 30
                        WHEN A.WHOUSE = 1005 THEN 40
                        WHEN A.WHOUSE = 1008 THEN 50
                        WHEN A.WHOUSE = 1021 THEN 60
                        WHEN A.WHOUSE = 1011 THEN 70
                        WHEN A.WHOUSE = 1022 THEN 80
                        WHEN A.WHOUSE = 1004 THEN 90
                        WHEN A.WHOUSE = 1007 THEN 100
                        WHEN A.WHOUSE = 1006 THEN 110
                        WHEN A.WHOUSE = 1014 THEN 120
                        WHEN A.WHOUSE = 1012 THEN 130
                        WHEN A.WHOUSE = 1023 THEN 140 ELSE 0 END) AS PRIORITIZATION,
                        (ISNULL(OPNWH,0)+ISNULL(TRNWH,0)) AS REMAIN,
                        ISNULL(M.CCCCOMMERCIALQTY,0) AS CCCCOMMERCIALQTY,
                        ISNULL(A.REMAINLIMMIN,0) AS REMAINLIMMIN,
                        ISNULL(A.REMAINLIMMAX,0) AS REMAINLIMMAX,
                        ISNULL(CCCQTYSTEPREPLACEMENT,1) AS CCCQTYSTEPREPLACEMENT,
                        ISNULL(C.ANAMENOMENA,0) AS ANAMENOMENA,
                        ISNULL(D.DESMEYMENA,0) AS DESMEYMENA
                        FROM MTRBRNLIMITS A
                        INNER JOIN MTRL M ON M.MTRL=A.MTRL
                        LEFT JOIN BRANCH B ON B.BRANCH=A.BRANCH AND A.COMPANY=B.COMPANY
                        LEFT JOIN (SELECT ML.COMPANY,ML.MTRL,ML.WHOUSE,SUM(ISNULL(ML.QTY1,0)-ISNULL(ML.QTY1COV,0)-ISNULL(ML.QTY1CANC,0)) AS ANAMENOMENA
                        FROM MTRLINES ML LEFT JOIN RESTMODE R ON ML.RESTMODE  = R.RESTMODE AND ML.COMPANY=R.COMPANY
                        WHERE ML.COMPANY={xSupport.ConnectionInfo.CompanyId} AND R.RESTCATEG = 1 GROUP BY ML.COMPANY,ML.MTRL,ML.WHOUSE) AS C ON C.MTRL=A.MTRL AND C.WHOUSE=A.WHOUSE AND C.COMPANY=A.COMPANY
                        LEFT JOIN (SELECT ML.COMPANY,ML.MTRL,ML.WHOUSE,SUM(ISNULL(ML.QTY1,0)-ISNULL(ML.QTY1COV,0)-ISNULL(ML.QTY1CANC,0)) AS DESMEYMENA
                        FROM MTRLINES ML LEFT JOIN RESTMODE R ON ML.RESTMODE  = R.RESTMODE AND ML.COMPANY=R.COMPANY
                        WHERE ML.COMPANY={xSupport.ConnectionInfo.CompanyId} AND R.RESTCATEG = 2 GROUP BY ML.COMPANY,ML.MTRL,ML.WHOUSE) AS D ON D.MTRL=A.MTRL AND D.WHOUSE=A.WHOUSE AND D.COMPANY=A.COMPANY
                        LEFT JOIN (SELECT MB.COMPANY,MB.MTRL ,MB.WHOUSE,SUM(ISNULL(MB.IMPQTY1,0)-ISNULL(MB.EXPQTY1,0)) AS OPNWH
                        FROM MTRBALSHEET MB WHERE MB.COMPANY={xSupport.ConnectionInfo.CompanyId} AND MB.FISCPRD=YEAR('{currentdate}') AND MB.PERIOD<MONTH('{currentdate}') GROUP BY MB.MTRL ,MB.WHOUSE,MB.COMPANY) E ON E.MTRL=A.MTRL AND E.WHOUSE=A.WHOUSE AND E.COMPANY=A.COMPANY
                        LEFT JOIN (SELECT T.COMPANY,T.MTRL ,T.WHOUSE,SUM((TP.FLG01*ISNULL(T.QTY1,0)-TP.FLG04*ISNULL(T.QTY1,0))) AS TRNWH 
                        FROM MTRTRN T JOIN TPRMS TP ON TP.COMPANY=T.COMPANY AND TP.SODTYPE=T.SODTYPE AND TP.TPRMS=T.TPRMS 
                        WHERE T.COMPANY={xSupport.ConnectionInfo.CompanyId} AND T.FISCPRD=YEAR('{currentdate}') AND T.SODTYPE=51 AND T.TRNDATE>=DATEADD(mm, DATEDIFF(mm, 0, '{currentdate}'), 0) 
                        AND T.TRNDATE<='{currentdate}' GROUP BY T.MTRL,T.WHOUSE,T.COMPANY) F ON F.MTRL=A.MTRL AND F.WHOUSE=A.WHOUSE AND F.COMPANY=A.COMPANY
                        LEFT JOIN TRDR T ON T.TRDR=M.MTRSUP
                        WHERE 
                        A.COMPANY ={xSupport.ConnectionInfo.CompanyId}
                        AND M.ISACTIVE=1
                        AND A.WHOUSE IN (1001,1002,1003,1004,1005,1006,1007,1008,1011,1012,1014,1021,1022,1023)
                        AND ISNULL(A.REMAINLIMMIN,0.0) <> 0.0 AND ISNULL(A.REMAINLIMMAX,0.0) <>0
                        AND T.CCCSUPCHAINSCENH IS NOT NULL
						/*AND A.MTRL IN (243871,2349,2370,32047)*/
                        /*
                            AND A.MTRL IN (/*243871,2349,2370,*/
                            32047
                            /*,2350,2437,31811,2357,20899,22134,31755,32047*/)
                        */
                        ORDER BY A.MTRL,A.WHOUSE";
            try
            {
                var DataForRefilling = new List<MtrlPerWhouseData>();
                using (XTable ds = xSupport.GetSQLDataSet(query))
                {
                    if (ds.Count > 0)
                    {
                        var dataList = ds.CreateDataTable(true).AsEnumerable().ToList();
                        var groupedList = dataList.GroupBy(x => Convert.ToInt32(x["MTRL"].ToString())).ToList();
                        foreach (var group in groupedList)
                        {
                            var myItem = new MtrlPerWhouseData
                            {
                                Mtrl = group.Key,
                                MtrlCode = group.First()["MTRLCODE"].ToString(),
                                MtrlName = group.First()["MTRLNAME"].ToString(),
                                MtrUnit1 = Convert.ToInt32(group.First()["MTRUNIT1"]),
                                MtrUnit2 = Convert.ToInt32(group.First()["MTRUNIT2"]),
                                Mu21 = Convert.ToDouble(group.First()["MU21"]),                              
                                WhouseData = group.Select(x => new WhouseData
                                {
                                    Whouse = Convert.ToInt32(x["WHOUSE"]),
                                    CentralWhouse = Convert.ToInt32(x["CENTRALWHOUSE"]),
                                    Prioritization = Convert.ToInt32(x["PRIORITIZATION"]),
                                    Remain = Convert.ToDouble(x["REMAIN"]),
                                    AvailableRemain = (Convert.ToDouble(x["REMAIN"]) 
                                                    + (Convert.ToDouble(x["ANAMENOMENA"]) > 0 ? Convert.ToDouble(x["ANAMENOMENA"]) : 0.0) 
                                                    - (Convert.ToDouble(x["DESMEYMENA"]) > 0 ? Convert.ToDouble(x["DESMEYMENA"]) : 0.0)),
                                    CccCommercialQty = Convert.ToDouble(x["CCCCOMMERCIALQTY"]),
                                    RemainLimMin = Convert.ToDouble(x["REMAINLIMMIN"]),
                                    RemainLimMax = Convert.ToDouble(x["REMAINLIMMAX"]),
                                    CccQtyStepReplacement = Convert.ToDouble(x["CCCQTYSTEPREPLACEMENT"]),
                                    Anamenomena = Convert.ToDouble(x["ANAMENOMENA"])>0 ? Convert.ToDouble(x["ANAMENOMENA"]) : 0.0,
                                    Desmeymena = Convert.ToDouble(x["DESMEYMENA"])>0 ? Convert.ToDouble(x["DESMEYMENA"]) : 0.0,
                                    NewOrder = 0,
                                    RefillingQty =0.0,
                                    RefillingQtyUsr=0.0,
                                    RefMtrUnit1 = Convert.ToInt32(group.First()["MTRUNIT1"]),
                                    RefMtrUnit2 = Convert.ToInt32(group.First()["MTRUNIT2"])
                                    //Packages = 0.0
                                }).OrderBy(x=>x.Prioritization).ToList()
                            };
                            DataForRefilling.Add(myItem);
                        }

                        if (DataForRefilling.Count > 0)
                        {
                            //loop mtrl data
                            foreach (var data in DataForRefilling)
                            {
                                var mtrunit2 = data.MtrUnit2;
                                var mu21 = data.Mu21;

                                //list of central whouse
                                //var centralWhouseList = data.WhouseData.Where(w => w.Whouse == w.CentralWhouse).ToList();
                                var centralWhouses = data.WhouseData.Where(w => w.CentralWhouse > 0).Select(w => w.CentralWhouse).Distinct().ToList();

                                foreach (var cwh in centralWhouses)
                                {
                                    var centralWhouseList = data.WhouseData.Where(w => w.Whouse == cwh).ToList();

                                    //loop list of central whouse
                                    foreach (var cwhouse in centralWhouseList)
                                    {
                                        var centralWhouse = cwhouse.Whouse;

                                        //Διαθέσιμο για αναπληρώσεις καταστημάτων
                                        //Ανάθεση ποσότητας κατά είδος για αναπλήρωση από την κεντρική αποθήκη.
                                        var qtyToRefill = cwhouse.Remain - cwhouse.RemainLimMin > 0.0 ? cwhouse.Remain - cwhouse.RemainLimMin : 0.0;

                                        cwhouse.CentralQtyRef = qtyToRefill;
                                        cwhouse.CentralPackageRef = Math.Round((qtyToRefill / mu21), MidpointRounding.AwayFromZero);

                                        if (qtyToRefill > 0.0)
                                        {
                                            var branchWhouseList = new MtrlPerWhouseData
                                            {
                                                Mtrl = data.Mtrl,
                                                MtrlCode = data.MtrlCode,
                                                MtrlName = data.MtrlName,
                                                MtrUnit2 = data.MtrUnit2,
                                                Mu21 = data.Mu21,
                                                WhouseData = data.WhouseData.Where(x => x.Whouse != x.CentralWhouse && x.CentralWhouse == centralWhouse && !centralWhouses.Contains(x.Whouse) ).OrderBy(x => x.Prioritization).ToList()
                                            };

                                            //Phase 1 //Ιεραρχική αναπλήρωση καταστημάτων έως την Εμπορική Ποσότητα
                                            foreach (var whouse in branchWhouseList.WhouseData)
                                            {
                                                whouse.Ph1Qty = whouse.Remain - whouse.CccCommercialQty <= 0 ? (whouse.Remain - whouse.CccCommercialQty) * (-1) : 0.0;
                                            }
                                            var sumQtyPhase1 = branchWhouseList.WhouseData.Sum(x => x.Ph1Qty);

                                            //έλεγχος διαθεσιμότητας στην κεντική αποθήκη 
                                            if (cwhouse.CentralQtyRef - sumQtyPhase1 > 0)
                                            {
                                                //Phase 2 //Ιεραρχική αναπλήρωση καταστημάτων έως το επίπεδο αποθέματος Min.
                                                foreach (var whouse in branchWhouseList.WhouseData)
                                                {
                                                    whouse.Ph2Qty = whouse.Remain - whouse.RemainLimMin <= 0 ? (whouse.Remain - whouse.RemainLimMin) * (-1) : 0.0;
                                                }
                                                var sumQtyPhase2 = branchWhouseList.WhouseData.Sum(x => x.Ph2Qty);

                                                if (cwhouse.CentralQtyRef - sumQtyPhase2 > 0)
                                                {
                                                    //Phase 3 //Αναλογική αναπλήρωση καταστημάτων έως το επίπεδο αποθέματος Max.
                                                    foreach (var whouse in branchWhouseList.WhouseData)
                                                    {
                                                        whouse.Ph3Qty = whouse.Remain - whouse.RemainLimMax <= 0 ? (whouse.Remain - whouse.RemainLimMax) * (-1) : 0.0;
                                                    }
                                                    var sumQtyPhase3 = branchWhouseList.WhouseData.Sum(x => x.Ph3Qty);

                                                    if ((cwhouse.CentralQtyRef - sumQtyPhase3 > 0))
                                                    {
                                                        foreach (var whouse in branchWhouseList.WhouseData)
                                                        {
                                                            whouse.Ph3QtyFin = whouse.Ph3Qty;
                                                            whouse.RefillingQty = whouse.Ph3QtyFin;
                                                            whouse.RefillingQtyUsr = whouse.Ph3QtyFin;
                                                            whouse.FinalPhase = 3;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        foreach (var whouse in branchWhouseList.WhouseData)
                                                        {
                                                            whouse.Ph3Step1 = whouse.Ph3Qty * whouse.Prioritization;
                                                        }
                                                        var sumStep1 = branchWhouseList.WhouseData.Sum(x => x.Ph3Step1);
                                                        var syntelestis = (cwhouse.CentralQtyRef - sumQtyPhase3) / sumStep1;

                                                        foreach (var whouse in branchWhouseList.WhouseData)
                                                        {
                                                            whouse.Ph3Step2 = whouse.Ph3Step1 * syntelestis * (-1);
                                                            whouse.Ph3Step3 = Math.Round(whouse.Ph3Step2, MidpointRounding.AwayFromZero);
                                                            whouse.Ph3Step4 = whouse.Ph3Qty - whouse.Ph3Step3;
                                                            whouse.Ph3QtyFin = whouse.Ph3Step4;
                                                            whouse.RefillingQty = whouse.Ph3QtyFin;
                                                            whouse.RefillingQtyUsr = whouse.Ph3QtyFin;
                                                            whouse.FinalPhase = 3;
                                                        }
                                                    }

                                                    //Phase 4 //Στρογγυλοποίηση της ποσότητας βάση της συσκευασίας διακινήσεων 
                                                    if (mtrunit2 == 107 || mtrunit2 == 108) //Κιβώτια ή Πακέτο
                                                    {
                                                        foreach (var whouse in branchWhouseList.WhouseData)
                                                        {
                                                            whouse.Ph4Qty = Math.Round((whouse.Ph3QtyFin / mu21), MidpointRounding.AwayFromZero);
                                                            whouse.RefillingQtyUsr = whouse.Ph4Qty;
                                                            whouse.FinalPhase = 4;
                                                        }

                                                        ///ELEGXOS SOS POSOTITON META THN FASI 4
                                                        //var sumPackages = branchWhouseList.WhouseData.Where(x => x.Whouse != x.CentralWhouse && x.CentralWhouse>0).Sum(y => y.RefillingQty);
                                                        var sumPackages = branchWhouseList.WhouseData.Where(x => x.Whouse != x.CentralWhouse && x.CentralWhouse > 0).Sum(y => y.RefillingQtyUsr);
                                                        var centralSumPackages = cwhouse.CentralPackageRef;
                                                        if (sumPackages > centralSumPackages)
                                                        {
                                                            var qtyFix = centralSumPackages - sumPackages;

                                                            var fixBranchWhouseList = new MtrlPerWhouseData
                                                            {
                                                                Mtrl = branchWhouseList.Mtrl,
                                                                MtrlCode = branchWhouseList.MtrlCode,
                                                                MtrlName = branchWhouseList.MtrlName,
                                                                MtrUnit2 = branchWhouseList.MtrUnit2,
                                                                Mu21 = branchWhouseList.Mu21,
                                                                WhouseData = branchWhouseList.WhouseData.OrderByDescending(x => x.Ph4Qty).ToList()
                                                            };

                                                            foreach (var fix in fixBranchWhouseList.WhouseData)
                                                            {
                                                                if (qtyFix != 0)
                                                                {
                                                                    //fix.RefillingQty = fix.RefillingQty + qtyFix;
                                                                    fix.RefillingQtyUsr = fix.RefillingQtyUsr + qtyFix;
                                                                    qtyFix = qtyFix + 1;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        foreach (var whouse in branchWhouseList.WhouseData)
                                                        {
                                                            whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    var remainForRefilling = cwhouse.CentralQtyRef;
                                                    var totalSum = 0.0;
                                                    var endsum = 0;
                                                    foreach (var whouse in branchWhouseList.WhouseData)
                                                    {
                                                        totalSum = +totalSum + whouse.Ph2Qty;
                                                        if (remainForRefilling - totalSum > 0)
                                                        {
                                                            var newPh2Qty = whouse.Ph2Qty;
                                                            whouse.Ph2Qty = newPh2Qty;
                                                            whouse.RefillingQty = newPh2Qty;
                                                            whouse.RefillingQtyUsr = newPh2Qty;
                                                            whouse.FinalPhase = 2;
                                                            whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                        }
                                                        else
                                                        {
                                                            if (endsum == 0)
                                                            {
                                                                var newPh2Qty = totalSum - remainForRefilling;
                                                                whouse.Ph2Qty = newPh2Qty;
                                                                whouse.RefillingQty = newPh2Qty;
                                                                whouse.RefillingQtyUsr = newPh2Qty;
                                                                whouse.FinalPhase = 2;
                                                                endsum = 1;
                                                                whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                            }
                                                            else
                                                            {
                                                                whouse.Ph2Qty = 0.0;
                                                                whouse.RefillingQty = 0.0;
                                                                whouse.RefillingQtyUsr = 0.0;
                                                                whouse.FinalPhase = 2;
                                                                whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                var remainForRefilling = cwhouse.CentralQtyRef;
                                                var totalSum = 0.0;
                                                var endsum = 0;
                                                foreach (var whouse in branchWhouseList.WhouseData)
                                                {
                                                    totalSum = +totalSum + whouse.Ph1Qty;
                                                    if (remainForRefilling - totalSum > 0)
                                                    {
                                                        var newPh1Qty = whouse.Ph1Qty;
                                                        whouse.Ph1Qty = newPh1Qty;
                                                        whouse.RefillingQty = newPh1Qty;
                                                        whouse.RefillingQtyUsr = newPh1Qty;
                                                        whouse.FinalPhase = 1;
                                                        whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                    }
                                                    else
                                                    {
                                                        if (endsum == 0)
                                                        {
                                                            var newPh1Qty = totalSum - remainForRefilling;
                                                            whouse.Ph1Qty = newPh1Qty;
                                                            whouse.RefillingQty = newPh1Qty;
                                                            whouse.RefillingQtyUsr = newPh1Qty;
                                                            whouse.FinalPhase = 1;
                                                            whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                            endsum = 1;
                                                        }
                                                        else
                                                        {
                                                            whouse.Ph1Qty = 0.0;
                                                            whouse.RefillingQty = 0.0;
                                                            whouse.RefillingQtyUsr = 0.0;
                                                            whouse.FinalPhase = 1;
                                                            whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                        }
                                                    }
                                                }
                                            }

                                            //Αναπλήρωση κεντρικής αποθήκης
                                            //Μετά τον έλεγχο για αναπλήρωση των καταστημάτων πραγματοποιούμε έλεγχο για την αναπλήρωση της κεντρικής αποθήκης , 
                                            //με την υπόλοιπη ποσότητα (Όπως έχει διαμορφωθεί στην φάση 3 ή στην φάση 4 αν υπάρχει) που έχει απομείνει μετά την αναπλήρωση των καταστημάτων.
                                            //Ακολουθούμε την ίδια διαδικασία με τα καταστήματα.

                                            var sumRefillingQty = 0.0;                                            
                                            var finalPhase = branchWhouseList.WhouseData.Select(x => x.FinalPhase).Distinct().ToList().FirstOrDefault();
                                            if (finalPhase == 4)
                                            {
                                                sumRefillingQty = branchWhouseList.WhouseData.Sum(x => (x.RefillingQtyUsr * mu21));
                                            }
                                            else
                                            {
                                                sumRefillingQty = branchWhouseList.WhouseData.Sum(x => x.RefillingQty);
                                            }

                                            var newqtyToRefill = cwhouse.CentralQtyRef - sumRefillingQty;
                                            cwhouse.CentralQtyRef = newqtyToRefill;
                                            cwhouse.CentralPackageRef = Math.Round((newqtyToRefill / mu21), MidpointRounding.AwayFromZero);
                                            if (newqtyToRefill > 0)
                                            {
                                                var newbranchWhouseList = new MtrlPerWhouseData
                                                {
                                                    Mtrl = data.Mtrl,
                                                    MtrlCode = data.MtrlCode,
                                                    MtrlName = data.MtrlName,
                                                    MtrUnit2 = data.MtrUnit2,
                                                    Mu21 = data.Mu21,
                                                    WhouseData = data.WhouseData.Where(x => x.Whouse != x.CentralWhouse && x.CentralWhouse == centralWhouse && centralWhouses.Contains(x.Whouse)).OrderBy(x => x.Prioritization).ToList()
                                                };
                                                //Phase 1 //Ιεραρχική αναπλήρωση καταστημάτων έως την Εμπορική Ποσότητα
                                                foreach (var whouse in newbranchWhouseList.WhouseData)
                                                {
                                                    whouse.Ph1Qty = whouse.Remain - whouse.CccCommercialQty <= 0 ? (whouse.Remain - whouse.CccCommercialQty) * (-1) : 0.0;
                                                }
                                                var newsumQtyPhase1 = newbranchWhouseList.WhouseData.Sum(x => x.Ph1Qty);
                                                //έλεγχος διαθεσιμότητας στην κεντική αποθήκη 
                                                if (cwhouse.CentralQtyRef - sumQtyPhase1 > 0)
                                                {
                                                    //Phase 2 //Ιεραρχική αναπλήρωση καταστημάτων έως το επίπεδο αποθέματος Min.
                                                    foreach (var whouse in newbranchWhouseList.WhouseData)
                                                    {
                                                        whouse.Ph2Qty = whouse.Remain - whouse.RemainLimMin <= 0 ? (whouse.Remain - whouse.RemainLimMin) * (-1) : 0.0;
                                                    }
                                                    var sumQtyPhase2 = newbranchWhouseList.WhouseData.Sum(x => x.Ph2Qty);

                                                    if (cwhouse.CentralQtyRef - sumQtyPhase2 > 0)
                                                    {
                                                        //Phase 3 //Αναλογική αναπλήρωση καταστημάτων έως το επίπεδο αποθέματος Max.
                                                        foreach (var whouse in newbranchWhouseList.WhouseData)
                                                        {
                                                            whouse.Ph3Qty = whouse.Remain - whouse.RemainLimMax <= 0 ? (whouse.Remain - whouse.RemainLimMax) * (-1) : 0.0;
                                                        }
                                                        var sumQtyPhase3 = newbranchWhouseList.WhouseData.Sum(x => x.Ph3Qty);

                                                        if ((cwhouse.CentralQtyRef - sumQtyPhase3 > 0))
                                                        {
                                                            foreach (var whouse in newbranchWhouseList.WhouseData)
                                                            {
                                                                whouse.Ph3QtyFin = whouse.Ph3Qty;
                                                                whouse.RefillingQty = whouse.Ph3QtyFin;
                                                                whouse.RefillingQtyUsr = whouse.Ph3QtyFin;
                                                                whouse.FinalPhase = 3;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            foreach (var whouse in newbranchWhouseList.WhouseData)
                                                            {
                                                                whouse.Ph3Step1 = whouse.Ph3Qty * whouse.Prioritization;
                                                            }
                                                            var sumStep1 = newbranchWhouseList.WhouseData.Sum(x => x.Ph3Step1);
                                                            var syntelestis = (cwhouse.CentralQtyRef - sumQtyPhase3) / sumStep1;

                                                            foreach (var whouse in newbranchWhouseList.WhouseData)
                                                            {
                                                                whouse.Ph3Step2 = whouse.Ph3Step1 * syntelestis * (-1);
                                                                whouse.Ph3Step3 = Math.Round(whouse.Ph3Step2, MidpointRounding.AwayFromZero);
                                                                whouse.Ph3Step4 = whouse.Ph3Qty - whouse.Ph3Step3;
                                                                whouse.Ph3QtyFin = whouse.Ph3Step4;
                                                                whouse.RefillingQty = whouse.Ph3QtyFin;
                                                                whouse.RefillingQtyUsr = whouse.Ph3QtyFin;
                                                                whouse.FinalPhase = 3;
                                                            }
                                                        }

                                                        //Phase 4 //Στρογγυλοποίηση της ποσότητας βάση της συσκευασίας διακινήσεων 
                                                        if (mtrunit2 == 107 || mtrunit2 == 108) //Κιβώτια ή Πακέτο
                                                        {
                                                            foreach (var whouse in newbranchWhouseList.WhouseData)
                                                            {
                                                                whouse.Ph4Qty = Math.Round((whouse.Ph3QtyFin / mu21), MidpointRounding.AwayFromZero);
                                                                whouse.RefillingQtyUsr = whouse.Ph4Qty;
                                                                whouse.FinalPhase = 4;
                                                            }

                                                            ///ELEGXOS SOS POSOTITON META THN FASI 4
                                                            //var sumPackages = branchWhouseList.WhouseData.Where(x => x.Whouse != x.CentralWhouse && x.CentralWhouse>0).Sum(y => y.RefillingQty);
                                                            var sumPackages = newbranchWhouseList.WhouseData.Where(x => x.Whouse != x.CentralWhouse && x.CentralWhouse > 0).Sum(y => y.RefillingQtyUsr);
                                                            var centralSumPackages = cwhouse.CentralPackageRef;
                                                            if (sumPackages > centralSumPackages)
                                                            {
                                                                var qtyFix = centralSumPackages - sumPackages;

                                                                var fixBranchWhouseList = new MtrlPerWhouseData
                                                                {
                                                                    Mtrl = newbranchWhouseList.Mtrl,
                                                                    MtrlCode = newbranchWhouseList.MtrlCode,
                                                                    MtrlName = newbranchWhouseList.MtrlName,
                                                                    MtrUnit2 = newbranchWhouseList.MtrUnit2,
                                                                    Mu21 = newbranchWhouseList.Mu21,
                                                                    WhouseData = newbranchWhouseList.WhouseData.OrderByDescending(x => x.Ph4Qty).ToList()
                                                                };

                                                                foreach (var fix in fixBranchWhouseList.WhouseData)
                                                                {
                                                                    if (qtyFix != 0)
                                                                    {
                                                                        fix.RefillingQtyUsr = fix.RefillingQtyUsr + qtyFix;
                                                                        qtyFix = qtyFix + 1;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            foreach (var whouse in newbranchWhouseList.WhouseData)
                                                            {
                                                                whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var remainForRefilling = cwhouse.CentralQtyRef;
                                                        var totalSum = 0.0;
                                                        var endsum = 0;
                                                        foreach (var whouse in newbranchWhouseList.WhouseData)
                                                        {
                                                            totalSum = +totalSum + whouse.Ph2Qty;
                                                            if (remainForRefilling - totalSum > 0)
                                                            {
                                                                var newPh2Qty = whouse.Ph2Qty;
                                                                whouse.Ph2Qty = newPh2Qty;
                                                                whouse.RefillingQty = newPh2Qty;
                                                                whouse.RefillingQtyUsr = newPh2Qty;
                                                                whouse.FinalPhase = 2;
                                                                whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                            }
                                                            else
                                                            {
                                                                if (endsum == 0)
                                                                {
                                                                    var newPh2Qty = totalSum - remainForRefilling;
                                                                    whouse.Ph2Qty = newPh2Qty;
                                                                    whouse.RefillingQty = newPh2Qty;
                                                                    whouse.RefillingQtyUsr = newPh2Qty;
                                                                    whouse.FinalPhase = 2;
                                                                    endsum = 1;
                                                                    whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                                }
                                                                else
                                                                {
                                                                    whouse.Ph2Qty = 0.0;
                                                                    whouse.RefillingQty = 0.0;
                                                                    whouse.RefillingQtyUsr = 0.0;
                                                                    whouse.FinalPhase = 2;
                                                                    whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    var remainForRefilling = cwhouse.CentralQtyRef;
                                                    var totalSum = 0.0;
                                                    var endsum = 0;
                                                    foreach (var whouse in newbranchWhouseList.WhouseData)
                                                    {
                                                        totalSum = +totalSum + whouse.Ph1Qty;
                                                        if (remainForRefilling - totalSum > 0)
                                                        {
                                                            var newPh1Qty = whouse.Ph1Qty;
                                                            whouse.Ph1Qty = newPh1Qty;
                                                            whouse.RefillingQty = newPh1Qty;
                                                            whouse.RefillingQtyUsr = newPh1Qty;
                                                            whouse.FinalPhase = 1;
                                                            whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                        }
                                                        else
                                                        {
                                                            if (endsum == 0)
                                                            {
                                                                var newPh1Qty = totalSum - remainForRefilling;
                                                                whouse.Ph1Qty = newPh1Qty;
                                                                whouse.RefillingQty = newPh1Qty;
                                                                whouse.RefillingQtyUsr = newPh1Qty;
                                                                whouse.FinalPhase = 1;
                                                                whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                                endsum = 1;
                                                            }
                                                            else
                                                            {
                                                                whouse.Ph1Qty = 0.0;
                                                                whouse.RefillingQty = 0.0;
                                                                whouse.RefillingQtyUsr = 0.0;
                                                                whouse.FinalPhase = 1;
                                                                whouse.RefMtrUnit2 = whouse.RefMtrUnit1;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            //Αναπλήρωση κεντρικής αποθήκης  
                                        }
                                    }
                                }
                            }

                            //apotelesmata
                            try
                            {
                                using (var Refilling = xSupport.CreateModule("CCCREFILLING"))
                                {
                                    S1Tools.HideWarningsFromS1Module(Refilling, xSupport);
                                    Refilling.InsertData();
                                    Refilling.GetTable("CCCREFILLINGH").Current["NAME"] = $@"Αναπλήρωση Καταστημάτων({DateTime.Now.ToString("dddd, dd MMMM yyyy")})";
                                    Refilling.GetTable("CCCREFILLINGH").Current["TRNDATE"] = DateTime.Now;
                                    Refilling.GetTable("CCCREFILLINGH").Current["TRNDATEREF"] = DateTime.Now.AddDays(1);
                                    foreach (var data in DataForRefilling)
                                    {
                                        var centralWhouses = data.WhouseData.Where(w => w.CentralWhouse > 0).Select(w => w.CentralWhouse).Distinct().ToList();
                                        var sumRefillingQty = data.WhouseData.Sum(q=>q.RefillingQtyUsr);
                                        //θα πρέπει να υπάρχει ποσότητα προς αναπλήρωση για να πραγματοποιθεί η καταχώρηση
                                        if (sumRefillingQty > 0)
                                        {
                                            using (var RefillingMtrl = Refilling.GetTable("CCCREFILLINGMTRL"))
                                            {
                                                RefillingMtrl.Current.Append();
                                                RefillingMtrl.Current["MTRL"] = data.Mtrl;
                                                using (var RefillingWhouse = Refilling.GetTable("CCCREFILLINGWHOUSE"))
                                                {
                                                    foreach (var whouse in data.WhouseData)
                                                    {
                                                        //if (whouse.Whouse != whouse.CentralWhouse)
                                                        if (whouse.Whouse != whouse.CentralWhouse && whouse.CentralWhouse > 0 && !centralWhouses.Contains(whouse.Whouse))
                                                        {
                                                            RefillingWhouse.Current.Append();
                                                            RefillingWhouse.Current["ISCENTRAL"] = 0;
                                                            //RefillingWhouse.Current["CENTRALQTYREF"] = 0;                                                       
                                                            RefillingWhouse.Current["MTRL"] = data.Mtrl;
                                                            RefillingWhouse.Current["WHOUSE"] = whouse.Whouse;
                                                            RefillingWhouse.Current["REMAIN"] = whouse.Remain;
                                                            RefillingWhouse.Current["CCCCOMMERCIALQTY"] = whouse.CccCommercialQty;
                                                            RefillingWhouse.Current["REMAINLIMMIN"] = whouse.RemainLimMin;
                                                            RefillingWhouse.Current["REMAINLIMMAX"] = whouse.RemainLimMax;
                                                            //RefillingWhouse.Current["NOMOVES"] = 0;
                                                            RefillingWhouse.Current["REFILLINGQTY"] = whouse.RefillingQty;
                                                            RefillingWhouse.Current["REFILLINGQTYUSR"] = whouse.RefillingQtyUsr;
                                                            RefillingWhouse.Current["FINALPHASE"] = whouse.FinalPhase;
                                                            RefillingWhouse.Current["CENTRALWHOUSE"] = whouse.CentralWhouse;
                                                            RefillingWhouse.Current["REFMTRUNIT1"] = whouse.RefMtrUnit1;
                                                            RefillingWhouse.Current["REFMTRUNIT2"] = whouse.RefMtrUnit2;
                                                            RefillingWhouse.Current["CHOICE"] = 0;
                                                            RefillingWhouse.Current.Post();
                                                        }
                                                    }
                                                }

                                                foreach (var cwh in centralWhouses)
                                                {
                                                    using (var RefillingWhouse = Refilling.GetTable("CCCREFILLINGWHOUSECEN"))
                                                    {
                                                        foreach (var whouse in data.WhouseData)
                                                        {
                                                            //if (whouse.Whouse == whouse.CentralWhouse)
                                                            if (whouse.Whouse == cwh && whouse.CentralWhouse > 0 && centralWhouses.Contains(whouse.Whouse))
                                                            {
                                                                RefillingWhouse.Current.Append();
                                                                RefillingWhouse.Current["ISCENTRAL"] = 1;
                                                                RefillingWhouse.Current["CENTRALQTYREF"] = whouse.CentralQtyRef;
                                                                RefillingWhouse.Current["MTRL"] = data.Mtrl;
                                                                RefillingWhouse.Current["WHOUSE"] = whouse.Whouse;
                                                                RefillingWhouse.Current["REMAIN"] = whouse.Remain;
                                                                RefillingWhouse.Current["CCCCOMMERCIALQTY"] = whouse.CccCommercialQty;
                                                                RefillingWhouse.Current["REMAINLIMMIN"] = whouse.RemainLimMin;
                                                                RefillingWhouse.Current["REMAINLIMMAX"] = whouse.RemainLimMax;
                                                                //RefillingWhouse.Current["NOMOVES"] = 0;
                                                                RefillingWhouse.Current["REFILLINGQTY"] = whouse.RefillingQty;
                                                                RefillingWhouse.Current["REFILLINGQTYUSR"] = whouse.RefillingQtyUsr;
                                                                RefillingWhouse.Current["FINALPHASE"] = whouse.FinalPhase;
                                                                RefillingWhouse.Current["CENTRALWHOUSE"] = whouse.CentralWhouse;
                                                                RefillingWhouse.Current["REFMTRUNIT1"] = whouse.RefMtrUnit1;
                                                                RefillingWhouse.Current["REFMTRUNIT2"] = whouse.RefMtrUnit2;
                                                                RefillingWhouse.Current["CHOICE"] = 0;
                                                                RefillingWhouse.Current.Post();
                                                            }
                                                        }
                                                    }
                                                }
                                                RefillingMtrl.Current.Post();
                                            }
                                        }
                                    }
                                    Refilling.PostData();
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
