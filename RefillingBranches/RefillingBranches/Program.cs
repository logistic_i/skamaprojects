﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Softone;

namespace RefillingBranches
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var xconame = Properties.Settings.Default.XcoName;
                var username = Properties.Settings.Default.UserName;
                var password = Properties.Settings.Default.Password;
                var company = Properties.Settings.Default.Company;
                var branch = Properties.Settings.Default.Branch;
                var softOnePath = @"C:\Softone\Soft1\";
#if DEBUG
                xconame = Properties.Settings.Default.XcoNameDebug;
                username = Properties.Settings.Default.UserName;
                password = Properties.Settings.Default.Password;
                company = Properties.Settings.Default.Company;
                branch = Properties.Settings.Default.Branch;
                softOnePath = @"C:\Softone\Soft1_521\";
#endif
                var xSupport = SoftoneClient.Login(xconame, username, password, company, branch, softOnePath, DateTime.Now.Date);
                RefillingCalc rec = new RefillingCalc(xSupport);
                rec.RefillingCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
