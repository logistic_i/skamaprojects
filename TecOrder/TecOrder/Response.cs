﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TecOrder
{
        [XmlRoot(ElementName = "OrdRspIssueDate")]
        public class OrdRspIssueDate
        {
            [XmlElement(ElementName = "Date")]
            public Date Date { get; set; }
        }

        [XmlRoot(ElementName = "MessageStatus")]
        public class MessageStatus
        {
            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "OrdRspHeader")]
        public class OrdRspHeader
        {
            [XmlElement(ElementName = "OrdRspId")]
            public string OrdRspId { get; set; }

            [XmlElement(ElementName = "OrdRspIssueDate")]
            public OrdRspIssueDate OrdRspIssueDate { get; set; }

            [XmlElement(ElementName = "OrderRef")]
            public OrderRef OrderRef { get; set; }

            [XmlElement(ElementName = "MessageStatus")]
            public MessageStatus MessageStatus { get; set; }

            [XmlElement(ElementName = "SellerParty")]
            public SellerParty SellerParty { get; set; }

            [XmlElement(ElementName = "BuyerParty")]
            public BuyerParty BuyerParty { get; set; }

            [XmlElement(ElementName = "DeliveryParty")]
            public DeliveryParty DeliveryParty { get; set; }

            [XmlElement(ElementName = "Currency")]
            public string Currency { get; set; }

            [XmlElement(ElementName = "FreeText")]
            public FreeText FreeText { get; set; }
        }

        [XmlRoot(ElementName = "RequestedQuantity")]
        public class RequestedQuantity
        {
            [XmlElement(ElementName = "Quantity")]
            public Quantity Quantity { get; set; }
        }

        [XmlRoot(ElementName = "ConfirmedQuantity")]
        public class ConfirmedQuantity
        {
            [XmlElement(ElementName = "Quantity")]
            public Quantity Quantity { get; set; }
        }

        [XmlRoot(ElementName = "RequestedDeliveryDate")]
        public class RequestedDeliveryDate
        {
            [XmlElement(ElementName = "Date")]
            public Date Date { get; set; }
        }

        [XmlRoot(ElementName = "DeliveryInfo")]
        public class DeliveryInfo
        {
            [XmlElement(ElementName = "Quantity")]
            public Quantity Quantity { get; set; }

            [XmlElement(ElementName = "Date")]
            public Date Date { get; set; }
        }

        [XmlRoot(ElementName = "ItemInstruction")]
        public class ItemInstruction
        {
            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "OrdRspItem")]
        public class OrdRspItem
        {
            [XmlElement(ElementName = "PositionNumber")]
            public string PositionNumber { get; set; }

            [XmlElement(ElementName = "RequestedQuantity")]
            public RequestedQuantity RequestedQuantity { get; set; }

            [XmlElement(ElementName = "ConfirmedQuantity")]
            public ConfirmedQuantity ConfirmedQuantity { get; set; }

            [XmlElement(ElementName = "ProductId")]
            public ProductId ProductId { get; set; }

            [XmlElement(ElementName = "ProductDescription")]
            public ProductDescription ProductDescription { get; set; }

            [XmlElement(ElementName = "RequestedDeliveryDate")]
            public RequestedDeliveryDate RequestedDeliveryDate { get; set; }

            [XmlElement(ElementName = "DeliveryInfo")]
            public List<DeliveryInfo> DeliveryInfo { get; set; }

            [XmlElement(ElementName = "ItemInstruction")]
            public ItemInstruction ItemInstruction { get; set; }

            [XmlElement(ElementName = "OrderItemRef")]
            public OrderItemRef OrderItemRef { get; set; }

            [XmlElement(ElementName = "FreeText")]
            public List<FreeText> FreeText { get; set; }

            [XmlElement(ElementName = "ProductPrice")]
            public ProductPrice ProductPrice { get; set; }

            [XmlElement(ElementName = "QtyVariance")]
            public QtyVariance QtyVariance { get; set; }
        }

        [XmlRoot(ElementName = "ProductPrice")]
        public class ProductPrice
        {
            [XmlElement(ElementName = "Price")]
            public string Price { get; set; }

            [XmlElement(ElementName = "PriceCategory")]
            public string PriceCategory { get; set; }

            [XmlElement(ElementName = "PriceUnit")]
            public PriceUnit PriceUnit { get; set; }
        }

        [XmlRoot(ElementName = "PriceUnit")]
        public class PriceUnit
        {
            [XmlElement(ElementName = "Quantity")]
            public Quantity Quantity { get; set; }
        }

        [XmlRoot(ElementName = "Document")]
        public class Document
        {
            [XmlAttribute(AttributeName = "Type")]
            public string Type { get; set; }

            [XmlAttribute(AttributeName = "Version")]
            public string Version { get; set; }
        }

        [XmlRoot(ElementName = "Date")]
        public class Date
        {
            [XmlAttribute(AttributeName = "Format")]
            public string Format { get; set; }

            [XmlAttribute(AttributeName = "Qualifier")]
            public string Qualifier { get; set; }

            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "DesAdvIssueDate")]
        public class DesAdvIssueDate
        {
            [XmlElement(ElementName = "Date")]
            public Date Date { get; set; }
        }

        [XmlRoot(ElementName = "DeliveryDate")]
        public class DeliveryDate
        {
            [XmlElement(ElementName = "Date")]
            public Date Date { get; set; }
        }

        [XmlRoot(ElementName = "TransportDetails")]
        public class TransportDetails
        {
            [XmlElement(ElementName = "TransportMode")]
            public string TransportMode { get; set; }

            [XmlElement(ElementName = "TransportTypeMeansCode")]
            public string TransportTypeMeansCode { get; set; }

            [XmlElement(ElementName = "TransportTypeMeansFree")]
            public string TransportTypeMeansFree { get; set; }

            [XmlElement(ElementName = "CarrierIdentificationILN")]
            public string CarrierIdentificationILN { get; set; }
        }

        [XmlRoot(ElementName = "OrderRef")]
        public class OrderRef
        {
            [XmlElement(ElementName = "SellerOrderNumber")]
            public string SellerOrderNumber { get; set; }

            [XmlElement(ElementName = "BuyerOrderNumber")]
            public string BuyerOrderNumber { get; set; }

            [XmlElement(ElementName = "Date")]
            public Date Date { get; set; }
        }

        [XmlRoot(ElementName = "Address")]
        public class Address
        {
            [XmlElement(ElementName = "Name1")]
            public string Name1 { get; set; }

            [XmlElement(ElementName = "Street1")]
            public string Street1 { get; set; }

            [XmlElement(ElementName = "PostalCode")]
            public string PostalCode { get; set; }

            [XmlElement(ElementName = "City")]
            public string City { get; set; }

            [XmlElement(ElementName = "CountryCode")]
            public string CountryCode { get; set; }

            [XmlElement(ElementName = "CountryName")]
            public string CountryName { get; set; }
        }

        [XmlRoot(ElementName = "SellerParty")]
        public class SellerParty
        {
            [XmlElement(ElementName = "PartyNumber")]
            public string PartyNumber { get; set; }

            [XmlElement(ElementName = "Address")]
            public Address Address { get; set; }
        }

        [XmlRoot(ElementName = "BuyerParty")]
        public class BuyerParty
        {
            [XmlElement(ElementName = "PartyNumber")]
            public string PartyNumber { get; set; }

            [XmlElement(ElementName = "Address")]
            public Address Address { get; set; }
        }

        [XmlRoot(ElementName = "DeliveryParty")]
        public class DeliveryParty
        {
            [XmlElement(ElementName = "PartyNumber")]
            public string PartyNumber { get; set; }

            [XmlElement(ElementName = "Address")]
            public Address Address { get; set; }
        }

        [XmlRoot(ElementName = "FreeText")]
        public class FreeText
        {
            [XmlAttribute(AttributeName = "Caption")]
            public string Caption { get; set; }

            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "DesAdvHeader")]
        public class DesAdvHeader
        {
            [XmlElement(ElementName = "DesAdvId")]
            public string DesAdvId { get; set; }

            [XmlElement(ElementName = "DesAdvIssueDate")]
            public DesAdvIssueDate DesAdvIssueDate { get; set; }

            [XmlElement(ElementName = "DeliveryDate")]
            public DeliveryDate DeliveryDate { get; set; }

            [XmlElement(ElementName = "TransportDetails")]
            public TransportDetails TransportDetails { get; set; }

            [XmlElement(ElementName = "OrderRef")]
            public OrderRef OrderRef { get; set; }

            [XmlElement(ElementName = "SellerParty")]
            public SellerParty SellerParty { get; set; }

            [XmlElement(ElementName = "BuyerParty")]
            public BuyerParty BuyerParty { get; set; }

            [XmlElement(ElementName = "DeliveryParty")]
            public DeliveryParty DeliveryParty { get; set; }

            [XmlElement(ElementName = "FreeText")]
            public List<FreeText> FreeText { get; set; }
        }

        [XmlRoot(ElementName = "PkgInfo")]
        public class PkgInfo
        {
            [XmlElement(ElementName = "PacketCount")]
            public string PacketCount { get; set; }

            [XmlElement(ElementName = "PacketKind")]
            public string PacketKind { get; set; }

            [XmlElement(ElementName = "PacketKindFreeText")]
            public string PacketKindFreeText { get; set; }
        }

        [XmlRoot(ElementName = "MeasurementUnit")]
        public class MeasurementUnit
        {
            [XmlAttribute(AttributeName = "MeasurementUnitQualifier")]
            public string MeasurementUnitQualifier { get; set; }

            [XmlAttribute(AttributeName = "UoM")]
            public string UoM { get; set; }

            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "Measurements")]
        public class Measurements
        {
            [XmlElement(ElementName = "MeasurementUnit")]
            public List<MeasurementUnit> MeasurementUnit { get; set; }
        }

        [XmlRoot(ElementName = "Package")]
        public class Package
        {
            [XmlElement(ElementName = "PkgNumber")]
            public string PkgNumber { get; set; }

            [XmlElement(ElementName = "PkgInfo")]
            public PkgInfo PkgInfo { get; set; }

            [XmlElement(ElementName = "Measurements")]
            public Measurements Measurements { get; set; }

            [XmlElement(ElementName = "PkgId")]
            public List<PkgId> PkgId { get; set; }

            [XmlElement(ElementName = "FreeText")]
            public FreeText FreeText { get; set; }

            [XmlElement(ElementName = "PkgItem")]
            public List<PkgItem> PkgItem { get; set; }
        }

        [XmlRoot(ElementName = "PkgId")]
        public class PkgId
        {
            [XmlElement(ElementName = "PkgIdentSystem")]
            public string PkgIdentSystem { get; set; }

            [XmlElement(ElementName = "PkgIdentNumberQualifier")]
            public string PkgIdentNumberQualifier { get; set; }

            [XmlElement(ElementName = "PkgIdentNumber")]
            public string PkgIdentNumber { get; set; }
        }

        [XmlRoot(ElementName = "Quantity")]
        public class Quantity
        {
            [XmlAttribute(AttributeName = "UoM")]
            public string UoM { get; set; }

            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "DeliveredQuantity")]
        public class DeliveredQuantity
        {
            [XmlElement(ElementName = "Quantity")]
            public Quantity Quantity { get; set; }
        }

        [XmlRoot(ElementName = "ProductId")]
        public class ProductId
        {
            [XmlElement(ElementName = "MakerCode")]
            public string MakerCode { get; set; }

            [XmlElement(ElementName = "ProductNumber")]
            public string ProductNumber { get; set; }

            [XmlElement(ElementName = "Ean")]
            public string Ean { get; set; }

            [XmlElement(ElementName = "BuyerProductNumber")]
            public string BuyerProductNumber { get; set; }
        }

        [XmlRoot(ElementName = "ProductDescription")]
        public class ProductDescription
        {
            [XmlElement(ElementName = "ProductName1")]
            public string ProductName1 { get; set; }
        }

        [XmlRoot(ElementName = "OrderItemRef")]
        public class OrderItemRef
        {
            [XmlElement(ElementName = "SellerOrderItemRef")]
            public string SellerOrderItemRef { get; set; }

            [XmlElement(ElementName = "BuyerOrderItemRef")]
            public string BuyerOrderItemRef { get; set; }
        }

        [XmlRoot(ElementName = "PkgItem")]
        public class PkgItem
        {
            [XmlElement(ElementName = "PositionNumber")]
            public string PositionNumber { get; set; }

            [XmlElement(ElementName = "DeliveredQuantity")]
            public DeliveredQuantity DeliveredQuantity { get; set; }

            [XmlElement(ElementName = "ProductId")]
            public ProductId ProductId { get; set; }

            [XmlElement(ElementName = "ProductDescription")]
            public ProductDescription ProductDescription { get; set; }

            [XmlElement(ElementName = "OrderRef")]
            public OrderRef OrderRef { get; set; }

            [XmlElement(ElementName = "OrderItemRef")]
            public OrderItemRef OrderItemRef { get; set; }
        }

        [XmlRoot(ElementName = "DesAdv")]
        public class DesAdv
        {
            [XmlElement(ElementName = "Document")]
            public Document Document { get; set; }

            [XmlElement(ElementName = "DesAdvHeader")]
            public DesAdvHeader DesAdvHeader { get; set; }

            [XmlElement(ElementName = "Package")]
            public List<Package> Package { get; set; }
        }

        [XmlRoot(ElementName = "QtyVariance")]
        public class QtyVariance
        {
            [XmlElement(ElementName = "Quantity")]
            public Quantity Quantity { get; set; }

            [XmlElement(ElementName = "DeviationReason")]
            public string DeviationReason { get; set; }

            [XmlElement(ElementName = "ModificationReason")]
            public string ModificationReason { get; set; }

            [XmlElement(ElementName = "QtyVarComment")]
            public string QtyVarComment { get; set; }
        }

        [XmlRoot(ElementName = "OrdRsp")]
        public class OrdRsp
        {
            [XmlElement(ElementName = "Document")]
            public Document Document { get; set; }

            [XmlElement(ElementName = "OrdRspHeader")]
            public OrdRspHeader OrdRspHeader { get; set; }

            [XmlElement(ElementName = "OrdRspItem")]
            public List<OrdRspItem> OrdRspItem { get; set; }
        }

        [XmlRoot(ElementName = "InvoiceIssueDate")]
        public class InvoiceIssueDate
        {
            [XmlElement(ElementName = "Date")]
            public Date Date { get; set; }
        }

        [XmlRoot(ElementName = "DesAdvRef")]
        public class DesAdvRef
        {
            [XmlElement(ElementName = "DocumentNumber")]
            public string DocumentNumber { get; set; }

            [XmlElement(ElementName = "Date")]
            public Date Date { get; set; }
        }

        [XmlRoot(ElementName = "TaxTreatmentCode")]
        public class TaxTreatmentCode
        {
            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "InvoiceDueDate")]
        public class InvoiceDueDate
        {
            [XmlElement(ElementName = "Date")]
            public Date Date { get; set; }
        }

        [XmlRoot(ElementName = "PaymentDiscount")]
        public class PaymentDiscount
        {
            [XmlElement(ElementName = "DiscountPercent")]
            public string DiscountPercent { get; set; }

            [XmlElement(ElementName = "DiscountDays")]
            public string DiscountDays { get; set; }

            [XmlElement(ElementName = "Date")]
            public Date Date { get; set; }
        }

        [XmlRoot(ElementName = "PaymentTerms")]
        public class PaymentTerms
        {
            [XmlElement(ElementName = "PaymentDeadline")]
            public string PaymentDeadline { get; set; }

            [XmlElement(ElementName = "PaymentDiscount")]
            public PaymentDiscount PaymentDiscount { get; set; }
        }

        [XmlRoot(ElementName = "ContactInformation")]
        public class ContactInformation
        {
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }

            [XmlElement(ElementName = "Telephone")]
            public string Telephone { get; set; }

            [XmlElement(ElementName = "EMail")]
            public string EMail { get; set; }
        }

        [XmlRoot(ElementName = "InvoiceParty")]
        public class InvoiceParty
        {
            [XmlElement(ElementName = "PartyNumber")]
            public string PartyNumber { get; set; }

            [XmlElement(ElementName = "TaxRegistrationNumber")]
            public string TaxRegistrationNumber { get; set; }

            [XmlElement(ElementName = "Address")]
            public Address Address { get; set; }
        }

        [XmlRoot(ElementName = "InvoiceOrg")]
        public class InvoiceOrg
        {
            [XmlElement(ElementName = "InvoiceParty")]
            public InvoiceParty InvoiceParty { get; set; }
        }

        [XmlRoot(ElementName = "Banking")]
        public class Banking
        {
            [XmlElement(ElementName = "Bankname")]
            public string Bankname { get; set; }

            [XmlElement(ElementName = "BankAccount")]
            public string BankAccount { get; set; }

            [XmlElement(ElementName = "BankCodeNumber")]
            public string BankCodeNumber { get; set; }

            [XmlElement(ElementName = "IBAN")]
            public string IBAN { get; set; }

            [XmlElement(ElementName = "SWIFT-BIC")]
            public string SWIFTBIC { get; set; }
        }

        [XmlRoot(ElementName = "InvoiceHeader")]
        public class InvoiceHeader
        {
            [XmlElement(ElementName = "InvoiceId")]
            public string InvoiceId { get; set; }

            [XmlElement(ElementName = "MessageType")]
            public string MessageType { get; set; }

            [XmlElement(ElementName = "MessageFunction")]
            public string MessageFunction { get; set; }

            [XmlElement(ElementName = "InvoiceIssueDate")]
            public InvoiceIssueDate InvoiceIssueDate { get; set; }

            [XmlElement(ElementName = "DesAdvRef")]
            public DesAdvRef DesAdvRef { get; set; }

            [XmlElement(ElementName = "TaxTreatmentCode")]
            public TaxTreatmentCode TaxTreatmentCode { get; set; }

            [XmlElement(ElementName = "InvoiceDueDate")]
            public InvoiceDueDate InvoiceDueDate { get; set; }

            [XmlElement(ElementName = "PaymentInstruction")]
            public string PaymentInstruction { get; set; }

            [XmlElement(ElementName = "PaymentTerms")]
            public List<PaymentTerms> PaymentTerms { get; set; }

            [XmlElement(ElementName = "SellerParty")]
            public SellerParty SellerParty { get; set; }

            [XmlElement(ElementName = "BuyerParty")]
            public BuyerParty BuyerParty { get; set; }

            [XmlElement(ElementName = "DeliveryParty")]
            public DeliveryParty DeliveryParty { get; set; }

            [XmlElement(ElementName = "InvoiceOrg")]
            public InvoiceOrg InvoiceOrg { get; set; }

            [XmlElement(ElementName = "Currency")]
            public string Currency { get; set; }

            [XmlElement(ElementName = "Banking")]
            public Banking Banking { get; set; }
        }

        [XmlRoot(ElementName = "DesAdvItemRef")]
        public class DesAdvItemRef
        {
            [XmlElement(ElementName = "PositionNumber")]
            public string PositionNumber { get; set; }
        }

        [XmlRoot(ElementName = "Tax")]
        public class Tax
        {
            [XmlElement(ElementName = "TaxCode")]
            public string TaxCode { get; set; }

            [XmlElement(ElementName = "Percent")]
            public string Percent { get; set; }

            [XmlElement(ElementName = "Amount")]
            public string Amount { get; set; }

            [XmlElement(ElementName = "TaxableAmount")]
            public string TaxableAmount { get; set; }
        }

        [XmlRoot(ElementName = "UnitPrice")]
        public class UnitPrice
        {
            [XmlElement(ElementName = "Amount")]
            public string Amount { get; set; }
        }

        [XmlRoot(ElementName = "TotalPrice")]
        public class TotalPrice
        {
            [XmlElement(ElementName = "Amount")]
            public string Amount { get; set; }
        }

        [XmlRoot(ElementName = "TotalValue")]
        public class TotalValue
        {
            [XmlElement(ElementName = "Amount")]
            public string Amount { get; set; }
        }

        [XmlRoot(ElementName = "AdditionalPrice")]
        public class AdditionalPrice
        {
            [XmlAttribute(AttributeName = "AdditionalPriceType")]
            public string AdditionalPriceType { get; set; }

            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "ExternalTrade")]
        public class ExternalTrade
        {
            [XmlElement(ElementName = "CountryOfOrigin")]
            public string CountryOfOrigin { get; set; }
        }

        [XmlRoot(ElementName = "InvoiceDetail")]
        public class InvoiceDetail
        {
            [XmlElement(ElementName = "PositionNumber")]
            public string PositionNumber { get; set; }

            [XmlElement(ElementName = "Quantity")]
            public Quantity Quantity { get; set; }

            [XmlElement(ElementName = "ProductId")]
            public ProductId ProductId { get; set; }

            [XmlElement(ElementName = "ProductDescription")]
            public ProductDescription ProductDescription { get; set; }

            [XmlElement(ElementName = "DesAdvRef")]
            public DesAdvRef DesAdvRef { get; set; }

            [XmlElement(ElementName = "DesAdvItemRef")]
            public DesAdvItemRef DesAdvItemRef { get; set; }

            [XmlElement(ElementName = "OrderRef")]
            public OrderRef OrderRef { get; set; }

            [XmlElement(ElementName = "OrderItemRef")]
            public OrderItemRef OrderItemRef { get; set; }

            [XmlElement(ElementName = "Tax")]
            public Tax Tax { get; set; }

            [XmlElement(ElementName = "UnitPrice")]
            public UnitPrice UnitPrice { get; set; }

            [XmlElement(ElementName = "TotalPrice")]
            public TotalPrice TotalPrice { get; set; }

            [XmlElement(ElementName = "TotalValue")]
            public TotalValue TotalValue { get; set; }

            [XmlElement(ElementName = "AdditionalPrice")]
            public AdditionalPrice AdditionalPrice { get; set; }

            [XmlElement(ElementName = "ExternalTrade")]
            public ExternalTrade ExternalTrade { get; set; }

            [XmlElement(ElementName = "AllowOrCharge")]
            public List<AllowOrCharge> AllowOrCharge { get; set; }

            [XmlElement(ElementName = "FreeText")]
            public FreeText FreeText { get; set; }
        }

        [XmlRoot(ElementName = "AllowOrChargeIdentifier")]
        public class AllowOrChargeIdentifier
        {
            [XmlAttribute(AttributeName = "Value")]
            public string Value { get; set; }
        }

        [XmlRoot(ElementName = "AllowOrCharge")]
        public class AllowOrCharge
        {
            [XmlElement(ElementName = "AllowOrChargeSequenceNumber")]
            public string AllowOrChargeSequenceNumber { get; set; }

            [XmlElement(ElementName = "AllowOrChargeIdentifier")]
            public AllowOrChargeIdentifier AllowOrChargeIdentifier { get; set; }

            [XmlElement(ElementName = "AllowOrChargeCode")]
            public string AllowOrChargeCode { get; set; }

            [XmlElement(ElementName = "AllowOrChargeDescription")]
            public string AllowOrChargeDescription { get; set; }

            [XmlElement(ElementName = "Percent")]
            public string Percent { get; set; }

            [XmlElement(ElementName = "Amount")]
            public string Amount { get; set; }
        }


        [XmlRoot(ElementName = "InvoiceNetValue")]
        public class InvoiceNetValue
        {
            [XmlElement(ElementName = "Amount")]
            public string Amount { get; set; }
        }

        [XmlRoot(ElementName = "InvoiceGrossValue")]
        public class InvoiceGrossValue
        {
            [XmlElement(ElementName = "Amount")]
            public string Amount { get; set; }
        }

        [XmlRoot(ElementName = "InvoiceTaxAmount")]
        public class InvoiceTaxAmount
        {
            [XmlElement(ElementName = "Amount")]
            public string Amount { get; set; }
        }

        [XmlRoot(ElementName = "InvoiceAmountPayable")]
        public class InvoiceAmountPayable
        {
            [XmlElement(ElementName = "Amount")]
            public string Amount { get; set; }
        }

        [XmlRoot(ElementName = "InvoiceTotals")]
        public class InvoiceTotals
        {
            [XmlElement(ElementName = "InvoiceNetValue")]
            public InvoiceNetValue InvoiceNetValue { get; set; }

            [XmlElement(ElementName = "InvoiceGrossValue")]
            public InvoiceGrossValue InvoiceGrossValue { get; set; }

            [XmlElement(ElementName = "InvoiceTaxAmount")]
            public InvoiceTaxAmount InvoiceTaxAmount { get; set; }

            [XmlElement(ElementName = "InvoiceAmountPayable")]
            public InvoiceAmountPayable InvoiceAmountPayable { get; set; }
        }

        [XmlRoot(ElementName = "TaxTotals")]
        public class TaxTotals
        {
            [XmlElement(ElementName = "Tax")]
            public Tax Tax { get; set; }
        }

        [XmlRoot(ElementName = "TaxCodeDescription")]
        public class TaxCodeDescription
        {
            [XmlElement(ElementName = "TaxCode")]
            public string TaxCode { get; set; }

            [XmlElement(ElementName = "TaxDescription")]
            public string TaxDescription { get; set; }
        }

        [XmlRoot(ElementName = "InvoiceSummary")]
        public class InvoiceSummary
        {
            [XmlElement(ElementName = "AllowOrCharge")]
            public List<AllowOrCharge> AllowOrCharge { get; set; }

            [XmlElement(ElementName = "InvoiceTotals")]
            public InvoiceTotals InvoiceTotals { get; set; }

            [XmlElement(ElementName = "TaxTotals")]
            public TaxTotals TaxTotals { get; set; }

            [XmlElement(ElementName = "TaxCodeDescription")]
            public TaxCodeDescription TaxCodeDescription { get; set; }
        }

        [XmlRoot(ElementName = "Invoice")]
        public class Invoice
        {
            [XmlElement(ElementName = "Document")]
            public Document Document { get; set; }

            [XmlElement(ElementName = "InvoiceHeader")]
            public InvoiceHeader InvoiceHeader { get; set; }

            [XmlElement(ElementName = "InvoiceDetail")]
            public List<InvoiceDetail> InvoiceDetail { get; set; }

            [XmlElement(ElementName = "InvoiceSummary")]
            public InvoiceSummary InvoiceSummary { get; set; }
        }
    
}
