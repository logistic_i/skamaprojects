﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using TecCom;
using TecCom.OpenMessaging;
using TecCom.OpenMessaging.DTO;
using TecOrder.Dtos;

namespace TecOrder
{
    public static class Helper
    {
        public static List<RequestProduct> GetDummyProducts()
        {
            var products = new List<RequestProduct>();

            var descrs = new List<TecOrder.Dtos.ProductDescription>();
            descrs.Add(new TecOrder.Dtos.ProductDescription
            {
                Description = "something",
                Language = "EN"
            });

            products.Add(new RequestProduct
            {
                ProductNumber = "0810", //Full available
                ProductDescriptions = descrs,
                Uom = "PCE",
                Quantity = 10
            });
            products.Add(new RequestProduct
            {
                ProductNumber = "0811", //Partial available	10	Multiple Deliveries each 10 PCE for Backlog and StockOrder
                ProductDescriptions = descrs,
                Uom = "PCE",
                Quantity = 30
            });
            products.Add(new RequestProduct
            {
                ProductNumber = "0812", //Zero available	0	Multiple Deliveries each 10 PCE for Backlog and StockOrder
                ProductDescriptions = descrs,
                Uom = "PCE",
                Quantity = 10
            });
            products.Add(new RequestProduct
            {
                ProductNumber = "0813", //Obsolete material
                ProductDescriptions = descrs,
                Uom = "PCE",
                Quantity = 15
            });
            products.Add(new RequestProduct
            {
                ProductNumber = "0814", //Alternative material		Multiple Alternatives for Inq
                ProductDescriptions = descrs,
                Uom = "PCE",
                Quantity = 10
            });
            products.Add(new RequestProduct
            {
                ProductNumber = "0815", //Different package size		Additional Qty added
                ProductDescriptions = descrs,
                Uom = "PCE",
                Quantity = 15
            });
            products.Add(new RequestProduct
            {
                ProductNumber = "0816", //Minimum  quantity
                ProductDescriptions = descrs,
                Uom = "PCE",
                Quantity = 100
            });
            products.Add(new RequestProduct
            {
                ProductNumber = "0817", //UoM not matching		Switched UoM
                ProductDescriptions = descrs,
                Uom = "PCE",
                Quantity = 15
            });
            products.Add(new RequestProduct
            {
                ProductNumber = "0818", //Restriction on stock for inq		Second DeliveryDate if more than 1 PCE
                ProductDescriptions = descrs,
                Uom = "PCE",
                Quantity = 10
            });
            products.Add(new RequestProduct
            {
                ProductNumber = "0820", //MakerCode necessary		MakerCode * is refused
                ProductDescriptions = descrs,
                Uom = "PCE",
                Quantity = 15
            });
            products.Add(new RequestProduct
            {
                ProductNumber = "0821", //Scaled Prices		multiple Price information
                ProductDescriptions = descrs,
                Uom = "PCE",
                Quantity = 10
            });

            return products;
        }

        public static Trader GetDummyBuyer()
        {
            var buyer = new Trader();
            buyer.Name1 = "John Doe";
            buyer.Name2 = "Procurement Manager";
            buyer.Street1 = "Demo Street 1";
            buyer.PostalCode = "99999";
            buyer.City = "Demo City";
            buyer.Country = new Country { CountryCode = "GB", CountryName = "Demo Country" };

            return buyer;
        }

        public static Trader GetDummySupplier()
        {
            var supplier = new Trader();
            supplier.Name1 = "John Doe";
            supplier.Name2 = "Procurement Manager";
            supplier.Street1 = "Demo Street 1";
            supplier.PostalCode = "99999";
            supplier.City = "Demo City";
            supplier.Country = new Country { CountryCode = "GB", CountryName = "Demo Country" };

            return supplier;
        }

        public static void WriteXmlFile(string responseDoc, ParameterTypeID parameterTypeID, string xmlPath, string supplierNumber)
        {
            //var currentPath = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            //var xmlPath = $@"{currentPath}\xml\";

            Directory.CreateDirectory(xmlPath);
            var guid = Guid.NewGuid().ToString();
            var fileName = $@"{xmlPath}\{parameterTypeID.ToString().ToUpper()}_{guid}.xml";
            File.WriteAllText(fileName, responseDoc);
        }

        public static DocumentsResponse ReadFromXML()
        {
            var currentPath = Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory()));

            var res = new DocumentsResponse();
            res.DesAdvList = new List<DesAdv>();
            res.OrdRspList = new List<OrdRsp>();
            res.InvoiceList = new List<Invoice>();

            // check all xml files in the directory
            foreach (string xmlFile in Directory.EnumerateFiles(currentPath + @"\xml\", "*.xml"))
            {
                // read all xml files
                string contents = File.ReadAllText(xmlFile);
                var formatted = PrintXML(contents);
                File.WriteAllText(xmlFile, formatted);

                try
                {
                    contents = TxmlHelper.EnsureTxmlOrdRspNamespaceInDocument(contents);
                    if (xmlFile.Contains(ParameterTypeID.DesAdv.ToString()))
                    {
                        DesAdv response = (DesAdv)XMLHelper.GetObjectFromXML(typeof(DesAdv), contents);
                        res.DesAdvList.Add(response);
                    }
                    else if (xmlFile.Contains(ParameterTypeID.Invoice.ToString()))
                    {
                        Invoice response = (Invoice)XMLHelper.GetObjectFromXML(typeof(Invoice), contents);
                        res.InvoiceList.Add(response);
                    }
                    else if (xmlFile.Contains(ParameterTypeID.OrdRsp.ToString()))
                    {
                        OrdRsp response = (OrdRsp)XMLHelper.GetObjectFromXML(typeof(OrdRsp), contents);
                        res.OrdRspList.Add(response);
                    }
                }
                catch (Exception ex)
                {
                    res.Messages.Add(ex.Message);
                }
            }

            return res;
        }

        public static string PrintXML(string xml)
        {
            XDocument doc = XDocument.Parse(xml);
            return doc.ToString();
        }
    }
}