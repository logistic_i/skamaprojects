﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecOrder.Enums
{
    public enum ResponseClass
    {
        Information = 0,
        Warning = 1,
        Error = 2
    }
}
