﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecOrder.Enums
{
    public enum OrderType
    {
        undefined = 0,
        StockOrder = 1,
        Normal = 2,
        Express = 3,
        OverNight = 4,
        PickUp = 5,
        COD = 6,
        SeaFreight = 7,
        AirFreight = 8,
        SaturDayTillNine = 9,
        TillNine = 10,
        TillEighteen = 11,
        SaturdayTillTwelve = 12,
        TillTwelve = 13,
        ParcelPost = 14,
        PrivateParcelService = 15,
        Mail = 16,
        CourierExpress = 17
    }
}
