﻿using System;
using System.Collections.Generic;
using System.Linq;
using TecCom;
using TecCom.OpenMessaging;
using TecCom.OpenMessaging.Caller;
using TecCom.OpenMessaging.DTO;
using TecCom.OpenMessaging.Order.TXML.V4_1.Common;
using TecCom.OpenMessaging.Order.TXML.V4_1.Order;
using TecOrder.Dtos;
using TecOrder.Enums;

namespace TecOrder
{
    public class TecClient
    {
        private string _User;
        private string _Password;
        private string _WebServiceUrl;

        public TecClient(string user, string password)
        {
            _WebServiceUrl = "https://solutionportal.tecalliance.net/openmessaging/openmessaging.asmx";
            _User = user;
            _Password = password;
        }

        #region SubmitInquiry
        /// <summary>
        /// Method that sends inquiry to TecComm Platform for a list of products
        /// </summary>
        /// <param name="businessRelation"> Defines business relation between buyer and supplier</param>
        /// <param name="buyer"> Defines buyer's data such as address city, zip etc.</param>
        /// <param name="supplier"> Defines supplier's data such as address city, zip etc.</param>
        /// <param name="products"> List of products passed to supplier for availability inquiry</param>
        /// <returns>
        /// List of resProducts that has information of availability, net price, list price and other information if available
        /// </returns>
        public InquiryResp SubmitInquiry(BusinessRelation businessRelation, List<RequestProduct> products, Trader buyer = null, Trader supplier = null)
        {
            var res = new InquiryResp();

            try
            {
                var myorder = new TxmlOrder();
                myorder.OrderHeader.OrderType = OrderTypeIDs.RequestForOffer;
                myorder.OrderHeader.OrderFunction = OrderFunctionTypes.Original;

                myorder.OrderHeader.BuyerParty.PartyNumber.Qualifier = TecCom.OpenMessaging.Order.TXML.V4_1.Common.PartyNumberTypeQualifiers.SupplierNumber;
                myorder.OrderHeader.BuyerParty.PartyNumber.PartyNumberValue = businessRelation.BuyerNumber;


                myorder.OrderHeader.SellerParty.PartyNumber.Qualifier = TecCom.OpenMessaging.Order.TXML.V4_1.Common.PartyNumberTypeQualifiers.BuyerNumber;
                myorder.OrderHeader.SellerParty.PartyNumber.PartyNumberValue = businessRelation.SupplierNumber;

                myorder.OrderHeader.OrderIssueDate.DateValue.DateValue = DateTime.Now;
                myorder.OrderHeader.OrderProcessing.DispatchMode = DispatchModes.Normal;
                myorder.OrderHeader.Currency = "EUR";

                var orderitems = new List<OrderItemType>();

                var position = 1;

                foreach (var product in products)
                {
                    var orderitem = new OrderItemType();

                    orderitem.PositionNumber = position;
                    orderitem.RequestedQuantity.Quantity.UoM = product.Uom;
                    orderitem.RequestedQuantity.Quantity.QuantityValue = product.Quantity;
                    orderitem.ProductId.MakerCode = product.MakerCode;
                    orderitem.ProductId.ProductNumber = product.ProductNumber;
                    orderitem.ProductId.Ean = product.Ean;
                    orderitem.ProductId.BuyerProductNumber = product.BuyerProductNumber;


                    orderitems.Add(orderitem);

                    position++;
                }

                myorder.OrderItem = orderitems.ToArray();

                var myfunctioncaller = new FunctionCaller();
                myfunctioncaller.WebServiceURL = _WebServiceUrl;

                var myfunctioncallrequest = new FunctionCallRequest();
                myfunctioncallrequest.Authentication.User = _User;
                myfunctioncallrequest.Authentication.Password = _Password;
                myfunctioncallrequest.RequestedFunction.FunctionID = FunctionID.Order_SubmitInquiry;

                var myparameterdata = new ParameterData[1];
                myparameterdata[0] = new ParameterData();
                myparameterdata[0].ParameterType = ParameterTypeID.Inquiry;

                var RequestDocument = XMLHelper.GetXMLString(myorder);

                myparameterdata[0].ParameterValue = RequestDocument;

                myfunctioncallrequest.RequestedFunction.ParameterList = myparameterdata;

                var myfunctioncallresponse = default(FunctionCallResponse);

                var vallog = new TecCom.OpenMessaging.DTO.DTOValidationLog();
                var schema = new TecCom.OpenMessaging.DTO.DTOSchema(typeof(TecCom.OpenMessaging.DTO.FunctionCallRequest));
                schema.ValidateXmlDocument(XMLHelper.GetXMLString(myfunctioncallrequest), vallog);

                if (vallog.HighestSeverityLevel == SeverityLevel.FatalError)
                {
                    var error = vallog.ToString();
                }

                myfunctioncallresponse = myfunctioncaller.ProcessRequest(myfunctioncallrequest);

                var txt = string.Empty;
                var productsRes = new List<ResponseProduct>();

                if (myfunctioncallresponse.Status.Severity == SeverityLevel.Information)
                {
                    var ResponseDoc = myfunctioncallresponse.OriginatingFunction.ParameterList[0].ParameterValue;
                    try
                    {
                        ResponseDoc = TxmlHelper.EnsureTxmlOrdRspNamespaceInDocument(ResponseDoc);
                        TxmlOrdRsp OrderResponse = null;
                        OrderResponse = (TxmlOrdRsp)XMLHelper.GetObjectFromXML(typeof(TxmlOrdRsp), ResponseDoc);

                        foreach (OrdRspItemType ri in OrderResponse.OrdRspItem)
                        {
                            productsRes.Add(new ResponseProduct
                            {
                                ResponseQuantity = ri.ConfirmedQuantity.Quantity.QuantityValue,
                                ResponseQuantityUom = ri.ConfirmedQuantity?.Quantity.UoM,
                                ItemInstruction = ri.ItemInstruction?.Value.ToString(),
                                PositionNumber = ri.PositionNumber.ToString(),
                                ProductDescriptions = ri.ProductDescription?.Select(x => new Dtos.ProductDescription
                                {
                                    Description = x.ProductNameValue,
                                    Language = x.Language
                                }).ToList(),
                                BuyerProductNumber = ri.ProductId?.BuyerProductNumber,
                                Ean = ri.ProductId?.Ean,
                                MakerCode = ri.ProductId?.MakerCode,
                                ProductNumber = ri.ProductId?.ProductNumber,
                                Prices = ri.ProductPrice != null ? ri.ProductPrice.ToList().Select(x => new Dtos.ProductPrice
                                {
                                    PriceStr = x.Price,
                                    PriceValue = x.PriceValue,
                                    ReferenceUnit = x.PriceUnit?.Quantity.UoM,
                                    ReferenceQuantity = x.PriceUnit?.Quantity.QuantityValue,
                                    PriceCategory = x.PriceCategory.ToString()
                                }).ToList() : new List<Dtos.ProductPrice>(),
                                RequestedDeliveryDateStr = ri.RequestedDeliveryDate.DateValue.DateString,
                                RequestedQuantity = ri.RequestedQuantity.Quantity.QuantityValue,
                                RequestedQuantityUom = ri.RequestedQuantity.Quantity.UoM,
                                QtyVariances = ri.QtyVariance != null ? ri.QtyVariance.ToList().Select(x => new QuantityVariance
                                {
                                    DeviationReason = x.DeviationReason,
                                    ModificationReason = x.ModificationReason,
                                    QtyVarComment = x.QtyVarComment,
                                    QuantityUoM = x.Quantity?.UoM,
                                    QuantityValue = x.Quantity?.QuantityValue
                                }).ToList() : new List<QuantityVariance>(),
                                Responses = ri.Response != null ? ri.Response.ToList().Select(x => new Response
                                {
                                    Code = x.Code,
                                    Class = (ResponseClass)x.ResponseClass,
                                    Parameter = x.Parameter,
                                    Type = x.Type,
                                    Text = x.Text.Replace("%",x.Parameter)
                                }).ToList() : new List<Response>(),
                                AlternativeProducts = ri.Alternative != null ? ri.Alternative.ToList().Select(x => new AlternativeProduct
                                {
                                    ProductNumber = x.ProductId?.ProductNumber,
                                    ProductDescriptions = x.ProductDescription?.Select(y => new Dtos.ProductDescription
                                    {
                                        Description = y.ProductNameValue,
                                        Language = y.Language
                                    }).ToList(),
                                    BuyerProductNumber = x.ProductId?.BuyerProductNumber,
                                    Ean = x.ProductId?.Ean,
                                    MakerCode = x.ProductId?.MakerCode,
                                    Price = new Dtos.ProductPrice
                                    {
                                        PriceCategory = x.ProductPrice?.PriceCategory.ToString(),
                                        PriceStr = x.ProductPrice?.Price,
                                        PriceValue = x.ProductPrice?.PriceValue,
                                        ReferenceQuantity = x.ProductPrice?.PriceUnit?.Quantity?.QuantityValue,
                                        ReferenceUnit = x.ProductPrice?.PriceUnit?.Quantity?.UoM
                                    }
                                }).ToList() : new List<AlternativeProduct>()
                            });
                        }
                    }
                    catch (Exception ex)
                    {
                        txt += ex.ToString();
                    }
                }
                else
                {
                    txt += myfunctioncallresponse.Status.ToString();
                    txt += myfunctioncaller.LastRequestString;
                }

                res.Products = productsRes;
                res.Message = txt;
                return res;
            }
            catch (Exception ex)
            {
                res.Message = ex.Message;
                return res;
            }           
        }

        #endregion

        #region StockOrder
        /// <summary>
        /// Method that sends stock order to TecComm Platform.
        /// </summary>
        /// <param name="businessRelation"> Defines business relation between buyer and supplier</param>
        /// <param name="buyer"> Defines buyer's data such as address city, zip etc.</param>
        /// <param name="supplier"> Defines supplier's data such as address city, zip etc.</param>
        /// <param name="products"> List of products passed to supplier for order</param>
        /// <returns>
        /// Asynchronous method, returns only status and information, such as if order is placed to supplier's side.
        /// </returns>
        public OrderStatusResp SendStockOrder(BusinessRelation businessRelation, Trader buyer, Trader supplier, List<RequestProduct> products, Trader deliveryParty = null)
        {
            var res = new OrderStatusResp();

            try
            {
                var myorder = new TxmlOrder();
                myorder.OrderHeader.OrderType = OrderTypeIDs.Order;
                myorder.OrderHeader.OrderFunction = OrderFunctionTypes.Original;

                myorder.OrderHeader.BuyerParty.PartyNumber.Qualifier = TecCom.OpenMessaging.Order.TXML.V4_1.Common.PartyNumberTypeQualifiers.SupplierNumber;
                myorder.OrderHeader.BuyerParty.PartyNumber.PartyNumberValue = businessRelation.BuyerNumber;

                var mybuyeraddress = new TecCom.OpenMessaging.Order.TXML.V4_1.Common.AddressType();
                mybuyeraddress.Name1 = buyer.Name1;
                mybuyeraddress.Name2 = buyer.Name2;
                mybuyeraddress.Street1 = buyer.Street1;
                mybuyeraddress.PostalCode = buyer.PostalCode;
                mybuyeraddress.City = buyer.City;
                mybuyeraddress.CountryCode = buyer.Country.CountryCode;
                mybuyeraddress.CountryName = buyer.Country.CountryName;
                myorder.OrderHeader.BuyerParty.Address = mybuyeraddress;

                myorder.OrderHeader.SellerParty.PartyNumber.Qualifier = TecCom.OpenMessaging.Order.TXML.V4_1.Common.PartyNumberTypeQualifiers.BuyerNumber;
                myorder.OrderHeader.SellerParty.PartyNumber.PartyNumberValue = businessRelation.SupplierNumber;

                var myselleraddress = new TecCom.OpenMessaging.Order.TXML.V4_1.Common.AddressType();
                myselleraddress.Name1 = supplier.Name1;
                myselleraddress.Name2 = supplier.Name2;
                myselleraddress.Street1 = supplier.Street1;
                myselleraddress.PostalCode = supplier.PostalCode;
                myselleraddress.City = supplier.City;
                myselleraddress.CountryCode = supplier.Country.CountryCode;
                myselleraddress.CountryName = supplier.Country.CountryName;

                myorder.OrderHeader.SellerParty.Address = myselleraddress;

                myorder.OrderHeader.BuyerOrderNumber = "12345";
                myorder.OrderHeader.OrderIssueDate.DateValue.DateValue = DateTime.Now;
                myorder.OrderHeader.OrderProcessing.DispatchMode = DispatchModes.StockOrder;
                myorder.OrderHeader.Currency = "EUR";

                if (deliveryParty != null)
                {
                    myorder.OrderHeader.DeliveryParty = new TecCom.OpenMessaging.Order.TXML.V4_1.Common.PartyType2();
                    myorder.OrderHeader.DeliveryParty.PartyNumber = new TecCom.OpenMessaging.Order.TXML.V4_1.Common.PartyNumberType();
                    myorder.OrderHeader.DeliveryParty.PartyNumber.PartyNumberValue = deliveryParty.Code;
                    myorder.OrderHeader.DeliveryParty.PartyNumber.Qualifier = PartyNumberTypeQualifiers.SupplierNumber;
                    TecCom.OpenMessaging.Order.TXML.V4_1.Common.AddressType mydeliverypartyaddress = new TecCom.OpenMessaging.Order.TXML.V4_1.Common.AddressType();
                    mydeliverypartyaddress.Name1 = deliveryParty.Name1;
                    mydeliverypartyaddress.Street1 = deliveryParty.Street1;
                    mydeliverypartyaddress.PostalCode = deliveryParty.PostalCode;
                    mydeliverypartyaddress.City = deliveryParty.City;
                    mydeliverypartyaddress.CountryCode = deliveryParty.Country.CountryCode;
                    mydeliverypartyaddress.CountryName = deliveryParty.Country.CountryName;
                    myorder.OrderHeader.DeliveryParty.Address = mydeliverypartyaddress;
                }

                var orderitems = new List<OrderItemType>();

                var position = 1;

                foreach (var product in products)
                {
                    var orderitem = new OrderItemType();

                    orderitem.PositionNumber = position;
                    orderitem.RequestedQuantity.Quantity.UoM = product.Uom;
                    orderitem.RequestedQuantity.Quantity.QuantityValue = product.Quantity;
                    orderitem.ProductId.MakerCode = product.MakerCode;
                    orderitem.ProductId.ProductNumber = product.ProductNumber;
                    orderitem.ProductId.Ean = product.Ean;
                    orderitem.ProductId.BuyerProductNumber = product.BuyerProductNumber;

                    var myproductdescriptions = new List<ProductName>();

                    var myproductdescription = new ProductName();
                    myproductdescription.Language = "..";
                    myproductdescription.ProductNameValue = product.ProductDescriptions.Where(x => x.Language == "EN").FirstOrDefault().Description;
                    myproductdescriptions.Add(myproductdescription);

                    orderitem.ProductDescription = myproductdescriptions.ToArray();

                    orderitems.Add(orderitem);

                    position++;
                }

                myorder.OrderItem = orderitems.ToArray();

                var myfunctioncaller = new FunctionCaller();
                myfunctioncaller.WebServiceURL = _WebServiceUrl;

                var myfunctioncallrequest = new FunctionCallRequest();
                myfunctioncallrequest.Authentication.User = _User;
                myfunctioncallrequest.Authentication.Password = _Password;
                myfunctioncallrequest.RequestedFunction.FunctionID = FunctionID.Order_SendDocument;

                var myparameterdata = new ParameterData[1];
                myparameterdata[0] = new ParameterData();
                myparameterdata[0].ParameterType = ParameterTypeID.Order;

                var RequestDocument = XMLHelper.GetXMLString(myorder);

                myparameterdata[0].ParameterValue = RequestDocument;

                myfunctioncallrequest.RequestedFunction.ParameterList = myparameterdata;

                var myfunctioncallresponse = default(FunctionCallResponse);

                var vallog = new TecCom.OpenMessaging.DTO.DTOValidationLog();
                var schema = new TecCom.OpenMessaging.DTO.DTOSchema(typeof(TecCom.OpenMessaging.DTO.FunctionCallRequest));
                schema.ValidateXmlDocument(XMLHelper.GetXMLString(myfunctioncallrequest), vallog);

                if (vallog.HighestSeverityLevel == SeverityLevel.FatalError)
                {
                    var error = vallog.ToString();
                }

                myfunctioncallresponse = myfunctioncaller.ProcessRequest(myfunctioncallrequest);

                var txt = string.Empty;
                var productsRes = new List<ResponseProduct>();

                res.Status = myfunctioncallresponse.Status;
                if (res.Status.Severity == SeverityLevel.Information)
                {

                    var ResponseDoc = myfunctioncallresponse.OriginatingFunction.ParameterList[0].ParameterValue;
                    try
                    {
                        ResponseDoc = TxmlHelper.EnsureTxmlOrdRspNamespaceInDocument(ResponseDoc);
                        TxmlOrdRsp OrderResponse = null;
                        OrderResponse = (TxmlOrdRsp)XMLHelper.GetObjectFromXML(typeof(TxmlOrdRsp), ResponseDoc);

                    }
                    catch (Exception ex)
                    {
                        txt += ex.ToString();
                    }
                }
                else
                {
                    txt += myfunctioncallresponse.Status.ToString();
                    txt += myfunctioncaller.LastRequestString;
                }

                return res;
            }
            catch (Exception ex)
            {
                res.ExceptionMsg = ex.Message;
                return res;
            }
        }

        #endregion

        #region ExpressOrder
        /// <summary>
        /// Method that sends express order to TecComm Platform.
        /// </summary>
        /// <param name="businessRelation"> Defines business relation between buyer and supplier</param>
        /// <param name="buyer"> Defines buyer's data such as address city, zip etc.</param>
        /// <param name="supplier"> Defines supplier's data such as address city, zip etc.</param>
        /// <param name="products"> List of products passed to supplier for order</param>
        /// <returns>
        /// OrderResp which has Order Header and Details.
        /// </returns>
        public OrderResp SendExpressOrder(BusinessRelation businessRelation, Trader buyer, Trader supplier, List<RequestProduct> products, Trader deliveryParty)
        {
            var order = new OrderResp();

            try
            {
                var myorder = new TxmlOrder();
                myorder.OrderHeader.OrderType = OrderTypeIDs.Order;
                myorder.OrderHeader.OrderFunction = OrderFunctionTypes.Original;

                myorder.OrderHeader.BuyerParty.PartyNumber.Qualifier = TecCom.OpenMessaging.Order.TXML.V4_1.Common.PartyNumberTypeQualifiers.SupplierNumber;
                myorder.OrderHeader.BuyerParty.PartyNumber.PartyNumberValue = businessRelation.BuyerNumber;

                var mybuyeraddress = new TecCom.OpenMessaging.Order.TXML.V4_1.Common.AddressType();
                mybuyeraddress.Name1 = buyer.Name1;
                mybuyeraddress.Name2 = buyer.Name2;
                mybuyeraddress.Street1 = buyer.Street1;
                mybuyeraddress.PostalCode = buyer.PostalCode;
                mybuyeraddress.City = buyer.City;
                mybuyeraddress.CountryCode = buyer.Country.CountryCode;
                mybuyeraddress.CountryName = buyer.Country.CountryName;
                myorder.OrderHeader.BuyerParty.Address = mybuyeraddress;

                myorder.OrderHeader.SellerParty.PartyNumber.Qualifier = TecCom.OpenMessaging.Order.TXML.V4_1.Common.PartyNumberTypeQualifiers.BuyerNumber;
                myorder.OrderHeader.SellerParty.PartyNumber.PartyNumberValue = businessRelation.SupplierNumber;

                var myselleraddress = new TecCom.OpenMessaging.Order.TXML.V4_1.Common.AddressType();
                myselleraddress.Name1 = supplier.Name1;
                myselleraddress.Name2 = supplier.Name2;
                myselleraddress.Street1 = supplier.Street1;
                myselleraddress.PostalCode = supplier.PostalCode;
                myselleraddress.City = supplier.City;
                myselleraddress.CountryCode = supplier.Country.CountryCode;
                myselleraddress.CountryName = supplier.Country.CountryName;

                myorder.OrderHeader.SellerParty.Address = myselleraddress;

                if (deliveryParty != null)
                {
                    myorder.OrderHeader.DeliveryParty = new TecCom.OpenMessaging.Order.TXML.V4_1.Common.PartyType2();
                    myorder.OrderHeader.DeliveryParty.PartyNumber = new TecCom.OpenMessaging.Order.TXML.V4_1.Common.PartyNumberType();
                    myorder.OrderHeader.DeliveryParty.PartyNumber.PartyNumberValue = deliveryParty.Code;
                    myorder.OrderHeader.DeliveryParty.PartyNumber.Qualifier = PartyNumberTypeQualifiers.SupplierNumber;
                    TecCom.OpenMessaging.Order.TXML.V4_1.Common.AddressType mydeliverypartyaddress = new TecCom.OpenMessaging.Order.TXML.V4_1.Common.AddressType();
                    mydeliverypartyaddress.Name1 = deliveryParty.Name1;
                    mydeliverypartyaddress.Street1 = deliveryParty.Street1;
                    mydeliverypartyaddress.PostalCode = deliveryParty.PostalCode;
                    mydeliverypartyaddress.City = deliveryParty.City;
                    mydeliverypartyaddress.CountryCode = deliveryParty.Country.CountryCode;
                    mydeliverypartyaddress.CountryName = deliveryParty.Country.CountryName;
                    myorder.OrderHeader.DeliveryParty.Address = mydeliverypartyaddress;
                }

                myorder.OrderHeader.BuyerOrderNumber = "12345";
                myorder.OrderHeader.OrderIssueDate.DateValue.DateValue = DateTime.Now;
                myorder.OrderHeader.OrderProcessing.DispatchMode = DispatchModes.Express;
                myorder.OrderHeader.Currency = "EUR";

                var orderitems = new List<OrderItemType>();

                var position = 1;

                foreach (var product in products)
                {
                    var orderitem = new OrderItemType();

                    orderitem.PositionNumber = position;
                    orderitem.RequestedQuantity.Quantity.UoM = product.Uom;
                    orderitem.RequestedQuantity.Quantity.QuantityValue = product.Quantity;
                    orderitem.ProductId.MakerCode = product.MakerCode;
                    orderitem.ProductId.ProductNumber = product.ProductNumber;
                    orderitem.ProductId.Ean = product.Ean;
                    orderitem.ProductId.BuyerProductNumber = product.BuyerProductNumber;

                    var myproductdescriptions = new List<ProductName>();

                    var myproductdescription = new ProductName();
                    myproductdescription.Language = "..";
                    myproductdescription.ProductNameValue = product.ProductDescriptions.Where(x => x.Language == "EN").FirstOrDefault().Description;
                    myproductdescriptions.Add(myproductdescription);

                    orderitem.ProductDescription = myproductdescriptions.ToArray();

                    orderitems.Add(orderitem);

                    position++;
                }

                myorder.OrderItem = orderitems.ToArray();

                var myfunctioncaller = new FunctionCaller();
                myfunctioncaller.WebServiceURL = _WebServiceUrl;

                var myfunctioncallrequest = new FunctionCallRequest();
                myfunctioncallrequest.Authentication.User = _User;
                myfunctioncallrequest.Authentication.Password = _Password;
                myfunctioncallrequest.RequestedFunction.FunctionID = FunctionID.Order_SubmitOrder;

                var myparameterdata = new ParameterData[1];
                myparameterdata[0] = new ParameterData();
                myparameterdata[0].ParameterType = ParameterTypeID.Order;

                var RequestDocument = XMLHelper.GetXMLString(myorder);

                myparameterdata[0].ParameterValue = RequestDocument;

                myfunctioncallrequest.RequestedFunction.ParameterList = myparameterdata;

                var myfunctioncallresponse = default(FunctionCallResponse);

                var vallog = new TecCom.OpenMessaging.DTO.DTOValidationLog();
                var schema = new TecCom.OpenMessaging.DTO.DTOSchema(typeof(TecCom.OpenMessaging.DTO.FunctionCallRequest));
                schema.ValidateXmlDocument(XMLHelper.GetXMLString(myfunctioncallrequest), vallog);

                if (vallog.HighestSeverityLevel == SeverityLevel.FatalError)
                {
                    var error = vallog.ToString();
                }

                myfunctioncallresponse = myfunctioncaller.ProcessRequest(myfunctioncallrequest);

                var txt = string.Empty;
                var productsRes = new List<ResponseProduct>();

                if (myfunctioncallresponse.Status.Severity == SeverityLevel.Information)
                {

                    var ResponseDoc = myfunctioncallresponse.OriginatingFunction.ParameterList[0].ParameterValue;
                    try
                    {
                        ResponseDoc = TxmlHelper.EnsureTxmlOrdRspNamespaceInDocument(ResponseDoc);
                        TxmlOrdRsp OrderResponse = null;
                        OrderResponse = (TxmlOrdRsp)XMLHelper.GetObjectFromXML(typeof(TxmlOrdRsp), ResponseDoc);

                        var supOrder = new Order();
                        supOrder.Header = new OrderHeader
                        {
                            RefOrderId = OrderResponse.OrdRspHeader.OrderRef.BuyerOrderNumber
                        };

                        foreach (OrdRspItemType ri in OrderResponse.OrdRspItem)
                        {
                            productsRes.Add(new ResponseProduct
                            {
                                ResponseQuantity = ri.ConfirmedQuantity.Quantity.QuantityValue,
                                ResponseQuantityUom = ri.ConfirmedQuantity?.Quantity.UoM,
                                ItemInstruction = ri.ItemInstruction?.Value.ToString(),
                                PositionNumber = ri.PositionNumber.ToString(),
                                ProductDescriptions = ri.ProductDescription?.Select(x => new TecOrder.Dtos.ProductDescription
                                {
                                    Description = x.ProductNameValue,
                                    Language = x.Language
                                }).ToList(),
                                BuyerProductNumber = ri.ProductId?.BuyerProductNumber,
                                Ean = ri.ProductId?.Ean,
                                MakerCode = ri.ProductId?.MakerCode,
                                ProductNumber = ri.ProductId?.ProductNumber,
                                Prices = ri.ProductPrice?.ToList().Select(x => new TecOrder.Dtos.ProductPrice
                                {
                                    PriceStr = x.Price,
                                    PriceValue = x.PriceValue,
                                    ReferenceUnit = x.PriceUnit?.Quantity.UoM,
                                    ReferenceQuantity = x.PriceUnit?.Quantity.QuantityValue,
                                    PriceCategory = x.PriceCategory.ToString()
                                }).ToList(),
                                RequestedDeliveryDateStr = ri.RequestedDeliveryDate.DateValue.DateString,
                                RequestedQuantity = ri.RequestedQuantity.Quantity.QuantityValue,
                                RequestedQuantityUom = ri.RequestedQuantity.Quantity.UoM,
                                QtyVariances = ri.QtyVariance?.ToList().Select(x => new QuantityVariance
                                {
                                    DeviationReason = x.DeviationReason,
                                    ModificationReason = x.ModificationReason,
                                    QtyVarComment = x.QtyVarComment,
                                    QuantityUoM = x.Quantity?.UoM,
                                    QuantityValue = x.Quantity?.QuantityValue
                                }).ToList(),
                                Responses = ri.Response?.ToList().Select(x => new Response
                                {
                                    Code = x.Code,
                                    Class = (ResponseClass)x.ResponseClass,
                                    Parameter = x.Parameter,
                                    Type = x.Type,
                                    Text = x.Text
                                }).ToList(),
                                AlternativeProducts = ri.Alternative?.ToList().Select(x => new AlternativeProduct
                                {
                                    ProductNumber = x.ProductId?.ProductNumber,
                                    ProductDescriptions = x.ProductDescription?.Select(y => new TecOrder.Dtos.ProductDescription
                                    {
                                        Description = y.ProductNameValue,
                                        Language = y.Language
                                    }).ToList(),
                                    BuyerProductNumber = x.ProductId?.BuyerProductNumber,
                                    Ean = x.ProductId?.Ean,
                                    MakerCode = x.ProductId?.MakerCode,
                                    Price = new TecOrder.Dtos.ProductPrice
                                    {
                                        PriceCategory = x.ProductPrice?.PriceCategory.ToString(),
                                        PriceStr = x.ProductPrice?.Price,
                                        PriceValue = x.ProductPrice?.PriceValue,
                                        ReferenceQuantity = x.ProductPrice?.PriceUnit?.Quantity?.QuantityValue,
                                        ReferenceUnit = x.ProductPrice?.PriceUnit?.Quantity?.UoM
                                    }
                                }).ToList()
                            });
                        }

                        supOrder.Lines = new List<ResponseProduct>();
                        supOrder.Lines = productsRes;
                        order.Order = supOrder;
                        order.Message = string.Empty;
                        return order;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
                else
                {
                    throw new Exception(myfunctioncallresponse.Status.ToString());
                }

            }
            catch (Exception ex)
            {

                order.Message = ex.Message;
                return order;
            }
        }
        #endregion

        public DocumentsResponse GetDocuments(ParameterTypeID typeId, string filesPath)
        {
            var res = new DocumentsResponse();
            res.DesAdvList = new List<DesAdv>();
            res.OrdRspList = new List<OrdRsp>();
            res.InvoiceList = new List<Invoice>();

            try
            {
                //FunctionCallResponse response = null;
                //AuthenticationData authData = null;
                ServiceData businessData = null;
                ParameterData[] parameters = null;
                ParameterData param = null;

                var myfunctioncaller = new FunctionCaller();
                myfunctioncaller.WebServiceURL = _WebServiceUrl;

                var myfunctioncallrequest = new FunctionCallRequest();
                myfunctioncallrequest.Authentication.User = _User;
                myfunctioncallrequest.Authentication.Password = _Password;
                myfunctioncallrequest.Authentication.Language = language.en;
                myfunctioncallrequest.RequestedFunction.FunctionID = FunctionID.Order_GetDocuments;

                // set information about business process
                businessData = new ServiceData();
                businessData.FunctionID = FunctionID.Order_GetDocuments; // Function to receive messages
                parameters = new ParameterData[1];
                param = new ParameterData();

                param.ParameterType = typeId;
                parameters[0] = param;
                businessData.ParameterList = parameters;
                myfunctioncallrequest.RequestedFunction = businessData;
                //response = myfunctioncaller.ProcessRequest(myfunctioncallrequest);

                var myfunctioncallresponse = default(FunctionCallResponse);

                var vallog = new DTOValidationLog();
                var schema = new DTOSchema(typeof(FunctionCallRequest));
                schema.ValidateXmlDocument(XMLHelper.GetXMLString(myfunctioncallrequest), vallog);

                if (vallog.HighestSeverityLevel == SeverityLevel.FatalError)
                {
                    var error = vallog.ToString();
                }

                myfunctioncallresponse = myfunctioncaller.ProcessRequest(myfunctioncallrequest);

                var txt = string.Empty;

                if (myfunctioncallresponse.Status.Severity == SeverityLevel.Information)
                {
                    var myDocs = myfunctioncallresponse.OriginatingFunction.ParameterList;
                    if (myDocs == null)
                    {
                        return res;
                    }
                    for (int i = 0; i < myDocs.Length; i++)
                    {
                        try
                        {
                            var ResponseDoc = myDocs[i].ParameterValue;
                            try
                            {
                                ResponseDoc = TxmlHelper.EnsureTxmlOrdRspNamespaceInDocument(ResponseDoc);
                                if (typeId == ParameterTypeID.DesAdv)
                                {
                                    DesAdv response = (DesAdv)XMLHelper.GetObjectFromXML(typeof(DesAdv), ResponseDoc);
                                    var supplierNumber = response.DesAdvHeader.SellerParty.PartyNumber;
                                    filesPath = $@"{filesPath}\DESADV\{supplierNumber}";
                                    Helper.WriteXmlFile(ResponseDoc, typeId, filesPath, supplierNumber);
                                    res.DesAdvList.Add(response);
                                }
                                else if (typeId == ParameterTypeID.Invoice)
                                {
                                    Invoice response = (Invoice)XMLHelper.GetObjectFromXML(typeof(Invoice), ResponseDoc);
                                    var supplierNumber = response.InvoiceHeader.SellerParty.PartyNumber;
                                    filesPath = $@"{filesPath}\INVOIC\{supplierNumber}";
                                    Helper.WriteXmlFile(ResponseDoc, typeId, filesPath, supplierNumber);
                                    res.InvoiceList.Add(response);
                                }
                                else if (typeId == ParameterTypeID.OrdRsp)
                                {
                                    OrdRsp response = (OrdRsp)XMLHelper.GetObjectFromXML(typeof(OrdRsp), ResponseDoc);
                                    var supplierNumber = response.OrdRspHeader.SellerParty.PartyNumber;
                                    filesPath = $@"{filesPath}\ORDRSP\{supplierNumber}";
                                    Helper.WriteXmlFile(ResponseDoc, typeId, filesPath, supplierNumber);
                                    res.OrdRspList.Add(response);
                                }
                            }
                            catch (Exception ex)
                            {
                                res.Messages.Add(typeId + " - " + ex.Message);
                            }
                        }
                        catch (Exception ex)
                        {
                            res.Messages.Add(ex.ToString());
                        }
                    }
                }
                else
                {
                    txt += myfunctioncallresponse.Status.ToString();
                    txt += myfunctioncaller.LastRequestString;
                }

                res.Message = txt;
            }
            catch (Exception ex)
            {
                res.Messages.Add(typeId + " - " + ex.Message);
            }
            return res;
        }
    }
}