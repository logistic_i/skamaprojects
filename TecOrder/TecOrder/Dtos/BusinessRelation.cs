﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecOrder.Dtos
{
    public class BusinessRelation
    {
        public string BuyerNumber { get; set; }
        public string SupplierNumber { get; set; }
    }
}
