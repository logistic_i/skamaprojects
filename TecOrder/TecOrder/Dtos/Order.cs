﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecOrder.Dtos
{
    public class Order
    {
        public OrderHeader Header { get; set; }
        public List<ResponseProduct> Lines { get; set; }
    }
}
