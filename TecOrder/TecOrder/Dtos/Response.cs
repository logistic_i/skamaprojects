﻿using TecOrder.Enums;

namespace TecOrder.Dtos
{
    public class Response
    {
        public int Code { get; set; }
        public ResponseClass Class { get; set; }
        public string Type { get; set; }
        public string Text { get; set; }
        public string Parameter { get; set; }
    }
}