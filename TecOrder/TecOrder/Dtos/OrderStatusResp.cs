﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TecCom.OpenMessaging.DTO;

namespace TecOrder.Dtos
{
    public class OrderStatusResp
    {
        public StatusData Status { get; set; }
        public string ExceptionMsg { get; set; }
    }
}
