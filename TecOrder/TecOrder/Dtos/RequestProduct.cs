﻿namespace TecOrder.Dtos
{
    public class RequestProduct : BaseProduct
    {
        public string Uom { get; set; }
        public double Quantity { get; set; }
        public string PositionNumber { get; set; }
    }
}