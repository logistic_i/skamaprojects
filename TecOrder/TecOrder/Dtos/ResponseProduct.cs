﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace TecOrder.Dtos
{
    public class ResponseProduct : BaseProduct
    {
        public string PositionNumber { get; set; }
        public string ResponseQuantityUom { get; set; }
        public double ResponseQuantity { get; set; }
        public string ItemInstruction { get; set; }
        public List<ProductPrice> Prices { get; set; }
        public string RequestedDeliveryDateStr { get; set; }
        public DateTime RequestedDeliveryDate
        {
            get
            {
                return DateTime.TryParseExact
                    (
                    RequestedDeliveryDateStr,
                    "yyyyMMdd",
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None,
                    out DateTime dateValue
                    )
                    ? dateValue : DateTime.MinValue;
            }
        }
        public double? RequestedQuantity { get; set; }
        public string RequestedQuantityUom { get; set; }
        public List<QuantityVariance> QtyVariances { get; set; }
        public List<Response> Responses { get; set; }
        public List<AlternativeProduct> AlternativeProducts { get; internal set; }
    }
}
