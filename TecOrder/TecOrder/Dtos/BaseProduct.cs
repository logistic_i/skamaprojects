﻿using System.Collections.Generic;

namespace TecOrder.Dtos
{
    public class BaseProduct
    {
        public string ProductNumber { get; set; }
        public List<ProductDescription> ProductDescriptions { get; set; } = null;
        public string MakerCode { get; set; } = "*";
        public string Ean { get; set; }
        public string BuyerProductNumber { get; set; }
    }
}