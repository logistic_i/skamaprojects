﻿using System.Collections.Generic;

namespace TecOrder.Dtos
{
    public class DocumentsResponse
    {
        public string Message { get; set; }
        public List<ResponseProduct> List { get; set; }

        public List<DesAdv> DesAdvList { get; set; }
        public List<OrdRsp> OrdRspList { get; set; }
        public List<Invoice> InvoiceList { get; set; }
        public List<string> Messages { get; set; }
    }
}