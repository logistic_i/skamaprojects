﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecOrder.Dtos
{
    public class ProductPrice
    {
        public string PriceStr { get; set; }
        public string PriceCategory { get; set; }
        public string ReferenceUnit { get; set; }
        public decimal? PriceValue { get; set; }
        public double? ReferenceQuantity { get; internal set; }
    }
}
