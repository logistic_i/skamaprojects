﻿using System.Collections.Generic;

namespace TecOrder.Dtos
{
    public class AlternativeProduct : BaseProduct
    {
        public ProductPrice Price { get; set; }
    }
}
