﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TecOrder.Dtos
{
    public class QuantityVariance
    {
        public string DeviationReason { get; set; }
        public string ModificationReason { get; set; }
        public string QtyVarComment { get; set; }
        public double? QuantityValue { get; set; }
        public string QuantityUoM { get; set; }

    }
}
