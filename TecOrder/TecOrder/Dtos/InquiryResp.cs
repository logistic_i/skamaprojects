﻿using System.Collections.Generic;

namespace TecOrder.Dtos
{
    public class InquiryResp
    {
        public List<ResponseProduct> Products { get; set; }
        public string Message { get; set; }
    }
}