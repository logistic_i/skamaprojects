﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace MantisPrint
{
    class Program
    {
        static void Main(string[] args )
        {
            Log.Logger = new LoggerConfiguration()
                                .WriteTo.File("MyLogs.log")
                                .CreateLogger();

            //Log.Information("start");
            var listIDS =args[0];
            var printerName = args[1].Replace('"',' ');
            var TypeID = Convert.ToInt32(args[2]);//999 packing.dll 998 receiptskama.dll 997 SN Print
            var Quantity = Convert.ToInt32(args[3]);
            var DocID = Convert.ToInt32(args[4]);

            //Log.Information(printerName);
            //Log.Information(listIDS);

            var Ids = Array.ConvertAll(listIDS.Split(';'), int.Parse).ToList();
            try
            {
                //Log.Information("try to login");
                using (var xSupport = SoftoneClient.Login(
                                          Properties.Settings.Default.XCOName
                                        , Properties.Settings.Default.UserName
                                        , Properties.Settings.Default.Password
                                        , Properties.Settings.Default.Company
                                        , Properties.Settings.Default.Branch
                                        , Properties.Settings.Default.SoftOnePath
                                        , DateTime.Now
                                        ))
                {

                    if (xSupport != null)
                    {
                        Log.Information($"logged in with {xSupport.Info.UserName}"); 
                        var print = new Print(xSupport);
                        if (TypeID == 999)
                        {
                            for (int i = 0; i < Ids.Count; i++)
                            {
                                var idd = Ids[i];
                                //Log.Information($"print {Ids[i]} with printer name: {printerName}");
                                print.PrintSoftoneReport(Ids[i], printerName);
                            }
                        }
                        if (TypeID == 997)
                        {
                            for (int i = 0; i < Ids.Count; i++)
                            {
                                var idd = Ids[i];
                                //Log.Information($"print {Ids[i]} with printer name: {printerName}");
                                print.PrintSoftoneReportSN(Ids[i], printerName);
                            }
                        }
                        else if(TypeID == 998)
                        {
                            for (int x = 0; x < Ids.Count; x++)
                            {
                                for (int y = 1; y <= Quantity; y++)
                                {
                                    var idd = Ids[x];
                                    //Log.Information($"print {Ids[i]} with printer name: {printerName}");
                                    print.PrintSoftoneReportReceipt(Ids[x], printerName);
                                }
                            }
                        }

                        
                    }
                    else
                    {
                        Log.Error("xSupport is null");
                        Log.Information("xSupport is null");
                    }
                    
                }
            }
            catch(Exception ex)
            {
                var msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += " | " + ex.InnerException.Message;
                }
                Log.Error(msg);
                throw;
            }
            //Log.Information("stop");
        }
    }
}
