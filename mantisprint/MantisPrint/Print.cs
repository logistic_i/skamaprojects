﻿
using Serilog;
using Softone;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MantisPrint
{
    public class Print
    {
        private XSupport xSupport;
        public Print(XSupport _xSupport)
        {
            xSupport = _xSupport;
        }

        internal void PrintSoftoneReport(int cpspID,string PrinterName)
        {
            try
            {

                var query = $"select * from CCCPrintPackLblMantis where cpspID= {cpspID}";
                using (var ds = xSupport.GetSQLDataSet(query, null))
                {
                    if (ds.Count > 0)
                    {
                        //Log.Information("Ds Count"+ds.Count);
                        using (var printObj = xSupport.CreateModule("CCCMantisPrintlblPack"))
                        {
                            HideWarningsFromS1Module(printObj);
                            printObj.LocateData(cpspID);
                            object[] myArray;
                            myArray = new object[4];
                            object SysRequest = xSupport.GetStockObj("SysRequest", true);
                            myArray[0] = printObj.Handle;
                            myArray[1] = "1";
                            //Log.Information(PrinterName);
                            var printerNameStr = PrinterName.ToString();
                            //Log.Information(printerNameStr);                         
                            //myArray[2] = "PDF file";
                            //myArray[3] = "PDF file.pdf";
                            myArray[2] = $"{printerNameStr}";
                            var res = xSupport.CallPublished(SysRequest, "PrintForm", myArray);
                            //Log.Information(res.ToString());
                        }
                    }
                }
              
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal void PrintSoftoneReportReceipt(int ID, string PrinterName)
        {
            try
            {

                var query = $"SELECT * FROM am_PrintProductReceiptLbl where ID= {ID}";
                using (var ds = xSupport.GetSQLDataSet(query, null))
                {
                    if (ds.Count > 0)
                    {
                        //Log.Information("Ds Count"+ds.Count);
                        using (var printObj = xSupport.CreateModule("CCCMantisPrintReceiptLbl"))
                        {
                            HideWarningsFromS1Module(printObj);
                            printObj.LocateData(ID);
                            object[] myArray;
                            myArray = new object[4];
                            object SysRequest = xSupport.GetStockObj("SysRequest", true);
                            myArray[0] = printObj.Handle;
                            myArray[1] = "1";
                            //Log.Information(PrinterName);
                            var printerNameStr = PrinterName.ToString();
                            //Log.Information(printerNameStr);                         
                            //myArray[2] = "PDF file";
                            //myArray[3] = "PDF file.pdf";
                            myArray[2] = $"{printerNameStr}";
                            var res = xSupport.CallPublished(SysRequest, "PrintForm", myArray);
                            //Log.Information(res.ToString());
                        }
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }


        internal void PrintSoftoneReportSN(int ID, string PrinterName)
        {
            try
            {

                var query = $"select * from CCCCOMMITEDQTIES where id =  {ID}";
                using (var ds = xSupport.GetSQLDataSet(query, null))
                {
                    if (ds.Count > 0)
                    {
                        //Log.Information("Ds Count"+ds.Count);
                        using (var printObj = xSupport.CreateModule("CCCMantisPrintProductSN"))
                        {
                            HideWarningsFromS1Module(printObj);
                            printObj.LocateData(ID);
                            object[] myArray;
                            myArray = new object[4];
                            object SysRequest = xSupport.GetStockObj("SysRequest", true);
                            myArray[0] = printObj.Handle;
                            myArray[1] = "1";
                            //Log.Information(PrinterName);
                            var printerNameStr = PrinterName.ToString();
                            //Log.Information(printerNameStr);                         
                            //myArray[2] = "PDF file";
                            //myArray[3] = "PDF file.pdf";
                            myArray[2] = $"{printerNameStr}";
                            var res = xSupport.CallPublished(SysRequest, "PrintForm", myArray);
                            //Log.Information(res.ToString());
                        }
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        private void HideWarningsFromS1Module(XModule module)
        {
            object otherModule = xSupport.GetStockObj("ModuleIntf", true);
            object[] myArray1;
            myArray1 = new object[3];
            myArray1[0] = module.Handle;
            myArray1[1] = "WARNINGS";    //Param Name
            myArray1[2] = "OFF";         //Param Value
            xSupport.CallPublished(otherModule, "SetParamValue", myArray1);
        }

    }
}