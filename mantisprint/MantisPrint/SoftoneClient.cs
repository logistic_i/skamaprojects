﻿
using Softone;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MantisPrint
{
    internal static class SoftoneClient
    {
        internal static XSupport Login(string xcoName, string userName, string password, int company, int branch, string softOnePath, DateTime loginDate)
        {
            try
            {
                XSupport.InitInterop(0, Path.Combine(softOnePath, "XDll.dll"));
                XSupport xSupport = XSupport.Login(xcoName, userName, password, company, branch, loginDate);
                return xSupport;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}