﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using B2BOrderManager.Models;
using Softone;
using Serilog;
using System.Data;
using System.Dynamic;

namespace B2BOrderManager
{
    public class OrderManager
    {
        private XSupport _xSupport;
        public OrderManager(XSupport xSupport)
        {
            _xSupport = xSupport;
        }

        internal List<Order> GetApprovedOrders()
        {
            var orders = new List<Order>();
            try
            {
                var query = "SELECT FINDOC, FINCODE, TRNDATE FROM GT_B2BAPPROVEDORDERS";
                using (var ds = _xSupport.GetSQLDataSet(query, null))
                {
                    if (ds.Count > 0)
                    {
                        for (int i = 0; i < ds.Count; i++)
                        {
                            var order = new Order
                            {
                                Id = ds.GetAsInteger(i, "FINDOC"),
                                OrderCode = ds.GetAsString(i, "FINCODE"),
                                OrderDate = ds.GetAsDateTime(i, "TRNDATE")
                            };
                            orders.Add(order);
                        }
                    }
                }
                return orders;
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal void CreateWMSOrders(List<Order> b2bOrders)
        {
            var s1Tools = new S1Tools(_xSupport);
            using (var Saldoc = _xSupport.CreateModule("SALDOC"))
            {
                foreach (var order in b2bOrders)
                {
                    try
                    {
                        Saldoc.LocateData(order.Id);
                        var whouse = Convert.ToInt32(Saldoc.GetTable("MTRDOC").Current["WHOUSE"]);
                        var parWSeries = s1Tools.GetIdFromMemoryTable("SERIES", "SOSOURCE;FPRMS;WHOUSE", "SERIES", 1351, 10143, whouse);

                        var data = new
                        {
                            SALDOC = new
                            {
                                TRNDATE = Convert.ToDateTime(Saldoc.GetTable("SALDOC").Current["TRNDATE"]).ToString("yyyy-MM-dd"),
                                SERIES = parWSeries,
                                TRDR = Convert.ToInt32(Saldoc.GetTable("SALDOC").Current["TRDR"]),
                                TRDBRANCH = Saldoc.GetTable("SALDOC").Current["TRDBRANCH"] != DBNull.Value
                                                        ? Convert.ToInt32(Saldoc.GetTable("SALDOC").Current["TRDBRANCH"])
                                                        : (int?)null,
                                PRIORITY = Saldoc.GetTable("SALDOC").Current["PRIORITY"] != DBNull.Value
                                                        ? Convert.ToInt32(Saldoc.GetTable("SALDOC").Current["PRIORITY"])
                                                        : (int?)null,
                                COMMENTS = Saldoc.GetTable("SALDOC").Current["COMMENTS"],
                                CCCINFOORDER = Saldoc.GetTable("SALDOC").Current["CCCINFOORDER"] != DBNull.Value
                                                        ? Convert.ToInt32(Saldoc.GetTable("SALDOC").Current["CCCINFOORDER"])
                                                        : (int?)null,
                                CONVMODE = 1
                            },
                            MTRDOC = new
                            {
                                SOCARRIER = Saldoc.GetTable("MTRDOC").Current["SOCARRIER"] != DBNull.Value
                                                    ? Convert.ToInt32(Saldoc.GetTable("MTRDOC").Current["SOCARRIER"])
                                                    : (int?)null,
                                ROUTING = Saldoc.GetTable("MTRDOC").Current["ROUTING"] != DBNull.Value
                                                    ? Convert.ToInt32(Saldoc.GetTable("MTRDOC").Current["ROUTING"])
                                                    : (int?)null
                            },
                            ITELINES = Saldoc.GetTable("ITELINES").CreateDataTable(true).AsEnumerable().Select(x => new
                            {
                                MTRL = Convert.ToInt32(x["MTRL"]),
                                QTY1 = Convert.ToDouble(x["QTY1"]),
                                PRICE = Convert.ToDouble(x["PRICE"]),
                                DISC1PRC = x["DISC1PRC"] != DBNull.Value ? Convert.ToDouble(x["DISC1PRC"]) : (double?)null,
                                DISC2PRC = x["DISC2PRC"] != DBNull.Value ? Convert.ToDouble(x["DISC2PRC"]) : (double?)null,
                                DISC3PRC = x["DISC3PRC"] != DBNull.Value ? Convert.ToDouble(x["DISC3PRC"]) : (double?)null,
                                COMMENTS = !string.IsNullOrEmpty(x["COMMENTS"].ToString()) ? x["COMMENTS"].ToString() : null,
                                FINDOCS = Convert.ToInt32(x["FINDOC"]),
                                MTRLINESS = Convert.ToInt32(x["MTRLINES"])
                            }).ToList()
                        };

                        dynamic paramss = new ExpandoObject();
                        paramss.ISCUSTOM = 1;
                        SetDataResponse res = s1Tools.SetData(Saldoc.ObjName, "", data, paramss);
                        if (!res.Success)
                        {
                            throw new Exception($"OrderNumber : {order.OrderCode} | {res.Error}");
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex.Message);
                    }
                }
            }
        }
    }
}
