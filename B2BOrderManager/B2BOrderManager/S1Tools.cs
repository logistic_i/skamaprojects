﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using B2BOrderManager.Models;
using Newtonsoft.Json;
using Softone;

namespace B2BOrderManager
{
    public class S1Tools
    {
        private XSupport _xSupport;
        public S1Tools(XSupport xSupport)
        {
            _xSupport = xSupport;
        }

        internal int GetIdFromMemoryTable(string tableName, string keys, string returnValue, params object[] values)
        {
            var seriesTbl = _xSupport.GetMemoryTable(tableName);
            var index = seriesTbl.Find(keys, values);
            if (index == -1)
            {
                return index;
            }
            var id = seriesTbl.GetAsInteger(index, returnValue);
            return id;
        }

        internal SetDataResponse SetData(string objectName, string formName, object data, dynamic paramss)
        {
            try
            {
                paramss.WARNINGS = "OFF";
                paramss.NOMESSAGES = 1;

                var req = new SetDataRequest
                {
                    Object = objectName,
                    ObjectParams = paramss,
                    Form = formName,
                    Data = data
                };

                var requestStr = JsonConvert.SerializeObject(req);
                var resultStr = _xSupport.CallWebService(requestStr, out string contentType);
                var res = JsonConvert.DeserializeObject<SetDataResponse>(resultStr);
                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
