﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softone;

namespace B2BOrderManager
{
    public static class SoftoneClient
    {
        public static string XCOName { get; set; } = Properties.Settings.Default.XCOName;
        public static string Username { get; set; } = Properties.Settings.Default.UserName;
        public static string Password { get; set; } = Properties.Settings.Default.Password;
        public static int Company { get; set; } = Properties.Settings.Default.Company;
        public static int Branch { get; set; } = Properties.Settings.Default.Branch;
        public static string SoftonePath { get; set; } = Properties.Settings.Default.SoftOnePath;

        internal static XSupport Login()
        {
            try
            {
                XSupport.InitInterop(0, Path.Combine(SoftonePath, "XDll.dll"));

                XSupport xSupport = XSupport.Login(XCOName, Username, Password, Company, Branch, DateTime.Now);

                return xSupport;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
