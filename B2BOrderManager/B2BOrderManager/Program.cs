﻿using Serilog;
using System;
using System.Linq;

namespace B2BOrderManager
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.Debug()
               .WriteTo.File("logs\\b2bManager.log", rollingInterval: RollingInterval.Day, retainedFileCountLimit: 7)
               .CreateLogger();

            try
            {
                var s1Client = SoftoneClient.Login();
                if (s1Client == null)
                {
                    throw new Exception("Softone client is null!");
                }
                var orderManager = new OrderManager(s1Client);
                var b2bOrders = orderManager.GetApprovedOrders();
                if (!b2bOrders.Any())
                {
                    throw new Exception("Approved Β2Β orders not found!");
                }
                orderManager.CreateWMSOrders(b2bOrders);

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
    }
}
