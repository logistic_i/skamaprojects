﻿namespace Skama
{
    partial class CounterFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CounterFrm));
            this.pnlFilters = new DevExpress.XtraEditors.PanelControl();
            this.btnExtendedSearch = new DevExpress.XtraEditors.SimpleButton();
            this.btnSearch = new DevExpress.XtraEditors.SimpleButton();
            this.txtSearch = new DevExpress.XtraEditors.TextEdit();
            this.lblSearch = new DevExpress.XtraEditors.LabelControl();
            this.pnlProgress = new DevExpress.XtraEditors.PanelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.txtEndoQty = new DevExpress.XtraEditors.TextEdit();
            this.txtCusQty = new DevExpress.XtraEditors.TextEdit();
            this.lblEndoQty = new DevExpress.XtraEditors.LabelControl();
            this.lblCusQty = new DevExpress.XtraEditors.LabelControl();
            this.lblFooter = new DevExpress.XtraEditors.LabelControl();
            this.pnlInfo = new DevExpress.XtraEditors.PanelControl();
            this.gridInfo = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ABCRanking = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PriceR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CustomerPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TotalRemain = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WHRemain = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WHExpected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WHReserved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Expected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Reserved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ActualRemain = new DevExpress.XtraGrid.Columns.GridColumn();
            this.InitQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.OrderQty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnlAvailability = new DevExpress.XtraEditors.PanelControl();
            this.gridAvailability = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.virtualServerModeSource = new DevExpress.Data.VirtualServerModeSource(this.components);
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Skama.WaitForm1), true, true);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pnlFilters)).BeginInit();
            this.pnlFilters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSearch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlProgress)).BeginInit();
            this.pnlProgress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndoQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCusQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInfo)).BeginInit();
            this.pnlInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlAvailability)).BeginInit();
            this.pnlAvailability.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAvailability)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.virtualServerModeSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlFilters
            // 
            this.pnlFilters.Controls.Add(this.btnExtendedSearch);
            this.pnlFilters.Controls.Add(this.btnSearch);
            this.pnlFilters.Controls.Add(this.txtSearch);
            this.pnlFilters.Controls.Add(this.lblSearch);
            this.pnlFilters.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlFilters.Location = new System.Drawing.Point(0, 0);
            this.pnlFilters.Name = "pnlFilters";
            this.pnlFilters.Size = new System.Drawing.Size(1264, 47);
            this.pnlFilters.TabIndex = 0;
            // 
            // btnExtendedSearch
            // 
            this.btnExtendedSearch.Appearance.Font = new System.Drawing.Font("Tahoma", 10.25F, System.Drawing.FontStyle.Bold);
            this.btnExtendedSearch.Appearance.Options.UseFont = true;
            this.btnExtendedSearch.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnExtendedSearch.ImageOptions.SvgImage")));
            this.btnExtendedSearch.Location = new System.Drawing.Point(824, 5);
            this.btnExtendedSearch.Name = "btnExtendedSearch";
            this.btnExtendedSearch.Size = new System.Drawing.Size(204, 37);
            this.btnExtendedSearch.TabIndex = 5;
            this.btnExtendedSearch.Text = "Extra Αναζήτηση";
            this.btnExtendedSearch.Click += new System.EventHandler(this.btnExtendedSearch_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Appearance.Font = new System.Drawing.Font("Tahoma", 10.25F, System.Drawing.FontStyle.Bold);
            this.btnSearch.Appearance.Options.UseFont = true;
            this.btnSearch.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnSearch.ImageOptions.SvgImage")));
            this.btnSearch.Location = new System.Drawing.Point(585, 4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(195, 37);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "Αναζήτηση";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.EditValue = "";
            this.txtSearch.Location = new System.Drawing.Point(153, 12);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(393, 20);
            this.txtSearch.TabIndex = 3;
            // 
            // lblSearch
            // 
            this.lblSearch.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblSearch.Appearance.Options.UseFont = true;
            this.lblSearch.Location = new System.Drawing.Point(12, 12);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(135, 17);
            this.lblSearch.TabIndex = 2;
            this.lblSearch.Text = "Πεδίο αναζήτησης :";
            // 
            // pnlProgress
            // 
            this.pnlProgress.Controls.Add(this.btnCancel);
            this.pnlProgress.Controls.Add(this.btnOk);
            this.pnlProgress.Controls.Add(this.txtEndoQty);
            this.pnlProgress.Controls.Add(this.txtCusQty);
            this.pnlProgress.Controls.Add(this.lblEndoQty);
            this.pnlProgress.Controls.Add(this.lblCusQty);
            this.pnlProgress.Controls.Add(this.lblFooter);
            this.pnlProgress.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlProgress.Location = new System.Drawing.Point(0, 877);
            this.pnlProgress.Name = "pnlProgress";
            this.pnlProgress.Size = new System.Drawing.Size(1264, 44);
            this.pnlProgress.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F, System.Drawing.FontStyle.Bold);
            this.btnCancel.Appearance.Options.UseFont = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnCancel.ImageOptions.SvgImage")));
            this.btnCancel.Location = new System.Drawing.Point(1103, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(149, 37);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.Text = "Ακύρωση";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F, System.Drawing.FontStyle.Bold);
            this.btnOk.Appearance.Options.UseFont = true;
            this.btnOk.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnOk.ImageOptions.SvgImage")));
            this.btnOk.Location = new System.Drawing.Point(932, 5);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(149, 37);
            this.btnOk.TabIndex = 15;
            this.btnOk.Text = "Αποδοχή";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtEndoQty
            // 
            this.txtEndoQty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEndoQty.EditValue = "0";
            this.txtEndoQty.Location = new System.Drawing.Point(864, 12);
            this.txtEndoQty.Name = "txtEndoQty";
            this.txtEndoQty.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.txtEndoQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.txtEndoQty.Properties.Appearance.Options.UseBackColor = true;
            this.txtEndoQty.Properties.Appearance.Options.UseFont = true;
            this.txtEndoQty.Properties.ReadOnly = true;
            this.txtEndoQty.Size = new System.Drawing.Size(40, 26);
            this.txtEndoQty.TabIndex = 14;
            // 
            // txtCusQty
            // 
            this.txtCusQty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCusQty.EditValue = "0";
            this.txtCusQty.Location = new System.Drawing.Point(679, 12);
            this.txtCusQty.Name = "txtCusQty";
            this.txtCusQty.Properties.Appearance.BackColor = System.Drawing.Color.Red;
            this.txtCusQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.txtCusQty.Properties.Appearance.Options.UseBackColor = true;
            this.txtCusQty.Properties.Appearance.Options.UseFont = true;
            this.txtCusQty.Properties.ReadOnly = true;
            this.txtCusQty.Size = new System.Drawing.Size(40, 26);
            this.txtCusQty.TabIndex = 13;
            this.txtCusQty.TextChanged += new System.EventHandler(this.txtCusQty_TextChanged);
            // 
            // lblEndoQty
            // 
            this.lblEndoQty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEndoQty.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblEndoQty.Appearance.Options.UseFont = true;
            this.lblEndoQty.Location = new System.Drawing.Point(737, 16);
            this.lblEndoQty.Name = "lblEndoQty";
            this.lblEndoQty.Size = new System.Drawing.Size(115, 17);
            this.lblEndoQty.TabIndex = 2;
            this.lblEndoQty.Text = "Ενδοδιακινήσεις:";
            // 
            // lblCusQty
            // 
            this.lblCusQty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCusQty.Appearance.Font = new System.Drawing.Font("Tahoma", 10.25F, System.Drawing.FontStyle.Bold);
            this.lblCusQty.Appearance.Options.UseFont = true;
            this.lblCusQty.Location = new System.Drawing.Point(533, 15);
            this.lblCusQty.Name = "lblCusQty";
            this.lblCusQty.Size = new System.Drawing.Size(136, 17);
            this.lblCusQty.TabIndex = 1;
            this.lblCusQty.Text = "Σύνολο ποσότητας:";
            // 
            // lblFooter
            // 
            this.lblFooter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblFooter.Appearance.Font = new System.Drawing.Font("Tahoma", 10.25F, System.Drawing.FontStyle.Bold);
            this.lblFooter.Appearance.Options.UseFont = true;
            this.lblFooter.AutoEllipsis = true;
            this.lblFooter.Location = new System.Drawing.Point(12, 15);
            this.lblFooter.Name = "lblFooter";
            this.lblFooter.Size = new System.Drawing.Size(64, 17);
            this.lblFooter.TabIndex = 0;
            this.lblFooter.Text = "ItemInfo";
            // 
            // pnlInfo
            // 
            this.pnlInfo.Controls.Add(this.gridInfo);
            this.pnlInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlInfo.Location = new System.Drawing.Point(0, 47);
            this.pnlInfo.Name = "pnlInfo";
            this.pnlInfo.Size = new System.Drawing.Size(1264, 457);
            this.pnlInfo.TabIndex = 2;
            // 
            // gridInfo
            // 
            this.gridInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridInfo.EmbeddedNavigator.Appearance.BackColor = System.Drawing.Color.White;
            this.gridInfo.EmbeddedNavigator.Appearance.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.gridInfo.EmbeddedNavigator.Appearance.Options.UseBackColor = true;
            this.gridInfo.EmbeddedNavigator.Appearance.Options.UseFont = true;
            this.gridInfo.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridInfo.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridInfo.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridInfo.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridInfo.EmbeddedNavigator.Buttons.First.Visible = false;
            this.gridInfo.EmbeddedNavigator.Buttons.Last.Visible = false;
            this.gridInfo.EmbeddedNavigator.Buttons.Next.Visible = false;
            this.gridInfo.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridInfo.EmbeddedNavigator.Buttons.Prev.Visible = false;
            this.gridInfo.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridInfo.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridInfo.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(0, 1, true, true, "", "PrevPage"),
            new DevExpress.XtraEditors.NavigatorCustomButton(4, 4, true, true, "", "NextPage")});
            this.gridInfo.EmbeddedNavigator.TextStringFormat = "Σελίδα {0} από {1}";
            this.gridInfo.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridInfo_EmbeddedNavigator_ButtonClick);
            this.gridInfo.Location = new System.Drawing.Point(2, 2);
            this.gridInfo.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
            this.gridInfo.MainView = this.gridView1;
            this.gridInfo.Name = "gridInfo";
            this.gridInfo.Size = new System.Drawing.Size(1260, 453);
            this.gridInfo.TabIndex = 1;
            this.gridInfo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.gridView1.Appearance.FocusedRow.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.gridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id,
            this.Code,
            this.Description,
            this.ABCRanking,
            this.Price,
            this.PriceR,
            this.CustomerPrice,
            this.TotalRemain,
            this.WHRemain,
            this.WHExpected,
            this.WHReserved,
            this.Expected,
            this.Reserved,
            this.ActualRemain,
            this.InitQty,
            this.OrderQty});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridView1.GridControl = this.gridInfo;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridView1.OptionsCustomization.AllowColumnMoving = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView1_RowStyle);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.ShownEditor += new System.EventHandler(this.gridView1_ShownEditor);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanged);
            this.gridView1.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView1_CellValueChanging);
            this.gridView1.BeforeLeaveRow += new DevExpress.XtraGrid.Views.Base.RowAllowEventHandler(this.gridView1_BeforeLeaveRow);
            this.gridView1.AsyncCompleted += new System.EventHandler(this.gridView1_AsyncCompleted);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.DataSourceChanged += new System.EventHandler(this.gridView1_DataSourceChanged);
            // 
            // Id
            // 
            this.Id.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.Id.AppearanceHeader.Options.UseFont = true;
            this.Id.Caption = "Id";
            this.Id.FieldName = "Id";
            this.Id.Name = "Id";
            // 
            // Code
            // 
            this.Code.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.Code.AppearanceCell.Options.UseFont = true;
            this.Code.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.Code.AppearanceHeader.Options.UseFont = true;
            this.Code.Caption = "Κωδικός";
            this.Code.FieldName = "Code";
            this.Code.Name = "Code";
            this.Code.OptionsColumn.AllowEdit = false;
            this.Code.OptionsColumn.AllowFocus = false;
            this.Code.OptionsColumn.ReadOnly = true;
            this.Code.Visible = true;
            this.Code.VisibleIndex = 0;
            this.Code.Width = 95;
            // 
            // Description
            // 
            this.Description.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.Description.AppearanceCell.Options.UseFont = true;
            this.Description.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.Description.AppearanceHeader.Options.UseFont = true;
            this.Description.Caption = "Περιγραφή";
            this.Description.FieldName = "Description";
            this.Description.Name = "Description";
            this.Description.OptionsColumn.AllowEdit = false;
            this.Description.OptionsColumn.AllowFocus = false;
            this.Description.OptionsColumn.ReadOnly = true;
            this.Description.Visible = true;
            this.Description.VisibleIndex = 1;
            this.Description.Width = 95;
            // 
            // ABCRanking
            // 
            this.ABCRanking.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.ABCRanking.AppearanceCell.Options.UseFont = true;
            this.ABCRanking.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.75F);
            this.ABCRanking.AppearanceHeader.Options.UseFont = true;
            this.ABCRanking.Caption = "ABC";
            this.ABCRanking.FieldName = "ABCRanking";
            this.ABCRanking.Name = "ABCRanking";
            this.ABCRanking.OptionsColumn.AllowEdit = false;
            this.ABCRanking.OptionsColumn.AllowFocus = false;
            this.ABCRanking.Visible = true;
            this.ABCRanking.VisibleIndex = 2;
            this.ABCRanking.Width = 95;
            // 
            // Price
            // 
            this.Price.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.Price.AppearanceCell.Options.UseFont = true;
            this.Price.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.Price.AppearanceHeader.Options.UseFont = true;
            this.Price.Caption = "Τιμή χονδρικής";
            this.Price.FieldName = "Price";
            this.Price.Name = "Price";
            this.Price.OptionsColumn.AllowEdit = false;
            this.Price.OptionsColumn.AllowFocus = false;
            this.Price.OptionsColumn.ReadOnly = true;
            this.Price.Visible = true;
            this.Price.VisibleIndex = 3;
            this.Price.Width = 95;
            // 
            // PriceR
            // 
            this.PriceR.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.PriceR.AppearanceCell.Options.UseFont = true;
            this.PriceR.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.PriceR.AppearanceHeader.Options.UseFont = true;
            this.PriceR.Caption = "Τιμή λιανικής";
            this.PriceR.FieldName = "PriceR";
            this.PriceR.Name = "PriceR";
            this.PriceR.OptionsColumn.AllowEdit = false;
            this.PriceR.OptionsColumn.AllowFocus = false;
            this.PriceR.OptionsColumn.ReadOnly = true;
            this.PriceR.Visible = true;
            this.PriceR.VisibleIndex = 4;
            this.PriceR.Width = 95;
            // 
            // CustomerPrice
            // 
            this.CustomerPrice.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.CustomerPrice.AppearanceCell.Options.UseFont = true;
            this.CustomerPrice.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.CustomerPrice.AppearanceHeader.Options.UseFont = true;
            this.CustomerPrice.Caption = "Τιμή πελάτη";
            this.CustomerPrice.FieldName = "CustomerPrice";
            this.CustomerPrice.Name = "CustomerPrice";
            this.CustomerPrice.OptionsColumn.AllowEdit = false;
            this.CustomerPrice.OptionsColumn.AllowFocus = false;
            this.CustomerPrice.OptionsColumn.ReadOnly = true;
            this.CustomerPrice.Width = 95;
            // 
            // TotalRemain
            // 
            this.TotalRemain.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.TotalRemain.AppearanceCell.Options.UseFont = true;
            this.TotalRemain.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.TotalRemain.AppearanceHeader.Options.UseFont = true;
            this.TotalRemain.Caption = "Συνολικό διαθέσιμο";
            this.TotalRemain.FieldName = "TotalRemain";
            this.TotalRemain.Name = "TotalRemain";
            this.TotalRemain.OptionsColumn.AllowEdit = false;
            this.TotalRemain.OptionsColumn.AllowFocus = false;
            this.TotalRemain.OptionsColumn.ReadOnly = true;
            this.TotalRemain.Visible = true;
            this.TotalRemain.VisibleIndex = 5;
            this.TotalRemain.Width = 95;
            // 
            // WHRemain
            // 
            this.WHRemain.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.WHRemain.AppearanceCell.Options.UseFont = true;
            this.WHRemain.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.WHRemain.AppearanceHeader.Options.UseFont = true;
            this.WHRemain.Caption = "Υπόλοιπο Α.Χ.";
            this.WHRemain.FieldName = "WHRemain";
            this.WHRemain.Name = "WHRemain";
            this.WHRemain.OptionsColumn.AllowEdit = false;
            this.WHRemain.OptionsColumn.AllowFocus = false;
            this.WHRemain.OptionsColumn.ReadOnly = true;
            this.WHRemain.Visible = true;
            this.WHRemain.VisibleIndex = 6;
            this.WHRemain.Width = 95;
            // 
            // WHExpected
            // 
            this.WHExpected.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.WHExpected.AppearanceCell.Options.UseFont = true;
            this.WHExpected.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.WHExpected.AppearanceHeader.Options.UseFont = true;
            this.WHExpected.Caption = "Αναμενόμενα Α.Χ.";
            this.WHExpected.FieldName = "WHExpected";
            this.WHExpected.Name = "WHExpected";
            this.WHExpected.OptionsColumn.AllowEdit = false;
            this.WHExpected.OptionsColumn.AllowFocus = false;
            this.WHExpected.OptionsColumn.ReadOnly = true;
            this.WHExpected.Visible = true;
            this.WHExpected.VisibleIndex = 7;
            this.WHExpected.Width = 95;
            // 
            // WHReserved
            // 
            this.WHReserved.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.WHReserved.AppearanceCell.Options.UseFont = true;
            this.WHReserved.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.WHReserved.AppearanceHeader.Options.UseFont = true;
            this.WHReserved.Caption = "Δεσμευμένα Α.Χ.";
            this.WHReserved.FieldName = "WHReserved";
            this.WHReserved.Name = "WHReserved";
            this.WHReserved.OptionsColumn.AllowEdit = false;
            this.WHReserved.OptionsColumn.AllowFocus = false;
            this.WHReserved.OptionsColumn.ReadOnly = true;
            this.WHReserved.Visible = true;
            this.WHReserved.VisibleIndex = 8;
            this.WHReserved.Width = 95;
            // 
            // Expected
            // 
            this.Expected.Caption = "Αναμενόμενα Εταιρίας";
            this.Expected.FieldName = "Expected";
            this.Expected.Name = "Expected";
            this.Expected.OptionsColumn.AllowEdit = false;
            this.Expected.OptionsColumn.AllowFocus = false;
            // 
            // Reserved
            // 
            this.Reserved.Caption = "Δεσμευμένα Εταιρίας";
            this.Reserved.FieldName = "Reserved";
            this.Reserved.Name = "Reserved";
            this.Reserved.OptionsColumn.AllowEdit = false;
            this.Reserved.OptionsColumn.AllowFocus = false;
            // 
            // ActualRemain
            // 
            this.ActualRemain.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.75F, System.Drawing.FontStyle.Bold);
            this.ActualRemain.AppearanceCell.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.ActualRemain.AppearanceCell.Options.UseFont = true;
            this.ActualRemain.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F, System.Drawing.FontStyle.Bold);
            this.ActualRemain.AppearanceHeader.Options.UseFont = true;
            this.ActualRemain.Caption = "Διαθέσιμο υπόλοιπο ΑΧ";
            this.ActualRemain.FieldName = "ActualRemain";
            this.ActualRemain.Name = "ActualRemain";
            this.ActualRemain.OptionsColumn.AllowEdit = false;
            this.ActualRemain.OptionsColumn.AllowFocus = false;
            this.ActualRemain.OptionsColumn.ReadOnly = true;
            this.ActualRemain.Visible = true;
            this.ActualRemain.VisibleIndex = 9;
            this.ActualRemain.Width = 101;
            // 
            // InitQty
            // 
            this.InitQty.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.InitQty.AppearanceCell.Options.UseFont = true;
            this.InitQty.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.InitQty.AppearanceHeader.Options.UseFont = true;
            this.InitQty.Caption = "Αρχική ζήτηση";
            this.InitQty.FieldName = "InitQty";
            this.InitQty.Name = "InitQty";
            this.InitQty.Visible = true;
            this.InitQty.VisibleIndex = 10;
            this.InitQty.Width = 90;
            // 
            // OrderQty
            // 
            this.OrderQty.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.OrderQty.AppearanceCell.Options.UseFont = true;
            this.OrderQty.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.OrderQty.AppearanceHeader.Options.UseFont = true;
            this.OrderQty.Caption = "Ποσότητα Παραγγελίας";
            this.OrderQty.FieldName = "OrderQty";
            this.OrderQty.Name = "OrderQty";
            this.OrderQty.Visible = true;
            this.OrderQty.VisibleIndex = 11;
            this.OrderQty.Width = 94;
            // 
            // pnlAvailability
            // 
            this.pnlAvailability.Controls.Add(this.gridAvailability);
            this.pnlAvailability.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlAvailability.Location = new System.Drawing.Point(0, 504);
            this.pnlAvailability.Name = "pnlAvailability";
            this.pnlAvailability.Size = new System.Drawing.Size(1264, 373);
            this.pnlAvailability.TabIndex = 2;
            // 
            // gridAvailability
            // 
            this.gridAvailability.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridAvailability.Location = new System.Drawing.Point(2, 2);
            this.gridAvailability.LookAndFeel.SkinName = "Office 2019 Colorful";
            this.gridAvailability.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
            this.gridAvailability.MainView = this.gridView2;
            this.gridAvailability.Name = "gridAvailability";
            this.gridAvailability.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gridAvailability.Size = new System.Drawing.Size(1260, 369);
            this.gridAvailability.TabIndex = 1;
            this.gridAvailability.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.gridView2.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11});
            this.gridView2.GridControl = this.gridAvailability;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.gridView2.OptionsCustomization.AllowColumnMoving = false;
            this.gridView2.OptionsCustomization.AllowFilter = false;
            this.gridView2.OptionsCustomization.AllowGroup = false;
            this.gridView2.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView2.OptionsCustomization.AllowSort = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gridView2_RowStyle);
            this.gridView2.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView2_ShowingEditor);
            this.gridView2.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView2_CellValueChanged);
            this.gridView2.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView2_CellValueChanging);
            this.gridView2.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView2_ValidatingEditor);
            this.gridView2.InvalidValueException += new DevExpress.XtraEditors.Controls.InvalidValueExceptionEventHandler(this.gridView2_InvalidValueException);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.gridColumn1.AppearanceCell.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.Caption = "Κατάστημα - Αποθήκη";
            this.gridColumn1.FieldName = "WHDescription";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.gridColumn2.AppearanceCell.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.Caption = "Συνολικό Υπόλοιπο";
            this.gridColumn2.FieldName = "TotalRemain";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.gridColumn3.AppearanceCell.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.Caption = "Υπόλοιπο χωρίς min stock";
            this.gridColumn3.FieldName = "NoMinStockRemain";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.gridColumn4.AppearanceCell.Options.UseFont = true;
            this.gridColumn4.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.gridColumn4.AppearanceHeader.Options.UseFont = true;
            this.gridColumn4.Caption = "Αναμενόμενα";
            this.gridColumn4.FieldName = "ExpectedQty";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn5
            // 
            this.gridColumn5.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.gridColumn5.AppearanceCell.Options.UseFont = true;
            this.gridColumn5.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.gridColumn5.AppearanceHeader.Options.UseFont = true;
            this.gridColumn5.Caption = "Δεσμευμένα";
            this.gridColumn5.FieldName = "ReservedQty";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceCell.Options.UseFont = true;
            this.gridColumn6.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F, System.Drawing.FontStyle.Bold);
            this.gridColumn6.AppearanceHeader.Options.UseFont = true;
            this.gridColumn6.Caption = "Διαθέσιμο υπόλοιπο";
            this.gridColumn6.FieldName = "ActualRemain";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.gridColumn7.AppearanceCell.Options.UseFont = true;
            this.gridColumn7.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.gridColumn7.AppearanceHeader.Options.UseFont = true;
            this.gridColumn7.Caption = "Ποσότητα παραγγελίας";
            this.gridColumn7.FieldName = "OrderQty";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OrderQty", "Σύνολο={0:0.##}")});
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 7;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Κωδικός Α.Χ.";
            this.gridColumn8.FieldName = "WHCode";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Id Είδους";
            this.gridColumn9.FieldName = "ProductId";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.gridColumn10.AppearanceCell.Options.UseFont = true;
            this.gridColumn10.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.gridColumn10.AppearanceHeader.Options.UseFont = true;
            this.gridColumn10.Caption = "Πρόταση";
            this.gridColumn10.FieldName = "PropQty";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 6;
            // 
            // gridColumn11
            // 
            this.gridColumn11.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.gridColumn11.AppearanceCell.Options.UseFont = true;
            this.gridColumn11.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.gridColumn11.AppearanceHeader.Options.UseFont = true;
            this.gridColumn11.Caption = "Απευθείας";
            this.gridColumn11.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn11.FieldName = "Direct";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 8;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueGrayed = 0;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            this.repositoryItemCheckEdit1.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.repositoryItemCheckEdit1_EditValueChanging);
            // 
            // virtualServerModeSource
            // 
            this.virtualServerModeSource.ConfigurationChanged += new System.EventHandler<DevExpress.Data.VirtualServerModeRowsEventArgs>(this.virtualServerModeSource_ConfigurationChanged);
            this.virtualServerModeSource.MoreRows += new System.EventHandler<DevExpress.Data.VirtualServerModeRowsEventArgs>(this.virtualServerModeSource_MoreRows);
            this.virtualServerModeSource.AcquireInnerList += new System.EventHandler<DevExpress.Data.VirtualServerModeAcquireInnerListEventArgs>(this.virtualServerModeSource_AcquireInnerList);
            // 
            // splashScreenManager1
            // 
            this.splashScreenManager1.ClosingDelay = 500;
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.EnableBonusSkins = true;
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Office 2019 Colorful";
            // 
            // CounterFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 921);
            this.Controls.Add(this.pnlInfo);
            this.Controls.Add(this.pnlAvailability);
            this.Controls.Add(this.pnlProgress);
            this.Controls.Add(this.pnlFilters);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.Name = "CounterFrm";
            this.Text = "Αναζήτηση είδους";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.pnlFilters)).EndInit();
            this.pnlFilters.ResumeLayout(false);
            this.pnlFilters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSearch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlProgress)).EndInit();
            this.pnlProgress.ResumeLayout(false);
            this.pnlProgress.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndoQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCusQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInfo)).EndInit();
            this.pnlInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlAvailability)).EndInit();
            this.pnlAvailability.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridAvailability)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.virtualServerModeSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnlFilters;
        private DevExpress.XtraEditors.LabelControl lblSearch;
        public DevExpress.XtraEditors.TextEdit txtSearch;
        private DevExpress.XtraEditors.PanelControl pnlInfo;
        private DevExpress.XtraEditors.PanelControl pnlProgress;
        private DevExpress.XtraEditors.PanelControl pnlAvailability;
        public DevExpress.XtraGrid.GridControl gridInfo;
        private DevExpress.XtraGrid.Columns.GridColumn Id;
        private DevExpress.XtraGrid.Columns.GridColumn Code;
        private DevExpress.XtraGrid.Columns.GridColumn Description;
        private DevExpress.XtraGrid.Columns.GridColumn Price;
        private DevExpress.XtraGrid.Columns.GridColumn CustomerPrice;
        private DevExpress.XtraGrid.Columns.GridColumn WHRemain;
        private DevExpress.XtraGrid.Columns.GridColumn TotalRemain;
        private DevExpress.XtraGrid.Columns.GridColumn WHExpected;
        private DevExpress.XtraGrid.Columns.GridColumn WHReserved;
        private DevExpress.XtraGrid.Columns.GridColumn Expected;
        private DevExpress.XtraGrid.Columns.GridColumn Reserved;
        private DevExpress.XtraGrid.Columns.GridColumn OrderQty;
        public DevExpress.XtraGrid.GridControl gridAvailability;
        public DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.Data.VirtualServerModeSource virtualServerModeSource;
        public DevExpress.XtraEditors.LabelControl lblFooter;
        public DevExpress.XtraEditors.SimpleButton btnSearch;
        public DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn InitQty;
        private DevExpress.XtraEditors.LabelControl lblEndoQty;
        private DevExpress.XtraEditors.LabelControl lblCusQty;
        public DevExpress.XtraEditors.TextEdit txtCusQty;
        public DevExpress.XtraEditors.TextEdit txtEndoQty;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraGrid.Columns.GridColumn PriceR;
        public DevExpress.XtraEditors.SimpleButton btnCancel;
        public DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraGrid.Columns.GridColumn ActualRemain;
        public DevExpress.XtraEditors.SimpleButton btnExtendedSearch;
        private DevExpress.XtraGrid.Columns.GridColumn ABCRanking;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
    }
}