﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softone;
using Skama.Models;
using Skama.Enums;
using Newtonsoft.Json;
using System.Dynamic;

namespace Skama
{
    public class S1Tools
    {
        private XSupport _xSupport;

        public S1Tools(XSupport xSupport)
        {
            _xSupport = xSupport;
        }

        internal void HideWarningsFromS1Module(XModule XModule)
        {
            object otherModule = _xSupport.GetStockObj("ModuleIntf", true);
            object[] myArray1;
            myArray1 = new object[3];
            myArray1[0] = XModule.Handle;
            myArray1[1] = "WARNINGS";
            myArray1[2] = "OFF";
            _xSupport.CallPublished(otherModule, "SetParamValue", myArray1);
        }

        internal Tuple<int, int> GetSeriesAndBranchByWhouse(int whouse, int fprms)
        {
            var seriesTbl = _xSupport.GetMemoryTable("SERIES");
            var index = seriesTbl.Find("SOSOURCE;FPRMS;WHOUSE", 1351, fprms, whouse);
            if (index == -1)
            {
                return new Tuple<int, int>(index, index);
            }
            var series = seriesTbl.GetAsInteger(index, "SERIES");
            var branch = seriesTbl.GetAsInteger(index, "BRANCH");
            return new Tuple<int, int>(series, branch);
        }

        internal int GetIdFromMemoryTable(string tableName, string keys, string returnValue, params object[] values)
        {
            var seriesTbl = _xSupport.GetMemoryTable(tableName);
            var index = seriesTbl.Find(keys, values);
            if (index == -1)
            {
                return index;
            }
            var id = seriesTbl.GetAsInteger(index, returnValue);
            return id;
        }

        internal object GetFieldFromMemoryTable(string tableName, string keys, string returnValue, FieldType tp, params object[] values)
        {
            var tbl = _xSupport.GetMemoryTable(tableName);
            var index = tbl.Find(keys, values);
            if (index == -1)
            {
                return null;
            }
            switch (tp)
            {
                case FieldType.String:
                    return tbl.GetAsString(index, returnValue);
                case FieldType.Int:
                    return tbl.GetAsInteger(index, returnValue);
                case FieldType.Float:
                    return tbl.GetAsFloat(index, returnValue);
                case FieldType.Datetime:
                    return tbl.GetAsDateTime(index, returnValue);
                default:
                    return null;
            }
        }

        internal async Task<SetDataResponse> PostDocByWebMethodAsync(string objectName, string formName, object data, dynamic paramss)
        {
            return await Task.Run(() =>
              {
                  return PostDocByWebMethod( objectName, formName, data, paramss);
              });
        }

        internal SetDataResponse PostDocByWebMethod(string objectName, string formName, object data, dynamic paramss)
        {
            try
            {
                paramss.WARNINGS = "OFF";
                paramss.NOMESSAGES = 1;

                var req = new SetDataRequest
                {
                    Object = objectName,
                    ObjectParams = paramss,
                    Form = formName,
                    Data = data
                };

                var requestStr = JsonConvert.SerializeObject(req);
                var resultStr = _xSupport.CallWebService(requestStr, out string contentType);
                var res = JsonConvert.DeserializeObject<SetDataResponse>(resultStr);
                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
