﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.LookAndFeel;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using Microsoft.VisualBasic;
using Skama.Models;
using Softone;
using TecOrder;
using TecOrder.Dtos;

namespace Skama
{
    public partial class CounterFrm : Form
    {
        private XSupport _xSupport;
        private XModule _xModule;
        private Helper _helper;
        public List<ItemSearch> items { get; set; } = new List<ItemSearch>();
        public List<ItemSearch> cachedList { get; set; } = new List<ItemSearch>();
        public List<ItemInfo> availabilityList { get; set; } = new List<ItemInfo>();
        public List<ItemInfo> cachedAvailabilityList { get; set; } = new List<ItemInfo>();
        private int _batchCount = 50;
        public List<string> prevSearchValues = new List<string>();
        DXPopupMenu rightClickMenu = new DXPopupMenu();
        private bool _isRelFrm = false;

        public CounterFrm()
        {
            InitializeComponent();
        }

        public CounterFrm(XSupport xSupport, XModule xModule, Helper helper, bool isRelForm)
        {
            _xSupport = xSupport;
            _xModule = xModule;
            _helper = helper;
            _isRelFrm = isRelForm;
            InitializeComponent();
            DialogResult = DialogResult.Cancel;
            virtualServerModeSource.RowType = typeof(ItemSearch);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            var button = sender as SimpleButton;
            var cmd = button.Handle.ToInt32();
            SearchData(cmd);

        }

        private void SearchData(int cmd)
        {
            var txtInput = txtSearch.Text;
            if (string.IsNullOrEmpty(txtInput) || txtInput.Length < 3)
            {
                MessageBox.Show("Παρακαλώ πληκτρολογήστε τουλάχιστον 3 χαρακτήρες.", "Προσοχή!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (!splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.ShowWaitForm();
            }
            RefreshGridData(txtInput, "", cmd);
            gridView1.BestFitColumns();
            splashScreenManager1.CloseWaitForm();
        }

        private void virtualServerModeSource_ConfigurationChanged(object sender, DevExpress.Data.VirtualServerModeRowsEventArgs e)
        {
            var currSearchValue = txtSearch.Text;

            if (string.IsNullOrWhiteSpace(currSearchValue) || currSearchValue.Length < 3)
            {
                return;
            }
        }

        private List<ItemInfo> GetOrderQtyFromDatabase(List<ItemInfo> items)
        {
            foreach (var itm in items)
            {
                var whouseRemain = double.Parse(_xModule.EvalFormula($"FRemQty1PerWHouse({itm.ProductId}, {int.Parse(itm.WHCode)}, SALDOC.TRNDATE)").ToString());
                var whouseReserved = double.Parse(_xModule.EvalFormula($"FItemReserved({itm.ProductId}, '', '{itm.WHCode}')").ToString());
                itm.TotalRemain = whouseRemain;
                itm.ReservedQty = whouseReserved;
                itm.NoMinStockRemain = itm.TotalRemain - 1; //to read it from item
                itm.ActualRemain = itm.TotalRemain - itm.ReservedQty;
            }
            var itemInfoTbl = _xModule.GetTable("CCCITEMINFO");
            var dbItemInfo = itemInfoTbl.CreateDataTable(true).AsEnumerable().ToList();
            if (dbItemInfo.Any())
            {
                dbItemInfo = dbItemInfo.Where(x => Convert.ToInt32(x["MTRL"]) == items.First().ProductId).ToList();
                foreach (var row in dbItemInfo)
                {
                    var orderQty = Convert.ToDouble(row["ORDERQTY"]);
                    var whouse = Convert.ToInt32(row["WHOUSE"]);
                    var direct = row["DIRECT"] != DBNull.Value ? Convert.ToInt32(row["DIRECT"]) : 0;
                    var itemRow = items.Where(x => x.WhouseId == whouse).FirstOrDefault();
                    if (itemRow != null)
                    {
                        itemRow.OrderQty = orderQty;
                        itemRow.Direct = direct;
                        //if (itemRow.ActualRemain <= 0)
                        //{
                        //    itemRow.OrderQty = 0.0;
                        //    var recNo = itemInfoTbl.Find("WHOUSE", whouse);
                        //    itemInfoTbl.Current.Edit(recNo);
                        //    itemInfoTbl.Current["ORDERQTY"] = 0.0;
                        //    itemInfoTbl.Current.Post();
                        //}
                    }
                }
            }
            return items;
        }

        private void virtualServerModeSource_MoreRows(object sender, VirtualServerModeRowsEventArgs e)
        {
            var searchValue = txtSearch.Text;
            if (string.IsNullOrWhiteSpace(searchValue) || searchValue.Length < 3)
            {
                return;
            }

            if (e.CurrentRowCount < _batchCount && e.CurrentRowCount > 0)
            {
                return;
            }

            e.RowsTask = Task.Factory.StartNew(() =>
            {
                var nextBatch = new List<ItemSearch>();
                bool moreRows = true;
                var userWhouse = Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["WHOUSE"]);
                var docDate = Convert.ToDateTime(_xModule.GetTable("SALDOC").Current["TRNDATE"]);
                var skip = e.CurrentRowCount;
                var take = _batchCount;
                items = _helper.GetItems(searchValue, "", userWhouse, docDate, skip, take);
                var cachedIds = cachedList.Select(x => x.Id).ToList();
                foreach (var item in items)
                {
                    if (!cachedIds.Contains(item.Id))
                    {
                        cachedList.Add(item);
                        nextBatch.Add(item);
                    }
                    else
                    {
                        var cachedItem = cachedList.FirstOrDefault(x => x.Id == item.Id);
                        nextBatch.Add(cachedItem);
                    }
                }
                lock (_helper)
                {
                    if (nextBatch.Count > 0)
                    {
                        nextBatch = _helper.GetCustomerPrices(nextBatch);
                    }
                }
                return new VirtualServerModeRowsTaskResult(nextBatch, moreRows);

            }, e.CancellationToken);
        }

        private void virtualServerModeSource_AcquireInnerList(object sender, VirtualServerModeAcquireInnerListEventArgs e)
        {
            e.InnerList = items;
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {
                if (!(sender is GridView view))
                {
                    return;
                }

                var item = (ItemSearch)view.GetRow(e.FocusedRowHandle);
                if (item != null)
                {
                    ChangeDetailInfo(item);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Σφάλμα!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }

        private void ChangeDetailInfo(ItemSearch item)
        {
            var finalPrice = _helper.GetItemFinalPrice(item);
            if (finalPrice == 0)
            {
                finalPrice = item.Price;
            }
            var txt = $"Είδος : {item.Code} | Τιμή πελάτη : {finalPrice}";
            //var txt = $"{item.Code} | {item.Description}";
            lblFooter.Text = txt;
            txtCusQty.Text = item.OrderQty.ToString();
            var docWhouse = Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["WHOUSE"]);
            var sumEndoQty = availabilityList
                            .Where(x => x.ProductId == item.Id)
                            .Where(x => x.WhouseId != docWhouse)
                            .Sum(x => x.OrderQty);

            if (item.OrderQty + sumEndoQty >= item.InitQty)
            {
                txtCusQty.BackColor = Color.LightGreen;
            }
            else
            {
                txtCusQty.BackColor = Color.Salmon;
            }
            txtEndoQty.Text = sumEndoQty.ToString();
            var itemId = item.Id;
            GetAvailabilityPerBranch(itemId);
        }

        public void RefreshGridData(string searchValue, string inClause, int cmd)
        {
            var userWhouse = Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["WHOUSE"]);
            var docDate = Convert.ToDateTime(_xModule.GetTable("SALDOC").Current["TRNDATE"]);

            try
            {
                if (cmd == 999002)
                {
                    var mtrl = Convert.ToInt32(_xModule.GetTable("ITELINES").Current["MTRL"]);
                    inClause = mtrl.ToString();
                }
                items = _helper.GetItems(searchValue, inClause, userWhouse, docDate);
                if (items.Any())
                {
                    var cachedIds = cachedList.Select(x => x.Id).ToList();
                    foreach (var item in items)
                    {
                        if (!cachedIds.Contains(item.Id))
                        {
                            cachedList.Add(item);
                        }
                        else
                        {
                            var cachedItem = cachedList.FirstOrDefault(x => x.Id == item.Id);
                            if (cachedItem != null)
                            {
                                item.OrderQty = cachedItem.OrderQty;
                                item.InitQty = cachedItem.InitQty;
                            }
                        }
                    }

                    if (cmd == 999002)
                    {
                        var item = items.First();
                        var qty1 = 0.0;
                        var initQty = 0.0;
                        if (_xModule.GetTable("ITELINES").Current["QTY1"] != DBNull.Value)
                        {
                            qty1 = Convert.ToDouble(_xModule.GetTable("ITELINES").Current["QTY1"]);
                            if (qty1 <= item.TotalRemain)
                            {
                                item.OrderQty = qty1;
                            }
                            else
                            {
                                item.OrderQty = 0;
                            }
                        }
                        if (_xModule.GetTable("ITELINES").Current["CCCINITQTY"] != DBNull.Value)
                        {
                            initQty = Convert.ToDouble(_xModule.GetTable("ITELINES").Current["CCCINITQTY"]);
                            item.InitQty = initQty;
                        }
                        ChangeDetailInfo(item);
                    }

                    gridInfo.DataSource = items;
                }
                else
                {
                    MessageBox.Show("Δε βρέθηκαν εγγραφές!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //if (InvokeRequired)
            //{
            //    Invoke(new Action(() =>
            //    {
            //        virtualServerModeSource.Refresh();
            //        gridInfo.DataSource = virtualServerModeSource;
            //        gridView1.BestFitColumns();
            //    }));
            //}
            //else
            //{
            //    virtualServerModeSource.Refresh();
            //    gridInfo.DataSource = virtualServerModeSource;
            //    gridView1.BestFitColumns();
            //}
        }

        private void GetAvailabilityPerBranch(int itemId)
        {
            try
            {
                var docWhouse = Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["WHOUSE"]);
                var docDate = Convert.ToDateTime(_xModule.GetTable("SALDOC").Current["TRNDATE"]);

                var focusedItemList = cachedAvailabilityList.Where(x => x.ProductId == itemId).ToList(); 

                if (focusedItemList.Any())
                {
                    SetDatasource2AvailabilityGrid(focusedItemList, docDate, docWhouse);
                }
                else
                {
                    var itemInfoList = _helper.GetItemInfo(itemId, docDate);
                    availabilityList.AddRange(itemInfoList);
                    foreach (var row in itemInfoList)
                    {
                        var cachedAvailabilityItem = cachedAvailabilityList.Where(x => x.ProductId == row.ProductId && x.WhouseId == row.WhouseId).FirstOrDefault();
                        if (cachedAvailabilityItem == null)
                        {
                            cachedAvailabilityList.Add(row);
                        }
                    }
                    SetDatasource2AvailabilityGrid(itemInfoList, docDate, docWhouse);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SetDatasource2AvailabilityGrid(List<ItemInfo> availabilityList, DateTime docDate, int docWhouse)
        {
            var whouseId = Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["WHOUSE"]);
            var findoc = Convert.ToInt32(_xModule.GetTable("SALDOC").Current["FINDOC"]);
            var sortedList = _helper.SortItems(availabilityList, docWhouse);
            var docQuantity = 0.0;
            var focusedItem = gridView1.GetFocusedRow();
            if (focusedItem == null)
            {
                return;
            }
            var item = (ItemSearch)focusedItem;
            if (!btnSearch.Enabled && !_isRelFrm && item.OrderQty == 0)
            {
                var qty1 = Convert.ToDouble(_xModule.GetTable("ITELINES").Current["QTY1"]);
                if (qty1 <= item.TotalRemain)
                {
                    item.OrderQty = qty1;
                }
            }
            var orderQty = item.OrderQty;
            docQuantity = orderQty;
            //var currWhouseItem = sortedList?.Where(x => x.WhouseId == whouseId).FirstOrDefault();
            //if (currWhouseItem != null)
            //{
            //    //currWhouseItem.OrderQty = docQuantity;
            //}

            _helper.GenerateProposedQuantities(sortedList, docQuantity);

            if (!btnSearch.Enabled && !_isRelFrm)
            {
                var qty1 = Convert.ToDouble(_xModule.GetTable("ITELINES").Current["QTY1"]);
                var initQty1 = 0.0;
                if (_xModule.GetTable("ITELINES").Current["CCCINITQTY"] != DBNull.Value)
                {
                    initQty1 = Convert.ToDouble(_xModule.GetTable("ITELINES").Current["CCCINITQTY"]);
                }
                if (findoc > 0)
                {
                    item.OrderQty = qty1;
                    item.InitQty = initQty1;
                }
                sortedList = GetOrderQtyFromDatabase(sortedList);
                //currWhouseItem.OrderQty = qty1;
            }

            var endoQty = sortedList
                        .Where(x => x.WhouseId != whouseId)
                            .Where(x => x.ProductId == item.Id)
                            .Where(x => x.OrderQty > 0).Sum(y => y.OrderQty);
            txtEndoQty.Text = endoQty.ToString();

            gridAvailability.DataSource = sortedList;
            gridView2.BestFitColumns();
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                GridHitInfo hi = gridView1.CalcHitInfo(e.Location);
                if (hi.InRow)
                {
                    AddItemsToMenu(rightClickMenu);
                    UserLookAndFeel lf = UserLookAndFeel.Default;
                    ((IDXDropDownControl)rightClickMenu).Show(new SkinMenuManager(lf), gridInfo, hi.HitPoint);
                }
            }
        }

        void AddItemsToMenu(DXPopupMenu menu)
        {
            menu.Items.Clear();
            var WHExpResAnalysis = new DXMenuItem("Αναμενόμενα / Δεσμευμένα Α.Χ.", WHExpResAnalysis_Click);
            var ExpResAnalysis = new DXMenuItem("Αναμενόμενα / Δεσμευμένα Εταιρίας", ExpResAnalysis_Click);
            var alternativeItems = new DXMenuItem("Εναλλακτικά είδη", alternativeItems_Click);
            var inquiryRequest = new DXMenuItem("Inquiry", inquiryRequest_Click);
            var setOrderFromExpected = new DXMenuItem("Παραγγελία από αναμενόμενα προμηθευτή", setOrder_Click);
            menu.Items.Add(WHExpResAnalysis);
            menu.Items.Add(ExpResAnalysis);
            menu.Items.Add(alternativeItems);
            menu.Items.Add(inquiryRequest);
            //menu.Items.Add(setOrderFromExpected);
        }

        private void gridView1_ShownEditor(object sender, EventArgs e)
        {
            TextEdit edit = gridView1.ActiveEditor as TextEdit;
            edit.Properties.BeforeShowMenu += new DevExpress.XtraEditors.Controls.BeforeShowMenuEventHandler(Properties_BeforeShowMenu);
        }

        private void Properties_BeforeShowMenu(object sender, DevExpress.XtraEditors.Controls.BeforeShowMenuEventArgs e)
        {
            AddItemsToMenu(e.Menu);
        }

        private void setOrder_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(sender is DXMenuItem menuItem))
                {
                    return;
                }

                var userBranch = _xModule.GetTable("SALDOC").Current["BRANCH"] != DBNull.Value 
                    ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["BRANCH"]) : 0;
                if (userBranch == 0)
                {
                    throw new Exception("Για να μπορέσετε να καταχωρήσετε παραγγελία από τα αναμενόμενα, πρέπει πρώτα να καταχωρήσετε σειρά στο παραστατικό!");
                }
                var centralWhForCurrentBranch = GetCentralWhByBranch(userBranch);
                if (centralWhForCurrentBranch == 0)
                {
                    throw new Exception("Δεν βρέθηκε κεντρική αποθήκη για το υποκατάστημα σας!");
                }
                var otherCentralWhouse = centralWhForCurrentBranch == 1002 ? 1001 : 1002;

                var row = (ItemSearch)gridView1.GetFocusedRow();
                var itemId = row.Id;
                var orderQty = row.OrderQty;
                //if (orderQty == 0)
                //{
                //    throw new Exception("Δεν έχετε συμπληρώσει ποσότητα παραγγελίας!");
                //}
                int xPosition = this.Left + (this.Width / 2) - 200;
                int yPosition = this.Top + (this.Height / 2) - 100;
                string input = Interaction.InputBox("Παρακαλώ εισάγετε ποσότητα παραγγελίας", "Εισαγωγή", "", xPosition, yPosition);
                if (string.IsNullOrWhiteSpace(input))
                {
                    throw new Exception("Δεν έχετε συμπληρώσει ποσότητα παραγγελίας!");
                }
                if (System.Text.RegularExpressions.Regex.IsMatch(input, "[^0-9]"))
                {
                    throw new Exception("Θα πρέπει να πληκτρολογήσετε μόνο αριθμούς!");
                }

                var endoRows = (List<ItemInfo>)gridView2.DataSource;
                var qtyFromCentralWhouse = endoRows?.Where(x => x.WhouseId == centralWhForCurrentBranch).FirstOrDefault()?.OrderQty;
                if (qtyFromCentralWhouse == null || qtyFromCentralWhouse == 0)
                {
                   //throw new Exception("Δεν έχετε συμπληρώσει ποσότητα διακίνησης από τον ΑΧ!");
                }

                var actualRemainFromCentralWhouse = endoRows.Where(x => x.WhouseId == centralWhForCurrentBranch).FirstOrDefault()?.ActualRemain;

                if (actualRemainFromCentralWhouse != null && qtyFromCentralWhouse <= actualRemainFromCentralWhouse && row.InitQty == row.OrderQty)
                {
                    //throw new Exception("Η ζήτηση του πελάτη καλύπτεται από το διαθέσιμο υπόλοιπο της αποθήκης!");
                }

                var orderedPerWhouse = GetOrderedQtyPerWh(itemId);
                var totalOrdered = orderedPerWhouse.Where(x => x.RestCategory == 1).ToList();
                if (!totalOrdered.Any())
                {
                    throw new Exception("Δεν υπάρχουν αναμενόμενες παραλαβές από προμηθευτή για το συγκεκριμένο είδος!");
                }

                var orderedFromCentralObj = orderedPerWhouse.FirstOrDefault(x => x.Warehouse == centralWhForCurrentBranch && x.RestCategory == 1)?.Ordered;
                var reserverdFromCentralObj = orderedPerWhouse.FirstOrDefault(x => x.Warehouse == centralWhForCurrentBranch && x.RestCategory == 2)?.Ordered;
                double orderedFromCentral = 0;
                if (orderedFromCentralObj != null)
                {
                    orderedFromCentral = Convert.ToDouble(orderedFromCentralObj);
                }
                double reserverdFromCentral = 0;
                if (reserverdFromCentralObj != null)
                {
                    reserverdFromCentral = Convert.ToDouble(reserverdFromCentralObj);
                }

                var orderedFromOtherObj = orderedPerWhouse.FirstOrDefault(x => x.Warehouse == otherCentralWhouse && x.RestCategory == 1)?.Ordered;
                var reserverdFromOtherObj = orderedPerWhouse.FirstOrDefault(x => x.Warehouse == otherCentralWhouse && x.RestCategory == 2)?.Ordered;
                double orderedFromOther = 0;
                if (orderedFromOtherObj != null)
                {
                    orderedFromOther = Convert.ToDouble(orderedFromOtherObj);
                }
                double reserverdFromOther = 0;
                if (reserverdFromOtherObj != null)
                {
                    reserverdFromOther = Convert.ToDouble(reserverdFromOtherObj);
                }


                var remainQty = double.Parse(input);
                //var remainQty = row.InitQty - row.OrderQty;
                //if (row.InitQty == row.OrderQty)
                //{
                //    remainQty = row.OrderQty;
                //}
                if (remainQty <= orderedFromCentral - reserverdFromCentral)
                {
                    var series = centralWhForCurrentBranch == 1001 ? 10149 : 10249;
                    var startBranch = centralWhForCurrentBranch;
                    var lastBranch = userBranch;
                    var nextBranch = _helper.GetNextStationByStockFlow(startBranch, lastBranch);

                    _helper.PostOrdersFromOrdered(series, startBranch, nextBranch, itemId, remainQty);
                    MessageBox.Show("Επιτυχής καταχώριση της παραγγελίας από τα αναμενόμενα προμηθευτή.", "Προσοχή!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                if (remainQty <= orderedFromOther - reserverdFromOther)
                {
                    var series = otherCentralWhouse == 1001 ? 10149 : 10249;
                    var startBranch = otherCentralWhouse;
                    var lastBranch = userBranch;
                    var nextBranch = _helper.GetNextStationByStockFlow(startBranch, lastBranch);

                    _helper.PostOrdersFromOrdered(series, startBranch, nextBranch, itemId, remainQty);
                    MessageBox.Show("Επιτυχής καταχώριση της παραγγελίας από τα αναμενόμενα προμηθευτή.", "Προσοχή!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Προσοχή!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private List<OrderedPerWhouse> GetOrderedQtyPerWh(int itemId)
        {
            var res = new List<OrderedPerWhouse>();
            var query = $"DECLARE @mtrl int ; EXECUTE [dbo].[GT_OrderedAndReservedByWh] @mtrl = {itemId}";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    for (int i = 0; i < ds.Count; i++)
                    {
                        res.Add(new OrderedPerWhouse
                        {
                            RestCategory = ds.GetAsInteger(i, "RESTCATEG"),
                            Warehouse = ds.GetAsInteger(i, "BRANCH"),
                            Ordered = ds.GetAsFloat(i, "ORDERED")
                        });
                    }
                }
            }
            return res;
        }

        private int GetCentralWhByBranch(int userBranch)
        {
            var query = $"SELECT ISNULL(CCCBRANCHZONE,0) AS ZONE FROM BRANCH WHERE COMPANY = {_xSupport.ConnectionInfo.CompanyId} AND BRANCH = {userBranch}";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    var zone = ds.GetAsInteger(0, "ZONE");
                    switch (zone)
                    {
                        case 1:
                            return 1001;
                        case 2:
                            return 1002;
                        default:
                            return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
        }

        private void WHExpResAnalysis_Click(object sender, EventArgs e)
        {
            if (!(sender is DXMenuItem menuItem))
            {
                return;
            }

            var row = (ItemSearch)gridView1.GetFocusedRow();
            if (row != null)
            {
                var restFrm = new RestAnalysisFrm(_xSupport);
                var itemId = row.Id;
                var whouseId = Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["WHOUSE"]);
                var restList = _helper.GetRestAnalysis(itemId);
                if (restList.Any())
                {
                    restFrm.RefreshDataGrids(restList, whouseId);
                }

                restFrm.ShowDialog();
            }
        }

        private void ExpResAnalysis_Click(object sender, EventArgs e)
        {
            if (!(sender is DXMenuItem menuItem))
            {
                return;
            }

            var row = (ItemSearch)gridView1.GetFocusedRow();
            if (row != null)
            {
                var restFrm = new RestAnalysisFrm(_xSupport);
                var itemId = row.Id;
                var restList = _helper.GetRestAnalysis(itemId);
                if (restList.Any())
                {
                    restFrm.RefreshDataGrids(restList, 0);
                }

                restFrm.ShowDialog();
            }
        }

        private void alternativeItems_Click(object sender, EventArgs e)
        {
            if (!(sender is DXMenuItem menuItem))
            {
                return;
            }
            try
            {
                var row = gridView1.GetFocusedRow();
                var item = (ItemSearch)row;
                if (item != null)
                {
                    var itemCode = item.Code;
                    var b2bClient = new B2BClient();
                    var relProducts = b2bClient.GetRelatives(itemCode);
                    if (!relProducts.Any())
                    {
                        throw new Exception("Δε βρέθηκαν εναλλακτικά είδη!");
                    }
                    var relCodes = relProducts.Select(x => $"'{x.Code}'").ToList();
                    var inClause = string.Join(",", relCodes);
                    var ids = new List<int>();
                    var query = $"SELECT MTRL, CODE FROM MTRL WHERE COMPANY = {_xSupport.ConnectionInfo.CompanyId} AND SODTYPE = 51 AND CODE IN ({inClause})";
                    using (var ds = _xSupport.GetSQLDataSet(query, null))
                    {
                        if (ds.Count > 0)
                        {
                            for (int i = 0; i < ds.Count; i++)
                            {
                                var mtrl = ds.GetAsInteger(i, "MTRL");
                                if (mtrl > 0)
                                {
                                    ids.Add(mtrl);
                                }
                            }
                        }
                    }
                    if (!ids.Any())
                    {
                        throw new Exception("Δε βρέθηκαν εναλλακτικά είδη!");                       
                    }
                    var idsClause = string.Join(",", ids);
                    var isRelFrm = true;
                    var _relFrm = new CounterFrm(_xSupport, _xModule, _helper, isRelFrm);
                    _relFrm.Size = new Size(1280, 960);
                    _relFrm.WindowState = FormWindowState.Normal;
                    _relFrm.btnSearch.Enabled = false;
                    _relFrm.btnExtendedSearch.Enabled = false;
                    _relFrm.txtSearch.ReadOnly = true;
                    _relFrm.Text = "Εναλλακτικά είδη";
                    _relFrm.RefreshGridData("", idsClause, 0);
                    _relFrm.Visible = false;
                    var dialogRes = _relFrm.ShowDialog();
                    if (dialogRes == DialogResult.OK)
                    {
                        foreach (var itm in _relFrm.cachedAvailabilityList)
                        {
                            if (!cachedAvailabilityList.Contains(itm))
                            {
                                cachedAvailabilityList.Add(itm);
                            }
                        }

                        var cachedIds = cachedList.Select(x => x.Id).Distinct().ToList();
                        foreach (var tm in _relFrm.cachedList)
                        {
                            if (!cachedIds.Contains(tm.Id))
                            {
                                cachedList.Add(tm);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Προσοχή!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void inquiryRequest_Click(object sender, EventArgs e)
        {
            if (!(sender is DXMenuItem menuItem))
            {
                return;
            }

            var row = (ItemSearch)gridView1.GetFocusedRow();
            if (row == null)
            {
                return;
            }
            var userId = _xSupport.ConnectionInfo.UserId;
            var itemId = row.Id;
            var itemSupplier = GetItemSupplier(itemId);
            if (itemSupplier == null)
            {
                MessageBox.Show($"Δεν έχει δηλωθεί συνήθης προμηθευτής για το είδος.", "Προσοχή!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var ediList = _helper.GetEdiConfiguration(itemSupplier.Id);
            if (!ediList.Any())
            {
                MessageBox.Show($"Δεν υπάρχει EDI παραμετροποίηση για τον προμηθευτή {itemSupplier.Code} - {itemSupplier.Name}.", "Προσοχή!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var currentUserAccess = ediList.Where(x => x.UserId == userId).FirstOrDefault();
            if (currentUserAccess == null)
            {
                MessageBox.Show($"Δεν έχει προστεθεί ο χρήστης σας στην EDI παραμετροποίηση για τον προμηθευτή {itemSupplier.Code} - {itemSupplier.Name}.", "Προσοχή!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (currentUserAccess.InquiryUac == 0)
            {
                MessageBox.Show($"Μη διαθέσιμη εργασία.", "Προσοχή!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (row.OrderQty == 0)
            {
                MessageBox.Show($"Παρακαλώ συμπληρώστε ποσότητα παραγγελίας.", "Προσοχή!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var inquiryRes = SendInquiry(ediList, itemSupplier, row, row.OrderQty);
            if (!string.IsNullOrEmpty(inquiryRes.Message))
            {
                MessageBox.Show(inquiryRes.Message, "Προσοχή!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var productRes = inquiryRes.Products.FirstOrDefault();

            if (productRes != null)
            {
                var inquiryFrm = new InquiryFrm(productRes, currentUserAccess);
                inquiryFrm.Text = $"{itemSupplier.Code} - {itemSupplier.Name}";
                inquiryFrm.ShowDialog();
            }
        }

        private InquiryResp SendInquiry(List<EdiConfiguration> ediList, ItemSupplier itemSupplier, ItemSearch item, double quantity)
        {
            var username = "7182000021683";//"7182000040330";
            var password = "ddkaCkOG"; //"Rc62YxTb";
            var orderClient = new TecClient(username, password);
            var businessRelation = new BusinessRelation();

            businessRelation.BuyerNumber = ediList.FirstOrDefault().CustomerCode;
            businessRelation.SupplierNumber = itemSupplier.Code;
            var products = new List<RequestProduct>();
            var product = GetRequestProduct(item.Id, itemSupplier.Id);
            product.Quantity = quantity;
            products.Add(product);
            var res = orderClient.SubmitInquiry(businessRelation, products);
            return res;
        }


        private RequestProduct GetRequestProduct(int itemId, int supplierId)
        {
            var product = new RequestProduct();
            var query = $"SELECT MTRSUPCODE, NAME, MTRUNIT1 FROM MTRL M LEFT JOIN MTRSUPCODE S ON S.MTRL = M.MTRL AND S.TRDR = {supplierId} WHERE M.MTRL = {itemId}";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                product.ProductNumber = ds.GetAsString(0, "MTRSUPCODE");
                product.Uom = ds.GetAsInteger(0, "MTRUNIT1") == 101 ? "PCE" : "";
            }

            return product;
        }

        private ItemSupplier GetItemSupplier(int itemId)
        {
            var query = $"SELECT M.MTRSUP, T.CODE, T.NAME FROM MTRL M INNER JOIN TRDR T ON T.TRDR = M.MTRSUP WHERE M.MTRL = {itemId}";

            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    var itemSup = new ItemSupplier
                    {
                        Id = ds.GetAsInteger(0, "MTRSUP"),
                        Code = ds.GetAsString(0, "CODE"),
                        Name = ds.GetAsString(0, "NAME")
                    };

                    return itemSup;
                }
                else
                {
                    return null;
                }
            }
        }

        private void gridView2_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            var gridView = sender as GridView;

            if (e.Column.FieldName == "OrderQty")
            {
                var error = false;

                try
                {
                    var docWhouse = Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["WHOUSE"]);
                    var row = (ItemInfo)gridView.GetRow(e.RowHandle);
                    var value = e.Value.ToString() == "" ? "0" : e.Value.ToString();
                    var orderQty = double.Parse(value);
                    var masterGrid = gridView1;
                    var masterRow = masterGrid.GetFocusedRow();

                    var masterData = (ItemSearch)masterRow;
                    var ordQty = masterData.OrderQty;
                    if (ordQty == 0)
                    {
                        row.OrderQty = 0;
                        throw new Exception("Παρακαλώ συμπληρώστε πρώτα ποσότητα παραγγελίας στον πίνακα αναζήτησης.");
                    }

                    if (orderQty <= row.ActualRemain)
                    {
                        row.OrderQty = double.Parse(value);
                        var sumEndoQty = availabilityList
                            .Where(x => x.ProductId == row.ProductId)
                            .Where(x => x.WhouseId != docWhouse)
                            .Sum(x => x.OrderQty);

                        var relQtySum = availabilityList
                            .Where(x => x.ProductId == row.ProductId)
                            .Sum(x => x.OrderQty);

                        if (relQtySum > ordQty)
                        {
                            throw new Exception("Με τη ποσότητα που πληκτρολογήσατε έχετε ξεπεράσει την ποσότητα της παραγγελίας. Η καταχώριση ακυρώνεται.");
                        }

                        txtCusQty.Text = relQtySum.ToString();
                        txtEndoQty.Text = sumEndoQty.ToString();
                    }
                    else
                    {
                        throw new Exception("Η ποσότητα που πληκτρολογήσατε ξεπερνάει το διαθέσιμο υπόλοιπο του καταστήματος. H καταχώριση ακυρώνεται.");
                    }
                }
                catch (Exception ex)
                {
                    error = true;
                    MessageBox.Show(ex.Message, "Προσοχή!", MessageBoxButtons.OK);
                }
                finally
                {
                    if (error)
                    {
                        var colIndex = gridView.Columns.Where(x => x.FieldName == "OrderQty").FirstOrDefault()?.AbsoluteIndex.ToString();
                        if (!string.IsNullOrEmpty(colIndex))
                        {
                            var index = int.Parse(colIndex);
                            gridView.SetRowCellValue(e.RowHandle, gridView.Columns[index], "0");
                        }
                    }
                }
            }
        }

        private void repositoryItemCheckEdit1_EditValueChanging(object sender, ChangingEventArgs e)
        {
            if (!(sender is CheckEdit))
            {
                return;
            }
            var checkCol = sender as CheckEdit;
            var value = (int)e.NewValue;
            var focusedRow = gridView2.GetFocusedRow();
            if (focusedRow == null)
            {
                e.Cancel = true;
                return;
            }
            var data = (ItemInfo)focusedRow;
            var orderQty = data.OrderQty;
            if (orderQty == 0)
            {
                e.Cancel = true;
                return;
            }
            var docWH = Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["WHOUSE"]);
            var lineWH = data.WhouseId;
            var isValid = _helper.CheckIfIsValidDirectShipment(docWH, lineWH);
            if (!isValid)
            {
                MessageBox.Show("Δεν επιτρέπεται η απευθείας αποστολή μεταξύ των καταστημάτων.", "Προσοχή!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }

        private void gridView1_RowStyle(object sender, RowStyleEventArgs e)
        {
            if (!(sender is GridView grid))
            {
                return;
            }

            var datasource = grid.DataSource as VirtualServerModeSource;

            if (datasource != null)
            {
                return;
            }

            if (e.RowHandle < 0)
            {
                return;
            }

            var whQuantity = Convert.ToInt32(grid.GetRowCellValue(e.RowHandle, "WHRemain"));
            var actualWHQuantity = Convert.ToInt32(grid.GetRowCellValue(e.RowHandle, "ActualRemain"));
            var totalQuantity = Convert.ToInt32(grid.GetRowCellValue(e.RowHandle, "TotalRemain"));

            if (actualWHQuantity > 0)
            {
                e.Appearance.BackColor = Color.LightGreen;
                e.Appearance.BackColor2 = Color.LightGreen;
            }
            if (actualWHQuantity == 0 && totalQuantity > 0)
            {
                e.Appearance.BackColor = Color.LightYellow;
                e.Appearance.BackColor2 = Color.LightYellow;
            }
            if (actualWHQuantity <= 0 && totalQuantity > 0)
            {
                e.Appearance.BackColor = Color.LightYellow;
                e.Appearance.BackColor2 = Color.LightYellow;
            }
            if (actualWHQuantity <= 0 && totalQuantity <= 0)
            {
                e.Appearance.BackColor = Color.Salmon;
                e.Appearance.BackColor2 = Color.Salmon;
            }

            e.HighPriority = true;
        }

        private void gridView2_RowStyle(object sender, RowStyleEventArgs e)
        {
            if (!(sender is GridView grid))
            {
                return;
            }

            var datasource = grid.DataSource;

            if (datasource == null)
            {
                return;
            }

            if (e.RowHandle < 0)
            {
                return;
            }

            var whQuantity = Convert.ToInt32(grid.GetRowCellValue(e.RowHandle, "ActualRemain"));

            if (whQuantity > 0)
            {
                e.Appearance.BackColor = Color.LightGreen;
                e.Appearance.BackColor2 = Color.LightGreen;
            }
            else
            {
                e.Appearance.BackColor = Color.DarkGray;
                e.Appearance.BackColor2 = Color.DarkGray;
            }

            e.HighPriority = true;
        }

        private void gridView1_AsyncCompleted(object sender, EventArgs e)
        {
            var grid = sender as GridView;
            grid.BestFitColumns();
            var focusedRow = (ItemSearch)grid.GetFocusedRow();
            if (focusedRow != null)
            {
                ChangeDetailInfo(focusedRow);
            }
        }

        private void spnDemand_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                var spin = sender as SpinEdit;
                var qty = spin.Value;
                var datasource = gridAvailability.DataSource;
                if (datasource != null)
                {
                    var data = (List<ItemInfo>)datasource;
                    if (data.Any() && spin.Value > 0)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridView2_ShowingEditor(object sender, CancelEventArgs e)
        {
            var view = sender as GridView;
            if (view.FocusedColumn.FieldName != "OrderQty")
            {
                return;
            }
            var row = (ItemInfo)view.GetFocusedRow();
            var actualRemain = row.ActualRemain;
            //var docWhouse = Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["WHOUSE"]);
            if (actualRemain <= 0)
            {
                e.Cancel = true;
            }
            //else
            //{
            //    if (view.ActiveEditor != null)
            //    {
            //        view.ActiveEditor.IsModified = true;
            //    }
            //}
        }

        private void gridView2_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            ColumnView view = sender as ColumnView;
            GridColumn column = (e as EditFormValidateEditorEventArgs)?.Column ?? view.FocusedColumn;
            var grid = sender as GridView;
            var row = (ItemInfo)grid.GetFocusedRow();
            if (column.Name == "gridColumn7")
            {
                var actualRemain = row.ActualRemain;
                var totalRemain = row.TotalRemain;
                var docWhouse = Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["WHOUSE"]);
                if (string.IsNullOrEmpty(e.Value.ToString()))
                {
                    e.Value = "0";
                }
                if ((double.Parse(e.Value.ToString()) < 0) || (double.Parse(e.Value.ToString()) > totalRemain))
                {
                    e.Valid = false;
                }
            }
        }

        private void gridView2_InvalidValueException(object sender, DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs e)
        {
            ColumnView view = sender as ColumnView;
            if (view == null) return;
            e.ExceptionMode = ExceptionMode.DisplayError;
            e.WindowCaption = "Σφάλμα τιμής";
            e.ErrorText = "Η ποσότητα παραγγελίας δε πρέπει να ξεπερνάει το διαθέσιμο υπόλοιπο του καταστήματος.";

            var grid = sender as GridView;
            var row = (ItemInfo)grid.GetFocusedRow();
            row.OrderQty = 0;
            view.HideEditor();
        }

        private void gridView1_CellValueChanging(object sender, CellValueChangedEventArgs e)
        {
            var gridView = sender as GridView;
            var row = (ItemSearch)gridView.GetRow(e.RowHandle);

            if (e.Column.FieldName == "OrderQty")
            {
                try
                {
                    var value = e.Value.ToString() == "" ? "0" : e.Value.ToString();
                    var orderQty = double.Parse(value);
                    var totalRemain = row.TotalRemain;
                    if (orderQty > totalRemain)
                    {
                        var colIndex = gridView.Columns.Where(x => x.FieldName == "OrderQty").FirstOrDefault()?.AbsoluteIndex.ToString();
                        if (!string.IsNullOrEmpty(colIndex))
                        {
                            var index = int.Parse(colIndex);
                            gridView.SetRowCellValue(e.RowHandle, gridView.Columns[index], "0");
                        }
                        throw new Exception("Η ποσότητα που πληκτρολογήσατε ξεπερνάει το διαθέσιμο υπόλοιπο της εταιρίας. H καταχώριση ακυρώνεται.");
                    }
                    row.OrderQty = orderQty;
                    //txtCusQty.Text = orderQty.ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Προσοχή!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            if (e.Column.FieldName == "InitQty")
            {
                var value = e.Value.ToString() == "" ? "0" : e.Value.ToString();
                var initQty = double.Parse(value);
                row.InitQty = initQty;
            }
        }

        private void gridView1_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            var gridView = sender as GridView;
            var row = (ItemSearch)gridView.GetRow(e.RowHandle);

            if (e.Column.FieldName == "OrderQty")
            {
                try
                {
                    var itemId = row.Id;
                    GetAvailabilityPerBranch(itemId);

                    if (row.InitQty == 0)
                    {
                        gridView.SetRowCellValue(e.RowHandle, gridView.Columns["InitQty"], row.OrderQty);
                    }
                    //txtCusQty.Text = row.OrderQty.ToString();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Προσοχή!", MessageBoxButtons.OK);
                }
            }
        }

        private void gridView2_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            var gridView = sender as GridView;
            var row = (ItemInfo)gridView.GetRow(e.RowHandle);
            var docWhouse = Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["WHOUSE"]);
            if (e.Column.FieldName == "OrderQty")
            {
                try
                {

                    var focusedRow = (ItemSearch)gridView1.GetFocusedRow();
                    var orderQty = focusedRow.OrderQty;

                    var sumEndoQty = cachedAvailabilityList
                        .Where(x => x.ProductId == row.ProductId)
                        .Where(x => x.WhouseId != docWhouse)
                        .Sum(x => x.OrderQty);

                    txtEndoQty.Text = sumEndoQty.ToString();

                    var sumQty = cachedAvailabilityList
                        .Where(x => x.ProductId == focusedRow.Id)
                        .Sum(x => x.OrderQty);

                    txtCusQty.Text = sumQty.ToString();
                    var docWHRemain = cachedAvailabilityList
                        .Where(x => x.WhouseId == docWhouse && x.ProductId == focusedRow.Id)
                        .Select(x => x.ActualRemain).First();

                    var currWhouseQty = cachedAvailabilityList
                                .Where(x => x.WhouseId == docWhouse && x.ProductId == focusedRow.Id)
                                .Select(x => x.OrderQty).First();



                    if (docWHRemain <= 0)
                    {
                        if (sumEndoQty >= orderQty)
                        {
                            txtCusQty.BackColor = Color.LightGreen;
                        }
                        else
                        {
                            txtCusQty.BackColor = Color.Red;
                        }
                    }
                    else
                    {
                        if ((currWhouseQty + sumEndoQty) >= orderQty)
                        {
                            txtCusQty.BackColor = Color.LightGreen;
                        }
                        else
                        {
                            txtCusQty.BackColor = Color.Red;
                        }
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Προσοχή!", MessageBoxButtons.OK);
                }
            }
        }

        private void txtCusQty_TextChanged(object sender, EventArgs e)
        {
            var txtQty = sender as TextEdit;
            txtQty.BackColor = Color.Salmon;
            var focusedRow = (ItemSearch)gridView1.GetFocusedRow();
            if (focusedRow != null)
            {
                var orderQty = focusedRow.OrderQty;
                if (availabilityList.Any())
                {
                    var sumQty = availabilityList
                        .Where(x => x.ProductId == focusedRow.Id)
                        .Sum(x => x.OrderQty);

                    if (sumQty > 0 && sumQty >= orderQty)
                    {
                        txtCusQty.BackColor = Color.LightGreen;
                    }
                }
            }
        }

        private void gridInfo_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType != NavigatorButtonType.Custom)
            {
                return;
            }
            if (gridInfo.DataSource == null)
            {
                return;
            }
            e.Handled = true;
            var data = (List<ItemSearch>)gridInfo.DataSource;
            var skip = 0;
            var take = _batchCount;
            string tag = e.Button.Tag.ToString();
            switch (tag)
            {
                case "NextPage":
                    skip = cachedList.Count;
                    break;
                case "PrevPage":
                    skip = cachedList.Count - _batchCount;
                    break;
            }
            gridInfo.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = skip > 0;
            if (tag == "NextPage")
            {
                var userWhouse = Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["WHOUSE"]);
                var docDate = Convert.ToDateTime(_xModule.GetTable("SALDOC").Current["TRNDATE"]);
                var searchValue = txtSearch.Text;
                items = _helper.GetItems(searchValue, "", userWhouse, docDate, skip, take);
                gridInfo.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = items.Count == _batchCount;

                if (items.Any())
                {
                    cachedList.AddRange(items);
                    gridInfo.DataSource = items;
                }
            }
            if (tag == "PrevPage")
            {
                items = cachedList.Skip(skip - 50).Take(_batchCount).ToList();
                gridInfo.DataSource = items;
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            var view = sender as GridView;
            if (view.FocusedColumn.FieldName != "OrderQty")
            {
                return;
            }
            var row = (ItemSearch)view.GetFocusedRow();
            var actualRemain = row.ActualRemain;
            var totalRemain = row.TotalRemain;
            //var docWhouse = Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["WHOUSE"]);
            if (totalRemain <= 0)
            {
                e.Cancel = true;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (_isRelFrm)
            {
                DialogResult = DialogResult.OK;
                this.Close();
                return;
            }
            try
            {
                var whouseId = Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["WHOUSE"]);
                var currentIntem = (ItemSearch)gridView1.GetFocusedRow();
                if (currentIntem != null)
                {
                    var cachedItem = cachedList.Where(x => x.Id == currentIntem.Id).FirstOrDefault();
                    if (cachedItem != null)
                    {
                        cachedItem.OrderQty = currentIntem.OrderQty;
                        cachedItem.InitQty = currentIntem.InitQty;
                    }
                    var orderQty = currentIntem.OrderQty;
                    var whActualRemain = currentIntem.ActualRemain;
                    var sumQty = cachedAvailabilityList.Where(x => x.ProductId == currentIntem.Id)
                                    .Sum(x => x.OrderQty);
                    var docWhouseQty = cachedAvailabilityList.Where(x => x.ProductId == currentIntem.Id && x.WhouseId == whouseId)
                                    .Sum(x => x.OrderQty);

                    if (orderQty > 0 && sumQty == 0)
                    {
                        var whItem = cachedAvailabilityList.Where(x => x.WhouseId == whouseId && x.ProductId == currentIntem.Id).FirstOrDefault();
                        if (whItem != null)
                        {
                            whItem.OrderQty = orderQty;
                        }
                    }
                    if (sumQty == 0 && orderQty > whActualRemain)
                    {
                        throw new Exception("Η ποσότητα παραγγελίας ξεπερνά το διαθέσιμο υπόλοιπο του καταστήματος!");
                    }
                    if (sumQty > 0 && sumQty < orderQty)
                    {
                        throw new Exception("Το άθροισμα της ποσότητας στον πίνακα ανάλυσης είναι μικρότερο από την ποσότητα παραγγελίας!");
                    }
                    if (sumQty > 0 && sumQty > orderQty)
                    {
                        throw new Exception("Το άθροισμα της ποσότητας στον πίνακα ανάλυσης είναι μεγαλύτερο από την ποσότητα παραγγελίας!");
                    }
                    if (sumQty > 0 && orderQty < sumQty)
                    {
                        throw new Exception("Η ποσότητα παραγγελίας είναι μικρότερη από Το άθροισμα της ποσότητας στον πίνακα ανάλυσης!");
                    }
                    
                    AppendItelines();
                    DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Προσοχή!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AppendItelines()
        {
            try
            {
                var itemsWithQuantity = cachedList.Where(x => x.OrderQty > 0).ToList();//_frm.availabilityList.Where(x => x.OrderQty > 0).ToList();
                if (itemsWithQuantity.Any())
                {
                    if (!btnSearch.Enabled) // located σε γραμμή
                    {
                        var mtrlObj = _xModule.GetTable("ITELINES").Current["MTRL"];
                        if (mtrlObj == DBNull.Value)
                        {
                            return;
                        }
                        var mtrl = Convert.ToInt32(mtrlObj);
                        var focusedItem = itemsWithQuantity.Where(x => x.Id == mtrl).First();
                        _xModule.GetTable("ITELINES").Current["QTY1"] = focusedItem.OrderQty;
                        _xModule.GetTable("ITELINES").Current["CCCINITQTY"] = focusedItem.InitQty;
                        _xModule.GetTable("ITELINES").Current.Post();
                    }
                    else
                    {
                        var itelinesMtrls = _xModule.GetTable("ITELINES").CreateDataTable(true).AsEnumerable().Select(x => Convert.ToInt32(x["MTRL"])).ToList();
                        var grouped = itemsWithQuantity.GroupBy(x => x.Id)
                                    .Select(y => new ItemInfo
                                    {
                                        ProductId = y.Key,
                                        OrderQty = y.Sum(z => z.OrderQty)
                                    }).ToList();
                        using (var Itelines = _xModule.GetTable("ITELINES"))
                        {
                            foreach (var group in grouped)
                            {
                                if (itelinesMtrls.Contains(group.ProductId))
                                {
                                    continue;
                                }
                                Itelines.Current.Append();
                                Itelines.Current["MTRL"] = group.ProductId;
                                Itelines.Current["QTY1"] = group.OrderQty;
                                var initQty = cachedList.Where(x => x.Id == group.ProductId).FirstOrDefault()?.InitQty;
                                if (initQty != null && initQty > 0)
                                {
                                    Itelines.Current["CCCINITQTY"] = initQty;
                                }
                                Itelines.Current.Post();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridView1_BeforeLeaveRow(object sender, RowAllowEventArgs e)
        {

            var gridSearch = sender as GridView;
            var row = (ItemSearch)gridSearch.GetFocusedRow();
            if (row == null)
            {
                return;
            }
            if (row.OrderQty == 0)
            {
                return;
            }

            var sumQty = availabilityList
                    .Where(x => x.ProductId == row.Id)
                    .Sum(x => x.OrderQty);

            if (sumQty > 0 && sumQty < row.OrderQty)
            {
                MessageBox.Show("Το άθροισμα της ποσότητας στον πίνακα ανάλυσης είναι μικρότερο από την ποσότητα παραγγελίας!", "Προσοχή!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Allow = false;
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs ea = e as DXMouseEventArgs;
            GridView view = sender as GridView;
            GridHitInfo info = view.CalcHitInfo(ea.Location);
            if (info.InRow || info.InRowCell)
            {
                if (info.Column.FieldName == "Code" || info.Column.FieldName == "Description")
                {
                    var row = view.GetFocusedRow();
                    if (row != null)
                    {
                        var item = (ItemSearch)row;
                        var id = item.Id;
                        LocateSoftoneItem(id);
                    }
                }
            }
        }

        private void LocateSoftoneItem(int id)
        {
            try
            {
                var cmd = $"ITEM[AUTOLOCATE={id}]";
                _xSupport.ExecS1Command(cmd, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Σφάλμα", MessageBoxButtons.OK);
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            bool bHandled = false;
            var check = txtSearch.ContainsFocus;
            if (check && keyData == Keys.Enter)
            {
                bHandled = true;
                int cmd = 0;
                if (!btnSearch.Enabled)
                {
                    cmd = 999002;
                    return bHandled;
                }
                SearchData(cmd);
            }

            if (keyData == Keys.F3)
            {
                bHandled = true;
                ExtendedSearchData();
            }
            return bHandled;
        }

        private void btnExtendedSearch_Click(object sender, EventArgs e)
        {
            ExtendedSearchData();
        }

        private void ExtendedSearchData()
        {
            var searchPattern = txtSearch.Text;
            if (string.IsNullOrEmpty(searchPattern) || searchPattern.Length < 3)
            {
                MessageBox.Show("Παρακαλώ πληκτρολογήστε τουλάχιστον 3 χαρακτήρες.", "Προσοχή!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (!splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.ShowWaitForm();
            }
            var b2bClient = new B2BClient();
            var products = b2bClient.GetExtendedSearchItems(searchPattern);
            if (!products.Any())
            {
                splashScreenManager1.CloseWaitForm();
                MessageBox.Show("Δεν βρέθηκαν επιπλέον είδη με βάση το φίλτρο αναζήτησης.", "Προσοχή", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            var relCodes = products.Select(x => $"'{x.Code}'").ToList();
            var inClause = string.Join(",", relCodes);
            var ids = new List<int>();
            var query = $"SELECT MTRL, CODE FROM MTRL WHERE COMPANY = {_xSupport.ConnectionInfo.CompanyId} AND SODTYPE = 51 AND CODE IN ({inClause})";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    for (int i = 0; i < ds.Count; i++)
                    {
                        var mtrl = ds.GetAsInteger(i, "MTRL");
                        if (mtrl > 0)
                        {
                            ids.Add(mtrl);
                        }
                    }
                }
            }
            var idsClause = string.Join(",", ids);
            RefreshGridData("", idsClause, 0);
            gridView1.BestFitColumns();
            splashScreenManager1.CloseWaitForm();
        }

        private void gridView1_DataSourceChanged(object sender, EventArgs e)
        {
            var gridview = sender as GridView;
            var focusedRow = gridview.GetFocusedRow();
            if (focusedRow == null)
            {
                return;
            }
            var focusedItem = focusedRow as ItemSearch;
            ChangeDetailInfo(focusedItem);
        }
    }
}
