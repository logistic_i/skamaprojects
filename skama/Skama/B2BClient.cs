﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Skama.Models;

namespace Skama
{
    public class B2BClient
    {
        private string _url;

        public B2BClient()
        {
            _url = "https://zerog.gr/edesign/api3/sm-s1";
        }

        internal List<RelativeProduct> GetRelatives(string itemCode)
        {
            _url = $"{_url}/items/{itemCode}/relatives";
            var relProducts = new List<RelativeProduct>();
            var res = string.Empty;

            try
            {
                using (var webClient = new WebClient())
                {
                    webClient.Encoding = Encoding.UTF8;
                    res = webClient.DownloadString(_url);
                }
                if (!string.IsNullOrEmpty(res))
                {
                    var deserialized = JsonConvert.DeserializeObject<RelProductResponse>(res);
                    if (deserialized.StatusCode == "200")
                    {
                        if (deserialized.RelProducts == null)
                        {
                            return relProducts;
                        }
                        foreach (var product in deserialized.RelProducts)
                        {
                            relProducts.Add(product);
                        }
                    }
                    else
                    {
                        throw new Exception(deserialized.StatusText);
                    }
                }
                return relProducts;
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<ExtendedProduct> GetExtendedSearchItems(string searchPattern)
        {
            _url = $"{_url}/items?search_pattern={searchPattern}";
            var products = new List<ExtendedProduct>();
            var res = string.Empty;

            try
            {
                using (var webClient = new WebClient())
                {
                    webClient.Encoding = Encoding.UTF8;
                    res = webClient.DownloadString(_url);
                }
                if (!string.IsNullOrEmpty(res))
                {
                    var deserialized = JsonConvert.DeserializeObject<ExtProductResponse>(res);
                    if (deserialized.StatusCode == "200")
                    {
                        if (deserialized.Products != null)
                        {
                            foreach (var product in deserialized.Products)
                            {
                                products.Add(product);
                            }
                        }
                    }
                    else
                    {
                        throw new Exception(deserialized.StatusText);
                    }
                }
                return products;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
