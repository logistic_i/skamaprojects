﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Enums
{
    public enum FieldType
    {
        String,
        Int,
        Float,
        Datetime
    }
}
