﻿using Skama.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Softone;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace Skama
{
    public partial class RestAnalysisFrm : Form
    {
        private XSupport _xSupport;

        public RestAnalysisFrm()
        {
            InitializeComponent();
        }

        public RestAnalysisFrm(XSupport xSupport)
        {
            _xSupport = xSupport;
            InitializeComponent();
        }

        internal void RefreshDataGrids(List<RestData> restList, int whouseId)
        {
            try
            {
                var whAnalysis = false;
                if (whouseId > 0)
                {
                    whAnalysis = true;
                }
                var ordered = restList.Where(x => x.RestCategory == 1).ToList();
                if (whAnalysis)
                {
                    ordered = ordered.Where(x => x.WHId == whouseId).ToList();
                }

                var reserved = restList.Where(x => x.RestCategory == 2).ToList();
                if (whAnalysis)
                {
                    reserved = reserved.Where(x => x.WHId == whouseId).ToList();
                }

                if (ordered.Any())
                {
                    SetDataSource(gridView1, gridOrdered, ordered, whAnalysis);
                }

                if (reserved.Any())
                {
                    SetDataSource(gridView2, gridReserved, reserved, whAnalysis);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SetDataSource(GridView gridView, GridControl grid, List<RestData> data, bool whAnalysis)
        {
            grid.DataSource = data;
            if (whAnalysis)
            {
                gridView1.Columns["WHName"].Visible = false;
                gridView2.Columns["WHName"].Visible = false;
            }
            gridView.BestFitColumns();
            gridView.OptionsBehavior.ReadOnly = true;
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs ea = e as DXMouseEventArgs;
            GridView view = sender as GridView;
            GridHitInfo info = view.CalcHitInfo(ea.Location);
            if (info.InRow || info.InRowCell)
            {
                var row = view.GetFocusedRow();
                if (row != null)
                {
                    var item = (RestData)row;
                    LocateSoftoneDoc(item);
                }
            }
        }

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs ea = e as DXMouseEventArgs;
            GridView view = sender as GridView;
            GridHitInfo info = view.CalcHitInfo(ea.Location);
            if (info.InRow || info.InRowCell)
            {
                var row = view.GetFocusedRow();
                if (row != null)
                {
                    var item = (RestData)row;
                    LocateSoftoneDoc(item);
                }
            }
        }

        private void LocateSoftoneDoc(RestData item)
        {
            var sosource = item.SoSource;
            var findoc = item.Findoc;
            var objectName = string.Empty;
            switch (sosource)
            {
                case 1351:
                    objectName = "SALDOC";
                    break;
                case 1251:
                    objectName = "PURDOC";
                    break;
                case 1151:
                    objectName = "ITEDOC";
                    break;
            }
            if (!string.IsNullOrEmpty(objectName))
            {
                var cmd = $"{objectName}[AUTOLOCATE={findoc}]";
                _xSupport.ExecS1Command(cmd, null);
            }
        }
    }
}


