﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Softone;
using Skama.Models;
using System.Data;
using Newtonsoft.Json;
using System.Dynamic;
using System.Globalization;
using System.IO;
using Microsoft.Win32;
using System.Threading.Tasks;

namespace Skama
{
    public class Helper
    {
        private readonly XSupport _xSupport;
        private readonly XModule _xModule;
        private List<SkamaBranch> _orderBranches;
        private List<SkamaBranch> _receiptBranches;
        private S1Tools _tools;
        public List<ItemInfo> cachedAvailabilityList;
        private string _XCOName { get; set; }

        public Helper(XSupport xSupport, XModule xModule)
        {
            //var autoSupport = LoginWithAutomationUser(xSupport);
            _xSupport = xSupport;//autoSupport == null ? xSupport : autoSupport;
            _xModule = xModule;
            var skamaBranches = GetSkamaBranches();
            _orderBranches = skamaBranches.Where(x => x.Sodtype == 13).ToList();
            _receiptBranches = skamaBranches.Where(x => x.Sodtype == 12).ToList();
            _tools = new S1Tools(_xSupport);
            _XCOName = GetCustomXCOName();

        }

        private List<SkamaBranch> GetSkamaBranches()
        {
            var branches = new List<SkamaBranch>();
            var query = $"SELECT * FROM GT_SKAMABRANCHES ORDER BY 1,2,6";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    for (int i = 0; i < ds.Count; i++)
                    {
                        branches.Add(new SkamaBranch
                        {
                            Sodtype = ds.GetAsInteger(i, "SODTYPE"),
                            Trdr = ds.GetAsInteger(i, "TRDR"),
                            Trdbranch = ds.GetAsInteger(i, "TRDBRANCH"),
                            Code = ds.GetAsInteger(i, "CODE"),
                            Name = ds.GetAsInteger(i, "NAME"),
                            BranchId = ds.GetAsInteger(i, "CCCBRANCH")
                        });
                    }
                }
            }
            return branches;
        }

        private string GetCustomXCOName()
        {
            var xcoName = string.Empty;
            var query = "SELECT TOP 1 XCONAME FROM CCCGKISEUSER ";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                   xcoName = ds.GetAsString(0, "XCONAME");
                }
            }
            return xcoName;
        }

        internal XSupport LoginWithAutomationUser(XSupport xSupport)
        {
            XSupport autoSupport = null;
            var query = "SELECT TOP 1 C.XCONAME, U.CODE AS USERNAME, C.PASSWORD FROM USERS U INNER JOIN CCCGKISEUSER C ON C.USERS = U.USERS";
            using (var ds = xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    var xcoName = ds.GetAsString(0, "XCONAME");
                    var username = ds.GetAsString(0, "USERNAME");
                    var password = ds.GetAsString(0, "PASSWORD");
                    var company = xSupport.ConnectionInfo.CompanyId;
                    var branch = xSupport.ConnectionInfo.BranchId;
                    try
                    {
                        autoSupport = XSupport.Login(xcoName, username, password, company, branch, xSupport.ConnectionInfo.LoginDate);
                    }
                    catch (Exception)
                    {

                    }                   
                }
            }
            return autoSupport;
        }

        private List<OrderBranch> GetOrderBranches(int sodtype)
        {
            var orderBranches = new List<OrderBranch>();
            var query = $"SELECT TRDR, CCCBRANCH FROM TRDR WHERE COMPANY = {_xSupport.ConnectionInfo.CompanyId} AND SODTYPE = {sodtype} AND ISACTIVE = 1 AND CCCBRANCH IS NOT NULL";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    for (int i = 0; i < ds.Count; i++)
                    {
                        orderBranches.Add(new OrderBranch
                        {
                            Trdr = ds.GetAsInteger(i, "TRDR"),
                            BranchId = ds.GetAsInteger(i, "CCCBRANCH")
                        });
                    }
                }
            }
            return orderBranches;
        }

        internal bool ValidCustomParams()
        {
            var query = "SELECT TOP 1 C.XCONAME, U.CODE AS USERNAME, C.PASSWORD FROM USERS U INNER JOIN CCCGKISEUSER C ON C.USERS = U.USERS";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    var xcoName = ds.GetAsString(0, "XCONAME");
                    var username = ds.GetAsString(0, "USERNAME");
                    if (string.IsNullOrEmpty(xcoName))
                    {
                        return false;
                    }
                    if (string.IsNullOrEmpty(username))
                    {
                        return false;
                    }
                    return true;
                }
                else
                {
                    return true;
                }
            }
        }

        internal void AppendToLogTable(string errorMsg, string stackTrace = "")
        {
            try
            {
                var soft1User = Convert.ToInt32(_xModule.GetTable("FINDOC").Current["INSUSER"]);
                var findoc = Convert.ToInt32(_xModule.GetTable("FINDOC").Current["FINDOC"]);
                var logDate = DateTime.Now;
                using (var LogObj = _xSupport.CreateModule("CCCLOGOBJ"))
                {
                    _tools.HideWarningsFromS1Module(LogObj);
                    LogObj.InsertData();
                    LogObj.GetTable("CCCCUSTOMLOGS").Current["SOFT1USER"] = soft1User;
                    LogObj.GetTable("CCCCUSTOMLOGS").Current["FINDOC"] = findoc;
                    LogObj.GetTable("CCCCUSTOMLOGS").Current["ERRORMSG"] = errorMsg;
                    if (!string.IsNullOrEmpty(stackTrace))
                    {
                        LogObj.GetTable("CCCCUSTOMLOGS").Current["STACKTRACE"] = stackTrace;
                    }
                    LogObj.GetTable("CCCCUSTOMLOGS").Current["LOGDATE"] = logDate;
                    LogObj.PostData();
                }
            }
            catch(Exception ex)
            {
                var error = ex.Message;
            }
        }

        internal void ReverseMaxFindoc(int findoc)
        {
            var q = $"SELECT FINDOC FROM MTRDOC WHERE CCCMAXFINDOC = {findoc}";
            using (var ds = _xSupport.GetSQLDataSet(q, null))
            {
                if (ds.Count > 0)
                {
                    for (int i = 0; i < ds.Count; i++)
                    {
                        var initFindoc = ds.GetAsInteger(i, "FINDOC");
                        var qq = $"SELECT MAX(FINDOC) AS FN FROM MTRLINES WHERE CCCINITFINDOC = {initFindoc}";
                        using (var dss = _xSupport.GetSQLDataSet(qq, null))
                        {
                            if (dss.Count > 0)
                            {
                                for (int j = 0; j < dss.Count; j++)
                                {
                                    var relFindoc = dss.GetAsInteger(j, "FN");
                                    if (relFindoc == 0)
                                    {
                                        continue;
                                    }
                                    var updQ = $"UPDATE MTRDOC SET CCCMAXFINDOC = {relFindoc} WHERE FINDOC = {initFindoc}";
                                    _xSupport.ExecuteSQL(updQ, null);                                   
                                }
                            }
                        }
                    }
                }
            }
        }

        internal List<ItemSearch> GetItems(string searchValue, string inClause, int userWhouse, DateTime docDate, int skip = 0, int take = 0)
        {
            var todate = docDate.AddDays(1).ToString("yyyyMMdd");
            var fromdate = new DateTime(docDate.Year, docDate.Month, 1).ToString("yyyyMMdd");
            searchValue = RemoveSpecialChars(searchValue);
            if (string.IsNullOrEmpty(inClause))
            {
                inClause = "''";
            }
            else
            {
                inClause = $"'{inClause}'";
            }
            var fiscprd = docDate.Year;
            var period = docDate.Month;

            var items = new List<ItemSearch>();
            try
            {
                var query = $@"DECLARE @whouse int, @fiscprd int, @period int
                                , @fromdate nvarchar(10), @todate nvarchar(10)
                                , @searchvalue nvarchar(1000), @skip int, @take int
                                EXECUTE [dbo].[GetItems] @whouse = {userWhouse} ,@fiscprd = {fiscprd} ,@period = {period} 
                                ,@fromdate = '{fromdate}' ,@todate = '{todate}' ,@searchvalue = '{searchValue}', @inClause = {inClause} ,@skip = {skip} ,@take = {take}";

                using (var ds = _xSupport.GetSQLDataSet(query, null))
                {
                    if (ds.Count > 0)
                    {
                        for (int i = 0; i < ds.Count; i++)
                        {
                            items.Add(new ItemSearch
                            {
                                Id = ds.GetAsInteger(i, "Id"),
                                Code = ds.GetAsString(i, "Code"),
                                Description = ds.GetAsString(i, "Description"),
                                ABCRanking = ds.GetAsString(i, "ABCRanking"),
                                Price = ds.GetAsFloat(i, "Price"),
                                PriceR = ds.GetAsFloat(i, "PriceR"),
                                CustomerPrice = ds.GetAsFloat(i, "CustomerPrice"),
                                WHRemain = ds.GetAsFloat(i, "WHRemain"),
                                TotalRemain = ds.GetAsFloat(i, "TotalRemain") - ds.GetAsFloat(i, "Reserved"),
                                ActualRemain = ds.GetAsFloat(i, "WHRemain") - ds.GetAsFloat(i, "WHReserved"),
                                WHExpected = ds.GetAsFloat(i, "WHExpected"),
                                WHReserved = ds.GetAsFloat(i, "WHReserved"),
                                Expected = ds.GetAsFloat(i, "Expected"),
                                Reserved = ds.GetAsFloat(i, "Reserved")
                            });
                        }
                    }
                }

                return items;
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<ItemSearch> GetCustomerPrices(List<ItemSearch> items)
        {
            try
            {
                if (items != null)
                {
                    var linenum = 9990001;

                    var data = new
                    {
                        SALDOC = new
                        {
                            TRNDATE = Convert.ToDateTime(_xModule.GetTable("SALDOC").Current["TRNDATE"]).ToString("yyyy-MM-dd"),
                            SERIES = Convert.ToInt32(_xModule.GetTable("SALDOC").Current["SERIES"]).ToString(),
                            TRDR = Convert.ToInt32(_xModule.GetTable("SALDOC").Current["TRDR"]).ToString()
                        },
                        ITELINES = items.Select(x => new
                        {
                            MTRL = x.Id.ToString(),
                            QTY1 = "1",
                            LINENUM = linenum++
                        }).ToList()
                    };
                    var calcReq = new CalcRequest
                    {
                        Object = "SALDOC",
                        LocateInfo = "ITELINES:MTRL,LINEVAL",
                        Data = data
                    };
                    var calcStr = JsonConvert.SerializeObject(calcReq);
                    var resStr = _xSupport.CallWebService(calcStr, out string outstr);
                    var res = JsonConvert.DeserializeObject<CalcResp>(resStr);
                    if (res.Success)
                    {
                        items.ForEach(itm =>
                        {
                            var product = res.Products.Products.FirstOrDefault(x => int.Parse(x.ProductId) == itm.Id);
                            if (product != null)
                            {
                                itm.CustomerPrice = double.Parse(product.FinalPrice, NumberStyles.Number, CultureInfo.InvariantCulture);
                            }

                        });
                    }
                }

                return items;
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                throw;
            }
        }

        internal double GetItemFinalPrice(ItemSearch item)
        {
            double finalPrice = 0;

            var customerObj = _xModule.GetTable("SALDOC").Current["TRDR"];
            if (customerObj == DBNull.Value)
            {
                return 0;
            }

            if (item == null)
            {
                return finalPrice;
            }
            using (var Saldoc = _xSupport.CreateModule("SALDOC"))
            {
                _tools.HideWarningsFromS1Module(Saldoc);
                Saldoc.InsertData();
                Saldoc.GetTable("SALDOC").Current["SERIES"] = Convert.ToInt32(_xModule.GetTable("SALDOC").Current["SERIES"]);
                Saldoc.GetTable("SALDOC").Current["TRNDATE"] = Convert.ToDateTime(_xModule.GetTable("SALDOC").Current["TRNDATE"]);
                Saldoc.GetTable("SALDOC").Current["TRDR"] = Convert.ToInt32(_xModule.GetTable("SALDOC").Current["TRDR"]);
                using (var Itelines = Saldoc.GetTable("ITELINES"))
                {
                    Itelines.Current.Append();
                    Itelines.Current["MTRL"] = item.Id;
                    Itelines.Current["QTY1"] = (double)1;
                    finalPrice = Convert.ToDouble(Itelines.Current["LINEVAL"]);
                }
            }
            return finalPrice;
        }

        internal void UpdateMaxFindoc(int maxfindoc)
        {
            var query = $"SELECT DISTINCT CCCINITFINDOC FROM MTRLINES WHERE FINDOC = {maxfindoc}";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    for (int i = 0; i < ds.Count; i++)
                    {
                        var initfindoc = ds.GetAsInteger(i, "CCCINITFINDOC");
                        if (initfindoc == 0)
                        {
                            continue;
                        }
                        var q = $"UPDATE MTRDOC SET CCCMAXFINDOC = {maxfindoc} WHERE FINDOC = {initfindoc}";
                        _xSupport.ExecuteSQL(q, null);
                    }
                }
            }
        }

        internal List<ItemInfo> GetItemInfo(int itemId, DateTime docDate)
        {
            try
            {
                var infos = new List<ItemInfo>();
                var trndate = docDate.ToString("yyyyMMdd");
                var fiscprd = docDate.Year;

                var query = $@"DECLARE @mtrl int, @fiscprd int, @trndate varchar(10)
                              EXECUTE [dbo].[GetItemInfo] @mtrl = {itemId} ,@fiscprd = {fiscprd} ,@trndate = '{trndate}'";

                using (var ds = _xSupport.GetSQLDataSet(query, null))
                {
                    if (ds.Count > 0)
                    {
                        for (int i = 0; i < ds.Count; i++)
                        {
                            infos.Add(new ItemInfo
                            {
                                ProductId = itemId,
                                WhouseId = ds.GetAsInteger(i, "WhouseId"),
                                WHCode = ds.GetAsString(i, "WHCode"),
                                WHDescription = ds.GetAsString(i, "WHDescription"),
                                TotalRemain = ds.GetAsFloat(i, "TotalRemain"),
                                NoMinStockRemain = ds.GetAsFloat(i, "NoMinStockRemain"),
                                ExpectedQty = ds.GetAsFloat(i, "ExpectedQty"),
                                ReservedQty = ds.GetAsFloat(i, "ReservedQty"),
                                ActualRemain = ds.GetAsFloat(i, "ActualRemain")
                            });
                        }
                    }
                }

                return infos;
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<RestData> GetRestAnalysis(int itemId)
        {
            try
            {
                var restDataList = new List<RestData>();
                var query = $"SELECT * FROM GT_RESTANALYSIS WHERE ProductId = {itemId}";
                using (var ds = _xSupport.GetSQLDataSet(query, null))
                {
                    if (ds.Count > 0)
                    {
                        for (int i = 0; i < ds.Count; i++)
                        {
                            restDataList.Add(new RestData
                            {
                                Findoc = ds.GetAsInteger(i, "Findoc"),
                                SoSource = ds.GetAsInteger(i, "SoSource"),
                                Fincode = ds.GetAsString(i, "Fincode"),
                                WHId = ds.GetAsInteger(i, "WHId"),
                                WHName = ds.GetAsString(i, "WHName"),
                                SeriesName = ds.GetAsString(i, "SeriesName"),
                                Date = ds.GetAsDateTime(i, "Date").ToString("dd/MM/yyyy"),
                                DelivDate = ds.GetAsDateTime(i, "DelivDate").ToString("yyyyMMdd") != "18991230" 
                                ? ds.GetAsDateTime(i, "DelivDate").ToString("dd/MM/yyyy") : "",
                                ShipDate = ds.GetAsDateTime(i, "ShipDate").ToString("yyyyMMdd") != "18991230" 
                                ? ds.GetAsDateTime(i, "ShipDate").ToString("dd/MM/yyyy") : "",
                                TrdrName = ds.GetAsString(i, "TrdrName"),
                                BillTrdrName = ds.GetAsString(i, "BillTrdrName"),
                                ProductId = ds.GetAsInteger(i, "ProductId"),
                                ProductCode = ds.GetAsString(i, "ProductCode"),
                                ProductName = ds.GetAsString(i, "ProductName"),
                                RestCategory = ds.GetAsInteger(i, "RestCategory"),
                                RestQuantity = ds.GetAsFloat(i, "RestQuantity"),
                            });
                        }
                    }
                }

                return restDataList;

            }
            catch (Exception)
            {
                throw;
            }
        }

        internal void GenerateProposedQuantities(List<ItemInfo> sortedList, double docQuantity)
        {
            try
            {
                foreach (var item in sortedList)
                {
                    item.PropQty = 0;
                }
                if (docQuantity == 0)
                {
                    return;
                }
                var tmpQuantity = docQuantity;

                foreach (var item in sortedList)
                {
                    if (tmpQuantity > 0)
                    {
                        if (tmpQuantity <= item.ActualRemain)
                        {
                            item.PropQty = tmpQuantity;
                            tmpQuantity -= item.PropQty;
                        }
                        else
                        {
                            item.PropQty = item.ActualRemain < 0 ? 0.0 : item.ActualRemain;
                            tmpQuantity -= item.PropQty;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<ItemInfo> SortItems(List<ItemInfo> itemInfoList, int docWhouse)
        {
            try
            {
                var sortedList = new List<ItemInfo>();
                var query = $@"SELECT D.SORT, D.WHOUSE 
                                FROM CCCBRANCHSORT H 
                                INNER JOIN CCCBRANCHSORTDETAIL D ON D.CCCBRANCHSORT = H.CCCBRANCHSORT 
                                WHERE H.WHOUSE = {docWhouse}
                                ORDER BY CCCBRANCHSORTDETAIL";
                using (var ds = _xSupport.GetSQLDataSet(query, null))
                {
                    if (ds.Count > 0)
                    {
                        for (int i = 0; i < ds.Count; i++)
                        {
                            var whouseId = ds.GetAsInteger(i, "WHOUSE");
                            var whRec = itemInfoList.FirstOrDefault(x => x.WhouseId == whouseId);
                            if (whRec != null)
                            {
                                sortedList.Add(whRec);
                            }
                        }
                        return sortedList;
                    }
                }

                var whRecord = itemInfoList.Where(x => x.WhouseId == docWhouse).FirstOrDefault();
                if (whRecord != null)
                {
                    sortedList.Add(whRecord);
                    itemInfoList.Remove(whRecord);
                }

                var branches = _xSupport.GetMemoryTable("BRANCH")
                        .CreateDataTable(true)
                        .AsEnumerable()
                        .ToList()
                        .Where(x => Convert.ToInt32(x["COMPANY"]) == _xSupport.ConnectionInfo.CompanyId)
                        .ToList();

                foreach (var item in itemInfoList)
                {
                    var sort = branches
                        .Where(x => x["WHOUSES"].ToString().Contains(item.WhouseId.ToString()))
                        .Select(x => x["CCCSORT"] == DBNull.Value ? 9999 : Convert.ToInt32(x["CCCSORT"]))
                        .FirstOrDefault();
                    item.WHSort = sort;
                }


                var whouseTmp = _xSupport.GetMemoryTable("WHOUSE")
                    .CreateDataTable(true)
                    .AsEnumerable()
                    .ToList()
                    .Where(x => Convert.ToInt32(x["COMPANY"]) == _xSupport.ConnectionInfo.CompanyId)
                    .ToList();

                var docWhouseZone = branches
                    .Where(x => x["WHOUSES"].ToString().Contains(docWhouse.ToString()))
                    .Select(x => x["CCCBRANCHZONE"].ToString())
                    .FirstOrDefault();

                if (string.IsNullOrWhiteSpace(docWhouseZone))
                {
                    docWhouseZone = "99";
                }

                var sameZoneWhousesIds = branches
                    .Where(x => x["CCCBRANCHZONE"].ToString() == docWhouseZone)
                    .ToList()
                    .Select(x => x["WHOUSES"].ToString())
                    .ToList();

                var sameZoneWhouses = new BindingList<ItemInfo>();
                foreach (var id in sameZoneWhousesIds)
                {
                    var item = itemInfoList.Where(x => id.Contains(x.WhouseId.ToString())).FirstOrDefault();
                    if (item != null)
                    {
                        sameZoneWhouses.Add(item);
                    }
                }

                var sortedZoneWh = sameZoneWhouses.OrderBy(x => x.WHSort).ToList();
                foreach (var wh in sortedZoneWh)
                {
                    sortedList.Add(wh);
                    itemInfoList.Remove(wh);
                }

                itemInfoList.OrderBy(x => x.WHSort).ToList();
                foreach (var wh in itemInfoList)
                {
                    sortedList.Add(wh);
                }

                return sortedList;
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                var stackTrace = ex.StackTrace;
                AppendToLogTable(errorMsg, stackTrace);
                return itemInfoList;
            }
        }

        internal void PostOrdersFromOrdered(int paranSeries, int startBranch, int nextBranch, int itemId, double qty1)
        {
            try
            {
                var currentWhouse = _xModule.GetTable("MTRDOC").Current["WHOUSE"];
                var currentBranch = Convert.ToInt32(_xModule.GetTable("SALDOC").Current["BRANCH"]);
                var parepSeries = _tools.GetIdFromMemoryTable("SERIES", "SOSOURCE;FPRMS;WHOUSE", "SERIES", 1351, 10146, currentWhouse);

                var order = new OrderDocument();
                order.Saldoc = new DocHeader
                {
                    Trndate = Convert.ToDateTime(_xModule.GetTable("SALDOC").Current["TRNDATE"]).ToString("yyyy-MM-dd"),
                    Series = parepSeries,
                    Trdr = Convert.ToInt32(_xModule.GetTable("SALDOC").Current["TRDR"]),
                    TrdBranch = _xModule.GetTable("SALDOC").Current["TRDBRANCH"] != DBNull.Value
                                            ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["TRDBRANCH"])
                                            : (int?)null,
                    Priority = _xModule.GetTable("SALDOC").Current["PRIORITY"] != DBNull.Value
                                            ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["PRIORITY"])
                                            : (int?)null,
                    Payment = _xModule.GetTable("SALDOC").Current["PAYMENT"] != DBNull.Value
                                            ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["PAYMENT"])
                                            : (int?)null,
                    Shipment = _xModule.GetTable("SALDOC").Current["SHIPMENT"] != DBNull.Value
                                            ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["SHIPMENT"])
                                            : (int?)null,
                    SalesMan = _xModule.GetTable("SALDOC").Current["SALESMAN"] != DBNull.Value
                                            ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["SALESMAN"])
                                            : (int?)null,
                    ShipKind = _xModule.GetTable("SALDOC").Current["SHIPKIND"] != DBNull.Value
                                            ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["SHIPKIND"])
                                            : (int?)null,
                    Comments = _xModule.GetTable("SALDOC").Current["COMMENTS"].ToString(),
                    Remarks = _xModule.GetTable("SALDOC").Current["REMARKS"].ToString(),
                    InfoOrder = _xModule.GetTable("SALDOC").Current["CCCINFOORDER"] != DBNull.Value
                                            ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["CCCINFOORDER"])
                                            : (int?)null,
                    CustomShipAddress = _xModule.GetTable("SALDOC").Current["CCCSHIPPINGADDR"] != DBNull.Value
                                            ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["CCCINFOORDER"])
                                            : (int?)null
                };
                order.Mtrdoc = new DocMtrdoc
                { 
                    Socarrier = _xModule.GetTable("MTRDOC").Current["SOCARRIER"] != DBNull.Value
                                        ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["SOCARRIER"])
                                        : (int?)null,
                    Trucks = _xModule.GetTable("MTRDOC").Current["TRUCKS"] != DBNull.Value
                                        ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["TRUCKS"])
                                        : (int?)null,
                    TrucksNo = _xModule.GetTable("MTRDOC").Current["TRUCKSNO"].ToString(),
                    Routing = _xModule.GetTable("MTRDOC").Current["ROUTING"] != DBNull.Value
                                        ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["ROUTING"])
                                        : (int?)null,
                    BranchSec = startBranch,
                    ShippingAddress = _xModule.GetTable("MTRDOC").Current["SHIPPINGADDR"].ToString(),
                    ShipZip = _xModule.GetTable("MTRDOC").Current["SHPZIP"].ToString(),
                    ShipDistrict = _xModule.GetTable("MTRDOC").Current["SHPDISTRICT"].ToString(),
                    ShipCity = _xModule.GetTable("MTRDOC").Current["SHPCITY"].ToString(),
                    ShipDate = _xModule.GetTable("MTRDOC").Current["SHIPDATE"] != DBNull.Value
                                        ? Convert.ToDateTime(_xModule.GetTable("MTRDOC").Current["SHIPDATE"])
                                        : (DateTime?)null,
                    DelivDate = _xModule.GetTable("MTRDOC").Current["DELIVDATE"] != DBNull.Value
                                        ? Convert.ToDateTime(_xModule.GetTable("MTRDOC").Current["DELIVDATE"])
                                        : (DateTime?)null,
                    ShipTrdr = _xModule.GetTable("MTRDOC").Current["SHIPTRDR"] != DBNull.Value
                                        ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["SHIPTRDR"])
                                        : (int?)null,
                    BillTrdr = _xModule.GetTable("MTRDOC").Current["BILLTRDR"] != DBNull.Value
                                        ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["BILLTRDR"])
                                        : (int?)null,
                    OrderTrdr = _xModule.GetTable("MTRDOC").Current["ORDERTRDR"] != DBNull.Value
                                        ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["ORDERTRDR"])
                                        : (int?)null,
                    ShipTrdbranch = _xModule.GetTable("MTRDOC").Current["SHIPTRDBRANCH"] != DBNull.Value
                                        ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["SHIPTRDBRANCH"])
                                        : (int?)null,
                    BillTrdBranch = _xModule.GetTable("MTRDOC").Current["BILLTRDBRANCH"] != DBNull.Value
                                        ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["BILLTRDBRANCH"])
                                        : (int?)null,
                    OrderTrdBranch = _xModule.GetTable("MTRDOC").Current["ORDERTRDBRANCH"] != DBNull.Value
                                        ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["ORDERTRDBRANCH"])
                                        : (int?)null,
                };
                order.IteLines = new List<DocLine>();
                order.IteLines.Add(new DocLine
                {
                    Linenum = 9990001,
                    Mtrl = itemId,
                    Qty1 = qty1
                });

                dynamic paramss = new ExpandoObject();
                paramss.ISCUSTOM = 1;
                SetDataResponse res = null;
                res = _tools.PostDocByWebMethod(_xModule.ObjName, "", order, paramss);

                if (res != null && res.Success)
                {
                    var id = Convert.ToInt32(res.Id);
                    RemoveReservedQuantities(id);

                    order.Saldoc.Series = paranSeries;
                    var skamaId = _orderBranches.First().Trdr;
                    var trdbranchId = _orderBranches.Where(x => x.BranchId == nextBranch).FirstOrDefault()?.Trdbranch;
                    order.Mtrdoc.BillTrdr = order.Saldoc.Trdr;
                    order.Mtrdoc.BillTrdBranch = order.Saldoc.TrdBranch;
                    order.Saldoc.Trdr = skamaId;
                    order.Saldoc.TrdBranch = trdbranchId;
                    order.Mtrdoc.BranchSec = currentBranch;

                    var isAdmin = _xSupport.ConnectionInfo.IsAdministrator;
                    SetDataResponse result = null;
                    if (!isAdmin)
                    {
                        using (var autoSupport = LoginWithAutomationUser(_xSupport))
                        {
                            var autoTools = new S1Tools(autoSupport);
                            result = autoTools.PostDocByWebMethod("SALDOC", "", order, paramss);
                        }
                    }
                    else
                    {
                        result = _tools.PostDocByWebMethod("SALDOC", "", order, paramss);
                    }
                    if (result != null && !result.Success)
                    {
                        var errorMsg = result.Error;
                        AppendToLogTable(errorMsg);
                    }
                    else
                    {
                        var newId = result.Id;
                        var query = $"UPDATE FINDOC SET INSUSER = {_xSupport.ConnectionInfo.UserId}, UPDUSER = {_xSupport.ConnectionInfo.UserId} WHERE FINDOC = {newId}";
                        _xSupport.ExecuteSQL(query, null);
                    }
                    ResetLastLogin();
                }
                else
                {
                    throw new Exception(res.Error);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<AutomationDoc> GenerateRelDocs()
        {
            var docs = new List<AutomationDoc>();
            var findocItems = new List<FindocItem>();
            var findocId = Convert.ToInt32(_xModule.GetTable("SALDOC").Current["FINDOC"]);
            try
            {
                var docWhouse = Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["WHOUSE"]);
                var itemData = _xModule.GetTable("CCCITEMINFO").CreateDataTable(true).AsEnumerable().ToList();
                if (!itemData.Any())
                {
                    return docs;
                }

                foreach (var item in itemData)
                {
                    var recNo = _xModule.GetTable("ITELINES").Find("MTRLINES", int.Parse(item["MTRLINES"].ToString()));
                    var price = _xModule.GetTable("ITELINES").GetAsFloat(recNo, "PRICE");
                    var disc1Prc = _xModule.GetTable("ITELINES").GetAsFloat(recNo, "DISC1PRC");
                    var disc2Prc = _xModule.GetTable("ITELINES").GetAsFloat(recNo, "DISC2PRC");
                    var disc3Prc = _xModule.GetTable("ITELINES").GetAsFloat(recNo, "DISC3PRC");
                    var comments = _xModule.GetTable("ITELINES").GetAsString(recNo, "COMMENTS");
                    var relDocs = _xModule.GetTable("ITELINES").GetAsString(recNo, "CCCRELDOCS");
                    findocItems.Add(new FindocItem
                    {
                        Whouse = int.Parse(item["WHOUSE"].ToString()),
                        Findocs = findocId,
                        Mtrliness = int.Parse(item["MTRLINES"].ToString()),
                        Mtrl = int.Parse(item["MTRL"].ToString()),
                        Qty1 = double.Parse(item["ORDERQTY"].ToString()),
                        Price = price,
                        Disc1Prc = disc1Prc,
                        Disc2Prc = disc2Prc,
                        Disc3Prc = disc3Prc,
                        Comments = comments,
                        Reldocs = relDocs,
                        InitFindoc = findocId,
                        DirectShipment = item["DIRECT"] != DBNull.Value
                        ? int.Parse(item["DIRECT"].ToString()) == 1 ? true : false
                        : false
                    });
                }

                var lineGroup = findocItems.GroupBy(x => x.Mtrliness).ToList();
                var items = new List<FindocItem>();
                lineGroup.ForEach(line =>
                {
                    var byWh = line.ToList().GroupBy(x => x.Whouse)
                        .Select(y => new FindocItem
                        {
                            Mtrliness = y.First().Mtrliness,
                            Whouse = y.Key,
                            Findocs = findocId,
                            Mtrl = y.First().Mtrl,
                            Qty1 = y.Sum(z => z.Qty1),
                            Price = y.First().Price,
                            Disc1Prc = y.First().Disc1Prc,
                            Disc2Prc = y.First().Disc2Prc,
                            Disc3Prc = y.First().Disc3Prc,
                            Comments = y.First().Comments,
                            Reldocs = y.First().Reldocs,
                            InitFindoc = y.First().InitFindoc,
                            DirectShipment = y.First().DirectShipment
                        }).ToList();

                    items.AddRange(byWh);
                });

                var docWhouseItems = items.Where(x => x.Whouse == docWhouse).ToList();
                if (docWhouseItems.Any())
                {
                    docs.Add(new AutomationDoc
                    {
                        isMantis = true,
                        Data = docWhouseItems
                    });
                }

                var otherItems = items.Where(x => x.Whouse != docWhouse).ToList();
                if (otherItems.Any())
                {
                    var grouped = otherItems.GroupBy(x => x.Whouse).ToList();
                    foreach (var group in grouped)
                    {
                        docs.Add(new AutomationDoc
                        {
                            isMantis = false,
                            Data = group.ToList()
                        });
                    }
                }

                return docs;
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal List<EdiConfiguration> GetEdiConfiguration(int supplierId)
        {
            var ediList = new List<EdiConfiguration>();
            var query = $@"SELECT  
                              C.TECCOMID, T.CODE AS SUPCODE, L.DELIVERYNUMBER, L.SHIPTO
                            , CASE L.SHIPTO WHEN 2 THEN 1001 WHEN 1 THEN 1002 END AS BRANCH
                            , L.ORGID, L.CUSCODE, U.USERID
                            , ISNULL(U.REQUESTUAC,0) AS INQUIRYUAC, ISNULL(U.PRICEUAC,0) AS PRICEUAC
                            , ISNULL(U.STOCKUAC,0) AS STOCKUAC, ISNULL(U.ORDERUAC,0) AS ORDERUAC
                        FROM 
	                        CCCEDI C
                            INNER JOIN TRDR T ON T.TRDR = C.TRDR
	                        LEFT JOIN CCCEDILOCATIONS L ON L.CCCEDI = C.CCCEDI
	                        LEFT JOIN CCCEDIUAC U ON U.CCCEDI = C.CCCEDI
                        WHERE C.TRDR = {supplierId}";

            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    for (int i = 0; i < ds.Count; i++)
                    {
                        ediList.Add(new EdiConfiguration
                        {
                            TecCommId = ds.GetAsString(i, "TECCOMID"),
                            DeliveryNumber = ds.GetAsString(i, "DELIVERYNUMBER"),
                            SupplierCode = ds.GetAsString(i, "SUPCODE"),
                            ShipTo = ds.GetAsInteger(i, "SHIPTO"),
                            Branch = ds.GetAsInteger(i, "BRANCH"),
                            OrganizationId = ds.GetAsInteger(i, "ORGID"),
                            CustomerCode = ds.GetAsString(i, "CUSCODE"),
                            UserId = ds.GetAsInteger(i, "USERID"),
                            InquiryUac = ds.GetAsInteger(i, "INQUIRYUAC"),
                            StockOrderUac = ds.GetAsInteger(i, "STOCKUAC"),
                            ExpressOrderUac = ds.GetAsInteger(i, "ORDERUAC"),
                            PriceUac = ds.GetAsInteger(i, "PRICEUAC"),
                        });
                    }
                }
            }
            return ediList;
        }

        internal void PostDocs(List<AutomationDoc> docs, int docWhouse)
        {
            foreach (var doc in docs)
            {
                try
                {
                    var thisBranchSeries = _tools.GetIdFromMemoryTable("SERIES", "SOSOURCE;FPRMS;WHOUSE", "SERIES", 1351, 10146, docWhouse);//Παραγγελία Πελάτη με κάλυψη απο ενδοδιακίνηση
                    var whouseSec = doc.Data.First().Whouse;
                    var branchSec = _tools.GetIdFromMemoryTable("SERIES", "SOSOURCE;FPRMS;WHOUSE", "BRANCH", 1351, 10144, whouseSec);//Παραγγελία ενδοδιακίνησης προς άλλο υποκατάστημα

                    if (doc.isMantis)
                    {
                        thisBranchSeries = _tools.GetIdFromMemoryTable("SERIES", "SOSOURCE;FPRMS;WHOUSE", "SERIES", 1351, 10143, docWhouse); //Παραγγελία προς Mantis 
                    }

                    var linenum = 9990001;

                    var data = new
                    {
                        SALDOC = new
                        {
                            TRNDATE = Convert.ToDateTime(_xModule.GetTable("SALDOC").Current["TRNDATE"]).ToString("yyyy-MM-dd"),
                            SERIES = thisBranchSeries,
                            TRDR = Convert.ToInt32(_xModule.GetTable("SALDOC").Current["TRDR"]),
                            TRDBRANCH = _xModule.GetTable("SALDOC").Current["TRDBRANCH"] != DBNull.Value
                                                    ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["TRDBRANCH"])
                                                    : (int?)null,
                            PRIORITY = _xModule.GetTable("SALDOC").Current["PRIORITY"] != DBNull.Value
                                                    ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["PRIORITY"])
                                                    : (int?)null,
                            PAYMENT = _xModule.GetTable("SALDOC").Current["PAYMENT"] != DBNull.Value
                                                    ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["PAYMENT"])
                                                    : (int?)null,
                            SHIPMENT = _xModule.GetTable("SALDOC").Current["SHIPMENT"] != DBNull.Value
                                                    ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["SHIPMENT"])
                                                    : (int?)null,
                            SALESMAN = _xModule.GetTable("SALDOC").Current["SALESMAN"] != DBNull.Value
                                                    ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["SALESMAN"])
                                                    : (int?)null,
                            SHIPKIND = _xModule.GetTable("SALDOC").Current["SHIPKIND"] != DBNull.Value
                                                    ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["SHIPKIND"])
                                                    : (int?)null,
                            COMMENTS = _xModule.GetTable("SALDOC").Current["COMMENTS"],
                            REMARKS = _xModule.GetTable("SALDOC").Current["REMARKS"],
                            CCCINFOORDER = _xModule.GetTable("SALDOC").Current["CCCINFOORDER"] != DBNull.Value
                                                    ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["CCCINFOORDER"])
                                                    : (int?)null,
                            CCCSHIPPINGADDR = _xModule.GetTable("SALDOC").Current["CCCSHIPPINGADDR"] != DBNull.Value
                                                    ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["CCCINFOORDER"])
                                                    : (int?)null,
                            CONVMODE = 1
                        },
                        MTRDOC = new
                        {
                            SOCARRIER = _xModule.GetTable("MTRDOC").Current["SOCARRIER"] != DBNull.Value
                                                ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["SOCARRIER"])
                                                : (int?)null,
                            TRUCKS = _xModule.GetTable("MTRDOC").Current["TRUCKS"] != DBNull.Value
                                                ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["TRUCKS"])
                                                : (int?)null,
                            TRUCKSNO = _xModule.GetTable("MTRDOC").Current["TRUCKSNO"].ToString(),
                            ROUTING = _xModule.GetTable("MTRDOC").Current["ROUTING"] != DBNull.Value
                                                ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["ROUTING"])
                                                : (int?)null,
                            BRANCHSEC = !doc.isMantis ? branchSec : (int?)null,
                            SHIPPINGADDR = _xModule.GetTable("MTRDOC").Current["SHIPPINGADDR"].ToString(),
                            SHPZIP = _xModule.GetTable("MTRDOC").Current["SHPZIP"].ToString(),
                            SHPDISTRICT = _xModule.GetTable("MTRDOC").Current["SHPDISTRICT"].ToString(),
                            SHPCITY = _xModule.GetTable("MTRDOC").Current["SHPCITY"].ToString(),
                            SHIPDATE = _xModule.GetTable("MTRDOC").Current["SHIPDATE"] != DBNull.Value
                                                ? Convert.ToDateTime(_xModule.GetTable("MTRDOC").Current["SHIPDATE"])
                                                : (DateTime?)null,
                            DELIVDATE = _xModule.GetTable("MTRDOC").Current["DELIVDATE"] != DBNull.Value
                                                ? Convert.ToDateTime(_xModule.GetTable("MTRDOC").Current["DELIVDATE"])
                                                : (DateTime?)null,
                            SHIPTRDR = _xModule.GetTable("MTRDOC").Current["SHIPTRDR"] != DBNull.Value
                                                ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["SHIPTRDR"])
                                                : (int?)null,
                            BILLTRDR = _xModule.GetTable("MTRDOC").Current["BILLTRDR"] != DBNull.Value
                                                ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["BILLTRDR"])
                                                : (int?)null,
                            ORDERTRDR = _xModule.GetTable("MTRDOC").Current["ORDERTRDR"] != DBNull.Value
                                                ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["ORDERTRDR"])
                                                : (int?)null,
                            SHIPTRDBRANCH = _xModule.GetTable("MTRDOC").Current["SHIPTRDBRANCH"] != DBNull.Value
                                                ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["SHIPTRDBRANCH"])
                                                : (int?)null,
                            BILLTRDBRANCH = _xModule.GetTable("MTRDOC").Current["BILLTRDBRANCH"] != DBNull.Value
                                                ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["BILLTRDBRANCH"])
                                                : (int?)null,
                            ORDERTRDBRANCH = _xModule.GetTable("MTRDOC").Current["ORDERTRDBRANCH"] != DBNull.Value
                                                ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["ORDERTRDBRANCH"])
                                                : (int?)null,
                        },
                        ITELINES = doc.Data.Select(x => new
                        {
                            MTRL = x.Mtrl,
                            QTY1 = x.Qty1,
                            PRICE = x.Price,
                            DISC1PRC = x.Disc1Prc > 0 ? x.Disc1Prc : null,
                            DISC2PRC = x.Disc2Prc > 0 ? x.Disc2Prc : null,
                            DISC3PRC = x.Disc3Prc > 0 ? x.Disc3Prc : null,
                            COMMENTS = !string.IsNullOrEmpty(x.Comments) ? x.Comments : null,
                            FINDOCS = x.Findocs,
                            MTRLINESS = x.Mtrliness,
                            LINENUM = linenum++,
                            CCCRELDOCS = !string.IsNullOrEmpty(x.Reldocs)
                            ? $@"{x.Reldocs}|{x.Findocs};{x.Mtrliness}"
                            : $@"{x.Findocs};{ x.Mtrliness}"
                        }).ToList()
                    };

                    dynamic paramss = new ExpandoObject();
                    paramss.ISCUSTOM = 1;
                    SetDataResponse res = null;

                    res =  _tools.PostDocByWebMethod(_xModule.ObjName, "", data, paramss);

                    if (res!= null && res.Success)
                    {
                        if (!doc.isMantis) //Καταχώριση παραστατικού παραγγελίας ενδοδιακίνησης προς άλλο υποκατάστημα
                        {
                            var receiver = docWhouse;
                            var sender = whouseSec;
                            var nextStation = GetNextStationByStockFlow(receiver, sender);
                            if (nextStation == sender)
                            {
                                nextStation = receiver;
                            }
                            var orderTrdr = _orderBranches.Where(x => x.BranchId == nextStation).FirstOrDefault()?.Trdr;
                            var orderBranch = _orderBranches.Where(x => x.BranchId == nextStation).FirstOrDefault()?.Trdbranch;
                            if (doc.Data.First().DirectShipment == true)
                            {
                                orderBranch = _orderBranches.Where(x => x.BranchId == receiver).FirstOrDefault()?.Trdbranch;
                            }
                            var seriesForOtherBranch = _tools.GetIdFromMemoryTable("SERIES", "SOSOURCE;FPRMS;WHOUSE", "SERIES", 1351, 10144, whouseSec);
                            var id = Convert.ToInt32(res.Id);
                            RemoveReservedQuantities(id); //Προσωρινή άρση δεσμεύσεων
                            using (var Saldoc = _xSupport.CreateModule("SALDOC"))
                            {
                                Saldoc.LocateData(id);
                                PostEndoDocToOtherBranch(Saldoc, orderTrdr, orderBranch, seriesForOtherBranch);
                            }
                        }
                    }
                    else
                    {
                        var errorMsg = res.Error;
                        AppendToLogTable(errorMsg);
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    var stackTrace = ex.StackTrace;
                    AppendToLogTable(errorMsg, stackTrace);
                }
            }
        }

        internal bool CheckIfIsValidDirectShipment(int receiver, int sender)
        {
            if (receiver == sender)
            {
                return false;
            }
            var query = $@"SELECT 
	                            D.BRANCH AS SENDER, H.BRANCH AS RECEIVER, ISNULL(D.DIRECT,0) AS DIRECT 
                            FROM 
	                            CCCSTOCKFLOW H 
	                            INNER JOIN CCCSTOCKFLOWD D ON D.CCCSTOCKFLOW = H.CCCSTOCKFLOW 
                            WHERE 
	                            D.BRANCH = {sender} AND H.BRANCH = {receiver}";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    return ds.GetAsInteger(0, "DIRECT") == 1;
                }
                else
                {
                    return false;
                }
            }
        }

        private void RemoveReservedQuantities(int findoc)
        {
            try
            {
                var q = $"UPDATE MTRLINES SET PENDING = 0, QTY1CANC = QTY1 WHERE FINDOC = {findoc}";
                _xSupport.ExecuteSQL(q, null);
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                var stackTrace = ex.StackTrace;
                AppendToLogTable(errorMsg, stackTrace);
            }
        }

        private void PostEndoDocToOtherBranch(XModule saldoc, int? orderTrdr, int? orderBranch, int seriesForOtherBranch)
        {
            try
            {
                var initFindoc = Convert.ToInt32(saldoc.GetTable("SALDOC").Current["FINDOC"]);
                var lines = new List<FindocItem>();
                var itelinesTmp = saldoc.GetTable("ITELINES").CreateDataTable(true).AsEnumerable().ToList();
                var linenum = 9990001;
                foreach (var row in itelinesTmp)
                {
                    lines.Add(new FindocItem
                    {
                        Mtrl = Convert.ToInt32(row["MTRL"].ToString()),
                        Qty1 = Convert.ToDouble(row["QTY1"].ToString()),
                        Price = Convert.ToDouble(row["PRICE"].ToString()),
                        Disc1Prc = row["DISC1PRC"] != DBNull.Value ? Convert.ToDouble(row["DISC1PRC"].ToString()) : (double?)null,
                        Disc2Prc = row["DISC2PRC"] != DBNull.Value ? Convert.ToDouble(row["DISC2PRC"].ToString()) : (double?)null,
                        Disc3Prc = row["DISC3PRC"] != DBNull.Value ? Convert.ToDouble(row["DISC3PRC"].ToString()) : (double?)null,
                        Comments = row["COMMENTS"].ToString(),
                        Findocs = Convert.ToInt32(row["FINDOC"].ToString()),
                        Mtrliness = Convert.ToInt32(row["MTRLINES"].ToString()),
                        Reldocs = row["CCCRELDOCS"].ToString(),
                        InitFindoc = initFindoc,
                        Linenum = linenum
                    });
                    linenum++;
                }

                var endoData = new
                {
                    SALDOC = new
                    {
                        TRNDATE = Convert.ToDateTime(saldoc.GetTable("SALDOC").Current["TRNDATE"]).ToString("yyyy-MM-dd"),
                        SERIES = seriesForOtherBranch,
                        TRDR = orderTrdr,
                        TRDBRANCH = orderBranch,
                        COMMENTS = saldoc.GetTable("SALDOC").Current["COMMENTS"]
                        //CONVMODE = 1
                    },
                    MTRDOC = new
                    {
                        BILLTRDR = Convert.ToInt32(saldoc.GetTable("SALDOC").Current["TRDR"]),
                        BILLTRDBRANCH = saldoc.GetTable("SALDOC").Current["TRDBRANCH"] != DBNull.Value
                            ? Convert.ToInt32(saldoc.GetTable("SALDOC").Current["TRDBRANCH"])
                            : (int?)null,
                        BRANCHSEC = Convert.ToInt32(saldoc.GetTable("SALDOC").Current["BRANCH"]),
                        CCCINITPARE = 1
                    },
                    ITELINES = lines.Select(x => new
                    {
                        MTRL = x.Mtrl,
                        QTY1 = x.Qty1,
                        PRICE = x.Price,
                        DISC1PRC = x.Disc1Prc > 0 ? x.Disc1Prc : null,
                        DISC2PRC = x.Disc2Prc > 0 ? x.Disc2Prc : null,
                        DISC3PRC = x.Disc3Prc > 0 ? x.Disc3Prc : null,
                        COMMENTS = !string.IsNullOrEmpty(x.Comments) ? x.Comments : null,
                        FINDOCS = x.Findocs,
                        MTRLINESS = x.Mtrliness,
                        LINENUM = x.Linenum,
                        CCCRELDOCS = !string.IsNullOrEmpty(x.Reldocs)
                        ? $@"{x.Reldocs}|{x.Findocs};{x.Mtrliness}"
                        : $@"{x.Findocs};{ x.Mtrliness}",
                        CCCINITFINDOC = x.InitFindoc != null ? x.InitFindoc : null
                    }).ToList()
                };

                dynamic paramss = new ExpandoObject();
                paramss.ISCUSTOM = 1;
                if (!_xSupport.ConnectionInfo.IsAdministrator)
                {
                    using (var autoSupport = LoginWithAutomationUser(_xSupport))
                    {
                        var autoTools = new S1Tools(autoSupport);
                        SetDataResponse result = null;
                        result = autoTools.PostDocByWebMethod(saldoc.ObjName, "", endoData, paramss);
                        if (result != null && !result.Success)
                        {
                            var errorMsg = result.Error;
                            AppendToLogTable(errorMsg);
                        }
                        else
                        {
                            var newId = result.Id;
                            var query = $"UPDATE FINDOC SET INSUSER = {_xSupport.ConnectionInfo.UserId}, UPDUSER = {_xSupport.ConnectionInfo.UserId} WHERE FINDOC = {newId}";
                            _xSupport.ExecuteSQL(query, null);
                        }
                    }
                    ResetLastLogin();
                }
                else
                {
                    SetDataResponse result = null;
                    _tools.PostDocByWebMethod(saldoc.ObjName, "", endoData, paramss);
                    if (result != null && !result.Success)
                    {
                        var errorMsg = result.Error;
                        AppendToLogTable(errorMsg);
                    }
                }
            }
            catch (Exception ex)
            {
                var test = saldoc.GetTable("SALDOC").Current["TRNDATE"];
                var errorMsg = ex.Message;
                var stackTrace = ex.StackTrace;
                AppendToLogTable(errorMsg, stackTrace);
            }
        }

        internal void ResetLastLogin()
        {
            try
            {
                var softoneDir = _xSupport.Dir("EXE");
                var soft1Data = _xSupport.Dir("Soft1Data");
                var softoneDataDir = $@"{soft1Data}Data\";
                var xcoName = _XCOName;
                if (string.IsNullOrEmpty(xcoName))
                {
                    return;
                }
                var softoneDirXCOs = Directory.GetFiles(softoneDir, "*.XCO").Select(f => Path.GetFileName(f)).ToList();
                var softoneDataDirXCOs = Directory.GetFiles(softoneDataDir, "*.XCO").Select(f => Path.GetFileName(f)).ToList();
                if (softoneDirXCOs.Contains(xcoName) || softoneDataDirXCOs.Contains(xcoName))
                {
                    var regXCO = xcoName.Split('.').First();
                    var registryKey = Registry.CurrentUser.OpenSubKey($"Software\\SoftOne\\{regXCO}\\", RegistryKeyPermissionCheck.ReadWriteSubTree);
                    if (registryKey == null)
                    {
                        return;
                    }
                    var lastData = GetCompanyAndBranchNames(_xSupport.ConnectionInfo.CompanyId, _xSupport.ConnectionInfo.BranchId);
                    var companyName = lastData.Item1;
                    var branchName = lastData.Item2;
                    if (!string.IsNullOrEmpty(branchName))
                    {
                        registryKey.SetValue("LastBranch", branchName);
                    }
                    registryKey.SetValue("LastBranchID", _xSupport.ConnectionInfo.BranchId.ToString());
                    if (!string.IsNullOrEmpty(companyName))
                    {
                        registryKey.SetValue("LastCompany", companyName);
                    }
                    registryKey.SetValue("LastCompanyID", _xSupport.ConnectionInfo.CompanyId.ToString());
                    registryKey.SetValue("LastLogin", _xSupport.ConnectionInfo.UserName);
                }
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                var stackTrace = ex.StackTrace;
                AppendToLogTable(errorMsg, stackTrace);
            }
        }

        private Tuple<string, string> GetCompanyAndBranchNames(int companyId, int branchId)
        {
            var query = $"SELECT C.NAME AS COMPANYNAME, B.NAME AS BRANCHNAME FROM BRANCH B INNER JOIN COMPANY C ON C.COMPANY = B.COMPANY WHERE B.BRANCH = {branchId} AND B.COMPANY = {companyId}";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    return new Tuple<string, string>(ds.GetAsString(0, "COMPANYNAME"), ds.GetAsString(0, "BRANCHNAME"));
                }
                else
                {
                    return new Tuple<string, string>(string.Empty, string.Empty); ;
                }
            }
        }

        internal int CreateExpectedReceipt(int deliveryNoteId)
        {
            try
            {
                var series = Convert.ToInt32(_xModule.GetTable("SALDOC").Current["SERIES"]);
                var supBranch = _tools.GetIdFromMemoryTable("SERIES", "SOSOURCE;SERIES", "BRANCH", 1351, series);

                var supplierId = _receiptBranches.Where(x => x.BranchId == supBranch).FirstOrDefault()?.Trdr;
                if (supplierId == null)
                {
                    throw new Exception($"Δεν υπάρχει αντιστοίχιση με προμηθευτή για το κατάστημα {supBranch}");
                }
                var supplierBranch = _receiptBranches.Where(x => x.BranchId == supBranch).FirstOrDefault()?.Trdbranch;
                if (supplierBranch == null)
                {
                    throw new Exception($"Δεν υπάρχει αντιστοίχιση με προμηθευτή για το κατάστημα {supBranch}");
                }
                var customerId = Convert.ToInt32(_xModule.GetTable("SALDOC").Current["TRDR"]);
                var customerBranchId = _xModule.GetTable("SALDOC").Current["TRDBRANCH"] != DBNull.Value ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["TRDBRANCH"]) : 0;
                var cusBranch = _orderBranches.Where(x => x.Trdr == customerId && x.Trdbranch == customerBranchId).First()?.BranchId;
                var purdocSeries = _tools.GetIdFromMemoryTable("SERIES", "SOSOURCE;FPRMS;BRANCH", "SERIES", 1251, 20108, cusBranch);
                if (purdocSeries == -1)
                {
                    throw new Exception($"Δε βρέθηκε σειρά παραλαβής για το κατάστημα {cusBranch}.");
                }

                var lines = new List<FindocItem>();
                var itelinesTmp = _xModule.GetTable("ITELINES").CreateDataTable(true).AsEnumerable().ToList();
                var linenum = 9990001;
                foreach (var row in itelinesTmp)
                {
                    lines.Add(new FindocItem
                    {
                        Mtrl = Convert.ToInt32(row["MTRL"].ToString()),
                        Qty1 = Convert.ToDouble(row["QTY1"].ToString()),
                        Price = Convert.ToDouble(row["PRICE"].ToString()),
                        Disc1Prc = row["DISC1PRC"] != DBNull.Value ? Convert.ToDouble(row["DISC1PRC"].ToString()) : (double?)null,
                        Disc2Prc = row["DISC2PRC"] != DBNull.Value ? Convert.ToDouble(row["DISC2PRC"].ToString()) : (double?)null,
                        Disc3Prc = row["DISC3PRC"] != DBNull.Value ? Convert.ToDouble(row["DISC3PRC"].ToString()) : (double?)null,
                        Comments = row["COMMENTS"].ToString(),
                        Findocs = deliveryNoteId,
                        Mtrliness = Convert.ToInt32(row["MTRLINES"].ToString()),
                        Linenum = linenum,
                        Reldocs = row["CCCRELDOCS"].ToString(),
                        InitFindoc = row["CCCINITFINDOC"] != DBNull.Value
                        ? Convert.ToInt32(row["CCCINITFINDOC"].ToString())
                        : (int?)null
                    });

                    linenum++;
                }

                var data = new
                {
                    PURDOC = new
                    {
                        TRNDATE = Convert.ToDateTime(_xModule.GetTable("SALDOC").Current["TRNDATE"]).ToString("yyyy-MM-dd"),
                        SERIES = purdocSeries,
                        TRDR = supplierId,
                        TRDBRANCH = supplierBranch,
                        COMMENTS = _xModule.GetTable("SALDOC").Current["COMMENTS"],
                        CONVMODE = 1
                    },
                    MTRDOC = new
                    {
                        CCCRELCUSTOMER = _xModule.GetTable("MTRDOC").Current["BILLTRDBRANCH"] != DBNull.Value
                        ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["BILLTRDR"])
                        : (int?)null,
                        CCCRELCUSBRANCH = _xModule.GetTable("MTRDOC").Current["BILLTRDBRANCH"] != DBNull.Value
                        ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["BILLTRDBRANCH"])
                        : (int?)null,
                        BRANCHSEC = _xModule.GetTable("MTRDOC").Current["BRANCHSEC"]  != DBNull.Value
                        ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["BRANCHSEC"])
                        : (int?)null
                    },
                    ITELINES = lines.Select(x => new
                    {
                        MTRL = x.Mtrl,
                        QTY1 = x.Qty1,
                        PRICE = x.Price,
                        DISC1PRC = x.Disc1Prc > 0 ? x.Disc1Prc : null,
                        DISC2PRC = x.Disc2Prc > 0 ? x.Disc2Prc : null,
                        DISC3PRC = x.Disc3Prc > 0 ? x.Disc3Prc : null,
                        COMMENTS = !string.IsNullOrEmpty(x.Comments) ? x.Comments : null,
                        FINDOCS = x.Findocs,
                        MTRLINESS = x.Mtrliness,
                        LINENUM = x.Linenum,
                        CCCRELDOCS = !string.IsNullOrEmpty(x.Reldocs)
                        ? $@"{x.Reldocs}|{x.Findocs};{x.Mtrliness}"
                        : $@"{x.Findocs};{ x.Mtrliness}",
                        CCCINITFINDOC = x.InitFindoc != null ? x.InitFindoc : null
                    }).ToList()
                };

                var id = 0;
                dynamic paramss = new ExpandoObject();
                paramss.ISCUSTOM = 1;
                if (!_xSupport.ConnectionInfo.IsAdministrator)
                {
                    using (var autoSupport = LoginWithAutomationUser(_xSupport))
                    {
                        var autoTools = new S1Tools(autoSupport);
                        SetDataResponse result = autoTools.PostDocByWebMethod("PURDOC", "", data, paramss);
                        if (!result.Success)
                        {
                            var errorMsg = result.Error;
                            AppendToLogTable(errorMsg);
                        }
                        id = Convert.ToInt32(result.Id);
                        var query = $"UPDATE FINDOC SET INSUSER = {_xSupport.ConnectionInfo.UserId}, UPDUSER = {_xSupport.ConnectionInfo.UserId} WHERE FINDOC = {id}";
                        _xSupport.ExecuteSQL(query, null);
                    }
                    ResetLastLogin();
                }
                else
                {
                    SetDataResponse result = _tools.PostDocByWebMethod("PURDOC", "", data, paramss);
                    if (!result.Success)
                    {
                        var errorMsg = result.Error;
                        AppendToLogTable(errorMsg);
                    }
                    id = Convert.ToInt32(result.Id);
                }

                var receiptId = id;
                return receiptId;
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal int CreateEndoOrder(int receiptId, int trdr, int trdbranch)
        {
            try
            {
                var startBranch = Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["WHOUSE"]);
                var lastBranch = Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["BRANCHSEC"]);
                var nextBranch = GetNextStationByStockFlow(startBranch, lastBranch);
                if (startBranch == lastBranch)
                {
                    return 0;
                }
                var series = _tools.GetIdFromMemoryTable("SERIES", "SOSOURCE;FPRMS;WHOUSE", "SERIES", 1351, 10144, startBranch);//Παραγγελία ενδοδιακίνησης προς άλλο υποκατάστημα
                var customerId = _orderBranches.Where(x => x.BranchId == nextBranch).First().Trdr;
                var branchId = _orderBranches.Where(x => x.BranchId == nextBranch).First().Trdbranch;
                var lines = new List<FindocItem>();
                var itelinesTmp = _xModule.GetTable("ITELINES").CreateDataTable(true).AsEnumerable().ToList();
                var linenum = 9990001;
                foreach (var row in itelinesTmp)
                {
                    lines.Add(new FindocItem
                    {
                        Mtrl = Convert.ToInt32(row["MTRL"].ToString()),
                        Qty1 = Convert.ToDouble(row["QTY1"].ToString()),
                        Price = Convert.ToDouble(row["PRICE"].ToString()),
                        Disc1Prc = row["DISC1PRC"] != DBNull.Value ? Convert.ToDouble(row["DISC1PRC"].ToString()) : (double?)null,
                        Disc2Prc = row["DISC2PRC"] != DBNull.Value ? Convert.ToDouble(row["DISC2PRC"].ToString()) : (double?)null,
                        Disc3Prc = row["DISC3PRC"] != DBNull.Value ? Convert.ToDouble(row["DISC3PRC"].ToString()) : (double?)null,
                        Comments = row["COMMENTS"].ToString(),
                        Findocs = receiptId,
                        Mtrliness = Convert.ToInt32(row["MTRLINES"].ToString()),
                        Linenum = linenum,
                        Reldocs = row["CCCRELDOCS"].ToString(),
                        InitFindoc = row["CCCINITFINDOC"] != DBNull.Value
                        ? Convert.ToInt32(row["CCCINITFINDOC"].ToString())
                        : (int?)null
                    });

                    linenum++;
                }

                var data = new
                {
                    SALDOC = new
                    {
                        TRNDATE = Convert.ToDateTime(_xModule.GetTable("PURDOC").Current["TRNDATE"]).ToString("yyyy-MM-dd"),
                        SERIES = series,
                        TRDR = customerId,
                        TRDBRANCH = branchId,
                        COMMENTS = _xModule.GetTable("PURDOC").Current["COMMENTS"],
                        //CONVMODE = 1
                    },
                    MTRDOC = new
                    {
                        BILLTRDR = _xModule.GetTable("MTRDOC").Current["CCCRELCUSTOMER"] != DBNull.Value
                        ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["CCCRELCUSTOMER"])
                        : (int?)null,
                        BILLTRDBRANCH = _xModule.GetTable("MTRDOC").Current["CCCRELCUSBRANCH"] != DBNull.Value
                        ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["CCCRELCUSBRANCH"])
                        : (int?)null,
                        BRANCHSEC = _xModule.GetTable("MTRDOC").Current["BRANCHSEC"] != DBNull.Value
                        ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["BRANCHSEC"])
                        : (int?)null,
                        CCCINITPARE = 0
                    },
                    ITELINES = lines.Select(x => new
                    {
                        MTRL = x.Mtrl,
                        QTY1 = x.Qty1,
                        PRICE = x.Price,
                        DISC1PRC = x.Disc1Prc > 0 ? x.Disc1Prc : null,
                        DISC2PRC = x.Disc2Prc > 0 ? x.Disc2Prc : null,
                        DISC3PRC = x.Disc3Prc > 0 ? x.Disc3Prc : null,
                        COMMENTS = !string.IsNullOrEmpty(x.Comments) ? x.Comments : null,
                        FINDOCS = x.Findocs,
                        MTRLINESS = x.Mtrliness,
                        LINENUM = x.Linenum,
                        CCCRELDOCS = !string.IsNullOrEmpty(x.Reldocs)
                        ? $@"{x.Reldocs}|{x.Findocs};{x.Mtrliness}"
                        : $@"{x.Findocs};{ x.Mtrliness}",
                        CCCINITFINDOC = x.InitFindoc != null ? x.InitFindoc : null
                    }).ToList()
                };

                dynamic paramss = new ExpandoObject();
                paramss.ISCUSTOM = 1;
                SetDataResponse result = _tools.PostDocByWebMethod("SALDOC", "", data, paramss);
                if (!result.Success)
                {
                    var errorMsg = result.Error;
                    AppendToLogTable(errorMsg);
                }
                var id = Convert.ToInt32(result.Id);
                return id;

            }
            catch (Exception)
            {
                throw;
            }
        }

        internal int GetNextStationByStockFlow(int receiver, int sender)
        {
            var query = $@"SELECT TOP 1 ISNULL(M.BRANCH,D.BRANCH) AS BRANCH 
                            FROM CCCSTOCKFLOW H
                            INNER JOIN CCCSTOCKFLOWD D ON D.CCCSTOCKFLOW = H.CCCSTOCKFLOW
                            LEFT JOIN CCCSTOCKFLOWM M ON M.CCCSTOCKFLOW = D.CCCSTOCKFLOW AND M.CCCSTOCKFLOWD = D.CCCSTOCKFLOWD
                            WHERE D.BRANCH = {sender} AND H.BRANCH = {receiver}
                            ORDER BY M.SORTORDER ASC";

            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    return ds.GetAsInteger(0, "BRANCH");
                }
                else
                {
                    return 0;
                }
            }
        }

        internal bool CheckIfIsEndoDoc(int trdr, int trdbranch)
        {
            return _receiptBranches.Any(x => x.Trdr == trdr && x.Trdbranch == trdbranch);
        }

        internal string RemoveSpecialChars(string str)
        {
            char[] arr = str.ToCharArray();
            arr = Array.FindAll<char>(arr, (c => (char.IsLetterOrDigit(c))));
            str = new string(arr);
            return str;
        }
    }
}
