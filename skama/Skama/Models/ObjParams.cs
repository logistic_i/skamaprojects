﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class ObjParams
    {
        public string Warnings { get; set; }
        public int NoMessages { get; set; }
        public string MyCustomParam { get; set; }
    }
}
