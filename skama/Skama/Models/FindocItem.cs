﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class FindocItem
    {
        public int Whouse { get; set; }
        public int Mtrliness { get; set; }
        public int Findocs { get; set; }
        public int Linenum { get; set; }
        public int Mtrl { get; set; }
        public double Qty1 { get; set; }
        public double Price { get; set; }
        public double? Disc1Prc { get; set; }
        public double? Disc2Prc { get; internal set; }
        public double? Disc3Prc { get; internal set; }
        public string Reldocs { get; set; }
        public int? InitFindoc { get; set; }
        public string Comments { get; internal set; }
        public bool DirectShipment { get; set; }
    }
}
