﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class RelProductResponse : B2BProductResponseBase
    {
        [JsonProperty("result")]
        public List<RelativeProduct> RelProducts { get; set; }
    }
}
