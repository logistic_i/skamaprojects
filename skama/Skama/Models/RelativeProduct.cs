﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class RelativeProduct
    {       
        [JsonProperty("item_code")]
        public string Code { get; set; }

        [JsonProperty("relation_code")]
        public string RelationCode { get; set; }

        [JsonProperty("relation_descr")]
        public string RelationDescr { get; set; }

        [JsonProperty("relation_qty")]
        public double RelationQty { get; set; }

        [JsonProperty("has_relatives")]
        public bool HasRelatives { get; set; }
    }
}
