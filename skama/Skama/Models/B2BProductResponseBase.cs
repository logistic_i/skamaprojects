﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class B2BProductResponseBase
    {
        [JsonProperty("status_code")]
        public string StatusCode { get; set; }

        [JsonProperty("status_text")]
        public string StatusText { get; set; }
    }
}
