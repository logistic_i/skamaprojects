﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class ExtendedProduct
    { 
        [JsonProperty("item_code")]
        public string Code { get; set; }

        [JsonProperty("match_keyword")]
        public string MatchKeyword { get; set; }

        [JsonProperty("match_description")]
        public string MatchDescription { get; set; }

        [JsonProperty("has_relatives")]
        public bool HasRelatives { get; set; }
    }
}
