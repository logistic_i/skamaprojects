﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class ProductResp
    {
        [JsonProperty(PropertyName = "MTRL")]
        public string ProductId { get; set; }

        [JsonProperty(PropertyName = "LINEVAL")]
        public string FinalPrice { get; set; }
    }
}
