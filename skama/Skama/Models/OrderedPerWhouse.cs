﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class OrderedPerWhouse
    {
        public int RestCategory { get; set; }
        public int Warehouse { get; set; }
        public double Ordered { get; set; }
    }
}
