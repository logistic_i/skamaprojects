﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class SetDataRequest
    {
        public string Service { get; set; } = "setData";
        public string Object { get; set; }
        public object ObjectParams { get; set; }
        public object Data { get; set; }
        public string Form { get; set; }
    }
}
