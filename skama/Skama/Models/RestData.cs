﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class RestData
    {
        public int Findoc { get; set; }
        public int SoSource { get; set; }
        public string SeriesName { get; set; }
        public string Fincode { get; set; }
        public int WHId { get; set; }
        public string WHName { get; set; }
        public string Date { get; set; }
        public string DelivDate { get; set; }
        public string ShipDate { get; set; }
        public string TrdrName { get; set; }
        public string BillTrdrName { get; set; }
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public int RestCategory { get; set; }
        public double RestQuantity { get; set; }
    }
}

