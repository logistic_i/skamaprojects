﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class SkamaBranch
    {
        public int Sodtype { get; set; }
        public int Trdr { get; set; }
        public int Trdbranch { get; set; }
        public int Code { get; set; }
        public int Name { get; set; }
        public int BranchId { get; set; }
    }
}
