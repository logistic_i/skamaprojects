﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class CalcResp
    {
        [JsonProperty(PropertyName = "success")]
        public string SuccessStr { get;  set; }

        public bool Success {
            get
            {
                return !string.IsNullOrEmpty(SuccessStr) && SuccessStr.ToLower() == "true"; 
            } 
        }

        [JsonProperty(PropertyName = "data")]
        public CalcRespData Products { get; set; }
    }
}
