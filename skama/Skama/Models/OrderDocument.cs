﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class OrderDocument
    {
        [JsonProperty("SALDOC")]
        public DocHeader Saldoc { get; set; }
        [JsonProperty("MTRDOC")]
        public DocMtrdoc Mtrdoc { get; set; }
        [JsonProperty("ITELINES")]
        public List<DocLine> IteLines { get; set; }
    }
}
