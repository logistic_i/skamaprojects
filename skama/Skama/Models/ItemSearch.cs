﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class ItemSearch
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string ABCRanking { get; set; }
        public double Price { get; set; }
        public double PriceR { get; set; }
        public double CustomerPrice { get; set; }
        public double WHRemain { get; set; }
        public double TotalRemain { get; set; }
        public double WHExpected { get; set; }
        public double WHReserved { get; set; }
        public double Expected { get; set; }
        public double Reserved { get; set; }
        public double OrderQty { get; set; }
        public double InitQty { get; set; }
        public string WHName { get; set; }
        public double ActualRemain { get; set; }
    }
}
