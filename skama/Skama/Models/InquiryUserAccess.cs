﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class EdiConfiguration
    {
        public string TecCommId { get; set; }
        public string SupplierCode { get; set; }
        public string DeliveryNumber { get; set; }
        public int ShipTo { get; set; }
        public int Branch { get;  set; }
        public int OrganizationId { get; set; }
        public string CustomerCode { get; set; }
        public int UserId { get; set; }
        public int InquiryUac { get; set; }
        public int StockOrderUac { get; set; }
        public int ExpressOrderUac { get; set; }
        public int PriceUac { get; set; }
    }
}
