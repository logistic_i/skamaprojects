﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class ExtProductResponse : B2BProductResponseBase
    {
        [JsonProperty("result")]
        public List<ExtendedProduct> Products { get; set; }
    }
}
