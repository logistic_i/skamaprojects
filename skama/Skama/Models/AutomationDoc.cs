﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class AutomationDoc
    {
        public bool isMantis { get; set; }

        public List<FindocItem> Data { get; set; }
    }
}
