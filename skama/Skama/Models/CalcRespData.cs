﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class CalcRespData
    {
        [JsonProperty(PropertyName = "ITELINES")]
        public List<ProductResp> Products { get; set; }
    }
}
