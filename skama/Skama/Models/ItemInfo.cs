﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class ItemInfo
    {
        public int WhouseId { get; set; }
        public string WHCode { get; set; }
        public string WHDescription { get; set; }
        public int WHSort { get; set; }
        public int ProductId { get; set; }
        public double TotalRemain { get; set; }
        public double NoMinStockRemain { get; set; }
        public double ExpectedQty { get; set; }
        public double ReservedQty { get; set; }
        public double ActualRemain { get; set; }
        public double PropQty { get; set; }
        public double OrderQty { get; set; }
        public int Direct { get; set; }
    }
}
