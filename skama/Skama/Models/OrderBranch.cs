﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class OrderBranch
    {
        public int Trdr { get; set; }
        public int BranchId { get; set; }
    }
}
