﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class CalcRequest
    {
        public string Service { get; set; } = "calculate";
        public string Object { get; set; }
        public string LocateInfo { get; set; }
        public object Data { get; set; }
    }
}
