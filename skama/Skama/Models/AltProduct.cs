﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class AltProduct
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Ean { get; set; }
        public string PriceCategory { get; set; }
        public string PriceValue { get; set; }
    }
}
