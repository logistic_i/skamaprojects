﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class DocLine
    {
        [JsonProperty("MTRL")]
        public int Mtrl { get; set; }
        [JsonProperty("QTY1")]
        public double Qty1 { get; set; }
        [JsonProperty("PRICE")]
        public double Price { get; set; }
        [JsonProperty("DISC1PRC")]
        public double Disc1Prc { get; set; }
        [JsonProperty("DISC2PRC")]
        public double Disc2Prc { get; set; }
        [JsonProperty("DISC3PRC")]
        public double Disc3Prc { get; set; }
        [JsonProperty("COMMENTS")]
        public int Comments { get; set; }
        [JsonProperty("FINDOCS")]
        public int Findocs { get; set; }
        [JsonProperty("MTRLINESS")]
        public int Mtrliness { get; set; }
        [JsonProperty("LINENUM")]
        public int Linenum { get; set; }
        [JsonProperty("CCCRELDOCS")]
        public string RelDocs { get; set; }
    }
}
