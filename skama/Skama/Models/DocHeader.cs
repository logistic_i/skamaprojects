﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class DocHeader
    {
        [JsonProperty("TRNDATE")]
        public string Trndate { get; set; }
        [JsonProperty("SERIES")]
        public int Series { get; set; }
        [JsonProperty("TRDR")]
        public int Trdr { get; set; }
        [JsonProperty("TRDBRANCH")]
        public int? TrdBranch { get; set; }
        [JsonProperty("PRIORITY")]
        public int? Priority { get; set; }
        [JsonProperty("PAYMENT")]
        public int? Payment { get; set; }
        [JsonProperty("SHIPMENT")]
        public int? Shipment { get; set; }
        [JsonProperty("SALESMAN")]
        public int? SalesMan { get; set; }
        [JsonProperty("SHIPKIND")]
        public int? ShipKind { get; set; }
        [JsonProperty("COMMENTS")]
        public string Comments { get; set; }
        [JsonProperty("REMARKS")]
        public string Remarks { get; set; }
        [JsonProperty("CCCINFOORDER")]
        public int? InfoOrder { get; set; }
        [JsonProperty("CCCSHIPPINGADDR")]
        public int? CustomShipAddress { get; set; }
        [JsonProperty("CONVMODE")]
        public int Convmode { get; set; } = 1;
    }
}
