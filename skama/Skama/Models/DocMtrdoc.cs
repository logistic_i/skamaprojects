﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skama.Models
{
    public class DocMtrdoc
    {
        [JsonProperty("SOCARRIER")]
        public int? Socarrier { get; set; }
        [JsonProperty("TRUCKS")]
        public int? Trucks { get; set; }
        [JsonProperty("TRUCKSNO")]
        public string TrucksNo { get; set; }
        [JsonProperty("ROUTING")]
        public int? Routing { get; set; }
        [JsonProperty("BRANCHSEC")]
        public int? BranchSec { get; set; }
        [JsonProperty("SHIPPINGADDR")]
        public string ShippingAddress { get; set; }
        [JsonProperty("SHPZIP")]
        public string ShipZip { get; set; }
        [JsonProperty("SHPDISTRICT")]
        public string ShipDistrict { get; set; }
        [JsonProperty("SHPCITY")]
        public string ShipCity { get; set; }
        [JsonProperty("SHIPDATE")]
        public DateTime? ShipDate { get; set; }
        [JsonProperty("DELIVDATE")]
        public DateTime? DelivDate { get; set; }
        [JsonProperty("SHIPTRDR")]
        public int? ShipTrdr { get; set; }
        [JsonProperty("BILLTRDR")]
        public int? BillTrdr { get; set; }
        [JsonProperty("ORDERTRDR")]
        public int? OrderTrdr { get; set; }
        [JsonProperty("SHIPTRDBRANCH")]
        public int? ShipTrdbranch { get; set; }
        [JsonProperty("BILLTRDBRANCH")]
        public int? BillTrdBranch { get; set; }
        [JsonProperty("ORDERTRDBRANCH")]
        public int? OrderTrdBranch { get; set; }
    }
}
