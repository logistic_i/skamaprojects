﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Softone;

namespace Skama
{
    [WorksOn("ITEM")]
    public class Item : TXCode
    {
        public override void BeforePost()
        {
            var helper = new Helper(XSupport, XModule);
            var code = XModule.GetTable("MTRL").Current["CODE"].ToString();
            var name = XModule.GetTable("MTRL").Current["NAME"].ToString();
            var id = Convert.ToInt32(XModule.GetTable("MTRL").Current["MTRL"]);

            if (id < 0)
            {
                if (!Regex.IsMatch(code, "^[a-zA-Z0-9]*$"))
                {
                    throw new Exception("Ο κωδικός του είδους απαγορεύεται να περιέχει κενά διαστήματα, μη αγγλικούς και ειδικούς χαρακτήρες!");
                }
            }

            var searchCode = helper.RemoveSpecialChars(code);
            var searchName = helper.RemoveSpecialChars(name);
            if (!string.IsNullOrEmpty(searchCode))
            {
                XModule.GetTable("MTRL").Current["CCCSEARCHCODE"] = searchCode;
            }
            if (!string.IsNullOrEmpty(searchName))
            {
                XModule.GetTable("MTRL").Current["CCCSEARCHNAME"] = searchName;
            }
            base.BeforePost();
        }
    }
}
