﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skama.Models;
using Softone;
using TecOrder;
using TecOrder.Dtos;

namespace Skama
{
    [WorksOn("PURDOC")]
    public class Purdoc : TXCode
    {
        private Helper _helper = null;
        private int _userId;
        private int _company;
        private List<EdiConfiguration> _ediConfiguration { get; set; } = new List<EdiConfiguration>();
        private string _ediUsername { get; set; } = "7182000040330"; // SKAMA username
        private string _ediPassword { get; set; } = "Rc62YxTb"; // SKAMA PASSWORD

        public override void Initialize()
        {
            _helper = new Helper(XSupport, XModule);
            _userId = XSupport.ConnectionInfo.UserId;
            _company = XSupport.ConnectionInfo.CompanyId;
            base.Initialize();
        }

        public override object ExecCommand(int Cmd)
        {
            if (Cmd == 20201202)
            {
                var fprmsObj = XModule.GetTable("PURDOC").Current["FPRMS"];
                if (fprmsObj == DBNull.Value)
                {
                    return base.ExecCommand(Cmd);
                }
                var fprms = Convert.ToInt32(XModule.GetTable("PURDOC").Current["FPRMS"]);
                var check = CheckEdiValidation(fprms);
                if (!check)
                {
                    return base.ExecCommand(Cmd);
                }
                if (fprms == 20140)
                {
                    SendStockOrder();
                }
                if (fprms == 20141)
                {
                    SendExpressOrder();
                }

            }
            return base.ExecCommand(Cmd);
        }

        private bool CheckEdiValidation(int fprms)
        {
            try
            {
                if (fprms != 20140 && fprms != 20141)
                {
                    XSupport.Warning("Η εργασία είναι διαθέσιμη μόνο στους τύπους παραστατικών EDI Stock Order και Edi Express Order!");
                    return false;
                }

                var findoc = Convert.ToInt32(XModule.GetTable("PURDOC").Current["FINDOC"]);
                if (findoc < 0)
                {
                    XSupport.Warning("Προσοχή! Το παραστατικό δεν είναι καταχωρημένο!");
                    return false;
                }

                var supplier = Convert.ToInt32(XModule.GetTable("PURDOC").Current["TRDR"]);
                _ediConfiguration = _helper.GetEdiConfiguration(supplier);
                if (!_ediConfiguration.Any())
                {
                    var supplierName = XModule.GetTable("PURDOC").Current["X_TNAME"].ToString();
                    XSupport.Warning($"Δεν έχει οριστεί EDI παραμετροποίηση για τον προμηθευτή {supplierName}!");
                    return false;
                }

                var uac = _ediConfiguration.FirstOrDefault(x => x.UserId == _userId);
                if (uac == null)
                {
                    XSupport.Warning($"Προσοχή! Δεν έχει οριστεί ο χρήστης σας στην EDI παραμετροποίηση!");
                    return false;
                }

                if (fprms == 20140) //Stock Order
                {
                    var stockUac = uac.StockOrderUac;
                    if (stockUac == 0)
                    {
                        XSupport.Warning($"Προσοχή! Δεν έχετε δικαιώμα εκτέλεσης της συγεκριμένης εργασίας!");
                        return false;
                    }
                }

                if (fprms == 20141) // Express Order
                {
                    var expressOrderUac = uac.ExpressOrderUac;
                    if (expressOrderUac == 0)
                    {
                        XSupport.Warning($"Προσοχή! Δεν έχετε δικαιώμα εκτέλεσης της συγεκριμένης εργασίας!");
                        return false;
                    }
                }

                var allreadySent = false;
                var allreadySentObj = XModule.GetTable("MTRDOC").Current["CCCEDISENT"];
                if (allreadySentObj != DBNull.Value)
                {
                    var flag = Convert.ToInt32(allreadySentObj);
                    if (flag == 1)
                    {
                        allreadySent = true;
                    }
                }
                if (allreadySent)
                {
                    XSupport.Warning("Το παραστατικό έχει ήδη αποσταλλεί στον προμηθευτή!");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                XSupport.Warning(ex.Message);
                return false;
            }
        }

        private void SendStockOrder()
        {
            var findoc = Convert.ToInt32(XModule.GetTable("PURDOC").Current["FINDOC"]);
            var trdr = Convert.ToInt32(XModule.GetTable("PURDOC").Current["TRDR"]);

            var tecClient = new TecClient(_ediUsername, _ediPassword);
            var businessRelation = new BusinessRelation();
            businessRelation.BuyerNumber = _ediConfiguration.First().CustomerCode; //Test456 //104004247
            businessRelation.SupplierNumber = _ediConfiguration.First().SupplierCode; //SKAMA //0147

            var branch = Convert.ToInt32(XModule.GetTable("PURDOC").Current["BRANCH"]);
            var buyer = GetBuyerInfo();

            var supplier = new Trader
            {
                Name1 = XModule.GetTable("PURDOC").Current["X_TNAME"].ToString(),
                Street1 = XModule.GetTable("MTRDOC").Current["SHIPPINGADDR"].ToString(),
                PostalCode = XModule.GetTable("MTRDOC").Current["SHPZIP"].ToString(),
                City = XModule.GetTable("MTRDOC").Current["SHPCITY"].ToString(),
                Country = GetCountryBySupplier(trdr)
            };

            Trader deliveryParty = null;
            var branchConfiguration = _ediConfiguration.Where(x => x.Branch == branch).FirstOrDefault();
            if (branchConfiguration != null)
            {
                if(!string.IsNullOrEmpty(branchConfiguration.DeliveryNumber))
                {
                    deliveryParty = GetBranchInfo(branch);
                    deliveryParty.Code = branchConfiguration.DeliveryNumber;
                }
            }

            var products = GetOrderProducts(findoc);

            var stockOrderRes = tecClient.SendStockOrder(businessRelation, buyer, supplier, products, deliveryParty);
        }

        private void SendExpressOrder()
        {
            var findoc = Convert.ToInt32(XModule.GetTable("PURDOC").Current["FINDOC"]);
            var trdr = Convert.ToInt32(XModule.GetTable("PURDOC").Current["TRDR"]);

            var tecClient = new TecClient(_ediUsername, _ediPassword);
            var businessRelation = new BusinessRelation();
            businessRelation.BuyerNumber = _ediConfiguration.First().CustomerCode; //Test456 //104004247
            businessRelation.SupplierNumber = _ediConfiguration.First().SupplierCode; //SKAMA //0147

            var branch = Convert.ToInt32(XModule.GetTable("PURDOC").Current["BRANCH"]);
            var buyer = GetBuyerInfo();

            var supplier = new Trader
            {
                Name1 = XModule.GetTable("PURDOC").Current["X_TNAME"].ToString(),
                Street1 = XModule.GetTable("MTRDOC").Current["SHIPPINGADDR"].ToString(),
                PostalCode = XModule.GetTable("MTRDOC").Current["SHPZIP"].ToString(),
                City = XModule.GetTable("MTRDOC").Current["SHPCITY"].ToString(),
                Country = GetCountryBySupplier(trdr)
            };

            Trader deliveryParty = null;
            var branchConfiguration = _ediConfiguration.Where(x => x.Branch == branch).FirstOrDefault();
            if (branchConfiguration != null)
            {
                if (!string.IsNullOrEmpty(branchConfiguration.DeliveryNumber))
                {
                    deliveryParty = GetBranchInfo(branch);
                    deliveryParty.Code = branchConfiguration.DeliveryNumber;
                }
            }

            var products = GetOrderProducts(findoc);

            var expressOrderRes = tecClient.SendExpressOrder(businessRelation, buyer, supplier, products, deliveryParty);
        }

        private Trader GetBranchInfo(int branch)
        {
            var deliveryParty = new Trader();
            var query = $@"SELECT B.NAME, B.ADDRESS, B.ZIP, B.CITY, U.INTCODE, U.NAME AS COUNTRYNAME
                            FROM BRANCH B 
                            INNER JOIN COMPANY C ON C.COMPANY = B.COMPANY 
                            INNER JOIN COUNTRY U ON U.COUNTRY = C.COUNTRY
                            WHERE B.BRANCH = {branch} AND B.COMPANY = {_company}";
            using (var ds = XSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    deliveryParty.Name1 = ds.GetAsString(0, "NAME");
                    deliveryParty.Street1 = ds.GetAsString(0, "ADDRESS");
                    deliveryParty.PostalCode = ds.GetAsString(0, "ZIP");
                    deliveryParty.City = ds.GetAsString(0, "CITY");
                    deliveryParty.Country = new Country { CountryCode = ds.GetAsString(0, "INTCODE"), CountryName = ds.GetAsString(0, "COUNTRYNAME") };
                }
            }
            return deliveryParty;
        }

        private Trader GetBuyerInfo()
        {
            var buyer = new Trader();
            var query = $@"SELECT C.NAME, ADDRESS, ZIP, CITY, U.INTCODE, U.NAME AS COUNTRYNAME FROM COMPANY C INNER JOIN COUNTRY U ON U.COUNTRY = C.COUNTRY WHERE COMPANY = {_company}";
            using (var ds = XSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    buyer.Name1 = ds.GetAsString(0, "NAME");
                    buyer.Street1 = ds.GetAsString(0, "ADDRESS");
                    buyer.PostalCode = ds.GetAsString(0, "ZIP");
                    buyer.City = ds.GetAsString(0, "CITY");
                    buyer.Country = new Country { CountryCode = ds.GetAsString(0, "INTCODE"), CountryName = ds.GetAsString(0, "COUNTRYNAME") };
                }
            }
            return buyer;
        }

        private Country GetCountryBySupplier(int trdr)
        {
            var country = new Country();
            var query = $@"SELECT C.INTCODE, C.NAME 
                            FROM COUNTRY C
                            INNER JOIN TRDR T ON T.COUNTRY = C.COUNTRY
                            WHERE T.TRDR = {trdr}";
            using (var ds = XSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    country.CountryCode = ds.GetAsString(0, "INTCODE");
                    country.CountryName = ds.GetAsString(0, "NAME");
                }
            }
            return country;
        }

        private List<RequestProduct> GetOrderProducts(int findoc)
        {
            var products = new List<RequestProduct>();
            var query = $@"SELECT M.MTRLINES AS POSITION, M.MTRL AS PRODUCTID, S.MTRSUPCODE PRODUCTCODE, L.NAME AS PRODUCTNAME, M.QTY1 AS QUANTITY
                                        FROM FINDOC F INNER JOIN MTRLINES M ON M.FINDOC = F.FINDOC
	                                    INNER JOIN MTRL L ON L.MTRL = M.MTRL
	                                    LEFT JOIN MTRSUPCODE S ON S.MTRL = M.MTRL AND S.TRDR = F.TRDR
                                        WHERE M.FINDOC = {findoc} ORDER BY 1";
            using (var ds = XSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    for (int i = 0; i < ds.Count; i++)
                    {
                        var descrs = new List<TecOrder.Dtos.ProductDescription>();
                        descrs.Add(new TecOrder.Dtos.ProductDescription
                        {
                            Description = ds.GetAsString(i, "PRODUCTNAME"),
                            Language = "GR"

                        });
                        products.Add(new RequestProduct
                        {
                            PositionNumber = ds.GetAsInteger(i, "POSITION").ToString(),
                            ProductNumber = ds.GetAsString(i, "PRODUCTCODE"),
                            ProductDescriptions = descrs,
                            Uom = "PCE",
                            Quantity = ds.GetAsFloat(i, "QUANTITY")
                        });
                    }
                }
            }
            return products;
        }

        public override void AfterPost()
        {
            var findocId = Convert.ToInt32(XModule.GetTable("PURDOC").Current["FINDOC"]);

            var fprms = Convert.ToInt32(XModule.GetTable("PURDOC").Current["FPRMS"]);
            if (findocId < 0) 
            {
                var newId = XModule.ObjID();
                _helper.UpdateMaxFindoc(newId);
                if (fprms == 20108) //Αναμενόμενη παραλαβή
                {
                    var trdr = Convert.ToInt32(XModule.GetTable("PURDOC").Current["TRDR"]);
                    var trdbranch = XModule.GetTable("PURDOC").Current["TRDR"] != DBNull.Value ? Convert.ToInt32(XModule.GetTable("PURDOC").Current["TRDBRANCH"]) : 0;
                    var isEndoDoc = _helper.CheckIfIsEndoDoc(trdr, trdbranch);
                    if (isEndoDoc)
                    {
                        try
                        {
                            var orderId = _helper.CreateEndoOrder(newId, trdr, trdbranch);
                            if (orderId == 0) //παραλαβή στο τελικό κατάστημα
                            {
                                ResetReservedQuantities();
                            }
                        }
                        catch (Exception ex)
                        {
                            var errorMsg = ex.Message;
                            var stackTrace = ex.StackTrace;
                            _helper.AppendToLogTable(errorMsg, stackTrace);
                        }
                    }
                }
            }
            base.AfterPost();
        }

        public override void AfterDelete()
        {
            try
            {
                var findoc = Convert.ToInt32(XModule.GetTable("PURDOC").Current["FINDOC"]);
                _helper.ReverseMaxFindoc(findoc);
            }
            catch (Exception ex)
            {
                XSupport.Warning(ex.Message);
            }
            base.AfterDelete();
        }

        private void ResetReservedQuantities()
        {
            try
            {
                var Itelines = XModule.GetTable("ITELINES").CreateDataTable(true).AsEnumerable().ToList();
                var initFindocs = Itelines
                    .Select(x => x["CCCINITFINDOC"] != DBNull.Value
                     ? Convert.ToInt32(x["CCCINITFINDOC"].ToString()) : 0)
                    .Distinct()
                    .ToList();
                if (initFindocs.Any())
                {
                    var findocs = initFindocs.Where(x => x > 0).ToList();
                    foreach (var findoc in findocs)
                    {
                        var q = $"UPDATE MTRLINES SET PENDING = 1, QTY1CANC = 0 WHERE FINDOC = {findoc}";
                        XSupport.ExecuteSQL(q, null);
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                var stackTrace = ex.StackTrace;
                _helper.AppendToLogTable(errorMsg, stackTrace);
            }
        }
    }
}
