﻿using Softone;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Skama
{
    public class S1Init : TXCode
    {
        public override void Initialize()
        {
            var validSn = new List<string>();
            validSn.Add("02102339990413");
            validSn.Add("01104226044319");
            var currentSn = XSupport.ConnectionInfo.SerialNum;
            if (!validSn.Contains(currentSn))
            {
                throw new Exception("Δεν έχετε δικαιώμα χρήσης του Skama dll. Παρακαλώ αφαιρέστε το από το αρχείο σύνδεσης.");
            }

            MessageBox.Show("Τα custom εργαλεία και η ανάπτυξη κώδικα με εργαλεία τρίτων αποτελεί πνευματικά δικαιώματα της Logistic-i.", "Copyright © LOGISTIC-i 2021"
                , MessageBoxButtons.OK, MessageBoxIcon.Information);
            base.Initialize();
        }
    }
}
