﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json;
using Skama.Enums;
using Skama.Models;
using Softone;

namespace Skama
{
    [WorksOn("SALDOC")]
    public class Saldoc : TXCode
    {
        private CounterFrm _frm;
        private bool _gkise;
        private bool _newRecord = false;
        private Helper _helper = null;
        private S1Tools _tools = null;
        private List<int> validDocTypes = new List<int>();

        public override void Initialize()
        {
            _helper = new Helper(XSupport, XModule);
            _tools = new S1Tools(XSupport);
            XModule.SetEvent("ON_ITELINES_POST", OnItelinesPost);
            XModule.SetEvent("ON_ITELINES_AFTERDELETE", OnItelinesAfterDelete);
            XModule.SetEvent("ON_SALDOC_FINDOC", OnFindoc);
            validDocTypes.AddRange(new List<int>() { 10111, 10144, 10146 });
            base.Initialize();
        }

        public override void OnFormLoad(string formname)
        {
            _gkise = formname == "Διαχείριση Γκισέ" ? true : false;
            if (!_gkise)
            {
                return;
            }
            var isRelFrm = false;
            _frm = new CounterFrm(XSupport, XModule, _helper, isRelFrm);
            base.OnFormLoad(formname);
        }

        private void OnFindoc(object Sender, XEventArgs e)
        {
            try
            {
                var findoc = Convert.ToInt32(XModule.GetTable("SALDOC").Current["FINDOC"]);
                if (findoc < 0)
                {
                    return;
                }

                UpdateOfferStatus(findoc);

                if (!_gkise)
                {
                    return;
                }

                var fprms = Convert.ToInt32(XModule.GetTable("SALDOC").Current["FPRMS"]);
                if (fprms != 10106 && fprms != 10105)
                {
                    return;
                }
                var docs = _helper.GenerateRelDocs();
                if (!docs.Any())
                {
                    return;
                }
                if (!XSupport.ConnectionInfo.IsAdministrator)
                {
                    if (!_helper.ValidCustomParams())
                    {
                        XSupport.Warning("Λανθασμένη παραμετροποίηση Custom διαδικασιών! Η καταχώριση των σχετικών παραστατικών διακινήσεων ακυρώνεται!");
                    }
                }
                var whouse = Convert.ToInt32(XModule.GetTable("MTRDOC").Current["WHOUSE"]);

                _helper.PostDocs(docs, whouse);
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                var stackTrace = ex.StackTrace;
                _helper.AppendToLogTable(errorMsg, stackTrace);
            }
        }

        private void UpdateOfferStatus(int findoc)
        {
            try
            {
                var tfprms = Convert.ToInt32(XModule.GetTable("SALDOC").Current["TFPRMS"]);
                if (tfprms != 202 && tfprms != 201)
                {
                    return;
                }
                var query = $@"SELECT FINDOC FROM FINDOC WHERE SOSOURCE = 1351 AND COMPANY = {XSupport.ConnectionInfo.CompanyId} 
                                AND FINDOC IN (SELECT FINDOCS FROM MTRLINES WHERE FINDOC = {findoc})";
                using (var ds = XSupport.GetSQLDataSet(query))
                {
                    if (ds.Count == 0)
                    {
                        if (tfprms == 202)
                        {
                            XSupport.ExecuteSQL($"UPDATE FINDOC SET FINSTATES = 3002 WHERE FINDOC = {findoc}", null);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < ds.Count; i++)
                        {
                            var findocId = ds.GetAsInteger(i, "FINDOC");
                            if (tfprms == 201)
                            {
                                XSupport.ExecuteSQL($"UPDATE FINDOC SET FINSTATES = 3003 WHERE FINDOC = {findocId} ", null);
                            }
                            if (tfprms == 202)
                            {
                                XSupport.ExecuteSQL($"UPDATE FINDOC SET FINSTATES = 3007 WHERE FINDOC = {findocId}", null);
                                XSupport.ExecuteSQL($"UPDATE FINDOC SET FINSTATES = 3002 WHERE FINDOC = {findoc}", null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                XSupport.Warning(ex.Message);
            }
        }

        public override void AfterInsert()
        {
            if (!_gkise)
            {
                return;
            }

            _newRecord = true;
            _frm.availabilityList = new List<ItemInfo>();
            _frm.cachedList = new List<ItemSearch>();
            _frm.cachedAvailabilityList = new List<ItemInfo>();
            _frm.txtSearch.ReadOnly = false;
            _frm.btnSearch.Enabled = true;
            _frm.gridView1.OptionsBehavior.ReadOnly = false;
            _frm.gridView2.OptionsBehavior.ReadOnly = false;

            base.AfterInsert();
        }

        public override void AfterLocate()
        {
            try
            {
                if (!_gkise)
                {
                    return;
                }
                if (!_newRecord)
                {
                    return;
                }
                var fprms = Convert.ToInt32(XModule.GetTable("SALDOC").Current["FPRMS"]);
                var findoc = Convert.ToInt32(XModule.GetTable("SALDOC").Current["FINDOC"]);
                if (fprms != 10106 && fprms != 10105)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                var stackTrace = ex.StackTrace;
                MessageBox.Show(ex.Message, "Προσοχή!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                _newRecord = false;
            }
            _frm.availabilityList = new List<ItemInfo>();
            _frm.cachedList = new List<ItemSearch>();
            _frm.cachedAvailabilityList = new List<ItemInfo>();

            base.AfterLocate();
        }

        public override void BeforePost()
        {
            var fprms = Convert.ToInt32(XModule.GetTable("SALDOC").Current["FPRMS"]);
            var findoc = Convert.ToInt32(XModule.GetTable("FINDOC").Current["FINDOC"]);

            if (_gkise && (fprms == 10106 || fprms == 10105) && findoc < 0) //Παραγγελία από Γκισέ και B2B
            {
                var itemRows = XModule.GetTable("CCCITEMINFO").CreateDataTable(true).AsEnumerable().ToList();
                var itemsWithNoStock = CheckCurrentAvailability(itemRows);
                if (itemsWithNoStock.Any())
                {
                    var errorMsg = $"Η διαθεσιμότητα ορισμένων ειδών έχει αλλάξει. {Environment.NewLine}";
                    foreach (var item in itemsWithNoStock)
                    {
                        errorMsg += $"{item.WHName} | {item.Code} - {item.Description} | Ποσότητα Παραγγελίας : {item.OrderQty} | Διαθέσιμα : {item.TotalRemain} {Environment.NewLine}";
                    }
                    errorMsg += "Παρακαλώ προχωρήστε σε αναπροσαρμογή των ποσοτήτων.";
                    XSupport.Exception(errorMsg);
                }
            }

            if (findoc < 0 && fprms == 10111) //Δελτίο αποστολής ενδοδιακίνησης
            {
                UpdateRelDocs();
            }

            base.BeforePost();
        }

        public override void AfterPost()
        {
            var paramss = XModule.Params.Where(x => !string.IsNullOrEmpty(x)).ToList();
            var findocId = Convert.ToInt32(XModule.GetTable("SALDOC").Current["FINDOC"]);
            var fprms = Convert.ToInt32(XModule.GetTable("SALDOC").Current["FPRMS"]);
            if (findocId > 0)
            {
                _newRecord = false;
            }
            else
            {
                _newRecord = true;
                var newId = XModule.ObjID();

                if (validDocTypes.IndexOf(fprms) != -1)
                {
                    _helper.UpdateMaxFindoc(newId);
                }
                if (fprms != 10111)//Δελτίο αποστολής ενδοδιακίνησης
                {
                    return;
                }
                try
                {
                    var receiptId = _helper.CreateExpectedReceipt(newId);
                    if (receiptId > 0)
                    {
                        var query = $"SELECT 1 FROM CCCMANTISMASTER WHERE SOSOURCE = 1251 AND FROMSOURCE = 0 AND FINDOC = {receiptId}";
                        using (var ds = XSupport.GetSQLDataSet(query, null))
                        {
                            if (ds.Count == 0)
                            {
                                using (var Purdoc = XSupport.CreateModule("PURDOC")) //Post to Mantis
                                {
                                    Purdoc.LocateData(receiptId);
                                    Purdoc.PostData();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    var stackTrace = ex.StackTrace;
                    _helper.AppendToLogTable(errorMsg, stackTrace);
                }
            }

            if (_gkise)
            {
                _frm.availabilityList = new List<ItemInfo>();
                _frm.cachedList = new List<ItemSearch>();
                _frm.cachedAvailabilityList = new List<ItemInfo>();
            }

            base.AfterPost();
        }

        public override void AfterCancel()
        {
            _newRecord = false;
            base.AfterCancel();
        }

        public override void AfterDelete()
        {
            try
            {
                var findoc = Convert.ToInt32(XModule.GetTable("SALDOC").Current["FINDOC"]);
                _helper.ReverseMaxFindoc(findoc);
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                var stackTrace = ex.StackTrace;
                _helper.AppendToLogTable(errorMsg, stackTrace);
            }
            base.AfterDelete();
        }

        private void OnItelinesPost(object Sender, XEventArgs e)
        {
            if (!_gkise)
            {
                return;
            }
            var fprmsObj = XModule.GetTable("SALDOC").Current["FPRMS"];
            if (fprmsObj == DBNull.Value)
            {
                return;
            }
            var fprms = Convert.ToInt32(fprmsObj);
            if (fprms != 10106 && fprms != 10105)
            {
                return;
            }
            var mtrlObj = XModule.GetTable("ITELINES").Current["MTRL"];
            var whouse = Convert.ToInt32(XModule.GetTable("MTRDOC").Current["WHOUSE"]);
            if (mtrlObj == DBNull.Value)
            {
                return;
            }

            var mtrl = Convert.ToInt32(mtrlObj);
            var otherWhouseRecords = _frm.cachedAvailabilityList.Where(x => x.ProductId == mtrl && x.WhouseId != whouse && x.OrderQty > 0).ToList();
            if (!otherWhouseRecords.Any())
            {
                var qty1 = Convert.ToDouble(XModule.GetTable("ITELINES").Current["QTY1"]);
                var whouseRemain = double.Parse(XModule.EvalFormula($"FRemQty1PerWHouse({mtrl}, {whouse}, SALDOC.TRNDATE)").ToString());
                var whouseReserved = double.Parse(XModule.EvalFormula($"FItemReserved({mtrl}, '', '{whouse}')").ToString());
                var actualStock = whouseRemain - whouseReserved;
                if (qty1 > actualStock)
                {
                    XSupport.Exception("Η ποσότητα που πληκτρολογήσατε ξεπερνά το διαθέσιμο υπόλοιπο του καταστήματος.");
                }
            }
            try
            {
                var relRecords = _frm.cachedAvailabilityList.Where(x => x.ProductId == mtrl).ToList();
                AppendItemInfo(relRecords);
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                var stackTrace = ex.StackTrace;
                _helper.AppendToLogTable(errorMsg, stackTrace);
            }
        }

        private void OnItelinesAfterDelete(object Sender, XEventArgs e)
        {
            if (!_gkise)
            {
                return;
            }
            try
            {
                var whAnalysisTbl = XModule.GetTable("CCCITEMINFO");
                var whAnalysis = whAnalysisTbl.CreateDataTable(true).AsEnumerable().ToList();
                var itelines = XModule.GetTable("ITELINES").CreateDataTable(true).AsEnumerable().ToList();
                if (!itelines.Any())
                {
                    while (whAnalysisTbl.Count > 0)
                    {
                        whAnalysisTbl.Current.Delete();
                    }
                }
                else
                {
                    var existingIds = itelines.Select(x => int.Parse(x["MTRLINES"].ToString())).ToList();
                    if (whAnalysis.Any())
                    {
                        var whAnalysisIds = whAnalysis.Select(x => int.Parse(x["MTRLINES"].ToString())).ToList();
                        var idsToDelete = whAnalysisIds.Where(x => !existingIds.Contains(x)).Distinct().ToList();
                        if (idsToDelete.Any())
                        {
                            foreach (var id in idsToDelete)
                            {
                                while (whAnalysisTbl.Count > 0)
                                {
                                    var recordsToDelete = whAnalysisTbl.CreateDataTable(true)
                                        .AsEnumerable()
                                        .Where(x => Convert.ToInt32(x["MTRLINES"].ToString()) == id)
                                        .ToList();
                                    if (!recordsToDelete.Any())
                                    {
                                        break;
                                    }
                                    var recNo = whAnalysisTbl.Find("MTRLINES", id);
                                    if (recNo != -1)
                                    {
                                        whAnalysisTbl.Current.Edit(recNo);
                                        whAnalysisTbl.Current.Delete();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                XSupport.Warning(ex.Message);
            }
        }

        private List<ItemSearch> CheckCurrentAvailability(List<DataRow> itemRows)
        {
            var items = new List<ItemSearch>();
            if (itemRows.Any())
            {
                foreach (var row in itemRows)
                {
                    var itemId = Convert.ToInt32(row["MTRL"].ToString());
                    var itemCode = row[5].ToString();
                    var itemName = row[6].ToString();
                    var whouse = Convert.ToInt32(row["WHOUSE"].ToString());
                    var whName = XSupport.GetMemoryTable("WHOUSE").CreateDataTable(true).AsEnumerable().ToList()
                        .Where(x => x["WHOUSE"].ToString() == whouse.ToString()).First()["NAME"].ToString();
                    var orderQty = Convert.ToDouble(row["ORDERQTY"].ToString());

                    try
                    {
                        var whouseRemain = double.Parse(XModule.EvalFormula($"FRemQty1PerWHouse({itemId}, {whouse}, SALDOC.TRNDATE)").ToString());
                        var whouseReserved = double.Parse(XModule.EvalFormula($"FItemReserved({itemId}, '', '{whouse}')").ToString());
                        var actualStock = whouseRemain - whouseReserved;
                        if (orderQty > actualStock)
                        {
                            items.Add(new ItemSearch
                            {
                                Id = itemId,
                                Code = itemCode,
                                Description = itemName,
                                OrderQty = orderQty,
                                TotalRemain = actualStock,
                                WHName = whName
                            });
                        }

                    }
                    catch (Exception ex)
                    {
                        var errorMsg = ex.Message;
                        var stackTrace = ex.StackTrace;
                        _helper.AppendToLogTable(errorMsg, stackTrace);
                    }
                }
            }
            else
            {
                var itelines = XModule.GetTable("ITELINES").CreateDataTable(true).AsEnumerable().ToList();
                foreach (var row in itelines)
                {
                    try
                    {
                        var itemId = Convert.ToInt32(row["MTRL"].ToString());
                        var itemCode = row["X_CODE"].ToString();
                        var itemName = row["X_NAME"].ToString();
                        var whouse = Convert.ToInt32(XModule.GetTable("MTRDOC").Current["WHOUSE"]);
                        var whName = XSupport.GetMemoryTable("WHOUSE").CreateDataTable(true)
                            .AsEnumerable()
                            .ToList()
                            .Where(x=> Convert.ToInt32(x["WHOUSE"]) == whouse).
                            FirstOrDefault()?[4].ToString();
                        var orderQty = double.Parse(row["QTY1"].ToString());
                        var whouseRemain = double.Parse(XModule.EvalFormula($"FRemQty1PerWHouse({itemId}, {whouse}, SALDOC.TRNDATE)").ToString());
                        var whouseReserved = double.Parse(XModule.EvalFormula($"FItemReserved({itemId}, '', '{whouse}')").ToString());
                        var actualStock = whouseRemain - whouseReserved;
                        if (orderQty > actualStock)
                        {
                            items.Add(new ItemSearch
                            {
                                Id = itemId,
                                Code = itemCode,
                                Description = itemName,
                                OrderQty = orderQty,
                                TotalRemain = actualStock,
                                WHName = whName
                            });
                        }

                    }
                    catch (Exception ex)
                    {
                        var errorMsg = ex.Message;
                        var stackTrace = ex.StackTrace;
                        _helper.AppendToLogTable(errorMsg, stackTrace);
                    }
                }
            }
            return items;
        }

        private void UpdateRelDocs()
        {
            try
            {
                using (var Itelines = XModule.GetTable("ITELINES"))
                {
                    for (int i = 0; i < Itelines.Count; i++)
                    {
                        var findocss = 0;
                        if (Itelines.Current["FINDOCS"] != DBNull.Value)
                        {
                            var mtrlines = Convert.ToInt32(Itelines.Current["MTRLINES"]);
                            var recNo = Itelines.Find("MTRLINES", mtrlines);
                            if (recNo != -1)
                            {
                                Itelines.Current.Edit(recNo);
                                findocss = Convert.ToInt32(Itelines.Current["FINDOCS"]);
                                var mtrliness = Convert.ToInt32(Itelines.Current["MTRLINESS"]);
                                var reldocs = Itelines.Current["CCCRELDOCS"].ToString();
                                var newReldocs = !string.IsNullOrEmpty(reldocs) ? $@"{reldocs}|{findocss};{mtrliness}" : $@"{findocss};{mtrliness}";
                                Itelines.Current["CCCRELDOCS"] = newReldocs;
                                Itelines.Current.Post();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                var stackTrace = ex.StackTrace;
                _helper.AppendToLogTable(errorMsg, stackTrace);
            }
        }

        private void AppendItemInfo(List<ItemInfo> relRecords)
        {
            try
            {
                var whouseId = Convert.ToInt32(XModule.GetTable("MTRDOC").Current["WHOUSE"]);
                relRecords = relRecords.Where(x => x.OrderQty > 0).ToList();
                using (var ItemInfo = XModule.GetTable("CCCITEMINFO"))
                {
                    if (relRecords.Any())
                    {
                        foreach (var record in relRecords)
                        {
                            var recNo = ItemInfo.Find("WHOUSE;MTRL", record.WhouseId, record.ProductId);
                            var tbl = ItemInfo.CreateDataTable(true);
                            if (recNo > -1)
                            {
                                ItemInfo.Current.Edit(recNo);
                            }
                            else
                            {
                                ItemInfo.Current.Append();
                            }
                            ItemInfo.Current["WHOUSE"] = record.WhouseId;
                            ItemInfo.Current["TOTALREMAIN"] = record.TotalRemain;
                            ItemInfo.Current["NOMINREMAIN"] = record.NoMinStockRemain;
                            ItemInfo.Current["EXPECTEDQTY"] = record.ExpectedQty;
                            ItemInfo.Current["RESERVEDQTY"] = record.ReservedQty;
                            ItemInfo.Current["ACTUALREMAIN"] = record.ActualRemain;
                            ItemInfo.Current["PROPQTY"] = record.PropQty;
                            ItemInfo.Current["ORDERQTY"] = record.OrderQty;
                            ItemInfo.Current["DIRECT"] = record.Direct;
                            ItemInfo.Current.Post();
                        }
                    }
                    else
                    {
                        ItemInfo.Current.Append();
                        ItemInfo.Current["WHOUSE"] = whouseId;
                        ItemInfo.Current["ORDERQTY"] = Convert.ToDouble(XModule.GetTable("ITELINES").Current["QTY1"]);
                        ItemInfo.Current.Post();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override object ExecCommand(int Cmd)
        {
            if (!_gkise)
            {
                return base.ExecCommand(Cmd);
            }

            if (Cmd == 999001)
            {
                if (Convert.ToInt32(XModule.GetTable("FINDOC").Current["FINDOC"]) < 0)
                {
                    ShowItemSearch();
                }
                else
                {
                    XSupport.Exception("Το παραστατικό έχει καταχωρηθεί και δεν επιτρέπεται η προσθήκη επιπλέον ειδών.");
                }
            }
            else if (Cmd == 999002)
            {
                ShowItemInfo(Cmd);
            }

            return base.ExecCommand(Cmd);
        }

        private void ShowItemInfo(int cmd)
        {
            try
            {
                var fprmsObj = XModule.GetTable("FINDOC").Current["FPRMS"];
                if (fprmsObj == DBNull.Value)
                {
                    XSupport.Warning("Για να δείτε διαθεσιμότητες θα πρέπει πρώτα να επιλέξετε σειρά παρασταττικού.");
                    return;
                }
                var fprms = Convert.ToInt32(fprmsObj);
                if (fprms != 10106)
                {
                    return;
                }
                var findoc = Convert.ToInt32(XModule.GetTable("FINDOC").Current["FINDOC"]);
                var whouse = Convert.ToInt32(XModule.GetTable("MTRDOC").Current["WHOUSE"]);
                var mtrlObj = XModule.GetTable("ITELINES").Current["MTRL"];
                if (mtrlObj != DBNull.Value)
                {
                    var mtrl = Convert.ToInt32(mtrlObj);
                    var code = XModule.GetTable("ITELINES").Current["X_CODE"].ToString();
                    _frm.txtSearch.Text = code;
                    var orderQty = 0.0;
                    var qty1Obj = XModule.GetTable("ITELINES").Current["QTY1"];

                    if (qty1Obj != DBNull.Value)
                    {
                        orderQty = Convert.ToDouble(qty1Obj);
                    }

                    _frm.btnSearch.Enabled = false;
                    _frm.btnExtendedSearch.Enabled = false;
                    _frm.txtSearch.ReadOnly = true;
                    _frm.RefreshGridData(code, "", cmd);
                    if (findoc > 0)
                    { 
                        _frm.gridView1.OptionsBehavior.ReadOnly = true;
                        _frm.gridView2.OptionsBehavior.ReadOnly = true;
                    }
                    var branch = Convert.ToInt32(XModule.GetTable("SALDOC").Current["BRANCH"]);
                    var _tools = new S1Tools(XSupport);
                    var ret = _tools.GetFieldFromMemoryTable("BRANCH", "COMPANY;BRANCH", "NAME", FieldType.String, XSupport.ConnectionInfo.CompanyId, branch);
                    if (ret != null)
                    {
                        var branchName = ret.ToString();
                        if (!string.IsNullOrEmpty(branchName))
                        {
                            _frm.Text = $"Αναζήτηση είδους - {branchName}";
                        }
                    }
                    //var buttonID = XModule.OpenSubForm("ITEMSEARCH", 1);
                    _frm.Visible = false;
                    var dialogRes = _frm.ShowDialog();
                    if (findoc < 0 && dialogRes == DialogResult.OK)
                    {
                        var itemsWithOrderQty = _frm.availabilityList
                            .Where(x => x.ProductId == mtrl)
                            //.Where(x => x.OrderQty > 0)
                            .ToList();
                        UpdateItemInfoTbl(itemsWithOrderQty);
                    }
                }
            }
            catch (Exception ex)
            {
                XSupport.Warning(ex.Message);
            }
        }

        private void UpdateItemInfoTbl(List<ItemInfo> items)
        {
            try
            {
                var infoTbl = XModule.GetTable("CCCITEMINFO");
                foreach (var item in items)
                {
                    var mtrl = item.ProductId;
                    var whouse = item.WhouseId;
                    var recNo = infoTbl.Find("WHOUSE;MTRL", whouse, mtrl);
                    if (recNo != -1)
                    {
                        if (item.OrderQty > 0)
                        {
                            infoTbl.Current.Edit(recNo);
                            infoTbl.Current["ORDERQTY"] = item.OrderQty;
                            infoTbl.Current.Post();
                        }
                        else
                        {
                            infoTbl.Current.Delete();
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void ShowItemSearch()
        {
            var errorsMsg = new List<string>();
            var seriesObj = XModule.GetTable("SALDOC").Current["SERIES"];
            if (seriesObj == DBNull.Value)
            {
                errorsMsg.Add("Δεν έχετε επιλέξει σειρά παραστατικού.");
            }
            var customerObj = XModule.GetTable("SALDOC").Current["TRDR"];
            if (customerObj == DBNull.Value)
            {
                errorsMsg.Add("Δεν έχετε επιλέξει πελάτη.");
            }
            if (errorsMsg.Any())
            {
                var msg = string.Join(Environment.NewLine, errorsMsg);
                XSupport.Warning(msg);
                return;
            }

            var _tools = new S1Tools(XSupport);
            _frm.btnSearch.Enabled = true;
            _frm.btnExtendedSearch.Enabled = true;
            _frm.txtSearch.ReadOnly = false;
            _frm.txtSearch.Text = null;
            _frm.gridView1.OptionsBehavior.ReadOnly = false;
            _frm.gridView2.OptionsBehavior.ReadOnly = false;
            _frm.gridInfo.DataSource = null;
            _frm.gridAvailability.DataSource = null;
            _frm.lblFooter.Text = null;
            _frm.txtCusQty.Text = "0";
            _frm.txtEndoQty.Text = "0";
            _frm.prevSearchValues = new List<string>();
            //_frm.cachedList = new List<ItemSearch>();
            _frm.availabilityList = new List<ItemInfo>();
            _frm.cachedAvailabilityList = new List<ItemInfo>(); //??
            _frm.items = new List<ItemSearch>();
            _frm.txtSearch.Focus();

            var branch = Convert.ToInt32(XModule.GetTable("SALDOC").Current["BRANCH"]);
            var ret = _tools.GetFieldFromMemoryTable("BRANCH", "COMPANY;BRANCH", "NAME", FieldType.String, XSupport.ConnectionInfo.CompanyId, branch);
            if (ret != null)
            {
                var branchName = ret.ToString();
                if (!string.IsNullOrEmpty(branchName))
                {
                    _frm.Text = $" Αναζήτηση είδους - {branchName}";
                }
            }

            //var buttonID = XModule.OpenSubForm("ITEMSEARCH", 1);
            _frm.Visible = false;
            _frm.ShowDialog();
            //if (buttonID == 1)
            //{
            //    AppendItelines();                
            //}
        }

        internal void AppendItelines()
        {
            try
            {
                var whouseId = Convert.ToInt32(XModule.GetTable("MTRDOC").Current["WHOUSE"]);
                var itemsWithQuantity = _frm.cachedList.Where(x => x.OrderQty > 0).ToList();//_frm.availabilityList.Where(x => x.OrderQty > 0).ToList();
                if (itemsWithQuantity.Any())
                {
                    var grouped = itemsWithQuantity.GroupBy(x => x.Id)
                        .Select(y => new ItemInfo
                        {
                            ProductId = y.Key,
                            OrderQty = y.Sum(z => z.OrderQty)
                        }).ToList();
                    using (var Itelines = XModule.GetTable("ITELINES"))
                    {
                        foreach (var group in grouped)
                        {
                            var append = true;
                            var lineMtrls = Itelines.CreateDataTable(true).AsEnumerable().Select(x => Convert.ToInt32(x["MTRL"])).ToList();
                            if (lineMtrls.Contains(group.ProductId))
                            {
                                var ds = XSupport.GetSQLDataSet($"SELECT CODE FROM MTRL WHERE MTRL = {group.ProductId}");
                                var code = ds.GetAsString(0, "CODE");
                                var dr = MessageBox.Show($"Το είδος {code} υπάρχει ήδη στο παραστατικό. Να προστεθεί εκ νέου σε νέα γραμμή?"
                                    , "Προσθήκη είδους"
                                    , MessageBoxButtons.YesNo
                                    , MessageBoxIcon.Information
                                    , MessageBoxDefaultButton.Button2);
                                if (dr == DialogResult.No)
                                {
                                    append = false;
                                }
                            }

                            if (!append)
                            {
                                continue;
                            }
                            Itelines.Current.Append();
                            Itelines.Current["MTRL"] = group.ProductId;
                            Itelines.Current["QTY1"] = group.OrderQty;
                            var initQty = _frm.cachedList.Where(x => x.Id == group.ProductId).FirstOrDefault()?.InitQty;
                            if (initQty != null && initQty > 0)
                            {
                                Itelines.Current["CCCINITQTY"] = initQty;
                            }
                            Itelines.Current.Post();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
