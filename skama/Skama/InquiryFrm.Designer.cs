﻿namespace Skama
{
    partial class InquiryFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lblRespQty = new DevExpress.XtraEditors.LabelControl();
            this.lblInstr = new DevExpress.XtraEditors.LabelControl();
            this.lblPrDescr = new DevExpress.XtraEditors.LabelControl();
            this.lblPrNumber = new DevExpress.XtraEditors.LabelControl();
            this.lblResponseText = new DevExpress.XtraEditors.LabelControl();
            this.lblResponseQuantity = new DevExpress.XtraEditors.LabelControl();
            this.lblItemInstruction = new DevExpress.XtraEditors.LabelControl();
            this.lblProductDescr = new DevExpress.XtraEditors.LabelControl();
            this.lblProductNumber = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblNetPrc = new DevExpress.XtraEditors.LabelControl();
            this.lblListPrice = new DevExpress.XtraEditors.LabelControl();
            this.lblNetPrice = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.IsSplitterFixed = true;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.panelControl2);
            this.splitContainerControl1.Panel1.Controls.Add(this.panelControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.labelControl1);
            this.splitContainerControl1.Panel2.Controls.Add(this.lblNetPrc);
            this.splitContainerControl1.Panel2.Controls.Add(this.lblListPrice);
            this.splitContainerControl1.Panel2.Controls.Add(this.lblNetPrice);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(800, 450);
            this.splitContainerControl1.SplitterPosition = 567;
            this.splitContainerControl1.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gridControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 185);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(567, 265);
            this.panelControl2.TabIndex = 1;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 2);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(563, 261);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowViewCaption = true;
            this.gridView1.ViewCaption = "Εναλλακτικά είδη";
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.25F);
            this.gridColumn1.AppearanceCell.Options.UseFont = true;
            this.gridColumn1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.gridColumn1.AppearanceHeader.Options.UseFont = true;
            this.gridColumn1.Caption = "Κωδικός";
            this.gridColumn1.FieldName = "Code";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.25F);
            this.gridColumn2.AppearanceCell.Options.UseFont = true;
            this.gridColumn2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.gridColumn2.AppearanceHeader.Options.UseFont = true;
            this.gridColumn2.Caption = "Περιγραφή";
            this.gridColumn2.FieldName = "Name";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9.25F);
            this.gridColumn3.AppearanceCell.Options.UseFont = true;
            this.gridColumn3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.gridColumn3.AppearanceHeader.Options.UseFont = true;
            this.gridColumn3.Caption = "Barcode Ean";
            this.gridColumn3.FieldName = "Ean";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lblRespQty);
            this.panelControl1.Controls.Add(this.lblInstr);
            this.panelControl1.Controls.Add(this.lblPrDescr);
            this.panelControl1.Controls.Add(this.lblPrNumber);
            this.panelControl1.Controls.Add(this.lblResponseText);
            this.panelControl1.Controls.Add(this.lblResponseQuantity);
            this.panelControl1.Controls.Add(this.lblItemInstruction);
            this.panelControl1.Controls.Add(this.lblProductDescr);
            this.panelControl1.Controls.Add(this.lblProductNumber);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(567, 185);
            this.panelControl1.TabIndex = 0;
            // 
            // lblRespQty
            // 
            this.lblRespQty.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblRespQty.Appearance.Options.UseFont = true;
            this.lblRespQty.Location = new System.Drawing.Point(5, 91);
            this.lblRespQty.Name = "lblRespQty";
            this.lblRespQty.Size = new System.Drawing.Size(144, 17);
            this.lblRespQty.TabIndex = 8;
            this.lblRespQty.Text = "Ποσότητα κάλυψης :";
            // 
            // lblInstr
            // 
            this.lblInstr.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblInstr.Appearance.Options.UseFont = true;
            this.lblInstr.Location = new System.Drawing.Point(90, 131);
            this.lblInstr.Name = "lblInstr";
            this.lblInstr.Size = new System.Drawing.Size(59, 17);
            this.lblInstr.TabIndex = 7;
            this.lblInstr.Text = "Οδηγία :";
            // 
            // lblPrDescr
            // 
            this.lblPrDescr.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblPrDescr.Appearance.Options.UseFont = true;
            this.lblPrDescr.Location = new System.Drawing.Point(65, 49);
            this.lblPrDescr.Name = "lblPrDescr";
            this.lblPrDescr.Size = new System.Drawing.Size(88, 17);
            this.lblPrDescr.TabIndex = 6;
            this.lblPrDescr.Text = "Περιγραφή :";
            // 
            // lblPrNumber
            // 
            this.lblPrNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblPrNumber.Appearance.Options.UseFont = true;
            this.lblPrNumber.Location = new System.Drawing.Point(86, 12);
            this.lblPrNumber.Name = "lblPrNumber";
            this.lblPrNumber.Size = new System.Drawing.Size(66, 17);
            this.lblPrNumber.TabIndex = 5;
            this.lblPrNumber.Text = "Κωδικός :";
            // 
            // lblResponseText
            // 
            this.lblResponseText.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblResponseText.Appearance.Options.UseFont = true;
            this.lblResponseText.Location = new System.Drawing.Point(12, 153);
            this.lblResponseText.Name = "lblResponseText";
            this.lblResponseText.Size = new System.Drawing.Size(91, 16);
            this.lblResponseText.TabIndex = 4;
            this.lblResponseText.Text = "ResponseText";
            this.lblResponseText.Visible = false;
            // 
            // lblResponseQuantity
            // 
            this.lblResponseQuantity.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblResponseQuantity.Appearance.Options.UseFont = true;
            this.lblResponseQuantity.Location = new System.Drawing.Point(159, 91);
            this.lblResponseQuantity.Name = "lblResponseQuantity";
            this.lblResponseQuantity.Size = new System.Drawing.Size(119, 16);
            this.lblResponseQuantity.TabIndex = 3;
            this.lblResponseQuantity.Text = "ResponseQuantity";
            // 
            // lblItemInstruction
            // 
            this.lblItemInstruction.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblItemInstruction.Appearance.Options.UseFont = true;
            this.lblItemInstruction.Location = new System.Drawing.Point(159, 131);
            this.lblItemInstruction.Name = "lblItemInstruction";
            this.lblItemInstruction.Size = new System.Drawing.Size(102, 16);
            this.lblItemInstruction.TabIndex = 2;
            this.lblItemInstruction.Text = "ItemInstruction";
            // 
            // lblProductDescr
            // 
            this.lblProductDescr.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblProductDescr.Appearance.Options.UseFont = true;
            this.lblProductDescr.Location = new System.Drawing.Point(159, 49);
            this.lblProductDescr.Name = "lblProductDescr";
            this.lblProductDescr.Size = new System.Drawing.Size(124, 16);
            this.lblProductDescr.TabIndex = 1;
            this.lblProductDescr.Text = "ProductDescription";
            // 
            // lblProductNumber
            // 
            this.lblProductNumber.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblProductNumber.Appearance.Options.UseFont = true;
            this.lblProductNumber.Location = new System.Drawing.Point(159, 12);
            this.lblProductNumber.Name = "lblProductNumber";
            this.lblProductNumber.Size = new System.Drawing.Size(100, 16);
            this.lblProductNumber.TabIndex = 0;
            this.lblProductNumber.Text = "ProductNumber";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(1, 71);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(120, 17);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "Τιμή καταλόγου :";
            // 
            // lblNetPrc
            // 
            this.lblNetPrc.Appearance.Font = new System.Drawing.Font("Tahoma", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblNetPrc.Appearance.Options.UseFont = true;
            this.lblNetPrc.Location = new System.Drawing.Point(44, 22);
            this.lblNetPrc.Name = "lblNetPrc";
            this.lblNetPrc.Size = new System.Drawing.Size(75, 17);
            this.lblNetPrc.TabIndex = 7;
            this.lblNetPrc.Text = "Νέτη τιμή :";
            // 
            // lblListPrice
            // 
            this.lblListPrice.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblListPrice.Appearance.Options.UseFont = true;
            this.lblListPrice.Location = new System.Drawing.Point(128, 71);
            this.lblListPrice.Name = "lblListPrice";
            this.lblListPrice.Size = new System.Drawing.Size(55, 16);
            this.lblListPrice.TabIndex = 6;
            this.lblListPrice.Text = "ListPrice";
            // 
            // lblNetPrice
            // 
            this.lblNetPrice.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblNetPrice.Appearance.Options.UseFont = true;
            this.lblNetPrice.Location = new System.Drawing.Point(128, 22);
            this.lblNetPrice.Name = "lblNetPrice";
            this.lblNetPrice.Size = new System.Drawing.Size(54, 16);
            this.lblNetPrice.TabIndex = 5;
            this.lblNetPrice.Text = "NetPrice";
            // 
            // InquiryFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainerControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "InquiryFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Inquiry";
            this.Load += new System.EventHandler(this.InquiryFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lblProductNumber;
        private DevExpress.XtraEditors.LabelControl lblResponseText;
        private DevExpress.XtraEditors.LabelControl lblResponseQuantity;
        private DevExpress.XtraEditors.LabelControl lblItemInstruction;
        private DevExpress.XtraEditors.LabelControl lblProductDescr;
        private DevExpress.XtraEditors.LabelControl lblListPrice;
        private DevExpress.XtraEditors.LabelControl lblNetPrice;
        private DevExpress.XtraEditors.LabelControl lblRespQty;
        private DevExpress.XtraEditors.LabelControl lblInstr;
        private DevExpress.XtraEditors.LabelControl lblPrDescr;
        private DevExpress.XtraEditors.LabelControl lblPrNumber;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblNetPrc;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
    }
}