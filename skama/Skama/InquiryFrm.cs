﻿using DevExpress.XtraGrid.Views.Grid;
using Skama.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TecOrder.Dtos;

namespace Skama
{
    public partial class InquiryFrm : Form
    {
        private ResponseProduct _product;
        private EdiConfiguration _uac;

        public InquiryFrm()
        {
            InitializeComponent();
        }

        public InquiryFrm(ResponseProduct product, EdiConfiguration uac)
        {
            _uac = uac;
            _product = product;
            InitializeComponent();
        }

        private void InquiryFrm_Load(object sender, EventArgs e)
        {
            lblNetPrice.Text = string.Empty;
            lblListPrice.Text = string.Empty;
            lblProductNumber.Text = _product.ProductNumber;
            var descr = _product.ProductDescriptions.LastOrDefault(x => x.Language.ToLower() == "en")?.Description;
            if (!string.IsNullOrEmpty(descr))
            {
                lblProductDescr.Text = descr;
            }
            lblItemInstruction.Text = _product.ItemInstruction;
            lblResponseQuantity.Text = $"{_product.ResponseQuantity} {_product.ResponseQuantityUom}";

            if (_product.Responses.Any())
            {
                lblResponseText.Visible = true;
                lblResponseText.Text = _product.Responses.First().Text;
            }

            if (_uac.PriceUac == 1 && _product.Prices.Any())
            {
                lblNetPrice.Text = _product.Prices.FirstOrDefault(x => x.PriceCategory == "NetPrice")?.PriceStr;
                lblListPrice.Text = _product.Prices.FirstOrDefault(x => x.PriceCategory == "ListPrice")?.PriceStr;
            }

            if (_product.AlternativeProducts.Any())
            {
                var altProducts = new List<AltProduct>();
                foreach (var product in _product.AlternativeProducts)
                {
                    altProducts.Add(new AltProduct
                    {
                        Code = product.ProductNumber,
                        Name = product.ProductDescriptions.LastOrDefault(x => x.Language == "EN")?.Description,
                        Ean = product.Ean,
                        PriceCategory = product.Price?.PriceCategory,
                        PriceValue = product.Price?.PriceStr
                    });
                }
                gridControl1.DataSource = altProducts;
                gridView1.BestFitColumns();
                gridView1.OptionsBehavior.ReadOnly = true;
            }

        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (!(sender is GridView view))
            {
                return;
            }
            if (_uac.PriceUac == 0)
            {
                return;
            }
            var item = (AltProduct)view.GetRow(e.FocusedRowHandle);
            if (item != null)
            {
                lblNetPrice.Text = item.PriceValue;
                lblListPrice.Text = string.Empty;
            }
        }
    }
}
