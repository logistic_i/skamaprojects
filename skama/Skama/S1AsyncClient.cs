﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skama.Models;
using Softone;

namespace Skama
{
    public class S1AsyncClient
    {
        private XSupport _xSupport = null;
        private S1Tools _tools = null;
        private List<OrderBranch> _orderBranches;

        public S1AsyncClient()
        {

        }

        public S1AsyncClient(XSupport xSupport)
        {
            _xSupport = xSupport;
            _tools = new S1Tools(_xSupport);
            _orderBranches = GetOrderBranches(13);
        }

        internal void PostDocs(List<AutomationDoc> docs, int docWhouse, int findoc)
        {
            foreach (var doc in docs)
            {
                try
                {
                    using (var _xModule = _xSupport.CreateModule("SALDOC"))
                    {
                        _xModule.LocateData(findoc);
                        HideWarningsFromS1Module(_xModule);
                        var thisBranchSeries = _tools.GetIdFromMemoryTable("SERIES", "SOSOURCE;FPRMS;WHOUSE", "SERIES", 1351, 10146, docWhouse);//Παραγγελία Πελάτη με κάλυψη απο ενδοδιακίνηση
                        var whouseSec = doc.Data.First().Whouse;
                        var branchSec = _tools.GetIdFromMemoryTable("SERIES", "SOSOURCE;FPRMS;WHOUSE", "BRANCH", 1351, 10144, whouseSec);//Παραγγελία ενδοδιακίνησης προς άλλο υποκατάστημα

                        if (doc.isMantis)
                        {
                            thisBranchSeries = _tools.GetIdFromMemoryTable("SERIES", "SOSOURCE;FPRMS;WHOUSE", "SERIES", 1351, 10143, docWhouse); //Παραγγελία προς Mantis 
                        }

                        var data = new
                        {
                            SALDOC = new
                            {
                                TRNDATE = Convert.ToDateTime(_xModule.GetTable("SALDOC").Current["TRNDATE"]).ToString("yyyy-MM-dd"),
                                SERIES = thisBranchSeries,
                                TRDR = Convert.ToInt32(_xModule.GetTable("SALDOC").Current["TRDR"]),
                                TRDBRANCH = _xModule.GetTable("SALDOC").Current["TRDBRANCH"] != DBNull.Value
                                                        ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["TRDBRANCH"])
                                                        : (int?)null,
                                PRIORITY = _xModule.GetTable("SALDOC").Current["PRIORITY"] != DBNull.Value
                                                        ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["PRIORITY"])
                                                        : (int?)null,
                                COMMENTS = _xModule.GetTable("SALDOC").Current["COMMENTS"],
                                CCCINFOORDER = _xModule.GetTable("SALDOC").Current["CCCINFOORDER"] != DBNull.Value
                                                        ? Convert.ToInt32(_xModule.GetTable("SALDOC").Current["CCCINFOORDER"])
                                                        : (int?)null,
                                CONVMODE = 1
                            },
                            MTRDOC = new
                            {
                                SOCARRIER = _xModule.GetTable("MTRDOC").Current["SOCARRIER"] != DBNull.Value
                                                    ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["SOCARRIER"])
                                                    : (int?)null,
                                ROUTING = _xModule.GetTable("MTRDOC").Current["ROUTING"] != DBNull.Value
                                                    ? Convert.ToInt32(_xModule.GetTable("MTRDOC").Current["ROUTING"])
                                                    : (int?)null,
                                BRANCHSEC = !doc.isMantis ? branchSec : (int?)null
                            },
                            ITELINES = doc.Data.Select(x => new
                            {
                                MTRL = x.Mtrl,
                                QTY1 = x.Qty1,
                                PRICE = x.Price,
                                DISC1PRC = x.Disc1Prc > 0 ? x.Disc1Prc : null,
                                DISC2PRC = x.Disc2Prc > 0 ? x.Disc2Prc : null,
                                DISC3PRC = x.Disc3Prc > 0 ? x.Disc3Prc : null,
                                COMMENTS = !string.IsNullOrEmpty(x.Comments) ? x.Comments : null,
                                FINDOCS = x.Findocs,
                                MTRLINESS = x.Mtrliness,
                                CCCRELDOCS = !string.IsNullOrEmpty(x.Reldocs)
                                ? $@"{x.Reldocs}|{x.Findocs};{x.Mtrliness}"
                                : $@"{x.Findocs};{ x.Mtrliness}"
                            }).ToList()
                        };

                        dynamic paramss = new ExpandoObject();
                        paramss.ISCUSTOM = 1;
                        SetDataResponse res = null;
                        lock (_xModule) //lock (_xSupport)
                        {
                            res = _tools.PostDocByWebMethod(_xModule.ObjName, "", data, paramss);
                        }
                        if (res != null && res.Success)
                        {
                            if (!doc.isMantis) //Καταχώριση παραστατικού παραγγελίας ενδοδιακίνησης προς άλλο υποκατάστημα
                            {
                                var receiver = docWhouse;
                                var sender = whouseSec;
                                var nextStation = GetNextStationByStockFlow(receiver, sender);
                                if (nextStation == sender)
                                {
                                    nextStation = receiver;
                                }
                                var orderBranch = _orderBranches.Where(x => x.BranchId == nextStation).FirstOrDefault()?.Trdr;
                                var seriesForOtherBranch = _tools.GetIdFromMemoryTable("SERIES", "SOSOURCE;FPRMS;WHOUSE", "SERIES", 1351, 10144, whouseSec);
                                var id = Convert.ToInt32(res.Id);
                                RemoveReservedQuantities(id); //Προσωρινή άρση δεσμεύσεων
                                using (var Saldoc = _xSupport.CreateModule("SALDOC"))
                                {
                                    Saldoc.LocateData(id);
                                    PostEndoDocToOtherBranch(Saldoc, orderBranch, seriesForOtherBranch);
                                }
                            }
                        }
                        else
                        {
                            var errorMsg = res.Error;
                            AppendToLogTable(_xModule, errorMsg);
                        }
                    }
                }
                catch (Exception ex)
                {
                    var errorMsg = ex.Message;
                    var stackTrace = ex.StackTrace;
                    //AppendToLogTable(errorMsg, stackTrace);
                }
            }
        }

        private void PostEndoDocToOtherBranch(XModule saldoc, int? orderBranch, int seriesForOtherBranch)
        {
            try
            {
                var initFindoc = Convert.ToInt32(saldoc.GetTable("SALDOC").Current["FINDOC"]);
                var lines = new List<FindocItem>();
                var itelinesTmp = saldoc.GetTable("ITELINES").CreateDataTable(true).AsEnumerable().ToList();
                foreach (var row in itelinesTmp)
                {
                    lines.Add(new FindocItem
                    {
                        Mtrl = Convert.ToInt32(row["MTRL"].ToString()),
                        Qty1 = Convert.ToDouble(row["QTY1"].ToString()),
                        Price = Convert.ToDouble(row["PRICE"].ToString()),
                        Disc1Prc = row["DISC1PRC"] != DBNull.Value ? Convert.ToDouble(row["DISC1PRC"].ToString()) : (double?)null,
                        Disc2Prc = row["DISC2PRC"] != DBNull.Value ? Convert.ToDouble(row["DISC2PRC"].ToString()) : (double?)null,
                        Disc3Prc = row["DISC3PRC"] != DBNull.Value ? Convert.ToDouble(row["DISC3PRC"].ToString()) : (double?)null,
                        Comments = row["COMMENTS"].ToString(),
                        Findocs = Convert.ToInt32(row["FINDOC"].ToString()),
                        Mtrliness = Convert.ToInt32(row["MTRLINES"].ToString()),
                        Reldocs = row["CCCRELDOCS"].ToString(),
                        InitFindoc = initFindoc
                    });
                }

                var endoData = new
                {
                    SALDOC = new
                    {
                        TRNDATE = Convert.ToDateTime(saldoc.GetTable("SALDOC").Current["TRNDATE"]).ToString("yyyy-MM-dd"),
                        SERIES = seriesForOtherBranch,
                        TRDR = orderBranch,
                        COMMENTS = saldoc.GetTable("SALDOC").Current["COMMENTS"]
                        //CONVMODE = 1
                    },
                    MTRDOC = new
                    {
                        BILLTRDR = Convert.ToInt32(saldoc.GetTable("SALDOC").Current["TRDR"]),
                        BILLTRDBRANCH = saldoc.GetTable("SALDOC").Current["TRDBRANCH"] != DBNull.Value
                            ? Convert.ToInt32(saldoc.GetTable("SALDOC").Current["TRDBRANCH"])
                            : (int?)null,
                        BRANCHSEC = Convert.ToInt32(saldoc.GetTable("SALDOC").Current["BRANCH"]),
                        CCCINITPARE = 1
                    },
                    ITELINES = lines.Select(x => new
                    {
                        MTRL = x.Mtrl,
                        QTY1 = x.Qty1,
                        PRICE = x.Price,
                        DISC1PRC = x.Disc1Prc > 0 ? x.Disc1Prc : null,
                        DISC2PRC = x.Disc2Prc > 0 ? x.Disc2Prc : null,
                        DISC3PRC = x.Disc3Prc > 0 ? x.Disc3Prc : null,
                        COMMENTS = !string.IsNullOrEmpty(x.Comments) ? x.Comments : null,
                        FINDOCS = x.Findocs,
                        MTRLINESS = x.Mtrliness,
                        CCCRELDOCS = !string.IsNullOrEmpty(x.Reldocs)
                        ? $@"{x.Reldocs}|{x.Findocs};{x.Mtrliness}"
                        : $@"{x.Findocs};{ x.Mtrliness}",
                        CCCINITFINDOC = x.InitFindoc != null ? x.InitFindoc : null
                    }).ToList()
                };

                dynamic paramss = new ExpandoObject();
                paramss.ISCUSTOM = 1;
                if (!_xSupport.ConnectionInfo.IsAdministrator)
                {
                    using (var autoSupport = LoginWithAutomationUser(_xSupport))
                    {
                        var autoTools = new S1Tools(autoSupport);
                        SetDataResponse result = null;
                        result = autoTools.PostDocByWebMethod(saldoc.ObjName, "", endoData, paramss);
                        if (result != null && !result.Success)
                        {
                            var errorMsg = result.Error;
                            AppendToLogTable(saldoc, errorMsg);
                        }
                    }                   
                }
                else
                {
                    SetDataResponse result = null;
                    _tools.PostDocByWebMethod(saldoc.ObjName, "", endoData, paramss);
                    if (result != null && !result.Success)
                    {
                        var errorMsg = result.Error;
                        AppendToLogTable(saldoc, errorMsg);
                    }
                }
            }
            catch (Exception ex)
            {
                var test = saldoc.GetTable("SALDOC").Current["TRNDATE"];
                var errorMsg = ex.Message;
                var stackTrace = ex.StackTrace;
                AppendToLogTable(saldoc, errorMsg, stackTrace);
            }
        }

        private int GetNextStationByStockFlow(int receiver, int sender)
        {
            var query = $@"SELECT TOP 1 ISNULL(M.BRANCH,D.BRANCH) AS BRANCH 
                            FROM CCCSTOCKFLOW H
                            INNER JOIN CCCSTOCKFLOWD D ON D.CCCSTOCKFLOW = H.CCCSTOCKFLOW
                            LEFT JOIN CCCSTOCKFLOWM M ON M.CCCSTOCKFLOW = D.CCCSTOCKFLOW AND M.CCCSTOCKFLOWD = D.CCCSTOCKFLOWD
                            WHERE D.BRANCH = {sender} AND H.BRANCH = {receiver}
                            ORDER BY M.SORTORDER ASC";

            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    return ds.GetAsInteger(0, "BRANCH");
                }
                else
                {
                    return 0;
                }
            }
        }

        private List<OrderBranch> GetOrderBranches(int sodtype)
        {
            var orderBranches = new List<OrderBranch>();
            var query = $"SELECT TRDR, CCCBRANCH FROM TRDR WHERE COMPANY = {_xSupport.ConnectionInfo.CompanyId} AND SODTYPE = {sodtype} AND ISACTIVE = 1 AND CCCBRANCH IS NOT NULL";
            using (var ds = _xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    for (int i = 0; i < ds.Count; i++)
                    {
                        orderBranches.Add(new OrderBranch
                        {
                            Trdr = ds.GetAsInteger(i, "TRDR"),
                            BranchId = ds.GetAsInteger(i, "CCCBRANCH")
                        });
                    }
                }
            }
            return orderBranches;
        }

        private void RemoveReservedQuantities(int findoc)
        {
            try
            {
                var q = $"UPDATE MTRLINES SET PENDING = 0, QTY1CANC = QTY1 WHERE FINDOC = {findoc}";
                _xSupport.ExecuteSQL(q, null);
            }
            catch (Exception ex)
            {
                var errorMsg = ex.Message;
                var stackTrace = ex.StackTrace;
                //AppendToLogTable(errorMsg, stackTrace);
            }
        }

        internal void AppendToLogTable(XModule _xModule, string errorMsg, string stackTrace = "")
        {
            try
            {
                var soft1User = Convert.ToInt32(_xModule.GetTable("FINDOC").Current["INSUSER"]);
                var findoc = Convert.ToInt32(_xModule.GetTable("FINDOC").Current["FINDOC"]);
                var logDate = DateTime.Now;
                using (var LogObj = _xSupport.CreateModule("CCCLOGOBJ"))
                {
                    _tools.HideWarningsFromS1Module(LogObj);
                    LogObj.InsertData();
                    LogObj.GetTable("CCCCUSTOMLOGS").Current["SOFT1USER"] = soft1User;
                    LogObj.GetTable("CCCCUSTOMLOGS").Current["FINDOC"] = findoc;
                    LogObj.GetTable("CCCCUSTOMLOGS").Current["ERRORMSG"] = errorMsg;
                    if (!string.IsNullOrEmpty(stackTrace))
                    {
                        LogObj.GetTable("CCCCUSTOMLOGS").Current["STACKTRACE"] = stackTrace;
                    }
                    LogObj.GetTable("CCCCUSTOMLOGS").Current["LOGDATE"] = logDate;
                    LogObj.PostData();
                }
            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }

        private XSupport LoginWithAutomationUser(XSupport xSupport)
        {
            XSupport autoSupport = null;
            var query = "SELECT TOP 1 C.XCONAME, U.CODE AS USERNAME, C.PASSWORD FROM USERS U INNER JOIN CCCGKISEUSER C ON C.USERS = U.USERS";
            using (var ds = xSupport.GetSQLDataSet(query, null))
            {
                if (ds.Count > 0)
                {
                    var xcoName = ds.GetAsString(0, "XCONAME");
                    var username = ds.GetAsString(0, "USERNAME");
                    var password = ds.GetAsString(0, "PASSWORD");
                    var company = xSupport.ConnectionInfo.CompanyId;
                    var branch = xSupport.ConnectionInfo.BranchId;
                    try
                    {
                        autoSupport = XSupport.Login(xcoName, username, password, company, branch, xSupport.ConnectionInfo.LoginDate);
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            return autoSupport;
        }

        internal void HideWarningsFromS1Module(XModule XModule)
        {
            object otherModule = _xSupport.GetStockObj("ModuleIntf", true);
            object[] myArray1;
            myArray1 = new object[3];
            myArray1[0] = XModule.Handle;
            myArray1[1] = "WARNINGS";
            myArray1[2] = "OFF";
            _xSupport.CallPublished(otherModule, "SetParamValue", myArray1);
        }
    }
}
