﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softone;
using System.Windows.Forms;


namespace CreditCal
{
    [WorksOn("CCCTMPCREDIT")]
    public class CreditResults:TXCode
    {
        public override void Initialize()
        {
            XModule.SetEvent("ON_CCCTMPCREDITH_CRDAMNT", On_CccTmpCreditH_CrdAmnt);
            XModule.SetEvent("ON_CCCTMPCREDITD_AFTERPOST", On_CccTmpCreditD_AfterPost);
            XModule.SetEvent("ON_CCCTMPCREDITD_CRDAMNT", On_CccTmpCreditD_CrdAmnt);
            XModule.SetEvent("ON_CCCTMPCREDITD_CRDAMNT_VALIDATE", On_CccTmpCreditD_CrdAmnt_Validate);
            XModule.FieldColor("CCCTMPCREDITD.CRDAMNT1", 16777164);
            XModule.FieldColor("CCCTMPCREDITDGROUPED.CRDAMNT1", 16777164);
            XModule.FieldColor("CCCTMPCREDITD.CRDAMNT", 13434828);
            XModule.FieldColor("CCCTMPCREDITDGROUPED.CRDAMNT", 13434828);
            XModule.FieldColor("CCCTMPCREDITDGROUPED.CRDAMNTOVER", 13434879);
            XModule.FieldColor("CCCTMPCREDITH.CRDAMNTFINAL", 13434828);
            base.Initialize();
        }

        //public int changeH;
        public int changeInLine;
        public int changeVal;
        public double CrdAmntHOld;

        public override void BeforeDelete()
        {
            var TmpCreditH = XModule.GetTable("CCCTMPCREDITH");
            var TmpCreditD = XModule.GetTable("CCCTMPCREDITD");
            //var findoc = Convert.ToInt32(TmpCreditH.Current["FINDOC"]);
            //var TmpCreditHId = Convert.ToInt32(TmpCreditH.Current["CCCTMPCREDITH"]);
            var paramsList = XModule.Params;
            var selrecs = paramsList.FirstOrDefault(x => x.Contains("SELRECS")).Replace("?", ",");
            if (!string.IsNullOrEmpty(selrecs))
            {
                //Πιστωτικό Παραστατικό
                selrecs = selrecs.Replace("SELRECS=", "");
                var queryrecs = $@"SELECT CCCTMPCREDITH.FINDOC,FINDOC.FINCODE FROM CCCTMPCREDITH 
                                    INNER JOIN FINDOC ON CCCTMPCREDITH.FINDOC=FINDOC.FINDOC 
                                    WHERE CCCTMPCREDITH.FINDOC>0 AND {selrecs}";
                using (XTable dsrecs = XSupport.GetSQLDataSet(queryrecs))
                {
                    if (dsrecs.Count > 0)
                    {
                        for (int i = 0; i < dsrecs.Count; i++)
                        {
                            XSupport.Exception($"Δεν μπορείτε να διαγράψετε τα επιλεγμένα αποτελέσματα πιστωτικών διότι έχει δημιουργηθεί Πιστωτικό Παραστατικό ({dsrecs.GetAsString(i, "FINCODE")}).");
                        }
                    }
                }
            }
            base.BeforeDelete();
        }

        private void On_CccTmpCreditD_AfterPost(object Sender, XEventArgs e)
        {
            var TmpCreditH = XModule.GetTable("CCCTMPCREDITH");
            var TmpCreditD = XModule.GetTable("CCCTMPCREDITD");
            var TmpCreditHId = Convert.ToInt32(TmpCreditH.Current["CCCTMPCREDITH"]);
            var CrdFinAmntExc = Convert.ToDouble(TmpCreditD.Current["CRDFINAMNTEXC"]);
            var CrdAmnt1 = Convert.ToDouble(TmpCreditD.Current["CRDAMNT1"]);
            var NewCrdAmnt = Math.Round(CrdAmnt1, 2) + Math.Round(CrdFinAmntExc, 2);

            if (TmpCreditHId > 0 && TmpCreditD.Count > 0)
            {
                //if (CrdFinAmntExc == 0.0)
                if (NewCrdAmnt > 0.0 && CrdFinAmntExc == 0.0)
                {
                    var SumCrdAmnt = Math.Round(XModule.Sum("CCCTMPCREDITD.CRDAMNT"),2);
                    changeInLine = 1;
                    TmpCreditH.Current["CRDAMNT"] = SumCrdAmnt;
                }
                changeInLine = 0;
                //Συμπλήρωση Συγκεντρωτικών Αποτελεσμάτων Υπολογισμών
                Tools.GroupedCreditResults(XModule, XSupport, "TmpCredit");
                //Συμπλήρωση Αξία προς δημιουργία πιστ/κού
                Tools.CalculateFinalSumCreditVal(XModule, XSupport);
            }
        }

        private void On_CccTmpCreditD_CrdAmnt(object Sender, XEventArgs e)
        {
            var TmpCreditH = XModule.GetTable("CCCTMPCREDITH");
            var TmpCreditD = XModule.GetTable("CCCTMPCREDITD");
            var TmpCreditHId = Convert.ToInt32(TmpCreditH.Current["CCCTMPCREDITH"]);
            var CrdAmntH = Convert.ToDouble(TmpCreditD.Current["CRDAMNT"]); //Αξία πιστ/κού γραμμής

            var CrdFinAmntExc = Convert.ToDouble(TmpCreditD.Current["CRDFINAMNTEXC"]);
            var CrdAmnt1 = Convert.ToDouble(TmpCreditD.Current["CRDAMNT1"]);
            var NewCrdAmnt = Math.Round(CrdAmnt1, 2) + Math.Round(CrdFinAmntExc, 2);
            if (TmpCreditHId > 0 && TmpCreditD.Count > 0)
            {
                //if (CrdFinAmntExc != 0.0 && CrdAmntH != CrdAmntHOld)
                if (NewCrdAmnt < 0.0 && CrdFinAmntExc != 0.0  && CrdAmntH != CrdAmntHOld)
                {
                    XSupport.Warning("Δεν μπορείτε να αλλάξετε την αξία του είδη καταχωρημένου πιστωτικού.");
                    changeVal = 1;
                    TmpCreditD.Current["CRDAMNT"] = CrdAmntHOld;
                    CrdAmntHOld = 0.0;
                }
                changeVal = 0;
            }
        }

        private void On_CccTmpCreditD_CrdAmnt_Validate(object Sender, XEventArgs e)
        {
            var TmpCreditH = XModule.GetTable("CCCTMPCREDITH");
            var TmpCreditHId = Convert.ToInt32(TmpCreditH.Current["CCCTMPCREDITH"]);
            var TmpCreditD = XModule.GetTable("CCCTMPCREDITD");
            if (TmpCreditHId > 0 && TmpCreditD.Count > 0)
            {
                if (changeVal == 0)
                {
                    CrdAmntHOld = Convert.ToDouble(TmpCreditD.Current["CRDAMNT"]);
                }
            }
        }

        private void On_CccTmpCreditH_CrdAmnt(object Sender, XEventArgs e)
        {
            //Θα εκτελείτε όταν αλλάζουμε την Αξία του πιστωτικού στο header και όχι όταν αλλάζουμε την Αξία του πιστωτικού στις γραμμές.
            if (changeInLine == 0)
            {
                var TmpCreditH = XModule.GetTable("CCCTMPCREDITH");
                var TmpCreditD = XModule.GetTable("CCCTMPCREDITD");
                var TmpCreditHId = Convert.ToInt32(TmpCreditH.Current["CCCTMPCREDITH"]);
                var CrdAmntH = Convert.ToDouble(TmpCreditH.Current["CRDAMNT"]); //Αξία πιστ/κού
                var CrdAmntH1 = Convert.ToDouble(TmpCreditH.Current["CRDAMNT1"]); //Αξία υπολ/ σμού
                var SumCrdFinAmntExc = XModule.Sum("CCCTMPCREDITD.CRDFINAMNTEXC");
                var SumCrdAmnt = XModule.Sum("CCCTMPCREDITD.CRDAMNT");
                var SumCrdAmnt1 = XModule.Sum("CCCTMPCREDITD.CRDAMNT1");
                //τα num01 και num02 τα χρησιμοποιώ για υπολογισμούς.
                var num01 = SumCrdFinAmntExc;
                // το case στο num01 γίνεται για να δούμε αν υπάρχουν πιστωτικά αλλιώς 0
                var num02 = CrdAmntH - (num01 < 0 ? num01 : 0.0);

                if (CrdAmntH > 0 && TmpCreditHId > 0 && TmpCreditD.Count > 0)
                {
                    var repeats = TmpCreditD.Count;
                    for (int i = 0; i < repeats; i++)
                    {
                        if (TmpCreditD.GetAsFloat(i, "CRDFINAMNTEXC") == 0.0)
                        {
                            //var NewCrdAmntH = Math.Round(((CrdAmntH * TmpCreditD.GetAsFloat(i, "CRDAMNT1")) / CrdAmntH1), 2);
                            var NewCrdAmntH = Math.Round(((num02 * TmpCreditD.GetAsFloat(i, "CRDAMNT1")) / CrdAmntH1), 2);
                            TmpCreditD.Current.Edit(i);
                            TmpCreditD.Current["CRDAMNT"] = NewCrdAmntH;
                            TmpCreditD.Current.Post();
                        }
                    }
                    //Συμπλήρωση Συγκεντρωτικών Αποτελεσμάτων Υπολογισμών
                    Tools.GroupedCreditResults(XModule, XSupport, "TmpCredit");
                    //Συμπλήρωση Αξία προς δημιουργία πιστ/κού
                    Tools.CalculateFinalSumCreditVal(XModule, XSupport);
                }
            }
        }

        public override object ExecCommand(int Cmd)
        {
            if (Cmd == 20201002) //Δημιουργία Πιστωτικών
            {
                var paramsList = XModule.Params;
                var selrecs = paramsList.FirstOrDefault(x => x.Contains("SELRECS")).Replace("?",",");
                if (!string.IsNullOrEmpty(selrecs))
                {
                    selrecs = selrecs.Replace("SELRECS=", "");
                    var queryrecs = $@"SELECT CCCTMPCREDITH.*,CCCCREDITDATAH.NAME,CCCCREDITDATAH.SERIESCREDIT FROM CCCTMPCREDITH 
                                        LEFT JOIN CCCCREDITDATAH ON CCCTMPCREDITH.PRCRULE=CCCCREDITDATAH.CCCCREDITDATAH WHERE {selrecs}";
                    using (XTable dsrecs = XSupport.GetSQLDataSet(queryrecs))
                    {
                        if (dsrecs.Count > 0)
                        {
                            for (int i = 0; i < dsrecs.Count; i++)
                            {
                                var trdr = dsrecs.GetAsInteger(i, "TRDR");
                                var trdrBranch = dsrecs.GetAsInteger(i, "TRDBRANCH");
                                var TmpCreditHId = dsrecs.GetAsInteger(i, "CCCTMPCREDITH");
                                var seriesCredit = dsrecs.GetAsInteger(i, "SERIESCREDIT");
                                var rulename = dsrecs.GetAsString(i, "NAME");
                                //δημιουργημένου πιστωτικού παραστατικού 
                                var findocCredit = dsrecs.GetAsInteger(i, "FINDOC");
                                if (findocCredit == 0)
                                {
                                    if (seriesCredit > 0)
                                    {
                                        try
                                        {
                                            using (var SalDoc = XSupport.CreateModule("SALDOC"))
                                            {
                                                S1Tools.HideWarningsFromS1Module(SalDoc, XSupport);
                                                SalDoc.InsertData();
                                                SalDoc.GetTable("FINDOC").Current["SERIES"] = seriesCredit;
                                                SalDoc.GetTable("FINDOC").Current["TRDR"] = trdr;
                                                //SalDoc.GetTable("MTRDOC").Current["DELIVDATE"] = (DateTime?)null;
                                                if (trdrBranch > 0)
                                                {
                                                    SalDoc.GetTable("FINDOC").Current["TRDBRANCH"] = trdrBranch;
                                                }
                                                var queryLines = $@"SELECT * FROM CCCTMPCREDITD WHERE ISGROUPED = 1 AND ISNULL(CRDAMNT,0)>0 AND CCCTMPCREDITH={TmpCreditHId}";
                                                using (XTable dsLines = XSupport.GetSQLDataSet(queryLines))
                                                {
                                                    if (dsLines.Count > 0)
                                                    {
                                                        using (var Itelines = SalDoc.GetTable("ITELINES"))
                                                        {
                                                            for (int x = 0; x < dsLines.Count; x++)
                                                            {
                                                                Itelines.Current.Append();
                                                                Itelines.Current["MTRL"] = dsLines.GetAsInteger(x, "MTRL");
                                                                Itelines.Current["QTY1"] = dsLines.GetAsFloat(x, "QTY1");//(double)1;
                                                                Itelines.Current["PRICE"] = Math.Round((dsLines.GetAsFloat(x, "CRDAMNT") / dsLines.GetAsFloat(x, "QTY1")), 2);
                                                                Itelines.Current["LINEVAL"] = dsLines.GetAsFloat(x, "CRDAMNT");
                                                                Itelines.Current.Post();
                                                            }
                                                        }
                                                    }
                                                }
                                                var fincodecredit = "";
                                                var fincode = Convert.ToString(SalDoc.GetTable("FINDOC").Current["FINCODE"]);
                                                var cmpfincode = Convert.ToString(SalDoc.GetTable("FINDOC").Current["CMPFINCODE"]);
                                                if(fincode!="" && cmpfincode=="")
                                                {
                                                    fincodecredit = fincode;
                                                }
                                                if (fincode == "" && cmpfincode != "")
                                                {
                                                    fincodecredit = cmpfincode;
                                                }
                                                if (fincode != "" && cmpfincode != "")
                                                {
                                                    fincodecredit = fincode;
                                                }

                                                var findoc = SalDoc.PostData();
                                                //Σύνδεση δημιουργημένου πιστωτικού παραστατικού με τα αποτελέσματα πιστωτικών
                                                XSupport.ExecuteSQL($@"UPDATE CCCTMPCREDITH SET FINDOC={findoc},SOCRD=1 WHERE CCCTMPCREDITH={TmpCreditHId}");
                                                MessageBox.Show("Δημιουργήθηκε το πιστωτικό παραστατικό «"+ fincodecredit + "».", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            MessageBox.Show(ex.Message);
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Για να δημιουργήσετε ένα πιστωτικό παραστατικό ,θα πρέπει στο σενάριο «" + rulename + "» να είναι συμπληρωμένη η «Σειρά Έκδοσης Πιστωτικού».", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Υπάρχει δημιουργημένο πιστωτικό για τα επιλεγμένα αποτελέσματα πιστωτικών.", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                }
                            }
                        }
                    }
                }
            }

            if (Cmd == 20201214) //Διαγραφή Αποτελεσμάτων
            {
                var TmpCreditH = XModule.GetTable("CCCTMPCREDITH");
                var TmpCreditD = XModule.GetTable("CCCTMPCREDITD");
                var paramsList = XModule.Params;
                var selrecs = paramsList.FirstOrDefault(x => x.Contains("SELRECS")).Replace("?", ",");
                if (!string.IsNullOrEmpty(selrecs))
                {
                    //Πιστωτικό Παραστατικό
                    selrecs = selrecs.Replace("SELRECS=", "");
                    var queryrecs = $@"SELECT CCCTMPCREDITH.FINDOC,FINDOC.FINCODE,CCCTMPCREDITH.CCCTMPCREDITH FROM CCCTMPCREDITH 
                                    left JOIN FINDOC ON CCCTMPCREDITH.FINDOC=FINDOC.FINDOC 
                                    WHERE {selrecs}";
                    using (XTable dsrecs = XSupport.GetSQLDataSet(queryrecs))
                    {
                        if (dsrecs.Count > 0)
                        {
                            var deletecounter = 0;
                            for (int i = 0; i < dsrecs.Count; i++)
                            {
                                if (dsrecs.GetAsInteger(i, "FINDOC") > 0)
                                {
                                    //XSupport.Exception($"Δεν μπορείτε να διαγράψετε τα επιλεγμένα αποτελέσματα πιστωτικών διότι έχει δημιουργηθεί Πιστωτικό Παραστατικό ({dsrecs.GetAsString(i, "FINCODE")}).");
                                }
                                else
                                {
                                    deletecounter = deletecounter + 1;
                                }
                            }
                            if(deletecounter>0)
                            {
                                XModule.Exec("Button:Delete");
                            }
                        }
                    }
                }
            }
            
            return base.ExecCommand(Cmd);
        }
    }
}
