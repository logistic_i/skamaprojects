﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softone;

namespace CreditCal
{
    [WorksOn("SALDOC")]
    public class Saldoc : TXCode
    {
        public override void AfterDelete()
        {
            var Series = Convert.ToInt32(XModule.GetTable("SALDOC").Current["SERIES"]);
            var SaldocId = Convert.ToInt32(XModule.GetTable("SALDOC").Current["FINDOC"]);
            var querycheck = $@"SELECT CCCTMPCREDITH,FINDOC FROM CCCTMPCREDITH WHERE FINDOC = {SaldocId}";
            using (XTable dscheck = XSupport.GetSQLDataSet(querycheck))
            {
                if (dscheck.Count > 0)
                {
                    for (int i = 0; i < dscheck.Count; i++)
                    {
                        var TmpCreditHId = dscheck.GetAsInteger(i, "CCCTMPCREDITH");
                        //Διαγραφή σύνδεσης δημιουργημένου πιστωτικού παραστατικού με τα αποτελέσματα πιστωτικών μετά την διαγραφή του!
                        XSupport.ExecuteSQL($@"UPDATE CCCTMPCREDITH SET FINDOC=NULL,SOCRD=0 WHERE CCCTMPCREDITH={TmpCreditHId} AND FINDOC ={SaldocId}");
                    }
                }
            }
            base.AfterDelete();
        }
    }
}
