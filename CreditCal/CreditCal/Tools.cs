﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Softone;

namespace CreditCal
{
    public class Tools
    {
        XModule _xModule;
        XSupport _xSupport;

        public Tools(XModule xModule, XSupport xSupport)
        {
            _xModule = xModule;
            _xSupport = xSupport;
        }

        public List<CreditDataH>GetRuleData(int rules)
        {
            var FinalRuleList = new List<CreditDataH>();
            //GET RULE DATA
            try
            {
                var creditdhlist = new List<CreditDataH>();
                var querycreditdh = $@"SELECT CCCCREDITDATAH,COMPANY,ISNULL(CREDITMODEL,0) AS CREDITMODEL,NAME,FROMDATE,FINALDATE,
                                      ISNULL(DIMTRDR,0) AS DIMTRDR,ISNULL(DIMMTRL,0) AS DIMMTRL,ISNULL(ANALTRDRGROUP,0) AS ANALTRDRGROUP,
                                      ISNULL(ANALMTRLGROUP,0) AS ANALMTRLGROUP,SODTYPETRDR,SODTYPEMTRL,
                                      ISNULL(TURNOVERPART,0) AS TURNOVERPART,ISNULL(CREDITPART,0) AS CREDITPART,ISNULL(SCALEFLD,0) AS SCALEFLD,
                                      SERIES,FPRMS,SERIESEXC,FPRMSEXC,isnull(SERIESCREDIT,0) AS SERIESCREDIT,SOSOURCE,CALCULBEH,ISNULL(APPFLD,0) AS APPFLD,ISNULL(SCALECHOICE,0) AS SCALECHOICE,
                                      ISNULL(SCALEEXEC,0) AS SCALEEXEC,ISNULL(CREDITTYPE,0) AS CREDITTYPE
                                      FROM CCCCREDITDATAH WHERE CCCCREDITDATAH={rules}";

                using (XTable dscreditdh = _xSupport.GetSQLDataSet(querycreditdh))
                {
                    if (dscreditdh.Count > 0)
                    {
                        for (int i = 0; i < dscreditdh.Count; i++)
                        {
                            var creditdh = new CreditDataH
                            {
                                CreditDataHId = dscreditdh.GetAsInteger(i, "CCCCREDITDATAH"),
                                Company = dscreditdh.GetAsInteger(i, "COMPANY"),
                                CreditModel = dscreditdh.GetAsInteger(i, "CREDITMODEL"),
                                Name = dscreditdh.GetAsString(i, "NAME"),
                                FromDate = dscreditdh.GetAsDateTime(i, "FROMDATE"),
                                FinalDate = dscreditdh.GetAsDateTime(i, "FINALDATE"),
                                DimTrdr = dscreditdh.GetAsInteger(i, "DIMTRDR"),
                                DimMtrl = dscreditdh.GetAsInteger(i, "DIMMTRL"),
                                AnalTrdrGroup = dscreditdh.GetAsInteger(i, "ANALTRDRGROUP"),
                                AnalMtrlGroup = dscreditdh.GetAsInteger(i, "ANALMTRLGROUP"),
                                SodtypeTrdr = dscreditdh.GetAsInteger(i, "SODTYPETRDR"),
                                SodtypeMtrl = dscreditdh.GetAsInteger(i, "SODTYPEMTRL"),
                                TurnOverPart = dscreditdh.GetAsInteger(i, "TURNOVERPART"),
                                CreditPart = dscreditdh.GetAsInteger(i, "CREDITPART"),
                                ScaleFld = dscreditdh.GetAsInteger(i, "SCALEFLD"),
                                Series = dscreditdh.GetAsString(i, "SERIES"),
                                Fprms = dscreditdh.GetAsString(i, "FPRMS"),
                                SeriesExc = dscreditdh.GetAsString(i, "SERIESEXC"),
                                FprmsExc = dscreditdh.GetAsString(i, "FPRMSEXC"),
                                SeriesCredit = dscreditdh.GetAsInteger(i, "SERIESCREDIT"),
                                Sosource = dscreditdh.GetAsInteger(i, "SOSOURCE"),
                                Calculbeh = dscreditdh.GetAsInteger(i, "CALCULBEH"),
                                AppFld = dscreditdh.GetAsInteger(i, "APPFLD"),
                                ScaleChoice = dscreditdh.GetAsInteger(i, "SCALECHOICE"),
                                ScaleExec = dscreditdh.GetAsInteger(i, "SCALEEXEC"),
                                CreditType = dscreditdh.GetAsInteger(i, "CREDITTYPE")
                            };
                            creditdhlist.Add(creditdh);
                        }
                    }
                }

                var creditddlist = new List<CreditDataD>();
                var querycreditdd = $@"SELECT CCCCREDITDATAD,CCCCREDITDATAH,A.COMPANY,A.SODTYPE,
                                        ISNULL(ENTITYID,0) AS ENTITYID,ISNULL(GROUPID,0) AS GROUPID,ISNULL(ANALGROUPID,0) AS ANALGROUPID,
                                        ISNULL(GOAL,0.0) AS GOAL,ISNULL(BONUS,0.0) AS BONUS,ISNULL(TURNOVERPART,0) AS TURNOVERPART,
                                        ISNULL(CREDITPART,0) AS CREDITPART,ISNULL(CCCCREDITDATADS,0) AS CCCCREDITDATADS,SOREDIR,ISNULL(SCALEMETHOD,0) AS SCALEMETHOD
                                        ,ISNULL(GROUPFIELD,'') AS GROUPFIELD ,B.NAME,B.DATATABLE,B.GROUPTABLE,ISNULL(C.GROUPFIELDID,0) AS GROUPFIELDID
                                        FROM CCCCREDITDATAD A
                                        LEFT JOIN CCCGROUPINGH B ON B.CCCGROUPINGH=A.GROUPID
                                        LEFT JOIN CCCGROUPINGD C ON C.CCCGROUPINGH=B.CCCGROUPINGH AND C.CCCGROUPINGD=ANALGROUPID
                                        WHERE CCCCREDITDATAH={rules}";
                using (XTable dscreditdd = _xSupport.GetSQLDataSet(querycreditdd))
                {
                    if (dscreditdd.Count > 0)
                    {
                        for (int i = 0; i < dscreditdd.Count; i++)
                        {
                            var creditdd = new CreditDataD
                            {
                                CreditDataHId = dscreditdd.GetAsInteger(i, "CCCCREDITDATAH"),
                                CreditDataDId = dscreditdd.GetAsInteger(i, "CCCCREDITDATAD"),
                                Company = dscreditdd.GetAsInteger(i, "COMPANY"),
                                Sodtype = dscreditdd.GetAsInteger(i, "SODTYPE"),
                                EntityId = dscreditdd.GetAsInteger(i, "ENTITYID"),
                                GroupId = dscreditdd.GetAsInteger(i, "GROUPID"),
                                AnalGrupId = dscreditdd.GetAsInteger(i, "ANALGROUPID"),
                                Goal = dscreditdd.GetAsFloat(i, "GOAL"),
                                Bonus = dscreditdd.GetAsFloat(i, "BONUS"),
                                TurnOverPart = dscreditdd.GetAsInteger(i, "TURNOVERPART"),
                                CreditPart = dscreditdd.GetAsInteger(i, "CREDITPART"),
                                CreditDataDs = dscreditdd.GetAsInteger(i, "CCCCREDITDATADS"),
                                Soredir = dscreditdd.GetAsInteger(i, "SOREDIR"),
                                ScaleMethod = dscreditdd.GetAsInteger(i, "SCALEMETHOD"),
                                GroupFieldId = dscreditdd.GetAsInteger(i, "GROUPFIELDID"),
                                GroupField = dscreditdd.GetAsString(i, "GROUPFIELD")
                            };
                            creditddlist.Add(creditdd);
                        }
                    }
                }

                var scalelist = new List<Scale>();
                var queryscale = $@"SELECT ISNULL(SCALEVALUE,0.0) AS SCALEVALUE,ISNULL(PRICE,0.0) AS PRICE,ISNULL(DISCPRC,0.0) AS DISCPRC,
                                    CCCCREDITDATAH,COMPANY,CCCCREDITDATAD,ISNULL(BONUS,0.0) AS BONUS
                                    FROM CCCSCALE WHERE CCCCREDITDATAH={rules}";
                using (XTable dsscale = _xSupport.GetSQLDataSet(queryscale))
                {
                    if (dsscale.Count > 0)
                    {
                        for (int i = 0; i < dsscale.Count; i++)
                        {
                            var scale = new Scale
                            {
                                Company = dsscale.GetAsInteger(i, "COMPANY"),
                                ScaleValue = dsscale.GetAsFloat(i, "SCALEVALUE"),
                                Price = dsscale.GetAsFloat(i, "PRICE"),
                                DiscPrc = dsscale.GetAsFloat(i, "DISCPRC"),
                                Bonus = dsscale.GetAsFloat(i, "BONUS"),
                                CreditDataHId = dsscale.GetAsInteger(i, "CCCCREDITDATAH"),
                                CreditDataDId = dsscale.GetAsInteger(i, "CCCCREDITDATAD")
                            };
                            scalelist.Add(scale);
                        }
                    }
                }

                if (creditdhlist.Count > 0)
                {
                    foreach (var h in creditdhlist)
                    {
                        var listcreditdd = creditddlist.Where(x => x.CreditDataHId == h.CreditDataHId).ToList();
                        if (listcreditdd != null)
                        {
                            h.CreditDataD = new List<CreditDataD>();
                            h.CreditDataD.AddRange(listcreditdd);
                        }
                    }
                }

                if (creditddlist.Count > 0)
                {
                    foreach (var d in creditddlist)
                    {
                        var listanalysis = creditddlist.Where(x => d.CreditDataDId == x.CreditDataDs).ToList();
                        if (listanalysis != null)
                        {
                            d.Analysis = new List<CreditDataD>();
                            d.Analysis.AddRange(listanalysis);
                        }

                        var listscale = scalelist.Where(x => x.CreditDataHId == d.CreditDataHId && x.CreditDataDId == d.CreditDataDId).ToList();
                        if (listscale != null)
                        {
                            d.Scale = new List<Scale>();
                            d.Scale.AddRange(listscale);
                        }
                    }
                }
                
                FinalRuleList = creditdhlist.Where(x => x.CreditDataD.Any(y => y.CreditDataDs == 0)).Select(x => new CreditDataH
                {
                    AnalMtrlGroup = x.AnalMtrlGroup,
                    AnalTrdrGroup = x.AnalTrdrGroup,
                    AppFld = x.AppFld,
                    Company = x.Company,
                    CreditDataD = x.CreditDataD.Where(z => z.CreditDataDs == 0).ToList(),
                    CreditDataHId = x.CreditDataHId,
                    CreditModel = x.CreditModel,
                    CreditPart = x.CreditPart,
                    CreditType = x.CreditType,
                    DimMtrl = x.DimMtrl,
                    DimTrdr = x.DimTrdr,
                    FinalDate = x.FinalDate,
                    FromDate = x.FromDate,
                    Name = x.Name,
                    ScaleChoice = x.ScaleChoice,
                    ScaleExec = x.ScaleExec,
                    ScaleFld = x.ScaleFld,
                    Series = x.Series,
                    Fprms = x.Fprms,
                    SeriesExc = x.SeriesExc,
                    FprmsExc = x.FprmsExc,
                    SeriesCredit = x.SeriesCredit,
                    Sosource = x.Sosource,
                    Calculbeh = x.Calculbeh,
                    SodtypeMtrl = x.SodtypeMtrl,
                    SodtypeTrdr = x.SodtypeTrdr,
                    TurnOverPart = x.TurnOverPart
                }).ToList();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return FinalRuleList;
        }

        public List<CashDataH> GetCashRuleData(int rules,int trdr,int payment)
        {
            var FinalRuleList = new List<CashDataH>();
            //GET RULE DATA
            try
            {
                var cashhlist = new List<CashDataH>();
                var querycashh = $@"SELECT CCCCASHPAYMENTH,COMPANY,NAME,FROMDATE,FINALDATE,SODTYPETRDR,isnull(SERIESCREDIT,0) AS SERIESCREDIT,
                                    ISNULL(CREDITTYPE,0) AS CREDITTYPE,SOSOURCE,SERIES,FPRMS,SERIESEXC,FPRMSEXC 
                                    FROM CCCCASHPAYMENTH WHERE CCCCASHPAYMENTH={rules}";
                using (XTable dscashh = _xSupport.GetSQLDataSet(querycashh))
                {
                    if (dscashh.Count > 0)
                    {
                        for (int i = 0; i < dscashh.Count; i++)
                        {
                            var cashh = new CashDataH
                            {
                                CashDataHId = dscashh.GetAsInteger(i, "CCCCASHPAYMENTH"),
                                Company = dscashh.GetAsInteger(i, "COMPANY"),
                                Name = dscashh.GetAsString(i, "NAME"),
                                FromDate = dscashh.GetAsDateTime(i, "FROMDATE"),
                                FinalDate = dscashh.GetAsDateTime(i, "FINALDATE"),
                                SodtypeTrdr = dscashh.GetAsInteger(i, "SODTYPETRDR"),
                                Series = dscashh.GetAsString(i, "SERIES"),
                                Fprms = dscashh.GetAsString(i, "FPRMS"),
                                SeriesExc = dscashh.GetAsString(i, "SERIESEXC"),
                                FprmsExc = dscashh.GetAsString(i, "FPRMSEXC"),
                                SeriesCredit = dscashh.GetAsInteger(i, "SERIESCREDIT"),
                                Sosource = dscashh.GetAsInteger(i, "SOSOURCE"),
                                CreditType = dscashh.GetAsInteger(i, "CREDITTYPE")
                            };
                            cashhlist.Add(cashh);
                        }
                    }
                }

                var cashdlist = new List<CashDataD>();
                var trdrFilter = "";
                var paymentFilter = "";
                if (trdr > 0)
                {
                    trdrFilter = $" AND Q.ENTITYID ={trdr} ";
                }
                else
                {
                    trdrFilter = "";
                }
                if (payment > 0)
                {
                    paymentFilter = $" OR Q.PAYMENT ={payment} ";
                }
                else
                {
                    paymentFilter = "";
                }
                var querycashd = $@"SELECT * FROM ( SELECT CCCCASHPAYMENTD,CCCCASHPAYMENTH,COMPANY,SODTYPE,
                                        CASE WHEN ISNULL(SODTYPE,0)<>0 THEN ENTITYID END AS ENTITYID,
                                        CASE WHEN ISNULL(SODTYPE,0)=0 THEN ENTITYID END AS PAYMENT,
                                        SOREDIR,SCALEMETHOD,CCCCASHPAYMENTDS 
                                        FROM CCCCASHPAYMENTD ) Q WHERE Q.CCCCASHPAYMENTH={rules} {trdrFilter} {paymentFilter}";
                using (XTable dscashd = _xSupport.GetSQLDataSet(querycashd))
                {
                    if (dscashd.Count > 0)
                    {
                        for (int i = 0; i < dscashd.Count; i++)
                        {
                            var cashd = new CashDataD
                            {
                                CashDataHId = dscashd.GetAsInteger(i, "CCCCASHPAYMENTH"),
                                CashDataDId = dscashd.GetAsInteger(i, "CCCCASHPAYMENTD"),
                                Company = dscashd.GetAsInteger(i, "COMPANY"),
                                Sodtype = dscashd.GetAsInteger(i, "SODTYPE"),
                                EntityId = dscashd.GetAsInteger(i, "ENTITYID"),
                                Payment = dscashd.GetAsInteger(i, "PAYMENT"),
                                CashDataDs = dscashd.GetAsInteger(i, "CCCCASHPAYMENTDS"),
                                Soredir = dscashd.GetAsInteger(i, "SOREDIR"),
                                ScaleMethod = dscashd.GetAsInteger(i, "SCALEMETHOD"),
                            };
                            cashdlist.Add(cashd);
                        }
                    }
                }

                var cashscalelist = new List<CashScale>();
                var querycashscale = $@"SELECT CCCCASHPAYMENTH,CCCCASHPAYMENTD,COMPANY,
                                    ISNULL(SCALEVALUE,0.0) AS SCALEVALUE,ISNULL(PRICE,0.0) AS PRICE,ISNULL(BONUS,0.0) AS BONUS
                                    FROM CCCCASHPAYMENTSCALE WHERE CCCCASHPAYMENTH={rules}";
                using (XTable dscashscale = _xSupport.GetSQLDataSet(querycashscale))
                {
                    if (dscashscale.Count > 0)
                    {
                        for (int i = 0; i < dscashscale.Count; i++)
                        {
                            var cashscale = new CashScale
                            {
                                Company = dscashscale.GetAsInteger(i, "COMPANY"),
                                ScaleValue = dscashscale.GetAsFloat(i, "SCALEVALUE"),
                                Price = dscashscale.GetAsFloat(i, "PRICE"),
                                Bonus = dscashscale.GetAsFloat(i, "BONUS"),
                                CashDataHId = dscashscale.GetAsInteger(i, "CCCCASHPAYMENTH"),
                                CashDataDId = dscashscale.GetAsInteger(i, "CCCCASHPAYMENTD")
                            };
                            cashscalelist.Add(cashscale);
                        }
                    }
                }

                if (cashhlist.Count > 0)
                {
                    foreach (var h in cashhlist)
                    {
                        var listcashd = cashdlist.Where(x => x.CashDataHId == h.CashDataHId && x.EntityId !=0).ToList();
                        if (listcashd != null)
                        {
                            h.TrdrList = new List<CashDataD>();
                            h.TrdrList.AddRange(listcashd);
                        }

                        if (listcashd.Count > 0)
                        {
                            foreach (var p in listcashd)
                            {
                                var paymentlist = cashdlist.Where(x => x.CashDataHId == p.CashDataHId && x.EntityId == 0 && p.CashDataDId == x.CashDataDs).ToList();
                                if (paymentlist != null)
                                {
                                    p.PaymentList = new List<CashDataD>();
                                    p.PaymentList.AddRange(paymentlist);
                                }

                                if(paymentlist.Count>0)
                                {
                                    foreach(var s in paymentlist)
                                    {
                                        var listscale = cashscalelist.Where(x => x.CashDataHId == s.CashDataHId && x.CashDataDId == s.CashDataDId).ToList();
                                        if (listscale != null)
                                        {
                                            s.Scale = new List<CashScale>();
                                            s.Scale.AddRange(listscale);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                FinalRuleList = cashhlist;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return FinalRuleList;
        }

        internal static void CalculateFinalSumCreditVal(XModule XModule, XSupport XSupport)
        {
            var TmpCreditH = XModule.GetTable("CCCTMPCREDITH");
            var TmpCreditDlines = XModule.GetTable("CCCTMPCREDITDGROUPED");
            if (TmpCreditDlines.Count > 0)
            {
                var sumCreditVal = 0.0;
                var repeats = TmpCreditDlines.Count;
                for (int i = 0; i < repeats; i++)
                {
                    //if (TmpCreditDlines.GetAsFloat(i, "CRDFINAMNTEXC") == 0.0)
                    //{
                        sumCreditVal = sumCreditVal + Math.Round(TmpCreditDlines.GetAsFloat(i, "CRDAMNT"),2);
                    //}
                }
                TmpCreditH.Current["CRDAMNTFINAL"] = Math.Round(sumCreditVal,2);
            }
        }

        internal static void CalculateFinalSumCashVal(XModule XModule, XSupport XSupport)
        {
            var TmpCashH = XModule.GetTable("CCCTMPCASHH");
            var TmpCashDlines = XModule.GetTable("CCCTMPCASHDGROUPED");
            if (TmpCashDlines.Count > 0)
            {
                var sumCreditVal = 0.0;
                var repeats = TmpCashDlines.Count;
                for (int i = 0; i < repeats; i++)
                {
                    sumCreditVal = sumCreditVal + Math.Round(TmpCashDlines.GetAsFloat(i, "CRDAMNT"), 2);
                }
                TmpCashH.Current["CRDAMNTFINAL"] = Math.Round(sumCreditVal, 2);
            }
        }

        internal static void GroupedCreditResults(XModule XModule, XSupport XSupport, string strXmodule)
        {
            var TmpCreditDlines = XModule.GetTable(strXmodule == "TmpCredit"? "CCCTMPCREDITD" : "CCCTMPCASHD");
            var ungroupedResults = new List<TmpCreditD>();
            if (TmpCreditDlines.Count>0)
            {
                if (strXmodule == "TmpCredit") //πιστωτικά
                {
                    var repeats = TmpCreditDlines.Count;
                    for (int i = 0; i < repeats; i++)
                    {
                        ungroupedResults.Add(new TmpCreditD
                        {
                            Company = TmpCreditDlines.GetAsInteger(i, "COMPANY"),
                            Findoc = TmpCreditDlines.GetAsInteger(i, "FINDOC"),
                            Trdr = TmpCreditDlines.GetAsInteger(i, "TRDR"),
                            TrdBranch = TmpCreditDlines.GetAsInteger(i, "TRDBRANCH"),
                            TrnDate = TmpCreditDlines.GetAsDateTime(i, "TRNDATE"),
                            Sodtype = TmpCreditDlines.GetAsInteger(i, "SODTYPE"),
                            //Payment = TmpCreditDlines.GetAsInteger(i, "PAYMENT"),
                            Mtrl = TmpCreditDlines.GetAsInteger(i, "MTRL"),
                            Groupname = TmpCreditDlines.GetAsString(i, "GROUPNAME"),
                            Analgroupname = TmpCreditDlines.GetAsString(i, "ANALGROUPNAME"),
                            Qty1 = TmpCreditDlines.GetAsFloat(i, "QTY1"),
                            Qty2 = TmpCreditDlines.GetAsFloat(i, "QTY2"),
                            CrdAmnt = TmpCreditDlines.GetAsFloat(i, "CRDAMNT"),
                            CrdAmnt1 = TmpCreditDlines.GetAsFloat(i, "CRDAMNT1"),
                            DiscVal = TmpCreditDlines.GetAsFloat(i, "DISCVAL"),
                            CrdFinAmnt = TmpCreditDlines.GetAsFloat(i, "CRDFINAMNT"),
                            CrdFinAmntExc = TmpCreditDlines.GetAsFloat(i, "CRDFINAMNTEXC"),
                        });
                    }
                    //Έλεγχος μοναδικότητας mtrl
                    //var distinctMtrls = ungroupedResults.Select(x => x.Mtrl).Distinct().ToList();
                    //grouping credit results per mtrl

                    var groupedResults = ungroupedResults.GroupBy(x => x.Mtrl).Select(y => new TmpCreditD
                    {
                        Company = y.First().Company,
                        Trdr = y.First().Trdr,
                        TrdBranch = y.First().TrdBranch,
                        TrnDate = y.First().TrnDate,
                        Sodtype = y.First().Sodtype,
                        Mtrl = y.Key,
                        Groupname = y.First().Groupname,
                        Analgroupname = y.First().Analgroupname,
                        Qty1 = Math.Round(y.Sum(c => c.Qty1), 2),
                        Qty2 = Math.Round(y.Sum(c => c.Qty2), 2),
                        CrdAmnt = y.Sum(c => c.CrdAmnt) > 0 ? Math.Round(y.Sum(c => c.CrdAmnt), 2) : 0.0,
                        CrdAmnt1 = Math.Round(y.Sum(c => c.CrdAmnt1), 2),
                        DiscVal = Math.Round(y.Sum(c => c.DiscVal), 2),
                        CrdFinAmnt = Math.Round(y.Sum(c => c.CrdFinAmnt), 2),
                        CrdFinAmntExc = Math.Round(y.Sum(c => c.CrdFinAmntExc), 2),
                        CrdAmntOver = (Math.Round(y.Sum(c => c.CrdAmnt1), 2) + Math.Round(y.Sum(c => c.CrdFinAmntExc), 2)) < 0
                                        ? (Math.Round(y.Sum(c => c.CrdAmnt1), 2) + Math.Round(y.Sum(c => c.CrdFinAmntExc), 2))
                                        : 0.0
                    }).ToList();

                    if (groupedResults.Any())
                    {
                        foreach (var res in groupedResults)
                        {
                            using (var TmpCreditDGrouped = XModule.GetTable("CCCTMPCREDITDGROUPED"))
                            {
                                //Πραγματοποιώ έλεγχο αν υπάρχει το είδος στον πίνακα των Συγκεντρωτικών Αποτελεσμάτων Υπολογισμών και το μεταβάλω. Αν όχι το προσθέτω.
                                var recno = TmpCreditDGrouped.Find("MTRL", res.Mtrl);
                                if (recno > -1)
                                {
                                    TmpCreditDGrouped.Current.Edit(recno);
                                    TmpCreditDGrouped.Current["GROUPNAME"] = res.Groupname;
                                    TmpCreditDGrouped.Current["ANALGROUPNAME"] = res.Analgroupname;
                                    TmpCreditDGrouped.Current["QTY1"] = res.Qty1;
                                    TmpCreditDGrouped.Current["QTY2"] = res.Qty2;
                                    TmpCreditDGrouped.Current["CRDFINAMNT"] = res.CrdFinAmnt;
                                    TmpCreditDGrouped.Current["CRDFINAMNTEXC"] = res.CrdFinAmntExc;
                                    TmpCreditDGrouped.Current["CRDAMNT"] = res.CrdAmnt;
                                    TmpCreditDGrouped.Current["CRDAMNT1"] = res.CrdAmnt1;
                                    TmpCreditDGrouped.Current["CRDAMNTOVER"] = res.CrdAmntOver;
                                    TmpCreditDGrouped.Current["DISCVAL"] = res.DiscVal;
                                    TmpCreditDGrouped.Current["CALCNETVAL"] = (res.CrdFinAmnt-res.DiscVal);
                                    TmpCreditDGrouped.Current.Post();
                                }
                                else
                                {
                                    TmpCreditDGrouped.Current.Append();
                                    TmpCreditDGrouped.Current["TRDR"] = res.Trdr;
                                    if (res.TrdBranch > 0)
                                    {
                                        TmpCreditDGrouped.Current["TRDBRANCH"] = res.TrdBranch;
                                    }
                                    TmpCreditDGrouped.Current["TRNDATE"] = res.TrnDate;
                                    TmpCreditDGrouped.Current["SODTYPE"] = res.Sodtype;
                                    TmpCreditDGrouped.Current["MTRL"] = res.Mtrl;
                                    TmpCreditDGrouped.Current["GROUPNAME"] = res.Groupname;
                                    TmpCreditDGrouped.Current["ANALGROUPNAME"] = res.Analgroupname;
                                    TmpCreditDGrouped.Current["QTY1"] = res.Qty1;
                                    TmpCreditDGrouped.Current["QTY2"] = res.Qty2;
                                    TmpCreditDGrouped.Current["CRDFINAMNT"] = res.CrdFinAmnt;
                                    TmpCreditDGrouped.Current["CRDFINAMNTEXC"] = res.CrdFinAmntExc;
                                    TmpCreditDGrouped.Current["CRDAMNT"] = res.CrdAmnt;
                                    TmpCreditDGrouped.Current["CRDAMNT1"] = res.CrdAmnt1;
                                    TmpCreditDGrouped.Current["CRDAMNTOVER"] = res.CrdAmntOver;
                                    TmpCreditDGrouped.Current["DISCVAL"] = res.DiscVal;
                                    TmpCreditDGrouped.Current["CALCNETVAL"] = (res.CrdFinAmnt - res.DiscVal);
                                    TmpCreditDGrouped.Current.Post();
                                }
                            }
                        }
                    }
                }
                else //Cash Payment
                {
                    var repeats = TmpCreditDlines.Count;
                    for (int i = 0; i < repeats; i++)
                    {
                        ungroupedResults.Add(new TmpCreditD
                        {
                            Company = TmpCreditDlines.GetAsInteger(i, "COMPANY"),
                            Findoc = TmpCreditDlines.GetAsInteger(i, "FINDOC"),
                            Trdr = TmpCreditDlines.GetAsInteger(i, "TRDR"),
                            TrdBranch = TmpCreditDlines.GetAsInteger(i, "TRDBRANCH"),
                            TrnDate = TmpCreditDlines.GetAsDateTime(i, "TRNDATE"),
                            Sodtype = TmpCreditDlines.GetAsInteger(i, "SODTYPE"),
                            Payment = TmpCreditDlines.GetAsInteger(i, "PAYMENT"),
                            //Mtrl = TmpCreditDlines.GetAsInteger(i, "MTRL"),
                            //Qty1 = TmpCreditDlines.GetAsFloat(i, "QTY1"),
                            //Qty2 = TmpCreditDlines.GetAsFloat(i, "QTY2"),
                            CrdAmnt = TmpCreditDlines.GetAsFloat(i, "CRDAMNT"),
                            CrdAmnt1 = TmpCreditDlines.GetAsFloat(i, "CRDAMNT1"),
                            //DiscVal = TmpCreditDlines.GetAsFloat(i, "DISCVAL"),
                            CrdFinAmnt = TmpCreditDlines.GetAsFloat(i, "CRDFINAMNT"),
                            //CrdFinAmntExc = TmpCreditDlines.GetAsFloat(i, "CRDFINAMNTEXC"),
                        });
                    }
                    //Έλεγχος μοναδικότητας mtrl
                    //var distinctMtrls = ungroupedResults.Select(x => x.Mtrl).Distinct().ToList();
                    //grouping credit results per mtrl

                    var groupedResults = ungroupedResults.GroupBy(x => x.Payment).Select(y => new TmpCreditD
                    {
                        Company = y.First().Company,
                        Trdr = y.First().Trdr,
                        TrdBranch = y.First().TrdBranch,
                        TrnDate = y.First().TrnDate,
                        Sodtype = y.First().Sodtype,
                        Payment = y.First().Payment,
                        CrdAmnt = y.Sum(c => c.CrdAmnt) > 0 ? Math.Round(y.Sum(c => c.CrdAmnt), 2) : 0.0,
                        CrdAmnt1 = Math.Round(y.Sum(c => c.CrdAmnt1), 2),
                        DiscVal = Math.Round(y.Sum(c => c.DiscVal), 2),
                        CrdFinAmnt = Math.Round(y.Sum(c => c.CrdFinAmnt), 2),
                        CrdFinAmntExc = Math.Round(y.Sum(c => c.CrdFinAmntExc), 2),
                        CrdAmntOver = (Math.Round(y.Sum(c => c.CrdAmnt1), 2) + Math.Round(y.Sum(c => c.CrdFinAmntExc), 2)) < 0
                                        ? (Math.Round(y.Sum(c => c.CrdAmnt1), 2) + Math.Round(y.Sum(c => c.CrdFinAmntExc), 2))
                                        : 0.0
                    }).ToList();

                    if (groupedResults.Any())
                    {
                        foreach (var res in groupedResults)
                        {
                            using (var TmpCreditDGrouped = XModule.GetTable("CCCTMPCASHDGROUPED"))
                            {
                                //Πραγματοποιώ έλεγχο αν υπάρχει το είδος στον πίνακα των Συγκεντρωτικών Αποτελεσμάτων Υπολογισμών και το μεταβάλω. Αν όχι το προσθέτω.
                                var recno = TmpCreditDGrouped.Find("PAYMENT", res.Payment);
                                if (recno > -1)
                                {
                                    TmpCreditDGrouped.Current.Edit(recno);
                                    //TmpCreditDGrouped.Current["QTY1"] = res.Qty1;
                                    //TmpCreditDGrouped.Current["QTY2"] = res.Qty2;
                                    TmpCreditDGrouped.Current["CRDFINAMNT"] = res.CrdFinAmnt;
                                    //TmpCreditDGrouped.Current["CRDFINAMNTEXC"] = res.CrdFinAmntExc;
                                    TmpCreditDGrouped.Current["CRDAMNT"] = res.CrdAmnt;
                                    TmpCreditDGrouped.Current["CRDAMNT1"] = res.CrdAmnt1;
                                    //TmpCreditDGrouped.Current["CRDAMNTOVER"] = res.CrdAmntOver;
                                    //TmpCreditDGrouped.Current["DISCVAL"] = res.DiscVal;
                                    TmpCreditDGrouped.Current.Post();
                                }
                                else
                                {
                                    TmpCreditDGrouped.Current.Append();
                                    TmpCreditDGrouped.Current["TRDR"] = res.Trdr;
                                    if (res.TrdBranch > 0)
                                    {
                                        TmpCreditDGrouped.Current["TRDBRANCH"] = res.TrdBranch;
                                    }
                                    TmpCreditDGrouped.Current["TRNDATE"] = res.TrnDate;
                                    TmpCreditDGrouped.Current["SODTYPE"] = res.Sodtype;
                                    TmpCreditDGrouped.Current["PAYMENT"] = res.Payment;
                                    TmpCreditDGrouped.Current["CRDFINAMNT"] = res.CrdFinAmnt;
                                    TmpCreditDGrouped.Current["CRDAMNT"] = res.CrdAmnt;
                                    TmpCreditDGrouped.Current["CRDAMNT1"] = res.CrdAmnt1;
                                    TmpCreditDGrouped.Current.Post();
                                }
                            }
                        }
                    }
                }
            }
        }

        //Διόρθωση Στρογγυλοποίησης Αποτελεσμάτων από την Προοδευτική Κλιμάκωση 
        internal static void GetPerfectRounding(List<DataLinesFromScale> original, XModule XModule, XSupport XSupport)
        {
            var forceSum = 0.0;
            var decimals = 2;
            var distinctScaleById = original.Select(x => x.ScaleById).Distinct().ToList();
            foreach (var item in distinctScaleById)
            {
                var filteredRes = original.Where(x => x.ScaleById == item && x.ScaleMethod ==2).ToList();
                if (filteredRes.Any())
                {
                    forceSum = filteredRes.FirstOrDefault().SumValFromScale;

                    var rounded = filteredRes.Select(y => new DataLinesFromScale
                    {
                        Mtrl = y.Mtrl,
                        Findoc = y.Findoc,
                        Mtrtrn = y.Mtrtrn,
                        ScaleMethod = y.ScaleMethod,
                        ScaleById = y.ScaleById,
                        ScaleFldValue = y.ScaleFldValue,
                        ScaleFldValueLine = y.ScaleFldValueLine,
                        SumValFromScale = y.SumValFromScale,
                        FinalBonusVal = Math.Round(y.FinalBonusVal, decimals)
                    }).ToList();

                    var delta = Math.Round((forceSum - rounded.Sum(x => x.FinalBonusVal)) , decimals);
                    var deltaUnit = Convert.ToDouble(Math.Pow(0.1, decimals)) * Math.Sign(delta);
                    List<int> applyDeltaSequence;

                    if(delta != 0)
                    {
                        if (delta < 0)
                        {
                            applyDeltaSequence = filteredRes
                                                    .Zip(Enumerable.Range(0, int.MaxValue), (x, index) => new { x, index })
                                                    .OrderBy(a=> filteredRes[a.index].FinalBonusVal - rounded[a.index].FinalBonusVal)
                                                    .ThenByDescending(a => a.index)
                                                    .Select(a => a.index).ToList();
                        }
                        else
                        {
                            applyDeltaSequence = filteredRes
                                                .Zip(Enumerable.Range(0, int.MaxValue), (x, index) => new { x, index })
                                                .OrderByDescending(a => filteredRes[a.index].FinalBonusVal - rounded[a.index].FinalBonusVal)
                                                .Select(a => a.index).ToList();
                        }

                        Enumerable.Repeat(applyDeltaSequence, int.MaxValue)
                                   .SelectMany(x => x)
                                   .Take(Convert.ToInt32(delta / deltaUnit)).ToList().ForEach(index => rounded[index].FinalBonusVal += deltaUnit);    

                        var finalSum = rounded.Sum(x => x.FinalBonusVal);

                        if (rounded.Any())
                        {
                            foreach (var res in rounded)
                            {
                                using (var TmpCreditD = XModule.GetTable("CCCTMPCREDITD"))
                                {
                                    //Πραγματοποιώ έλεγχο αν υπάρχει το είδος στον πίνακα των Συγκεντρωτικών Αποτελεσμάτων Υπολογισμών και το μεταβάλω. Αν όχι το προσθέτω.
                                    var recno = TmpCreditD.Find("MTRL;FINDOC;MTRTRN", res.Mtrl, res.Findoc, res.Mtrtrn);
                                        
                                    if (recno > -1)
                                    {
                                        TmpCreditD.Current.Edit(recno);
                                        TmpCreditD.Current["CRDAMNT1"] = res.FinalBonusVal;
                                        TmpCreditD.Current["CRDAMNT"] = res.FinalBonusVal;
                                        TmpCreditD.Current.Post();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public double GetSumValFromScale(double scalefldvalue , List<Scale> scaleByLine)
        {
            var sumvalfromscale = 0.0;
            var restvalue = 0.0;
            for (int i = 0; i< scaleByLine.Count;i++)
            {
                var start = Convert.ToDouble((i - 1) >= 0 ? scaleByLine[i - 1].ScaleValue : 0.0);
                var end = Convert.ToDouble(scaleByLine[i].ScaleValue);
                var bonus = Convert.ToDouble(scaleByLine[i].Bonus);
                restvalue = (scalefldvalue - end);
                var calculvalue = 0.0;

                if (scalefldvalue > start && scalefldvalue <= end && restvalue < 0)
                {
                    calculvalue = scalefldvalue;
                    sumvalfromscale = sumvalfromscale + Math.Round((calculvalue * (bonus / 100)), 2);
                }

                if (scalefldvalue > start && scalefldvalue > end && restvalue > 0)
                {
                    calculvalue = end - start;
                    sumvalfromscale = sumvalfromscale + Math.Round((calculvalue * (bonus / 100)), 2);
                }
            }
            return sumvalfromscale;
        }
    }
}
