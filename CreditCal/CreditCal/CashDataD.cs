﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditCal
{
    public class CashDataD
    {
        public int CashDataHId { get; set; }
        public int CashDataDId { get; set; }
        public int Company { get; set; }
        public int Sodtype { get; set; }
        public int EntityId { get; set; }
        public int Payment { get; set; }
        public double SumValPerPayment { get; set; }
        public double FinalBonus { get; set; }
        //public int CreditPart { get; set; }
        public int CashDataDs { get; set; }
        public int Soredir { get; set; }
        public int ScaleMethod { get; set; }
        public List<CashScale> Scale { get; set; }
        public List<CashDataD> PaymentList { get; set; }
    }
}
