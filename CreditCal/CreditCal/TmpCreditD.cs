﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditCal
{
    public class TmpCreditD
    {
        public int Company { get; set; }
        public int Findoc { get; set; }
        public int Sodtype { get; set; }
        public int Trdr { get; set; }
        public int TrdBranch { get; set; }
        public DateTime TrnDate { get; set; }
        public int Mtrl { get; set; }
        public string Groupname { get; set; }
        public string Analgroupname { get; set; }
        public int Payment { get; set; }
        public double Qty1 { get; set; }
        public double Qty2 { get; set; }
        public double CrdAmnt { get; set; }
        public double CrdAmnt1 { get; set; }
        public double CrdFinAmnt { get; set; }
        public double CrdFinAmntExc { get; set; }
        public double CrdAmntOver { get; set; }
        public double DiscVal { get; set; }
    }
}