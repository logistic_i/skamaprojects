﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditCal
{
    public class TmpCreditH
    {
        public int Company { get; set; }
        public int Sodtype { get; set; }
        public int Trdr { get; set; }
        public int TrdBranch { get; set; }
        public DateTime TrnDate { get; set; }
        public int LRate { get; set; }
        public float CrdAmnt { get; set; }
        public float CrdAmnt1 { get; set; }
        public float CrdFinAmnt { get; set; }
        public int SoCrd { get; set; }
        public int SoUpd { get; set; }
        public int Sotype { get; set; }
        public int Prcrule { get; set; }
        public float DiscVal { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}