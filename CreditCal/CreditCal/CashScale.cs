﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditCal
{
    public class CashScale
    {
        public int Company { get; set; }
        public double ScaleValue { get; set; }
        public double Price { get; set; }
        public double Bonus { get; set; }
        public int CashDataHId { get; set; }
        public int CashDataDId { get; set; }
    }
}
