﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditCal
{
    public class CreditDataD
    {
        public int CreditDataHId { get; set; }
        public int CreditDataDId { get; set; }
        public int Company { get; set; }
        public int Sodtype { get; set; }
        public int EntityId { get; set; }
        public int GroupId { get; set; }
        public int AnalGrupId { get; set; }
        public double Goal { get; set; }
        public double Bonus { get; set; }
        public int TurnOverPart { get; set; }
        public int CreditPart { get; set; }
        public int CreditDataDs { get; set; }
        public int Soredir { get; set; }
        public int? ScaleMethod { get; set; }
        public string GroupField { get; set; }
        public int GroupFieldId { get; set; }
        public List<Scale> Scale { get; set; }
        public List<CreditDataD> Analysis { get; set; }
    }
}
