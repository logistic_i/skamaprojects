﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditCal
{
    public class CreditDataH
    {
        public int CreditDataHId { get; set; }
        public int Company { get; set; }
        public int CreditModel { get; set; }
        public string Name { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime FinalDate { get; set; }
        public int DimTrdr { get; set; }
        public int DimMtrl { get; set; }
        public int AnalTrdrGroup { get; set; }
        public int AnalMtrlGroup { get; set; }
        public int SodtypeTrdr { get; set; }
        public int SodtypeMtrl { get; set; }
        public int TurnOverPart { get; set; }
        public int CreditPart { get; set; }
        public int ScaleFld { get; set; }
        public string Series { get; set; }
        public string Fprms { get; set; }
        public string SeriesExc { get; set; }
        public string FprmsExc { get; set; }
        public int SeriesCredit { get; set; }
        public int Sosource { get; set; }
        public int Calculbeh { get; set; }
        public int AppFld { get; set; }
        public int ScaleChoice { get; set; }
        public int ScaleExec { get; set; }
        public int CreditType { get; set; }
        public List<CreditDataD> CreditDataD { get; set; }
    }
}