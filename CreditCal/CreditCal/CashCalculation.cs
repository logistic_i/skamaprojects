﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softone;
using System.Windows.Forms;
using System.Data;

namespace CreditCal
{
    [WorksOn("CCCCALCULCASH")]
    public class CashCalculation : TXCode
    {
        public override void Initialize()
        {

            XModule.SetEvent("ON_CCCCALCASHH_AFTERPOST", On_CccCalCashH_AfterPost);
            XModule.SetEvent("ON_CCCCALCASHH_RULES", On_CccCalCashH_Rules);
            //XModule.SetEvent("ON_CCCCALCASHH_FROMDATE", On_CccCalCreditH_Fromdate);
            //XModule.SetEvent("ON_CCCCALCASHH_FINALDATE", On_CccCalCreditH_Finaldate);
            base.Initialize();
        }

        public DateTime fromdateRule;
        public DateTime finaldateRule;

        private void On_CccCalCashH_Rules(object Sender, XEventArgs e)
        {
            //βάζω στα φίλτρα της εργασίας σαν προεπιλογή το διάστημα της επιλεγμένης Συνθήκης Δεδ. Cash Payment
            //var Calcredith = XModule.GetTable("CCCCALCASHH").CreateDataTable(true); //για να δο τα στοιχεία του πίνακα που επιλέγω
            var Calcredith = XModule.GetTable("CCCCALCASHH");
            var rules = Convert.ToInt32(Calcredith.Current["RULES"]);

            var sodtypetrdr = Calcredith.Current["R_SODTYPETRDR"];
            var trdrs = "";
            var trdrdata = "SELECT T.TRDR FROM TRDR T ";
            var fintrdrs = "";
            var currentsodtypetrdr = "";
            switch (sodtypetrdr)
            {
                case 12:
                    currentsodtypetrdr = "SUPPLIER";
                    break;
                case 13:
                    currentsodtypetrdr = "CUSTOMER";
                    break;
                case 15:
                    currentsodtypetrdr = "DEBTOR";
                    break;
                case 16:
                    currentsodtypetrdr = "CREDITOR";
                    break;
                case 14:
                    currentsodtypetrdr = "BANKACC";
                    break;
            }

            var querycriteria = $@"SELECT STUFF((SELECT ',' +CAST(ENTITYID AS VARCHAR(200)) FROM  CCCCASHPAYMENTD WHERE CCCCASHPAYMENTH ={rules} 
                                AND COMPANY={XSupport.ConnectionInfo.CompanyId} AND SODTYPE={sodtypetrdr} FOR XML PATH('')), 1, 1, '') AS ENTITYIDS";

            using (XTable dscriteria = XSupport.GetSQLDataSet(querycriteria))
            {
                if (dscriteria.Count > 0)
                {
                    for (int i = 0; i < dscriteria.Count; i++)
                    {
                        var entityids = dscriteria.GetAsString(i, "ENTITYIDS");
                        if (entityids != "")
                        {
                            trdrs = $"SELECT TRDR FROM TRDR TT WHERE TT.TRDR IN ({entityids})";
                        }
                    }

                    if (trdrs != "")
                    {
                        fintrdrs = trdrs;
                    }
                    else
                    {
                        fintrdrs = trdrdata;
                    }
                }
            }

            fromdateRule = Convert.ToDateTime(Calcredith.Current["R_FROMDATE"]);
            finaldateRule = Convert.ToDateTime(Calcredith.Current["R_FINALDATE"]);

            Calcredith.Current["FROMDATE"] = Calcredith.Current["R_FROMDATE"];
            Calcredith.Current["FINALDATE"] = Calcredith.Current["R_FINALDATE"];
            XModule.SetFieldEditor("CCCCALCASHH.TRDR", $"{currentsodtypetrdr}(W[A.TRDR IN ({fintrdrs})])");
        }

        private void On_CccCalCashH_AfterPost(object Sender, XEventArgs e)
        {
            var Calcredith = XModule.GetTable("CCCCALCASHH");
            var rules = Convert.ToInt32(Calcredith.Current["RULES"]);
            var trdbranchstr = "";
            var fromdate = Convert.ToDateTime(Calcredith.Current["FROMDATE"]).ToString("yyyyMMdd");
            var finaldate = Convert.ToDateTime(Calcredith.Current["FINALDATE"]).ToString("yyyyMMdd");


            if (XModule.GetTable("CCCCALCASHH").Current["FROMDATE"] != DBNull.Value)
            {
                fromdate = $" AND F.TRNDATE >='{fromdate}'";
            }
            else
            {
                fromdate = "";
            }
            if (XModule.GetTable("CCCCALCASHH").Current["FINALDATE"] != DBNull.Value)
            {
                finaldate = $" AND F.TRNDATE <='{finaldate}'";
            }
            else
            {
                finaldate = "";
            }

            var restrdr = int.TryParse(Calcredith.Current["TRDR"].ToString(), out int trdr);
            var restrdbarnch = int.TryParse(Calcredith.Current["TRDBRANCH"].ToString(), out int trdbranch);

            if (XModule.GetTable("CCCCALCASHH").Current["TRDBRANCH"] != DBNull.Value)
            {
                trdbranchstr = $" AND F.TRDBRANCH ='{trdbranch}'";
            }
            else
            {
                trdbranchstr = "";
            }

            var trdrs = "";
            var trdrdata = "SELECT T.TRDR FROM TRDR T ";
            var payments = "";
            var sosource = "";
            var fintrdrs = "";
            var tools = new Tools(XModule, XSupport);
            var FinalRuleList = tools.GetCashRuleData(rules,0,0);

            //Τύποι Υπολογισμών
            var fprms = "";
            if (FinalRuleList.FirstOrDefault()?.Fprms != "")
            {
                fprms = $" AND F.FPRMS IN({FinalRuleList.FirstOrDefault()?.Fprms})";
            }
            else
            {
                fprms = "";
            }

            //Σειρές Υπολογισμών
            var series = "";
            if (FinalRuleList.FirstOrDefault()?.Series != "")
            {
                series = $" AND F.SERIES IN({FinalRuleList.FirstOrDefault()?.Series})";
            }
            else
            {
                series = "";
            }

            //Τύποι που Αφαιρούνται
            var fprmsexc = "";
            if (FinalRuleList.FirstOrDefault()?.FprmsExc != "")
            {
                fprmsexc = $" AND F.FPRMS NOT IN({FinalRuleList.FirstOrDefault()?.FprmsExc})";
            }
            else
            {
                fprmsexc = "";
            }
            //Σειρές που Αφαιρούνται
            var seriesexc = "";
            if (FinalRuleList.FirstOrDefault()?.SeriesExc != "")
            {
                seriesexc = $" AND F.SERIES NOT IN({FinalRuleList.FirstOrDefault()?.SeriesExc})";
            }
            else
            {
                seriesexc = "";
            }

            if (trdr == 0)
            {
                if (trdrs != "")
                {
                    fintrdrs = trdrs;
                }
                else
                {
                    fintrdrs = trdrdata;
                }
            }
            else
            {
                fintrdrs = Convert.ToString(trdr);
            }

            //COLLECT CREDIT DATA
            try
            {
                var querycriteria = $@"SELECT * FROM (SELECT A.COMPANY,A.CCCCASHPAYMENTH,A.SODTYPETRDR AS SODTYPE,A.SOSOURCE,A.SODTYPETRDR,B.ENTITYID,SCALEMETHOD,
                                    STUFF((SELECT ',' + CAST(A1.ENTITYID AS VARCHAR) FROM CCCCASHPAYMENTD A1 
                                    WHERE A1.CCCCASHPAYMENTH=A.CCCCASHPAYMENTH AND B.CCCCASHPAYMENTD=A1.CCCCASHPAYMENTDS AND ISNULL(A1.SODTYPE,0)=0 FOR XML PATH('')), 1, 1, '') AS PAYMENTS
                                    FROM CCCCASHPAYMENTH A INNER JOIN CCCCASHPAYMENTD B ON B.CCCCASHPAYMENTH=A.CCCCASHPAYMENTH
                                    WHERE A.COMPANY={XSupport.ConnectionInfo.CompanyId} AND ISNULL(B.SODTYPE,0)<>0 AND B.ENTITYID IN ({fintrdrs})) Q
                                    WHERE CCCCASHPAYMENTH={rules}";

                using (XTable dscriteria = XSupport.GetSQLDataSet(querycriteria))
                {
                    if (dscriteria.Count > 0)
                    {
                        for (int i = 0; i < dscriteria.Count; i++)
                        {
                            trdrs = dscriteria.GetAsString(i, "ENTITYID");
                            payments = dscriteria.GetAsString(i, "PAYMENTS");
                            sosource = dscriteria.GetAsString(i, "SOSOURCE");

                            // loop TRDR
                            var querytrdr = $@"SELECT T.TRDR,T.CODE,T.NAME,T.AFM,T.SODTYPE FROM TRDR T WHERE COMPANY={XSupport.ConnectionInfo.CompanyId} AND T.TRDR IN ({trdrs})";
                            using (XTable dstrdr = XSupport.GetSQLDataSet(querytrdr))
                            {
                                if (dstrdr.Count > 0)
                                {
                                    for (int j = 0; j < dstrdr.Count; j++)
                                    {
                                        // GET FINDATA
                                        var currenttrdr = dstrdr.GetAsInteger(j, "TRDR");
                                        var trdrinfo = dstrdr.GetAsString(j, "CODE") + " - " + dstrdr.GetAsString(j, "NAME") + " Α.Φ.Μ.:" + dstrdr.GetAsString(j, "AFM");
                                        var currnetsodtype = dstrdr.GetAsInteger(j, "SODTYPE");

                                        var queryfindocs = $@"SELECT COMPANY,FINDOC,FINCODE,SOSOURCE,TRDR,TRDBRANCH,PAYMENT,TRNDATE,
                                                            SUMAMNT,SUM(AMNTANTI) AS AMNTANTI,SUM(TAMNTANTI) AS TAMNTANTI,SUM(OPNTAMNTANTI) AS OPNTAMNTANTI,TOCALCULATE
                                                            FROM 
                                                            (SELECT A.COMPANY,A.FINDOC,A.FINCODE,SOSOURCE,A.TRDR,A.TRDBRANCH,A.PAYMENT,
                                                            /*A.FINALDATE,A.ENDDATE,A.AMNT,A.TAMNT,A.OPNTAMNT,*/
                                                            A.TRNDATE,SUMAMNT,SUM(ISNULL(A.AMNTANTI,0)) AS AMNTANTI,SUM(ISNULL(A.TAMNTANTI,0)) AS TAMNTANTI,
                                                            SUM(ISNULL(A.OPNTAMNTANTI,0)) AS OPNTAMNTANTI,TOCALONDATE,
                                                            CASE WHEN SUMAMNT =SUM(ISNULL(A.TAMNTANTI,0)) THEN 1 ELSE 0 END AS TOCALONVAL
                                                            /*24/06/2021
                                                            ,(TOCALONDATE*(CASE WHEN SUMAMNT =SUM(ISNULL(A.TAMNTANTI,0)) THEN 1 ELSE 0 END)) AS TOCALCULATE
                                                            */
                                                            ,(TOCALONDATE*(CASE WHEN SUMAMNT >=SUM(ISNULL(A.TAMNTANTI,0)) THEN 1 ELSE 0 END)) AS TOCALCULATE
                                                            FROM(
                                                            SELECT FP.COMPANY,FP.FINPAYTERMS,FP.FINDOC,F.FINCODE,F.SOSOURCE,FP.TRDR,FP.TRDBRANCH,FP.PAYMENT,FP.PAYDEMANDMD,FP.SOPAYTYPE,
                                                            FP.FINALDATE,FP.TRNDATE,FP.ENDDATE,FP.AMNT,FP.TAMNT,FP.OPNTAMNT,F.SUMAMNT,FP.ISCLOSE,
                                                            FP2.FINDOC AS FINDOCANTI,F2.FINCODE AS FINCODEANTI,FP2.TRNDATE AS TRNDATEANTI,FP2.ENDDATE AS ENDDATEANTI ,FP2.AMNT AS AMNTANTI,FP2.TAMNT AS TAMNTANTI, FP2.OPNTAMNT AS OPNTAMNTANTI
                                                            ,CASE WHEN FP2.TRNDATE<=FP.FINALDATE THEN 1 ELSE 0 END AS TOCALONDATE
                                                            FROM FINPAYTERMS FP
                                                            INNER JOIN FINDOC F ON F.FINDOC =FP.FINDOC
                                                            LEFT JOIN FINPAYTERMS FP2 ON FP2.FINPAYTERMSS=FP.FINPAYTERMS AND FP2.FINDOCS=FP.FINDOC
                                                            LEFT JOIN FINDOC F2 ON F2.FINDOC=FP2.FINDOC
                                                            WHERE
                                                            FP.COMPANY={XSupport.ConnectionInfo.CompanyId} 
                                                            AND FP.TRDR={currenttrdr}
                                                            AND F.SOSOURCE={sosource} 
                                                            AND FP.PAYMENT IN ({payments}) 
                                                            {fromdate}{finaldate}{trdbranchstr}{fprms}{series}{fprmsexc}{seriesexc}
                                                            ) A
                                                            GROUP BY A.COMPANY,A.FINDOC,A.FINCODE,SOSOURCE,A.TRDR,A.TRDBRANCH,A.PAYMENT,
                                                            /*A.FINALDATE,A.ENDDATE,A.AMNT,A.TAMNT,A.OPNTAMNT,*/
                                                            A.TRNDATE,SUMAMNT,TOCALONDATE
                                                            ) Q
                                                            GROUP BY COMPANY,FINDOC,FINCODE,SOSOURCE,TRDR,TRDBRANCH,PAYMENT,TRNDATE,SUMAMNT,TOCALCULATE";  

                                        using (XTable dsfindocs = XSupport.GetSQLDataSet(queryfindocs))
                                        {
                                            if (dsfindocs.Count > 0)
                                            {
                                                var finalbonus = 0.0;
                                                //Μέθοδος Υπολ. Κλιμάκωσης
                                                //var scaleMethod = 0;
                                                //var finalbonusval = 0.0;
                                                //Για την καλύτερη στρογγυλοποίηση των αξιών των πιστωτικών που θα δημιουργηθούν όταν έχουμε κλιμάκωση με προοδευτικό τρόπο.
                                                var ResultsForPerfectRounding = new List<DataLinesFromScale>();

                                                var creditpartList = dsfindocs.CreateDataTable(true).AsEnumerable().ToList();
                                                var sumlineval = creditpartList.Where(x => Convert.ToInt32(x["TOCALCULATE"]) == 1).
                                                    Sum(x => x["TAMNTANTI"] != DBNull.Value ? Convert.ToDouble(x["TAMNTANTI"]) : 0);

                                                var cashTrdrPaymentList = new List<CashDataD>();
                                                var filresults = new List<Scale>();

                                                cashTrdrPaymentList = FinalRuleList.SelectMany(x=>x.TrdrList).ToList()
                                                                  .Where(y=>y.EntityId == currenttrdr).ToList()
                                                                  .SelectMany(z=>z.PaymentList).ToList();

                                                var scaleByLine = cashTrdrPaymentList.SelectMany(x => x.Scale).OrderBy(x => x.ScaleValue).ToList();

                                                foreach (var item in cashTrdrPaymentList)
                                                {
                                                    var currentpayment = 0;
                                                    var sumValPerPayment = 0.0;
                                                    if (item.ScaleMethod == 1)
                                                    {
                                                        currentpayment = item.Payment;
                                                        sumValPerPayment = creditpartList.Where(x => Convert.ToInt32(x["TOCALCULATE"]) == 1
                                                                                && Convert.ToInt32(x["PAYMENT"]) == currentpayment).
                                                        Sum(x => x["TAMNTANTI"] != DBNull.Value ? Convert.ToDouble(x["TAMNTANTI"]) : 0);

                                                        item.SumValPerPayment = sumValPerPayment;

                                                        //Μέθοδος Υπολ. Κλιμάκωσης // 1 = Αθροιστικά
                                                        //loop scale or scaleByLine
                                                        foreach (var sitem in scaleByLine)
                                                        {
                                                            if (item.SumValPerPayment >= sitem.ScaleValue)
                                                            {
                                                                finalbonus = sitem.Bonus;
                                                            }
                                                        }
                                                        item.FinalBonus = Math.Round(finalbonus / 100, 2);
                                                    }
                                                    else 
                                                    {
                                                        currentpayment = item.Payment;
                                                        sumValPerPayment = 0;

                                                        //var sumvalfromscale = tools.GetSumValFromScale(scalefldvalue, filresults);
                                                        //finalbonusval = ((sumvalfromscale * scalefldvalueline) / scalefldvalue);

                                                    }
                                                }

                                                var cashHasFinalBonus = cashTrdrPaymentList.Where(x => x.FinalBonus > (double)0).Count();

                                                if (cashHasFinalBonus > 0)
                                                {
                                                    //create credit result data
                                                    using (var TmpCash = XSupport.CreateModule("CCCTMPCASH"))
                                                    {
                                                        TmpCash.InsertData();
                                                        TmpCash.GetTable("CCCTMPCASHH").Current["SODTYPE"] = currnetsodtype;
                                                        TmpCash.GetTable("CCCTMPCASHH").Current["TRDR"] = currenttrdr;
                                                        if (trdbranch > 0)
                                                        {
                                                            TmpCash.GetTable("CCCTMPCASHH").Current["TRDBRANCH"] = trdbranch;
                                                        }
                                                        TmpCash.GetTable("CCCTMPCASHH").Current["TRNDATE"] = XSupport.ConnectionInfo.LoginDate;
                                                        TmpCash.GetTable("CCCTMPCASHH").Current["LRATE"] = 0;
                                                        TmpCash.GetTable("CCCTMPCASHH").Current["SOCRD"] = 0;
                                                        TmpCash.GetTable("CCCTMPCASHH").Current["SOUPD"] = 0;
                                                        TmpCash.GetTable("CCCTMPCASHH").Current["PRCRULE"] = rules;
                                                        TmpCash.GetTable("CCCTMPCASHH").Current["FROMDATE"] = Calcredith.Current["FROMDATE"];
                                                        TmpCash.GetTable("CCCTMPCASHH").Current["TODATE"] = Calcredith.Current["FINALDATE"];

                                                        foreach (var lines in creditpartList)
                                                        {
                                                            TmpCash.GetTable("CCCTMPCASHD").Current.Append();
                                                            TmpCash.GetTable("CCCTMPCASHD").Current["TRDR"] = currenttrdr;
                                                            if (trdbranch > 0)
                                                            {
                                                                TmpCash.GetTable("CCCTMPCASHD").Current["TRDBRANCH"] = trdbranch;
                                                            }
                                                            TmpCash.GetTable("CCCTMPCASHD").Current["TRNDATE"] = XSupport.ConnectionInfo.LoginDate;
                                                            TmpCash.GetTable("CCCTMPCASHD").Current["SODTYPE"] = currnetsodtype;
                                                            TmpCash.GetTable("CCCTMPCASHD").Current["FINDOC"] = Convert.ToInt32(lines["FINDOC"]);
                                                            TmpCash.GetTable("CCCTMPCASHD").Current["PAYMENT"] = Convert.ToInt32(lines["PAYMENT"]);

                                                            var linepayment = Convert.ToInt32(lines["PAYMENT"]);
                                                            //Επιλέγω ανάλογα αν θα πάρει την τιμή των τιμολογίων ή του πιστωτικού
                                                            /*24/06/2021
                                                            TmpCash.GetTable("CCCTMPCASHD").Current["CRDFINAMNT"] = Convert.ToInt32(lines["TOCALCULATE"]) ==1 ? Math.Round(Convert.ToDouble(lines["SUMAMNT"]), 2) : (double)0;
                                                            */
                                                            TmpCash.GetTable("CCCTMPCASHD").Current["CRDFINAMNT"] = Convert.ToInt32(lines["TOCALCULATE"]) == 1 ? Math.Round(Convert.ToDouble(lines["TAMNTANTI"]), 2) : (double)0;

                                                            //TmpCash.GetTable("CCCTMPCREDITD").Current["CRDFINAMNTEXC"] = Math.Round(scalefldvaluelineexc, 2);
                                                            var paymentlist = FinalRuleList.SelectMany(x => x.TrdrList).ToList()
                                                                      .Where(y => y.EntityId == currenttrdr).ToList()
                                                                      .SelectMany(z => z.PaymentList).ToList()
                                                                      .Where(h => h.Payment == linepayment).ToList();
                                                            var linefinalbonus = paymentlist.Select(x => x.FinalBonus).FirstOrDefault();
                                                            /*24/06/2021
                                                            TmpCash.GetTable("CCCTMPCASHD").Current["CRDAMNT1"] = Convert.ToDouble(lines["TOCALCULATE"]) * Math.Round(Math.Round(Convert.ToDouble(lines["SUMAMNT"]), 2) * linefinalbonus, 2);
                                                            TmpCash.GetTable("CCCTMPCASHD").Current["CRDAMNT"] = Convert.ToDouble(lines["TOCALCULATE"]) * Math.Round(Math.Round(Convert.ToDouble(lines["SUMAMNT"]), 2) * linefinalbonus, 2);
                                                            */
                                                            TmpCash.GetTable("CCCTMPCASHD").Current["CRDAMNT1"] = Convert.ToDouble(lines["TOCALCULATE"]) * Math.Round(Math.Round(Convert.ToDouble(lines["TAMNTANTI"]), 2) * linefinalbonus, 2);
                                                            TmpCash.GetTable("CCCTMPCASHD").Current["CRDAMNT"] = Convert.ToDouble(lines["TOCALCULATE"]) * Math.Round(Math.Round(Convert.ToDouble(lines["TAMNTANTI"]), 2) * linefinalbonus, 2);
                                                            TmpCash.GetTable("CCCTMPCASHD").Current.Post();
                                                        }

                                                        //Διόρθωση Στρογγυλοποίησης Αποτελεσμάτων από την Προοδευτική Κλιμάκωση
                                                        //Tools.GetPerfectRounding(ResultsForPerfectRounding, TmpCash, XSupport);

                                                        var ccctmpcreditd = TmpCash.GetTable("CCCTMPCASHD").CreateDataTable(true).AsEnumerable().ToList();
                                                        var sumcrdfinamnt = ccctmpcreditd.Sum(x => x["CRDFINAMNT"] != DBNull.Value ? double.Parse(x["CRDFINAMNT"].ToString()) : 0.0);
                                                        var sumcrdamnt1 = ccctmpcreditd.Sum(x => x["CRDAMNT1"] != DBNull.Value ? double.Parse(x["CRDAMNT1"].ToString()) : 0.0);
                                                        var sumcrdamnt = ccctmpcreditd.Sum(x => x["CRDAMNT"] != DBNull.Value ? double.Parse(x["CRDAMNT"].ToString()) : 0.0);

                                                        TmpCash.GetTable("CCCTMPCASHH").Current["CRDFINAMNT"] = Math.Round(sumcrdfinamnt, 2);
                                                        TmpCash.GetTable("CCCTMPCASHH").Current["CRDAMNT1"] = Math.Round(sumcrdamnt1, 2);
                                                        TmpCash.GetTable("CCCTMPCASHH").Current["CRDAMNT"] = Math.Round(sumcrdamnt, 2);

                                                        //Συμπλήρωση Συγκεντρωτικών Αποτελεσμάτων Υπολογισμών
                                                        Tools.GroupedCreditResults(TmpCash, XSupport, "TmpCash");
                                                        //Συμπλήρωση Αξία προς δημιουργία πιστ/κού
                                                        Tools.CalculateFinalSumCashVal(TmpCash, XSupport);

                                                        TmpCash.PostData();
                                                        MessageBox.Show($"Ολοκληρώθηκε η διαδικασία υπολογισμού cash payment για τον συναλλασσόμενο {trdrinfo}.", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                                                        //XModule.CloseForm();
                                                    }
                                                }
                                                else
                                                {
                                                    MessageBox.Show($"Ο επιλεγμένος συναλλασσόμενος ({trdrinfo}), το χρονικό διάστημα και για το σενάριο cash payment που έχετε επιλέξει δεν έχει πετύχει τον στόχο του , επομένως δεν δημιουργούνται πιστωτικά για αυτόν.", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                                }
                                            }
                                            else
                                            {
                                                MessageBox.Show($"Δεν υπάρχουν κινήσεις για τον συναλλασσόμενο ({trdrinfo}), το χρονικό διάστημα και για το σενάριο cash payment που έχετε επιλέξει. Συμπληρώστε διαφορετικά φίλτρα στην εργασία.", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
