﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Softone;

namespace CreditCal
{
    [WorksOn("PURDOC")]
    public class PurdocCash : TXCode
    {
        public static int formappingcredit = 0 ;

        public override void AfterDelete()
        {
            var Purdoc = XModule.GetTable("PURDOC");
            var PurdocId = Convert.ToInt32(Purdoc.Current["FINDOC"]) > 0 ? Convert.ToInt32(Purdoc.Current["FINDOC"]) : XModule.ObjID();
            var fullyTransf = Convert.ToInt32(Purdoc.Current["FULLYTRANSF"]);

            var queryToDelete = $@"SELECT F.FINDOC,F.SERIES,F.FINCODE,F.CCCCASHPAYMENTH FROM FINDOC F WHERE CCCFINDOCCASH ={PurdocId} AND COMPANY={XSupport.ConnectionInfo.CompanyId}";
            using (XTable dsToDelete = XSupport.GetSQLDataSet(queryToDelete))
            {
                if (dsToDelete.Count > 0)
                {
                    XSupport.Warning("Με την διαγραφή του παραστατικού θα διαγραφεί αυτόματα και το πιστωτικό που έχει δημιουργηθεί από σενάριο Cash Payment.");
                    for (int i = 0; i < dsToDelete.Count; i++)
                    {
                        using (var PurdocObj = XSupport.CreateModule("PURDOC;LikeDefault"))
                        {
                            S1Tools.HideWarningsFromS1Module(PurdocObj, XSupport);
                            try
                            {
                                PurdocObj.LocateData(dsToDelete.GetAsInteger(i, "FINDOC"));
                                PurdocObj.DeleteData();
                                /*
                                if (fullyTransf != 0)
                                {
                                    XSupport.ExecuteSQL($@"UPDATE FINDOC SET FULLYTRANSF=0 WHERE FINDOC = {PurdocId} AND COMPANY= {XSupport.ConnectionInfo.CompanyId}");
                                }
                                */
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                    }
                }
            }
            base.AfterDelete();
        }
        public override void AfterPost()
        {
            if (formappingcredit == 0)
            {
                var Purdoc = XModule.GetTable("PURDOC");
                var Itelines = XModule.GetTable("ITELINES");
                var Branch = Convert.ToInt32(Purdoc.Current["BRANCH"] != DBNull.Value ? Purdoc.Current["BRANCH"] : 0);
                var Trdr = Convert.ToInt32(Purdoc.Current["TRDR"] != DBNull.Value ? Purdoc.Current["TRDR"] : 0);
                var TrdrBranch = Convert.ToInt32(Purdoc.Current["TRDBRANCH"] != DBNull.Value ? Purdoc.Current["TRDBRANCH"] : 0);
                var Payment = Convert.ToInt32(Purdoc.Current["PAYMENT"] != DBNull.Value ? Purdoc.Current["PAYMENT"] : 0);
                var Trndate = Convert.ToDateTime(Purdoc.Current["TRNDATE"]);
                var Series = Convert.ToInt32(Purdoc.Current["SERIES"]);
                var Fprms = Convert.ToInt32(Purdoc.Current["FPRMS"]);
                var PurdocId = Convert.ToInt32(Purdoc.Current["FINDOC"]) > 0 ? Convert.ToInt32(Purdoc.Current["FINDOC"]) : XModule.ObjID();
                var tools = new Tools(XModule, XSupport);
                var cashhlist = new List<List<CashDataH>>();
                var newItelinesList = new List<NewItelinesCash>();
                var findocslist = new List<int>();

                var querycont = $@"SELECT CASE WHEN {Series} IN (SELECT Split.a.value('.', 'VARCHAR(100)') AS Data FROM (SELECT CAST ('<M>' + REPLACE(ISNULL(SERIES,''), ',', '</M><M>') + '</M>' AS XML) AS Data 
                               FROM CCCCASHPAYMENTH WHERE COMPANY={XSupport.ConnectionInfo.CompanyId} AND SERIES<>'') AS A CROSS APPLY Data.nodes ('/M') AS Split(a)) THEN 1 ELSE 0 END AS CONTINUECASH";
                using (XTable dscont = XSupport.GetSQLDataSet(querycont))
                {
                    if (dscont.GetAsInteger(0, "CONTINUECASH") == 1)
                    {
                        var query = $@"SELECT DISTINCT CCCCASHPAYMENTH,SERIESCREDIT FROM (SELECT A.CCCCASHPAYMENTH,A.NAME,A.FROMDATE,A.FINALDATE,A.SODTYPETRDR,A.SOSOURCE,
                            A.SERIESCREDIT,A.CREDITTYPE,ISNULL(A.SERIES, '') AS SERIES, ISNULL(A.FPRMS, '') AS FPRMS,
                            B.ENTITYID AS TRDR,C.ENTITYID AS PAYMENT
                            /*,ISNULL(D.SCALEVALUE, 0.0) AS SCALEVALUE, ISNULL(D.PRICE, 0.0) AS PRICE, ISNULL(D.BONUS, 0.0) AS BONUS*/
                            FROM CCCCASHPAYMENTH A
                            LEFT JOIN CCCCASHPAYMENTD B ON B.CCCCASHPAYMENTH = A.CCCCASHPAYMENTH AND B.SODTYPE IS NOT NULL
                            LEFT JOIN CCCCASHPAYMENTD C ON C.CCCCASHPAYMENTH = A.CCCCASHPAYMENTH AND C.SODTYPE IS NULL AND C.CCCCASHPAYMENTDS = B.CCCCASHPAYMENTD
                            /*LEFT JOIN CCCCASHPAYMENTSCALE D ON D.CCCCASHPAYMENTH = A.CCCCASHPAYMENTH AND D.CCCCASHPAYMENTD = C.CCCCASHPAYMENTD*/
                            WHERE A.COMPANY = {XSupport.ConnectionInfo.CompanyId}) Q WHERE TRDR = {Trdr} AND PAYMENT = {Payment} AND '{Trndate.ToString("yyyyMMdd")}' BETWEEN FROMDATE AND FINALDATE
                            AND (CASE 
                            WHEN SERIES <>'' THEN (CASE WHEN '{Series}' IN (SELECT Split.a.value('.', 'VARCHAR(100)') AS Data 
                            FROM ( SELECT CAST ('<M>' + REPLACE(SERIES, ',', '</M><M>') + '</M>' AS XML) AS Data) AS A CROSS APPLY Data.nodes ('/M') AS Split(a))  THEN 1 ELSE 0 END)
                            WHEN FPRMS <>'' THEN (CASE WHEN '{Fprms}' IN (SELECT Split.a.value('.', 'VARCHAR(100)') AS Data 
                            FROM ( SELECT CAST ('<M>' + REPLACE(FPRMS, ',', '</M><M>') + '</M>' AS XML) AS Data) AS A CROSS APPLY Data.nodes ('/M') AS Split(a))  THEN 1 ELSE 0 END)
                            ELSE 0 END) =1";
                        using (XTable ds = XSupport.GetSQLDataSet(query))
                        {
                            if (ds.Count > 0)
                            {
                                for (int i = 0; i < ds.Count; i++)
                                {
                                    var rules = ds.GetAsInteger(i, "CCCCASHPAYMENTH");
                                    var FinalRuleList = tools.GetCashRuleData(rules, Trdr, Payment);
                                    cashhlist.Add(FinalRuleList);
                                }
                            }
                        }

                        foreach (var list in cashhlist)
                        {
                            var cashTrdrPaymentList = new List<CashDataD>();
                            var filresults = new List<Scale>();
                            cashTrdrPaymentList = list.SelectMany(x => x.TrdrList).ToList().Where(y => y.EntityId == Trdr).ToList().SelectMany(z => z.PaymentList).ToList();
                            var scaleByLine = cashTrdrPaymentList.SelectMany(x => x.Scale).OrderBy(x => x.ScaleValue).ToList();
                            var rule = list.FirstOrDefault().CashDataHId;
                            var seriesCredit = list.FirstOrDefault().SeriesCredit;

                            if (Itelines.Count > 0)
                            {
                                for (int i = 0; i < Itelines.Count; i++)
                                {
                                    var findoc = Itelines.GetAsInteger(i, "FINDOC") >0 ? Itelines.GetAsInteger(i, "FINDOC")  : PurdocId; 
                                    var mtrlines = Itelines.GetAsInteger(i, "MTRLINES");
                                    var mtrl = Itelines.GetAsInteger(i, "MTRL");
                                    var qty1 = Itelines.GetAsFloat(i, "QTY1");
                                    var netlineval = Itelines.GetAsFloat(i, "NETLINEVAL");
                                    var newPrice = 0.0;
                                    foreach (var sitem in scaleByLine)
                                    {
                                        if (netlineval <= sitem.ScaleValue)
                                        {
                                            newPrice = Math.Round((sitem.Bonus / 100 * netlineval) / qty1, 2);
                                        }
                                    }
                                    if (newPrice > 0.0)
                                    {
                                        var itelines = new NewItelinesCash
                                        {
                                            Findoc = findoc,
                                            Mtrlines = mtrlines,
                                            Mtrl = mtrl,
                                            Qty1 = qty1,
                                            Price = newPrice,
                                            Rule = rule
                                        };
                                        newItelinesList.Add(itelines);
                                    }
                                }
                            }

                            if (newItelinesList.Where(x => x.Rule == rule).ToList().Count > 0)
                            {
                                formappingcredit = 1;
                                var deleteFindoc = 0;
                                // Έλεγχος έκδοσης παραστατικών cash payment
                                var querycheckcash = $@"SELECT ML.FINDOC,ML.MTRLINES,ML.FINDOCS,ML.MTRLINESS,ML.MTRL,ML.QTY1,ML.PRICE,ISNULL(F.CCCCASHPAYMENTH,0) AS CCCCASHPAYMENTH FROM FINDOC F INNER JOIN MTRLINES ML ON F.FINDOC=ML.FINDOC
                                                WHERE F.COMPANY = {XSupport.ConnectionInfo.CompanyId} AND ML.FINDOCS = {PurdocId} AND F.CCCCASHPAYMENTH = {rule}";
                                //$@"SELECT STUFF((SELECT ',' + CAST(FINDOC AS VARCHAR) FROM MTRLINES WHERE FINDOCS ={PurdocId} GROUP BY FINDOC FOR XML PATH('')), 1, 1, '') AS FINDOCS";
                                using (XTable dscheckcash = XSupport.GetSQLDataSet(querycheckcash))
                                {
                                    if (dscheckcash.Count > 0)
                                    {
                                        var findocsForDelete = new List<int>();
                                        var itelinesListNew = newItelinesList.Where(x => x.Rule == rule).ToList();
                                        var itelinesListExist = new List<NewItelinesCash>();
                                        for (int j = 0; j < dscheckcash.Count; j++)
                                        {
                                            var itelines = new NewItelinesCash
                                            {
                                                Findoc = dscheckcash.GetAsInteger(j, "FINDOCS"),
                                                Mtrlines = dscheckcash.GetAsInteger(j, "MTRLINESS"),
                                                Mtrl = dscheckcash.GetAsInteger(j, "MTRL"),
                                                Qty1 = dscheckcash.GetAsFloat(j, "QTY1"),
                                                Price = dscheckcash.GetAsFloat(j, "PRICE"),
                                                Rule = dscheckcash.GetAsInteger(j, "CCCCASHPAYMENTH")
                                            };
                                            itelinesListExist.Add(itelines);
                                            findocsForDelete.Add(dscheckcash.GetAsInteger(j, "FINDOC"));
                                        }

                                        var similarities = itelinesListExist.Where(y => itelinesListNew.
                                        Any(z => z.Findoc == y.Findoc && z.Mtrlines == y.Mtrlines && z.Mtrl == y.Mtrl && z.Qty1 == y.Qty1 && z.Price == y.Price && z.Rule == y.Rule)).ToList();
                                        findocsForDelete = findocsForDelete.Distinct().ToList();

                                        if (similarities.Count != itelinesListExist.Count)
                                        {
                                            deleteFindoc = 1;
                                            foreach (var findocdel in findocsForDelete)
                                            {
                                                using (var PurdocObj = XSupport.CreateModule("PURDOC;LikeDefault"))
                                                {
                                                    S1Tools.HideWarningsFromS1Module(PurdocObj, XSupport);
                                                    try
                                                    {
                                                        PurdocObj.LocateData(findocdel);
                                                        PurdocObj.DeleteData();
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        MessageBox.Show(ex.Message);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            deleteFindoc = 0;
                                        }
                                    }

                                    if ((dscheckcash.Count == 0 || deleteFindoc == 1) && Trdr > 0)
                                    {
                                        var ans = 0;
                                        if (deleteFindoc == 1)
                                        {
                                            ans = 6;
                                        }
                                        else
                                        {
                                            ans = XSupport.AskYesNoCancel("Δημιουργία Πιστωτικού Cash Payment", "Βάση σεναρίου Cash Payment , θα πρέπει να δημιουργηθεί ένα νέο Πιστωτικό παραστατικό για την τρέχουσα καταχώρηση. Θέλετε να προχωρήσετε στην αυτόματη δημιουργία του?");
                                            // 6=Yes, 7=No, 2=Cancel
                                        }
                                        if (ans == 6)
                                        {
                                            using (var PurdocObjN = XSupport.CreateModule("PURDOC;LikeDefault"))
                                            {
                                                S1Tools.HideWarningsFromS1Module(PurdocObjN, XSupport);
                                                try
                                                {
                                                    PurdocObjN.InsertData();
                                                    PurdocObjN.GetTable("PURDOC").Current["SERIES"] = seriesCredit;
                                                    PurdocObjN.GetTable("PURDOC").Current["BRANCH"] = Branch;
                                                    PurdocObjN.GetTable("PURDOC").Current["TRNDATE"] = Trndate;
                                                    PurdocObjN.GetTable("PURDOC").Current["TRDR"] = Trdr;
                                                    if (TrdrBranch > 0)
                                                    {
                                                        PurdocObjN.GetTable("PURDOC").Current["TRDBRANCH"] = TrdrBranch;
                                                    }
                                                    PurdocObjN.GetTable("PURDOC").Current["CCCCASHPAYMENTH"] = rule;
                                                    PurdocObjN.GetTable("PURDOC").Current["CCCFINDOCCASH"] = PurdocId;
                                                    PurdocObjN.GetTable("PURDOC").Current["CONVMODE"] = 1;

                                                    foreach (var item in newItelinesList)
                                                    {
                                                        PurdocObjN.GetTable("ITELINES").Current.Append();
                                                        PurdocObjN.GetTable("ITELINES").Current["MTRL"] = item.Mtrl;
                                                        PurdocObjN.GetTable("ITELINES").Current["QTY1"] = item.Qty1;
                                                        PurdocObjN.GetTable("ITELINES").Current["PRICE"] = item.Price;
                                                        PurdocObjN.GetTable("ITELINES").Current["FINDOCS"] = item.Findoc;
                                                        PurdocObjN.GetTable("ITELINES").Current["MTRLINESS"] = item.Mtrlines;
                                                        PurdocObjN.GetTable("ITELINES").Current.Post();
                                                    }
                                                    var idfindoc = PurdocObjN.PostData();
                                                    findocslist.Add(idfindoc);
                                                    if (deleteFindoc == 0)
                                                    {
                                                        XSupport.Warning("Ολοκληρώθηκε η δημιουργία Πιστωτικού Cash Payment.");
                                                    }                                         
                                                }
                                                catch (Exception ex)
                                                {
                                                    MessageBox.Show(ex.Message);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //FiFo αντιστοίχιση (Πλήρης).
                        if (findocslist.Count > 0)
                        {
                            var findocs = string.Join(",", findocslist);
                            var module = XSupport.CallPublished(XSupport.GetStockObj("ModuleIntf", true), "CreateModule", new object[] { "PURDOC,WARNINGS:OFF,NOMESSAGES:1" });
                            var locate = XSupport.CallPublished(XSupport.GetStockObj("ModuleIntf", true), "LocateModule", new object[] { module, PurdocId });
                            var proglib = XSupport.CallPublished(XSupport.GetStockObj("ProgLibIntf", true), "ModuleCommand", new object[] { module, 1032, findocs });
                            //var postmodule = XSupport.CallPublished(XSupport.GetStockObj("ModuleIntf", true), "PostModule", new object[] { module });
                            //var destroymodule = XSupport.CallPublished(XSupport.GetStockObj("ModuleIntf", true), "DestroyModule", new object[] { module });
                            /*
                            foreach (var fin in findocslist)
                            {
                                XModule.Exec($"XCMD:PURDOC[AUTOLOCATE= {fin}]");
                            }
                            */
                        }
                        formappingcredit = 0;
                        base.AfterPost();
                    }
                }
            }
        }
    }
}
