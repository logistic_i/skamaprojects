﻿using System;
using System.Collections.Generic;
using System.Linq;
using Softone;
using System.Windows.Forms;
using System.Data;

namespace CreditCal
{
    [WorksOn("CCCCALCULCREDIT")]
    public class CreditCalculation : TXCode
    {
        public override void Initialize()
        {
            XModule.SetEvent("ON_CCCCALCREDITH_AFTERPOST", On_CccCalCreditH_AfterPost);
            XModule.SetEvent("ON_CCCCALCREDITH_RULES", On_CccCalCreditH_Rules);
            XModule.SetEvent("ON_CCCCALCREDITH_FROMDATE", On_CccCalCreditH_Fromdate);
            XModule.SetEvent("ON_CCCCALCREDITH_FINALDATE", On_CccCalCreditH_Finaldate);
            base.Initialize();
        }

        public DateTime fromdateRule;
        public DateTime finaldateRule;

        private void On_CccCalCreditH_Fromdate(object Sender, XEventArgs e)
        {
            var Calcredith = XModule.GetTable("CCCCALCREDITH");
            var credittype = Convert.ToInt32(Calcredith.Current["R_CREDITTYPE"]);
            var fromdate = Convert.ToDateTime(Calcredith.Current["FROMDATE"]);
            var fromdatecomparison = DateTime.Compare(fromdate, fromdateRule);

            if (Convert.ToDateTime(Calcredith.Current["FROMDATE"]) != fromdateRule)
            {
                //Δεν πρέπει να ξεπερνάνε οι ημερομηνίες τα όρια την συνθήκης που έχουμε επιλέξει.
                if (fromdatecomparison >= 0)
                {
                    if (Calcredith.Current["FINALDATE"] != DBNull.Value)
                    {
                        var finaldate = Convert.ToDateTime(Calcredith.Current["FINALDATE"]);
                        var monthdiff = (fromdate.Year - finaldate.Year) * 12 + fromdate.Month - finaldate.Month;
                        if (monthdiff <= 0)
                        {
                            switch (credittype)
                            {
                                case 1:
                                    if (monthdiff > 1)
                                    {
                                        Calcredith.Current["FROMDATE"] = fromdateRule;
                                        MessageBox.Show("Το ημερομηνιακό διάστημα που διαλέξατε είναι μεγαλύτερο από το διάστημα πιστωτικών. Επιλέξτε άλλη ημερομηνία!", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    }
                                    break;
                                case 3:
                                    if (monthdiff > 3)
                                    {
                                        Calcredith.Current["FROMDATE"] = fromdateRule;
                                        MessageBox.Show("Το ημερομηνιακό διάστημα που διαλέξατε είναι μεγαλύτερο από το διάστημα πιστωτικών. Επιλέξτε άλλη ημερομηνία!", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    }
                                    break;
                                case 6:
                                    if (monthdiff > 6)
                                    {
                                        Calcredith.Current["FROMDATE"] = fromdateRule;
                                        MessageBox.Show("Το ημερομηνιακό διάστημα που διαλέξατε είναι μεγαλύτερο από το διάστημα πιστωτικών. Επιλέξτε άλλη ημερομηνία!", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    }
                                    break;
                                case 12:
                                    if (monthdiff > 12)
                                    {
                                        Calcredith.Current["FROMDATE"] = fromdateRule;
                                        MessageBox.Show("Το ημερομηνιακό διάστημα που διαλέξατε είναι μεγαλύτερο από το διάστημα πιστωτικών. Επιλέξτε άλλη ημερομηνία!", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            Calcredith.Current["FROMDATE"] = fromdateRule;
                            MessageBox.Show("Η Ημερομηνία  Από είναι μεγαλύτερη από την Ημερομηνία Έως. Επιλέξτε άλλη ημερομηνία!", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                }
                else
                {
                    Calcredith.Current["FROMDATE"] = fromdateRule;
                    MessageBox.Show("Η Ημερομηνία Από είναι εκτός ορίων την συνθήκης πιστωτικών. Επιλέξτε άλλη ημερομηνία!", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        private void On_CccCalCreditH_Finaldate(object Sender, XEventArgs e)
        {
            var Calcredith = XModule.GetTable("CCCCALCREDITH");
            var credittype = Convert.ToInt32(Calcredith.Current["R_CREDITTYPE"]);
            var finaldate = Convert.ToDateTime(Calcredith.Current["FINALDATE"]);
            var finaldatecomparison = DateTime.Compare(finaldate, finaldateRule);

            if (Convert.ToDateTime(Calcredith.Current["FINALDATE"]) != finaldateRule)
            {
                //Δεν πρέπει να ξεπερνάνε οι ημερομηνίες τα όρια την συνθήκης που έχουμε επιλέξει.
                if (finaldatecomparison <= 0)
                {
                    if (Calcredith.Current["FROMDATE"] != DBNull.Value)
                    {
                        var fromdate = Convert.ToDateTime(Calcredith.Current["FROMDATE"]);
                        var monthdiff = (fromdate.Year - finaldate.Year) * 12 + fromdate.Month - finaldate.Month;
                        if (monthdiff <= 0)
                        {
                            switch (credittype)
                            {
                                case 1:
                                    if (monthdiff > 1)
                                    {
                                        Calcredith.Current["FINALDATE"] = finaldateRule;
                                        MessageBox.Show("Το ημερομηνιακό διάστημα που διαλέξατε είναι μεγαλύτερο από το διάστημα πιστωτικών. Επιλέξτε άλλη ημερομηνία!", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    }
                                    break;
                                case 3:
                                    if (monthdiff > 3)
                                    {
                                        Calcredith.Current["FINALDATE"] = finaldateRule;
                                        MessageBox.Show("Το ημερομηνιακό διάστημα που διαλέξατε είναι μεγαλύτερο από το διάστημα πιστωτικών. Επιλέξτε άλλη ημερομηνία!", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    }
                                    break;
                                case 6:
                                    if (monthdiff > 6)
                                    {
                                        Calcredith.Current["FINALDATE"] = finaldateRule;
                                        MessageBox.Show("Το ημερομηνιακό διάστημα που διαλέξατε είναι μεγαλύτερο από το διάστημα πιστωτικών. Επιλέξτε άλλη ημερομηνία!", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    }
                                    break;
                                case 12:
                                    if (monthdiff > 12)
                                    {
                                        Calcredith.Current["FINALDATE"] = finaldateRule;
                                        MessageBox.Show("Το ημερομηνιακό διάστημα που διαλέξατε είναι μεγαλύτερο από το διάστημα πιστωτικών. Επιλέξτε άλλη ημερομηνία!", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            Calcredith.Current["FINALDATE"] = finaldateRule;
                            MessageBox.Show("Η Ημερομηνία  Από είναι μεγαλύτερη από την Ημερομηνία Έως. Επιλέξτε άλλη ημερομηνία!", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                }
                else
                {
                    Calcredith.Current["FINALDATE"] = finaldateRule;
                    MessageBox.Show("Η Ημερομηνία Έως είναι εκτός ορίων την συνθήκης πιστωτικών. Επιλέξτε άλλη ημερομηνία!", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        private void On_CccCalCreditH_Rules(object Sender, XEventArgs e)
        {
            //βάζω στα φίλτρα της εργασίας σαν προεπιλογή το διάστημα της επιλεγμένης Συνθήκης Δεδ.Πιστωτικών
            //var Calcredith = XModule.GetTable("CCCCALCREDITH").CreateDataTable(true); //για να δο τα στοιχεία του πίνακα που επιλέγω
            var Calcredith = XModule.GetTable("CCCCALCREDITH");
            var rules = Convert.ToInt32(Calcredith.Current["RULES"]);

            var sodtypetrdr = Calcredith.Current["R_SODTYPETRDR"];
            var trdrs = "";
            var trdrdata = "SELECT T.TRDR FROM TRDR T ";
            var fintrdrs = "";
            var currentsodtypetrdr = "";
            switch (sodtypetrdr)
            {
                case 12:
                    currentsodtypetrdr = "SUPPLIER";
                    break;
                case 13:
                    currentsodtypetrdr = "CUSTOMER";
                    break;
                case 15:
                    currentsodtypetrdr = "DEBTOR";
                    break;
                case 16:
                    currentsodtypetrdr = "CREDITOR";
                    break;
                case 14:
                    currentsodtypetrdr = "BANKACC";
                    break;
            }

            var querycriteria = $@"SELECT COMPANY,CCCCREDITDATAH,SODTYPE,GROUPFIELD,GROUPTABLE,DATATABLE,ENTITYIDS,GRPFLDIDSTURNOVERPART,GRPFLDIDSCREDITPART 
                                    FROM CCCVCREDCRITERIA WHERE CCCCREDITDATAH={rules} AND SODTYPE={sodtypetrdr} AND COMPANY={XSupport.ConnectionInfo.CompanyId}";
            using (XTable dscriteria = XSupport.GetSQLDataSet(querycriteria))
            {
                if (dscriteria.Count > 0)
                {
                    for (int i = 0; i < dscriteria.Count; i++)
                    {
                        var sodtype = dscriteria.GetAsInteger(i, "SODTYPE");
                        var groupfieldidsturnoverpart = dscriteria.GetAsString(i, "GRPFLDIDSTURNOVERPART");
                        var groupfieldidscreditpart = dscriteria.GetAsString(i, "GRPFLDIDSCREDITPART");
                        var entityids = dscriteria.GetAsString(i, "ENTITYIDS");
                        var grouptable = dscriteria.GetAsString(i, "GROUPTABLE");
                        var datatable = dscriteria.GetAsString(i, "DATATABLE");

                        if (groupfieldidsturnoverpart != "" && entityids == "")
                        {
                            trdrdata = trdrdata + $" INNER JOIN (SELECT TRDR FROM {grouptable} WHERE SODTYPE={sodtype} AND {datatable} IN ({groupfieldidsturnoverpart})) T{i} ON T.TRDR=T{i}.TRDR ";
                        }
                        if (groupfieldidsturnoverpart == "" && entityids != "")
                        {
                            trdrs = $"SELECT TRDR FROM TRDR TT WHERE TT.TRDR IN ({entityids})";
                        }
                    }

                    if (trdrs != "")
                    {
                        fintrdrs = trdrs;
                    }
                    else
                    {
                        fintrdrs = trdrdata;
                    }
                }
            }

            fromdateRule = Convert.ToDateTime(Calcredith.Current["R_FROMDATE"]);
            finaldateRule = Convert.ToDateTime(Calcredith.Current["R_FINALDATE"]);

            Calcredith.Current["FROMDATE"] = Calcredith.Current["R_FROMDATE"];
            Calcredith.Current["FINALDATE"] = Calcredith.Current["R_FINALDATE"];
            XModule.SetFieldEditor("CCCCALCREDITH.TRDR", $"{currentsodtypetrdr}(W[A.TRDR IN ({fintrdrs})])");  
        }

        private void On_CccCalCreditH_AfterPost(object Sender, XEventArgs e)
        { 
            var Calcredith = XModule.GetTable("CCCCALCREDITH");
            var rules = Convert.ToInt32(Calcredith.Current["RULES"]);

            var incnegativeval = 0; //Συμμέτοχή αρνητικών τιμών στον υπολ. Πιστωτικών
            var negvalstr = $@"SELECT INCNEGATIVEVAL FROM CCCCREDITDATAH WHERE CCCCREDITDATAH = {rules}";
            using (XTable dsnegval = XSupport.GetSQLDataSet(negvalstr))
            {
                if (dsnegval.Count > 0)
                {
                    for (int i = 0; i < dsnegval.Count; i++)
                    {
                        incnegativeval = dsnegval.GetAsInteger(i, "INCNEGATIVEVAL");
                    }
                }
            }

            var trdbranchstr = "";
            var fromdate = Convert.ToDateTime(Calcredith.Current["FROMDATE"]).ToString("yyyyMMdd");
            var finaldate = Convert.ToDateTime(Calcredith.Current["FINALDATE"]).ToString("yyyyMMdd");
            var excludecredit = Convert.ToInt32(Calcredith.Current["EXCLUDECREDIT"]);
            var excludecreditfilter = "";

            if (XModule.GetTable("CCCCALCREDITH").Current["FROMDATE"] != DBNull.Value)
            {
                fromdate = $" AND F.TRNDATE >='{fromdate}'";
            }
            else
            {
                fromdate = "";
            }
            if (XModule.GetTable("CCCCALCREDITH").Current["FINALDATE"] != DBNull.Value)
            {
                finaldate = $" AND F.TRNDATE <='{finaldate}'";
            }
            else
            {
                finaldate = "";
            }

            if (excludecredit == 1)
            {
                excludecreditfilter = $" AND ISNULL(F.CCCEXCLUDECREDIT,0) <> 1 AND ISNULL(ML.CCCEXCLUDECREDIT,0) <> 1 ";
            }
            else
            {
                excludecreditfilter = "";
            }

            var restrdr = int.TryParse(Calcredith.Current["TRDR"].ToString(), out int trdr);
            var restrdbarnch = int.TryParse(Calcredith.Current["TRDBRANCH"].ToString(), out int trdbranch);

            if (XModule.GetTable("CCCCALCREDITH").Current["TRDBRANCH"] != DBNull.Value)
            {
                trdbranchstr = $" AND F.TRDBRANCH ='{trdbranch}'";
            }
            else
            {
                trdbranchstr = "";
            }

            var mtrls = "";
            var trdrs = "";
            var mtrldata = "SELECT M.MTRL FROM MTRL M WHERE ";//"SELECT M.MTRL FROM MTRL M ";
            var trdrdata = "SELECT T.TRDR FROM TRDR T WHERE "; // "SELECT T.TRDR FROM TRDR T ";
            var finmtrls = "";
            var fintrdrs = "";
            var extraselect = "";
            var extrajoins = "";
            var extrawhere = "";
            //var linesextraselect = "";
            //var linesextrajoin = "";
            var unionmtrldate = "";
            var tools = new Tools(XModule, XSupport);
            var FinalRuleList = tools.GetRuleData(rules);

            //Τύποι Υπολογισμών
            var fprms = "";
            if (FinalRuleList.FirstOrDefault()?.Fprms != "")
            {
                fprms = $" AND F.FPRMS IN({FinalRuleList.FirstOrDefault()?.Fprms})";
            }
            else
            {
                fprms = "";
            }

            //Σειρές Υπολογισμών
            var series = "";
            if (FinalRuleList.FirstOrDefault()?.Series != "")
            {
                series = $" AND F.SERIES IN({FinalRuleList.FirstOrDefault()?.Series})";
            }
            else
            {
                series = "";
            }

            //Τύποι που Αφαιρούνται
            var fprmsexc = "";
            if (FinalRuleList.FirstOrDefault()?.FprmsExc != "")
            {
                fprmsexc = $" AND F.FPRMS IN({FinalRuleList.FirstOrDefault()?.FprmsExc})";
            }
            else
            {
                fprmsexc = "";
            }
            //Σειρές που Αφαιρούνται
            var seriesexc = "";
            if (FinalRuleList.FirstOrDefault()?.SeriesExc != "")
            {
                seriesexc = $" AND F.SERIES IN({FinalRuleList.FirstOrDefault()?.SeriesExc})";
            }
            else
            {
                seriesexc = "";
            }

            //COLLECT CREDIT DATA
            try
            {
                var querycriteria = $@"SELECT COMPANY,CCCCREDITDATAH,SODTYPE,GROUPFIELD,GROUPTABLE,DATATABLE,ENTITYIDS,GRPFLDIDSTURNOVERPART,GRPFLDIDSCREDITPART,FLDIDSTURNOVERPART,FLDIDSCREDITPART 
                                    FROM CCCVCREDCRITERIA WHERE CCCCREDITDATAH={rules} AND COMPANY={XSupport.ConnectionInfo.CompanyId}";
                using (XTable dscriteria = XSupport.GetSQLDataSet(querycriteria))
                {
                    if (dscriteria.Count > 0)
                    {
                        var mtrlgroupcounter=0;

                        var filterCreditPartCase = "";
                        var cases = "";
                        var cases2 = "";

                        for (int i = 0; i < dscriteria.Count; i++)
                        {
                            var sodtype = dscriteria.GetAsInteger(i, "SODTYPE");
                            var groupfieldidsturnoverpart = dscriteria.GetAsString(i, "GRPFLDIDSTURNOVERPART");
                            var groupfieldidscreditpart = dscriteria.GetAsString(i, "GRPFLDIDSCREDITPART");
                            var fieldidsturnoverpart = dscriteria.GetAsString(i, "FLDIDSTURNOVERPART");
                            var fieldidscreditpart = dscriteria.GetAsString(i, "FLDIDSCREDITPART");
                            var entityids = dscriteria.GetAsString(i, "ENTITYIDS");
                            var grouptable = dscriteria.GetAsString(i, "GROUPTABLE");
                            var datatable = dscriteria.GetAsString(i, "DATATABLE");
                            var groupfield = dscriteria.GetAsString(i, "GROUPFIELD");

                            //Υλικά
                            if (sodtype == 51 || sodtype == 52 || sodtype == 53 || sodtype == 54 || sodtype == 61)
                            {
                                //Συμπλήρωση ομαδοποίησης και ανάλυσης ομαδοποίησης στις γραμμές του είδους
                                mtrlgroupcounter = mtrlgroupcounter + 1;
                                var linesextraselect = "";
                                var linesextrajoin = "";
                                linesextraselect = linesextraselect + $@",X{i}.{groupfield} AS GROUPFIELDID, '{grouptable}' AS GROUPTABLE ,'{datatable}' AS DATATABLE,'{groupfield}' AS GROUPFIELD";
                                linesextrajoin = linesextrajoin + $@" LEFT JOIN  {grouptable} X{i} ON M.MTRL=X{i}.MTRL LEFT JOIN {datatable} Z{i} ON X{i}.{groupfield}=Z{i}.{(groupfield != datatable ? datatable: groupfield)}";
                                
                                var mtrlgroupdata = $@"SELECT M.MTRL,M.CODE,M.NAME{linesextraselect} FROM MTRL M {linesextrajoin} WHERE M.SODTYPE IN (51,52,53,54,61) AND M.COMPANY = {XSupport.ConnectionInfo.CompanyId}";
                                unionmtrldate = unionmtrldate + (mtrlgroupcounter > 1 ? " UNION ALL " : "")+ mtrlgroupdata;

                                if (groupfieldidsturnoverpart != "" && entityids == "")
                                {
                                    //Βγάλαμε το sodtype διότι έχουμε και υπηρεσίες όχι μόνο είδη αποθήκης

                                    //mtrldata = mtrldata + $@" INNER JOIN (SELECT MTRL FROM {grouptable} 
                                    //WHERE /*SODTYPE={sodtype} AND*/ {datatable} IN ({groupfieldidsturnoverpart})) M{i} ON M.MTRL=M{i}.MTRL ";

                                    mtrldata = mtrldata + $@" {(mtrldata != "SELECT M.MTRL FROM MTRL M WHERE " ? "OR" :"")} M.MTRL IN (SELECT MTRL FROM {grouptable} WHERE {(datatable != groupfield? groupfield : datatable)} IN ({groupfieldidsturnoverpart}))";

                                    if (grouptable != "MTRL")
                                    {
                                        cases = cases + " WHEN EJ" + i + "." + datatable + " IN (" + groupfieldidscreditpart + ") THEN 1 ";

                                        cases2 = cases2 + (cases2 == "" ? cases2 : " AND ") + "EJ" + i + "." + datatable + " IN (" + groupfieldidscreditpart + ")";

                                        filterCreditPartCase = $@",(CASE{cases} ELSE 0 END ) AS FILTERCREDITPART ";

                                        extraselect = extraselect + ",EJ" + i + "." + datatable; // + filterCreditPartCase;
                                                                                                 //",(CASE WHEN EJ" + i + "." + datatable + " IN ("+ groupfieldidscreditpart + ") THEN 1 ELSE 0 END) AS FILTERCREDITPART";
                                        extrajoins = extrajoins + " LEFT JOIN " + grouptable + " EJ" + i + " ON EJ" + i + ".MTRL=Q.MTRL ";
                                        extrawhere = extrawhere + ",EJ" + i + "." + datatable;
                                    }
                                    else
                                    {
                                        cases = cases + " WHEN EJ" + i + "." + (datatable != groupfield ? groupfield : datatable) + " IN (" + groupfieldidscreditpart + ") THEN 1 ";

                                        cases2 = cases2 + (cases2 == "" ? cases2 : " AND ") + "EJ" + i + "." + (datatable != groupfield ? groupfield : datatable) + " IN (" + groupfieldidscreditpart + ")";

                                        filterCreditPartCase = $@",(CASE{cases} ELSE 0 END ) AS FILTERCREDITPART ";
                                        extraselect = extraselect + ",EJ" + i + "." + (datatable != groupfield ? groupfield : datatable);
                                        extrajoins = extrajoins + " LEFT JOIN " + grouptable + " EJ" + i + " ON EJ" + i + ".MTRL=Q.MTRL ";
                                        extrawhere = extrawhere + ",EJ" + i + "." + (datatable != groupfield ? groupfield : datatable);
                                    }
                                }
                                if (groupfieldidsturnoverpart == "" && entityids != "")
                                {
                                    cases2 = cases2 + (cases2 == "" ? cases2 : (fieldidscreditpart !=""? " AND Q.MTRL IN (" + fieldidscreditpart + ")" : ""));

                                    mtrls = $"SELECT MTRL FROM MTRL MM WHERE MM.MTRL IN ({entityids})";
                                    //05/03/2021
                                    //var str = fieldidscreditpart != ""? ",(CASE WHEN Q.MTRL IN (" + fieldidscreditpart + ") THEN 1 ELSE 0 END) AS FILTERCREDITPART" : "";
                                    var str = "";
                                    extraselect = extraselect + ",Q.MTRL" + str;
                                    extrawhere = extrawhere + ",Q.MTRL";
                                }
                            }

                            //Συναλλασσόμενοι
                            if (sodtype == 12 || sodtype == 13 || sodtype == 14 || sodtype == 15 || sodtype == 16)
                            {
                                if (groupfieldidsturnoverpart != "" && entityids == "")
                                {
                                    //trdrdata = trdrdata + $" INNER JOIN (SELECT TRDR FROM {grouptable} WHERE SODTYPE={sodtype} AND {datatable} IN ({groupfieldidsturnoverpart})) T{i} ON T.TRDR=T{i}.TRDR ";
                                    trdrdata = trdrdata + $@"{(trdrdata != "SELECT T.TRDR FROM TRDR T WHERE " ? "OR" : "")} T.TRDR IN (SELECT TRDR FROM {grouptable} WHERE SODTYPE={sodtype} AND {datatable} IN ({groupfieldidsturnoverpart}))";
                                    if (grouptable != "TRDR")
                                    {
                                        cases = cases + " WHEN EJ" + i + "." + datatable + " IN (" + groupfieldidscreditpart + ") THEN 1 ";

                                        cases2 = cases2 + (cases2 == "" ? cases2 : " AND ") + "EJ" + i + "." + datatable + " IN (" + groupfieldidscreditpart + ")";

                                        filterCreditPartCase = $@",(CASE{cases} ELSE 0 END ) AS FILTERCREDITPART ";

                                        extraselect = extraselect + ",EJ" + i + "." + datatable;
                                            //",(CASE WHEN EJ" + i + "." + datatable + " IN (" + groupfieldidscreditpart + ") THEN 1 ELSE 0 END) AS FILTERCREDITPART";
                                        extrajoins = extrajoins + " LEFT JOIN " + grouptable + " EJ" + i + " ON EJ" + i + ".TRDR=Q.TRDR ";
                                        extrawhere = extrawhere + ",EJ" + i + "." + datatable;
                                    }
                                    else
                                    {
                                        cases = cases + " WHEN EJ" + i + "." + (datatable != groupfield ? groupfield : datatable) + " IN (" + groupfieldidscreditpart + ") THEN 1 ";

                                        cases2 = cases2 + (cases2 == "" ? cases2 : " AND ") + "EJ" + i + "." + (datatable != groupfield ? groupfield : datatable) + " IN (" + groupfieldidscreditpart + ")";

                                        filterCreditPartCase = $@",(CASE{cases} ELSE 0 END ) AS FILTERCREDITPART ";
                                        extraselect = extraselect + ",EJ" + i + "." + (datatable != groupfield ? groupfield : datatable);
                                        extrajoins = extrajoins + " LEFT JOIN " + grouptable + " EJ" + i + " ON EJ" + i + ".TRDR=Q.TRDR ";
                                        extrawhere = extrawhere + ",EJ" + i + "." + (datatable != groupfield ? groupfield : datatable);
                                    }
                                }
                                if (groupfieldidsturnoverpart == "" && entityids != "")
                                {
                                    cases2 = cases2 + (cases2 == "" ? cases2 : (fieldidscreditpart != "" ? "AND Q.TRDR IN (" + fieldidscreditpart + ")":"")) ;

                                    trdrs = $"SELECT TRDR FROM TRDR TT WHERE TT.TRDR IN ({entityids})";
                                    //05/03/2021
                                    //var str = fieldidscreditpart != "" ? ",(CASE WHEN Q.TRDR IN (" + fieldidscreditpart + ") THEN 1 ELSE 0 END) AS FILTERCREDITPART" : "";
                                    var str = "";
                                    extraselect = extraselect + ",Q.TRDR" + str;
                                    extrawhere = extrawhere + ",Q.TRDR";
                                }
                            }
                        }
                        //005/32021
                        //extraselect = extraselect + filterCreditPartCase;

                        var case2f = $@",(CASE WHEN {cases2} THEN 1 ELSE 0 END ) AS FILTERCREDITPART ";
                        extraselect = extraselect + case2f;
                    }
                }
                if (mtrls != "")
                {
                    finmtrls = mtrls;
                }
                else
                {
                    finmtrls = mtrldata;
                }

                if (trdr == 0)
                {
                    if (trdrs != "")
                    {
                        fintrdrs = trdrs;
                    }
                    else
                    {
                        fintrdrs = trdrdata;
                    }
                }
                else
                {
                    fintrdrs = Convert.ToString(trdr);
                }

                // loop TRDR

                var querytrdr = $@"SELECT T.TRDR,T.CODE,T.NAME,T.AFM,T.SODTYPE FROM TRDR T WHERE COMPANY={XSupport.ConnectionInfo.CompanyId} AND T.TRDR IN ({fintrdrs})";
                using (XTable dstrdr = XSupport.GetSQLDataSet(querytrdr))
                {
                    if (dstrdr.Count > 0)
                    {
                        for (int i = 0; i < dstrdr.Count; i++)
                        {
                            // GET FINDATA
                            var currenttrdr = dstrdr.GetAsInteger(i, "TRDR");
                            var trdrinfo = dstrdr.GetAsString(i, "CODE")+" - "+ dstrdr.GetAsString(i, "NAME")+" Α.Φ.Μ.:"+ dstrdr.GetAsString(i, "AFM");
                            var currnetsodtype = dstrdr.GetAsInteger(i, "SODTYPE");
                            var queryfindocsexc = "";

                            if (fprmsexc != "" || seriesexc != "")
                            {
                                queryfindocsexc = $@"

                                                UNION ALL

                                                SELECT 
                                                Q.FINDOC,Q.MTRTRN,Q.TRDR AS CURRENTTRDR,Q.TRDBRANCH,Q.SODTYPE,Q.MTRL,SUM(ISNULL(Q.QTY*FLGHEADER,0)) AS QTY,
                                                0 AS QTY1,0 AS QTY2,0 AS PRICE,0 AS DISC1VAL,0 AS DISC2VAL,0 AS DISC3VAL,0 AS SUMDISCVAL,0 AS VATAMNT,0 AS EXPVAL
                                                ,0 AS NETLINEVAL,SUM(ISNULL(Q.NETLINEVAL*FLGHEADER,0)) AS NETLINEVALEXC,0 AS LINEVAL,SUM(ISNULL(Q.LINEVAL*FLGHEADER,0)) AS LINEVALEXC
                                                {extraselect}
                                                FROM 
                                                (SELECT F.FINDOC,M.MTRTRN,F.TRNDATE,F.FINCODE,F.SOSOURCE,F.FPRMS,F.TFPRMS,F.SERIES,F.BRANCH, F.TRDR,F.TRDBRANCH,F.PAYMENT,ML.MTRL,ML.SODTYPE,ML.VAT,ML.QTY,ML.QTY1,ML.QTY2, ML.PRICE,ML.DISC1PRC,
                                                ML.DISC1VAL,ML.DISC2PRC,ML.DISC2VAL,ML.DISC3PRC,ML.DISC3VAL, ML.VATAMNT,ML.EXPVAL,ML.NETLINEVAL,ML.LINEVAL, 
                                                CASE WHEN (m.discval*t.flg02)<>0 AND (m.discval*t.flg05)=0 THEN (m.discval*t.flg02) WHEN (m.discval*t.flg02)=0 AND (m.discval*t.flg05)<>0 
                                                THEN (m.discval*t.flg05) ELSE 0 END  AS TOTALDISCVAL, M.DISCPRC AS TOTALDISCPRC, 

                                                /*CASE WHEN ISNULL(FT.FLG01,0)>0 AND ISNULL(FT.FLG02,0)=0 
                                                THEN 1 WHEN ISNULL(FT.FLG01,0)<0 AND ISNULL(FT.FLG02,0)=0 THEN -1 WHEN ISNULL(FT.FLG01,0)=0 AND ISNULL(FT.FLG02,0)>0 THEN -1 
                                                WHEN ISNULL(FT.FLG01,0)=0 AND ISNULL(FT.FLG02,0)<0 THEN 1 ELSE 0 END AS FLGHEADER*/

                                                CASE WHEN F.SOSOURCE = 1351 
												THEN (CASE WHEN ISNULL(FT.FLG01,0)>0 AND ISNULL(FT.FLG02,0)=0 THEN 1 
												WHEN ISNULL(FT.FLG01,0)<0 AND ISNULL(FT.FLG02,0)=0 THEN -1 
												WHEN ISNULL(FT.FLG01,0)=0 AND ISNULL(FT.FLG02,0)>0 THEN -1 
                                                WHEN ISNULL(FT.FLG01,0)=0 AND ISNULL(FT.FLG02,0)<0 THEN 1 
												ELSE 0 END )*(1)
												WHEN F.SOSOURCE = 1251  THEN 
												(CASE WHEN ISNULL(FT.FLG01,0)>0 AND ISNULL(FT.FLG02,0)=0 THEN 1 
												WHEN ISNULL(FT.FLG01,0)<0 AND ISNULL(FT.FLG02,0)=0 THEN -1 
												WHEN ISNULL(FT.FLG01,0)=0 AND ISNULL(FT.FLG02,0)>0 THEN -1 
                                                WHEN ISNULL(FT.FLG01,0)=0 AND ISNULL(FT.FLG02,0)<0 THEN 1 
												ELSE 0 END )*(-1) END AS FLGHEADER

                                                FROM FINDOC F  INNER JOIN MTRDOC D ON F.FINDOC=D.FINDOC
                                                INNER JOIN MTRTRN M ON F.FINDOC=M.FINDOC INNER JOIN TRDTRN TR ON F.FINDOC=TR.FINDOC INNER JOIN TPRMS T ON M.COMPANY = T.COMPANY AND M.SODTYPE = T.SODTYPE 
                                                AND M.TPRMS=T.TPRMS INNER JOIN TPRMS FT ON TR.COMPANY = FT.COMPANY AND TR.SODTYPE = FT.SODTYPE AND TR.TPRMS=FT.TPRMS INNER JOIN MTRLINES ML ON ML.FINDOC=F.FINDOC 
                                                AND ML.MTRL=M.MTRL AND ML.MTRLINES=M.MTRTRN 
                                                WHERE 
                                                F.COMPANY={XSupport.ConnectionInfo.CompanyId} AND (FT.FLG01<>0 OR FT.FLG02<>0) AND F.ISCANCEL=0 
                                                /*AND ML.SODTYPE=51 */ 
                                                AND ML.MTRL IN ({finmtrls}) AND F.TRDR IN ({fintrdrs}) AND F.TRDR ={currenttrdr}{fromdate}{finaldate}{trdbranchstr}{fprmsexc}{seriesexc}
                                                ) Q {extrajoins}
                                                GROUP BY Q.FINDOC,Q.MTRTRN,Q.MTRL,Q.PRICE,Q.SODTYPE,Q.TRDR,Q.TRDBRANCH{extrawhere}
                                                ";
                            }

                            var queryfindocs = $@"SELECT * FROM (
                                                SELECT 
                                                Q.FINDOC,Q.MTRTRN,Q.TRDR AS CURRENTTRDR,Q.TRDBRANCH,Q.SODTYPE,Q.MTRL AS MTRLID,
                                                SUM(ISNULL(Q.QTY*FLGHEADER,0)) AS QTY,
                                                SUM(ISNULL(Q.QTY1*FLGHEADER,0)) AS QTY1,SUM(ISNULL(Q.QTY2*FLGHEADER,0)) AS QTY2,Q.PRICE,
                                                SUM(ISNULL(Q.DISC1VAL*FLGHEADER,0)) AS DISC1VAL,SUM(ISNULL(Q.DISC2VAL*FLGHEADER,0)) AS DISC2VAL,SUM(ISNULL(Q.DISC3VAL*FLGHEADER,0)) AS DISC3VAL,
                                                (SUM(ISNULL(Q.DISC1VAL*FLGHEADER,0))+SUM(ISNULL(Q.DISC2VAL*FLGHEADER,0))+SUM(ISNULL(Q.DISC3VAL*FLGHEADER,0))) AS SUMDISCVAL,
                                                SUM(ISNULL(Q.VATAMNT*FLGHEADER,0)) AS VATAMNT,SUM(ISNULL(Q.EXPVAL*FLGHEADER,0)) AS EXPVAL,SUM(ISNULL(Q.NETLINEVAL*FLGHEADER,0)) AS NETLINEVAL,0 AS NETLINEVALEXC,SUM(ISNULL(Q.LINEVAL*FLGHEADER,0)) AS LINEVAL,0 AS LINEVALEXC
                                                {extraselect}
                                                FROM 
                                                (SELECT F.FINDOC,M.MTRTRN,F.TRNDATE,F.FINCODE,F.SOSOURCE,F.FPRMS,F.TFPRMS,F.SERIES,F.BRANCH, F.TRDR,F.TRDBRANCH,F.PAYMENT,ML.MTRL,ML.SODTYPE,ML.VAT,ML.QTY,ML.QTY1,ML.QTY2, ML.PRICE,ML.DISC1PRC,
                                                ML.DISC1VAL,ML.DISC2PRC,ML.DISC2VAL,ML.DISC3PRC,ML.DISC3VAL, ML.VATAMNT,ML.EXPVAL,ML.NETLINEVAL,ML.LINEVAL, 
                                                CASE WHEN (m.discval*t.flg02)<>0 AND (m.discval*t.flg05)=0 THEN (m.discval*t.flg02) WHEN (m.discval*t.flg02)=0 AND (m.discval*t.flg05)<>0 
                                                THEN (m.discval*t.flg05) ELSE 0 END  AS TOTALDISCVAL, M.DISCPRC AS TOTALDISCPRC, 
                                                
                                                /*CASE WHEN ISNULL(FT.FLG01,0)>0 AND ISNULL(FT.FLG02,0)=0 
                                                THEN 1 WHEN ISNULL(FT.FLG01,0)<0 AND ISNULL(FT.FLG02,0)=0 THEN -1 WHEN ISNULL(FT.FLG01,0)=0 AND ISNULL(FT.FLG02,0)>0 THEN -1 
                                                WHEN ISNULL(FT.FLG01,0)=0 AND ISNULL(FT.FLG02,0)<0 THEN 1 ELSE 0 END AS FLGHEADER*/

                                                CASE WHEN F.SOSOURCE = 1351 
												THEN (CASE WHEN ISNULL(FT.FLG01,0)>0 AND ISNULL(FT.FLG02,0)=0 THEN 1 
												WHEN ISNULL(FT.FLG01,0)<0 AND ISNULL(FT.FLG02,0)=0 THEN -1 
												WHEN ISNULL(FT.FLG01,0)=0 AND ISNULL(FT.FLG02,0)>0 THEN -1 
                                                WHEN ISNULL(FT.FLG01,0)=0 AND ISNULL(FT.FLG02,0)<0 THEN 1 
												ELSE 0 END )*(1)
												WHEN F.SOSOURCE = 1251  THEN 
												(CASE WHEN ISNULL(FT.FLG01,0)>0 AND ISNULL(FT.FLG02,0)=0 THEN 1 
												WHEN ISNULL(FT.FLG01,0)<0 AND ISNULL(FT.FLG02,0)=0 THEN -1 
												WHEN ISNULL(FT.FLG01,0)=0 AND ISNULL(FT.FLG02,0)>0 THEN -1 
                                                WHEN ISNULL(FT.FLG01,0)=0 AND ISNULL(FT.FLG02,0)<0 THEN 1 
												ELSE 0 END )*(-1) END AS FLGHEADER

                                                FROM FINDOC F  
                                                INNER JOIN MTRDOC D ON F.FINDOC=D.FINDOC
                                                INNER JOIN MTRTRN M ON F.FINDOC=M.FINDOC 
                                                INNER JOIN TRDTRN TR ON F.FINDOC=TR.FINDOC INNER JOIN TPRMS T ON M.COMPANY = T.COMPANY AND M.SODTYPE = T.SODTYPE 
                                                AND M.TPRMS=T.TPRMS INNER JOIN TPRMS FT ON TR.COMPANY = FT.COMPANY AND TR.SODTYPE = FT.SODTYPE AND TR.TPRMS=FT.TPRMS INNER JOIN MTRLINES ML ON ML.FINDOC=F.FINDOC 
                                                AND ML.MTRL=M.MTRL AND ML.MTRLINES=M.MTRTRN 
                                                WHERE 
                                                F.COMPANY={XSupport.ConnectionInfo.CompanyId} AND (FT.FLG01<>0 OR FT.FLG02<>0) AND F.ISCANCEL=0 
                                                /*AND ML.SODTYPE=51 */ 
                                                AND ML.MTRL IN ({finmtrls}) AND F.TRDR IN ({fintrdrs}) AND F.TRDR ={currenttrdr}{fromdate}{finaldate}{trdbranchstr}{fprms}{series}{excludecreditfilter}
                                                ) Q {extrajoins}
                                                GROUP BY Q.FINDOC,Q.MTRTRN,Q.MTRL,Q.PRICE,Q.SODTYPE,Q.TRDR,Q.TRDBRANCH{extrawhere} 
                                                {queryfindocsexc}
                                                ) Z ORDER BY MTRLID";

                            using (XTable dsfindocs = XSupport.GetSQLDataSet(queryfindocs))
                            {
                                if (dsfindocs.Count > 0)
                                {
                                    //Είδη που συμμετέχουν στο Τζίρο
                                    var turnoverpartList = dsfindocs.CreateDataTable(true).AsEnumerable().ToList();
                                    var sumqty = turnoverpartList.Sum(x => x["QTY"] != DBNull.Value ? Convert.ToDouble(x["QTY"]) : 0);
                                    var sumqty1 = turnoverpartList.Sum(x => x["QTY1"] != DBNull.Value ? Convert.ToDouble(x["QTY1"]) : 0);
                                    var sumqty2 = turnoverpartList.Sum(x => x["QTY2"] != DBNull.Value ? Convert.ToDouble(x["QTY2"]) : 0);
                                    var sumlineval = turnoverpartList.Sum(x => x["LINEVAL"] != DBNull.Value ? Convert.ToDouble(x["LINEVAL"]) : 0);
                                    var sumdiscval = turnoverpartList.Sum(x => x["SUMDISCVAL"] != DBNull.Value ? Convert.ToDouble(x["SUMDISCVAL"]) : 0);
                                    var sumnetlineval = turnoverpartList.Sum(x => x["NETLINEVAL"] != DBNull.Value ? Convert.ToDouble(x["NETLINEVAL"]) : 0);

                                    //Είδη που συμμετέχουν στα Πιστωτικά
                                    //var creditpartList = dsfindocs.CreateDataTable(true).AsEnumerable().Where(q => Convert.ToInt32(q["FILTERCREDITPART"]) == 1).ToList();
                                    var creditpartList = dsfindocs.CreateDataTable(true).AsEnumerable().ToList();
                                    var sumqtycp = creditpartList.Where(x => Convert.ToInt32(x["FILTERCREDITPART"]) == 1).Sum(x => x["QTY"] != DBNull.Value ? Convert.ToDouble(x["QTY"]) : 0);
                                    var sumqty1cp = creditpartList.Where(x => Convert.ToInt32(x["FILTERCREDITPART"]) == 1).Sum(x => x["QTY1"] != DBNull.Value ? Convert.ToDouble(x["QTY1"]) : 0);
                                    var sumqty2cp = creditpartList.Where(x => Convert.ToInt32(x["FILTERCREDITPART"]) == 1).Sum(x => x["QTY2"] != DBNull.Value ? Convert.ToDouble(x["QTY2"]) : 0);
                                    var sumlinevalcp = creditpartList.Where(x => Convert.ToInt32(x["FILTERCREDITPART"]) == 1).Sum(x => x["LINEVAL"] != DBNull.Value ? Convert.ToDouble(x["LINEVAL"]) : 0);
                                    var sumdiscvalcp = creditpartList.Where(x => Convert.ToInt32(x["FILTERCREDITPART"]) == 1).Sum(x => x["SUMDISCVAL"] != DBNull.Value ? Convert.ToDouble(x["SUMDISCVAL"]) : 0);
                                    var sumnetlinevalcp = creditpartList.Where(x => Convert.ToInt32(x["FILTERCREDITPART"]) == 1).Sum(x => x["NETLINEVAL"] != DBNull.Value ? Convert.ToDouble(x["NETLINEVAL"]) : 0);

                                    var finalbonus = 0.0;
                                    //Μέθοδος Υπολ. Κλιμάκωσης
                                    int? scaleMethod = 0;
                                    var finalbonusval = 0.0;
                                    //Για την καλύτερη στρογγυλοποίηση των αξιών των πιστωτικών που θα δημιουργηθούν όταν έχουμε κλιμάκωση με προοδευτικό τρόπο.
                                    var ResultsForPerfectRounding = new List<DataLinesFromScale>();
                                    //Πεδίο Εφαρμογής
                                    var appfld = FinalRuleList.FirstOrDefault()?.AppFld;
                                    //Επιλογή Κλιμάκωσης
                                    var scalechoice = FinalRuleList.FirstOrDefault()?.ScaleChoice;
                                    //Πεδίο Κλιμάκωσης
                                    var scalefld = FinalRuleList.FirstOrDefault()?.ScaleFld;
                                    //Εφαρμογή Κλιμάκωσης
                                    var scaleexex = FinalRuleList.FirstOrDefault()?.ScaleExec;

                                    var scalefldvalue = 0.0;
                                    var scalefldvaluecp = 0.0;
                                    var scalefldvalueline = 0.0;
                                    var scalefldvaluelineexc = 0.0;
                                    switch (scalefld)
                                    {
                                        case 5: //Κλιμάκωση κατά ποσότητα
                                            scalefldvalue = sumqty;
                                            scalefldvaluecp = sumqtycp;
                                            break;
                                        case 6: //Κλιμάκωση κατά ποσότητα 1
                                            scalefldvalue = sumqty1;
                                            scalefldvaluecp = sumqty1cp;
                                            break;
                                        case 7: //Κλιμάκωση κατά ποσότητα 2
                                            scalefldvalue = sumqty2;
                                            scalefldvaluecp = sumqty2cp;
                                            break;
                                        case 8: //Κλιμάκωση κατά αξία
                                            scalefldvalue = sumlineval;
                                            scalefldvaluecp = sumlinevalcp;
                                            break;
                                        case 30: //Κλιμάκωση κατά καθαρή αξία
                                            scalefldvalue = sumnetlineval;
                                            scalefldvaluecp = sumnetlinevalcp;
                                            break;
                                            //case 31: //Κλιμάκωση κατά τιμή
                                            //    scalefldvalue = sumqty;
                                            //    break;
                                    }

                                    var creditDataDList = new List<CreditDataD>();
                                    var creditDataDListPreAnalysis = new List<CreditDataD>();
                                    var filresults = new List<Scale>();

                                    switch (scalechoice)
                                    {
                                        case 1: //Κλιμάκωση ανά Συναλλασσόμενο
                                            creditDataDList = FinalRuleList.Where(x => x.CreditDataD.Any(y => y.EntityId == currenttrdr && y.Sodtype== currnetsodtype))
                                                                    .SelectMany(x => x.CreditDataD.Where(y => y.EntityId == currenttrdr && y.Sodtype == currnetsodtype)).ToList();
                                            filresults = creditDataDList.SelectMany(x => x.Scale).OrderBy(x => x.ScaleValue).ToList();
                                            break;

                                        case 2: //Κλιμάκωση ανά Ομαδοποίηση Συναλλασσομένου
                                            creditDataDList = FinalRuleList.Where(x => x.CreditDataD.Any(y => y.GroupId != 0 && y.Sodtype == currnetsodtype))
                                                                    .SelectMany(x => x.CreditDataD.Where(y => y.GroupId != 0 && y.Sodtype == currnetsodtype)).ToList();
                                            filresults = creditDataDList.SelectMany(x => x.Scale).OrderBy(x => x.ScaleValue).ToList();
                                            break;

                                        case 3: //Κλιμάκωση ανά Ανάλυση Ομαδοποίησης Συναλλασσομένου
                                            creditDataDListPreAnalysis = FinalRuleList.Where(x => x.CreditDataD.Any(y => y.GroupId != 0 && y.Sodtype == currnetsodtype))
                                                                    .SelectMany(x => x.CreditDataD.Where(y => y.GroupId != 0 && y.Sodtype == currnetsodtype)).ToList();
                                            //Ανάλυση
                                            creditDataDList = creditDataDListPreAnalysis.Where(x => x.Analysis.Any(y => y.Scale.Count > 0))
                                                .SelectMany(x => x.Analysis.Where(y => y.Scale.Count > 0)).ToList();
                                            filresults = creditDataDList.SelectMany(x => x.Scale).OrderBy(x => x.ScaleValue).ToList();
                                            break;

                                        case 4: //Κλιμάκωση ανά Υλικό
                                            creditDataDList = FinalRuleList.Where(x => x.CreditDataD.Any(y => y.EntityId != 0 && (y.Sodtype == 51 || y.Sodtype == 52)))
                                                                    .SelectMany(x => x.CreditDataD.Where(y => y.EntityId != 0 && (y.Sodtype == 51 || y.Sodtype == 52))).ToList();
                                            filresults = creditDataDList.SelectMany(x => x.Scale).OrderBy(x => x.ScaleValue).ToList();
                                            break;

                                        case 5: //Κλιμάκωση ανά Ομαδοποίηση Υλικού
                                            creditDataDList = FinalRuleList.Where(x => x.CreditDataD.Any(y => y.GroupId != 0 && (y.Sodtype == 51 || y.Sodtype ==52)))
                                                                    .SelectMany(x => x.CreditDataD.Where(y => y.GroupId != 0 && (y.Sodtype == 51 || y.Sodtype == 52))).ToList();
                                            filresults = creditDataDList.SelectMany(x => x.Scale).OrderBy(x => x.ScaleValue).ToList();
                                            break;

                                        case 6: //Κλιμάκωση ανά Ανάλυση Ομαδοποίησης Υλικού
                                            creditDataDListPreAnalysis = FinalRuleList.Where(x => x.CreditDataD.Any(y => y.GroupId != 0 && (y.Sodtype == 51 || y.Sodtype == 52)))
                                                                .SelectMany(x => x.CreditDataD.Where(y => y.GroupId != 0 && (y.Sodtype == 51 || y.Sodtype == 52))).ToList();
                                            //Ανάλυση
                                            creditDataDList = creditDataDListPreAnalysis.Where(x => x.Analysis.Any(y => y.Scale.Count > 0))
                                                .SelectMany(x=>x.Analysis.Where(y => y.Scale.Count > 0)).ToList();
                                            filresults = creditDataDList.SelectMany(x => x.Scale).OrderBy(x => x.ScaleValue).ToList();
                                            break;
                                    }

                                    //create credit result data
                                    //if (finalbonus > 0.0 && dsfindocs.Count > 0)
                                    if (dsfindocs.Count > 0)
                                    {
                                        using (var TmpCredit = XSupport.CreateModule("CCCTMPCREDIT"))
                                        {
                                            TmpCredit.InsertData();
                                            TmpCredit.GetTable("CCCTMPCREDITH").Current["SODTYPE"] = currnetsodtype;
                                            TmpCredit.GetTable("CCCTMPCREDITH").Current["TRDR"] = currenttrdr;
                                            if (trdbranch > 0)
                                            {
                                                TmpCredit.GetTable("CCCTMPCREDITH").Current["TRDBRANCH"] = trdbranch;
                                            }
                                            TmpCredit.GetTable("CCCTMPCREDITH").Current["TRNDATE"] = XSupport.ConnectionInfo.LoginDate;
                                            TmpCredit.GetTable("CCCTMPCREDITH").Current["LRATE"] = 0;
                                            TmpCredit.GetTable("CCCTMPCREDITH").Current["SOCRD"] = 0;
                                            TmpCredit.GetTable("CCCTMPCREDITH").Current["SOUPD"] = 0;
                                            TmpCredit.GetTable("CCCTMPCREDITH").Current["PRCRULE"] = rules;
                                            TmpCredit.GetTable("CCCTMPCREDITH").Current["FROMDATE"] = Calcredith.Current["FROMDATE"];
                                            TmpCredit.GetTable("CCCTMPCREDITH").Current["TODATE"] = Calcredith.Current["FINALDATE"];

                                            for (int f = 0; f < creditpartList.Count; f++)
                                            {
                                                if (scalechoice == 1)//Κλιμάκωση ανά Συναλλασσόμενο
                                                {
                                                    var entity = 0;
                                                    entity = Convert.ToInt32(creditpartList[f]["TRDR"]);

                                                    //Είδη που συμμετέχουν στο Τζίρο
                                                    var filtercreditpartList = creditpartList.Where(z => Convert.ToInt32(z["TRDR"]) == Convert.ToInt32(entity)).ToList();
                                                    var sumqtyPerEntity = filtercreditpartList.Sum(x => x["QTY"] != DBNull.Value ? Convert.ToDouble(x["QTY"]) : 0);
                                                    var sumqty1PerEntity = filtercreditpartList.Sum(x => x["QTY1"] != DBNull.Value ? Convert.ToDouble(x["QTY1"]) : 0);
                                                    var sumqty2PerEntity = filtercreditpartList.Sum(x => x["QTY2"] != DBNull.Value ? Convert.ToDouble(x["QTY2"]) : 0);
                                                    var sumlinevalPerEntity = filtercreditpartList.Sum(x => x["LINEVAL"] != DBNull.Value ? Convert.ToDouble(x["LINEVAL"]) : 0);
                                                    var sumdiscvalPerEntity = filtercreditpartList.Sum(x => x["SUMDISCVAL"] != DBNull.Value ? Convert.ToDouble(x["SUMDISCVAL"]) : 0);
                                                    var sumnetlinevalPerEntity = filtercreditpartList.Sum(x => x["NETLINEVAL"] != DBNull.Value ? Convert.ToDouble(x["NETLINEVAL"]) : 0);

                                                    //Είδη που συμμετέχουν στα Πιστωτικά
                                                    var filtercreditpartListcp = creditpartList.Where(z => Convert.ToInt32(z["FILTERCREDITPART"]) == 1 && Convert.ToInt32(z["TRDR"]) == Convert.ToInt32(entity)).ToList();
                                                    var sumqtyPerEntitycp = filtercreditpartListcp.Sum(x => x["QTY"] != DBNull.Value ? Convert.ToDouble(x["QTY"]) : 0);
                                                    var sumqty1PerEntitycp = filtercreditpartListcp.Sum(x => x["QTY1"] != DBNull.Value ? Convert.ToDouble(x["QTY1"]) : 0);
                                                    var sumqty2PerEntitycp = filtercreditpartListcp.Sum(x => x["QTY2"] != DBNull.Value ? Convert.ToDouble(x["QTY2"]) : 0);
                                                    var sumlinevalPerEntitycp = filtercreditpartListcp.Sum(x => x["LINEVAL"] != DBNull.Value ? Convert.ToDouble(x["LINEVAL"]) : 0);
                                                    var sumdiscvalPerEntitycp = filtercreditpartListcp.Sum(x => x["SUMDISCVAL"] != DBNull.Value ? Convert.ToDouble(x["SUMDISCVAL"]) : 0);
                                                    var sumnetlinevalPerEntitycp = filtercreditpartListcp.Sum(x => x["NETLINEVAL"] != DBNull.Value ? Convert.ToDouble(x["NETLINEVAL"]) : 0);

                                                    switch (scalefld)
                                                    {
                                                        case 5: //Κλιμάκωση κατά ποσότητα
                                                            scalefldvalue = sumqtyPerEntity;
                                                            scalefldvaluecp = sumqtyPerEntitycp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                        case 6: //Κλιμάκωση κατά ποσότητα 1
                                                            scalefldvalue = sumqty1PerEntity;
                                                            scalefldvaluecp = sumqty1PerEntitycp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                        case 7: //Κλιμάκωση κατά ποσότητα 2
                                                            scalefldvalue = sumqty2PerEntity;
                                                            scalefldvaluecp = sumqty2PerEntitycp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                        case 8: //Κλιμάκωση κατά αξία
                                                            scalefldvalue = sumlinevalPerEntity;
                                                            scalefldvaluecp = sumlinevalPerEntitycp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                        case 30: //Κλιμάκωση κατά καθαρή αξία
                                                            scalefldvalue = sumnetlinevalPerEntity;
                                                            scalefldvaluecp = sumnetlinevalPerEntitycp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["NETLINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["NETLINEVALEXC"]);
                                                            break;
                                                        case 31: //Κλιμάκωση κατά τιμή
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                    }

                                                    //Μέθοδος Υπολ. Κλιμάκωσης // 1 = Αθροιστικά , 2 = Προοδευτικά
                                                    scaleMethod = creditDataDList.Where(x => x.EntityId == entity).FirstOrDefault()?.ScaleMethod;
                                                    if (scaleMethod != null && scaleMethod == 2)
                                                    {
                                                        var sumvalfromscale = tools.GetSumValFromScale(scalefldvalue, filresults);
                                                        finalbonusval = ((sumvalfromscale * scalefldvalueline) / scalefldvalue);

                                                        ResultsForPerfectRounding.Add(new DataLinesFromScale
                                                        {
                                                            Mtrl = Convert.ToInt32(creditpartList[f]["MTRLID"]),
                                                            Findoc = Convert.ToInt32(creditpartList[f]["FINDOC"]),
                                                            Mtrtrn = Convert.ToInt32(creditpartList[f]["MTRTRN"]),
                                                            ScaleMethod = scaleMethod,
                                                            ScaleById = entity,
                                                            ScaleFldValue = scalefldvalue,
                                                            ScaleFldValueLine = scalefldvalueline,
                                                            SumValFromScale = sumvalfromscale,
                                                            FinalBonusVal = finalbonusval
                                                        });
                                                    }
                                                    else
                                                    {
                                                        //loop scale or filresults
                                                        foreach (var item in filresults)
                                                        {
                                                            if (scaleexex == 1) // Εφαρμογή Κλιμάκωσης στον Τζίρο
                                                            {
                                                                if (scalefldvalue >= item.ScaleValue)
                                                                {
                                                                    finalbonus = item.Bonus;
                                                                }
                                                            }
                                                            if (scaleexex == 2) // Εφαρμογή Κλιμάκωσης στον Bonus
                                                            {
                                                                if (scalefldvaluecp >= item.ScaleValue)
                                                                {
                                                                    finalbonus = item.Bonus;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                if (scalechoice == 4)//Κλιμάκωση ανά Υλικό
                                                {
                                                    var entity = 0;
                                                    entity = Convert.ToInt32(creditpartList[f]["MTRL"]);

                                                    //Είδη που συμμετέχουν στο Τζίρο
                                                    var filtercreditpartList = creditpartList.Where(z => Convert.ToInt32(z["MTRL"]) == Convert.ToInt32(entity)).ToList();
                                                    var sumqtyPerEntity = filtercreditpartList.Sum(x => x["QTY"] != DBNull.Value ? Convert.ToDouble(x["QTY"]) : 0);
                                                    var sumqty1PerEntity = filtercreditpartList.Sum(x => x["QTY1"] != DBNull.Value ? Convert.ToDouble(x["QTY1"]) : 0);
                                                    var sumqty2PerEntity = filtercreditpartList.Sum(x => x["QTY2"] != DBNull.Value ? Convert.ToDouble(x["QTY2"]) : 0);
                                                    var sumlinevalPerEntity = filtercreditpartList.Sum(x => x["LINEVAL"] != DBNull.Value ? Convert.ToDouble(x["LINEVAL"]) : 0);
                                                    var sumdiscvalPerEntity = filtercreditpartList.Sum(x => x["SUMDISCVAL"] != DBNull.Value ? Convert.ToDouble(x["SUMDISCVAL"]) : 0);
                                                    var sumnetlinevalPerEntity = filtercreditpartList.Sum(x => x["NETLINEVAL"] != DBNull.Value ? Convert.ToDouble(x["NETLINEVAL"]) : 0);

                                                    //Είδη που συμμετέχουν στα Πιστωτικά
                                                    var filtercreditpartListcp = creditpartList.Where(z => Convert.ToInt32(z["FILTERCREDITPART"]) == 1 && Convert.ToInt32(z["MTRL"]) == Convert.ToInt32(entity)).ToList();
                                                    var sumqtyPerEntitycp = filtercreditpartListcp.Sum(x => x["QTY"] != DBNull.Value ? Convert.ToDouble(x["QTY"]) : 0);
                                                    var sumqty1PerEntitycp = filtercreditpartListcp.Sum(x => x["QTY1"] != DBNull.Value ? Convert.ToDouble(x["QTY1"]) : 0);
                                                    var sumqty2PerEntitycp = filtercreditpartListcp.Sum(x => x["QTY2"] != DBNull.Value ? Convert.ToDouble(x["QTY2"]) : 0);
                                                    var sumlinevalPerEntitycp = filtercreditpartListcp.Sum(x => x["LINEVAL"] != DBNull.Value ? Convert.ToDouble(x["LINEVAL"]) : 0);
                                                    var sumdiscvalPerEntitycp = filtercreditpartListcp.Sum(x => x["SUMDISCVAL"] != DBNull.Value ? Convert.ToDouble(x["SUMDISCVAL"]) : 0);
                                                    var sumnetlinevalPerEntitycp = filtercreditpartListcp.Sum(x => x["NETLINEVAL"] != DBNull.Value ? Convert.ToDouble(x["NETLINEVAL"]) : 0);

                                                    switch (scalefld)
                                                    {
                                                        case 5: //Κλιμάκωση κατά ποσότητα
                                                            scalefldvalue = sumqtyPerEntity;
                                                            scalefldvaluecp = sumqtyPerEntitycp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                        case 6: //Κλιμάκωση κατά ποσότητα 1
                                                            scalefldvalue = sumqty1PerEntity;
                                                            scalefldvaluecp = sumqty1PerEntitycp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                        case 7: //Κλιμάκωση κατά ποσότητα 2
                                                            scalefldvalue = sumqty2PerEntity;
                                                            scalefldvaluecp = sumqty2PerEntitycp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                        case 8: //Κλιμάκωση κατά αξία
                                                            scalefldvalue = sumlinevalPerEntity;
                                                            scalefldvaluecp = sumlinevalPerEntitycp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                        case 30: //Κλιμάκωση κατά καθαρή αξία
                                                            scalefldvalue = sumnetlinevalPerEntity;
                                                            scalefldvaluecp = sumnetlinevalPerEntitycp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["NETLINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["NETLINEVALEXC"]);
                                                            break;
                                                        case 31: //Κλιμάκωση κατά τιμή
                                                            //    scalefldvalue = sumqty;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                    }

                                                    var scaleByLine = creditDataDList.Where(x => x.EntityId == entity).SelectMany(x => x.Scale).OrderBy(x => x.ScaleValue).ToList();

                                                    //Μέθοδος Υπολ. Κλιμάκωσης // 1 = Αθροιστικά , 2 = Προοδευτικά
                                                    scaleMethod = creditDataDList.Where(x => x.EntityId == entity).FirstOrDefault()?.ScaleMethod;
                                                    if (scaleMethod != null && scaleMethod == 2)
                                                    {
                                                        var sumvalfromscale = tools.GetSumValFromScale(scalefldvalue, scaleByLine);
                                                        finalbonusval = ((sumvalfromscale * scalefldvalueline) / scalefldvalue);

                                                        ResultsForPerfectRounding.Add(new DataLinesFromScale
                                                        {
                                                            Mtrl = Convert.ToInt32(creditpartList[f]["MTRLID"]),
                                                            Findoc = Convert.ToInt32(creditpartList[f]["FINDOC"]),
                                                            Mtrtrn = Convert.ToInt32(creditpartList[f]["MTRTRN"]),
                                                            ScaleMethod = scaleMethod,
                                                            ScaleById = entity,
                                                            ScaleFldValue = scalefldvalue,
                                                            ScaleFldValueLine = scalefldvalueline,
                                                            SumValFromScale = sumvalfromscale,
                                                            FinalBonusVal = finalbonusval
                                                        });
                                                    }
                                                    else
                                                    {
                                                        //loop scale or scaleByLine
                                                        foreach (var item in scaleByLine)
                                                        {
                                                            if (scaleexex == 1) // Εφαρμογή Κλιμάκωσης στον Τζίρο
                                                            {
                                                                if (scalefldvalue >= item.ScaleValue)
                                                                {
                                                                    finalbonus = item.Bonus;
                                                                }
                                                            }
                                                            if (scaleexex == 2) // Εφαρμογή Κλιμάκωσης στον Bonus
                                                            {
                                                                if (scalefldvaluecp >= item.ScaleValue)
                                                                {
                                                                    finalbonus = item.Bonus;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                if (scalechoice == 5 || scalechoice == 2) //Κλιμάκωση ανά Ομαδοποίηση Υλικού //Κλιμάκωση ανά Ομαδοποίηση Συναλλασσομένου
                                                {
                                                    var groupFldId = 0;
                                                    var groupFld = "";
                                                    groupFld = creditDataDList.FirstOrDefault().GroupField;
                                                    groupFldId = Convert.ToInt32(creditpartList[f][groupFld]);
                                                    var grouplistids = creditDataDList.Where(x => x.Analysis.Any(y => y.GroupFieldId == groupFldId)).ToList();
                                                    var groupFieldIds = grouplistids.SelectMany(x => x.Analysis).Select(x => x.GroupFieldId).Distinct().ToList();

                                                    //Βρίσκω το σύνολο του στόχου για την κάθε ομαδοποίηση και κάνω loop για να δω την κλιμάκωση.
                                                    //Είδη που συμμετέχουν στο Τζίρο
                                                    var filtercreditpartList = creditpartList.Where(z => groupFieldIds.Contains(Convert.ToInt32(z[groupFld]))).ToList();
                                                    //var sumsPerGroup = filtercreditpartList.Sum(x => Convert.ToDouble(x["LINEVAL"]));
                                                    var sumqtyPerGroup = filtercreditpartList.Sum(x => x["QTY"] != DBNull.Value ? Convert.ToDouble(x["QTY"]) : 0);
                                                    var sumqty1PerGroup = filtercreditpartList.Sum(x => x["QTY1"] != DBNull.Value ? Convert.ToDouble(x["QTY1"]) : 0);
                                                    var sumqty2PerGroup = filtercreditpartList.Sum(x => x["QTY2"] != DBNull.Value ? Convert.ToDouble(x["QTY2"]) : 0);
                                                    var sumlinevalPerGroup = filtercreditpartList.Sum(x => x["LINEVAL"] != DBNull.Value ? Convert.ToDouble(x["LINEVAL"]) : 0);
                                                    var sumdiscvalPerGroup = filtercreditpartList.Sum(x => x["SUMDISCVAL"] != DBNull.Value ? Convert.ToDouble(x["SUMDISCVAL"]) : 0);
                                                    var sumnetlinevalPerGroup = filtercreditpartList.Sum(x => x["NETLINEVAL"] != DBNull.Value ? Convert.ToDouble(x["NETLINEVAL"]) : 0);

                                                    //Είδη που συμμετέχουν στα Πιστωτικά
                                                    var filtercreditpartListcp = creditpartList.Where(z => Convert.ToInt32(z["FILTERCREDITPART"]) == 1 && groupFieldIds.Contains(Convert.ToInt32(z[groupFld]))).ToList();
                                                    //var sumsPerGroupcp = filtercreditpartListcp.Sum(x => Convert.ToDouble(x["LINEVAL"]));
                                                    var sumqtyPerGroupcp = filtercreditpartListcp.Sum(x => x["QTY"] != DBNull.Value ? Convert.ToDouble(x["QTY"]) : 0);
                                                    var sumqty1PerGroupcp = filtercreditpartListcp.Sum(x => x["QTY1"] != DBNull.Value ? Convert.ToDouble(x["QTY1"]) : 0);
                                                    var sumqty2PerGroupcp = filtercreditpartListcp.Sum(x => x["QTY2"] != DBNull.Value ? Convert.ToDouble(x["QTY2"]) : 0);
                                                    var sumlinevalPerGroupcp = filtercreditpartListcp.Sum(x => x["LINEVAL"] != DBNull.Value ? Convert.ToDouble(x["LINEVAL"]) : 0);
                                                    var sumdiscvalPerGroupcp = filtercreditpartListcp.Sum(x => x["SUMDISCVAL"] != DBNull.Value ? Convert.ToDouble(x["SUMDISCVAL"]) : 0);
                                                    var sumnetlinevalPerGroupcp = filtercreditpartListcp.Sum(x => x["NETLINEVAL"] != DBNull.Value ? Convert.ToDouble(x["NETLINEVAL"]) : 0);

                                                    switch (scalefld)
                                                    {
                                                        case 5: //Κλιμάκωση κατά ποσότητα
                                                            scalefldvalue = sumqtyPerGroup;
                                                            scalefldvaluecp = sumqtyPerGroupcp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                        case 6: //Κλιμάκωση κατά ποσότητα 1
                                                            scalefldvalue = sumqty1PerGroup;
                                                            scalefldvaluecp = sumqty1PerGroupcp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                        case 7: //Κλιμάκωση κατά ποσότητα 2
                                                            scalefldvalue = sumqty2PerGroup;
                                                            scalefldvaluecp = sumqty2PerGroupcp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                        case 8: //Κλιμάκωση κατά αξία
                                                            scalefldvalue = sumlinevalPerGroup;
                                                            scalefldvaluecp = sumlinevalPerGroupcp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                        case 30: //Κλιμάκωση κατά καθαρή αξία
                                                            scalefldvalue = sumnetlinevalPerGroup;
                                                            scalefldvaluecp = sumnetlinevalPerGroupcp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["NETLINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["NETLINEVALEXC"]);
                                                            break;
                                                        case 31: //Κλιμάκωση κατά τιμή
                                                            //    scalefldvalue = sumqty;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                    }

                                                    var scaleByLine = creditDataDList.Where(x => x.Analysis.Where(q => q.GroupField == groupFld && q.GroupFieldId == groupFldId).Count() > 0)
                                                      .SelectMany(x => x.Scale).OrderBy(x => x.ScaleValue).ToList();

                                                    //Μέθοδος Υπολ. Κλιμάκωσης // 1 = Αθροιστικά , 2 = Προοδευτικά
                                                    //scaleMethod = creditDataDList.Where(x => x.GroupFieldId == groupFldId).FirstOrDefault().ScaleMethod;
                                                    scaleMethod = creditDataDList.Where(x => x.Analysis.Where(q => q.GroupField == groupFld && q.GroupFieldId == groupFldId).Count() > 0).FirstOrDefault()?.ScaleMethod;
                                                    if (scaleMethod != null && scaleMethod == 2)
                                                    {
                                                        var sumvalfromscale = tools.GetSumValFromScale(scalefldvalue, scaleByLine);
                                                        finalbonusval = ((sumvalfromscale * scalefldvalueline) / scalefldvalue);

                                                        ResultsForPerfectRounding.Add(new DataLinesFromScale
                                                        {
                                                            Mtrl = Convert.ToInt32(creditpartList[f]["MTRLID"]),
                                                            Findoc = Convert.ToInt32(creditpartList[f]["FINDOC"]),
                                                            Mtrtrn = Convert.ToInt32(creditpartList[f]["MTRTRN"]),
                                                            ScaleMethod = scaleMethod,
                                                            ScaleById = groupFldId,
                                                            ScaleFldValue = scalefldvalue,
                                                            ScaleFldValueLine = scalefldvalueline,
                                                            SumValFromScale = sumvalfromscale,
                                                            FinalBonusVal = finalbonusval
                                                        });
                                                    }
                                                    else
                                                    {
                                                        //loop scale or scaleByLine
                                                        foreach (var item in scaleByLine)
                                                        {
                                                            if (scaleexex == 1) // Εφαρμογή Κλιμάκωσης στον Τζίρο
                                                            {
                                                                if (scalefldvalue >= item.ScaleValue)
                                                                {
                                                                    finalbonus = item.Bonus;
                                                                }
                                                            }
                                                            if (scaleexex == 2) // Εφαρμογή Κλιμάκωσης στον Bonus
                                                            {
                                                                if (scalefldvaluecp >= item.ScaleValue)
                                                                {
                                                                    finalbonus = item.Bonus;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                if (scalechoice == 6 || scalechoice == 3) //Κλιμάκωση ανά Ανάλυση Ομαδοποίησης Υλικού //Κλιμάκωση ανά Ανάλυση Ομαδοποίησης Συναλλασσομένου
                                                {
                                                    var groupFldId = 0;
                                                    var groupFld = "";
                                                    groupFld = creditDataDList.FirstOrDefault().GroupField;
                                                    groupFldId = Convert.ToInt32(creditpartList[f][groupFld]);

                                                    //Ανάλυση
                                                    var grouplistids = creditDataDList.Where(y => y.GroupFieldId == groupFldId).ToList();
                                                    var groupFieldIds = grouplistids.Select(x => x.GroupFieldId).Distinct().ToList();

                                                    //Βρίσκω το σύνολο του στόχου για την κάθε ομαδοποίηση και κάνω loop για να δω την κλιμάκωση.
                                                    //Είδη που συμμετέχουν στο Τζίρο
                                                    var filtercreditpartList = creditpartList.Where(z => groupFieldIds.Contains(Convert.ToInt32(z[groupFld]))).ToList();
                                                    //var sumsPerGroup = filtercreditpartList.Sum(x => Convert.ToDouble(x["LINEVAL"]));
                                                    var sumqtyPerGroup = filtercreditpartList.Sum(x => x["QTY"] != DBNull.Value ? Convert.ToDouble(x["QTY"]) : 0);
                                                    var sumqty1PerGroup = filtercreditpartList.Sum(x => x["QTY1"] != DBNull.Value ? Convert.ToDouble(x["QTY1"]) : 0);
                                                    var sumqty2PerGroup = filtercreditpartList.Sum(x => x["QTY2"] != DBNull.Value ? Convert.ToDouble(x["QTY2"]) : 0);
                                                    var sumlinevalPerGroup = filtercreditpartList.Sum(x => x["LINEVAL"] != DBNull.Value ? Convert.ToDouble(x["LINEVAL"]) : 0);
                                                    var sumdiscvalPerGroup = filtercreditpartList.Sum(x => x["SUMDISCVAL"] != DBNull.Value ? Convert.ToDouble(x["SUMDISCVAL"]) : 0);
                                                    var sumnetlinevalPerGroup = filtercreditpartList.Sum(x => x["NETLINEVAL"] != DBNull.Value ? Convert.ToDouble(x["NETLINEVAL"]) : 0);

                                                    //Είδη που συμμετέχουν στα Πιστωτικά
                                                    var filtercreditpartListcp = creditpartList.Where(z => Convert.ToInt32(z["FILTERCREDITPART"]) == 1 && groupFieldIds.Contains(Convert.ToInt32(z[groupFld]))).ToList();
                                                    //var sumsPerGroupcp = filtercreditpartListcp.Sum(x => Convert.ToDouble(x["LINEVAL"]));
                                                    var sumqtyPerGroupcp = filtercreditpartListcp.Sum(x => x["QTY"] != DBNull.Value ? Convert.ToDouble(x["QTY"]) : 0);
                                                    var sumqty1PerGroupcp = filtercreditpartListcp.Sum(x => x["QTY1"] != DBNull.Value ? Convert.ToDouble(x["QTY1"]) : 0);
                                                    var sumqty2PerGroupcp = filtercreditpartListcp.Sum(x => x["QTY2"] != DBNull.Value ? Convert.ToDouble(x["QTY2"]) : 0);
                                                    var sumlinevalPerGroupcp = filtercreditpartListcp.Sum(x => x["LINEVAL"] != DBNull.Value ? Convert.ToDouble(x["LINEVAL"]) : 0);
                                                    var sumdiscvalPerGroupcp = filtercreditpartListcp.Sum(x => x["SUMDISCVAL"] != DBNull.Value ? Convert.ToDouble(x["SUMDISCVAL"]) : 0);
                                                    var sumnetlinevalPerGroupcp = filtercreditpartListcp.Sum(x => x["NETLINEVAL"] != DBNull.Value ? Convert.ToDouble(x["NETLINEVAL"]) : 0);

                                                    switch (scalefld)
                                                    {
                                                        case 5: //Κλιμάκωση κατά ποσότητα
                                                            scalefldvalue = sumqtyPerGroup;
                                                            scalefldvaluecp = sumqtyPerGroupcp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                        case 6: //Κλιμάκωση κατά ποσότητα 1
                                                            scalefldvalue = sumqty1PerGroup;
                                                            scalefldvaluecp = sumqty1PerGroupcp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                        case 7: //Κλιμάκωση κατά ποσότητα 2
                                                            scalefldvalue = sumqty2PerGroup;
                                                            scalefldvaluecp = sumqty2PerGroupcp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                        case 8: //Κλιμάκωση κατά αξία
                                                            scalefldvalue = sumlinevalPerGroup;
                                                            scalefldvaluecp = sumlinevalPerGroupcp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                        case 30: //Κλιμάκωση κατά καθαρή αξία
                                                            scalefldvalue = sumnetlinevalPerGroup;
                                                            scalefldvaluecp = sumnetlinevalPerGroupcp;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["NETLINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["NETLINEVALEXC"]);
                                                            break;
                                                        case 31: //Κλιμάκωση κατά τιμή
                                                            //    scalefldvalue = sumqty;
                                                            scalefldvalueline = Convert.ToDouble(creditpartList[f]["LINEVAL"]);
                                                            scalefldvaluelineexc = Convert.ToDouble(creditpartList[f]["LINEVALEXC"]);
                                                            break;
                                                    }

                                                    var scaleByLine = creditDataDList.Where(x => x.GroupField == groupFld && x.GroupFieldId == groupFldId)
                                                      .SelectMany(x => x.Scale).OrderBy(x => x.ScaleValue).ToList();

                                                    //Μέθοδος Υπολ. Κλιμάκωσης // 1 = Αθροιστικά , 2 = Προοδευτικά
                                                    scaleMethod = creditDataDList.Where(x => x.GroupFieldId == groupFldId).FirstOrDefault()?.ScaleMethod;
                                                    if (scaleMethod != null && scaleMethod == 2)
                                                    {
                                                        var sumvalfromscale = tools.GetSumValFromScale(scalefldvalue, scaleByLine);
                                                        finalbonusval = ((sumvalfromscale * scalefldvalueline) / scalefldvalue);

                                                        ResultsForPerfectRounding.Add(new DataLinesFromScale
                                                        {
                                                            Mtrl = Convert.ToInt32(creditpartList[f]["MTRLID"]),
                                                            Findoc = Convert.ToInt32(creditpartList[f]["FINDOC"]),
                                                            Mtrtrn = Convert.ToInt32(creditpartList[f]["MTRTRN"]),
                                                            ScaleMethod = scaleMethod,
                                                            ScaleById = groupFldId,
                                                            ScaleFldValue = scalefldvalue,
                                                            ScaleFldValueLine = scalefldvalueline,
                                                            SumValFromScale = sumvalfromscale,
                                                            FinalBonusVal = finalbonusval
                                                        });
                                                    }
                                                    else
                                                    {
                                                        //loop scale or scaleByLine
                                                        foreach (var item in scaleByLine)
                                                        {
                                                            if (scaleexex == 1) // Εφαρμογή Κλιμάκωσης στον Τζίρο
                                                            {
                                                                if (scalefldvalue >= item.ScaleValue)
                                                                {
                                                                    finalbonus = item.Bonus;
                                                                }
                                                            }
                                                            if (scaleexex == 2) // Εφαρμογή Κλιμάκωσης στον Bonus
                                                            {
                                                                if (scalefldvaluecp >= item.ScaleValue)
                                                                {
                                                                    finalbonus = item.Bonus;
                                                                }
                                                            }
                                                        }
                                                    }  
                                                }

                                                TmpCredit.GetTable("CCCTMPCREDITD").Current.Append();
                                                TmpCredit.GetTable("CCCTMPCREDITD").Current["TRDR"] = currenttrdr;
                                                if (trdbranch > 0)
                                                {
                                                    TmpCredit.GetTable("CCCTMPCREDITD").Current["TRDBRANCH"] = trdbranch;
                                                }
                                                TmpCredit.GetTable("CCCTMPCREDITD").Current["TRNDATE"] = XSupport.ConnectionInfo.LoginDate;
                                                TmpCredit.GetTable("CCCTMPCREDITD").Current["SODTYPE"] = Convert.ToInt32(creditpartList[f]["SODTYPE"]);//dsfindocs.GetAsInteger(f, "SODTYPE");
                                                TmpCredit.GetTable("CCCTMPCREDITD").Current["FINDOC"] = Convert.ToInt32(creditpartList[f]["FINDOC"]);
                                                TmpCredit.GetTable("CCCTMPCREDITD").Current["MTRTRN"] = Convert.ToInt32(creditpartList[f]["MTRTRN"]);
                                                TmpCredit.GetTable("CCCTMPCREDITD").Current["MTRL"] = Convert.ToInt32(creditpartList[f]["MTRLID"]);//dsfindocs.GetAsInteger(f, "MTRL");

                                                var querymtrldata = $@" SELECT G.MTRL,G.CODE,G.NAME,D.GROUPNAME,D.ANALGROUPNAME
                                                                            FROM (SELECT * FROM ({unionmtrldate}) Q 
                                                                            GROUP BY MTRL,CODE,NAME,GROUPFIELDID,GROUPTABLE,DATATABLE,GROUPFIELD ) G
                                                                            INNER JOIN 
                                                                            (SELECT CCCCREDITDATAH,ISNULL(GROUPID,0) AS GROUPID,ISNULL(ANALGROUPID,0) AS ANALGROUPID,
                                                                            ISNULL(GROUPFIELD,'') AS GROUPFIELD ,ISNULL(B.NAME,'') AS GROUPNAME,B.DATATABLE,B.GROUPTABLE,ISNULL(C.GROUPFIELDID,0) AS GROUPFIELDID,ISNULL(C.NAME,'') AS ANALGROUPNAME
                                                                            FROM CCCCREDITDATAD A 
                                                                            LEFT JOIN CCCGROUPINGH B ON B.CCCGROUPINGH=A.GROUPID
                                                                            LEFT JOIN CCCGROUPINGD C ON C.CCCGROUPINGH=B.CCCGROUPINGH AND C.CCCGROUPINGD=ANALGROUPID
                                                                            WHERE 
                                                                            A.COMPANY={XSupport.ConnectionInfo.CompanyId} AND CCCCREDITDATAH={rules} AND A.SODTYPE IN (51,52,53,54,61) AND SOREDIR=6
                                                                            ) D ON D.GROUPTABLE=G.GROUPTABLE AND D.GROUPFIELD=G.GROUPFIELD AND D.DATATABLE =G.DATATABLE AND D.GROUPFIELDID =G.GROUPFIELDID
                                                                            WHERE G.MTRL = {Convert.ToInt32(creditpartList[f]["MTRLID"])}";
                                                using (XTable dsmtrldata = XSupport.GetSQLDataSet(querymtrldata))
                                                {
                                                    if (dsmtrldata.Count > 0)
                                                    {
                                                        var groupname = "";
                                                        var analgroupname = "";
                                                        for (int d = 0; d < dsmtrldata.Count; d++)
                                                        {
                                                            groupname = groupname + (groupname != "" ? "," : "") + dsmtrldata.GetAsString(d, "GROUPNAME");
                                                            analgroupname = analgroupname + (analgroupname != "" ? "," : "") + dsmtrldata.GetAsString(d, "ANALGROUPNAME");
                                                        }
                                                        TmpCredit.GetTable("CCCTMPCREDITD").Current["GROUPNAME"] = groupname;
                                                        TmpCredit.GetTable("CCCTMPCREDITD").Current["ANALGROUPNAME"] = analgroupname;
                                                    }
                                                }


                                                TmpCredit.GetTable("CCCTMPCREDITD").Current["QTY1"] = Math.Round(Convert.ToDouble(creditpartList[f]["QTY1"]), 2);//Math.Round(dsfindocs.GetAsFloat(f, "QTY1"), 2);
                                                TmpCredit.GetTable("CCCTMPCREDITD").Current["QTY2"] = Math.Round(Convert.ToDouble(creditpartList[f]["QTY2"]), 2);//Math.Round(dsfindocs.GetAsFloat(f, "QTY2"), 2);
                                                
                                                //Επιλέγω ανάλογα αν θα πάρει την τιμή των τιμολογίων ή του πιστωτικού
                                                TmpCredit.GetTable("CCCTMPCREDITD").Current["CRDFINAMNT"] = Math.Round(scalefldvalueline, 2);
                                                TmpCredit.GetTable("CCCTMPCREDITD").Current["CRDFINAMNTEXC"] = Math.Round(scalefldvaluelineexc, 2);

                                                if (Convert.ToInt32(creditpartList[f]["FILTERCREDITPART"]) == 1)
                                                {
                                                    //Μέθοδος Υπολ. Κλιμάκωσης // 1 = Αθροιστικά , 2 = Προοδευτικά
                                                    if (scaleMethod == 2)
                                                    {
                                                        TmpCredit.GetTable("CCCTMPCREDITD").Current["CRDAMNT1"] = Math.Round(finalbonusval, 2);
                                                        if (incnegativeval == 1)
                                                        {
                                                            TmpCredit.GetTable("CCCTMPCREDITD").Current["CRDAMNT"] = Math.Round(finalbonusval, 2);
                                                        }
                                                        else
                                                        {
                                                            var lineval = Math.Round(finalbonusval, 2);
                                                            if (lineval >= 0)
                                                            {
                                                                TmpCredit.GetTable("CCCTMPCREDITD").Current["CRDAMNT"] = Math.Round(finalbonusval, 2);
                                                            }
                                                            else
                                                            {
                                                                TmpCredit.GetTable("CCCTMPCREDITD").Current["CRDAMNT"] = 0.0;
                                                            }
                                                        }
                                                        //TmpCredit.GetTable("CCCTMPCREDITD").Current["CRDAMNT1"] = Math.Round(finalbonusval, 2);
                                                        //TmpCredit.GetTable("CCCTMPCREDITD").Current["CRDAMNT"] = Math.Round(finalbonusval, 2);
                                                    }
                                                    else
                                                    {
                                                        TmpCredit.GetTable("CCCTMPCREDITD").Current["CRDAMNT1"] = Math.Round((scalefldvalueline * (finalbonus / 100)), 2);
                                                        if (incnegativeval == 1)
                                                        {
                                                            TmpCredit.GetTable("CCCTMPCREDITD").Current["CRDAMNT"] = Math.Round((scalefldvalueline * (finalbonus / 100)), 2);
                                                        }
                                                        else
                                                        {
                                                            var lineval = Math.Round((scalefldvalueline * (finalbonus / 100)), 2);
                                                            if (lineval >= 0)
                                                            {
                                                                TmpCredit.GetTable("CCCTMPCREDITD").Current["CRDAMNT"] = Math.Round((scalefldvalueline * (finalbonus / 100)), 2);
                                                            }
                                                            else
                                                            {
                                                                TmpCredit.GetTable("CCCTMPCREDITD").Current["CRDAMNT"] = 0.0;
                                                            }
                                                        }
                                                        //TmpCredit.GetTable("CCCTMPCREDITD").Current["CRDAMNT1"] = Math.Round((scalefldvalueline * (finalbonus / 100)), 2);
                                                        //TmpCredit.GetTable("CCCTMPCREDITD").Current["CRDAMNT"] = Math.Round((scalefldvalueline * (finalbonus / 100)), 2);
                                                    }
                                                }
                                                else
                                                {
                                                    TmpCredit.GetTable("CCCTMPCREDITD").Current["CRDAMNT1"] = 0.0;
                                                    TmpCredit.GetTable("CCCTMPCREDITD").Current["CRDAMNT"] = 0.0;
                                                }

                                                //αν είναι πιστωτικά που εξαιρούνται
                                                if (scalefldvaluelineexc!=0.0)
                                                {
                                                    TmpCredit.GetTable("CCCTMPCREDITD").Current["CRDAMNT"] = Math.Round(scalefldvaluelineexc, 2);
                                                }

                                                TmpCredit.GetTable("CCCTMPCREDITD").Current["DISCVAL"] = Math.Round(Convert.ToDouble(creditpartList[f]["SUMDISCVAL"]), 2);//Math.Round(dsfindocs.GetAsFloat(f, "SUMDISCVAL"), 2);
                                                TmpCredit.GetTable("CCCTMPCREDITD").Current["CALCNETVAL"] = (Math.Round(scalefldvalueline, 2)- Math.Round(Convert.ToDouble(creditpartList[f]["SUMDISCVAL"]), 2));                             
                                                TmpCredit.GetTable("CCCTMPCREDITD").Current.Post();
                                            }

                                            //Διόρθωση Στρογγυλοποίησης Αποτελεσμάτων από την Προοδευτική Κλιμάκωση
                                            Tools.GetPerfectRounding(ResultsForPerfectRounding, TmpCredit, XSupport);

                                            var ccctmpcreditd = TmpCredit.GetTable("CCCTMPCREDITD").CreateDataTable(true).AsEnumerable().ToList();
                                            var sumcrdfinamnt = ccctmpcreditd.Sum(x => x["CRDFINAMNT"] != DBNull.Value ? double.Parse(x["CRDFINAMNT"].ToString()) : 0.0);

                                            var sumcrdfinamntCalc = ccctmpcreditd
                                                .Where(x => (x["CRDAMNT1"] != DBNull.Value ? double.Parse(x["CRDAMNT1"].ToString()) : 0.0) != 0.0)
                                                .Sum(x => x["CRDFINAMNT"] != DBNull.Value ? double.Parse(x["CRDFINAMNT"].ToString()) : 0.0);

                                            var sumcrdamnt1 = ccctmpcreditd.Sum(x => x["CRDAMNT1"] != DBNull.Value ? double.Parse(x["CRDAMNT1"].ToString()) : 0.0);
                                            var sumcrdamnt = ccctmpcreditd.Sum(x => x["CRDAMNT"] != DBNull.Value ? double.Parse(x["CRDAMNT"].ToString()) : 0.0);
                                            var sumdiscvaltotal = ccctmpcreditd.Sum(x => x["DISCVAL"] != DBNull.Value ? double.Parse(x["DISCVAL"].ToString()) : 0.0);

                                            TmpCredit.GetTable("CCCTMPCREDITH").Current["CRDFINAMNT"] = Math.Round(sumcrdfinamnt, 2);
                                            TmpCredit.GetTable("CCCTMPCREDITH").Current["CRDFINAMNTCALC"] = Math.Round(sumcrdfinamntCalc, 2);
                                            TmpCredit.GetTable("CCCTMPCREDITH").Current["CRDAMNT1"] = Math.Round(sumcrdamnt1, 2);
                                            TmpCredit.GetTable("CCCTMPCREDITH").Current["CRDAMNT"] = Math.Round(sumcrdamnt, 2);
                                            TmpCredit.GetTable("CCCTMPCREDITH").Current["DISCVAL"] = Math.Round(sumdiscvaltotal, 2);

                                            //Συμπλήρωση Συγκεντρωτικών Αποτελεσμάτων Υπολογισμών
                                            Tools.GroupedCreditResults(TmpCredit, XSupport, "TmpCredit");
                                            //Συμπλήρωση Αξία προς δημιουργία πιστ/κού
                                            Tools.CalculateFinalSumCreditVal(TmpCredit, XSupport);
                                            TmpCredit.PostData(); 
                                        }
                                        MessageBox.Show($"Ολοκληρώθηκε η διαδικασία υπολογισμού πιστωτικών για τον συναλλασσόμενο {trdrinfo}.", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                                        //XModule.CloseForm();
                                    }
                                    else if (finalbonus == 0.0)
                                    {
                                        MessageBox.Show($"Ο επιλεγμένος συναλλασσόμενος ({trdrinfo}), το χρονικό διάστημα και για το σενάριο πιστωτικών που έχετε επιλέξει δεν έχει πετύχει τον στόχο του , επομένως δεν δημιουργούνται πιστωτικά για αυτόν.", "Eιδοποίηση",MessageBoxButtons.OK,MessageBoxIcon.Stop);
                                    }
                                    else if (creditpartList.Count == 0)
                                    {
                                        MessageBox.Show("Για το σενάριο πιστωτικών που έχετε επιλέξει δεν έχουν χαρακτηριστεί είδη ή κάποια ομαδοποίηση ειδών που θα συμμετέχουν στα πιστωτικά.", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    }
                                }
                                else
                                {
                                    MessageBox.Show($"Δεν υπάρχουν κινήσεις για τον συναλλασσόμενο ({trdrinfo}), το χρονικό διάστημα και για το σενάριο πιστωτικών που έχετε επιλέξει. Συμπληρώστε διαφορετικά φίλτρα στην εργασία.", "Eιδοποίηση", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                }
                            }
                        }
                        XModule.CloseForm();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}