﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditCal
{
    class DataLinesFromScale
    {
        public int Mtrl { get; set; }
        public int Findoc { get; set; }
        public int Mtrtrn { get; set; }
        public int? ScaleMethod { get; set; }
        public int ScaleById { get; set; }
        public double SumValFromScale { get; set; } //το αποτέλεσμα από την Κλιμάκωσης
        public double ScaleFldValue { get; set; } // Κλιμάκωση κατά
        public double ScaleFldValueLine { get; set; } // Κλιμάκωση κατά
        public double FinalBonusVal { get; set; }
    }
}
