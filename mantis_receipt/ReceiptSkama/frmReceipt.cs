﻿using DevExpress.Utils.Menu;
using DevExpress.XtraGrid.Views.Grid;
using Mantis.LVision.LVForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ReceiptSkama
{
    //public partial class frmReceipt : Form

    public partial class frmReceipt : LVSearchForm
    {
        public int LogisticSite { get; set; }
        public int UserId { get; set; }

        public bool IsAdmin { get; set; }

        public override bool LV_OnInit()
        {//τιμες login Mantis
            LogisticSite = this.LogisticSiteID;
            UserId = this.UserID;
            CheckIfIsAdmin();
            return true;
        }

        public frmReceipt()
        {
            InitializeComponent();
            MessageBox.Show("Τα custom εργαλεία και η ανάπτυξη κώδικα με εργαλεία τρίτων αποτελεί πνευματικά δικαιώματα της Logistic-i.", "Copyright © LOGISTIC-i 2021"
                , MessageBoxButtons.OK, MessageBoxIcon.Information);

            gridView1.RowStyle += GridView1_RowStyle;
            gridView1.OptionsClipboard.AllowCopy = DevExpress.Utils.DefaultBoolean.True;
            gridView1.PopupMenuShowing += GridView1_PopupMenuShowing;
        }

        private void CheckIfIsAdmin()
        {
            IsAdmin = true;
            var sql = $@"select case when uro_CategoryID = 3 then 1 else 0 end as isadmin from lv_users
                         inner join LV_UserRoles on uro_UserID = usr_ID
                         where usr_ID = { UserId} and usr_LogisticSiteID={LogisticSite}";

            var isadmin = Helper.FillData(sql,this.ConnectionString);
            if (isadmin.Rows.Count == 1 && isadmin.Rows[0][0].ToString() == "0")
            {
                IsAdmin = false;
                FillLogisticSites();
            }
        }

        private void FillReceipts()
        {
            var selectedCode = string.Empty;
            var inputDate = string.Empty;
            var actualDate = string.Empty;
            var supplierCode = string.Empty;
            var logisticsiteCode = string.Empty;

            if (dtInputDate.SelectedText.Length > 0)
                inputDate = " and convert(date,rct_inputdate,103)='" + Convert.ToDateTime(dtInputDate.SelectedText).ToString("yyyy-MM-dd") + "'";
            if (dtReceiptDate.SelectedText.Length > 0)
                actualDate = " and convert(date,rct_ActualDate,103)='" + Convert.ToDateTime(dtReceiptDate.SelectedText).ToString("yyyy-MM-dd") + "'";
            if (cmbReceiptsD.SelectedText.Length > 0)
                selectedCode = " and rct_code like '%" + cmbReceiptsD.SelectedText + "%'";
            if (cmbSuppliers.SelectedText.Length > 0)
            {
                var code = cmbSuppliers.SelectedText.Split('-')[0];
                supplierCode = $" and spl_code like '%{code}%'";
            }
            if (cmbLogisticUnit.SelectedText.Length > 0)
            {
                var code = cmbLogisticUnit.SelectedText.Split('-')[0];
                logisticsiteCode = $" and los_Code like '%{code}%'";
            }
            else if (Convert.ToString(cmbLogisticUnit.EditValue).Length > 0)
            {
                var code = Convert.ToString(cmbLogisticUnit.EditValue).Split('-')[0];
                logisticsiteCode = $" and los_Code like '%{code}%'";
            }
            var sql = $@"select distinct rct_id,rct_code from lv_receipt
                         inner join LV_Supplier on spl_ID=rct_SupplierID
						 inner join COM_LogisticSite on rct_LogisticSiteID=los_ID
						left join LV_ReceiptAttributesValues on rav_AttributeID=13 and rav_ReceiptID=rct_ID
						 where 1=1 and isnull(rav_Value,'02')<>'01' " + selectedCode + inputDate + actualDate + supplierCode + logisticsiteCode +
                         " order by 2";
            var receipts = Helper.FillData(sql,this.ConnectionString);
            cmbReceiptsD.Properties.Items.Clear();

            foreach (DataRow receipt in receipts.Rows)
            {
                cmbReceiptsD.Properties.Items.Add(receipt["rct_code"]);
            }
        }

        //public override bool LV_OnInit()
        //{
        //    return true;
        //}

        private void button1_Click(object sender, EventArgs e)
        {
            GetReceiptLines();
        }

        private void GetReceiptLines()
        {
            var SelectedText = cmbReceiptsD.SelectedText;
            //var sql = string.Empty;

            if (SelectedText.Length > 0)
            {
                var sql = $@"select * from am_ccccomitedqties
                          where rct_Code = '{SelectedText}'";
                var data = Helper.FillData(sql,this.ConnectionString);
                data.Columns.Add("A/A", System.Type.GetType("System.Int32")).SetOrdinal(0);
                var y = 1;
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    data.Rows[i][0] = y;
                    y++;
                }
                grdDatasource.DataSource = data;

                gridView1.Columns["rct_Code"].Visible = false;
                gridView1.Columns["ProductId"].Visible = false;
                gridView1.Columns["ExpectedQty"].Visible = false;
                gridView1.Columns["cccid"].Visible = false;

            }
            else
            {
                MessageBox.Show("Παρακαλώ, επιλέξτε μια παραλαβή!");
            }
        }

        private void GridView1_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle != -1)
            {
                var restOfQties = Convert.ToDecimal(gridView1.GetDataRow(e.RowHandle)[7]);
                if (restOfQties < 0)
                    e.Appearance.BackColor = Color.FromArgb(247, 143, 143); //red

                if (restOfQties == 0)
                    e.Appearance.BackColor = Color.FromArgb(102, 255, 178); //green

                if (restOfQties > 0)
                    e.Appearance.BackColor = Color.FromArgb(102, 204, 255); //blue

                e.HighPriority = true;
            }
        }

        private void CmbReceiptsD_Closed(object sender, DevExpress.XtraEditors.Controls.ClosedEventArgs e)
        {
            if (e.CloseMode == DevExpress.XtraEditors.PopupCloseMode.Cancel)
                cmbReceiptsD.ClosePopup();
            if (e.CloseMode == DevExpress.XtraEditors.PopupCloseMode.ButtonClick)
                cmbReceiptsD.ClosePopup();
        }

        private void dtInputDate_EditValueChanged(object sender, EventArgs e)
        {
            FillLogisticSites();
        }

        private void FillLogisticSites()
        {
            var selectedCode = string.Empty;
            var inputDate = string.Empty;
            var actualdate = string.Empty;
            var currentLogisticUnit = string.Empty;

            if (IsAdmin)
            {
                if (dtInputDate.SelectedText.Length > 0)
                    inputDate = $" and convert(date,rct_inputdate,103)='{Convert.ToDateTime(dtInputDate.SelectedText).ToString("yyyy-MM-dd")}'";
                if (dtReceiptDate.SelectedText.Length > 0)
                    actualdate = $" and convert(date,rct_ActualDate,103)='{Convert.ToDateTime(dtReceiptDate.SelectedText).ToString("yyyy-MM-dd")}'";
                if (cmbLogisticUnit.SelectedText.Length > 0)
                    selectedCode = $" and (los_Code+'-'+los_Description like '%{cmbLogisticUnit.SelectedText}%')";
            }
            else
            {
                currentLogisticUnit = $" and los_id={LogisticSite}";
            }
            var sql = $@"select distinct los_ID,los_Code+'-'+los_Description as logisticsite
                            from LV_Receipt
							inner join COM_LogisticSite on rct_LogisticSiteID=los_ID
                            where 1 = 1  " + selectedCode + inputDate + actualdate + currentLogisticUnit;
            var logisticsites = Helper.FillData(sql,this.ConnectionString);
            cmbLogisticUnit.Properties.Items.Clear();

            foreach (DataRow receipt in logisticsites.Rows)
            {
                cmbLogisticUnit.Properties.Items.Add(receipt["logisticsite"]);
            }

            if (!IsAdmin)
            {
                cmbLogisticUnit.Enabled = false;
                cmbLogisticUnit.EditValue = logisticsites.Rows[0]["logisticsite"];
                cmbLogisticUnit.Refresh();
            }
        }

        private void FillSuppliers()
        {
            var selectedCode = string.Empty;
            var inputDate = string.Empty;
            var actualdate = string.Empty;
            var selectedLogisticSite = string.Empty;

            if (dtInputDate.SelectedText.Length > 0)
                inputDate = $" and convert(date,rct_inputdate,103)='{Convert.ToDateTime(dtInputDate.SelectedText).ToString("yyyy-MM-dd")}'";
            if (dtReceiptDate.SelectedText.Length > 0)
                actualdate = $" and convert(date,rct_ActualDate,103)='{Convert.ToDateTime(dtReceiptDate.SelectedText).ToString("yyyy-MM-dd")}'";
            if (cmbSuppliers.SelectedText.Length > 0)
                selectedCode = $" and (spl_code like '%{cmbSuppliers.SelectedText}%' or cmp_FullName like '%{cmbSuppliers.SelectedText}%')";
            if (cmbLogisticUnit.SelectedText.Length > 0)
                selectedLogisticSite = $" and (los_code+'-'+los_Description like '%{cmbLogisticUnit.SelectedText}%')";

            var sql = $@"select distinct spl_ID,spl_Code + '-' + cmp_FullName as supplier
                            from LV_Receipt
                            inner join LV_Supplier on spl_ID = rct_SupplierID
                            inner join LV_Company on cmp_ID = spl_CompanyID
							inner join COM_LogisticSite on rct_LogisticSiteID=los_ID
							left join LV_ReceiptAttributesValues on rav_AttributeID=13 and rav_ReceiptID=rct_ID
						  where isnull(rav_Value,'02')<>'01' " + selectedCode + inputDate + actualdate + selectedLogisticSite;
            var suppliers = Helper.FillData(sql,this.ConnectionString);
            cmbSuppliers.Properties.Items.Clear();

            foreach (DataRow receipt in suppliers.Rows)
            {
                cmbSuppliers.Properties.Items.Add(receipt["supplier"]);
            }
        }

        private void cmbReceiptsD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            FillReceipts();
            cmbReceiptsD.ShowPopup();
        }

        private void cmbReceiptsD_KeyPress(object sender, KeyPressEventArgs e)
        {
            FillReceipts();
            cmbReceiptsD.ShowPopup();
        }

        private void cmbSuppliers_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            FillSuppliers();
            cmbSuppliers.ShowPopup();
        }

        private void cmbSuppliers_KeyPress(object sender, KeyPressEventArgs e)
        {
            FillSuppliers();
            cmbSuppliers.ShowPopup();
        }

        private void btnInitialize_Click(object sender, EventArgs e)
        {
            dtInputDate.SelectedText = string.Empty;
            dtInputDate.EditValue = null;
            dtReceiptDate.SelectedText = string.Empty;
            dtReceiptDate.EditValue = null;
            cmbReceiptsD.Properties.Items.Clear();
            cmbSuppliers.Properties.Items.Clear();
            cmbLogisticUnit.Properties.Items.Clear();
            cmbReceiptsD.SelectedText = string.Empty;
            cmbReceiptsD.EditValue = null;
            cmbSuppliers.SelectedText = string.Empty;
            cmbSuppliers.EditValue = null;
            cmbLogisticUnit.SelectedText = string.Empty;
            cmbLogisticUnit.EditValue = null;
            dtInputDate.Enabled = true;
            dtReceiptDate.Enabled = true;
            cmbSuppliers.Enabled = true;
            cmbLogisticUnit.Enabled = true;
            CheckIfIsAdmin();
            grdDatasource.DataSource = null;
            grdDatasource.Refresh();
        }

        private void cmbSuppliers_SelectedValueChanged(object sender, EventArgs e)
        {
            FillReceipts();
            cmbReceiptsD.ShowPopup();
        }

        private void cmbReceiptsD_SelectedValueChanged(object sender, EventArgs e)
        {
            var SelectedText = cmbReceiptsD.SelectedText;
            if (SelectedText.Length > 0)
            {
                var sql = $@"select spl_Code + '-' + cmp_FullName as supplier,
                            FORMAT (rct_inputdate, 'dd/MM/yyyy ') as rct_inputdate,
                            FORMAT (rct_ActualDate, 'dd/MM/yyyy ') as rct_ActualDate,los_Code+'-'+los_Description as logisticsite
                            from LV_Receipt
						    inner join LV_Supplier on spl_ID=rct_SupplierID
                            inner join LV_Company on cmp_ID = spl_CompanyID
							inner join COM_LogisticSite on rct_LogisticSiteID=los_ID
							left join LV_ReceiptAttributesValues on rav_AttributeID=13 and rav_ReceiptID=rct_ID
						  where isnull(rav_Value,'02')<>'01' and rct_Code = '{SelectedText}'";

                var receipt = Helper.FillData(sql,this.ConnectionString);

                if (receipt.Rows.Count == 0) return;

                dtInputDate.EditValue = receipt.Rows[0]["rct_inputdate"].ToString();
                dtInputDate.Refresh();
                dtInputDate.Enabled = false;
                dtReceiptDate.EditValue = receipt.Rows[0]["rct_ActualDate"].ToString();
                dtReceiptDate.Refresh();
                dtReceiptDate.Enabled = false;
                cmbSuppliers.SelectedText = receipt.Rows[0]["supplier"].ToString();
                cmbSuppliers.Refresh();
                cmbSuppliers.Enabled = false;
                cmbLogisticUnit.EditValue = receipt.Rows[0]["logisticsite"].ToString();
                cmbLogisticUnit.Refresh();
                cmbLogisticUnit.Enabled = false;
            }
        }

        private void GridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.MenuType == GridMenuType.Row && view.SelectedRowsCount > 0)
            {
                int rowHandle = e.HitInfo.RowHandle;
                e.Menu.Items.Clear();
                DXMenuItem PrintLabel = PrintItem(view, rowHandle);//Εκτύπωση Ετικετών
                e.Menu.Items.Add(PrintLabel);
                DXMenuItem PrintSN = PrintSNView(view, rowHandle);//Εκτύπωση Ετικετών Serial Number
                e.Menu.Items.Add(PrintSN);
            }
        }

        private DXMenuItem PrintItem(GridView view, int rowHandle)
        {
            DXMenuItem Print = new DXMenuItem("Εκτύπωση Ετικετών", new EventHandler(OnMenuItemPrintclick));
            return Print;
        }

        private DXMenuItem PrintSNView(GridView view, int rowHandle)
        {
            DXMenuItem Print = new DXMenuItem("Εκτύπωση S/N", new EventHandler(OnMenuItemPrintSNclick));
            return Print;
        }

        private void OnMenuItemPrintclick(object sender, EventArgs e)
        {//επιλογή εκτύπωση ετικετών
            try
            {
                var rows = gridView1.GetSelectedRows();

                if (rows.Count() == 0)
                {
                    MessageBox.Show("Δεν έχετε επιλέξει κάποια γραμμή.");
                }
                else
                {
                    this.SuspendLayout();
                    var docId = gridView1.GetRowCellValue(rows[0], "rct_Code").ToString();
                    var quantity = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(gridView1.GetRowCellValue(rows[0], "ExpectedQty"))));
                    var rowIds = GetRowIds(docId);
                    var frm = new frmPrintLabel(rowIds, quantity,1);
                    frm.ShowDialog();

                    this.ResumeLayout();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void OnMenuItemPrintSNclick(object sender, EventArgs e)
        {//επιλογή εκτύπωση ετικετών
            try
            {
                var rows = gridView1.GetSelectedRows();

                if (rows.Count() == 0)
                {
                    MessageBox.Show("Δεν έχετε επιλέξει κάποια γραμμή.");
                }
                else
                {
                    this.SuspendLayout();
                    var docId = gridView1.GetRowCellValue(rows[0], "rct_Code").ToString();
                    var quantity = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(gridView1.GetRowCellValue(rows[0], "ExpectedQty"))));
                    var rowIds = GetRowIdsSN(docId);
                    var frm = new frmPrintLabel(rowIds, quantity,2);
                    frm.ShowDialog();

                    this.ResumeLayout();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private int[] GetRowIds(string docid)
        {
            var rowIds = new List<int>();
            var rows = gridView1.GetSelectedRows();
            for (int i = 0; i < rows.Length; i++)
            {
                var productid = gridView1.GetRowCellValue(rows[i], "ProductId").ToString();
#if DEBUG
                var query = $"SELECT * FROM [S1Skama].[dbo].[am_PrintProductReceiptLbl] where rctCode='{docid}' and rciProductID={productid}";
#else
                var query = $"SELECT * FROM [Softone].[dbo].[am_PrintProductReceiptLbl] where rctCode='{docid}' and rciProductID={productid}";
#endif
                var row = Helper.FillData(query,this.ConnectionString);
                rowIds.Add(Convert.ToInt32(row.Rows[0]["ID"]));
            }
            return rowIds.ToArray();
        }

        private int[] GetRowIdsSN(string docid)
        {
            var rowIds = new List<int>();
            var rows = gridView1.GetSelectedRows();
            for (int i = 0; i < rows.Length; i++)
            {
                var productid = gridView1.GetRowCellValue(rows[i], "ProductId").ToString();
#if DEBUG
                var query = $"SELECT * FROM [S1Skama].[dbo].[am_PrintProductReceiptLbl] where rctCode='{docid}' and rciProductID={productid}";
#else
                var query = $"SELECT * FROM [Softone].[dbo].[am_PrintProductReceiptLbl] where rctCode='{docid}' and rciProductID={productid}";
#endif
                var row = Helper.FillData(query,this.ConnectionString);
                rowIds.Add(Convert.ToInt32(row.Rows[0]["CCCID"]));
            }
            return rowIds.ToArray();
        }

        private void cmbLogisticUnit_SelectedValueChanged(object sender, EventArgs e)
        {
            FillSuppliers();
        }

        private void cmbLogisticUnit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            FillLogisticSites();
            cmbLogisticUnit.ShowPopup();
        }

        private void cmbLogisticUnit_KeyPress(object sender, KeyPressEventArgs e)
        {
            FillLogisticSites();
            cmbLogisticUnit.ShowPopup();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (CheckIfStarted())
            {
                StartOrStopReceipt("1");
                MessageBox.Show("Η παραλαβή άρχισε, επιτυχώς.");
            }
            else
                MessageBox.Show("Η ΠΑΡΑΛΑΒΗ ΕΧΕΙ ΗΔΗ ΞΕΚΙΝΗΣΕΙ ή ΕΧΕΙ ΟΛΟΚΛΗΡΩΘΕΙ!");
        }

        private bool CheckIfStarted()
        {
            var sql = $@"select dbo.ValidateInsertReceipt({LogisticSiteID},'{cmbReceiptsD.SelectedText}')";
            var started = Helper.FillData(sql,this.ConnectionString);
            return started.Rows[0][0].ToString() == "1" ? true : false;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            //check before proceed
            var receiptcode = gridView1.GetRowCellValue(0, "rct_Code").ToString();
            var sql = $@"select case when count(*)>0 then 'ΔΕΝ ΕΧΕΙ ΕΙΣΑΧΘΕΙ Η ΠΟΣΟΤΗΤΑ ΣΤΟ MANTIS ΠΑΡΑΚΑΛΩ ΠΕΡΙΜΕΝΕΤΕ. ΔΟΚΙΜΑΣΤΕ ΞΑΝΑ ΑΡΓΟΤΕΡΑ.' else '0' end from CustomReceipt 
            inner join LV_Receipt on rct_ID = cr_ReceiptID
            where rct_Code = '{receiptcode}' and cr_InsertedFlag = 0";
            var proceedOrNot = Helper.GetOneValue(sql,this.ConnectionString);

            if (!proceedOrNot.Equals("0"))
            {
                this.SuspendLayout();
                MessageBox.Show(proceedOrNot);
                this.ResumeLayout();
                return;
            }
                

            this.SuspendLayout();
            DialogResult dialogResult = MessageBox.Show("Να κλείσει η παραλαβή?", "Κλείσιμο Παραλαβής", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                if (CheckIfStopped())
                {
                    WriteBeforeClose();
                    StartOrStopReceipt("3");
                    MessageBox.Show("Η παραλαβή έκλεισε, επιτυχώς.");
                }
                else
                {
                    MessageBox.Show("Η ΠΑΡΑΛΑΒΗ ΔΕΝ ΕΧΕΙ ΞΕΚΙΝΗΣΕΙ ή ΔΕΝ ΥΠΑΡΧΕΙ.");
                }
            }
            

            this.ResumeLayout();
        }

        private void WriteBeforeClose()
        {
            var sql = $@"insert into CustomClosedReceiptSSCC
                        select distinct Lsc_SSCC,stc_ID,log_receiptid,lou_LogisticSiteID,1
                        from lv_log with(nolock)
                        inner join LV_LogStock with(nolock) on lsk_LogID= log_id
                        inner join LV_Receipt with(nolock) on rct_ID= log_ReceiptID
                        inner join LV_LogStockContainer with(nolock) on Lsc_ID= lsk_FromContainerID
                        inner join LV_StockContainer with(nolock) on stc_sscc = Lsc_SSCC
                        inner join COM_LogisticUnit with(nolock) on lou_ID = lsk_LogisticUnitID
                        left outer join CustomClosedReceiptSSCC with(nolock) on ReceiptID=rct_ID
                        where log_TransactionTypeID=1
                        and rct_code = '{cmbReceiptsD.SelectedText}'
                        and SSCC is null";
            using (SqlConnection con = new SqlConnection(this.ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private bool CheckIfStopped()
        {
            var sql = $@"select  case when count(*)=0 then 0 else 1 end
                        from LV_Receipt
                        inner join CCCInsertStartReceipt on rct_id = srtc_ReceiptID
                        where srtc_transactiontype not in (2,3)  and rct_code= '{cmbReceiptsD.SelectedText}'";
            var started = Helper.FillData(sql,this.ConnectionString);
            return started.Rows[0][0].ToString() == "1";
        }

        private void StartOrStopReceipt(string Type)
        {
            using (SqlConnection con = new SqlConnection(this.ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("mp_InsertToReceiptStock", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@RctCode", SqlDbType.VarChar).Value = cmbReceiptsD.SelectedText;
                    cmd.Parameters.Add("@UserID", SqlDbType.VarChar).Value = UserId;
                    cmd.Parameters.Add("@Type", SqlDbType.VarChar).Value = Type;

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private void btnNegative_Click(object sender, EventArgs e)
        {
            GetReceiptLines();
            for (int i = gridView1.RowCount - 1; i >= 0; i--)
            {
                var restOfQties = Convert.ToInt32(gridView1.GetDataRow(i)[7]);
                if (restOfQties >= 0)
                    gridView1.DeleteRow(i);
            }
        }

        private void btnEqual_Click(object sender, EventArgs e)
        {
            GetReceiptLines();
            for (int i = gridView1.RowCount - 1; i >= 0; i--)
            {
                var restOfQties = Convert.ToInt32(gridView1.GetDataRow(i)[7]);
                if (restOfQties != 0)
                    gridView1.DeleteRow(i);
            }
        }

        private void btnPositive_Click(object sender, EventArgs e)
        {
            GetReceiptLines();
            for (int i = gridView1.RowCount - 1; i >= 0; i--)
            {
                var restOfQties = Convert.ToInt32(gridView1.GetDataRow(i)[7]);
                if (restOfQties <= 0)
                    gridView1.DeleteRow(i);
            }
        }

        private void btnPriorityReceipt_Click(object sender, EventArgs e)
        {
            try
            {
                var rows = gridView1.GetSelectedRows();
                
                    this.SuspendLayout();
                    var docId = gridView1.GetRowCellValue(rows[0], "rct_Code").ToString();
                    var frm = new frmPriorityReceipt(docId,this.ConnectionString);
                    frm.ShowDialog();

                    this.ResumeLayout();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}