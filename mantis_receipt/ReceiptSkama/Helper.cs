﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace ReceiptSkama
{
    public static class Helper
    {
        //public static string ConStr = @"Data Source=10.10.0.8\SQL2019;Initial Catalog=LVScama;Persist Security Info=True;User ID=sa;Password=qqQQ11!!";

        //public static string GetConDB()
        //{

        //    var conStr = Properties.Settings.Default.connStr;
        //    return conStr;
        //}

        internal static string GetMacAddress()
        {
            var firstMacAddress = NetworkInterface.GetAllNetworkInterfaces()
                                  .Where(x => x.OperationalStatus == OperationalStatus.Up)
                                  .Where(x => x.NetworkInterfaceType != NetworkInterfaceType.Ppp)
                                  .Where(x => x.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                                  .Select(x => x.GetPhysicalAddress().ToString())
                                  .FirstOrDefault(x => !string.IsNullOrWhiteSpace(x));
            return firstMacAddress;
        }
        public static List<string> GetPrinters()
        {

            List<string> PrinterLists = new List<string>();
            //εγκατεστημενοι εκτυπωτες στον υπολογιστη
            String pkInstalledPrinters;
            PrinterLists.Add("-------ΚΕΝΟ-----------");
            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                pkInstalledPrinters = PrinterSettings.InstalledPrinters[i];
                PrinterLists.Add(pkInstalledPrinters);
            }

            return PrinterLists;
        }

        public static DataTable FillData(string sql,string conStr)
        {
            try
            {
                var dataTable = new DataTable();
                using (SqlConnection con = new SqlConnection(conStr))
                {
                    //
                    // Open the SqlConnection.
                    //
                    con.Open();
                    //
                    // This code uses an SqlCommand based on the SqlConnection.
                    //
                    using (SqlCommand command = new SqlCommand(sql, con))
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        dataTable.Load(reader);
                    }
                    con.Close();

                    return dataTable;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static string GetOneValue(string sql, string conStr)
        {
            try
            {
                string result = string.Empty;
                using (SqlConnection con = new SqlConnection(conStr))
                {
                    //
                    // Open the SqlConnection.
                    //
                    con.Open();
                    //
                    // This code uses an SqlCommand based on the SqlConnection.
                    //
                    using (SqlCommand command = new SqlCommand(sql, con))
                    {
                        command.CommandText = sql;
                        result = command.ExecuteScalar().ToString();
                    }
                    con.Close();

                    return result;
                }

            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }

    
}
