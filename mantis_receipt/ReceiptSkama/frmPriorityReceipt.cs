﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Mantis.LVision.LVForms;

namespace ReceiptSkama
{
    public partial class frmPriorityReceipt :  LVSearchForm
    {
        public frmPriorityReceipt(string receiptCode,string constring)
        {
            InitializeComponent();
            FillData(receiptCode, constring);
        }

        private void FillData(string receiptCode,string constring)
        {
            var sql = $"select sscc from am_getpackinglistsscc where rct_Code='{receiptCode}' ";
            var dt =Helper.FillData(sql, constring);
            gridControl1.DataSource = dt;
        }
    }
}
