﻿using Mantis.LVision.Win32;
using System.Windows.Forms;

namespace ReceiptSkama
{
    partial class frmReceipt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblReceiptCode = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.cmbReceiptsD = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dtInputDate = new DevExpress.XtraEditors.DateEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtReceiptDate = new DevExpress.XtraEditors.DateEdit();
            this.cmbSuppliers = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblFilter = new System.Windows.Forms.Label();
            this.btnPositive = new DevExpress.XtraEditors.SimpleButton();
            this.btnEqual = new DevExpress.XtraEditors.SimpleButton();
            this.btnNegative = new DevExpress.XtraEditors.SimpleButton();
            this.btnStop = new DevExpress.XtraEditors.SimpleButton();
            this.btnStart = new DevExpress.XtraEditors.SimpleButton();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbLogisticUnit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnInitialize = new System.Windows.Forms.Button();
            this.grdDatasource = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnPriorityReceipt = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.cmbReceiptsD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtInputDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtInputDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReceiptDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReceiptDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSuppliers.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbLogisticUnit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdDatasource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblReceiptCode
            // 
            this.lblReceiptCode.AutoSize = true;
            this.lblReceiptCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblReceiptCode.Location = new System.Drawing.Point(851, 40);
            this.lblReceiptCode.Name = "lblReceiptCode";
            this.lblReceiptCode.Size = new System.Drawing.Size(124, 13);
            this.lblReceiptCode.TabIndex = 2;
            this.lblReceiptCode.Text = "Κωδικός Παραλαβής";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.button1.Location = new System.Drawing.Point(1110, 47);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(154, 39);
            this.button1.TabIndex = 5;
            this.button1.Text = "Ανανέωση";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmbReceiptsD
            // 
            this.cmbReceiptsD.Location = new System.Drawing.Point(854, 58);
            this.cmbReceiptsD.Name = "cmbReceiptsD";
            this.cmbReceiptsD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbReceiptsD.Size = new System.Drawing.Size(226, 20);
            this.cmbReceiptsD.TabIndex = 4;
            this.cmbReceiptsD.SelectedValueChanged += new System.EventHandler(this.cmbReceiptsD_SelectedValueChanged);
            this.cmbReceiptsD.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.cmbReceiptsD_ButtonClick);
            this.cmbReceiptsD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbReceiptsD_KeyPress);
            // 
            // dtInputDate
            // 
            this.dtInputDate.EditValue = null;
            this.dtInputDate.Location = new System.Drawing.Point(24, 58);
            this.dtInputDate.Name = "dtInputDate";
            this.dtInputDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtInputDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtInputDate.Size = new System.Drawing.Size(198, 20);
            this.dtInputDate.TabIndex = 1;
            this.dtInputDate.EditValueChanged += new System.EventHandler(this.dtInputDate_EditValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label1.Location = new System.Drawing.Point(21, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Ημερομηνία Καταχώρησης";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label2.Location = new System.Drawing.Point(225, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Ημερομηνία Παραλαβής";
            // 
            // dtReceiptDate
            // 
            this.dtReceiptDate.EditValue = null;
            this.dtReceiptDate.Location = new System.Drawing.Point(228, 58);
            this.dtReceiptDate.Name = "dtReceiptDate";
            this.dtReceiptDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtReceiptDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtReceiptDate.Size = new System.Drawing.Size(198, 20);
            this.dtReceiptDate.TabIndex = 2;
            // 
            // cmbSuppliers
            // 
            this.cmbSuppliers.Location = new System.Drawing.Point(622, 58);
            this.cmbSuppliers.Name = "cmbSuppliers";
            this.cmbSuppliers.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbSuppliers.Size = new System.Drawing.Size(226, 20);
            this.cmbSuppliers.TabIndex = 3;
            this.cmbSuppliers.SelectedValueChanged += new System.EventHandler(this.cmbSuppliers_SelectedValueChanged);
            this.cmbSuppliers.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.cmbSuppliers_ButtonClick);
            this.cmbSuppliers.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbSuppliers_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label3.Location = new System.Drawing.Point(619, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Προμηθευτής";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.btnPriorityReceipt);
            this.panel1.Controls.Add(this.lblFilter);
            this.panel1.Controls.Add(this.btnPositive);
            this.panel1.Controls.Add(this.btnEqual);
            this.panel1.Controls.Add(this.btnNegative);
            this.panel1.Controls.Add(this.btnStop);
            this.panel1.Controls.Add(this.btnStart);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cmbLogisticUnit);
            this.panel1.Controls.Add(this.btnInitialize);
            this.panel1.Controls.Add(this.cmbSuppliers);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lblReceiptCode);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.dtReceiptDate);
            this.panel1.Controls.Add(this.cmbReceiptsD);
            this.panel1.Controls.Add(this.dtInputDate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1438, 178);
            this.panel1.TabIndex = 13;
            // 
            // lblFilter
            // 
            this.lblFilter.AutoSize = true;
            this.lblFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.lblFilter.Location = new System.Drawing.Point(1133, 146);
            this.lblFilter.Name = "lblFilter";
            this.lblFilter.Size = new System.Drawing.Size(50, 13);
            this.lblFilter.TabIndex = 19;
            this.lblFilter.Text = "Φίλτρα";
            // 
            // btnPositive
            // 
            this.btnPositive.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.btnPositive.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.btnPositive.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.btnPositive.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnPositive.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.btnPositive.Appearance.Options.UseBackColor = true;
            this.btnPositive.Appearance.Options.UseBorderColor = true;
            this.btnPositive.Appearance.Options.UseFont = true;
            this.btnPositive.Appearance.Options.UseForeColor = true;
            this.btnPositive.Location = new System.Drawing.Point(1351, 141);
            this.btnPositive.Name = "btnPositive";
            this.btnPositive.Size = new System.Drawing.Size(75, 23);
            this.btnPositive.TabIndex = 18;
            this.btnPositive.Text = ">0";
            this.btnPositive.Click += new System.EventHandler(this.btnPositive_Click);
            // 
            // btnEqual
            // 
            this.btnEqual.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(255)))), ((int)(((byte)(178)))));
            this.btnEqual.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(255)))), ((int)(((byte)(178)))));
            this.btnEqual.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(255)))), ((int)(((byte)(178)))));
            this.btnEqual.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnEqual.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(255)))), ((int)(((byte)(178)))));
            this.btnEqual.Appearance.Options.UseBackColor = true;
            this.btnEqual.Appearance.Options.UseBorderColor = true;
            this.btnEqual.Appearance.Options.UseFont = true;
            this.btnEqual.Appearance.Options.UseForeColor = true;
            this.btnEqual.Location = new System.Drawing.Point(1270, 141);
            this.btnEqual.Name = "btnEqual";
            this.btnEqual.Size = new System.Drawing.Size(75, 23);
            this.btnEqual.TabIndex = 17;
            this.btnEqual.Text = "=0";
            this.btnEqual.Click += new System.EventHandler(this.btnEqual_Click);
            // 
            // btnNegative
            // 
            this.btnNegative.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(143)))), ((int)(((byte)(143)))));
            this.btnNegative.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(143)))), ((int)(((byte)(143)))));
            this.btnNegative.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(143)))), ((int)(((byte)(143)))));
            this.btnNegative.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnNegative.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(143)))), ((int)(((byte)(143)))));
            this.btnNegative.Appearance.Options.UseBackColor = true;
            this.btnNegative.Appearance.Options.UseBorderColor = true;
            this.btnNegative.Appearance.Options.UseFont = true;
            this.btnNegative.Appearance.Options.UseForeColor = true;
            this.btnNegative.Location = new System.Drawing.Point(1189, 141);
            this.btnNegative.Name = "btnNegative";
            this.btnNegative.Size = new System.Drawing.Size(75, 23);
            this.btnNegative.TabIndex = 16;
            this.btnNegative.Text = "<0";
            this.btnNegative.Click += new System.EventHandler(this.btnNegative_Click);
            // 
            // btnStop
            // 
            this.btnStop.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Danger;
            this.btnStop.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnStop.Appearance.Options.UseBackColor = true;
            this.btnStop.Appearance.Options.UseFont = true;
            this.btnStop.Location = new System.Drawing.Point(1272, 96);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(154, 39);
            this.btnStop.TabIndex = 15;
            this.btnStop.Text = "ΚΛΕΙΣΙΜΟ ΠΑΡΑΛΑΒΗΣ";
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStart
            // 
            this.btnStart.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.btnStart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnStart.Appearance.Options.UseBackColor = true;
            this.btnStart.Appearance.Options.UseFont = true;
            this.btnStart.Location = new System.Drawing.Point(1112, 96);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(154, 39);
            this.btnStart.TabIndex = 14;
            this.btnStart.Text = "ΕΝΑΡΞΗ ΠΑΡΑΛΑΒΗΣ";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label4.Location = new System.Drawing.Point(429, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Επιχειρησιακή Μονάδα";
            // 
            // cmbLogisticUnit
            // 
            this.cmbLogisticUnit.Location = new System.Drawing.Point(432, 58);
            this.cmbLogisticUnit.Name = "cmbLogisticUnit";
            this.cmbLogisticUnit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbLogisticUnit.Size = new System.Drawing.Size(184, 20);
            this.cmbLogisticUnit.TabIndex = 13;
            this.cmbLogisticUnit.SelectedValueChanged += new System.EventHandler(this.cmbLogisticUnit_SelectedValueChanged);
            this.cmbLogisticUnit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.cmbLogisticUnit_ButtonClick);
            this.cmbLogisticUnit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbLogisticUnit_KeyPress);
            // 
            // btnInitialize
            // 
            this.btnInitialize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnInitialize.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.btnInitialize.Location = new System.Drawing.Point(1270, 47);
            this.btnInitialize.Name = "btnInitialize";
            this.btnInitialize.Size = new System.Drawing.Size(154, 39);
            this.btnInitialize.TabIndex = 6;
            this.btnInitialize.Text = "Αρχικοποίηση";
            this.btnInitialize.UseVisualStyleBackColor = false;
            this.btnInitialize.Click += new System.EventHandler(this.btnInitialize_Click);
            // 
            // grdDatasource
            // 
            this.grdDatasource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDatasource.Location = new System.Drawing.Point(0, 178);
            this.grdDatasource.MainView = this.gridView1;
            this.grdDatasource.Name = "grdDatasource";
            this.grdDatasource.Size = new System.Drawing.Size(1438, 584);
            this.grdDatasource.TabIndex = 14;
            this.grdDatasource.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.grdDatasource;
            this.gridView1.Name = "gridView1";
            // 
            // btnPriorityReceipt
            // 
            this.btnPriorityReceipt.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnPriorityReceipt.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnPriorityReceipt.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnPriorityReceipt.Appearance.Options.UseBackColor = true;
            this.btnPriorityReceipt.Appearance.Options.UseFont = true;
            this.btnPriorityReceipt.Location = new System.Drawing.Point(854, 96);
            this.btnPriorityReceipt.Name = "btnPriorityReceipt";
            this.btnPriorityReceipt.Size = new System.Drawing.Size(226, 39);
            this.btnPriorityReceipt.TabIndex = 20;
            this.btnPriorityReceipt.Text = "ΠΡΟΤΕΡΑΙΟΤΗΤΑ ΠΑΡΑΛΑΒΗΣ";
            this.btnPriorityReceipt.Click += new System.EventHandler(this.btnPriorityReceipt_Click);
            // 
            // frmReceipt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1438, 762);
            this.Controls.Add(this.grdDatasource);
            this.Controls.Add(this.panel1);
            this.Name = "frmReceipt";
            this.Text = "Διαλογή Ποσοτήτων Παραλαβών";
            ((System.ComponentModel.ISupportInitialize)(this.cmbReceiptsD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtInputDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtInputDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReceiptDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtReceiptDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSuppliers.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbLogisticUnit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdDatasource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Label lblReceiptCode;
        private Button button1;
        private DevExpress.XtraEditors.ComboBoxEdit cmbReceiptsD;
        private DevExpress.XtraEditors.DateEdit dtInputDate;
        private Label label1;
        private Label label2;
        private DevExpress.XtraEditors.DateEdit dtReceiptDate;
        private DevExpress.XtraEditors.ComboBoxEdit cmbSuppliers;
        private Label label3;
        private Panel panel1;
        private Button btnInitialize;
        private Label label4;
        private DevExpress.XtraEditors.ComboBoxEdit cmbLogisticUnit;
        private DevExpress.XtraEditors.SimpleButton btnStop;
        private DevExpress.XtraEditors.SimpleButton btnStart;
        private Label lblFilter;
        private DevExpress.XtraEditors.SimpleButton btnPositive;
        private DevExpress.XtraEditors.SimpleButton btnEqual;
        private DevExpress.XtraEditors.SimpleButton btnNegative;
        private DevExpress.XtraGrid.GridControl grdDatasource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnPriorityReceipt;
    }
}