﻿using Mantis.LVision.LVForms;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace ReceiptSkama
{
    //public partial class frmPrintLabel : Form
    public partial class frmPrintLabel : LVSearchForm
    {
        public int[] _rowIds { get; set; }

        public int PrintType { get; set; } //1:εκτυπωση ταμπελακι 2: εκτυπωση serial number

        public frmPrintLabel(int[] rowIds, int qty, int printype)
        {
            InitializeComponent();
            cmbPrinters.Properties.Items.AddRange(Helper.GetPrinters());
            _rowIds = rowIds;
            txtQty.Text = qty.ToString();
            PrintType = printype;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (PrintType == 1)
            {
                PrintData();
            }
            else if (PrintType == 2)
            {
                PrintSNData();
            }
        }

        public void PrintData()
        {//εκτυπωση ετικετων
            try

            {
                var ValidPrinter = cmbPrinters.SelectedText;
                if (string.IsNullOrEmpty(ValidPrinter))
                {
                    MessageBox.Show("Δεν Έχετε Ορίσει Εκτυπωτή Για Ετικέτες!");
                }
                else
                {
                    var rowIds = string.Join<int>(";", _rowIds);
                    var MantisPath = Path.GetDirectoryName(Application.ExecutablePath);
                    var fileName = $"{MantisPath}\\MantisPrint\\MantisPrint.exe";
                    var args = $"{rowIds} \"{ValidPrinter}\" 998 {txtQty.Text} -1";

                    //εκτέλεση του MantisPrint.exe
                    this.SuspendLayout();
                    Process proc = new Process();
                    proc.StartInfo.FileName = fileName;
                    proc.StartInfo.Arguments = args;
                    proc.Start();
                    this.ResumeLayout();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void PrintSNData()
        {//εκτυπωση ετικετων
            try

            {
                var ValidPrinter = cmbPrinters.SelectedText;
                if (string.IsNullOrEmpty(ValidPrinter))
                {
                    MessageBox.Show("Δεν Έχετε Ορίσει Εκτυπωτή Για Ετικέτες!");
                }
                else
                {
                    var rowIds = string.Join<int>(";", _rowIds);
                    var MantisPath = Path.GetDirectoryName(Application.ExecutablePath);
                    var fileName = $"{MantisPath}\\MantisPrint\\MantisPrint.exe";
                    var args = $"{rowIds} \"{ValidPrinter}\" 997 {txtQty.Text} -1";

                    //εκτέλεση του MantisPrint.exe
                    this.SuspendLayout();
                    Process proc = new Process();
                    proc.StartInfo.FileName = fileName;
                    proc.StartInfo.Arguments = args;
                    proc.Start();
                    this.ResumeLayout();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}