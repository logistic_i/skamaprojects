﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Softone;
//using System.Linq;

namespace CalcSalPrices
{
    [WorksOn("CCCCALCSALPRICE")] //Υπολογισμός τιμών πώλησης
    public class PriceCalculation :TXCode
    {
        //FrmPrices frmPrices = null;
        public override void Initialize()
        {
            XModule.SetEvent("ON_CCCCALCSALPRICED_BEFOREDELETE", On_CccCalcSalPriceD_BeforeDelete);
            XModule.SetEvent("ON_CCCCALCSALPRICEH_BEAPPROVED", On_CccCalcSalPriceH_BeApproved);
            XModule.SetEvent("ON_CCCCALCSALPRICEH_SELECTALL", On_CccCalcSalPriceH_SelectAll);
            //XModule.SetEvent("ON_CCCCALCSALPRICEH_STATUS", On_CccCalcSalPriceH_Status);
            XModule.SetEvent("ON_CCCCALCSALPRICEH_TYPEPRICE", On_CccCalcSalPriceH_TypePrice);
            XModule.FieldColor("CCCCALCSALPRICED.ISCHANGE", 13421823);
            XModule.FieldColor("CCCCALCSALPRICED.CALMARKUPW", 13434829);
            XModule.FieldColor("CCCCALCSALPRICED.NMARKUPW", 1769242);
            XModule.FieldColor("CCCCALCSALPRICED.CALMARKUPR", 16777164);
            XModule.FieldColor("CCCCALCSALPRICED.NMARKUPR", 16776986);

            /*
            //ME XRISI WINDOWS FORM
            frmPrices = new FrmPrices(XSupport, XModule);
            XModule.InsertControl(frmPrices, "*PANEL(pricePanel, Δεδομένα τιμών)");
            //pricePanel
            S1Tools.SetPropertyCustom(XModule, "PANEL", "pricePanel", "VISIBLE", "FALSE");
            */
            base.Initialize();
        }

        public override void AfterInsert()
        {
            S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelGridSel", "VISIBLE", "FALSE");
            S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelPrTrdr", "VISIBLE", "FALSE");
            S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelApproval", "VISIBLE", "FALSE");

            S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.STATUS", "READONLY", "FALSE");
            S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.TYPEPRICE", "READONLY", "FALSE");
            S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.TRNDATEISACT", "READONLY", "FALSE");
            S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.FINDOC", "READONLY", "FALSE");
            S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.FROMDATEL", "READONLY", "FALSE");
            S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.FROMDATEH", "READONLY", "FALSE");
            S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.PRCRULE", "READONLY", "FALSE");
            S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.TRDRPRCRULE", "READONLY", "FALSE");
            //S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.USERAPPROVAL", "READONLY", "FALSE");
            //S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.RSNAPPROVAL", "READONLY", "FALSE");
            S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.BEAPPROVED", "READONLY", "FALSE");
            S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.SELECTALL", "READONLY", "FALSE");

            S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.NMARKUPW", "READONLY", "FALSE");
            S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.NMARKUPR", "READONLY", "FALSE");
            S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.ISCHANGE", "READONLY", "FALSE");

            XModule.SetFieldEditor("CCCCALCSALPRICEH.STATUS", "$STATUSLIST(W[1,2,5])");

            base.AfterInsert();
        }

        public override void BeforePost()
        {
            var CalcSalPrisceH = XModule.GetTable("CCCCALCSALPRICEH");
            CalcSalPrisceH.Current["UPDDATE"] = DateTime.Now;
            CalcSalPrisceH.Current["UPDUSER"] = XSupport.ConnectionInfo.UserId;
            base.BeforePost();
        }

        public override void AfterLocate()
        {
            //S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.TRNDATEISACT", "READONLY", "TRUE");
            var CalcSalPrisceH = XModule.GetTable("CCCCALCSALPRICEH");
            var status = Convert.ToInt32(CalcSalPrisceH.Current["STATUS"]);
            var prcruleFilter = Convert.ToInt32(CalcSalPrisceH.Current["PRCRULE"] != DBNull.Value ? CalcSalPrisceH.Current["PRCRULE"] : 0);
            var prcruleTrdrFilter = Convert.ToInt32(CalcSalPrisceH.Current["TRDRPRCRULE"] != DBNull.Value ? CalcSalPrisceH.Current["TRDRPRCRULE"] : 0);
            S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelGridSel", "VISIBLE", "TRUE");

            var newlisteditor = "";
            switch (status)
            {
                case 1: //Επεξεργασία
                    newlisteditor = "$STATUSLIST(W[1,2,5])";
                    break;
                case 2: //Προς έγκριση
                    newlisteditor = "$STATUSLIST(W[2,3,5])";
                    break;
                case 3: //Εγκρίθηκε
                    newlisteditor = "$STATUSLIST(W[3,5])";
                    break;
                case 4: //Ενημερώθηκε
                    newlisteditor = "$STATUSLIST(W[4])";
                    break;
                case 5: //Ακυρώθηκε
                    newlisteditor = "$STATUSLIST(W[5])";
                    break;
            }
            XModule.SetFieldEditor("CCCCALCSALPRICEH.STATUS", newlisteditor);

            var insUser = XSupport.ConnectionInfo.UserId;
            //var checkquery = $@"SELECT ISNULL(CCCUSERAPPROVAL,0) AS CCCUSERAPPROVAL FROM USERS WHERE USERS ={insUser}";
            var checkquery = $@"SELECT PRSN,CODE,NAME,NAME2,USERS,ISNULL(CCCPRSNAPPROVAL,0) AS CCCPRSNAPPROVAL FROM PRSN
                                WHERE COMPANY={XSupport.ConnectionInfo.CompanyId} AND ISACTIVE=1 AND SODTYPE=20 AND USERS ={insUser} AND ISNULL(CCCPRSNAPPROVAL,0) =1";
            //var isuserapproval = 0;
            var isprsnapproval = 0;
            using (XTable checkds = XSupport.GetSQLDataSet(checkquery))
            {
                if (checkds.Count > 0)
                {
                    //isuserapproval = checkds.GetAsInteger(0, "CCCUSERAPPROVAL");
                    isprsnapproval = checkds.GetAsInteger(0, "CCCPRSNAPPROVAL");
                }
            }
            if (isprsnapproval/*isuserapproval*/ == 1)
            {
                S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelApproval", "VISIBLE", "TRUE");
            }
            else
            {
                S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelApproval", "VISIBLE", "FALSE");
            }

            if (prcruleFilter > 0 && prcruleTrdrFilter > 0)
            {
                S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelPrTrdr", "VISIBLE", "TRUE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.PRCRULEDISC1PRC", "VISIBLE", "TRUE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.LEADCHARGE", "VISIBLE", "TRUE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.PRCRULEPRICE", "VISIBLE", "TRUE");
            }
            else
            {
                S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelPrTrdr", "VISIBLE", "FALSE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.PRCRULEDISC1PRC", "VISIBLE", "FALSE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.LEADCHARGE", "VISIBLE", "FALSE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.PRCRULEPRICE", "VISIBLE", "FALSE");
            }

            if (status == 3 || status == 4 || status == 5)
            {
                /*22/03/2021
                if (status == 4 || status == 5)
                {
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.STATUS", "READONLY", "TRUE");
                }
                else
                {
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.STATUS", "READONLY", "FALSE");
                }
                */
                //22/03/2021
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.STATUS", "READONLY", "TRUE");

                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.TYPEPRICE", "READONLY", "TRUE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.TRNDATEISACT", "READONLY", "TRUE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.FINDOC", "READONLY", "TRUE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.FROMDATEL", "READONLY", "TRUE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.FROMDATEH", "READONLY", "TRUE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.PRCRULE", "READONLY", "TRUE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.TRDRPRCRULE", "READONLY", "TRUE");
                //S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.USERAPPROVAL", "READONLY", "TRUE");
                //S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.RSNAPPROVAL", "READONLY", "TRUE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.BEAPPROVED", "READONLY", "TRUE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.SELECTALL", "READONLY", "TRUE");

                /*22/03/2021
                if (isprsnapproval == 1 && status == 3)
                {
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.NMARKUPW", "READONLY", "FALSE");
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.NMARKUPR", "READONLY", "FALSE");
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.ISCHANGE", "READONLY", "FALSE");
                }
                else
                {
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.NMARKUPW", "READONLY", "TRUE");
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.NMARKUPR", "READONLY", "TRUE");
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.ISCHANGE", "READONLY", "TRUE");
                }
                */

                //22/03/2021
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.NMARKUPW", "READONLY", "TRUE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.NMARKUPR", "READONLY", "TRUE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.ISCHANGE", "READONLY", "TRUE");

                //XModule.SetProperty("EDITOPTIONS", "READONLY=TRUE");
            }
            else
            {
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.STATUS", "READONLY", "FALSE");
                
                if (status == 2)
                {
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.TYPEPRICE", "READONLY", "TRUE");
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.TRNDATEISACT", "READONLY", "TRUE");
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.FINDOC", "READONLY", "TRUE");
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.FROMDATEL", "READONLY", "TRUE");
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.FROMDATEH", "READONLY", "TRUE");
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.PRCRULE", "READONLY", "TRUE");
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.TRDRPRCRULE", "READONLY", "TRUE");
                }
                else
                {
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.TYPEPRICE", "READONLY", "FALSE");
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.TRNDATEISACT", "READONLY", "FALSE");
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.FINDOC", "READONLY", "FALSE");
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.FROMDATEL", "READONLY", "FALSE");
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.FROMDATEH", "READONLY", "FALSE");
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.PRCRULE", "READONLY", "FALSE");
                    S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.TRDRPRCRULE", "READONLY", "FALSE");
                }
                
                //S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.USERAPPROVAL", "READONLY", "FALSE");
                //S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.RSNAPPROVAL", "READONLY", "FALSE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.BEAPPROVED", "READONLY", "FALSE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICEH.SELECTALL", "READONLY", "FALSE");

                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.NMARKUPW", "READONLY", "FALSE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.NMARKUPR", "READONLY", "FALSE");
                S1Tools.SetPropertyCustom(XModule, "FIELD", "CCCCALCSALPRICED.ISCHANGE", "READONLY", "FALSE");
                //XModule.SetProperty("EDITOPTIONS", "READONLY=FALSE");
            }
            base.AfterLocate();
        }

        private void On_CccCalcSalPriceH_BeApproved(object Sender, XEventArgs e)
        {
            var CalcSalPrisceH = XModule.GetTable("CCCCALCSALPRICEH");
            var BeApproved = Convert.ToInt32(CalcSalPrisceH.Current["BEAPPROVED"]);

            var checkquery = $@"SELECT PRSN,CODE,NAME,NAME2,USERS,ISNULL(CCCPRSNAPPROVAL,0) AS CCCPRSNAPPROVAL FROM PRSN
                                WHERE COMPANY={XSupport.ConnectionInfo.CompanyId} AND ISACTIVE=1 AND SODTYPE=20 AND USERS ={XSupport.ConnectionInfo.UserId} AND ISNULL(CCCPRSNAPPROVAL,0) =1";
            var userapproval = 0;
            var prsnapproval = 0;
            using (XTable checkds = XSupport.GetSQLDataSet(checkquery))
            {
                if (checkds.Count > 0)
                {
                    userapproval = checkds.GetAsInteger(0, "USERS");
                    prsnapproval = checkds.GetAsInteger(0, "PRSN");
                }
            }

            if (BeApproved == 1)
            {
                //CalcSalPrisceH.Current["USERAPPROVAL"] = XSupport.ConnectionInfo.UserId;
                CalcSalPrisceH.Current["USERAPPROVAL"] = userapproval;
                CalcSalPrisceH.Current["PRSNAPPROVAL"] = prsnapproval;
                CalcSalPrisceH.Current["STATUS"] = 3;
            }
            else
            {
                CalcSalPrisceH.Current["USERAPPROVAL"] = (int?)null;
                CalcSalPrisceH.Current["PRSNAPPROVAL"] = (int?)null;
                CalcSalPrisceH.Current["STATUS"] = 1;
                XModule.FocusField("CCCCALCSALPRICEH.USERAPPROVAL");
            }
        }

        private void On_CccCalcSalPriceD_BeforeDelete(object Sender, XEventArgs e)
        {
            var CalcSalPrisceH = XModule.GetTable("CCCCALCSALPRICEH");
            var status = Convert.ToInt32(CalcSalPrisceH.Current["STATUS"]);
            if (status == 3 || status == 4 || status == 5)
            {
                XSupport.Exception("Δεν επιτρέπεται η διαγραφή γραμμών στον πίνακα των αποτελεσμάτων όταν η κατάσταση της εγγραφής είναι «Εγκρίθηκε» ή «Ενημερώθηκε» ή «Ακυρώθηκε».");
            }
        }

        private void On_CccCalcSalPriceH_TypePrice(object Sender, XEventArgs e)
        {
            var CalcSalPrisceH = XModule.GetTable("CCCCALCSALPRICEH");
            var CalcSalPrisceD = XModule.GetTable("CCCCALCSALPRICED");
            var Status = Convert.ToInt32(CalcSalPrisceH.Current["STATUS"]);
            var TypePrice = Convert.ToInt32(CalcSalPrisceH.Current["TYPEPRICE"] != DBNull.Value ? CalcSalPrisceH.Current["TYPEPRICE"] : 0);
            if (CalcSalPrisceD.Count > 0)
            {
                var tabletypeprice = Convert.ToInt32(CalcSalPrisceD.Current["TYPEPRICE"]);
                if (TypePrice != tabletypeprice)
                {
                    XSupport.Warning("Έχετε πραγματοποιήσει αλλαγή «Τιμής Υπολογισμού» και θα πραγματοποιηθεί εκ νέου υπολογισμός των τιμών με βάση τα φίλτρα που θέσατε.");
                    ExecCommand(27012021);
                }
            }
        }
        
        /*
        private void On_CccCalcSalPriceH_Status(object Sender, XEventArgs e)
        {
            var CalcSalPrisceH = XModule.GetTable("CCCCALCSALPRICEH");
            var status = Convert.ToInt32(CalcSalPrisceH.Current["STATUS"]);
        }
        */
        /*
        private void On_CccCalcSalPriceH_Findoc(object Sender, XEventArgs e)
        {
            var CalcSalPrisceH = XModule.GetTable("CCCCALCSALPRICEH");
            var findocFilter = Convert.ToInt32(CalcSalPrisceH.Current["FINDOC"] != DBNull.Value ? CalcSalPrisceH.Current["FINDOC"] : 0);
            if (findocFilter > 0)
            {
                CalcSalPrisceH.Current["FROMDATEL"] = (DateTime?)null;// DBNull.Value;
                CalcSalPrisceH.Current["FROMDATEH"] = (DateTime?)null;// DBNull.Value;
                CalcSalPrisceH.Current["PRCRULE"] = (int?)null;// DBNull.Value;
            }
        }
        */
        /*
        private void On_CccCalcSalPriceH_FromDateL(object Sender, XEventArgs e)
        {
            var CalcSalPrisceH = XModule.GetTable("CCCCALCSALPRICEH");
            var mindate = Convert.ToDateTime("31/12/1899");
            var fromDateLFilter = Convert.ToDateTime(CalcSalPrisceH.Current["FROMDATEL"] != DBNull.Value ? CalcSalPrisceH.Current["FROMDATEL"] : mindate).ToString("yyyyMMdd");
            if (fromDateLFilter != "18991231")
            {
                XModule.GetTable("CCCCALCSALPRICEH").Current["FINDOC"] = null;// DBNull.Value;
                CalcSalPrisceH.Current["FROMDATEH"] = (DateTime?)null;// DBNull.Value;
                CalcSalPrisceH.Current["PRCRULE"] = (int?)null;//DBNull.Value;
            }
        }
        */
        /*
        private void On_CccCalcSalPriceH_FromDateH(object Sender, XEventArgs e)
        {
            var CalcSalPrisceH = XModule.GetTable("CCCCALCSALPRICEH");
            var mindate = Convert.ToDateTime("31/12/1899");
            var fromDateHFilter = Convert.ToDateTime(CalcSalPrisceH.Current["FROMDATEH"] != DBNull.Value ? CalcSalPrisceH.Current["FROMDATEH"] : mindate).ToString("yyyyMMdd");
            if (fromDateHFilter != "18991231")
            {
                CalcSalPrisceH.Current["FINDOC"] = (int?)null;// DBNull.Value;
                CalcSalPrisceH.Current["FROMDATEL"] = (DateTime?)null;// DBNull.Value;
                CalcSalPrisceH.Current["PRCRULE"] = (int?)null;// DBNull.Value;
            }
        }
        */
        /*
        private void On_CccCalcSalPriceH_Prcrule(object Sender, XEventArgs e)
        {
            var CalcSalPrisceH = XModule.GetTable("CCCCALCSALPRICEH");
            var prcruleFilter = Convert.ToInt32(CalcSalPrisceH.Current["PRCRULE"] != DBNull.Value ? CalcSalPrisceH.Current["PRCRULE"] : 0);
            if (prcruleFilter > 0)
            {
                CalcSalPrisceH.Current["FROMDATEL"] = (DateTime?)null;// DBNull.Value;
                CalcSalPrisceH.Current["FROMDATEH"] = (DateTime?)null;// DBNull.Value;
                CalcSalPrisceH.Current["FINDOC"] = (int?)null;// DBNull.Value;
            }
        }
        */

        private void On_CccCalcSalPriceH_SelectAll(object Sender, XEventArgs e)
        {
            var CalcSalPrisceH = XModule.GetTable("CCCCALCSALPRICEH");
            var CalcSalPrisceD = XModule.GetTable("CCCCALCSALPRICED");
            var selectall = Convert.ToInt32(CalcSalPrisceH.Current["SELECTALL"] != DBNull.Value ? CalcSalPrisceH.Current["SELECTALL"] : 0);

            if(CalcSalPrisceD.Count>0)
            {
                for (int i = 0; i < CalcSalPrisceD.Count; i++)
                {
                    CalcSalPrisceD.Current.Edit(i);
                    CalcSalPrisceD.Current["ISCHANGE"] = selectall;
                }
            }
        }

        public override object ExecCommand(int Cmd)
        {
            if (Cmd == 27012021) //Επιβεβαίωση Φίλτρων
            {
                /*
                //Υπολογισμός τιμών κόστους
                try
                {
                    using (var CalcCost = XSupport.CreateModule("ITECALCCOST"))
                    {
                        CalcCost.InsertData();
                        var TblIteCost = CalcCost.GetTable("MTRDIALOG");
                        TblIteCost.Current["FROMCODE"] = "";
                        TblIteCost.Current["CALCPRCS"] = "5";
                        CalcCost.BatchExecute();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                */

                var CalcSalPrisceH = XModule.GetTable("CCCCALCSALPRICEH");
                var CalcSalPrisceHID = Convert.ToInt32(CalcSalPrisceH.Current["CCCCALCSALPRICEH"]);
                var CalcSalPrisceD = XModule.GetTable("CCCCALCSALPRICED");
                var TypePrice = Convert.ToInt32(CalcSalPrisceH.Current["TYPEPRICE"] != DBNull.Value ? CalcSalPrisceH.Current["TYPEPRICE"] : 0);
                var findocFilter = Convert.ToInt32(CalcSalPrisceH.Current["FINDOC"]!= DBNull.Value ? CalcSalPrisceH.Current["FINDOC"] : 0);
                var mindate = Convert.ToDateTime("30/12/1899");
                var fromDateLFilter = Convert.ToDateTime(CalcSalPrisceH.Current["FROMDATEL"] != DBNull.Value ? CalcSalPrisceH.Current["FROMDATEL"] : mindate).ToString("yyyyMMdd");
                var fromDateHFilter = Convert.ToDateTime(CalcSalPrisceH.Current["FROMDATEH"] != DBNull.Value ? CalcSalPrisceH.Current["FROMDATEH"] : DateTime.MaxValue).ToString("yyyyMMdd");
                var prcruleFilter = Convert.ToInt32(CalcSalPrisceH.Current["PRCRULE"] != DBNull.Value ? CalcSalPrisceH.Current["PRCRULE"] :0);
                var prcruleTrdrFilter = Convert.ToInt32(CalcSalPrisceH.Current["TRDRPRCRULE"] != DBNull.Value ? CalcSalPrisceH.Current["TRDRPRCRULE"] : 0);
                var status = Convert.ToInt32(CalcSalPrisceH.Current["STATUS"]);
                var fromdate = "";
                var todate = "";
                var findoc = "";

                if (CalcSalPrisceH.Current["FROMDATEL"] != DBNull.Value)
                {
                    fromdate = $" AND A.TRNDATE >='{fromDateLFilter}' ";
                }
                else
                {
                    fromdate = "";
                }
                if (CalcSalPrisceH.Current["FROMDATEH"] != DBNull.Value)
                {
                    todate = $" AND A.TRNDATE <='{fromDateHFilter}' ";
                }
                else
                {
                    todate = "";
                }
                if (CalcSalPrisceH.Current["FINDOC"] != DBNull.Value)
                {
                    findoc = $" AND A.FINDOC ={findocFilter} ";
                }
                else
                {
                    findoc = "";
                }

                if (status == 3 || status == 4 || status == 5)
                {
                    XSupport.Exception("Δεν επιτρέπεται η εκτέλεση των φίλτρων όταν η κατάσταση της εγγραφής είναι  «Εγκρίθηκε» ή «Ενημερώθηκε» ή «Ακυρώθηκε».");
                }
                else
                {
                    if (CalcSalPrisceH.Current["FROMDATEL"] != DBNull.Value || CalcSalPrisceH.Current["FROMDATEH"] != DBNull.Value 
                        || CalcSalPrisceH.Current["FINDOC"] != DBNull.Value || CalcSalPrisceH.Current["PRCRULE"] != DBNull.Value)
                    {

                        if (/*CalcSalPrisceHID < 0 &&*/ CalcSalPrisceD.Count > 0)
                        {
                            while (CalcSalPrisceD.Count > 0)
                            {
                                CalcSalPrisceD.Current.Delete();
                            }
                        }

                        if (CalcSalPrisceD.Count == 0)
                        {
                            if (TypePrice != 0)
                            {
                                var query = "";
                                if (prcruleFilter > 0)
                                {
                                    if (prcruleTrdrFilter > 0)
                                    {
                                        S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelGridSel", "VISIBLE", "TRUE");
                                        query = $@"SELECT A.DIM2 AS MTRL,M.CODE,M.NAME,
                                            [dbo].[fnGetCostPrice]({XSupport.ConnectionInfo.CompanyId},A.DIM2,{XSupport.ConnectionInfo.YearId},{XSupport.ConnectionInfo.PeriodId},5) AS PURLPRICE,
                                            /*ISNULL(P.PURLPRICE,0.0) AS PURLPRICE,P.PURLPDATE, */
                                            (SELECT TOP 1 CLCDATE FROM ( SELECT MTRL,MAX(CLCDATE) OVER (PARTITION BY MTRL ORDER BY CLCDATE DESC) AS CLCDATE
                                            FROM MTRCPRICES WHERE COMPANY = 1001 AND MTRL= A.DIM2 AND ISNULL(PURLPRICE,0) <>0 GROUP BY CLCDATE,MTRL) WW) AS CLCDATE,
                                            0.0 AS PRICEKTISIS, 

                                            /*
                                            CASE WHEN ISNULL(A.FLD01,0)=0 THEN (SELECT TOP 1 ISNULL(FLD01,0) FROM (SELECT *,MAX(A1.FROMDATE) OVER(PARTITION BY A1.DIM1,A1.DIM2) AS MAXFROMDATE FROM PRCRDATALNS A1 WHERE A1.PRCRULE = A.PRCRULE) W 
                                            WHERE FROMDATE = MAXFROMDATE AND DIM1 = A.DIM1 AND DIM2=A.DIM2) ELSE ISNULL(A.FLD01,0) END AS PRCRULEPRICE,
                                            CASE WHEN ISNULL(A.FLD02,0)=0 THEN (SELECT TOP 1 ISNULL(FLD02,0) FROM (SELECT *,MAX(A1.FROMDATE) OVER(PARTITION BY A1.DIM1,A1.DIM2) AS MAXFROMDATE FROM PRCRDATALNS A1 WHERE A1.PRCRULE = A.PRCRULE) W 
                                            WHERE FROMDATE = MAXFROMDATE AND DIM1 = A.DIM1 AND DIM2=A.DIM2) ELSE ISNULL(A.FLD02,0) END AS PRCRULEDISC1PRC,
                                            */

                                            CASE WHEN ISNULL(A.FLD01,0)=0 THEN ISNULL(PLNS.FLD01,0) ELSE ISNULL(A.FLD01,0) END AS PRCRULEPRICE,
                                            CASE WHEN ISNULL(A.FLD02,0)=0 THEN ISNULL(PLNS.FLD02,0) ELSE ISNULL(A.FLD02,0) END AS PRCRULEDISC1PRC,

                                            ISNULL(LG.FLD01,0) AS LEADCHARGE,
                                            ISNULL(PW.FLD01, 0.0) AS PRCRULEPRICEW,
                                            ISNULL(PR.FLD01, 0.0) AS PRCRULEPRICER,
                                            ISNULL(M.MARKUPW, 0.0) AS MARKUPW,
                                            ISNULL(M.MARKUPR, 0.0) AS MARKUPR
                                            FROM PRCRDATA A
                                            INNER JOIN MTRL M ON M.MTRL = A.DIM2
                                            /*
                                            LEFT JOIN
                                            (SELECT* FROM (SELECT A1.COMPANY, A1.MTRL, A1.FISCPRD, A1.PURLPRICE, A1.PURLPDATE, MAX(A1.FISCPRD) OVER(PARTITION BY A1.MTRL) AS MAXPURFISCPRD FROM MTRDATA A1) W WHERE FISCPRD = MAXPURFISCPRD) P ON P.MTRL = A.DIM2 AND P.COMPANY = A.COMPANY
                                            */
                                            LEFT JOIN
                                            (SELECT* FROM (SELECT*, MAX(A1.FROMDATE) OVER(PARTITION BY A1.DIM1) AS MAXFROMDATE FROM PRCRDATA A1 WHERE A1.PRCRULE = 2) W WHERE FROMDATE = MAXFROMDATE) PW ON PW.DIM1 = A.DIM2 AND PW.COMPANY = A.COMPANY /*ΤΙΜΟΚΑΤΑΛΟΓΟΣ ΧΟΝΤΡΙΚΗΣ*/
                                            LEFT JOIN
                                            (SELECT* FROM (SELECT*, MAX(A1.FROMDATE) OVER(PARTITION BY A1.DIM1) AS MAXFROMDATE FROM PRCRDATA A1 WHERE A1.PRCRULE = 3) W WHERE FROMDATE = MAXFROMDATE) PR ON PR.DIM1 = A.DIM2 AND PR.COMPANY = A.COMPANY /*ΤΙΜΟΚΑΤΑΛΟΓΟΣ ΛΙΑΝΙΚΗΣ*/
                                            LEFT JOIN
											(SELECT* FROM (SELECT*, MAX(A1.FROMDATE) OVER(PARTITION BY A1.DIM1,A1.DIM2) AS MAXFROMDATE FROM PRCRDATA A1 WHERE A1.PRCRULE = 9005) W WHERE FROMDATE = MAXFROMDATE) LG ON LG.DIM1 = A.DIM1 AND LG.DIM2 = A.DIM2 AND PR.COMPANY = A.COMPANY /*Lead Charge*/
                                            
                                            LEFT JOIN 
                                            (SELECT * FROM (SELECT *,MAX(A1.FROMDATE) OVER(PARTITION BY A1.DIM1,A1.DIM2) AS MAXFROMDATE FROM PRCRDATALNS A1 WHERE A1.PRCRULE = {prcruleFilter}) W 
                                            WHERE W.FROMDATE = MAXFROMDATE
                                            ) PLNS ON PLNS.DIM1 = A.DIM1 AND PLNS.DIM2 = A.DIM2 AND PLNS.COMPANY = A.COMPANY
                                            WHERE

                                            A.COMPANY = {XSupport.ConnectionInfo.CompanyId} AND A.SODTYPE = 12 AND A.PRCRULE ={prcruleFilter} AND A.DIM1 = {prcruleTrdrFilter}
                                            GROUP BY A.DIM2,M.PRICEW,M.PRICER,M.MARKUPW,M.MARKUPR,M.CODE,M.NAME,
                                            /*P.PURLPRICE,P.PURLPDATE,*/
                                            PW.FLD01,PR.FLD01,A.FLD01,A.FLD02,A.PRCRULE,A.DIM1,
                                            LG.FLD01,PLNS.FLD01,PLNS.FLD02
                                            ORDER BY A.DIM2";
                                    }
                                    else
                                    {
                                        XSupport.Warning("Για να εμφανιστούν τα αποτελέσματα θα πρέπει να έχετε συμπληρώσει τον «Προμηθευτή Τιμοκαταλόγου», εφόσον έχετε επιλέξει «Τιμοκατάλογο».");
                                    }
                                }
                                else
                                {
                                    query = $@"SELECT A.MTRL,M.CODE,M.NAME,
                                        [dbo].[fnGetCostPrice]({XSupport.ConnectionInfo.CompanyId},A.MTRL,{XSupport.ConnectionInfo.YearId},{XSupport.ConnectionInfo.PeriodId},5) AS PURLPRICE,

                                        /*
                                        ISNULL(P.PURLPRICE,0.0) AS PURLPRICE,P.PURLPDATE,*/ 

                                        (SELECT TOP 1 CLCDATE FROM ( SELECT MTRL,MAX(CLCDATE) OVER (PARTITION BY MTRL ORDER BY CLCDATE DESC) AS CLCDATE
                                        FROM MTRCPRICES WHERE COMPANY = 1001 AND MTRL= A.MTRL AND ISNULL(PURLPRICE,0) <>0 GROUP BY CLCDATE,MTRL) WW) AS CLCDATE,
                                        0.0 AS PRICEKTISIS, 0.0 AS PRCRULEPRICE,0.0 AS PRCRULEDISC1PRC,0.0 AS LEADCHARGE,
                                        ISNULL(PW.FLD01, 0.0) AS PRCRULEPRICEW,
                                        ISNULL(PR.FLD01, 0.0) AS PRCRULEPRICER,
                                        ISNULL(M.MARKUPW, 0.0) AS MARKUPW,
                                        ISNULL(M.MARKUPR, 0.0) AS MARKUPR
                                        FROM MTRTRN A
                                        INNER JOIN MTRL M ON M.MTRL = A.MTRL
                                        /*
                                        LEFT JOIN
                                        (SELECT* FROM (SELECT A1.COMPANY, A1.MTRL, A1.FISCPRD, A1.PURLPRICE, A1.PURLPDATE, MAX(A1.FISCPRD) OVER(PARTITION BY A1.MTRL) AS MAXPURFISCPRD FROM MTRDATA A1) W WHERE FISCPRD = MAXPURFISCPRD) P ON P.MTRL = A.MTRL AND P.COMPANY = A.COMPANY
                                        */
                                        LEFT JOIN
                                        (SELECT* FROM (SELECT*, MAX(A1.FROMDATE) OVER(PARTITION BY A1.DIM1) AS MAXFROMDATE FROM PRCRDATA A1 WHERE A1.PRCRULE = 2) W WHERE FROMDATE = MAXFROMDATE) PW ON PW.DIM1 = A.MTRL AND PW.COMPANY = A.COMPANY /*ΤΙΜΟΚΑΤΑΛΟΓΟΣ ΧΟΝΤΡΙΚΗΣ*/
                                        LEFT JOIN
                                        (SELECT* FROM (SELECT*, MAX(A1.FROMDATE) OVER(PARTITION BY A1.DIM1) AS MAXFROMDATE FROM PRCRDATA A1 WHERE A1.PRCRULE = 3) W WHERE FROMDATE = MAXFROMDATE) PR ON PR.DIM1 = A.MTRL AND PR.COMPANY = A.COMPANY /*ΤΙΜΟΚΑΤΑΛΟΓΟΣ ΛΙΑΝΙΚΗΣ*/
                                        WHERE
                                        A.COMPANY = {XSupport.ConnectionInfo.CompanyId} AND A.SODTYPE = 51 AND A.SOSOURCE = 1251
                                        {fromdate} {todate} {findoc}
                                        GROUP BY
                                        A.MTRL,M.PRICEW,M.PRICER,M.MARKUPW,M.MARKUPR,M.CODE,M.NAME,
                                        /*P.PURLPRICE,P.PURLPDATE,*/
                                        PW.FLD01,PR.FLD01
                                        ORDER BY A.MTRL";
                                }

                                if (query != "")
                                {
                                    using (XTable ds = XSupport.GetSQLDataSet(query))
                                    {
                                        if (ds.Count > 0)
                                        {
                                            /*
                                            var dt = ds.CreateDataTable(true);
                                            frmPrices.dataGridView1.DataSource = dt;

                                            */
                                            
                                            //SetDatasetLinks
                                            var vdsTable = XSupport.CallPublished(XSupport.GetStockObj("ModuleIntf", true), "GetDataset", new object[] { XModule.Handle, "CCCCALCSALPRICED" });
                                            var SetDatasetLinksOff = XSupport.CallPublished(XSupport.GetStockObj("ModuleIntf", true), "SetDatasetLinks", new object[] { XModule.Handle, vdsTable, 0 });

                                            S1Tools.SetPropertyCustom(XModule, "PANEL", "PanelGridSel", "VISIBLE", "TRUE");
                                            var newcalcpricew = (double)0;
                                            var newcalcpricer = (double)0;
                                            for (int i = 0; i < ds.Count; i++)
                                            {
                                                CalcSalPrisceD.Current.Append();
                                                CalcSalPrisceD.Current["TYPEPRICE"] = TypePrice;
                                                CalcSalPrisceD.Current["MTRL"] = ds.GetAsInteger(i, "MTRL");
                                                CalcSalPrisceD.Current["PRICEKTISIS"] = ds.GetAsFloat(i, "PRICEKTISIS");
                                                var clcdate = ds.GetAsDateTime(i, "CLCDATE").ToString("yyyyMMdd");
                                                if (clcdate != "18991230")
                                                {
                                                    CalcSalPrisceD.Current["CLCDATE"] = ds.GetAsDateTime(i, "CLCDATE");
                                                }
                                                CalcSalPrisceD.Current["PURLPRICE"] = ds.GetAsFloat(i, "PURLPRICE");

                                                CalcSalPrisceD.Current["PRCRULEPRICE"] = ds.GetAsFloat(i, "PRCRULEPRICE");
                                                CalcSalPrisceD.Current["PRCRULEDISC1PRC"] = ds.GetAsFloat(i, "PRCRULEDISC1PRC");
                                                CalcSalPrisceD.Current["LEADCHARGE"] = ds.GetAsFloat(i, "LEADCHARGE");
                                                CalcSalPrisceD.Current["PRCRULEPRICEW"] = ds.GetAsFloat(i, "PRCRULEPRICEW");
                                                CalcSalPrisceD.Current["PRCRULEPRICER"] = ds.GetAsFloat(i, "PRCRULEPRICER");
                                                CalcSalPrisceD.Current["MARKUPW"] = ds.GetAsFloat(i, "MARKUPW");
                                                CalcSalPrisceD.Current["MARKUPR"] = ds.GetAsFloat(i, "MARKUPR");

                                                switch (TypePrice)
                                                {
                                                    case 1: //Τιμή Κτήσης
                                                        newcalcpricew = Math.Round(ds.GetAsFloat(i, "PRICEKTISIS") + (ds.GetAsFloat(i, "PRICEKTISIS") * (ds.GetAsFloat(i, "MARKUPW") / 100)), 2);
                                                        newcalcpricer = Math.Round(ds.GetAsFloat(i, "PRICEKTISIS") + (ds.GetAsFloat(i, "PRICEKTISIS") * (ds.GetAsFloat(i, "MARKUPR") / 100)), 2);
                                                        break;
                                                    case 2: //Τιμή Αγοράς
                                                        newcalcpricew = Math.Round(ds.GetAsFloat(i, "PURLPRICE") + (ds.GetAsFloat(i, "PURLPRICE") * (ds.GetAsFloat(i, "MARKUPW") / 100)), 2);
                                                        newcalcpricer = Math.Round(ds.GetAsFloat(i, "PURLPRICE") + (ds.GetAsFloat(i, "PURLPRICE") * (ds.GetAsFloat(i, "MARKUPR") / 100)), 2);
                                                        break;
                                                    case 3: //Τιμή Τιμοκαταλόγου
                                                        var newprcruleprice = ((ds.GetAsFloat(i, "PRCRULEPRICE")
                                                                            - (ds.GetAsFloat(i, "PRCRULEPRICE") * (ds.GetAsFloat(i, "PRCRULEDISC1PRC") / 100)))
                                                                            + ds.GetAsFloat(i, "LEADCHARGE"));

                                                        //newcalcpricew = Math.Round(ds.GetAsFloat(i, "PRCRULEPRICE") + (ds.GetAsFloat(i, "PRCRULEPRICE") * (ds.GetAsFloat(i, "MARKUPW") / 100)), 2);
                                                        //newcalcpricer = Math.Round(ds.GetAsFloat(i, "PRCRULEPRICE") + (ds.GetAsFloat(i, "PRCRULEPRICE") * (ds.GetAsFloat(i, "MARKUPR") / 100)), 2);
                                                        newcalcpricew = Math.Round(newprcruleprice + (newprcruleprice*(ds.GetAsFloat(i, "MARKUPW") / 100)), 2);
                                                        newcalcpricer = Math.Round(newprcruleprice + (newprcruleprice * (ds.GetAsFloat(i, "MARKUPR") / 100)), 2);
                                                        break;
                                                }

                                                CalcSalPrisceD.Current["CALMARKUPW"] = newcalcpricew;
                                                CalcSalPrisceD.Current["CALMARKUPR"] = newcalcpricer;
                                                CalcSalPrisceD.Current["NMARKUPW"] = newcalcpricew;
                                                CalcSalPrisceD.Current["NMARKUPR"] = newcalcpricer;
                                                CalcSalPrisceD.Current.Post();
                                            }

                                            var SetDatasetLinksOn = XSupport.CallPublished(XSupport.GetStockObj("ModuleIntf", true), "SetDatasetLinks", new object[] { XModule.Handle, vdsTable, 1 });
                                        }
                                        else
                                        {
                                            XSupport.Warning("Δεν υπάρχουν αποτελέσματα για τα φίλτρα που ορίσατε.");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                XSupport.Warning("Για να εμφανιστούν τα αποτελέσματα θα πρέπει να έχετε συμπληρώσει τον «Τιμή Υπολογισμού».");
                            }
                        }
                    }
                    else
                    {
                        XSupport.Warning("Για να εμφανιστούν τα αποτελέσματα θα πρέπει να έχετε συμπληρώσει έστω ένα από τα φίλτρα.");
                    }
                }
            }
            return base.ExecCommand(Cmd);
        }
    }
}
