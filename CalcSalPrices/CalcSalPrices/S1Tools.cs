﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softone;

namespace CalcSalPrices
{
    public static class S1Tools
    {
        internal static void HideWarningsFromS1Module(XModule XModule, XSupport XSupport)
        {
            object otherModule = XSupport.GetStockObj("ModuleIntf", true);
            object[] myArray1;
            myArray1 = new object[3];
            myArray1[0] = XModule.Handle;
            myArray1[1] = "WARNINGS";    //Param Name
            myArray1[2] = "OFF";         //Param Value
            XSupport.CallPublished(otherModule, "SetParamValue", myArray1);
        }

        internal static void SetPropertyCustom(XModule XModule,string fieldpanel, string fieldpanelname,string property, string truefalse)
        {
            var myobj = new object[3];
            myobj[0] = fieldpanelname;
            myobj[1] = property;
            myobj[2] = truefalse;
            XModule.SetProperty(fieldpanel, myobj);
        }
    }
}
