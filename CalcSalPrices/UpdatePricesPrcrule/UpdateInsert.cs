﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softone;

namespace UpdatePricesPrcrule
{
    public class UpdateInsert
    {
        XSupport xSupport = null;

        public UpdateInsert(XSupport _xSupport)
        {
            xSupport = _xSupport;
        }
        public void InsertIntoPrcrdata()
        {
            var query = $@"SELECT A.CCCCALCSALPRICEH,A.STATUS,A.TRNDATEISACT,B.MTRL,M.CODE,M.NAME,2 AS PRCRULEW,B.NMARKUPW,3 AS PRCRULER,B.NMARKUPR,ISNULL(P1.FLD01,0.0) AS FLD01W,ISNULL(P2.FLD01,0.0) AS FLD01R
                            FROM CCCCALCSALPRICEH A
                            INNER JOIN CCCCALCSALPRICED B ON A.CCCCALCSALPRICEH=B.CCCCALCSALPRICEH
                            LEFT JOIN MTRL M ON M.MTRL=B.MTRL AND M.COMPANY=A.COMPANY
                            LEFT JOIN PRCRDATA P1 ON P1.DIM1=B.MTRL AND P1.PRCRULE=2 AND P1.SODTYPE=13 AND P1.SOTYPE=1 AND P1.DIM2 =2 AND P1.FROMDATE=A.TRNDATEISACT /*ΤΙΜΟΚΑΤΑΛΟΓΟΣ ΧΟΝΤΡΙΚΗΣ*/
                            LEFT JOIN PRCRDATA P2 ON P2.DIM1=B.MTRL AND P2.PRCRULE=3 AND P2.SODTYPE=13 AND P2.SOTYPE=1 AND P2.DIM2 =1 AND P2.FROMDATE=A.TRNDATEISACT /*ΤΙΜΟΚΑΤΑΛΟΓΟΣ ΛΙΑΝΙΚΗΣ*/
                            WHERE A.COMPANY ={xSupport.ConnectionInfo.CompanyId} AND A.BEAPPROVED = 1 AND B.ISCHANGE = 1 AND A.STATUS = 3 AND 1=1";
            try
            {
                using (XTable ds = xSupport.GetSQLDataSet(query))
                {
                    if (ds.Count > 0)
                    {
                        List<int> IdsList = new List<int>();
                        var totalquery = "";
                        for (int i = 0; i < ds.Count; i++)
                        {
                            IdsList.Add(ds.GetAsInteger(i, "CCCCALCSALPRICEH"));

                            if (ds.GetAsFloat(i, "FLD01W") > 0.0) /*ΤΙΜΟΚΑΤΑΛΟΓΟΣ ΧΟΝΤΡΙΚΗΣ*/
                            {
                                var query1 = $@"UPDATE PRCRDATA SET FLD01={ds.GetAsFloat(i, "NMARKUPW").ToString().Replace(",",".")} WHERE COMPANY={xSupport.ConnectionInfo.CompanyId}  AND SODTYPE=13  AND SOTYPE=1  
                                                AND PRCRULE={ds.GetAsInteger(i, "PRCRULEW")}  AND DIM1={ds.GetAsInteger(i, "MTRL")}  AND DIM2=2  AND DIM3=0 AND FROMDATE='{ds.GetAsDateTime(i, "TRNDATEISACT").ToString("yyyyMMdd")}'";
                                totalquery = totalquery + System.Environment.NewLine + query1;
                            }
                            else
                            {
                                var query2 = $@"INSERT INTO PRCRDATA (COMPANY,SODTYPE,SOTYPE,PRCRULE,DIM1,DIM2,DIM3,FROMDATE,LOCKID,FLD01) VALUES ({xSupport.ConnectionInfo.CompanyId} ,13 ,1 ,{ds.GetAsInteger(i, "PRCRULEW")} ,{ds.GetAsInteger(i, "MTRL")},2 ,0 ,'{ds.GetAsDateTime(i, "TRNDATEISACT").ToString("yyyyMMdd")}' ,0 ,{ds.GetAsFloat(i, "NMARKUPW").ToString().Replace(",", ".")} )";
                                totalquery = totalquery + System.Environment.NewLine + query2;
                            }

                            if (ds.GetAsFloat(i, "FLD01R") > 0.0) /*ΤΙΜΟΚΑΤΑΛΟΓΟΣ ΛΙΑΝΙΚΗΣ*/
                            {
                                var query3 = $@"UPDATE PRCRDATA SET FLD01={ds.GetAsFloat(i, "NMARKUPR").ToString().Replace(",", ".")} WHERE COMPANY={xSupport.ConnectionInfo.CompanyId}  AND SODTYPE=13  AND SOTYPE=1  
                                                AND PRCRULE={ds.GetAsInteger(i, "PRCRULER")}  AND DIM1={ds.GetAsInteger(i, "MTRL")}  AND DIM2=1  AND DIM3=0 AND FROMDATE='{ds.GetAsDateTime(i, "TRNDATEISACT").ToString("yyyyMMdd")}'";
                                totalquery = totalquery + System.Environment.NewLine + query3;
                            }
                            else
                            {
                                var query4 = $@"INSERT INTO PRCRDATA (COMPANY,SODTYPE,SOTYPE,PRCRULE,DIM1,DIM2,DIM3,FROMDATE,LOCKID,FLD01) VALUES ({xSupport.ConnectionInfo.CompanyId} ,13 ,1 ,{ds.GetAsInteger(i, "PRCRULER")} ,{ds.GetAsInteger(i, "MTRL")},1 ,0 ,'{ds.GetAsDateTime(i, "TRNDATEISACT").ToString("yyyyMMdd")}' ,0 ,{ds.GetAsFloat(i, "NMARKUPR").ToString().Replace(",", ".")} )";
                                totalquery = totalquery + System.Environment.NewLine + query4;
                            }
                        }

                        if (totalquery != "")
                        {
                            var distinctedList = IdsList.Distinct().ToList();
                            var ids = string.Join(",", distinctedList);
                            var updateTable = $@"UPDATE CCCCALCSALPRICEH SET STATUS = 4 WHERE COMPANY ={xSupport.ConnectionInfo.CompanyId} AND CCCCALCSALPRICEH IN ({ids})";
                            xSupport.ExecuteSQL(totalquery);
                            xSupport.ExecuteSQL(updateTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }
    }
}
